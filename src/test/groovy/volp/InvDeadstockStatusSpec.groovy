package volp

import grails.testing.gorm.DomainUnitTest
import spock.lang.Specification

class InvDeadstockStatusSpec extends Specification implements DomainUnitTest<InvDeadstockStatus> {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        expect:"fix me"
            true == false
    }
}
