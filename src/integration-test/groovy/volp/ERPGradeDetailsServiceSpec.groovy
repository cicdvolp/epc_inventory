package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPGradeDetailsServiceSpec extends Specification {

    ERPGradeDetailsService ERPGradeDetailsService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPGradeDetails(...).save(flush: true, failOnError: true)
        //new ERPGradeDetails(...).save(flush: true, failOnError: true)
        //ERPGradeDetails ERPGradeDetails = new ERPGradeDetails(...).save(flush: true, failOnError: true)
        //new ERPGradeDetails(...).save(flush: true, failOnError: true)
        //new ERPGradeDetails(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPGradeDetails.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPGradeDetailsService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPGradeDetails> ERPGradeDetailsList = ERPGradeDetailsService.list(max: 2, offset: 2)

        then:
        ERPGradeDetailsList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPGradeDetailsService.count() == 5
    }

    void "test delete"() {
        Long ERPGradeDetailsId = setupData()

        expect:
        ERPGradeDetailsService.count() == 5

        when:
        ERPGradeDetailsService.delete(ERPGradeDetailsId)
        sessionFactory.currentSession.flush()

        then:
        ERPGradeDetailsService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPGradeDetails ERPGradeDetails = new ERPGradeDetails()
        ERPGradeDetailsService.save(ERPGradeDetails)

        then:
        ERPGradeDetails.id != null
    }
}
