package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPExamMarksImportMethodMatrixServiceSpec extends Specification {

    ERPExamMarksImportMethodMatrixService ERPExamMarksImportMethodMatrixService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPExamMarksImportMethodMatrix(...).save(flush: true, failOnError: true)
        //new ERPExamMarksImportMethodMatrix(...).save(flush: true, failOnError: true)
        //ERPExamMarksImportMethodMatrix ERPExamMarksImportMethodMatrix = new ERPExamMarksImportMethodMatrix(...).save(flush: true, failOnError: true)
        //new ERPExamMarksImportMethodMatrix(...).save(flush: true, failOnError: true)
        //new ERPExamMarksImportMethodMatrix(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPExamMarksImportMethodMatrix.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPExamMarksImportMethodMatrixService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPExamMarksImportMethodMatrix> ERPExamMarksImportMethodMatrixList = ERPExamMarksImportMethodMatrixService.list(max: 2, offset: 2)

        then:
        ERPExamMarksImportMethodMatrixList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPExamMarksImportMethodMatrixService.count() == 5
    }

    void "test delete"() {
        Long ERPExamMarksImportMethodMatrixId = setupData()

        expect:
        ERPExamMarksImportMethodMatrixService.count() == 5

        when:
        ERPExamMarksImportMethodMatrixService.delete(ERPExamMarksImportMethodMatrixId)
        sessionFactory.currentSession.flush()

        then:
        ERPExamMarksImportMethodMatrixService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPExamMarksImportMethodMatrix ERPExamMarksImportMethodMatrix = new ERPExamMarksImportMethodMatrix()
        ERPExamMarksImportMethodMatrixService.save(ERPExamMarksImportMethodMatrix)

        then:
        ERPExamMarksImportMethodMatrix.id != null
    }
}
