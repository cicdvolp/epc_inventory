package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPSelfAppraisalParameterAuthorityServiceSpec extends Specification {

    ERPSelfAppraisalParameterAuthorityService ERPSelfAppraisalParameterAuthorityService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPSelfAppraisalParameterAuthority(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalParameterAuthority(...).save(flush: true, failOnError: true)
        //ERPSelfAppraisalParameterAuthority ERPSelfAppraisalParameterAuthority = new ERPSelfAppraisalParameterAuthority(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalParameterAuthority(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalParameterAuthority(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPSelfAppraisalParameterAuthority.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPSelfAppraisalParameterAuthorityService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPSelfAppraisalParameterAuthority> ERPSelfAppraisalParameterAuthorityList = ERPSelfAppraisalParameterAuthorityService.list(max: 2, offset: 2)

        then:
        ERPSelfAppraisalParameterAuthorityList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPSelfAppraisalParameterAuthorityService.count() == 5
    }

    void "test delete"() {
        Long ERPSelfAppraisalParameterAuthorityId = setupData()

        expect:
        ERPSelfAppraisalParameterAuthorityService.count() == 5

        when:
        ERPSelfAppraisalParameterAuthorityService.delete(ERPSelfAppraisalParameterAuthorityId)
        sessionFactory.currentSession.flush()

        then:
        ERPSelfAppraisalParameterAuthorityService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPSelfAppraisalParameterAuthority ERPSelfAppraisalParameterAuthority = new ERPSelfAppraisalParameterAuthority()
        ERPSelfAppraisalParameterAuthorityService.save(ERPSelfAppraisalParameterAuthority)

        then:
        ERPSelfAppraisalParameterAuthority.id != null
    }
}
