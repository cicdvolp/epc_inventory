package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPStudentFeesOnlineTransactionServiceSpec extends Specification {

    ERPStudentFeesOnlineTransactionService ERPStudentFeesOnlineTransactionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPStudentFeesOnlineTransaction(...).save(flush: true, failOnError: true)
        //new ERPStudentFeesOnlineTransaction(...).save(flush: true, failOnError: true)
        //ERPStudentFeesOnlineTransaction ERPStudentFeesOnlineTransaction = new ERPStudentFeesOnlineTransaction(...).save(flush: true, failOnError: true)
        //new ERPStudentFeesOnlineTransaction(...).save(flush: true, failOnError: true)
        //new ERPStudentFeesOnlineTransaction(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPStudentFeesOnlineTransaction.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPStudentFeesOnlineTransactionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPStudentFeesOnlineTransaction> ERPStudentFeesOnlineTransactionList = ERPStudentFeesOnlineTransactionService.list(max: 2, offset: 2)

        then:
        ERPStudentFeesOnlineTransactionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPStudentFeesOnlineTransactionService.count() == 5
    }

    void "test delete"() {
        Long ERPStudentFeesOnlineTransactionId = setupData()

        expect:
        ERPStudentFeesOnlineTransactionService.count() == 5

        when:
        ERPStudentFeesOnlineTransactionService.delete(ERPStudentFeesOnlineTransactionId)
        sessionFactory.currentSession.flush()

        then:
        ERPStudentFeesOnlineTransactionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPStudentFeesOnlineTransaction ERPStudentFeesOnlineTransaction = new ERPStudentFeesOnlineTransaction()
        ERPStudentFeesOnlineTransactionService.save(ERPStudentFeesOnlineTransaction)

        then:
        ERPStudentFeesOnlineTransaction.id != null
    }
}
