package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPFeedbackCategoryOfferingServiceSpec extends Specification {

    ERPFeedbackCategoryOfferingService ERPFeedbackCategoryOfferingService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPFeedbackCategoryOffering(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategoryOffering(...).save(flush: true, failOnError: true)
        //ERPFeedbackCategoryOffering ERPFeedbackCategoryOffering = new ERPFeedbackCategoryOffering(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategoryOffering(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategoryOffering(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPFeedbackCategoryOffering.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPFeedbackCategoryOfferingService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPFeedbackCategoryOffering> ERPFeedbackCategoryOfferingList = ERPFeedbackCategoryOfferingService.list(max: 2, offset: 2)

        then:
        ERPFeedbackCategoryOfferingList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPFeedbackCategoryOfferingService.count() == 5
    }

    void "test delete"() {
        Long ERPFeedbackCategoryOfferingId = setupData()

        expect:
        ERPFeedbackCategoryOfferingService.count() == 5

        when:
        ERPFeedbackCategoryOfferingService.delete(ERPFeedbackCategoryOfferingId)
        sessionFactory.currentSession.flush()

        then:
        ERPFeedbackCategoryOfferingService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPFeedbackCategoryOffering ERPFeedbackCategoryOffering = new ERPFeedbackCategoryOffering()
        ERPFeedbackCategoryOfferingService.save(ERPFeedbackCategoryOffering)

        then:
        ERPFeedbackCategoryOffering.id != null
    }
}
