package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPForeignNationalFeesStudentMasterServiceSpec extends Specification {

    ERPForeignNationalFeesStudentMasterService ERPForeignNationalFeesStudentMasterService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPForeignNationalFeesStudentMaster(...).save(flush: true, failOnError: true)
        //new ERPForeignNationalFeesStudentMaster(...).save(flush: true, failOnError: true)
        //ERPForeignNationalFeesStudentMaster ERPForeignNationalFeesStudentMaster = new ERPForeignNationalFeesStudentMaster(...).save(flush: true, failOnError: true)
        //new ERPForeignNationalFeesStudentMaster(...).save(flush: true, failOnError: true)
        //new ERPForeignNationalFeesStudentMaster(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPForeignNationalFeesStudentMaster.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPForeignNationalFeesStudentMasterService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPForeignNationalFeesStudentMaster> ERPForeignNationalFeesStudentMasterList = ERPForeignNationalFeesStudentMasterService.list(max: 2, offset: 2)

        then:
        ERPForeignNationalFeesStudentMasterList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPForeignNationalFeesStudentMasterService.count() == 5
    }

    void "test delete"() {
        Long ERPForeignNationalFeesStudentMasterId = setupData()

        expect:
        ERPForeignNationalFeesStudentMasterService.count() == 5

        when:
        ERPForeignNationalFeesStudentMasterService.delete(ERPForeignNationalFeesStudentMasterId)
        sessionFactory.currentSession.flush()

        then:
        ERPForeignNationalFeesStudentMasterService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPForeignNationalFeesStudentMaster ERPForeignNationalFeesStudentMaster = new ERPForeignNationalFeesStudentMaster()
        ERPForeignNationalFeesStudentMasterService.save(ERPForeignNationalFeesStudentMaster)

        then:
        ERPForeignNationalFeesStudentMaster.id != null
    }
}
