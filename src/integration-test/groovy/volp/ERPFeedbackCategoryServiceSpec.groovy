package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPFeedbackCategoryServiceSpec extends Specification {

    ERPFeedbackCategoryService ERPFeedbackCategoryService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPFeedbackCategory(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategory(...).save(flush: true, failOnError: true)
        //ERPFeedbackCategory ERPFeedbackCategory = new ERPFeedbackCategory(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategory(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategory(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPFeedbackCategory.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPFeedbackCategoryService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPFeedbackCategory> ERPFeedbackCategoryList = ERPFeedbackCategoryService.list(max: 2, offset: 2)

        then:
        ERPFeedbackCategoryList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPFeedbackCategoryService.count() == 5
    }

    void "test delete"() {
        Long ERPFeedbackCategoryId = setupData()

        expect:
        ERPFeedbackCategoryService.count() == 5

        when:
        ERPFeedbackCategoryService.delete(ERPFeedbackCategoryId)
        sessionFactory.currentSession.flush()

        then:
        ERPFeedbackCategoryService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPFeedbackCategory ERPFeedbackCategory = new ERPFeedbackCategory()
        ERPFeedbackCategoryService.save(ERPFeedbackCategory)

        then:
        ERPFeedbackCategory.id != null
    }
}
