package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPFeedbackCategoryTypeServiceSpec extends Specification {

    ERPFeedbackCategoryTypeService ERPFeedbackCategoryTypeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPFeedbackCategoryType(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategoryType(...).save(flush: true, failOnError: true)
        //ERPFeedbackCategoryType ERPFeedbackCategoryType = new ERPFeedbackCategoryType(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategoryType(...).save(flush: true, failOnError: true)
        //new ERPFeedbackCategoryType(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPFeedbackCategoryType.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPFeedbackCategoryTypeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPFeedbackCategoryType> ERPFeedbackCategoryTypeList = ERPFeedbackCategoryTypeService.list(max: 2, offset: 2)

        then:
        ERPFeedbackCategoryTypeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPFeedbackCategoryTypeService.count() == 5
    }

    void "test delete"() {
        Long ERPFeedbackCategoryTypeId = setupData()

        expect:
        ERPFeedbackCategoryTypeService.count() == 5

        when:
        ERPFeedbackCategoryTypeService.delete(ERPFeedbackCategoryTypeId)
        sessionFactory.currentSession.flush()

        then:
        ERPFeedbackCategoryTypeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPFeedbackCategoryType ERPFeedbackCategoryType = new ERPFeedbackCategoryType()
        ERPFeedbackCategoryTypeService.save(ERPFeedbackCategoryType)

        then:
        ERPFeedbackCategoryType.id != null
    }
}
