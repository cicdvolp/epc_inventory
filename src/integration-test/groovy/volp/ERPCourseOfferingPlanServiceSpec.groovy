package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCourseOfferingPlanServiceSpec extends Specification {

    ERPCourseOfferingPlanService ERPCourseOfferingPlanService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCourseOfferingPlan(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingPlan(...).save(flush: true, failOnError: true)
        //ERPCourseOfferingPlan ERPCourseOfferingPlan = new ERPCourseOfferingPlan(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingPlan(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingPlan(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCourseOfferingPlan.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCourseOfferingPlanService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCourseOfferingPlan> ERPCourseOfferingPlanList = ERPCourseOfferingPlanService.list(max: 2, offset: 2)

        then:
        ERPCourseOfferingPlanList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCourseOfferingPlanService.count() == 5
    }

    void "test delete"() {
        Long ERPCourseOfferingPlanId = setupData()

        expect:
        ERPCourseOfferingPlanService.count() == 5

        when:
        ERPCourseOfferingPlanService.delete(ERPCourseOfferingPlanId)
        sessionFactory.currentSession.flush()

        then:
        ERPCourseOfferingPlanService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCourseOfferingPlan ERPCourseOfferingPlan = new ERPCourseOfferingPlan()
        ERPCourseOfferingPlanService.save(ERPCourseOfferingPlan)

        then:
        ERPCourseOfferingPlan.id != null
    }
}
