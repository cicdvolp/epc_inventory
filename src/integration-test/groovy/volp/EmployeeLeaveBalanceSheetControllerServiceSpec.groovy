package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class EmployeeLeaveBalanceSheetControllerServiceSpec extends Specification {

    EmployeeLeaveBalanceSheetControllerService employeeLeaveBalanceSheetControllerService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new EmployeeLeaveBalanceSheetController(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveBalanceSheetController(...).save(flush: true, failOnError: true)
        //EmployeeLeaveBalanceSheetController employeeLeaveBalanceSheetController = new EmployeeLeaveBalanceSheetController(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveBalanceSheetController(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveBalanceSheetController(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //employeeLeaveBalanceSheetController.id
    }

    void "test get"() {
        setupData()

        expect:
        employeeLeaveBalanceSheetControllerService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<EmployeeLeaveBalanceSheetController> employeeLeaveBalanceSheetControllerList = employeeLeaveBalanceSheetControllerService.list(max: 2, offset: 2)

        then:
        employeeLeaveBalanceSheetControllerList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        employeeLeaveBalanceSheetControllerService.count() == 5
    }

    void "test delete"() {
        Long employeeLeaveBalanceSheetControllerId = setupData()

        expect:
        employeeLeaveBalanceSheetControllerService.count() == 5

        when:
        employeeLeaveBalanceSheetControllerService.delete(employeeLeaveBalanceSheetControllerId)
        sessionFactory.currentSession.flush()

        then:
        employeeLeaveBalanceSheetControllerService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        EmployeeLeaveBalanceSheetController employeeLeaveBalanceSheetController = new EmployeeLeaveBalanceSheetController()
        employeeLeaveBalanceSheetControllerService.save(employeeLeaveBalanceSheetController)

        then:
        employeeLeaveBalanceSheetController.id != null
    }
}
