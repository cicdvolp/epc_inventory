package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCoursePlanServiceSpec extends Specification {

    ERPCoursePlanService ERPCoursePlanService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCoursePlan(...).save(flush: true, failOnError: true)
        //new ERPCoursePlan(...).save(flush: true, failOnError: true)
        //ERPCoursePlan ERPCoursePlan = new ERPCoursePlan(...).save(flush: true, failOnError: true)
        //new ERPCoursePlan(...).save(flush: true, failOnError: true)
        //new ERPCoursePlan(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCoursePlan.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCoursePlanService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCoursePlan> ERPCoursePlanList = ERPCoursePlanService.list(max: 2, offset: 2)

        then:
        ERPCoursePlanList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCoursePlanService.count() == 5
    }

    void "test delete"() {
        Long ERPCoursePlanId = setupData()

        expect:
        ERPCoursePlanService.count() == 5

        when:
        ERPCoursePlanService.delete(ERPCoursePlanId)
        sessionFactory.currentSession.flush()

        then:
        ERPCoursePlanService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCoursePlan ERPCoursePlan = new ERPCoursePlan()
        ERPCoursePlanService.save(ERPCoursePlan)

        then:
        ERPCoursePlan.id != null
    }
}
