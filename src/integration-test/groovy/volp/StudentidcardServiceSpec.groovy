package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class StudentidcardServiceSpec extends Specification {

    StudentidcardService studentidcardService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new Studentidcard(...).save(flush: true, failOnError: true)
        //new Studentidcard(...).save(flush: true, failOnError: true)
        //Studentidcard studentidcard = new Studentidcard(...).save(flush: true, failOnError: true)
        //new Studentidcard(...).save(flush: true, failOnError: true)
        //new Studentidcard(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //studentidcard.id
    }

    void "test get"() {
        setupData()

        expect:
        studentidcardService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<Studentidcard> studentidcardList = studentidcardService.list(max: 2, offset: 2)

        then:
        studentidcardList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        studentidcardService.count() == 5
    }

    void "test delete"() {
        Long studentidcardId = setupData()

        expect:
        studentidcardService.count() == 5

        when:
        studentidcardService.delete(studentidcardId)
        sessionFactory.currentSession.flush()

        then:
        studentidcardService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        Studentidcard studentidcard = new Studentidcard()
        studentidcardService.save(studentidcard)

        then:
        studentidcard.id != null
    }
}
