package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class LeaveTypeServiceSpec extends Specification {

    LeaveTypeService leaveTypeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new LeaveType(...).save(flush: true, failOnError: true)
        //new LeaveType(...).save(flush: true, failOnError: true)
        //LeaveType leaveType = new LeaveType(...).save(flush: true, failOnError: true)
        //new LeaveType(...).save(flush: true, failOnError: true)
        //new LeaveType(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //leaveType.id
    }

    void "test get"() {
        setupData()

        expect:
        leaveTypeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<LeaveType> leaveTypeList = leaveTypeService.list(max: 2, offset: 2)

        then:
        leaveTypeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        leaveTypeService.count() == 5
    }

    void "test delete"() {
        Long leaveTypeId = setupData()

        expect:
        leaveTypeService.count() == 5

        when:
        leaveTypeService.delete(leaveTypeId)
        sessionFactory.currentSession.flush()

        then:
        leaveTypeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        LeaveType leaveType = new LeaveType()
        leaveTypeService.save(leaveType)

        then:
        leaveType.id != null
    }
}
