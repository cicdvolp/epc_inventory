package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPExamCAPTransactionServiceSpec extends Specification {

    ERPExamCAPTransactionService ERPExamCAPTransactionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPExamCAPTransaction(...).save(flush: true, failOnError: true)
        //new ERPExamCAPTransaction(...).save(flush: true, failOnError: true)
        //ERPExamCAPTransaction ERPExamCAPTransaction = new ERPExamCAPTransaction(...).save(flush: true, failOnError: true)
        //new ERPExamCAPTransaction(...).save(flush: true, failOnError: true)
        //new ERPExamCAPTransaction(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPExamCAPTransaction.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPExamCAPTransactionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPExamCAPTransaction> ERPExamCAPTransactionList = ERPExamCAPTransactionService.list(max: 2, offset: 2)

        then:
        ERPExamCAPTransactionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPExamCAPTransactionService.count() == 5
    }

    void "test delete"() {
        Long ERPExamCAPTransactionId = setupData()

        expect:
        ERPExamCAPTransactionService.count() == 5

        when:
        ERPExamCAPTransactionService.delete(ERPExamCAPTransactionId)
        sessionFactory.currentSession.flush()

        then:
        ERPExamCAPTransactionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPExamCAPTransaction ERPExamCAPTransaction = new ERPExamCAPTransaction()
        ERPExamCAPTransactionService.save(ERPExamCAPTransaction)

        then:
        ERPExamCAPTransaction.id != null
    }
}
