package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPOfficeOrderDetailsServiceSpec extends Specification {

    ERPOfficeOrderDetailsService ERPOfficeOrderDetailsService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPOfficeOrderDetails(...).save(flush: true, failOnError: true)
        //new ERPOfficeOrderDetails(...).save(flush: true, failOnError: true)
        //ERPOfficeOrderDetails ERPOfficeOrderDetails = new ERPOfficeOrderDetails(...).save(flush: true, failOnError: true)
        //new ERPOfficeOrderDetails(...).save(flush: true, failOnError: true)
        //new ERPOfficeOrderDetails(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPOfficeOrderDetails.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPOfficeOrderDetailsService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPOfficeOrderDetails> ERPOfficeOrderDetailsList = ERPOfficeOrderDetailsService.list(max: 2, offset: 2)

        then:
        ERPOfficeOrderDetailsList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPOfficeOrderDetailsService.count() == 5
    }

    void "test delete"() {
        Long ERPOfficeOrderDetailsId = setupData()

        expect:
        ERPOfficeOrderDetailsService.count() == 5

        when:
        ERPOfficeOrderDetailsService.delete(ERPOfficeOrderDetailsId)
        sessionFactory.currentSession.flush()

        then:
        ERPOfficeOrderDetailsService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPOfficeOrderDetails ERPOfficeOrderDetails = new ERPOfficeOrderDetails()
        ERPOfficeOrderDetailsService.save(ERPOfficeOrderDetails)

        then:
        ERPOfficeOrderDetails.id != null
    }
}
