package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPFacultyPostServiceSpec extends Specification {

    ERPFacultyPostService ERPFacultyPostService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPFacultyPost(...).save(flush: true, failOnError: true)
        //new ERPFacultyPost(...).save(flush: true, failOnError: true)
        //ERPFacultyPost ERPFacultyPost = new ERPFacultyPost(...).save(flush: true, failOnError: true)
        //new ERPFacultyPost(...).save(flush: true, failOnError: true)
        //new ERPFacultyPost(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPFacultyPost.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPFacultyPostService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPFacultyPost> ERPFacultyPostList = ERPFacultyPostService.list(max: 2, offset: 2)

        then:
        ERPFacultyPostList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPFacultyPostService.count() == 5
    }

    void "test delete"() {
        Long ERPFacultyPostId = setupData()

        expect:
        ERPFacultyPostService.count() == 5

        when:
        ERPFacultyPostService.delete(ERPFacultyPostId)
        sessionFactory.currentSession.flush()

        then:
        ERPFacultyPostService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPFacultyPost ERPFacultyPost = new ERPFacultyPost()
        ERPFacultyPostService.save(ERPFacultyPost)

        then:
        ERPFacultyPost.id != null
    }
}
