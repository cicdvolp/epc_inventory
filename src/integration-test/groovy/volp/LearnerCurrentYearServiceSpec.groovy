package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class LearnerCurrentYearServiceSpec extends Specification {

    LearnerCurrentYearService learnerCurrentYearService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new LearnerCurrentYear(...).save(flush: true, failOnError: true)
        //new LearnerCurrentYear(...).save(flush: true, failOnError: true)
        //LearnerCurrentYear learnerCurrentYear = new LearnerCurrentYear(...).save(flush: true, failOnError: true)
        //new LearnerCurrentYear(...).save(flush: true, failOnError: true)
        //new LearnerCurrentYear(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //learnerCurrentYear.id
    }

    void "test get"() {
        setupData()

        expect:
        learnerCurrentYearService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<LearnerCurrentYear> learnerCurrentYearList = learnerCurrentYearService.list(max: 2, offset: 2)

        then:
        learnerCurrentYearList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        learnerCurrentYearService.count() == 5
    }

    void "test delete"() {
        Long learnerCurrentYearId = setupData()

        expect:
        learnerCurrentYearService.count() == 5

        when:
        learnerCurrentYearService.delete(learnerCurrentYearId)
        sessionFactory.currentSession.flush()

        then:
        learnerCurrentYearService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        LearnerCurrentYear learnerCurrentYear = new LearnerCurrentYear()
        learnerCurrentYearService.save(learnerCurrentYear)

        then:
        learnerCurrentYear.id != null
    }
}
