package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPSelfAppraisalAuthorityTypeServiceSpec extends Specification {

    ERPSelfAppraisalAuthorityTypeService ERPSelfAppraisalAuthorityTypeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPSelfAppraisalAuthorityType(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalAuthorityType(...).save(flush: true, failOnError: true)
        //ERPSelfAppraisalAuthorityType ERPSelfAppraisalAuthorityType = new ERPSelfAppraisalAuthorityType(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalAuthorityType(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalAuthorityType(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPSelfAppraisalAuthorityType.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPSelfAppraisalAuthorityTypeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPSelfAppraisalAuthorityType> ERPSelfAppraisalAuthorityTypeList = ERPSelfAppraisalAuthorityTypeService.list(max: 2, offset: 2)

        then:
        ERPSelfAppraisalAuthorityTypeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPSelfAppraisalAuthorityTypeService.count() == 5
    }

    void "test delete"() {
        Long ERPSelfAppraisalAuthorityTypeId = setupData()

        expect:
        ERPSelfAppraisalAuthorityTypeService.count() == 5

        when:
        ERPSelfAppraisalAuthorityTypeService.delete(ERPSelfAppraisalAuthorityTypeId)
        sessionFactory.currentSession.flush()

        then:
        ERPSelfAppraisalAuthorityTypeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPSelfAppraisalAuthorityType ERPSelfAppraisalAuthorityType = new ERPSelfAppraisalAuthorityType()
        ERPSelfAppraisalAuthorityTypeService.save(ERPSelfAppraisalAuthorityType)

        then:
        ERPSelfAppraisalAuthorityType.id != null
    }
}
