package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class EmployeeLeaveBalanceSheetServiceSpec extends Specification {

    EmployeeLeaveBalanceSheetService employeeLeaveBalanceSheetService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new EmployeeLeaveBalanceSheet(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveBalanceSheet(...).save(flush: true, failOnError: true)
        //EmployeeLeaveBalanceSheet employeeLeaveBalanceSheet = new EmployeeLeaveBalanceSheet(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveBalanceSheet(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveBalanceSheet(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //employeeLeaveBalanceSheet.id
    }

    void "test get"() {
        setupData()

        expect:
        employeeLeaveBalanceSheetService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<EmployeeLeaveBalanceSheet> employeeLeaveBalanceSheetList = employeeLeaveBalanceSheetService.list(max: 2, offset: 2)

        then:
        employeeLeaveBalanceSheetList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        employeeLeaveBalanceSheetService.count() == 5
    }

    void "test delete"() {
        Long employeeLeaveBalanceSheetId = setupData()

        expect:
        employeeLeaveBalanceSheetService.count() == 5

        when:
        employeeLeaveBalanceSheetService.delete(employeeLeaveBalanceSheetId)
        sessionFactory.currentSession.flush()

        then:
        employeeLeaveBalanceSheetService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        EmployeeLeaveBalanceSheet employeeLeaveBalanceSheet = new EmployeeLeaveBalanceSheet()
        employeeLeaveBalanceSheetService.save(employeeLeaveBalanceSheet)

        then:
        employeeLeaveBalanceSheet.id != null
    }
}
