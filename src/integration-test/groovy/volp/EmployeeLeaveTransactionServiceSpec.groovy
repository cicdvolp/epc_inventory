package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class EmployeeLeaveTransactionServiceSpec extends Specification {

    EmployeeLeaveTransactionService employeeLeaveTransactionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new EmployeeLeaveTransaction(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveTransaction(...).save(flush: true, failOnError: true)
        //EmployeeLeaveTransaction employeeLeaveTransaction = new EmployeeLeaveTransaction(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveTransaction(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveTransaction(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //employeeLeaveTransaction.id
    }

    void "test get"() {
        setupData()

        expect:
        employeeLeaveTransactionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<EmployeeLeaveTransaction> employeeLeaveTransactionList = employeeLeaveTransactionService.list(max: 2, offset: 2)

        then:
        employeeLeaveTransactionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        employeeLeaveTransactionService.count() == 5
    }

    void "test delete"() {
        Long employeeLeaveTransactionId = setupData()

        expect:
        employeeLeaveTransactionService.count() == 5

        when:
        employeeLeaveTransactionService.delete(employeeLeaveTransactionId)
        sessionFactory.currentSession.flush()

        then:
        employeeLeaveTransactionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        EmployeeLeaveTransaction employeeLeaveTransaction = new EmployeeLeaveTransaction()
        employeeLeaveTransactionService.save(employeeLeaveTransaction)

        then:
        employeeLeaveTransaction.id != null
    }
}
