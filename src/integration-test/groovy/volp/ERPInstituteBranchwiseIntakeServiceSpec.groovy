package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPInstituteBranchwiseIntakeServiceSpec extends Specification {

    ERPInstituteBranchwiseIntakeService ERPInstituteBranchwiseIntakeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPInstituteBranchwiseIntake(...).save(flush: true, failOnError: true)
        //new ERPInstituteBranchwiseIntake(...).save(flush: true, failOnError: true)
        //ERPInstituteBranchwiseIntake ERPInstituteBranchwiseIntake = new ERPInstituteBranchwiseIntake(...).save(flush: true, failOnError: true)
        //new ERPInstituteBranchwiseIntake(...).save(flush: true, failOnError: true)
        //new ERPInstituteBranchwiseIntake(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPInstituteBranchwiseIntake.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPInstituteBranchwiseIntakeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPInstituteBranchwiseIntake> ERPInstituteBranchwiseIntakeList = ERPInstituteBranchwiseIntakeService.list(max: 2, offset: 2)

        then:
        ERPInstituteBranchwiseIntakeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPInstituteBranchwiseIntakeService.count() == 5
    }

    void "test delete"() {
        Long ERPInstituteBranchwiseIntakeId = setupData()

        expect:
        ERPInstituteBranchwiseIntakeService.count() == 5

        when:
        ERPInstituteBranchwiseIntakeService.delete(ERPInstituteBranchwiseIntakeId)
        sessionFactory.currentSession.flush()

        then:
        ERPInstituteBranchwiseIntakeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPInstituteBranchwiseIntake ERPInstituteBranchwiseIntake = new ERPInstituteBranchwiseIntake()
        ERPInstituteBranchwiseIntakeService.save(ERPInstituteBranchwiseIntake)

        then:
        ERPInstituteBranchwiseIntake.id != null
    }
}
