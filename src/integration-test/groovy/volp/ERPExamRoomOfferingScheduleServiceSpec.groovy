package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPExamRoomOfferingScheduleServiceSpec extends Specification {

    ERPExamRoomOfferingScheduleService ERPExamRoomOfferingScheduleService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPExamRoomOfferingSchedule(...).save(flush: true, failOnError: true)
        //new ERPExamRoomOfferingSchedule(...).save(flush: true, failOnError: true)
        //ERPExamRoomOfferingSchedule ERPExamRoomOfferingSchedule = new ERPExamRoomOfferingSchedule(...).save(flush: true, failOnError: true)
        //new ERPExamRoomOfferingSchedule(...).save(flush: true, failOnError: true)
        //new ERPExamRoomOfferingSchedule(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPExamRoomOfferingSchedule.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPExamRoomOfferingScheduleService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPExamRoomOfferingSchedule> ERPExamRoomOfferingScheduleList = ERPExamRoomOfferingScheduleService.list(max: 2, offset: 2)

        then:
        ERPExamRoomOfferingScheduleList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPExamRoomOfferingScheduleService.count() == 5
    }

    void "test delete"() {
        Long ERPExamRoomOfferingScheduleId = setupData()

        expect:
        ERPExamRoomOfferingScheduleService.count() == 5

        when:
        ERPExamRoomOfferingScheduleService.delete(ERPExamRoomOfferingScheduleId)
        sessionFactory.currentSession.flush()

        then:
        ERPExamRoomOfferingScheduleService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPExamRoomOfferingSchedule ERPExamRoomOfferingSchedule = new ERPExamRoomOfferingSchedule()
        ERPExamRoomOfferingScheduleService.save(ERPExamRoomOfferingSchedule)

        then:
        ERPExamRoomOfferingSchedule.id != null
    }
}
