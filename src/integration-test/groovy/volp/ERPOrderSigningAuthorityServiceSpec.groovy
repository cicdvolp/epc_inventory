package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPOrderSigningAuthorityServiceSpec extends Specification {

    ERPOrderSigningAuthorityService ERPOrderSigningAuthorityService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPOrderSigningAuthority(...).save(flush: true, failOnError: true)
        //new ERPOrderSigningAuthority(...).save(flush: true, failOnError: true)
        //ERPOrderSigningAuthority ERPOrderSigningAuthority = new ERPOrderSigningAuthority(...).save(flush: true, failOnError: true)
        //new ERPOrderSigningAuthority(...).save(flush: true, failOnError: true)
        //new ERPOrderSigningAuthority(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPOrderSigningAuthority.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPOrderSigningAuthorityService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPOrderSigningAuthority> ERPOrderSigningAuthorityList = ERPOrderSigningAuthorityService.list(max: 2, offset: 2)

        then:
        ERPOrderSigningAuthorityList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPOrderSigningAuthorityService.count() == 5
    }

    void "test delete"() {
        Long ERPOrderSigningAuthorityId = setupData()

        expect:
        ERPOrderSigningAuthorityService.count() == 5

        when:
        ERPOrderSigningAuthorityService.delete(ERPOrderSigningAuthorityId)
        sessionFactory.currentSession.flush()

        then:
        ERPOrderSigningAuthorityService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPOrderSigningAuthority ERPOrderSigningAuthority = new ERPOrderSigningAuthority()
        ERPOrderSigningAuthorityService.save(ERPOrderSigningAuthority)

        then:
        ERPOrderSigningAuthority.id != null
    }
}
