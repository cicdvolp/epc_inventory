package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPLearnerCourseHistoryServiceSpec extends Specification {

    ERPLearnerCourseHistoryService ERPLearnerCourseHistoryService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPLearnerCourseHistory(...).save(flush: true, failOnError: true)
        //new ERPLearnerCourseHistory(...).save(flush: true, failOnError: true)
        //ERPLearnerCourseHistory ERPLearnerCourseHistory = new ERPLearnerCourseHistory(...).save(flush: true, failOnError: true)
        //new ERPLearnerCourseHistory(...).save(flush: true, failOnError: true)
        //new ERPLearnerCourseHistory(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPLearnerCourseHistory.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPLearnerCourseHistoryService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPLearnerCourseHistory> ERPLearnerCourseHistoryList = ERPLearnerCourseHistoryService.list(max: 2, offset: 2)

        then:
        ERPLearnerCourseHistoryList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPLearnerCourseHistoryService.count() == 5
    }

    void "test delete"() {
        Long ERPLearnerCourseHistoryId = setupData()

        expect:
        ERPLearnerCourseHistoryService.count() == 5

        when:
        ERPLearnerCourseHistoryService.delete(ERPLearnerCourseHistoryId)
        sessionFactory.currentSession.flush()

        then:
        ERPLearnerCourseHistoryService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPLearnerCourseHistory ERPLearnerCourseHistory = new ERPLearnerCourseHistory()
        ERPLearnerCourseHistoryService.save(ERPLearnerCourseHistory)

        then:
        ERPLearnerCourseHistory.id != null
    }
}
