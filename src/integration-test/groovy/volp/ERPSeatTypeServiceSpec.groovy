package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPSeatTypeServiceSpec extends Specification {

    ERPSeatTypeService ERPSeatTypeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPSeatType(...).save(flush: true, failOnError: true)
        //new ERPSeatType(...).save(flush: true, failOnError: true)
        //ERPSeatType ERPSeatType = new ERPSeatType(...).save(flush: true, failOnError: true)
        //new ERPSeatType(...).save(flush: true, failOnError: true)
        //new ERPSeatType(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPSeatType.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPSeatTypeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPSeatType> ERPSeatTypeList = ERPSeatTypeService.list(max: 2, offset: 2)

        then:
        ERPSeatTypeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPSeatTypeService.count() == 5
    }

    void "test delete"() {
        Long ERPSeatTypeId = setupData()

        expect:
        ERPSeatTypeService.count() == 5

        when:
        ERPSeatTypeService.delete(ERPSeatTypeId)
        sessionFactory.currentSession.flush()

        then:
        ERPSeatTypeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPSeatType ERPSeatType = new ERPSeatType()
        ERPSeatTypeService.save(ERPSeatType)

        then:
        ERPSeatType.id != null
    }
}
