package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPShiftServiceSpec extends Specification {

    ERPShiftService ERPShiftService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPShift(...).save(flush: true, failOnError: true)
        //new ERPShift(...).save(flush: true, failOnError: true)
        //ERPShift ERPShift = new ERPShift(...).save(flush: true, failOnError: true)
        //new ERPShift(...).save(flush: true, failOnError: true)
        //new ERPShift(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPShift.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPShiftService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPShift> ERPShiftList = ERPShiftService.list(max: 2, offset: 2)

        then:
        ERPShiftList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPShiftService.count() == 5
    }

    void "test delete"() {
        Long ERPShiftId = setupData()

        expect:
        ERPShiftService.count() == 5

        when:
        ERPShiftService.delete(ERPShiftId)
        sessionFactory.currentSession.flush()

        then:
        ERPShiftService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPShift ERPShift = new ERPShift()
        ERPShiftService.save(ERPShift)

        then:
        ERPShift.id != null
    }
}
