package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ApplicationAcademicYearServiceSpec extends Specification {

    ApplicationAcademicYearService applicationAcademicYearService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ApplicationAcademicYear(...).save(flush: true, failOnError: true)
        //new ApplicationAcademicYear(...).save(flush: true, failOnError: true)
        //ApplicationAcademicYear applicationAcademicYear = new ApplicationAcademicYear(...).save(flush: true, failOnError: true)
        //new ApplicationAcademicYear(...).save(flush: true, failOnError: true)
        //new ApplicationAcademicYear(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //applicationAcademicYear.id
    }

    void "test get"() {
        setupData()

        expect:
        applicationAcademicYearService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ApplicationAcademicYear> applicationAcademicYearList = applicationAcademicYearService.list(max: 2, offset: 2)

        then:
        applicationAcademicYearList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        applicationAcademicYearService.count() == 5
    }

    void "test delete"() {
        Long applicationAcademicYearId = setupData()

        expect:
        applicationAcademicYearService.count() == 5

        when:
        applicationAcademicYearService.delete(applicationAcademicYearId)
        sessionFactory.currentSession.flush()

        then:
        applicationAcademicYearService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ApplicationAcademicYear applicationAcademicYear = new ApplicationAcademicYear()
        applicationAcademicYearService.save(applicationAcademicYear)

        then:
        applicationAcademicYear.id != null
    }
}
