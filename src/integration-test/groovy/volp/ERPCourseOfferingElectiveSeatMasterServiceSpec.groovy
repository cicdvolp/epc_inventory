package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCourseOfferingElectiveSeatMasterServiceSpec extends Specification {

    ERPCourseOfferingElectiveSeatMasterService ERPCourseOfferingElectiveSeatMasterService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCourseOfferingElectiveSeatMaster(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingElectiveSeatMaster(...).save(flush: true, failOnError: true)
        //ERPCourseOfferingElectiveSeatMaster ERPCourseOfferingElectiveSeatMaster = new ERPCourseOfferingElectiveSeatMaster(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingElectiveSeatMaster(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingElectiveSeatMaster(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCourseOfferingElectiveSeatMaster.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCourseOfferingElectiveSeatMasterService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCourseOfferingElectiveSeatMaster> ERPCourseOfferingElectiveSeatMasterList = ERPCourseOfferingElectiveSeatMasterService.list(max: 2, offset: 2)

        then:
        ERPCourseOfferingElectiveSeatMasterList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCourseOfferingElectiveSeatMasterService.count() == 5
    }

    void "test delete"() {
        Long ERPCourseOfferingElectiveSeatMasterId = setupData()

        expect:
        ERPCourseOfferingElectiveSeatMasterService.count() == 5

        when:
        ERPCourseOfferingElectiveSeatMasterService.delete(ERPCourseOfferingElectiveSeatMasterId)
        sessionFactory.currentSession.flush()

        then:
        ERPCourseOfferingElectiveSeatMasterService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCourseOfferingElectiveSeatMaster ERPCourseOfferingElectiveSeatMaster = new ERPCourseOfferingElectiveSeatMaster()
        ERPCourseOfferingElectiveSeatMasterService.save(ERPCourseOfferingElectiveSeatMaster)

        then:
        ERPCourseOfferingElectiveSeatMaster.id != null
    }
}
