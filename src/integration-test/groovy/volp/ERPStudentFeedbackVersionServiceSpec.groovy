package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPStudentFeedbackVersionServiceSpec extends Specification {

    ERPStudentFeedbackVersionService ERPStudentFeedbackVersionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPStudentFeedbackVersion(...).save(flush: true, failOnError: true)
        //new ERPStudentFeedbackVersion(...).save(flush: true, failOnError: true)
        //ERPStudentFeedbackVersion ERPStudentFeedbackVersion = new ERPStudentFeedbackVersion(...).save(flush: true, failOnError: true)
        //new ERPStudentFeedbackVersion(...).save(flush: true, failOnError: true)
        //new ERPStudentFeedbackVersion(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPStudentFeedbackVersion.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPStudentFeedbackVersionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPStudentFeedbackVersion> ERPStudentFeedbackVersionList = ERPStudentFeedbackVersionService.list(max: 2, offset: 2)

        then:
        ERPStudentFeedbackVersionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPStudentFeedbackVersionService.count() == 5
    }

    void "test delete"() {
        Long ERPStudentFeedbackVersionId = setupData()

        expect:
        ERPStudentFeedbackVersionService.count() == 5

        when:
        ERPStudentFeedbackVersionService.delete(ERPStudentFeedbackVersionId)
        sessionFactory.currentSession.flush()

        then:
        ERPStudentFeedbackVersionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPStudentFeedbackVersion ERPStudentFeedbackVersion = new ERPStudentFeedbackVersion()
        ERPStudentFeedbackVersionService.save(ERPStudentFeedbackVersion)

        then:
        ERPStudentFeedbackVersion.id != null
    }
}
