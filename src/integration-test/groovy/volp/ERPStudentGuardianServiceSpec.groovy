package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPStudentGuardianServiceSpec extends Specification {

    ERPStudentGuardianService ERPStudentGuardianService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPStudentGuardian(...).save(flush: true, failOnError: true)
        //new ERPStudentGuardian(...).save(flush: true, failOnError: true)
        //ERPStudentGuardian ERPStudentGuardian = new ERPStudentGuardian(...).save(flush: true, failOnError: true)
        //new ERPStudentGuardian(...).save(flush: true, failOnError: true)
        //new ERPStudentGuardian(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPStudentGuardian.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPStudentGuardianService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPStudentGuardian> ERPStudentGuardianList = ERPStudentGuardianService.list(max: 2, offset: 2)

        then:
        ERPStudentGuardianList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPStudentGuardianService.count() == 5
    }

    void "test delete"() {
        Long ERPStudentGuardianId = setupData()

        expect:
        ERPStudentGuardianService.count() == 5

        when:
        ERPStudentGuardianService.delete(ERPStudentGuardianId)
        sessionFactory.currentSession.flush()

        then:
        ERPStudentGuardianService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPStudentGuardian ERPStudentGuardian = new ERPStudentGuardian()
        ERPStudentGuardianService.save(ERPStudentGuardian)

        then:
        ERPStudentGuardian.id != null
    }
}
