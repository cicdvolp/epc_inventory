package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPStudentAdmissionMainCategoryServiceSpec extends Specification {

    ERPStudentAdmissionMainCategoryService ERPStudentAdmissionMainCategoryService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPStudentAdmissionMainCategory(...).save(flush: true, failOnError: true)
        //new ERPStudentAdmissionMainCategory(...).save(flush: true, failOnError: true)
        //ERPStudentAdmissionMainCategory ERPStudentAdmissionMainCategory = new ERPStudentAdmissionMainCategory(...).save(flush: true, failOnError: true)
        //new ERPStudentAdmissionMainCategory(...).save(flush: true, failOnError: true)
        //new ERPStudentAdmissionMainCategory(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPStudentAdmissionMainCategory.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPStudentAdmissionMainCategoryService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPStudentAdmissionMainCategory> ERPStudentAdmissionMainCategoryList = ERPStudentAdmissionMainCategoryService.list(max: 2, offset: 2)

        then:
        ERPStudentAdmissionMainCategoryList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPStudentAdmissionMainCategoryService.count() == 5
    }

    void "test delete"() {
        Long ERPStudentAdmissionMainCategoryId = setupData()

        expect:
        ERPStudentAdmissionMainCategoryService.count() == 5

        when:
        ERPStudentAdmissionMainCategoryService.delete(ERPStudentAdmissionMainCategoryId)
        sessionFactory.currentSession.flush()

        then:
        ERPStudentAdmissionMainCategoryService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPStudentAdmissionMainCategory ERPStudentAdmissionMainCategory = new ERPStudentAdmissionMainCategory()
        ERPStudentAdmissionMainCategoryService.save(ERPStudentAdmissionMainCategory)

        then:
        ERPStudentAdmissionMainCategory.id != null
    }
}
