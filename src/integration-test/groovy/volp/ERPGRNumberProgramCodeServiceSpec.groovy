package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPGRNumberProgramCodeServiceSpec extends Specification {

    ERPGRNumberProgramCodeService ERPGRNumberProgramCodeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPGRNumberProgramCode(...).save(flush: true, failOnError: true)
        //new ERPGRNumberProgramCode(...).save(flush: true, failOnError: true)
        //ERPGRNumberProgramCode ERPGRNumberProgramCode = new ERPGRNumberProgramCode(...).save(flush: true, failOnError: true)
        //new ERPGRNumberProgramCode(...).save(flush: true, failOnError: true)
        //new ERPGRNumberProgramCode(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPGRNumberProgramCode.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPGRNumberProgramCodeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPGRNumberProgramCode> ERPGRNumberProgramCodeList = ERPGRNumberProgramCodeService.list(max: 2, offset: 2)

        then:
        ERPGRNumberProgramCodeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPGRNumberProgramCodeService.count() == 5
    }

    void "test delete"() {
        Long ERPGRNumberProgramCodeId = setupData()

        expect:
        ERPGRNumberProgramCodeService.count() == 5

        when:
        ERPGRNumberProgramCodeService.delete(ERPGRNumberProgramCodeId)
        sessionFactory.currentSession.flush()

        then:
        ERPGRNumberProgramCodeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPGRNumberProgramCode ERPGRNumberProgramCode = new ERPGRNumberProgramCode()
        ERPGRNumberProgramCodeService.save(ERPGRNumberProgramCode)

        then:
        ERPGRNumberProgramCode.id != null
    }
}
