package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPSelfAppraisalParameterServiceSpec extends Specification {

    ERPSelfAppraisalParameterService ERPSelfAppraisalParameterService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPSelfAppraisalParameter(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalParameter(...).save(flush: true, failOnError: true)
        //ERPSelfAppraisalParameter ERPSelfAppraisalParameter = new ERPSelfAppraisalParameter(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalParameter(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalParameter(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPSelfAppraisalParameter.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPSelfAppraisalParameterService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPSelfAppraisalParameter> ERPSelfAppraisalParameterList = ERPSelfAppraisalParameterService.list(max: 2, offset: 2)

        then:
        ERPSelfAppraisalParameterList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPSelfAppraisalParameterService.count() == 5
    }

    void "test delete"() {
        Long ERPSelfAppraisalParameterId = setupData()

        expect:
        ERPSelfAppraisalParameterService.count() == 5

        when:
        ERPSelfAppraisalParameterService.delete(ERPSelfAppraisalParameterId)
        sessionFactory.currentSession.flush()

        then:
        ERPSelfAppraisalParameterService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPSelfAppraisalParameter ERPSelfAppraisalParameter = new ERPSelfAppraisalParameter()
        ERPSelfAppraisalParameterService.save(ERPSelfAppraisalParameter)

        then:
        ERPSelfAppraisalParameter.id != null
    }
}
