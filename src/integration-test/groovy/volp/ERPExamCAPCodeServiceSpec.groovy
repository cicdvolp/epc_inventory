package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPExamCAPCodeServiceSpec extends Specification {

    ERPExamCAPCodeService ERPExamCAPCodeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPExamCAPCode(...).save(flush: true, failOnError: true)
        //new ERPExamCAPCode(...).save(flush: true, failOnError: true)
        //ERPExamCAPCode ERPExamCAPCode = new ERPExamCAPCode(...).save(flush: true, failOnError: true)
        //new ERPExamCAPCode(...).save(flush: true, failOnError: true)
        //new ERPExamCAPCode(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPExamCAPCode.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPExamCAPCodeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPExamCAPCode> ERPExamCAPCodeList = ERPExamCAPCodeService.list(max: 2, offset: 2)

        then:
        ERPExamCAPCodeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPExamCAPCodeService.count() == 5
    }

    void "test delete"() {
        Long ERPExamCAPCodeId = setupData()

        expect:
        ERPExamCAPCodeService.count() == 5

        when:
        ERPExamCAPCodeService.delete(ERPExamCAPCodeId)
        sessionFactory.currentSession.flush()

        then:
        ERPExamCAPCodeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPExamCAPCode ERPExamCAPCode = new ERPExamCAPCode()
        ERPExamCAPCodeService.save(ERPExamCAPCode)

        then:
        ERPExamCAPCode.id != null
    }
}
