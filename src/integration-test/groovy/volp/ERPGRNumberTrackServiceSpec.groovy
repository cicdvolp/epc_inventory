package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPGRNumberTrackServiceSpec extends Specification {

    ERPGRNumberTrackService ERPGRNumberTrackService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPGRNumberTrack(...).save(flush: true, failOnError: true)
        //new ERPGRNumberTrack(...).save(flush: true, failOnError: true)
        //ERPGRNumberTrack ERPGRNumberTrack = new ERPGRNumberTrack(...).save(flush: true, failOnError: true)
        //new ERPGRNumberTrack(...).save(flush: true, failOnError: true)
        //new ERPGRNumberTrack(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPGRNumberTrack.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPGRNumberTrackService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPGRNumberTrack> ERPGRNumberTrackList = ERPGRNumberTrackService.list(max: 2, offset: 2)

        then:
        ERPGRNumberTrackList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPGRNumberTrackService.count() == 5
    }

    void "test delete"() {
        Long ERPGRNumberTrackId = setupData()

        expect:
        ERPGRNumberTrackService.count() == 5

        when:
        ERPGRNumberTrackService.delete(ERPGRNumberTrackId)
        sessionFactory.currentSession.flush()

        then:
        ERPGRNumberTrackService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPGRNumberTrack ERPGRNumberTrack = new ERPGRNumberTrack()
        ERPGRNumberTrackService.save(ERPGRNumberTrack)

        then:
        ERPGRNumberTrack.id != null
    }
}
