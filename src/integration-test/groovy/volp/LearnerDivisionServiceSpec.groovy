package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class LearnerDivisionServiceSpec extends Specification {

    LearnerDivisionService learnerDivisionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new LearnerDivision(...).save(flush: true, failOnError: true)
        //new LearnerDivision(...).save(flush: true, failOnError: true)
        //LearnerDivision learnerDivision = new LearnerDivision(...).save(flush: true, failOnError: true)
        //new LearnerDivision(...).save(flush: true, failOnError: true)
        //new LearnerDivision(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //learnerDivision.id
    }

    void "test get"() {
        setupData()

        expect:
        learnerDivisionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<LearnerDivision> learnerDivisionList = learnerDivisionService.list(max: 2, offset: 2)

        then:
        learnerDivisionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        learnerDivisionService.count() == 5
    }

    void "test delete"() {
        Long learnerDivisionId = setupData()

        expect:
        learnerDivisionService.count() == 5

        when:
        learnerDivisionService.delete(learnerDivisionId)
        sessionFactory.currentSession.flush()

        then:
        learnerDivisionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        LearnerDivision learnerDivision = new LearnerDivision()
        learnerDivisionService.save(learnerDivision)

        then:
        learnerDivision.id != null
    }
}
