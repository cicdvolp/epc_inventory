package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPGRNumberInstituteCodeServiceSpec extends Specification {

    ERPGRNumberInstituteCodeService ERPGRNumberInstituteCodeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPGRNumberInstituteCode(...).save(flush: true, failOnError: true)
        //new ERPGRNumberInstituteCode(...).save(flush: true, failOnError: true)
        //ERPGRNumberInstituteCode ERPGRNumberInstituteCode = new ERPGRNumberInstituteCode(...).save(flush: true, failOnError: true)
        //new ERPGRNumberInstituteCode(...).save(flush: true, failOnError: true)
        //new ERPGRNumberInstituteCode(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPGRNumberInstituteCode.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPGRNumberInstituteCodeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPGRNumberInstituteCode> ERPGRNumberInstituteCodeList = ERPGRNumberInstituteCodeService.list(max: 2, offset: 2)

        then:
        ERPGRNumberInstituteCodeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPGRNumberInstituteCodeService.count() == 5
    }

    void "test delete"() {
        Long ERPGRNumberInstituteCodeId = setupData()

        expect:
        ERPGRNumberInstituteCodeService.count() == 5

        when:
        ERPGRNumberInstituteCodeService.delete(ERPGRNumberInstituteCodeId)
        sessionFactory.currentSession.flush()

        then:
        ERPGRNumberInstituteCodeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPGRNumberInstituteCode ERPGRNumberInstituteCode = new ERPGRNumberInstituteCode()
        ERPGRNumberInstituteCodeService.save(ERPGRNumberInstituteCode)

        then:
        ERPGRNumberInstituteCode.id != null
    }
}
