package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCourseOfferingInstructorServiceSpec extends Specification {

    ERPCourseOfferingInstructorService ERPCourseOfferingInstructorService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCourseOfferingInstructor(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingInstructor(...).save(flush: true, failOnError: true)
        //ERPCourseOfferingInstructor ERPCourseOfferingInstructor = new ERPCourseOfferingInstructor(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingInstructor(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingInstructor(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCourseOfferingInstructor.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCourseOfferingInstructorService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCourseOfferingInstructor> ERPCourseOfferingInstructorList = ERPCourseOfferingInstructorService.list(max: 2, offset: 2)

        then:
        ERPCourseOfferingInstructorList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCourseOfferingInstructorService.count() == 5
    }

    void "test delete"() {
        Long ERPCourseOfferingInstructorId = setupData()

        expect:
        ERPCourseOfferingInstructorService.count() == 5

        when:
        ERPCourseOfferingInstructorService.delete(ERPCourseOfferingInstructorId)
        sessionFactory.currentSession.flush()

        then:
        ERPCourseOfferingInstructorService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCourseOfferingInstructor ERPCourseOfferingInstructor = new ERPCourseOfferingInstructor()
        ERPCourseOfferingInstructorService.save(ERPCourseOfferingInstructor)

        then:
        ERPCourseOfferingInstructor.id != null
    }
}
