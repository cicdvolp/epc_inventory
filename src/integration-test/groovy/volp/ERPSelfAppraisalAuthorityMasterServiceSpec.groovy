package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPSelfAppraisalAuthorityMasterServiceSpec extends Specification {

    ERPSelfAppraisalAuthorityMasterService ERPSelfAppraisalAuthorityMasterService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPSelfAppraisalAuthorityMaster(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalAuthorityMaster(...).save(flush: true, failOnError: true)
        //ERPSelfAppraisalAuthorityMaster ERPSelfAppraisalAuthorityMaster = new ERPSelfAppraisalAuthorityMaster(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalAuthorityMaster(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalAuthorityMaster(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPSelfAppraisalAuthorityMaster.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPSelfAppraisalAuthorityMasterService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPSelfAppraisalAuthorityMaster> ERPSelfAppraisalAuthorityMasterList = ERPSelfAppraisalAuthorityMasterService.list(max: 2, offset: 2)

        then:
        ERPSelfAppraisalAuthorityMasterList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPSelfAppraisalAuthorityMasterService.count() == 5
    }

    void "test delete"() {
        Long ERPSelfAppraisalAuthorityMasterId = setupData()

        expect:
        ERPSelfAppraisalAuthorityMasterService.count() == 5

        when:
        ERPSelfAppraisalAuthorityMasterService.delete(ERPSelfAppraisalAuthorityMasterId)
        sessionFactory.currentSession.flush()

        then:
        ERPSelfAppraisalAuthorityMasterService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPSelfAppraisalAuthorityMaster ERPSelfAppraisalAuthorityMaster = new ERPSelfAppraisalAuthorityMaster()
        ERPSelfAppraisalAuthorityMasterService.save(ERPSelfAppraisalAuthorityMaster)

        then:
        ERPSelfAppraisalAuthorityMaster.id != null
    }
}
