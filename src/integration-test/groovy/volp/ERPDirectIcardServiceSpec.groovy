package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPDirectIcardServiceSpec extends Specification {

    ERPDirectIcardService ERPDirectIcardService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPDirectIcard(...).save(flush: true, failOnError: true)
        //new ERPDirectIcard(...).save(flush: true, failOnError: true)
        //ERPDirectIcard ERPDirectIcard = new ERPDirectIcard(...).save(flush: true, failOnError: true)
        //new ERPDirectIcard(...).save(flush: true, failOnError: true)
        //new ERPDirectIcard(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPDirectIcard.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPDirectIcardService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPDirectIcard> ERPDirectIcardList = ERPDirectIcardService.list(max: 2, offset: 2)

        then:
        ERPDirectIcardList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPDirectIcardService.count() == 5
    }

    void "test delete"() {
        Long ERPDirectIcardId = setupData()

        expect:
        ERPDirectIcardService.count() == 5

        when:
        ERPDirectIcardService.delete(ERPDirectIcardId)
        sessionFactory.currentSession.flush()

        then:
        ERPDirectIcardService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPDirectIcard ERPDirectIcard = new ERPDirectIcard()
        ERPDirectIcardService.save(ERPDirectIcard)

        then:
        ERPDirectIcard.id != null
    }
}
