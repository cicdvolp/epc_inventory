package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCourseLearnerDetentionServiceSpec extends Specification {

    ERPCourseLearnerDetentionService ERPCourseLearnerDetentionService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCourseLearnerDetention(...).save(flush: true, failOnError: true)
        //new ERPCourseLearnerDetention(...).save(flush: true, failOnError: true)
        //ERPCourseLearnerDetention ERPCourseLearnerDetention = new ERPCourseLearnerDetention(...).save(flush: true, failOnError: true)
        //new ERPCourseLearnerDetention(...).save(flush: true, failOnError: true)
        //new ERPCourseLearnerDetention(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCourseLearnerDetention.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCourseLearnerDetentionService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCourseLearnerDetention> ERPCourseLearnerDetentionList = ERPCourseLearnerDetentionService.list(max: 2, offset: 2)

        then:
        ERPCourseLearnerDetentionList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCourseLearnerDetentionService.count() == 5
    }

    void "test delete"() {
        Long ERPCourseLearnerDetentionId = setupData()

        expect:
        ERPCourseLearnerDetentionService.count() == 5

        when:
        ERPCourseLearnerDetentionService.delete(ERPCourseLearnerDetentionId)
        sessionFactory.currentSession.flush()

        then:
        ERPCourseLearnerDetentionService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCourseLearnerDetention ERPCourseLearnerDetention = new ERPCourseLearnerDetention()
        ERPCourseLearnerDetentionService.save(ERPCourseLearnerDetention)

        then:
        ERPCourseLearnerDetention.id != null
    }
}
