package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPSelfAppraisalFacultyTypeServiceSpec extends Specification {

    ERPSelfAppraisalFacultyTypeService ERPSelfAppraisalFacultyTypeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPSelfAppraisalFacultyType(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalFacultyType(...).save(flush: true, failOnError: true)
        //ERPSelfAppraisalFacultyType ERPSelfAppraisalFacultyType = new ERPSelfAppraisalFacultyType(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalFacultyType(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalFacultyType(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPSelfAppraisalFacultyType.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPSelfAppraisalFacultyTypeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPSelfAppraisalFacultyType> ERPSelfAppraisalFacultyTypeList = ERPSelfAppraisalFacultyTypeService.list(max: 2, offset: 2)

        then:
        ERPSelfAppraisalFacultyTypeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPSelfAppraisalFacultyTypeService.count() == 5
    }

    void "test delete"() {
        Long ERPSelfAppraisalFacultyTypeId = setupData()

        expect:
        ERPSelfAppraisalFacultyTypeService.count() == 5

        when:
        ERPSelfAppraisalFacultyTypeService.delete(ERPSelfAppraisalFacultyTypeId)
        sessionFactory.currentSession.flush()

        then:
        ERPSelfAppraisalFacultyTypeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPSelfAppraisalFacultyType ERPSelfAppraisalFacultyType = new ERPSelfAppraisalFacultyType()
        ERPSelfAppraisalFacultyTypeService.save(ERPSelfAppraisalFacultyType)

        then:
        ERPSelfAppraisalFacultyType.id != null
    }
}
