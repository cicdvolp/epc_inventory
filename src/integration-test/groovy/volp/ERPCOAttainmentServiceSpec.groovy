package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCOAttainmentServiceSpec extends Specification {

    ERPCOAttainmentService ERPCOAttainmentService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCOAttainment(...).save(flush: true, failOnError: true)
        //new ERPCOAttainment(...).save(flush: true, failOnError: true)
        //ERPCOAttainment ERPCOAttainment = new ERPCOAttainment(...).save(flush: true, failOnError: true)
        //new ERPCOAttainment(...).save(flush: true, failOnError: true)
        //new ERPCOAttainment(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCOAttainment.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCOAttainmentService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCOAttainment> ERPCOAttainmentList = ERPCOAttainmentService.list(max: 2, offset: 2)

        then:
        ERPCOAttainmentList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCOAttainmentService.count() == 5
    }

    void "test delete"() {
        Long ERPCOAttainmentId = setupData()

        expect:
        ERPCOAttainmentService.count() == 5

        when:
        ERPCOAttainmentService.delete(ERPCOAttainmentId)
        sessionFactory.currentSession.flush()

        then:
        ERPCOAttainmentService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCOAttainment ERPCOAttainment = new ERPCOAttainment()
        ERPCOAttainmentService.save(ERPCOAttainment)

        then:
        ERPCOAttainment.id != null
    }
}
