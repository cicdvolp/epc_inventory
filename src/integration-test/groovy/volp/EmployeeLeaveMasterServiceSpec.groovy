package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class EmployeeLeaveMasterServiceSpec extends Specification {

    EmployeeLeaveMasterService employeeLeaveMasterService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new EmployeeLeaveMaster(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveMaster(...).save(flush: true, failOnError: true)
        //EmployeeLeaveMaster employeeLeaveMaster = new EmployeeLeaveMaster(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveMaster(...).save(flush: true, failOnError: true)
        //new EmployeeLeaveMaster(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //employeeLeaveMaster.id
    }

    void "test get"() {
        setupData()

        expect:
        employeeLeaveMasterService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<EmployeeLeaveMaster> employeeLeaveMasterList = employeeLeaveMasterService.list(max: 2, offset: 2)

        then:
        employeeLeaveMasterList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        employeeLeaveMasterService.count() == 5
    }

    void "test delete"() {
        Long employeeLeaveMasterId = setupData()

        expect:
        employeeLeaveMasterService.count() == 5

        when:
        employeeLeaveMasterService.delete(employeeLeaveMasterId)
        sessionFactory.currentSession.flush()

        then:
        employeeLeaveMasterService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        EmployeeLeaveMaster employeeLeaveMaster = new EmployeeLeaveMaster()
        employeeLeaveMasterService.save(employeeLeaveMaster)

        then:
        employeeLeaveMaster.id != null
    }
}
