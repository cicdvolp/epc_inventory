package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCourseOfferingFeedbackLoadLinkingServiceSpec extends Specification {

    ERPCourseOfferingFeedbackLoadLinkingService ERPCourseOfferingFeedbackLoadLinkingService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCourseOfferingFeedbackLoadLinking(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingFeedbackLoadLinking(...).save(flush: true, failOnError: true)
        //ERPCourseOfferingFeedbackLoadLinking ERPCourseOfferingFeedbackLoadLinking = new ERPCourseOfferingFeedbackLoadLinking(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingFeedbackLoadLinking(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingFeedbackLoadLinking(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCourseOfferingFeedbackLoadLinking.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCourseOfferingFeedbackLoadLinkingService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCourseOfferingFeedbackLoadLinking> ERPCourseOfferingFeedbackLoadLinkingList = ERPCourseOfferingFeedbackLoadLinkingService.list(max: 2, offset: 2)

        then:
        ERPCourseOfferingFeedbackLoadLinkingList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCourseOfferingFeedbackLoadLinkingService.count() == 5
    }

    void "test delete"() {
        Long ERPCourseOfferingFeedbackLoadLinkingId = setupData()

        expect:
        ERPCourseOfferingFeedbackLoadLinkingService.count() == 5

        when:
        ERPCourseOfferingFeedbackLoadLinkingService.delete(ERPCourseOfferingFeedbackLoadLinkingId)
        sessionFactory.currentSession.flush()

        then:
        ERPCourseOfferingFeedbackLoadLinkingService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCourseOfferingFeedbackLoadLinking ERPCourseOfferingFeedbackLoadLinking = new ERPCourseOfferingFeedbackLoadLinking()
        ERPCourseOfferingFeedbackLoadLinkingService.save(ERPCourseOfferingFeedbackLoadLinking)

        then:
        ERPCourseOfferingFeedbackLoadLinking.id != null
    }
}
