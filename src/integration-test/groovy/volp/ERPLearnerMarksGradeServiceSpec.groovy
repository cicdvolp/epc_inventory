package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPLearnerMarksGradeServiceSpec extends Specification {

    ERPLearnerMarksGradeService ERPLearnerMarksGradeService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPLearnerMarksGrade(...).save(flush: true, failOnError: true)
        //new ERPLearnerMarksGrade(...).save(flush: true, failOnError: true)
        //ERPLearnerMarksGrade ERPLearnerMarksGrade = new ERPLearnerMarksGrade(...).save(flush: true, failOnError: true)
        //new ERPLearnerMarksGrade(...).save(flush: true, failOnError: true)
        //new ERPLearnerMarksGrade(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPLearnerMarksGrade.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPLearnerMarksGradeService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPLearnerMarksGrade> ERPLearnerMarksGradeList = ERPLearnerMarksGradeService.list(max: 2, offset: 2)

        then:
        ERPLearnerMarksGradeList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPLearnerMarksGradeService.count() == 5
    }

    void "test delete"() {
        Long ERPLearnerMarksGradeId = setupData()

        expect:
        ERPLearnerMarksGradeService.count() == 5

        when:
        ERPLearnerMarksGradeService.delete(ERPLearnerMarksGradeId)
        sessionFactory.currentSession.flush()

        then:
        ERPLearnerMarksGradeService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPLearnerMarksGrade ERPLearnerMarksGrade = new ERPLearnerMarksGrade()
        ERPLearnerMarksGradeService.save(ERPLearnerMarksGrade)

        then:
        ERPLearnerMarksGrade.id != null
    }
}
