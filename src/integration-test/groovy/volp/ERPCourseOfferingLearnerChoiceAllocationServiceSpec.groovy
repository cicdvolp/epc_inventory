package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPCourseOfferingLearnerChoiceAllocationServiceSpec extends Specification {

    ERPCourseOfferingLearnerChoiceAllocationService ERPCourseOfferingLearnerChoiceAllocationService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPCourseOfferingLearnerChoiceAllocation(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingLearnerChoiceAllocation(...).save(flush: true, failOnError: true)
        //ERPCourseOfferingLearnerChoiceAllocation ERPCourseOfferingLearnerChoiceAllocation = new ERPCourseOfferingLearnerChoiceAllocation(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingLearnerChoiceAllocation(...).save(flush: true, failOnError: true)
        //new ERPCourseOfferingLearnerChoiceAllocation(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPCourseOfferingLearnerChoiceAllocation.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPCourseOfferingLearnerChoiceAllocationService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPCourseOfferingLearnerChoiceAllocation> ERPCourseOfferingLearnerChoiceAllocationList = ERPCourseOfferingLearnerChoiceAllocationService.list(max: 2, offset: 2)

        then:
        ERPCourseOfferingLearnerChoiceAllocationList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPCourseOfferingLearnerChoiceAllocationService.count() == 5
    }

    void "test delete"() {
        Long ERPCourseOfferingLearnerChoiceAllocationId = setupData()

        expect:
        ERPCourseOfferingLearnerChoiceAllocationService.count() == 5

        when:
        ERPCourseOfferingLearnerChoiceAllocationService.delete(ERPCourseOfferingLearnerChoiceAllocationId)
        sessionFactory.currentSession.flush()

        then:
        ERPCourseOfferingLearnerChoiceAllocationService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPCourseOfferingLearnerChoiceAllocation ERPCourseOfferingLearnerChoiceAllocation = new ERPCourseOfferingLearnerChoiceAllocation()
        ERPCourseOfferingLearnerChoiceAllocationService.save(ERPCourseOfferingLearnerChoiceAllocation)

        then:
        ERPCourseOfferingLearnerChoiceAllocation.id != null
    }
}
