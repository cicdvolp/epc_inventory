package volp

import grails.testing.mixin.integration.Integration
import grails.gorm.transactions.Rollback
import spock.lang.Specification
import org.hibernate.SessionFactory

@Integration
@Rollback
class ERPSelfAppraisalSubParameterServiceSpec extends Specification {

    ERPSelfAppraisalSubParameterService ERPSelfAppraisalSubParameterService
    SessionFactory sessionFactory

    private Long setupData() {
        // TODO: Populate valid domain instances and return a valid ID
        //new ERPSelfAppraisalSubParameter(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalSubParameter(...).save(flush: true, failOnError: true)
        //ERPSelfAppraisalSubParameter ERPSelfAppraisalSubParameter = new ERPSelfAppraisalSubParameter(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalSubParameter(...).save(flush: true, failOnError: true)
        //new ERPSelfAppraisalSubParameter(...).save(flush: true, failOnError: true)
        assert false, "TODO: Provide a setupData() implementation for this generated test suite"
        //ERPSelfAppraisalSubParameter.id
    }

    void "test get"() {
        setupData()

        expect:
        ERPSelfAppraisalSubParameterService.get(1) != null
    }

    void "test list"() {
        setupData()

        when:
        List<ERPSelfAppraisalSubParameter> ERPSelfAppraisalSubParameterList = ERPSelfAppraisalSubParameterService.list(max: 2, offset: 2)

        then:
        ERPSelfAppraisalSubParameterList.size() == 2
        assert false, "TODO: Verify the correct instances are returned"
    }

    void "test count"() {
        setupData()

        expect:
        ERPSelfAppraisalSubParameterService.count() == 5
    }

    void "test delete"() {
        Long ERPSelfAppraisalSubParameterId = setupData()

        expect:
        ERPSelfAppraisalSubParameterService.count() == 5

        when:
        ERPSelfAppraisalSubParameterService.delete(ERPSelfAppraisalSubParameterId)
        sessionFactory.currentSession.flush()

        then:
        ERPSelfAppraisalSubParameterService.count() == 4
    }

    void "test save"() {
        when:
        assert false, "TODO: Provide a valid instance to save"
        ERPSelfAppraisalSubParameter ERPSelfAppraisalSubParameter = new ERPSelfAppraisalSubParameter()
        ERPSelfAppraisalSubParameterService.save(ERPSelfAppraisalSubParameter)

        then:
        ERPSelfAppraisalSubParameter.id != null
    }
}
