package volp

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class RoleController {

    RoleService roleService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]


    def registrarRoleLinks()
    {
        ////println("In registrarRoleLink  : " + params)
        Login login=Login.findById(session.loginid)
        Instructor instructor=Instructor.findByUid(login.username)

        RoleType rt = RoleType.findByTypeAndOrganization("Leave Management", instructor.organization)
        Role r =  Role.findByRoleAndRoletypeAndOrganization("Registrar",rt, instructor.organization)
        def registrarroleLinks = RoleLink.findAllByOrganizationAndRole(instructor.organization,r)

        r =  Role.findByRoleAndRoletypeAndOrganization("HOD",rt, instructor.organization)
        def hodroleLinks = RoleLink.findAllByOrganizationAndRole(instructor.organization,r)


        r =  Role.findByRoleAndRoletypeAndOrganization("Establishment Section",rt, instructor.organization)
        def estroleLinks = RoleLink.findAllByOrganizationAndRole(instructor.organization,r)


        r =  Role.findByRoleAndRoletypeAndOrganization("Faculty",rt, instructor.organization)
        def selfroleLinks = RoleLink.findAllByOrganizationAndRole(instructor.organization,r)

        [registrarroleLinks:registrarroleLinks , hodroleLinks:hodroleLinks,estroleLinks:estroleLinks,selfroleLinks:selfroleLinks]
    }

  /*  def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond roleService.list(params), model:[roleCount: roleService.count()]
    }

    def show(Long id) {
        respond roleService.get(id)
    }

    def create() {
        respond new Role(params)
    }

    def save(Role role) {
        if (role == null) {
            notFound()
            return
        }

        try {
            roleService.save(role)
        } catch (ValidationException e) {
            respond role.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'role.label', default: 'Role'), role.id])
                redirect role
            }
            '*' { respond role, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond roleService.get(id)
    }

    def update(Role role) {
        if (role == null) {
            notFound()
            return
        }

        try {
            roleService.save(role)
        } catch (ValidationException e) {
            respond role.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'role.label', default: 'Role'), role.id])
                redirect role
            }
            '*'{ respond role, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        roleService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'role.label', default: 'Role'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'role.label', default: 'Role'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    } */
}
