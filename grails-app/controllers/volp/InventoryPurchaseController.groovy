package volp

import javax.servlet.http.Part
import java.nio.file.Paths
import java.text.SimpleDateFormat

class InventoryPurchaseController {

    def index() {}

    //Add Purchase Requisition
    def addInvPR() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addInvPR params-->: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org  = instructor.organization
            def inv_material_list = InvMaterial.findAllByIsactiveAndOrganization(true,org)
            def academic_year_list = AcademicYear.findAllByIsactive(true,[sort:"ay",order:"desc"])
            //def inv_purchase_type_list = InvPurchaseType.findAllByOrganizationAndIsactive(org,true)
            def inv_budget_list = InvBudget.findAllByOrganizationAndIsactiveAndIsapprove(org,true,true)
            def inv_invitation_status_list = InvInvitationStatus.findAllByOrganizationAndIsactive(org,true)
            def department_list = Department.findAllByOrganization(org)
            def inv_vendor_list = InvVendor.findAllByOrganizationAndIsactive(org,true)
            def inv_approval_status_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org,true)
            def inv_budget_level_list = InvBudgetLevel.findAllByOrganizationAndIsactive(org,true)
            def inv_budget_type_list = InvBudgetType.findAllByOrganizationAndIsactive(org,true)
            def InvPurchaseRequisition = InvPurchaseRequisition.findAllByOrganization(org)
            //println"InvPurchaseRequisition-->"+InvPurchaseRequisition
            ArrayList array = new ArrayList()
            AWSBucketService awsBucketService = new AWSBucketService()
            AWSBucket aws = AWSBucket.findByContent("documents")
            for(i in InvPurchaseRequisition){
                //println"i----------->"+i
                HashMap hm=new HashMap()
                def awsimagelink = i.file_path
                String file = awsBucketService.getPresignedUrl(aws.bucketname,awsimagelink, aws.region)
                hm.put("url",file)
                hm.put("data",i)

                def prEscalations = InvPurchaseRequisitionEscalation.findAllByInvpurchaserequisition(i)
                hm.put("escalation",prEscalations)
                array.add(hm)
                //println"array------>"+array
            }

            [inv_material_list: inv_material_list,inv_budget_level_list: inv_budget_level_list,inv_vendor_list: inv_vendor_list,department_list: department_list,inv_invitation_status_list: inv_invitation_status_list,inv_budget_list: inv_budget_list,
             inv_budget_type_list: inv_budget_type_list,inv_approval_status_list: inv_approval_status_list,InvPurchaseRequisition: array,academic_year_list: academic_year_list]
        }
    }
    def saveInvPR() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveInvPR : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if(params.academic_year == 'null') {
                flash.error = "Please Select Academic Year..!"
                redirect(controller:"inventoryPurchase", action:"addInvPR")
                return
            }
            if(params.inv_purchase_type == 'null') {
                flash.error = "Please Select Purchase Type..!"
                redirect(controller:"inventoryPurchase", action:"addInvPR")
                return
            }
            if(params.inv_budget == 'null') {
                flash.error = "Please Select Budget..!"
                redirect(controller:"inventoryPurchase", action:"addInvPR")
                return
            }
            def department
            if(params.department != 'null' || params?.department != '' || params?.department != null){
                department = Department.findByName(params.department)
            }

            AcademicYear academicYear = AcademicYear.findByAy(params.academic_year)
            InvPurchaseType invPurchaseType = InvPurchaseType.findById(params.inv_purchase_type)
            InvBudget invBudget = InvBudget.findById(params.inv_budget)
            InvInvitationStatus invInvitationStatus = InvInvitationStatus.findByName("Not-Invited")

            InvApprovalStatus invApprovalStatus = InvApprovalStatus.findByName("In-Process")
            InvPurchaseRequisition invPurchaseRequisition = InvPurchaseRequisition.findById(params.invpr)
            def purpose = params.purpose.trim().toUpperCase()
             InvPurchaseRequisition invPurchaseRequisition1 = InvPurchaseRequisition.findByInvbudgetAndPurpose(invBudget,purpose)
            if(invPurchaseRequisition1 == null){
                if(invPurchaseRequisition) {

                    invPurchaseRequisition.purpose = purpose
                    AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                    AWSBucket awsBucket = AWSBucket.findByContent("documents")
                    AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                    Part filePart = request.getPart("newFile")
                    def fileobj = request.getFile("newFile")
                    if(!fileobj.empty){
                        String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                        int indexOfDot = filePartName.lastIndexOf('.')
                        def fileExtention
                        if (indexOfDot > 0) {
                            fileExtention = filePartName.substring(indexOfDot + 1)
                        }
                        String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                        def folderPath
                        if(params.department != 'null' || params?.department != '' || params?.department != null){
                            folderPath  = awsFolderPath.path + awsBucket?.content +"/"+"inventory/PR"+"/"+academicYear?.ay+"/"+department?.name
                        }else{
                            def BL = InvBudgetLevel.findById(params.budgetLevel)
                            folderPath  = awsFolderPath.path + awsBucket?.content +"/"+"inventory/PR"+"/"+academicYear?.ay+"/"+BL?.name
                        }
                         def fileName = params.prn.toInteger() +"_PR_"+ date +"."+ fileExtention
                        String existsFilePath = ""
                        def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                        if(isUpload){
                            invPurchaseRequisition.file_path = folderPath
                            invPurchaseRequisition.file_name = fileName
                        }else {
                            flash.error = "Something went wrong while uploading file..!"
                            println "Something went wrong while uploading file..!"
                        }
                    }

                    invPurchaseRequisition.updation_username = login.username
                    invPurchaseRequisition.updation_date = new Date()
                    invPurchaseRequisition.updation_ip_address = request.getRemoteAddr()
                    invPurchaseRequisition.save(flush: true, failOnError: true)
                    flash.message = "Updated Successfully..!"
                }
                else {
                    invPurchaseRequisition = InvPurchaseRequisition.findByOrganizationAndPrn(org,params.prn)
                    if(invPurchaseRequisition) {
                        flash.error = "Already Exists..!"
                    } else {
                        invPurchaseRequisition = new InvPurchaseRequisition()
                        invPurchaseRequisition.isactive = true
                        invPurchaseRequisition.isapproved = false
                        def PRPO  = PRPOTracking.findByOrganization(org)
                        if(PRPO == null){
                            PRPO = new PRPOTracking()
                            PRPO.PRN = 00001
                            PRPO.PON = 00001
                            PRPO.invoiceNo = 00001
                            PRPO.recieptNo = 00001
                            PRPO.organization = org
                            PRPO.creation_username = login.username
                            PRPO.updation_username = login.username
                            PRPO.creation_date = new Date()
                            PRPO.updation_date = new Date()
                            PRPO.creation_ip_address = request.getRemoteAddr()
                            PRPO.updation_ip_address = request.getRemoteAddr()
                            PRPO.save(flush: true,failOnError: true)

                            invPurchaseRequisition.prn = PRPO.PRN
                            PRPO.PRN = PRPO.PRN + 1
                        }else{
                            invPurchaseRequisition.prn = PRPO.PRN
                            PRPO.PRN = PRPO.PRN + 1
                        }
                        invPurchaseRequisition.amount = invBudget?.amount
                        invPurchaseRequisition.purpose = purpose
                        invPurchaseRequisition.request_date = new Date()
                        invPurchaseRequisition.academicyear = academicYear
                        invPurchaseRequisition.invpurchasetype = invPurchaseType
                        invPurchaseRequisition.invbudget = invBudget
                        invPurchaseRequisition.invinvitationstatus = invInvitationStatus
                        if(params.department != 'null'){
                            invPurchaseRequisition.department = department
                        }
                        invPurchaseRequisition.invapprovalstatus = invApprovalStatus
                        invPurchaseRequisition.requestedby = instructor
                        invPurchaseRequisition.organization = org
                        AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                        AWSBucket awsBucket = AWSBucket.findByContent("documents")
                        AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                        Part filePart = request.getPart("newFile")
                        def fileobj = request.getFile("newFile")
                        if(!fileobj.empty){
                            String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                            int indexOfDot = filePartName.lastIndexOf('.')
                            def fileExtention
                            if (indexOfDot > 0) {
                                fileExtention = filePartName.substring(indexOfDot + 1)
                                println("fileExtention :: " + fileExtention)
                            }
                            String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                            def folderPath
                            if(params.department != 'null'){
                                folderPath  = awsFolderPath.path + awsBucket?.content +"/"+"inventory/PR"+"/"+academicYear?.ay+"/"+department?.name
                            }else{
                                def BL = InvBudgetLevel.findById(params.budgetLevel)
                                folderPath  = awsFolderPath.path + awsBucket?.content +"/"+"inventory/PR"+"/"+academicYear?.ay+"/"+BL?.name
                            }
                            def fileName = PRPO.PRN +"_PR_"+ date +"."+ fileExtention
                            String existsFilePath = ""
                              def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                            if(isUpload){
                                invPurchaseRequisition.file_path = folderPath
                                invPurchaseRequisition.file_name = fileName
                                println "File uploaded"
                            }else {
                                println "Something went wrong while uploading file..!"
                            }

                        }
                        if (fileobj.empty) {
                            flash.error = "File is empty..!"
                        }
                        invPurchaseRequisition.creation_username = login.username
                        invPurchaseRequisition.updation_username = login.username
                        invPurchaseRequisition.creation_date = new Date()
                        invPurchaseRequisition.updation_date = new Date()
                        invPurchaseRequisition.creation_ip_address = request.getRemoteAddr()
                        invPurchaseRequisition.updation_ip_address = request.getRemoteAddr()
                        invPurchaseRequisition.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully..!"
                    }
                }
                InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Purchase Request", true, org)
                def invApprovingAuthorityLevels = InvFinanceApprovingAuthorityLevel.findAllWhere(organization:org,isactive:true, invfinanceapprovingcategory:invFinanceApprovingCategory)
                for(authority_level in invApprovingAuthorityLevels) {
                    InvPurchaseRequisitionEscalation invPurchaseRequisitionEscalation = InvPurchaseRequisitionEscalation.findByInvpurchaserequisitionAndInvfinanceapprovingauthoritylevel(invPurchaseRequisition,authority_level)
                    if(invPurchaseRequisitionEscalation) {
                        println("invPurchaseRequisitionEscalation 1 :: " + invPurchaseRequisitionEscalation)
                    }else {
                        invPurchaseRequisitionEscalation = new InvPurchaseRequisitionEscalation()
                        invPurchaseRequisitionEscalation.action_date = new Date()
                        invPurchaseRequisitionEscalation.remark = null
                        invPurchaseRequisitionEscalation.organization = org
                        invPurchaseRequisitionEscalation.invpurchaserequisition = invPurchaseRequisition
                        invPurchaseRequisitionEscalation.invfinanceapprovingauthoritylevel = authority_level
                        invPurchaseRequisitionEscalation.invapprovalstatus = invApprovalStatus
                        invPurchaseRequisitionEscalation.actionby = instructor
                        invPurchaseRequisitionEscalation.isactive = true
                        invPurchaseRequisitionEscalation.creation_username = login.username
                        invPurchaseRequisitionEscalation.updation_username = login.username
                        invPurchaseRequisitionEscalation.creation_date = new Date()
                        invPurchaseRequisitionEscalation.updation_date = new Date()
                        invPurchaseRequisitionEscalation.creation_ip_address = request.getRemoteAddr()
                        invPurchaseRequisitionEscalation.updation_ip_address = request.getRemoteAddr()
                        invPurchaseRequisitionEscalation.save(flush: true, failOnError: true)
                        //println("invPurchaseRequisitionEscalation 2 :: " + invPurchaseRequisitionEscalation)
                    }
                }
            }
            else{
                flash.error = "Requisition Already Present,Please Edit!"
            }
            redirect(controller:"inventoryPurchase", action: "addInvPR")
            return
        }
    }
    def deleteInvPR() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteInvPR :: " + params)
            InvPurchaseRequisition invPurchaseRequisition = InvPurchaseRequisition.findById(params.inv_purchase_requisition)
            if (invPurchaseRequisition) {
                try {
                    invPurchaseRequisition.delete(flush: true, failOnError: true)
                    flash.message = "Delected successfully..!"
                } catch (Exception e) {
                    flash.error = "Foreign Key Constraints,Cannot Delete,Please Turn Is-active to false..!"
                }
            } else {
                flash.error = "Purchase Requisition Not found"
            }

            redirect(controller: "inventoryPurchase", action: "addInvPR")
            return
        }
    }
    def activationInvPR() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationInvPR :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvPurchaseRequisition invPurchaseRequisition = InvPurchaseRequisition.findById(params.inv_purchase_requisition)
            def PREscalations = InvPurchaseRequisitionEscalation.findAllWhere(invpurchaserequisition:invPurchaseRequisition)
            if(invPurchaseRequisition) {
                if(invPurchaseRequisition.isactive) {
                    invPurchaseRequisition.isactive = false
                    invPurchaseRequisition.updation_username = login.username
                    invPurchaseRequisition.updation_date = new Date()
                    invPurchaseRequisition.updation_ip_address = request.getRemoteAddr()
                    invPurchaseRequisition.save(flush: true, failOnError: true)
                    for(pre in PREscalations){
                        pre.isactive = false
                        pre.save(failOnError: true,flush: true)
                    }
                    flash.message = "In-active Successfully..!"
                } else {
                    invPurchaseRequisition.isactive = true
                    invPurchaseRequisition.updation_username = login.username
                    invPurchaseRequisition.updation_date = new Date()
                    invPurchaseRequisition.updation_ip_address = request.getRemoteAddr()
                    invPurchaseRequisition.save(flush: true, failOnError: true)
                    for(pre in PREscalations){
                        pre.isactive = true
                        pre.save(failOnError: true,flush: true)
                    }
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Purchase Requisition Not found"
            }
            redirect(controller: "inventoryPurchase", action: "addInvPR")
            return
        }
    }
    def viewBudget() {
        println(" i am view Single Budget Details")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invBudget = InvBudget.findById(params.invBudgetId)
        println("invBudget >>"+invBudget)
        def invBudgetDetailsList = InvBudgetDetails.findAllByInvbudgetAndIsactive(invBudget,true)
        [invBudgetDetailsList:invBudgetDetailsList]
    }
    def displayfile() {
        println("in displayfile " + params)
        if (session.user == null) {
            redirect(controller: "login", action: 'erplogin')
        }
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def academiccalenderlist=AcademicCalendar.findAllByOrganizationAndIsactive(instructor.organization,true)

        def array=[]
        AWSBucketService awsBucketService = new AWSBucketService()
        AWSBucket aws = AWSBucket.findByContent("documents")
        for(i in academiccalenderlist)
        {
            HashMap hm=new HashMap()

            def awsimagelink = i.filepath
            String file = awsBucketService.getPresignedUrl(aws.bucketname,awsimagelink, aws.region)
            hm.put("url",file)
            hm.put("data",i)
            array.add(hm)
        }
        [academiccalenderlist:array]

    }
    def fetchBudgetInfo(){
        println"fetchBudgetInfo params-->"+params
        if(params.budgetId != null){
            def invBudget = InvBudget.findById(params.budgetId)
            def budgetAY = invBudget?.academicyear
            def budgetLevel = invBudget?.invbudgetlevel
            def budgetDept = invBudget?.department
            def budgetType = invBudget?.invbudgettype
            def activity_name = invBudget?.activity_name
            [budgetAY:budgetAY,budgetLevel:budgetLevel,budgetDept:budgetDept,budgetType:budgetType,activity_name:activity_name]
        }

    }
    def viewSingleBudgetDetails(params){
        println(" i am viewSingleBudgetDetails"+params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invBudget = InvBudget.findById(params.invBudgetId)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invBudgetDetailsList = InvBudgetDetails.findAllByInvbudgetAndIsactive(invBudget,true)//dont add is-active
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive:true)
        [invBudgetDetailsList:invBudgetDetailsList,invMaterialList:invMaterialList,invBudgetId:params.invBudgetId]
    }

    //Not in USE
    def addPRItem() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addPRItem : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org  = instructor.organization

            def Inv_Material_List = InvMaterial.findAllByOrganizationAndIsactive(org,true)
            def Inv_Purchase_Requisition_list = InvPurchaseRequisition.findAllByOrganizationAndIsactive(org,true)
            def InvPurchaseRequisitionDetails = InvPurchaseRequisitionDetails.findAllByOrganization(org)
            //println("InvPurchaseRequisitionDetails"+InvPurchaseRequisitionDetails)
            [InvPurchaseRequisitionDetails: InvPurchaseRequisitionDetails,Inv_Purchase_Requisition_list: Inv_Purchase_Requisition_list,Inv_Material_List: Inv_Material_List]
        }
    }
    def savePRItem() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("savePRItem : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if(params.materialId == null) {
                flash.error = "Please Select Material..!"
            }
            if(params.PRID == null) {
                flash.error = "Please Select Purchase Requisition..!"
            }
            if(params.materialId && params.PRID != null) {
                InvMaterial invMaterial = InvMaterial.findById(params.materialId)
                InvPurchaseRequisition invPurchaseRequisition = InvPurchaseRequisition.findById(params.PRID)
                InvPurchaseRequisitionDetails invPurchaseRequisitionDetails = InvPurchaseRequisitionDetails.findByOrganizationAndInvmaterialAndInvpurchaserequisition(org,invMaterial,invPurchaseRequisition)
                    if(invPurchaseRequisitionDetails) {
                        flash.error = "Already Exists ..!"
                    } else {
                        invPurchaseRequisitionDetails = new InvPurchaseRequisitionDetails()
                        invPurchaseRequisitionDetails.isactive = true
                        invPurchaseRequisitionDetails.quantity_required = params.quantity.toInteger()
                        invPurchaseRequisitionDetails.approx_cost_per_unit = params.costPerUnit.toDouble()
                        invPurchaseRequisitionDetails.total_cost = params.quantity.toInteger() * params.costPerUnit.toDouble()
                        invPurchaseRequisitionDetails.remark = params.remarks
                        invPurchaseRequisitionDetails.organization = org
                        invPurchaseRequisitionDetails.invpurchaserequisition = invPurchaseRequisition
                        invPurchaseRequisitionDetails.invmaterial = invMaterial
                        invPurchaseRequisitionDetails.creation_username = login.username
                        invPurchaseRequisitionDetails.updation_username = login.username
                        invPurchaseRequisitionDetails.creation_date = new Date()
                        invPurchaseRequisitionDetails.updation_date = new Date()
                        invPurchaseRequisitionDetails.creation_ip_address = request.getRemoteAddr()
                        invPurchaseRequisitionDetails.updation_ip_address = request.getRemoteAddr()
                        invPurchaseRequisitionDetails.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully..!"
                    }
            } else {
                flash.error = "Mandatory fields are missing..!"
            }
            redirect(controller:"inventoryPurchase", action: "addInvPR")
            return
        }
    }
    def editPRItem() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("editPRItem : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if(params.materialId == null) {
                flash.error = "Please Select Material..!"
            }
            if(params.PRID == null) {
                flash.error = "Please Select Purchase Requisition..!"
            }
            if(params.materialId && params.PRID && params.PRDetailsId != null) {
                InvMaterial invMaterial = InvMaterial.findById(params.materialId)
                InvPurchaseRequisition invPurchaseRequisition = InvPurchaseRequisition.findById(params.PRID)
                InvPurchaseRequisitionDetails invPurchaseRequisitionDetails = InvPurchaseRequisitionDetails.findById(params.PRDetailsId)
                if(invPurchaseRequisitionDetails) {
                    InvPurchaseRequisitionDetails invPurchaseRequisitionDetails1 = InvPurchaseRequisitionDetails.findByOrganizationAndInvmaterialAndInvpurchaserequisition(org,invMaterial,invPurchaseRequisition)
                    println("invPurchaseRequisitionDetails1 >>"+invPurchaseRequisitionDetails1)
                    if(invPurchaseRequisitionDetails?.id != invPurchaseRequisitionDetails1?.id && invPurchaseRequisitionDetails1) {
                        flash.error = "Already Exists..!"
                    } else {
                        if(params.isactive == 'on') {
                            invPurchaseRequisitionDetails.isactive = true
                        } else {
                            invPurchaseRequisitionDetails.isactive = false
                        }
                        invPurchaseRequisitionDetails.quantity_required = params.quantity.toInteger()
                        invPurchaseRequisitionDetails.approx_cost_per_unit = params.costPerUnit.toDouble()
                        invPurchaseRequisitionDetails.total_cost = params.quantity.toInteger() * params.costPerUnit.toDouble()
                        invPurchaseRequisitionDetails.remark = params.remarks
                        invPurchaseRequisitionDetails.organization = org
                        invPurchaseRequisitionDetails.invpurchaserequisition = invPurchaseRequisition
                        invPurchaseRequisitionDetails.invmaterial = invMaterial
                        invPurchaseRequisitionDetails.updation_username = login.username
                        invPurchaseRequisitionDetails.updation_date = new Date()
                        invPurchaseRequisitionDetails.updation_ip_address = request.getRemoteAddr()
                        invPurchaseRequisitionDetails.save(flush: true, failOnError: true)
                        flash.message = "Updated Successfully..!"
                    }
                }
            } else {
                flash.error = "Mandatory fields are missing..!"
            }
            redirect(controller:"inventoryPurchase", action: "viewSinglePRDetails",params:[PRID: params.PRID])
            return
        }
    }
    def deletePRItem() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deletePRItem :: " + params)
            InvPurchaseRequisitionDetails invPurchaseRequisitionDetails = InvPurchaseRequisitionDetails.findById(params.inv_prd)
            if (invPurchaseRequisitionDetails) {
                try {
                    invPurchaseRequisitionDetails.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Purchase Requisition Details can not delete..!"
                }
            } else {
                flash.error = "Purchase Requisition Details Not found"
            }
            flash.message = "Delected successfully..!"
            redirect(controller: "inventoryPurchase", action: "addPRItem")
            return
        }
    }
    def activationPRItem() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationPRItem :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvPurchaseRequisitionDetails invPurchaseRequisitionDetails = InvPurchaseRequisitionDetails.findById(params.inv_prd)
            if(invPurchaseRequisitionDetails) {
                if(invPurchaseRequisitionDetails.isactive) {
                    invPurchaseRequisitionDetails.isactive = false
                    invPurchaseRequisitionDetails.updation_username = login.username
                    invPurchaseRequisitionDetails.updation_date = new Date()
                    invPurchaseRequisitionDetails.updation_ip_address = request.getRemoteAddr()
                    invPurchaseRequisitionDetails.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully..!"
                } else {
                    invPurchaseRequisitionDetails.isactive = true
                    invPurchaseRequisitionDetails.updation_username = login.username
                    invPurchaseRequisitionDetails.updation_date = new Date()
                    invPurchaseRequisitionDetails.updation_ip_address = request.getRemoteAddr()
                    invPurchaseRequisitionDetails.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Purchase Requisition Details Not found"
            }
            redirect(controller: "inventoryPurchase", action: "addPRItem")
            return
        }
    }
    def viewSinglePRDetails(params){
        println(" i am viewSinglePRDetails"+params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invPR = InvPurchaseRequisition.findById(params.PRID)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invPRDetailsList = InvPurchaseRequisitionDetails.findAllByInvpurchaserequisitionAndIsactive(invPR,true)//dont add is-active
        //println"invPRDetailsList-->"+invPRDetailsList
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive:true)
        def isapproved = invPR?.isapproved
        //println"--->"+isapproved
        [invPRDetailsList:invPRDetailsList,invMaterialList:invMaterialList,PRID:params.PRID,isapproved:isapproved]
    }

    //Approve PR
    def getInstpost() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getInstpost : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor, true, org)
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Purchase Request", true, org)
            def invPRList = InvPurchaseRequisition.findAllWhere(organization: org)
            [invPRList:invPRList,invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority, instructor:instructor, invFinanceApprovingCategory:invFinanceApprovingCategory]
        }
    }
    def getallprdetails() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getallprdetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Purchase Request", true, org)
            InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.post)

            InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByInvfinanceapprovingcategoryAndInvfinanceapprovingauthorityAndOrganizationAndIsactive(invFinanceApprovingCategory, invInstructorFinanceApprovingAuthority.invfinanceapprovingauthority, org, true)
            if(!InvFinanceApprovingAuthorityLevel) {
                render "<div clas='alert alert-danger'>Please fill master of InvFinanceApprovingAuthorityLevel for category : " + invFinanceApprovingCategory.name + "</div>"
                return
            }
            def invPR = InvPurchaseRequisition.findById(params.PRID)
            def invPurchaseRequisitionEscalation_list = InvPurchaseRequisitionEscalation.findAllByOrganizationAndInvfinanceapprovingauthoritylevelAndIsactiveAndInvpurchaserequisition(org, invFinanceApprovingAuthorityLevel, true,invPR)
            if(invPurchaseRequisitionEscalation_list.size() == 0) {
                render "<div class='alert alert-danger'>Purchase Requestion not found for approval...</div>"
                return
            }
            println("invPurchaseRequisitionEscalation_list :: " + invPurchaseRequisitionEscalation_list)

            [invPurchaseRequisitionEscalation_list : invPurchaseRequisitionEscalation_list,
             invFinanceApprovingCategory : invFinanceApprovingCategory,
             invFinanceApprovingAuthorityLevel : invFinanceApprovingAuthorityLevel,
             invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority]
        }
    }
    def getPREscalationforApprove() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getPREscalationforApprove : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invapprovalstatus_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org, true)
            def next_level_approved = false

            InvPurchaseRequisitionEscalation invPurchaseRequisitionEscalation = InvPurchaseRequisitionEscalation.findById(params.pre_id)
            def PREscalations = InvPurchaseRequisitionEscalation.findAllByInvpurchaserequisition(invPurchaseRequisitionEscalation?.invpurchaserequisition)
            for(PRE in PREscalations)
            {
                if(PRE?.invfinanceapprovingauthoritylevel?.islast == true)
                {
                    if (PRE?.invapprovalstatus?.name == 'Approved')
                    {
                        next_level_approved = true
                        if(PRE == invPurchaseRequisitionEscalation)
                        {
                            next_level_approved = false
                        }
                    }
                }
            }
            println("next_level_approved :: " + next_level_approved)
            if(invPurchaseRequisitionEscalation) {
                def purchaseTypeList = InvPurchaseType.findAllWhere(organization: org)
                def vendorList = InvVendor.findAllWhere(organization: org,isactive: true)
                def list = InvPurchaseRequisitionEscalation.findAllByOrganizationAndInvpurchaserequisitionAndIsactive(org, invPurchaseRequisitionEscalation?.invpurchaserequisition, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [vendorList:vendorList,purchaseTypeList:purchaseTypeList,list:list, invPurchaseRequisitionEscalation : invPurchaseRequisitionEscalation, invapprovalstatus_list:invapprovalstatus_list, next_level_approved:next_level_approved]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }
    def saveApprovePR() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveApprovePR : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvApprovalStatus invApprovalStatus =  InvApprovalStatus.findById(params.approvingstatus)
            InvPurchaseRequisitionEscalation invPurchaseRequisitionEscalation = InvPurchaseRequisitionEscalation.findById(params.pre_id)
            if(invPurchaseRequisitionEscalation) {

                invPurchaseRequisitionEscalation.action_date = new Date()
                invPurchaseRequisitionEscalation.remark = params.remark
                invPurchaseRequisitionEscalation.isactive = true
                invPurchaseRequisitionEscalation.actionby = instructor
                invPurchaseRequisitionEscalation.updation_username = login.username
                invPurchaseRequisitionEscalation.updation_date = new Date()
                invPurchaseRequisitionEscalation.updation_ip_address = request.getRemoteAddr()
                invPurchaseRequisitionEscalation.invapprovalstatus = invApprovalStatus
                invPurchaseRequisitionEscalation.save(flush: true, failOnError: true)

                if(invPurchaseRequisitionEscalation?.invfinanceapprovingauthoritylevel?.islast) {
                    InvPurchaseRequisition invPurchaseRequisition = invPurchaseRequisitionEscalation.invpurchaserequisition
                    invPurchaseRequisition.invapprovalstatus = invApprovalStatus

                    if(invApprovalStatus?.name == "Rejected")
                    {
                        invPurchaseRequisition.isapproved = false
                        invPurchaseRequisition.invpurchasetype = null
                        invPurchaseRequisition.invvendor = null

                    }
                    else if(invApprovalStatus?.name == "Approved")
                    {
                        invPurchaseRequisition.isapproved = true
                        def purchaseType = InvPurchaseType.findById(params.purchaseType)
                        if(purchaseType.name == "Direct"){
                            invPurchaseRequisition.invpurchasetype = purchaseType
                            def vendor = InvVendor.findById(params.vendor)
                            invPurchaseRequisition.invvendor = vendor
                        }
                    }
                    else if(invApprovalStatus?.name == "In-Process")
                    {
                        invPurchaseRequisition.isapproved = false
                        invPurchaseRequisition.invpurchasetype = null
                        invPurchaseRequisition.invvendor = null
                    }



                    invPurchaseRequisition.updation_username = login.username
                    invPurchaseRequisition.updation_date = new Date()
                    invPurchaseRequisition.updation_ip_address = request.getRemoteAddr()
                    invPurchaseRequisition.save(flush: true, failOnError: true)
                }

                def list = InvPurchaseRequisitionEscalation.findAllByOrganizationAndInvpurchaserequisitionAndIsactive(org, invPurchaseRequisitionEscalation?.invpurchaserequisition, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [list:list, invPurchaseRequisitionEscalation : invPurchaseRequisitionEscalation]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }
    def approvePR() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addApprovePR : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org  = instructor.organization

            def academic_year_list = AcademicYear.findAllByIsactive(true)
            def inv_purchase_type_list = InvPurchaseType.findAllByOrganizationAndIsactive(org,true)
            def department_list = Department.findAllByOrganization(org)
            def inv_approval_status_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org,true)

            [inv_approval_status_list:inv_approval_status_list,department_list:department_list,academic_year_list:academic_year_list,inv_purchase_type_list:inv_purchase_type_list]
        }
    }
    def fetchApprovePR() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("fetchApprovePR : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org  = instructor.organization

            def academic_year = AcademicYear.findById(params.academic_year)
            def inv_purchase_type = InvPurchaseType.findById(params.inv_purchase_type)
            def department = Department.findById(params.department)
            def inv_approval_status = InvApprovalStatus.findById(params.inv_approval_status)

            def InvPurchaseRequisitionList = InvPurchaseRequisition.findAllByOrganizationAndIsactiveAndAcademicyearAndInvpurchasetypeAndDepartmentAndInvapprovalstatus(org,true,academic_year,inv_purchase_type,department,inv_approval_status)
            println("InvPurchaseRequisition >>"+InvPurchaseRequisitionList)
            [InvPurchaseRequisitionList: InvPurchaseRequisitionList]
            redirect(controller: "inventoryPurchase", action: "approvePR")
        }
    }

    //Reports
    def viewPR(){
        println(" i am viewPR Accounts Dept")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invPRList = InvPurchaseRequisition.findAllWhere(organization: org,isapproved: true)
        [invPRList:invPRList]
    }
    def viewPRDetails(){
        println(" i am viewPRDetails" + params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            redirect(action: 'viewSinglePRDetails',params:params)
        }
    }

    //InvPurchaseRequisitionEscalation or InvPurchaseRequisitionHistory
    /*def fetchInvPurchaseRequisitionEscalation() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addInvPurchaseRequisitionEscalation : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org  = instructor.organization

            //def academic_year_list = AcademicYear.findAllByIsactive(true)
            //def inv_purchase_type_list = InvPurchaseType.findAllByOrganizationAndIsactive(org,true)
            //def department_list = Department.findAllByOrganization(org)
            def InvPurchaseRequisitionEscalationList = InvPurchaseRequisitionEscalation.findAllByOrganization(org)

            [InvPurchaseRequisitionEscalationList:InvPurchaseRequisitionEscalationList]
        }
    }*/

    //Purchase Order
    def addPO(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addPO : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def quotationList = InvQuotation.findAllWhere(organization: instructor?.organization,isapproved:true,isactive: true)
            def invPRList = InvPurchaseRequisition.findAllWhere(organization: instructor?.organization,isapproved:true,isactive: true)
            def purchaseOrders = InvPurchaseOrder.findAllWhere(organization: instructor?.organization)
            ArrayList POList = new ArrayList()
            HashMap hm = new HashMap()
            for(i in purchaseOrders){
                def poEscalations = InvPurchaseOrderEscalation.findAllByInvpurchaseorder(i)
                hm.put("data",i)
                hm.put("escalation",poEscalations)
                POList.add(hm)
            }
            //println"------>"+POList
            def invMaterialList = InvMaterial.findAllWhere(organization: instructor?.organization,isactive:true)
            [invPRList:invPRList,quotationList:quotationList,POList:POList,invMaterialList:invMaterialList]
        }
    }
    def fetchQuotationDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addPO : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def invPR = InvPurchaseRequisition.findById(params.PR)
            def quotation = InvQuotation.findByIsapprovedAndOrganizationAndInvpurchaserequisition(true,instructor?.organization,invPR)
            def QFound = false
            if(quotation !=null){
                QFound = true
            }
            [quotation:quotation,QFound:QFound]
        }
    }
    def savePO(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println"In savePO" + params
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invPR = InvPurchaseRequisition.findById(params.PR)
            def invQuo = InvQuotation.findByInvpurchaserequisitionAndIsapproved(invPR,true)

            def amt =java.lang.Double.parseDouble(params.namount)
            def tax =java.lang.Double.parseDouble(params.ntax)
            if(params.namount !='null' && params.ntax !='null' && invQuo != null){
                def invPO = InvPurchaseOrder.findByInvquotation(invQuo)
                if(invPO == null){
                    invPO = new InvPurchaseOrder()
                    def PRPO = PRPOTracking.findByOrganization(org)
                    invPO.pon = PRPO.PON
                    PRPO.PON++
                    invPO.amount = amt
                    invPO.tax = tax
                    invPO.total = (amt + (amt*tax)/100)
                    invPO.purpose = params.npurpose
                    invPO.purchase_order_date = new java.util.Date()
                    invPO.isactive = true
                    invPO.isapproved =false

                    invPO.invvendor =invQuo?.invvendor
                    invPO.organization =org
                    invPO.invquotation =invQuo
                    invPO.releasedby =instructor

                    invPO.creation_username = login.username
                    invPO.updation_username = login.username
                    invPO.creation_date = new Date()
                    invPO.updation_date = new Date()
                    invPO.creation_ip_address = request.getRemoteAddr()
                    invPO.updation_ip_address = request.getRemoteAddr()
                    invPO.save(flush: true, failOnError: true)

                    def invApprovalStatus = InvApprovalStatus.findByNameAndIsactive("In-Process",true)
                    def invFinanceApprovingCategory = InvFinanceApprovingCategory.findByName("Purchase Order")
                    def invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findAllWhere(organization:org,isactive:true,invfinanceapprovingcategory:invFinanceApprovingCategory)
                    for(authLevels in invFinanceApprovingAuthorityLevel){
                        InvPurchaseOrderEscalation invPurchaseOrderEscalation = InvPurchaseOrderEscalation.findByInvpurchaseorderAndInvfinanceapprovingauthoritylevel(invPO,authLevels)
                        if(invPurchaseOrderEscalation == null)
                        {
                            invPurchaseOrderEscalation = new InvPurchaseOrderEscalation()

                            invPurchaseOrderEscalation.remark = invPO?.purpose
                            invPurchaseOrderEscalation.action_date = new java.util.Date()

                            invPurchaseOrderEscalation.organization = org
                            invPurchaseOrderEscalation.invpurchaseorder = invPO
                            invPurchaseOrderEscalation.invvendor = invPO?.invvendor
                            invPurchaseOrderEscalation.invfinanceapprovingauthoritylevel = authLevels
                            invPurchaseOrderEscalation.invapprovalstatus = invApprovalStatus
                            invPurchaseOrderEscalation.actionby = instructor
                            invPurchaseOrderEscalation.isactive = true
                            /*
                            if(authLevels?.level_no == 1){
                                invPurchaseOrderEscalation.isactive = true
                            }
                            else{
                                invPurchaseOrderEscalation.isactive = false
                            }
                            */
                            invPurchaseOrderEscalation.creation_username = instructor.uid
                            invPurchaseOrderEscalation.updation_username = instructor.uid
                            invPurchaseOrderEscalation.updation_ip_address = request.getRemoteAddr()
                            invPurchaseOrderEscalation.creation_ip_address = request.getRemoteAddr()
                            invPurchaseOrderEscalation.updation_date = new java.util.Date()
                            invPurchaseOrderEscalation.creation_date = new java.util.Date()
                            invPurchaseOrderEscalation.save(flush: true, failOnError: true)
                            println"saving invPurchaseOrderEscalation for Authority Level " + authLevels?.level_no

                        }else{
                            continue
                        }
                    }
                    flash.message= "PO Succesfully Added"
                }else{
                    flash.error= "PO for this quotation already exist."
                }
            }else{
                flash.error = "Get Quotations And Approve Them to Proceed"
            }

        }
        redirect(controller:"inventoryPurchase", action: "addPO")
    }
    def editPO(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
        println"editPO"+params
            if(params.POId != 'null' || params.quotationId != 'null'){
                println"In editPO"
                Login login = Login.findById(session.loginid)
                def invPO = InvPurchaseOrder.findById(params.POId)
                def quotation = InvQuotation.findById(params.quotationId)
                def invPOEscalations = InvPurchaseOrderEscalation.findAllByInvpurchaseorder(invPO)
                if(quotation != null){
                    if(invPO != null){
                        if(params.amount){invPO.amount = java.lang.Double.parseDouble(params.amount)}
                        if(params.tax){ invPO.tax = java.lang.Double.parseDouble(params.tax)}
                        if(params.total){invPO.total = java.lang.Double.parseDouble(params.total)}
                        if(params.isactive){invPO.isactive = true
                            for(poe in invPOEscalations){
                                poe.isactive = true
                                poe.save(flush: true,failOnError: true)
                            }}
                        else{
                            invPO.isactive = false
                            for(poe in invPOEscalations){
                                poe.isactive = false
                                poe.save(flush: true,failOnError: true)
                            }
                        }
                        invPO.updation_username = login.username
                        invPO.updation_date = new Date()
                        invPO.updation_ip_address = request.getRemoteAddr()
                        invPO.save(flush: true, failOnError: true)
                        flash.message= "PO Succesfully Edited"
                    }else{
                        flash.error= "Purhcase Order Not Found"
                    }
                }else{flash.error = "Error Finding Quotation,Please retry"}
            }else{
                flash.error = "Something Went Wrong,Please try again later."
            }
        }
        redirect(controller:"inventoryPurchase", action: "addPO")
    }
    def activatePO(){
        println"params"+params
        Login login = Login.findById(session.loginid)
        InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findById(params.invPOId)
        if(invPurchaseOrder) {
            if(params.isactive) {
                invPurchaseOrder.isactive = true
                invPurchaseOrder.updation_username = login.username
                invPurchaseOrder.updation_date = new Date()
                invPurchaseOrder.updation_ip_address = request.getRemoteAddr()
                invPurchaseOrder.save(flush: true, failOnError: true)
                flash.message = "Succesful"
            } else {
                invPurchaseOrder.isactive = false
                invPurchaseOrder.updation_username = login.username
                invPurchaseOrder.updation_date = new Date()
                invPurchaseOrder.updation_ip_address = request.getRemoteAddr()
                invPurchaseOrder.save(flush: true, failOnError: true)
                flash.message = "Succesful"
            }
        } else {
            flash.error = "Purchase Order Not found"
        }
        return
        redirect(controller:"inventoryPurchase", action: "addPO")
    }
    def viewSinglePODetails(){
        println(" i am viewSinglePODetails"+params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invPO = InvPurchaseOrder.findById(params.POID)
        def invPODetailsList = InvPurchaseOrderDetails.findAllByInvpurchaseorderAndIsactive(invPO,true)
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive:true)
        def isapproved = invPO?.isapproved

        [invPODetailsList:invPODetailsList,invMaterialList:invMaterialList,POID:params.POID,isapproved:isapproved]

    }

    def addPODetails(){
        println(" i am addPODetails")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invPODetails = InvPurchaseOrderDetails.findAllWhere(organization: org)
        def invPOList = InvPurchaseOrder.findAllWhere(organization: org,isactive:true)
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive:true)
        [invPODetails:invPODetails,invPOList:invPOList,invMaterialList:invMaterialList]

    }
    def savePODetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.POID == null || params.materialId == null)
            {
                flash.message = "Kindly fill all required Details"
            }
            else{
                println" i am savePODetails"+params
                Login login = Login.findById(session.loginid)
                Instructor instructor = Instructor.findByUid(login.username)
                def org = instructor.organization
                InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findById(params.POID)
                InvMaterial invMaterial = InvMaterial.findById(params.materialId)
                int qty = Integer.parseInt(params.quantity)
                double acpu = Double.parseDouble(params.costPerUnit)
                InvPurchaseOrderDetails invPurchaseOrderDetails = InvPurchaseOrderDetails.findByInvpurchaseorderAndInvmaterialAndOrganization(invPurchaseOrder,invMaterial,org)

                if(invPurchaseOrderDetails == null){
                    invPurchaseOrderDetails = new InvPurchaseOrderDetails()
                    invPurchaseOrderDetails.quantity = qty
                    invPurchaseOrderDetails.cost_per_unit = acpu
                    invPurchaseOrderDetails.total_cost = qty * acpu
                    //invPurchaseOrderDetails.remark = params.remarks
                    invPurchaseOrderDetails.invpurchaseorder = invPurchaseOrder
                    invPurchaseOrderDetails.invmaterial = invMaterial
                    invPurchaseOrderDetails.organization = org
                    invPurchaseOrderDetails.isactive = true
                    /if(params.isactive == 'on') {
                invBudgetDetails.isactive = true
            } else {
                invBudgetDetails.isactive = false
            }*/
                    invPurchaseOrderDetails.creation_username = instructor.uid
                    invPurchaseOrderDetails.updation_username = instructor.uid
                    invPurchaseOrderDetails.updation_ip_address = request.getRemoteAddr()
                    invPurchaseOrderDetails.creation_ip_address = request.getRemoteAddr()
                    invPurchaseOrderDetails.updation_date = new java.util.Date()
                    invPurchaseOrderDetails.creation_date = new java.util.Date()

                    invPurchaseOrderDetails.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                }else{
                    flash.error = "Duplicate Entry"
                }
            }
        }
        redirect(action: 'addPO')
    }
    def editPODetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
            if(params.POID == 'null' || params.materialId == 'null')
            {
                flash.error = "Kindly fill all required Details"
            }
            else{
                println" i am editPODetails"+params
                Login login = Login.findById(session.loginid)
                Instructor instructor = Instructor.findByUid(login.username)
                def org = instructor.organization
                InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findById(params.POID)
                InvMaterial invMaterial = InvMaterial.findById(params.materialId)
                int qty = Integer.parseInt(params.quantity)
                double acpu = Double.parseDouble(params.costPerUnit)
                InvPurchaseOrderDetails invPurchaseOrderDetails = InvPurchaseOrderDetails.findById(params.PODetailsId)
                if(invPurchaseOrderDetails != null){
                    invPurchaseOrderDetails.quantity = qty
                    invPurchaseOrderDetails.cost_per_unit = acpu
                    invPurchaseOrderDetails.total_cost = qty * acpu
                    invPurchaseOrderDetails.invpurchaseorder = invPurchaseOrder
                    invPurchaseOrderDetails.invmaterial = invMaterial
                    invPurchaseOrderDetails.organization = org
                    /*if(params.isactive == 'on') {
                        invPurchaseOrderDetails.isactive = true
                    } else {
                        invPurchaseOrderDetails.isactive = false
                    }*/
                    invPurchaseOrderDetails.updation_username = instructor.uid
                    invPurchaseOrderDetails.updation_ip_address = request.getRemoteAddr()
                    invPurchaseOrderDetails.updation_date = new java.util.Date()
                    invPurchaseOrderDetails.save(flush: true, failOnError: true)
                    flash.message = "Updated Successfully"
                }else{
                    flash.error = 'Record Not Found'
                }
            }
        }
        redirect(action: 'viewSinglePODetails',params:[POID:params.POID] )
    }
    def deletePODetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            println(" i am deletePODetails")
            InvPurchaseOrderDetails invPurchaseOrderDetails = InvPurchaseOrderDetails.findById(params.PODetailsId)
            if (invPurchaseOrderDetails == null)
            {
                flash.error = "Record Not Found"
            }
            else {
                invPurchaseOrderDetails.delete(flush: true, failOnError: true)
                flash.message = "Deleted Successfully"
            }
        }
        redirect(action: 'addPODetails')
    }
    def activatePODetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            println(" i am activatePODetails" + params)
            Login login = Login.findById(session.loginid)
            InvPurchaseOrderDetails invPurchaseOrderDetails = InvPurchaseOrderDetails.findById(params.PODetailsId)
            if(invPurchaseOrderDetails) {
                if(invPurchaseOrderDetails.isactive) {
                    invPurchaseOrderDetails.isactive = false
                    invPurchaseOrderDetails.updation_username = login.username
                    invPurchaseOrderDetails.updation_date = new Date()
                    invPurchaseOrderDetails.updation_ip_address = request.getRemoteAddr()
                    invPurchaseOrderDetails.save(flush: true, failOnError: true)
                    flash.message = "In-Active Successfully.."
                } else {
                    invPurchaseOrderDetails.isactive = true
                    invPurchaseOrderDetails.updation_username = login.username
                    invPurchaseOrderDetails.updation_date = new Date()
                    invPurchaseOrderDetails.updation_ip_address = request.getRemoteAddr()
                    invPurchaseOrderDetails.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Budget Details Not found"
            }

        }
        redirect(action: 'addPODetails')
    }

    //Not in USE
    def approvePO(){
        println(" i am approvePO")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def login_int_dept = instructor?.department
        def invPOEscalationList = new ArrayList()
        def invInstructoryFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor,true,org)
        for(auths in invInstructoryFinanceApprovingAuthority)
        {
            if(auths?.instructor?.department == login_int_dept)
            {
                def invFinanceApprovingAuthority = auths?.invfinanceapprovingauthority
                def invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findAllWhere(invfinanceapprovingauthority:invFinanceApprovingAuthority,isactive:true,organization:org)
                for(levels in invFinanceApprovingAuthorityLevel){
                    def invPOEscalation = InvPurchaseOrderEscalation.findByInvfinanceapprovingauthoritylevelAndIsactiveAndOrganization(levels,true,org)
                    if(invPOEscalation != null){
                        invPOEscalationList.push(invPOEscalation)
                    }
                }
            }
        }
        def invApprovalStatusList = InvApprovalStatus.findAllWhere(organization:org)
        [invPOEscalationList:invPOEscalationList,invApprovalStatusList:invApprovalStatusList,instructor:instructor]

    }
    def updatePOStatus(){
        println"i am in updatePOStatus"+params
        if(params.invPOId != 'null' && params.approvalStatusId != 'null' && params.POEId != 'null'){
            InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findById(params.invPOId)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            InvApprovalStatus invApprovalStatus = InvApprovalStatus.findById(params.approvalStatusId)
            if(invPurchaseOrder != null && invApprovalStatus != null){
                def escalations = InvPurchaseOrderEscalation.findAllWhere(invpurchaseorder:invPurchaseOrder)
                def singleEsc = InvPurchaseOrderEscalation.findById(params.POEId)
                for(esc in escalations)
                {
                    if(esc?.invfinanceapprovingauthoritylevel?.level_no == singleEsc?.invfinanceapprovingauthoritylevel?.level_no)
                    {
                        if(invApprovalStatus?.name == "In-Process"){
                            esc.remark = params.remark
                            esc.actionby = instructor
                            esc.save(flush: true,failOnError: true)
                        }
                        else if(invApprovalStatus?.name == "Approved")
                        {
                            esc.invapprovalstatus = invApprovalStatus
                            esc.isactive =false
                            esc.remark = params.remark
                            esc.actionby = instructor
                            esc.action_date = new java.util.Date()
                            esc.save(flush: true,failOnError: true)
                            if(esc?.invfinanceapprovingauthoritylevel?.islast == true){
                                invPurchaseOrder.isapproved = true
                                //invPurchaseOrder.invapprovalstatus = invApprovalStatus
                                invPurchaseOrder.save(flush: true,failOnError: true)
                            }

                        }else if(invApprovalStatus.name == "Rejected")
                        {
                            invPurchaseOrder.isactive = false;
                            invPurchaseOrder.save(flush: true,failOnError: true)

                            //Reject All Escalations ----Isactive made false
                            for(esc1 in escalations)
                            {
                                esc1.isactive = false;
                                esc1.action_date = new java.util.Date()
                                esc1.save(flush: true,failOnError: true)
                            }

                            //Reject All PO Details ----Isactive made false
                            def PODetails = InvPurchaseOrderDetails.findAllWhere(invpurchaseorder:invPurchaseOrder)
                            for(dets in PODetails)
                            {
                                dets.isactive = false
                                dets.save(flush: true,failOnError: true)
                            }
                            break
                        }
                        else{
                            flash.error = "Status Updation Error"
                        }
                    }else if(esc?.invfinanceapprovingauthoritylevel?.level_no > singleEsc?.invfinanceapprovingauthoritylevel?.level_no)
                    {
                        esc.isactive =true;
                        esc.action_date = new java.util.Date()
                        esc.save(flush: true,failOnError: true)
                        break;
                    }
                }
                flash.message = "Saved Successfully"
            }else
            {
                flash.error = "Record Not Found"
            }
        }else{
            flash.error = "Values Missing"
        }
        redirect(controller:"inventoryPurchase", action: "approvePO")
    }

    //Not in USE
    def viewApprovalHistory(){
        println(" i am viewApprovalHistory")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            redirect(action: 'viewApprovalHistoryDetails',params:params)
        }
    }
    def viewApprovalHistoryDetails(params){
        println(" i am viewApprovalHistoryDetails")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invPurchase = InvPurchaseOrder.findById(params.invPOId)
        def invPOEscalation= InvPurchaseOrderEscalation.findAllByInvpurchaseorder(invPurchase)
        [invPOEscalation:invPOEscalation]

    }

    //Purchase Order Reports
    def viewPO(){
        println(" i am viewPO Accounts Dept")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
       def invPOList = InvPurchaseOrder.findAllWhere(organization: org,isapproved: true)
        [invPOList:invPOList]
    }
    def viewPODetails(){
        println(" i am viewPODetails" + params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            redirect(action: 'viewSinglePRDetails',params:params)
        }
    }

    //Approve PO
    def getInstpostPO() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getInstpostPO : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invPOList = InvPurchaseOrder.findAllWhere(organization: org)
            def invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor, true, org)
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Purchase Order", true, org)
            [invPOList:invPOList,invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority, instructor:instructor, invFinanceApprovingCategory:invFinanceApprovingCategory]
        }
    }
    def getallPOdetails() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getallPOdetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Purchase Order", true, org)
            InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.post)

            InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByInvfinanceapprovingcategoryAndInvfinanceapprovingauthorityAndOrganizationAndIsactive(invFinanceApprovingCategory, invInstructorFinanceApprovingAuthority.invfinanceapprovingauthority, org, true)
            if(!InvFinanceApprovingAuthorityLevel) {
                render "<div clas='alert alert-danger'>Please fill master of InvFinanceApprovingAuthorityLevel for category : " + invFinanceApprovingCategory.name + "</div>"
                return
            }
            def invPO = InvPurchaseOrder.findById(params.POID)
            def invPOEscalation = InvPurchaseOrderEscalation.findAllByOrganizationAndInvfinanceapprovingauthoritylevelAndIsactiveAndInvpurchaseorder(org, invFinanceApprovingAuthorityLevel, true,invPO)
            if(invPOEscalation.size() == 0) {
                render "<div class='alert alert-danger'>Purchase Order not found for approval...</div>"
                return
            }
            println("invPOEscalation :: " + invPOEscalation)

            [invPOEscalation : invPOEscalation,
             invFinanceApprovingCategory : invFinanceApprovingCategory,
             invFinanceApprovingAuthorityLevel : invFinanceApprovingAuthorityLevel,
             invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority]
        }
    }
    def getPOEscalationforApprove() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getPOEscalationforApprove : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invapprovalstatus_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org, true)
            def next_level_approved = false

            InvPurchaseOrderEscalation invPurchaseOrderEscalation = InvPurchaseOrderEscalation.findById(params.poe_id)
            def POEscalations = InvPurchaseOrderEscalation.findAllByInvpurchaseorder(invPurchaseOrderEscalation?.invpurchaseorder)
            for(POE in POEscalations){
                if(POE?.invfinanceapprovingauthoritylevel?.islast == true) {
                    if (POE?.invapprovalstatus?.name == 'Approved') {
                        next_level_approved = true
                        if(POE == invPurchaseOrderEscalation){
                            next_level_approved = false
                        }
                    }
                }
            }
            println("next_level_approved :: " + next_level_approved)
            if(invPurchaseOrderEscalation) {
                def list = InvPurchaseOrderEscalation.findAllByOrganizationAndInvpurchaseorderAndIsactive(org,invPurchaseOrderEscalation?.invpurchaseorder,true)
                [list:list, invPurchaseOrderEscalation : invPurchaseOrderEscalation, invapprovalstatus_list:invapprovalstatus_list, next_level_approved:next_level_approved]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }
    def saveApprovePO() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveApprovePO : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvApprovalStatus invApprovalStatus =  InvApprovalStatus.findById(params.approvingstatus)
            InvPurchaseOrderEscalation invPurchaseOrderEscalation = InvPurchaseOrderEscalation.findById(params.poe_id)
            println"-->invPurchaseOrderEscalation"+invPurchaseOrderEscalation
            if(invPurchaseOrderEscalation) {
                invPurchaseOrderEscalation.action_date = new Date()
                invPurchaseOrderEscalation.remark = params.remark
                invPurchaseOrderEscalation.isactive = true
                invPurchaseOrderEscalation.actionby = instructor
                invPurchaseOrderEscalation.updation_username = login.username
                invPurchaseOrderEscalation.updation_date = new Date()
                invPurchaseOrderEscalation.updation_ip_address = request.getRemoteAddr()
                invPurchaseOrderEscalation.invapprovalstatus = invApprovalStatus
                invPurchaseOrderEscalation.save(flush: true, failOnError: true)

                if(invPurchaseOrderEscalation?.invfinanceapprovingauthoritylevel?.islast) {
                    InvPurchaseOrder invPurchaseOrder = invPurchaseOrderEscalation.invpurchaseorder
                    if(invApprovalStatus?.name == "Rejected")
                    {
                        invPurchaseOrder.isapproved = false
                    }
                    else if(invApprovalStatus?.name == "Approved")
                    {
                        invPurchaseOrder.isapproved = true
                    }
                    else if(invApprovalStatus?.name == "In-Process")
                    {
                        invPurchaseOrder.isapproved = false
                    }
                    invPurchaseOrder.updation_username = login.username
                    invPurchaseOrder.updation_date = new Date()
                    invPurchaseOrder.updation_ip_address = request.getRemoteAddr()
                    invPurchaseOrder.save(flush: true, failOnError: true)
                }

                def list = InvPurchaseOrderEscalation.findAllByOrganizationAndinvpurchaseorderAndIsactive(org, invPurchaseOrderEscalation?.invpurchaseorder, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [list:list, InvPurchaseOrderEscalation : InvPurchaseOrderEscalation]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }

    //Approve Quotation
    def getInstpostQuotation() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getInstpostQuotation : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invQuotationList = InvQuotation.findAllWhere(organization: org,isactive: true)
            def invPRList = InvPurchaseRequisition.findAllWhere(organization: org,isapproved: true)
            def invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor, true, org)
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Quotation", true, org)
            [invPRList:invPRList,invQuotationList:invQuotationList,invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority, instructor:instructor, invFinanceApprovingCategory:invFinanceApprovingCategory]
        }
    }
    def getallQuotationdetails() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getallQuotationdetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Quotation", true, org)
            InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.post)
            InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByInvfinanceapprovingcategoryAndInvfinanceapprovingauthorityAndOrganizationAndIsactive(invFinanceApprovingCategory, invInstructorFinanceApprovingAuthority.invfinanceapprovingauthority, org, true)
            if(!InvFinanceApprovingAuthorityLevel) {
                render "<div clas='alert alert-danger'>Please fill master of InvFinanceApprovingAuthorityLevel for category : " + invFinanceApprovingCategory.name + "</div>"
                return
            }
            InvPurchaseRequisition invPR = InvPurchaseRequisition.findById(params.PR)
            println"invPR--->"+invPR
            def quotationEscalations = InvQuotationEscalation.findAllByOrganizationAndInvfinanceapprovingauthoritylevelAndIsactiveAndInvpurchaserequisition(org, invFinanceApprovingAuthorityLevel, true,invPR)
            println"quotationEscalations----->"+quotationEscalations
           if(quotationEscalations.size() == 0) {
                render "<div class='alert alert-danger'>Quotation not found for approval...</div>"
                return
            }
            [quotationEscalations : quotationEscalations,
             invFinanceApprovingCategory : invFinanceApprovingCategory,
             invFinanceApprovingAuthorityLevel : invFinanceApprovingAuthorityLevel,
             invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority,
            QuotationId:params.QuotationId]
        }
    }
    def getQuotationEscalationforApprove() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getQuotationEscalationforApprove : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            def invapprovalstatus_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org, true)
            def next_level_approved = false
            def invQuo = InvQuotation.findById(params.QuotationId)
            def QOEscalations = InvQuotationEscalation.findAllByInvquotation(invQuo)
            def invQuotationEscalation = InvQuotationEscalation.findById(params.qoe_id)
            for(QOE in QOEscalations){
                if(QOE?.invfinanceapprovingauthoritylevel?.islast == true) {
                    if (QOE?.invapprovalstatus?.name == 'Approved') {
                        next_level_approved = true
                        if(QOE == invQuotationEscalation){
                            next_level_approved = false
                        }
                    }
                }
            }
            println("next_level_approved :: " + next_level_approved)
            if(invQuo) {                def list = InvQuotationEscalation.findAllWhere(organization: org,invquotation: invQuo,isactive: true)

                 [list:list, invQuotationEscalation : invQuotationEscalation, invapprovalstatus_list:invapprovalstatus_list, next_level_approved:next_level_approved]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }
    def saveApproveQuotation() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveApproveQuotation : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvApprovalStatus invApprovalStatus =  InvApprovalStatus.findById(params.approvingstatus)
            InvQuotationEscalation invQuotationEscalation = InvQuotationEscalation.findById(params.qoe_id)
            InvQuotation invQuotation = invQuotationEscalation?.invquotation
            if(invQuotationEscalation) {
                invQuotationEscalation.action_date = new Date()
                invQuotationEscalation.remark = params.remark
                invQuotationEscalation.isactive = true
                invQuotationEscalation.actionby = instructor
                invQuotationEscalation.updation_username = login.username
                invQuotationEscalation.updation_date = new Date()
                invQuotationEscalation.updation_ip_address = request.getRemoteAddr()
                invQuotationEscalation.invapprovalstatus = invApprovalStatus
                invQuotationEscalation.save(flush: true, failOnError: true)

                if(invQuotationEscalation?.invfinanceapprovingauthoritylevel?.islast) {
                    if(invApprovalStatus?.name == "Rejected")
                    {
                        invQuotation.isapproved = false
                    }
                    else if(invApprovalStatus?.name == "Approved")
                    {
                        invQuotation.isapproved = true
                        def invPR = InvPurchaseRequisition.findById(invQuotationEscalation?.invpurchaserequisition?.id)
                        for(item in InvQuotation.findAllByOrganizationAndInvpurchaserequisition(org,invPR)){
                            if(item?.id == invQuotation?.id){
                                continue
                            }else{
                                item.isapproved = false
                                item.save(flush: true,failOnError: true)
                                for(QE in InvQuotationEscalation.findAllByInvquotation(item)){
                                    def status = InvApprovalStatus.findByName("Rejected")
                                    QE.invapprovalstatus = status
                                    QE.remark = "Some other Quotation Approved"
                                    QE.save(flush: true,failOnError: true)
                                }
                            }
                        }
                    }
                    else if(invApprovalStatus?.name == "In-Process")
                    {
                        invQuotation.isapproved = false
                    }

                    invQuotation.updation_username = login.username
                    invQuotation.updation_date = new Date()
                    invQuotation.updation_ip_address = request.getRemoteAddr()
                    invQuotation.save(flush: true, failOnError: true)
                }

                //def list = invQuotationEscalation.findAllByOrganizationAndInvQuotationAndIsactive(org, invQuotationEscalation?.invquotation, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                def list = invQuotationEscalation.findAllWhere(organization: org,invquotation:  invQuotationEscalation?.invquotation,isactive:  true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [list:list, invQuotationEscalation : invQuotationEscalation]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }

        }
    }
    def viewQuotationDetails(){
        println"viewQuotationDetails-->"+params
        println(" i am viewSingleBudgetDetails"+params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invQuotation = InvQuotation.findById(params.QuotationId)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invQuotationDetailsList = InvQuotationDetails.findAllByInvquotationAndIsactive(invQuotation,true)//dont add is-active
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive:true)
        [invQuotationDetailsList:invQuotationDetailsList,invMaterialList:invMaterialList,invQuotationId:params.QuotationId]

    }

    //Reciept Flow
    def generateReciept(){
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        println("generateReciept params : " + params)
        //def academicYearList = AcademicYear.findAllByIsactive(true).sort{it.ay}
        def academicYearList = AcademicYear.findAllByIsactive(true).sort{it.ay}
        def invVendorList = InvVendor.findAllWhere(isactive: true,organization: org)
        def paymentMethodList = InvPaymentMethod.findAllWhere(isactive: true,organization: org)
        [academicYearList:academicYearList,invVendorList:invVendorList,paymentMethodList:paymentMethodList]
    }
    def getPOValuesByVendor(){
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        println("getPOValuesByVendor params--->" + params)
        InvVendor invVendor = InvVendor.findByCompany_name(params.vendor)
        def invPOList = InvPurchaseOrder.findByInvvendorAndOrganization(invVendor,org)
        [invPOList:invPOList]
    }
    def getInvoiceByPO(){
        //println"getInvoiceByPO-->"+params
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        println("getInvoiceByPO params--->" + params)
        def invPO = InvPurchaseOrder.findByPon(params.PONO)
        def invoiceList = Invoice.findAllByInvpurchaseorderAndOrganizationAndIsapproved(invPO,org,true)
        [invoiceList:invoiceList]
    }
    def getValuesByPO(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        println"getValuesByPO params--->"+params
        InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findByPon(params.PONO)
        def purpose = invPurchaseOrder?.purpose
        def pon = invPurchaseOrder?.pon
        def purchase_order_date = invPurchaseOrder?.purchase_order_date
        def total = invPurchaseOrder?.total
        [purpose:purpose,pon:pon,purchase_order_date:purchase_order_date,total:total]
    }
    def getValuesByInvoice(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        println"getValuesByInvoice params--->"+params
        Invoice invoice = Invoice.findByInvoice_number(params.invoice)
        def amount = invoice?.amount
        def total = invoice?.total
        def tax = invoice?.tax
        def date = invoice?.invoice_date
        [amount:amount,tax:tax,date:date,total:total]
    }
    def saveReciept(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        println"saveReciept params--->"+params
        def invoice = Invoice.findByInvoice_number(params.invoice)
        def invReciept = InvReceipt.findByInvoiceAndOrganization(invoice,org)
        if(invReciept == null){
            invReciept = new InvReceipt()
            def PRPO = PRPOTracking.findByOrganization(org)
            invReciept.receipt_no = PRPO?.recieptNo
            PRPO?.recieptNo++
            invReciept.receipt_date = params.recieptDate
            invReciept.amount = java.lang.Double.parseDouble(params.recieptAmt)

            invReciept.account_number = params.bankAccNo
            invReciept.bank_name = params.bankName
            invReciept.bank_branch = params.bankAdd
            invReciept.ifsc_code = params.IFSC
            invReciept.transaction_number = params.recieptTransId
            invReciept.transaction_number = params.recieptTransId
            invReciept.isactive = true
            invReciept.remark = params.remark

            invReciept.organization = org
            def invPO = InvPurchaseOrder.findByPon(params.PON)
            //println"invPO----->"+invPO
            def invVendor = invPO?.invvendor
            //println"invVendor----->"+invVendor
            /*
            def invQuotation=InvQuotation.findByInvvendorAndIsapprovedAndIsactive(invPO?.invvendor,true,true)
            println"invQuotation----->"+invQuotation
            def invMat = InvMaterial.findByIdAndOrganization("1",org)
            println"invMat----->"+invMat            */

            println"invoice----->"+invoice
            def paymentMethod = InvPaymentMethod.findByName(params.paymentMethod)
            //println"paymentMethod----->"+paymentMethod
            /*
            invReciept.invquotation = invQuotation
            invReciept.invmaterial = invMat
             */
            invReciept.invoice = invoice
            invReciept.invpurchaseorder = invPO
            invReciept.invvendor = invVendor

            invReciept.invpaymentmethod = paymentMethod
            invReciept.paymentby = instructor

            invReciept.creation_username = instructor.uid
            invReciept.updation_username = instructor.uid
            invReciept.creation_date = new Date()
            invReciept.updation_date = new Date()
            invReciept.updation_ip_address = request.getRemoteAddr()
            invReciept.creation_ip_address = request.getRemoteAddr()
            invReciept.save(flush: true,failOnError: true)
            flash.message = "Reciept Succesfully Added"

        }else{
            flash.error = "Reciept With Invoice Number:" + invoice?.invoice_number + " already generated"
        }
        redirect(action: 'generateReciept')
    }
    def fetchAllRecieptByPO(){
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        println("fetchAllRecieptByPO-->" + params)
        def recieptList = InvReceipt.findAllWhere(organization: org)
        [recieptList:recieptList]
    }
    def viewInvoiceDetailsByReciept(){
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        println("viewInvoiceDetailsByReciept-->" + params)
        def invoice= Invoice.findById(params.invoiceId)
        def invoiceDetailsList = InvoiceDetails.findAllWhere(invoice: invoice,isactive: true)
        [invoiceDetailsList:invoiceDetailsList]
    }

    //Reciept Reports
    def viewReciepts(){
        println(" i am viewPO Accounts Dept")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def recieptList = InvReceipt.findAllWhere(organization: org,isactive: true)
        [recieptList:recieptList]
    }
    def viewInvoiceByReciept(){
        println(" i am viewSinglePRDetails"+params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invReciept = InvReceipt.findById(params.recieptId)
        def invoiceList = Invoice.findById(invReciept?.invoice?.id)
        //println"invoice-->"+invoice
        [invoiceList:invoiceList]
    }
    def viewInvoiceDetailsByInvoice(){
        println"viewInvoiceDetailsByInvoice-->"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invoice = Invoice.findById(params.invoiceId)
        //println"invoice-->"+invoice
        def invoiceDetailsList = InvoiceDetails.findAllByInvoice(invoice)
        //println"invoiceDetails-->"+invoiceDetailsList
        [invoiceDetailsList:invoiceDetailsList]
    }

    //Invoice Reports
    def viewInvoice(){
        println(" i am viewInvoice Accounts Dept")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def vendorList = InvVendor.findAllWhere(organization: org,isactive: true)
        [vendorList:vendorList]
    }
    def getAllInvoiceByVendor(){
        println"getAllInvoiceByVendor params-->"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invVendor = InvVendor.findById(params.vendor)
        def invoiceList = Invoice.findAllWhere(organization: org,invvendor: invVendor)
        [invoiceList:invoiceList]
    }

    //Quotation Reports
    def viewQuotations(){
        println(" i am viewInvoice Accounts Dept")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def vendorList = InvVendor.findAllWhere(organization: org,isactive: true)
        [vendorList:vendorList]
    }
    def getAllQuotationsByVendor(){
        println"getAllQuotationsByVendor params-->"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invVendor = InvVendor.findById(params.vendor)
        def quotationList = InvQuotation.findAllWhere(organization: org,invvendor: invVendor,isapproved:true)
        [quotationList:quotationList]
    }
    def viewQuotationDetailsByQuotation(){
        println"viewQuotationDetailsByQuotation-->"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invQuotation = InvQuotation.findById(params.quotationId)
        println"invQuotation-->"+invQuotation
        def quotationDetailsList = InvQuotationDetails.findAllByInvquotation(invQuotation)
        println"quotationDetailsList-->"+quotationDetailsList
        [quotationDetailsList:quotationDetailsList]
    }

}
