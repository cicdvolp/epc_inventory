package volp

import javax.mail.Message
import javax.mail.PasswordAuthentication
import javax.mail.Transport
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

class EntranceAdmissionLoginController {

    def easignup(){
        println"in easingup"//landing page
    }

    //to send email OTP
    def verifyemail(){
        println("in verifyemail:"+params)
        String email=params.email.trim()
        def org = Organization.findById("4")

        session.otpemail = email
        if(email!=null)
        {
            java.util.Random random=new java.util.Random()
            String otp=""
            int min=0,max=0,n
            for(int i=1;i<=6;i++)
            {
                if(i==1)
                    min=1
                else
                    min=0
                max=9
                n=random.nextInt(max)+min
                otp=otp+n
            }
            Otp otpobj=Otp.findByEmail(email)
            Date date=new java.util.Date()
            if(otpobj==null)
            {
                //insert otp
                otpobj=new Otp()
                otpobj.email=email
                otpobj.otp=otp
                otpobj.otpgenerationtime=date
                otpobj.save(failOnError:true,flush:true)
                println"otp"+otp
            }
            else
            {
                //update otp
                otpobj.otp=otp
                otpobj.otpgenerationtime=date
                otpobj.save(failOnError:true,flush:true)
            }
            try {
                String username
                String password
                if(org?.establishment_email && org.establishment_email_credentials){
                    username = org?.establishment_email
                    password = org.establishment_email_credentials
                }
                String sendto = email
                String subject = "Verify Your Email for VOLP"
                String body = "Your OTP for registration is "+otp+"\n"+"Thanks, VOLP Team"

                Properties props = new Properties();
                props.put("mail.smtp.auth", "true");
                props.put("mail.smtp.starttls.enable", "true");
                props.put("mail.smtp.host", "smtp.gmail.com");
                props.put("mail.smtp.port", "587");

                javax.mail.Session session = javax.mail.Session.getInstance(props,
                        new javax.mail.Authenticator() {
                            protected PasswordAuthentication getPasswordAuthentication() {
                                return new PasswordAuthentication(username, password);
                            }
                        });

                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress(username));
                message.setRecipients(Message.RecipientType.TO,
                        InternetAddress.parse(sendto));
                message.setSubject(subject);
                message.setText(body);

                Transport.send(message);
                System.out.println("Mail Sent Successfully.....");

                println("Now sending mail of otp...."+otp)
                redirect(controller:"EntranceAdmissionLogin", action:"processemail",params:params)

                return
            } catch (Exception e){
                println("Error :: " + e)
                render "<script>\n" +
                        "     alert(\"Email Sending Failed.\")\n" +
                        "history.back();\n"+
                        "  </script>"
                return
            }
        }
        else
        {
            render "<script>\n" +
                    "     alert(\"Please register or check your username.\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
        }
    }

    def processemail(){
        println("in processemail"+params)
        [email:params.email]
    }

    def verifyotp(){
        println("in verifyotp:"+params)
//        println("in verifyotp:"+session)
        String email=session.otpemail
        String otp=params.otp
        println("Email:"+email+" and otp:"+otp)
        Otp otpobj=Otp.findByEmailAndOtp(email,otp)
        if(otpobj!=null)
        {
            Date otpgenerationtime=otpobj.otpgenerationtime
            Date currenttime=new java.util.Date()
            long diff = currenttime.getTime() - otpgenerationtime.getTime() ;
            long diffInMinutes = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(diff);
            if(diffInMinutes<=30)
            {
                redirect (action:"registereastudent",params:params)
                return
            }
            else
            {
                render "<script>\n" +
                        "     alert(\"OTP is expired..please generate OTP again..\")\n" +
                        "history.back();\n"+
                        "  </script>"
                return
            }
        }
        else
        {
            render "<script>\n" +
                    "     alert(\"OTP do not match, Please Enter OTP again...\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
        }
    }

    def registereastudent(){
        println"registereastudent-->"+ params
        [email:params.email,otp:params.otp]
    }

    def saveeastudent(){
        println"saveeastudent"+params
        if(params.fname == null && params.lname == null){
            println"Please fill all details"
            render "<script>\n" +
                    "     alert(\"Please fill all details...\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
            //redirect (action:"registereastudent",params:params)
        }else{
            if(params.pass != params.cpass){
                render "<script>\n" +
                        "     alert(\"Password Do Not Match...\")\n" +
                        "history.back();\n"+
                        "  </script>"
                return
                //redirect (action:"registereastudent",params:params)
            }else{
                def EA = EntranceApplicant.findByEmail(params.email)
                if(EA == null)
                {
                    EA = new EntranceApplicant()
                    EA.email = params.email.trim()
                    EA.password = params.pass.trim()
                    EA.fullname = params.fname.trim()
                    EA.mobilenumber = params.mbno

                    EA.email_otp_generation_date = new java.util.Date()
                    EA.email_otp = params.otp

                    EA.creation_username = params.email
                    EA.updation_username = params.email
                    EA.creation_date = new java.util.Date()
                    EA.updation_date = new java.util.Date()
                    EA.creation_ip_address = "0.0.0.1"
                    EA.updation_ip_address = "0.0.0.1"
                    EA.save(flush: true,failOnError: true)
                    redirect (controller: "EntranceAdmissionLogin", action: "login")
                }else{
                   println"Email already registered in system,please use different mail"
                    render "<script>\n" +
                            "     alert(\"Email Already Registered..\")\n" +
                            "history.back();\n"+
                            "  </script>"
                    return
                    //flash.error="Email Already Registered"
                }
            }
        }
    }

    def login() {
        println("I am in Entrance Admission Login - " + params)
        // SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        // sendMailService.sendmail("volp.vu@gmail.com","volp@108","deepak.pawar@vit.edu","Your Leave Application dated:" + formatter.format(new java.util.Date()) + " is Sanctioned..","Your Leave Application Dated:" + new java.util.Date() + " is Sanctioned..\n\n Details are as below:" + "\n\n Leave Type:" + "xyz" + "\n\n From Date:" + new java.util.Date() + " \n To Date:" + new java.util.Date() +"\n\nNumber of Days:"+"3"+ " \n\n Thanks","")
        // test()
        def loginlogo = Loginlogo.list()[0]
        def image
        def text_color = "#F39C12"
        def button_color = "#F39C12"
        if(loginlogo) {
            image = loginlogo?.logo_path + loginlogo?.logo_name
            text_color = loginlogo?.text_color
            button_color = loginlogo?.button_color
        }

        AWSFolderPath awsFolderPath = AWSFolderPath.findById(7)
        String url = awsFolderPath.path
        println("url :: " + url)

        if (params.logout) {
            session.loginid = null
            println("session Invalidated...")
            session.invalidate()
            redirect(controller: "EntranceAdmissionLogin", action: "login")
        } else if (params.username) {
            [emp_code: params.username, loginlogo:image, text_color : text_color, button_color : button_color, url:url]
        } else {
            if(params.error){
                def error = flash.loginerror
                flash.loginerror = error
                [test : 'test', loginlogo:image, text_color : text_color, button_color : button_color, url:url]
            }else if (session.loginid == null) {
                session.invalidate()
                [loginlogo:image, text_color : text_color, button_color : button_color, url:url]
            } else {
                println("session.loginid is not null" + session.loginid)
                redirect(controller: "EntranceAdmissionLogin", action: "login")
            }
        }
    }

    def processerplogin() {
        println("I am in Processer Entrance Admission Login...123"+params)
        session.profilephoto = null
        session.isphotopresent = false
        session.email = params.username
        ////println("I am in processerplogin..."+session)
        String email=params.username.replaceAll(" ", "")
        String password=params.password
        Organization organization = null
        println(email+"::"+password)
        EntranceApplicant login = EntranceApplicant.findByEmailAndPasswordAndIsblocked(email,password,false)
        println("login >>"+ login)

        if(login==null) {
            EntranceApplicant blocked = EntranceApplicant.findByEmailAndPasswordAndIsblocked(email,password,true)
            println"blocked?-->"+blocked
            if(blocked){
                flash.loginerror = "Your Login Is Blocked..!!!"
            } else {
                flash.error = "You Have Entered Invalid Username Or Password...!"
            }
            redirect(controller: 'EntranceAdmissionLogin', action: 'login', params : [error : true])
            return
        }else {
            session.loginid = login?.id
            session.fullname = login?.fullname
            session.email = login?.email
//            session.profilephoto = ""
            redirect(controller: 'entranceApplicant', action: 'eadashboard', params : [error : false])
            return
        }

    }

    //forgot passowrd
    def forgetPassword(){
        println("in forgetPassword in EntranceAdmissionLogin")
        //LoginImages logimg = LoginImages.findByName("Reset")
        //String logimglnk = null
        //def path = DomainPath.list().path
        //if(logimg!=null) {
        //logimglnk = path[0] + logimg.imgpath + logimg.imgname
        // logimglnk = logimglnk.replace("\\", "/")
        //}
        // [logimglnk:logimglnk]
    }

    def verifyemailFP(){
        println("in verifyemailFP:"+params)
        String email=params.email.trim()
        def org = Organization.findById("4")
        def EAStudent = EntranceApplicant.findByEmail(email)
        if(EAStudent){
            session.otpemail = email
            if(email!=null)
            {
                java.util.Random random=new java.util.Random()
                String otp=""
                int min=0,max=0,n
                for(int i=1;i<=6;i++)
                {
                    if(i==1)
                        min=1
                    else
                        min=0
                    max=9
                    n=random.nextInt(max)+min
                    otp=otp+n
                }
                Otp otpobj=Otp.findByEmail(email)
                Date date=new java.util.Date()
                if(otpobj==null)
                {
                    //insert otp
                    otpobj=new Otp()
                    otpobj.email=email
                    otpobj.otp=otp
                    otpobj.otpgenerationtime=date
                    otpobj.save(failOnError:true,flush:true)
                    println"otp"+otp
                }
                else
                {
                    //update otp
                    otpobj.otp=otp
                    otpobj.otpgenerationtime=date
                    otpobj.save(failOnError:true,flush:true)
                }
                try {
                    String username
                    String password
                    if(org?.establishment_email && org.establishment_email_credentials){
                        username = org?.establishment_email
                        password = org.establishment_email_credentials
                    }
                    String sendto = email
                    String subject = "Verify Your Email for VOLP"
                    String body = "Your OTP for registration is "+otp+"\n"+"Thanks, VOLP Team"

                    Properties props = new Properties();
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.starttls.enable", "true");
                    props.put("mail.smtp.host", "smtp.gmail.com");
                    props.put("mail.smtp.port", "587");

                    javax.mail.Session session = javax.mail.Session.getInstance(props,
                            new javax.mail.Authenticator() {
                                protected PasswordAuthentication getPasswordAuthentication() {
                                    return new PasswordAuthentication(username, password);
                                }
                            });

                    Message message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(username));
                    message.setRecipients(Message.RecipientType.TO,
                            InternetAddress.parse(sendto));
                    message.setSubject(subject);
                    message.setText(body);

                    Transport.send(message);
                    System.out.println("Mail Sent Successfully.....");

                    println("Now sending mail of otp...."+otp)
                    redirect(controller:"EntranceAdmissionLogin", action:"processemailFP",params:params)

                    return
                } catch (Exception e){
                    println("Error :: " + e)
                    render "<script>\n" +
                            "     alert(\"Email Sending Failed.\")\n" +
                            "history.back();\n"+
                            "  </script>"
                    return
                }
            }
            else
            {
                render "<script>\n" +
                        "     alert(\"Please register or check your username.\")\n" +
                        "history.back();\n"+
                        "  </script>"
                return
            }
        }else{
            render "<script>\n" +
                    "     alert(\"Email Not Found,Please Register...\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
        }

    }

    def processemailFP(){
        println("in processemailFP"+params)
        [email:params.email]
    }

    def verifyotpFP(){
        println("in verifyotpFP:"+params)
//        println("in verifyotp:"+session)
        String email=session.otpemail
        String otp=params.otp
        println("Email:"+email+" and otp:"+otp)
        Otp otpobj=Otp.findByEmailAndOtp(email,otp)
        if(otpobj!=null)
        {
            Date otpgenerationtime=otpobj.otpgenerationtime
            Date currenttime=new java.util.Date()
            long diff = currenttime.getTime() - otpgenerationtime.getTime() ;
            long diffInMinutes = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(diff);
            if(diffInMinutes<=30)
            {
                redirect (action:"resetPassword",params:params)
                return
            }
            else
            {
                render "<script>\n" +
                        "     alert(\"OTP is expired..please generate OTP again..\")\n" +
                        "history.back();\n"+
                        "  </script>"
                return
            }
        }
        else
        {
            render "<script>\n" +
                    "     alert(\"OTP do not match, Please Enter OTP again...\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
        }
    }

    def resetPassword(){
        println"in resetpassword-->"+params
        [email: params.email,otp:params.otp]
    }

    def updatePassword(){
        println"updatePassword"+params
        if(params.email == null){
            println"Please fill all details"
            render "<script>\n" +
                    "     alert(\"Please fill all details...\")\n" +
                    "history.back();\n"+
                    "  </script>"
            return
            //redirect (action:"resetPassword",params:params)
        }else{
            if(params.pass != params.cpass){
                println"Passwords do not match"
                render "<script>\n" +
                        "     alert(\"Password Do Not Match...\")\n" +
                        "history.back();\n"+
                        "  </script>"
                return
                //redirect (action:"resetPassword",params:params)
            }else{
                def EA = EntranceApplicant.findByEmail(params.email)
                def EAOldPass = EA?.password
                if(params.pass == EAOldPass){
                    render "<script>\n" +
                            "     alert(\"New Password cannot be same as old password...\")\n" +
                            "history.back();\n"+
                            "  </script>"
                    return
                }else{
                    if(EA != null)
                    {
                        EA.password = params.pass.trim()
                        EA.updation_username = params.email
                        EA.updation_date = new java.util.Date()
                        EA.updation_ip_address = "0.0.0.1"
                        EA.save(flush: true,failOnError: true)
                        redirect (action:"login",controller: "EntranceAdmissionLogin")
                    }else{
                        render "<script>\n" +
                                "     alert(\"Email Not Registered,Please contact Support...\")\n" +
                                "history.back();\n"+
                                "  </script>"
                        return
                    }
                }
            }
        }
    }

    // logout
    def logout() {
        println("logout :: " + params)
        session.invalidate()
        redirect(controller: "entranceAdmissionLogin", action:"login", params : [error : false])
        return
    }
}
