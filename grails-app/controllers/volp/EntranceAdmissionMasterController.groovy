package volp

class EntranceAdmissionMasterController {

    def index() { }

    //master-Exemption
    def addExemption() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addExemption : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def exemptionList = Exemption.findAllByOrganization(org)
            println("exemptionList : "+exemptionList)
            [exemptionList:exemptionList]
        }
    }
    def saveExemption() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveExemption : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if(!params.exemptionName) {
                println("params.exemptionName "+params.exemptionName)
                flash.error = "Please add a Exemption Name ..."
                redirect(action: 'addExemption')
            }
            else{
                Exemption exemption = Exemption.findByName(params.exemptionName)
                if(exemption==null)
                {
                    exemption=new Exemption()
                    exemption.name = params.exemptionName
                    if(params.isactive == 'on') {
                        exemption.isactive = true
                    } else {
                        exemption.isactive = false
                    }
                    exemption.organization = org
                    exemption.creation_username = instructor.uid
                    exemption.updation_username = instructor.uid
                    exemption.updation_ip_address = request.getRemoteAddr()
                    exemption.creation_ip_address = request.getRemoteAddr()
                    exemption.updation_date = new java.util.Date()
                    exemption.creation_date = new java.util.Date()
                    exemption.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addExemption')
                }else {

                    flash.error = "Exemption already Exists..."
                    redirect(action:'addExemption')
                }
            }

        }

    }
    def editExemption(){
        println(" i am editExemption :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        Exemption exemption1  = Exemption.findByName(params.exemptionName)
        Exemption exemption  = Exemption.findById(params.exemptionId)
        if (exemption1 == null) {
            if(params.isactive=='on')
            {
                exemption.isactive=true
            }
            else
            {
                exemption.isactive=false
            }
            exemption.name = params.exemptionName
            exemption.organization = org
            exemption.creation_username = login.username
            exemption.updation_username = login.username
            exemption.updation_ip_address = request.getRemoteAddr()
            exemption.creation_ip_address = request.getRemoteAddr()
            exemption.updation_date = new java.util.Date()
            exemption.creation_date = new java.util.Date()
            exemption.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addExemption')
        } else {
            flash.error = "Exemption Already Exist..."
            redirect(action: 'addExemption')
        }
    }
    def activateExemption() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateExemption :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            Exemption exemption = Exemption.findById(params.exemptionId)
            if(exemption) {
                if(exemption.isactive) {
                    exemption.isactive = false
                    exemption.updation_username = login.username
                    exemption.updation_date = new java.util.Date()
                    exemption.updation_ip_address = request.getRemoteAddr()
                    exemption.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    exemption.isactive = true
                    exemption.updation_username = login.username
                    exemption.updation_date = new java.util.Date()
                    exemption.updation_ip_address = request.getRemoteAddr()
                    exemption.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Exemption Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addExemption")
            return
        }
    }
    def deleteExemption(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteExemption:: " + params)
            Exemption exemption = Exemption.findById(params.exemptionId)
            if (exemption) {
                try {
                    exemption.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Exemption Cannot be Deleted .."
                }
            } else {
                flash.error = "Exemption  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addExemption")
            return
        }
    }

    //master-ExemptionType
    def addExemptionType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addExemptionType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def exemptiontypeList = ExemptionType.findAllByOrganization(org)
            println("exemptiontypeList : "+exemptiontypeList)
            def programTypeList = ProgramType.findAllByOrganization(org)
            println("programTypeList : "+programTypeList)
            [exemptiontypeList:exemptiontypeList,programTypeList:programTypeList]
        }
    }
    def saveExemptionType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveExemptionType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if(params.exemptionTypeName == 'null' || params.programType == 'null') {
                flash.error = "Select all mandatory fileds...."
                redirect(action: 'addExemptionType')
            }

            def split_str = params.abbr.split(" ")
            println("split_str :: " + split_str)
            def abbr = ""
            if(split_str.size() != 0) {
                if (split_str.size() == 1) {
                    abbr = abbr + split_str[0]
                    println("abbr 0 :: " + abbr)
                } else {
                    for (abr in split_str) {
                        if(abbr.equals("")) {
                            abbr = abr + abbr
                        } else {
                            abbr = abbr + "_" + abr
                        }
                        println("abbr : " + abbr)
                    }
                    println("abbr 1 :: " + abbr)
                }
            }
            println("abbr 2 :: " + abbr)

            ProgramType programtype = ProgramType.findByName(params.programType)
            ExemptionType exemptionType = ExemptionType.findByNameAndProgramtype(params.exemptionTypeName,programtype)
            println("exemptionType : "+exemptionType)
            if(exemptionType ==null) {
                exemptionType=new ExemptionType()
                exemptionType.name = params.exemptionTypeName
                exemptionType.programtype = programtype
                if(params.isactive == 'on') {
                    exemptionType.isactive = true
                } else {
                    exemptionType.isactive = false
                }
                exemptionType.organization = org
                exemptionType.abbr = abbr
                exemptionType.creation_username = instructor.uid
                exemptionType.updation_username = instructor.uid
                exemptionType.updation_ip_address = request.getRemoteAddr()
                exemptionType.creation_ip_address = request.getRemoteAddr()
                exemptionType.updation_date = new java.util.Date()
                exemptionType.creation_date = new java.util.Date()
                exemptionType.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addExemptionType')
            }else {
                exemptionType.name = params.exemptionTypeName
                exemptionType.programtype = programtype
                if(params.isactive == 'on') {
                    exemptionType.isactive = true
                } else {
                    exemptionType.isactive = false
                }
                exemptionType.abbr = abbr
                exemptionType.updation_username = instructor.uid
                exemptionType.updation_ip_address = request.getRemoteAddr()
                exemptionType.updation_date = new java.util.Date()
                exemptionType.save(flush: true, failOnError: true)
                flash.message = "Updated Successfully"
                redirect(action: 'addExemptionType')
            }
        }

    }
    def editExemptionType(){
        println(" in editExemptionType :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        if(params.exemptionTypeName == 'null' || params.programType == 'null') {
            flash.error = "Please add a Exemption type...."
        }

        def split_str = params.abbr.split(" ")
        println("split_str :: " + split_str)
        def abbr = ""
        if(split_str.size() != 0) {
            if (split_str.size() == 1) {
                abbr = abbr + split_str[0]
                println("abbr 0 :: " + abbr)
            } else {
                for (abr in split_str) {
                    if(abbr.equals("")) {
                        abbr = abr + abbr
                    } else {
                        abbr = abbr + "_" + abr
                    }
                    println("abbr : " + abbr)
                }
                println("abbr 1 :: " + abbr)
            }
        }
        println("abbr 2 :: " + abbr)

        ProgramType programtype = ProgramType.findByName(params.programType)
        ExemptionType exemptionType1 = ExemptionType.findByNameAndProgramtype(params.exemptionTypeName,programtype)
        println("exemptionType1 : "+exemptionType1)
        ExemptionType exemptionType  = ExemptionType.findById(params.exemptionTypeId)

        if (exemptionType1 == null) {
            if(params.isactive=='on')
            {
                exemptionType.isactive=true
            }
            else
            {
                exemptionType.isactive=false
            }
            exemptionType.name = params.exemptionTypeName
            exemptionType.programtype = programtype
            exemptionType.organization = org
            exemptionType.creation_username = login.username
            exemptionType.updation_username = login.username
            exemptionType.updation_ip_address = request.getRemoteAddr()
            exemptionType.creation_ip_address = request.getRemoteAddr()
            exemptionType.updation_date = new java.util.Date()
            exemptionType.creation_date = new java.util.Date()
            exemptionType.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addExemptionType')
        } else {
            exemptionType.name = params.exemptionTypeName
            exemptionType.programtype = programtype
            if(params.isactive == 'on') {
                exemptionType.isactive = true
            } else {
                exemptionType.isactive = false
            }
            exemptionType.abbr = abbr
            exemptionType.updation_username = instructor.uid
            exemptionType.updation_ip_address = request.getRemoteAddr()
            exemptionType.updation_date = new java.util.Date()
            exemptionType.save(flush: true, failOnError: true)
            flash.message = "Updated Successfully"
            redirect(action: 'addExemptionType')
        }


    }
    def activateExemptionType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateExemptionTYpe :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            ExemptionType exemptionType = ExemptionType.findById(params.exemptionTypeId)
            if(exemptionType) {
                if(exemptionType.isactive) {
                    exemptionType.isactive = false
                    exemptionType.updation_username = login.username
                    exemptionType.updation_date = new java.util.Date()
                    exemptionType.updation_ip_address = request.getRemoteAddr()
                    exemptionType.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    exemptionType.isactive = true
                    exemptionType.updation_username = login.username
                    exemptionType.updation_date = new java.util.Date()
                    exemptionType.updation_ip_address = request.getRemoteAddr()
                    exemptionType.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "ExemptionType Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addExemptionType")
            return
        }
    }
    def deleteExemptionType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteExemptionType:: " + params)
            ExemptionType exemptionType = ExemptionType.findById(params.exemptionTypeId)
            if (exemptionType) {
                try {
                    exemptionType.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "ExemptionType Cannot be Deleted .."
                }
            } else {
                flash.error = "ExemptionType  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addExemptionType")
            return
        }
    }

    //EntanceCategoryType
    def addEntranceCategoryType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceCategoryType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceCategoryType_list = EntranceCategoryType.findAll()
            println("entranceCategoryType_list : "+entranceCategoryType_list)
            [entranceCategoryType_list:entranceCategoryType_list]
        }
    }
    def saveEntranceCategoryType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceCategoryType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if(!params.entranceCategoryType) {
                println("params.entranceCategoryType "+params.entranceCategoryType)
                flash.error = "Please add entrance Category Type Name ..."
                redirect(action: 'addEntranceCategoryType')
            }
            else{
                EntranceCategoryType entranceCategoryType = EntranceCategoryType.findByName(params.entranceCategoryType)
                if(entranceCategoryType==null)
                {
                    entranceCategoryType=new EntranceCategoryType()
                    entranceCategoryType.name = params.entranceCategoryType
                    if(params.isactive == 'on') {
                        entranceCategoryType.isactive = true
                    } else {
                        entranceCategoryType.isactive = false
                    }
                    entranceCategoryType.organization = org
                    entranceCategoryType.creation_username = instructor.uid
                    entranceCategoryType.updation_username = instructor.uid
                    entranceCategoryType.updation_ip_address = request.getRemoteAddr()
                    entranceCategoryType.creation_ip_address = request.getRemoteAddr()
                    entranceCategoryType.updation_date = new java.util.Date()
                    entranceCategoryType.creation_date = new java.util.Date()
                    entranceCategoryType.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEntranceCategoryType')
                }else {

                    flash.error = "Entrance Category Type already Exists..."
                    redirect(action:'addEntranceCategoryType')
                }
            }

        }

    }
    def editEntranceCategoryType(){
        println(" i am editEntranceCategoryType :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        EntranceCategoryType entranceCategoryType1  = EntranceCategoryType.findByName(params.entranceCategoryType)
        EntranceCategoryType entranceCategoryType  = EntranceCategoryType.findById(params.entranceCategoryTypeId)
        if (entranceCategoryType1 == null) {
            if(params.isactive=='on')
            {
                entranceCategoryType.isactive=true
            }
            else
            {
                entranceCategoryType.isactive=false
            }
            entranceCategoryType.name = params.entranceCategoryType
            entranceCategoryType.organization = org
            entranceCategoryType.creation_username = login.username
            entranceCategoryType.updation_username = login.username
            entranceCategoryType.updation_ip_address = request.getRemoteAddr()
            entranceCategoryType.creation_ip_address = request.getRemoteAddr()
            entranceCategoryType.updation_date = new java.util.Date()
            entranceCategoryType.creation_date = new java.util.Date()
            entranceCategoryType.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addEntranceCategoryType')
        } else {
            flash.error = "Entrance Category Type Already Exist..."
            redirect(action: 'addEntranceCategoryType')
        }
    }
    def activateEntranceCategoryType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEntranceCategoryType :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceCategoryType entranceCategoryType  = EntranceCategoryType.findById(params.entranceCategoryTypeId)
            if(entranceCategoryType) {
                if(entranceCategoryType.isactive) {
                    entranceCategoryType.isactive = false
                    entranceCategoryType.updation_username = login.username
                    entranceCategoryType.updation_date = new java.util.Date()
                    entranceCategoryType.updation_ip_address = request.getRemoteAddr()
                    entranceCategoryType.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceCategoryType.isactive = true
                    entranceCategoryType.updation_username = login.username
                    entranceCategoryType.updation_date = new java.util.Date()
                    entranceCategoryType.updation_ip_address = request.getRemoteAddr()
                    entranceCategoryType.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "entranceCategoryType Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceCategoryType")
            return
        }
    }
    def deleteEntranceCategoryType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceCategoryType:: " + params)
            EntranceCategoryType entranceCategoryType  = EntranceCategoryType.findById(params.entranceCategoryTypeId)
            if (entranceCategoryType) {
                try {
                    entranceCategoryType.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceCategoryType")
            return
        }
    }

    // EntranceCategory
    def addEntranceCategory(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceCategory : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceCategory_list = EntranceCategory.findAll()
            def entranceCategoryType_list = EntranceCategoryType.findAll()
            [entranceCategory_list:entranceCategory_list,entranceCategoryType_list:entranceCategoryType_list]
        }
    }
    def saveEntranceCategory(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceCategory : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if(params.entranceCategoryName == 'null' || params.entranceCategoryType == 'null') {
                flash.error = "Please add fill all fields.."
            }

            EntranceCategoryType entranceCategoryType = EntranceCategoryType.findByName(params.entranceCategoryType)
            EntranceCategory entranceCategory = EntranceCategory.findByNameAndEntrancecategorytype(params.entranceCategoryName,entranceCategoryType)
            if(entranceCategory ==null)
            {
                entranceCategory=new EntranceCategory()
                entranceCategory.name = params.entranceCategoryName
                entranceCategory.entrancecategorytype = entranceCategoryType
                if(params.isactive == 'on') {
                    entranceCategory.isactive = true
                } else {
                    entranceCategory.isactive = false
                }
                entranceCategory.organization = org
                entranceCategory.creation_username = instructor.uid
                entranceCategory.updation_username = instructor.uid
                entranceCategory.updation_ip_address = request.getRemoteAddr()
                entranceCategory.creation_ip_address = request.getRemoteAddr()
                entranceCategory.updation_date = new java.util.Date()
                entranceCategory.creation_date = new java.util.Date()
                entranceCategory.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addEntranceCategory')
            }else {

                flash.error = "Entrance Category already Exists..."
                redirect(action:'addEntranceCategory')
            }
        }

    }
    def editEntranceCategory(){
        println(" i am editEntranceCategory :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        EntranceCategoryType entranceCategoryType = EntranceCategoryType.findByName(params.entranceCategoryType)
        EntranceCategory entranceCategory1  = EntranceCategory.findByNameAndEntrancecategorytype(params.entranceCategoryName,entranceCategoryType)
        EntranceCategory entranceCategory = EntranceCategory.findById(params.entranceCategoryId)
        if (entranceCategory1 == null) {
            if(params.isactive=='on')
            {
                entranceCategory.isactive=true
            }
            else
            {
                entranceCategory.isactive=false
            }
            entranceCategory.name = params.entranceCategoryName
            entranceCategory.entrancecategorytype= entranceCategoryType
            entranceCategory.organization = org
            entranceCategory.creation_username = login.username
            entranceCategory.updation_username = login.username
            entranceCategory.updation_ip_address = request.getRemoteAddr()
            entranceCategory.creation_ip_address = request.getRemoteAddr()
            entranceCategory.updation_date = new java.util.Date()
            entranceCategory.creation_date = new java.util.Date()
            entranceCategory.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addEntranceCategory')
        } else {
            flash.error = "Entrance Category Already Exist..."
            redirect(action: 'addEntranceCategory')
        }
    }
    def activateEntranceCategory(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEntranceCategory :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceCategory entranceCategory = EntranceCategory.findById(params.entranceCategoryId)
            if(entranceCategory) {
                if(entranceCategory.isactive) {
                    entranceCategory.isactive = false
                    entranceCategory.updation_username = login.username
                    entranceCategory.updation_date = new java.util.Date()
                    entranceCategory.updation_ip_address = request.getRemoteAddr()
                    entranceCategory.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceCategory.isactive = true
                    entranceCategory.updation_username = login.username
                    entranceCategory.updation_date = new java.util.Date()
                    entranceCategory.updation_ip_address = request.getRemoteAddr()
                    entranceCategory.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Category Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceCategory")
            return
        }

    }
    def deleteEntranceCategory(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceCategory:: " + params)
            EntranceCategory entranceCategory = EntranceCategory.findById(params.entranceCategoryId)
            if (entranceCategory) {
                try {
                    entranceCategory.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceCategory")
            return
        }
    }

    // MaritalStatus
    def addMaritalStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addMaritalStatus : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def maritalStatus_list = MaritalStatus.findAll()
            [maritalStatus_list:maritalStatus_list]
        }
    }
    def saveMaritalStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveMaritalStatus : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if(!params.maritalStatusName) {
                println("params.maritalStatusName "+params.maritalStatusName)
                flash.error = "Please add Marital Status ..."
                redirect(action: 'addMaritalStatus')
            }
            else{
                MaritalStatus maritalStatus = MaritalStatus.findByName(params.maritalStatusName)
                if(maritalStatus==null)
                {
                    maritalStatus=new MaritalStatus()
                    maritalStatus.name = params.maritalStatusName
                    if(params.isactive == 'on') {
                        maritalStatus.isactive = true
                    } else {
                        maritalStatus.isactive = false
                    }
                    maritalStatus.organization = org
                    maritalStatus.creation_username = instructor.uid
                    maritalStatus.updation_username = instructor.uid
                    maritalStatus.updation_ip_address = request.getRemoteAddr()
                    maritalStatus.creation_ip_address = request.getRemoteAddr()
                    maritalStatus.updation_date = new java.util.Date()
                    maritalStatus.creation_date = new java.util.Date()
                    maritalStatus.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addMaritalStatus')
                }else {

                    flash.error = "Marital Status already Exists..."
                    redirect(action:'addMaritalStatus')
                }
            }

        }
    }
    def activateMaritalStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateMaritalStatus :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            MaritalStatus maritalStatus = MaritalStatus.findById(params.maritalStatusId)
            if(maritalStatus) {
                if(maritalStatus.isactive) {
                    maritalStatus.isactive = false
                    maritalStatus.updation_username = login.username
                    maritalStatus.updation_date = new java.util.Date()
                    maritalStatus.updation_ip_address = request.getRemoteAddr()
                    maritalStatus.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    maritalStatus.isactive = true
                    maritalStatus.updation_username = login.username
                    maritalStatus.updation_date = new java.util.Date()
                    maritalStatus.updation_ip_address = request.getRemoteAddr()
                    maritalStatus.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Marital Status Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addMaritalStatus")
            return
        }
    }
    def deleteMaritalStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteMaritalStatus:: " + params)
            MaritalStatus maritalStatus = MaritalStatus.findById(params.maritalStatusId)
            if (maritalStatus) {
                try {
                    maritalStatus.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addMaritalStatus")
            return
        }
    }
    def editMaritalStatus(){
        println(" i am editMaritalStatus :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        MaritalStatus maritalStatus1 = MaritalStatus.findByName(params.maritalStatusName)
        MaritalStatus maritalStatus = MaritalStatus.findById(params.maritalStatusId)
        if(!maritalStatus1){
            if(params.isactive=='on')
            {
                maritalStatus.isactive=true
            }
            else
            {
                maritalStatus.isactive=false
            }
            maritalStatus.name = params.maritalStatusName
            maritalStatus.organization = org
            maritalStatus.creation_username = login.username
            maritalStatus.updation_username = login.username
            maritalStatus.updation_ip_address = request.getRemoteAddr()
            maritalStatus.creation_ip_address = request.getRemoteAddr()
            maritalStatus.updation_date = new java.util.Date()
            maritalStatus.creation_date = new java.util.Date()
            maritalStatus.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addMaritalStatus')
        }else{
            flash.error = "Marital Status Already Exist..."
            redirect(action: 'addMaritalStatus')
        }

    }

    //Employment STatus
    def addEmploymentStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEmploymentStatus : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def employmentStatus_list = EmploymentStatus.findAll()
            println("employmentStatus_list : "+employmentStatus_list)
            [employmentStatus_list:employmentStatus_list]
        }
    }
    def saveEmploymentStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEmploymentStatus : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if(!params.employmentStatusname) {
                println("params.employmentStatusname "+params.employmentStatusname)
                flash.error = "Please add Employment Status ..."
                redirect(action: 'addEmploymentStatus')
            }
            else{
                EmploymentStatus employmentStatus = EmploymentStatus.findByName(params.employmentStatusname)
                if(employmentStatus==null)
                {
                    employmentStatus=new EmploymentStatus()
                    employmentStatus.name = params.employmentStatusname
                    if(params.isactive == 'on') {
                        employmentStatus.isactive = true
                    } else {
                        employmentStatus.isactive = false
                    }
                    employmentStatus.organization = org
                    employmentStatus.creation_username = instructor.uid
                    employmentStatus.updation_username = instructor.uid
                    employmentStatus.updation_ip_address = request.getRemoteAddr()
                    employmentStatus.creation_ip_address = request.getRemoteAddr()
                    employmentStatus.updation_date = new java.util.Date()
                    employmentStatus.creation_date = new java.util.Date()
                    employmentStatus.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEmploymentStatus')
                }else {
                    flash.error = "Employment Status already Exists..."
                    redirect(action:'addEmploymentStatus')
                }
            }

        }
    }
    def activateEmploymentStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEmploymentStatus :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EmploymentStatus employmentStatus = EmploymentStatus.findById(params.employmentStatusId)
            if(employmentStatus) {
                if(employmentStatus.isactive) {
                    employmentStatus.isactive = false
                    employmentStatus.updation_username = login.username
                    employmentStatus.updation_date = new java.util.Date()
                    employmentStatus.updation_ip_address = request.getRemoteAddr()
                    employmentStatus.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    employmentStatus.isactive = true
                    employmentStatus.updation_username = login.username
                    employmentStatus.updation_date = new java.util.Date()
                    employmentStatus.updation_ip_address = request.getRemoteAddr()
                    employmentStatus.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Employment Status Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEmploymentStatus")
            return
        }
    }
    def deleteEmploymentStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEmploymentStatus:: " + params)
            EmploymentStatus employmentStatus = EmploymentStatus.findById(params.employmentStatusId)
            if (employmentStatus) {
                try {
                    employmentStatus.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEmploymentStatus")
            return
        }
    }
    def editEmploymentStatus(){
        println(" i am editEmploymentStatus :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        EmploymentStatus employmentStatus1 = EmploymentStatus.findByName(params.employmentStatusname)
        EmploymentStatus employmentStatus = EmploymentStatus.findById(params.employmentStatusId)
        if(!employmentStatus1){
            if(params.isactive=='on')
            {employmentStatus.isactive=true}
            else {employmentStatus.isactive=false}
            employmentStatus.name = params.employmentStatusname
            employmentStatus.organization = org
            employmentStatus.creation_username = login.username
            employmentStatus.updation_username = login.username
            employmentStatus.updation_ip_address = request.getRemoteAddr()
            employmentStatus.creation_ip_address = request.getRemoteAddr()
            employmentStatus.updation_date = new java.util.Date()
            employmentStatus.creation_date = new java.util.Date()
            employmentStatus.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addEmploymentStatus')
        }else{
            flash.error = "Employment Status Already Exist..."
            redirect(action: 'addEmploymentStatus')
        }

    }

    //EntranceDegree
    def addEntranceDegree(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceDegree : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceDegree_list = EntranceDegree.findAll()
            println("entranceDegree_list : "+entranceDegree_list)
            [entranceDegree_list:entranceDegree_list]
        }
    }
    def saveEntranceDegree(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceDegree : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if(!params.entranceDegreeName) {
                println("params.entranceDegreeName "+params.entranceDegreeName)
                flash.error = "Please add Entrance Degree."
                redirect(action: 'addEntranceDegree')
            }
            else{
                EntranceDegree entranceDegree = EntranceDegree.findByName(params.entranceDegreeName)
                if(entranceDegree==null)
                {
                    entranceDegree=new EntranceDegree()
                    entranceDegree.name = params.entranceDegreeName
                    if(params.isactive == 'on') {
                        entranceDegree.isactive = true
                    } else {
                        entranceDegree.isactive = false
                    }
                    if(params.iscompulsory == 'on') {
                        entranceDegree.iscompulsory = true
                    } else {
                        entranceDegree.iscompulsory = false
                    }
                    entranceDegree.organization = org
                    entranceDegree.creation_username = instructor.uid
                    entranceDegree.updation_username = instructor.uid
                    entranceDegree.updation_ip_address = request.getRemoteAddr()
                    entranceDegree.creation_ip_address = request.getRemoteAddr()
                    entranceDegree.updation_date = new java.util.Date()
                    entranceDegree.creation_date = new java.util.Date()
                    entranceDegree.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEntranceDegree')
                }else {
                    flash.error = "Entrance Degree Already Exists..."
                    redirect(action:'addEntranceDegree')
                }
            }
        }
    }
    def activateEntranceDegree(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEntranceDegree :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceDegree entranceDegree = EntranceDegree.findById(params.entranceDegreeId)
            if(entranceDegree) {
                if(entranceDegree.isactive) {
                    entranceDegree.isactive = false
                    entranceDegree.updation_username = login.username
                    entranceDegree.updation_date = new java.util.Date()
                    entranceDegree.updation_ip_address = request.getRemoteAddr()
                    entranceDegree.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceDegree.isactive = true
                    entranceDegree.updation_username = login.username
                    entranceDegree.updation_date = new java.util.Date()
                    entranceDegree.updation_ip_address = request.getRemoteAddr()
                    entranceDegree.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Degree Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceDegree")
            return
        }
    }
    def activateIs_CompulsoryEntranceDegree(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateIs_CompulsoryEntranceDegree :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceDegree entranceDegree = EntranceDegree.findById(params.entranceDegreeId)
            if(entranceDegree) {
                if(entranceDegree.iscompulsory) {
                    entranceDegree.iscompulsory = false
                    entranceDegree.updation_username = login.username
                    entranceDegree.updation_date = new java.util.Date()
                    entranceDegree.updation_ip_address = request.getRemoteAddr()
                    entranceDegree.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceDegree.iscompulsory = true
                    entranceDegree.updation_username = login.username
                    entranceDegree.updation_date = new java.util.Date()
                    entranceDegree.updation_ip_address = request.getRemoteAddr()
                    entranceDegree.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Degree Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceDegree")
            return
        }
    }
    def deleteEntranceDegree(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceDegree:: " + params)
            EntranceDegree entranceDegree = EntranceDegree.findById(params.entranceDegreeId)
            if (entranceDegree) {
                try {
                    entranceDegree.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceDegree")
            return
        }
    }
    def editEntranceDegree(){
        println(" i am editEntranceDegree :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        EntranceDegree entranceDegree1 = EntranceDegree.findByName(params.entranceDegreeName)
        EntranceDegree entranceDegree = EntranceDegree.findById(params.entranceDegreeId)
        if(entranceDegree != null && entranceDegree1== null|| entranceDegree1 == entranceDegree){
            entranceDegree.name = params.entranceDegreeName
            entranceDegree.organization = org
            entranceDegree.creation_username = login.username
            entranceDegree.updation_username = login.username
            entranceDegree.updation_ip_address = request.getRemoteAddr()
            entranceDegree.creation_ip_address = request.getRemoteAddr()
            entranceDegree.updation_date = new java.util.Date()
            entranceDegree.creation_date = new java.util.Date()
            entranceDegree.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addEntranceDegree')
        }else{
            flash.error = "Entrance Degree Already Exist..."
            redirect(action: 'addEntranceDegree')
        }
    }

    //EntranceApplicationStatus
    /*def addEntranceApplicationStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceApplicationStatus : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceApplicationStatus_list = EntranceApplicationStatus.findAllByOrganization(org)
            println("entranceApplicationStatus_list : "+entranceApplicationStatus_list)
            [entranceApplicationStatus_list:entranceApplicationStatus_list]
        }
    }
    def saveEntranceApplicationStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceDegree : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if(!params.entranceApplicationStatusName) {
                println("params.entranceApplicationStatusName "+params.entranceApplicationStatusName)
                flash.error = "Please add Entrance Application Status Name."
                redirect(action: 'addEntranceApplicationStatus')
            }
            else{
                EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findByName(params.entranceApplicationStatusName)
                if(entranceApplicationStatus == null)
                {
                    entranceApplicationStatus=new EntranceApplicationStatus()
                    entranceApplicationStatus.name = params.entranceApplicationStatusName
                    if(params.isactive == 'on') {
                        entranceApplicationStatus.isactive = true
                    } else {
                        entranceApplicationStatus.isactive = false
                    }
                    entranceApplicationStatus.organization = org
                    entranceApplicationStatus.creation_username = instructor.uid
                    entranceApplicationStatus.updation_username = instructor.uid
                    entranceApplicationStatus.updation_ip_address = request.getRemoteAddr()
                    entranceApplicationStatus.creation_ip_address = request.getRemoteAddr()
                    entranceApplicationStatus.updation_date = new java.util.Date()
                    entranceApplicationStatus.creation_date = new java.util.Date()
                    entranceApplicationStatus.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEntranceApplicationStatus')
                }else {
                    flash.error = "Entrance Application Status Already Exists..."
                    redirect(action:'addEntranceApplicationStatus')
                }
            }

        }

    }
    def activateEntranceApplicationStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEmploymentStatus :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findById(params.entranceApplicationStatusId)
            if(entranceApplicationStatus) {
                if(entranceApplicationStatus.isactive) {
                    entranceApplicationStatus.isactive = false
                    entranceApplicationStatus.updation_username = login.username
                    entranceApplicationStatus.updation_date = new java.util.Date()
                    entranceApplicationStatus.updation_ip_address = request.getRemoteAddr()
                    entranceApplicationStatus.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceApplicationStatus.isactive = true
                    entranceApplicationStatus.updation_username = login.username
                    entranceApplicationStatus.updation_date = new java.util.Date()
                    entranceApplicationStatus.updation_ip_address = request.getRemoteAddr()
                    entranceApplicationStatus.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Application Status Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceApplicationStatus")
            return
        }
    }
    def deleteEntranceApplicationStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceDegree:: " + params)
            EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findById(params.entranceApplicationStatusId)
            if (entranceApplicationStatus) {
                try {
                    entranceApplicationStatus.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceApplicationStatus")
            return
        }
    }
    def editEntranceApplicationStatus(){
        println(" i am editEntranceApplicationStatus :: "+params)
        if (session.user == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        EntranceApplicationStatus entranceApplicationStatus1 = EntranceApplicationStatus.findByName(params.entranceApplicationStatusName)
        EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findById(params.entranceApplicationStatusId)
        if(!entranceApplicationStatus1){
            if(params.isactive=='on') {entranceApplicationStatus.isactive=true} else {entranceApplicationStatus.isactive=false}
            entranceApplicationStatus.name = params.entranceApplicationStatusName
            entranceApplicationStatus.organization = org
            entranceApplicationStatus.creation_username = login.username
            entranceApplicationStatus.updation_username = login.username
            entranceApplicationStatus.updation_ip_address = request.getRemoteAddr()
            entranceApplicationStatus.creation_ip_address = request.getRemoteAddr()
            entranceApplicationStatus.updation_date = new java.util.Date()
            entranceApplicationStatus.creation_date = new java.util.Date()
            entranceApplicationStatus.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addEntranceApplicationStatus')
        }else{
            flash.error = "Entrance Application Status Already Exist..."
            redirect(action: 'addEntranceApplicationStatus')
        }
    }*/

    //EntranceDocumentType
    def addEntranceDocumentType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceDocumentType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceDocumentType_list = EntranceDocumentType.findAll()
            println("entranceDocumentType_list : "+entranceDocumentType_list)
            [entranceDocumentType_list:entranceDocumentType_list]
        }
    }
    def saveEntranceDocumentType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceDocumentType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceDocumentType entranceDocumentType = EntranceDocumentType.findByName(params.documentName)
            if(entranceDocumentType==null)
            {
                entranceDocumentType=new EntranceDocumentType()
                entranceDocumentType.name = params.documentName
                if(params.isactive == 'on') {
                    entranceDocumentType.isactive = true
                } else {
                    entranceDocumentType.isactive = false
                }
                if(params.iscompulsory == 'on') {
                    entranceDocumentType.iscompulsory = true
                } else {
                    entranceDocumentType.iscompulsory = false
                }
                entranceDocumentType.extension = params.documentextension
                entranceDocumentType.info = params.documentinfo
                entranceDocumentType.resolution = params.documentresolution
                entranceDocumentType.size = params.documentsize
                entranceDocumentType.organization = org
                entranceDocumentType.creation_username = instructor.uid
                entranceDocumentType.updation_username = instructor.uid
                entranceDocumentType.updation_ip_address = request.getRemoteAddr()
                entranceDocumentType.creation_ip_address = request.getRemoteAddr()
                entranceDocumentType.updation_date = new java.util.Date()
                entranceDocumentType.creation_date = new java.util.Date()
                entranceDocumentType.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addEntranceDocumentType')
            }else {
                flash.error = "Entrance Document Type already Exists..."
                redirect(action:'addEntranceDocumentType')
            }

        }
    }
    def activateEntranceDocumentType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEntranceDocumentType :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceDocumentType entranceDocumentType = EntranceDocumentType.findById(params.entranceDocumentTypeId)
            if(entranceDocumentType) {
                if(entranceDocumentType.isactive) {
                    entranceDocumentType.isactive = false
                    entranceDocumentType.updation_username = login.username
                    entranceDocumentType.updation_date = new java.util.Date()
                    entranceDocumentType.updation_ip_address = request.getRemoteAddr()
                    entranceDocumentType.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceDocumentType.isactive = true
                    entranceDocumentType.updation_username = login.username
                    entranceDocumentType.updation_date = new java.util.Date()
                    entranceDocumentType.updation_ip_address = request.getRemoteAddr()
                    entranceDocumentType.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Document Type Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceDocumentType")
            return
        }
    }
    def activate_IsCompulsoryEntranceDocumentType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activate_IsCompulsoryEntranceDocumentType :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceDocumentType entranceDocumentType = EntranceDocumentType.findById(params.entranceDocumentTypeId)
            if(entranceDocumentType) {
                if(entranceDocumentType.iscompulsory) {
                    entranceDocumentType.iscompulsory = false
                    entranceDocumentType.updation_username = login.username
                    entranceDocumentType.updation_date = new java.util.Date()
                    entranceDocumentType.updation_ip_address = request.getRemoteAddr()
                    entranceDocumentType.save(flush: true, failOnError: true)
                    flash.message = "Compulsory De-activated Successfully.."
                } else {
                    entranceDocumentType.iscompulsory = true
                    entranceDocumentType.updation_username = login.username
                    entranceDocumentType.updation_date = new java.util.Date()
                    entranceDocumentType.updation_ip_address = request.getRemoteAddr()
                    entranceDocumentType.save(flush: true, failOnError: true)
                    flash.message = "Compulsory Activated Successfully.."
                }
            } else {
                flash.error = "Entrance Document Type Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceDocumentType")
            return
        }
    }
    def deleteEntranceDocumentType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceDocumentType:: " + params)
            EntranceDocumentType entranceDocumentType = EntranceDocumentType.findById(params.entranceDocumentTypeId)
            if (entranceDocumentType) {
                try {
                    entranceDocumentType.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceDocumentType")
            return
        }
    }
    def editEntranceDocumentType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In editEntranceDocumentType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceDocumentType entranceDocumentType1 = EntranceDocumentType.findByName(params.documentname)
            println("entranceDocumentType1 : " + entranceDocumentType1)
            EntranceDocumentType entranceDocumentType = EntranceDocumentType.findById(params.entranceDocumentTypeId)
            println("entranceDocumentType : " + entranceDocumentType)
            if (entranceDocumentType != null && entranceDocumentType1== null|| entranceDocumentType1 == entranceDocumentType){
                entranceDocumentType.name = params.documentname
                if (params.isactive == 'on') {
                    entranceDocumentType.isactive = true
                } else {
                    entranceDocumentType.isactive = false
                }
                if (params.iscompulsory == 'on') {
                    entranceDocumentType.iscompulsory = true
                } else {
                    entranceDocumentType.iscompulsory = false
                }
                entranceDocumentType.extension = params.documentextension
                entranceDocumentType.info = params.documentinfo
                entranceDocumentType.resolution = params.documentresolution
                entranceDocumentType.size = params.documentsize
                entranceDocumentType.organization = org
                entranceDocumentType.creation_username = instructor.uid
                entranceDocumentType.updation_username = instructor.uid
                entranceDocumentType.updation_ip_address = request.getRemoteAddr()
                entranceDocumentType.creation_ip_address = request.getRemoteAddr()
                entranceDocumentType.updation_date = new java.util.Date()
                entranceDocumentType.creation_date = new java.util.Date()
                entranceDocumentType.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addEntranceDocumentType')
            } else {
                flash.error = "Entrance Document Type Already Exists..."
                redirect(action: 'addEntranceDocumentType')
            }
        }
    }

    //EntranceRejectionReason
    def addEntranceRejectionReason(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceRejectionReason : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceRejectionReason_list = EntranceRejectionReason.findAllByOrganization(org)
            println("entranceRejectionReason_list : "+entranceRejectionReason_list)
            [entranceRejectionReason_list:entranceRejectionReason_list]
        }
    }
    def saveEntranceRejectionReason(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceRejectionReason : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceRejectionReason entranceRejectionReason = EntranceRejectionReason.findByName(params.eRejectionReasonName)
            if(entranceRejectionReason==null)
            {
                entranceRejectionReason=new EntranceRejectionReason()
                entranceRejectionReason.name = params.eRejectionReasonName
                if(params.isactive == 'on') {
                    entranceRejectionReason.isactive = true
                } else {
                    entranceRejectionReason.isactive = false
                }
                entranceRejectionReason.organization = org
                entranceRejectionReason.creation_username = instructor.uid
                entranceRejectionReason.updation_username = instructor.uid
                entranceRejectionReason.updation_ip_address = request.getRemoteAddr()
                entranceRejectionReason.creation_ip_address = request.getRemoteAddr()
                entranceRejectionReason.updation_date = new java.util.Date()
                entranceRejectionReason.creation_date = new java.util.Date()
                entranceRejectionReason.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addEntranceRejectionReason')
            }else {

                flash.error = "Rejection Reason already Exists..."
                redirect(action:'addEntranceRejectionReason')
            }


        }
    }
    def deleteEntranceRejectionReason(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceRejectionReason:: " + params)
            EntranceRejectionReason entranceRejectionReason = EntranceRejectionReason.findById(params.eRejectionReasonId)
            if (entranceRejectionReason) {
                try {
                    entranceRejectionReason.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceRejectionReason")
            return
        }
    }
    def activateEntranceRejectionReason(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEntranceRejectionReason :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceRejectionReason entranceRejectionReason = EntranceRejectionReason.findById(params.eRejectionReasonId)
            if(entranceRejectionReason) {
                if(entranceRejectionReason.isactive) {
                    entranceRejectionReason.isactive = false
                    entranceRejectionReason.updation_username = login.username
                    entranceRejectionReason.updation_date = new java.util.Date()
                    entranceRejectionReason.updation_ip_address = request.getRemoteAddr()
                    entranceRejectionReason.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceRejectionReason.isactive = true
                    entranceRejectionReason.updation_username = login.username
                    entranceRejectionReason.updation_date = new java.util.Date()
                    entranceRejectionReason.updation_ip_address = request.getRemoteAddr()
                    entranceRejectionReason.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Rejection Reason Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceRejectionReason")
            return
        }
    }
    def editEntranceRejectionReason(){
        println(" i am editEntranceRejectionReason :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        EntranceRejectionReason entranceRejectionReason1 = EntranceRejectionReason.findByName(params.eRejectionReasonName)
        EntranceRejectionReason entranceRejectionReason = EntranceRejectionReason.findById(params.eRejectionReasonId)
        if(!entranceRejectionReason1){
            if(params.isactive=='on')
            {
                entranceRejectionReason.isactive=true
            }
            else
            {
                entranceRejectionReason.isactive=false
            }
            entranceRejectionReason.name = params.eRejectionReasonName
            entranceRejectionReason.organization = org
            entranceRejectionReason.creation_username = login.username
            entranceRejectionReason.updation_username = login.username
            entranceRejectionReason.updation_ip_address = request.getRemoteAddr()
            entranceRejectionReason.creation_ip_address = request.getRemoteAddr()
            entranceRejectionReason.updation_date = new java.util.Date()
            entranceRejectionReason.creation_date = new java.util.Date()
            entranceRejectionReason.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addEntranceRejectionReason')
        }else{
            flash.error = "Entrance Rejection ReasonAlready Exist..."
            redirect(action: 'addEntranceRejectionReason')
        }
    }

    //EntranceAuthorityType
    def addEntranceAuthorityType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceAuthorityType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceAuthorityType_list = EntranceAuthorityType.findAllByOrganization(org)
            println("entranceAuthorityType_list : "+entranceAuthorityType_list)
            [entranceAuthorityType_list:entranceAuthorityType_list]
        }
    }
    def saveEntranceAuthorityType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceAuthorityType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(params.entranceAuthorityTypeName)
            if(entranceAuthorityType==null)
            {
                entranceAuthorityType=new EntranceAuthorityType()
                entranceAuthorityType.name = params.entranceAuthorityTypeName
                if(params.islastauthority == 'on') {
                    entranceAuthorityType.islastauthority = true
                } else {
                    entranceAuthorityType.islastauthority = false
                }
                entranceAuthorityType.organization = org
                entranceAuthorityType.creation_username = instructor.uid
                entranceAuthorityType.updation_username = instructor.uid
                entranceAuthorityType.updation_ip_address = request.getRemoteAddr()
                entranceAuthorityType.creation_ip_address = request.getRemoteAddr()
                entranceAuthorityType.updation_date = new java.util.Date()
                entranceAuthorityType.creation_date = new java.util.Date()
                entranceAuthorityType.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addEntranceAuthorityType')
            }else {

                flash.error = "Entrance Authority Type already Exists..."
                redirect(action:'addEntranceAuthorityType')
            }
        }
    }
    def deleteEntranceAuthorityType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceAuthorityType:: " + params)
            EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findById(params.entranceAuthorityTypeId)
            if (entranceAuthorityType) {
                try {
                    entranceAuthorityType.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceAuthorityType")
            return
        }
    }
    def activateIsLast_EAuthorityType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateIsLast_EAuthorityType :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findById(params.entranceAuthorityTypeId)
            if(entranceAuthorityType) {
                if(entranceAuthorityType.islastauthority) {
                    entranceAuthorityType.islastauthority = false
                    entranceAuthorityType.updation_username = login.username
                    entranceAuthorityType.updation_date = new java.util.Date()
                    entranceAuthorityType.updation_ip_address = request.getRemoteAddr()
                    entranceAuthorityType.save(flush: true, failOnError: true)
                    flash.message = "Is-last? De-activated Successfully.."
                } else {
                    entranceAuthorityType.islastauthority = true
                    entranceAuthorityType.updation_username = login.username
                    entranceAuthorityType.updation_date = new java.util.Date()
                    entranceAuthorityType.updation_ip_address = request.getRemoteAddr()
                    entranceAuthorityType.save(flush: true, failOnError: true)
                    flash.message = "Is-last? Activated Successfully.."
                }
            } else {
                flash.error = "Entrance Authority Type Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceAuthorityType")
            return
        }
    }
    def editEntranceAuthorityType(){
        println(" in editEntranceAuthorityType :: "+params)
        if (session.loginid == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization

        EntranceAuthorityType entranceAuthorityType1 = EntranceAuthorityType.findByName(params.entranceAuthorityTypeName)
        EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findById(params.entranceAuthorityTypeId)
        if(!entranceAuthorityType1){
            if(params.islastauthority=='on')
            {
                entranceAuthorityType.islastauthority=true
            }
            else
            {
                entranceAuthorityType.islastauthority=false
            }
            entranceAuthorityType.name = params.entranceAuthorityTypeName
            entranceAuthorityType.organization = org
            entranceAuthorityType.creation_username = login.username
            entranceAuthorityType.updation_username = login.username
            entranceAuthorityType.updation_ip_address = request.getRemoteAddr()
            entranceAuthorityType.creation_ip_address = request.getRemoteAddr()
            entranceAuthorityType.updation_date = new java.util.Date()
            entranceAuthorityType.creation_date = new java.util.Date()
            entranceAuthorityType.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addEntranceAuthorityType')
        }else{
            flash.error = "Entrance Authority Type Already Exist..."
            redirect(action: 'addEntranceAuthorityType')
        }
    }

    //Eligibility
    def addEligibility(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEligibility : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def eligibility_list = Eligibility.findAllByOrganization(org)
            println("eligibility_list : "+eligibility_list)
            def programTypeList = ProgramType.findAllByOrganization(org)
            println("programTypeList : "+programTypeList)
            [eligibility_list:eligibility_list,programTypeList:programTypeList]
        }
    }
    def saveEligibility(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEligibility : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if( params.programType == 'null') {
                flash.error = "Please add a Program type...."
            }
            else{
                ProgramType programtype = ProgramType.findByName(params.programType)
                Eligibility eligibility = Eligibility.findByNameAndProgramtype(params.eligibilityName,programtype)
                if(eligibility==null)
                {
                    eligibility=new Eligibility()
                    eligibility.name = params.eligibilityName
                    eligibility.sort_order = Integer.parseInt(params.sortOrder)
                    eligibility.programtype = programtype
                    if(params.isactive == 'on') {eligibility.isactive = true} else {eligibility.isactive = false}
                    eligibility.organization = org
                    eligibility.creation_username = instructor.uid
                    eligibility.updation_username = instructor.uid
                    eligibility.updation_ip_address = request.getRemoteAddr()
                    eligibility.creation_ip_address = request.getRemoteAddr()
                    eligibility.updation_date = new java.util.Date()
                    eligibility.creation_date = new java.util.Date()
                    eligibility.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEligibility')
                }else {

                    flash.error = " Eligibility Already Exists..."
                    redirect(action:'addEligibility')
                }
            }

        }
    }
    def activateEligibility(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEligibility :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            Eligibility eligibility = Eligibility.findById(params.eligibilityId)
            if(eligibility) {
                if(eligibility.isactive) {
                    eligibility.isactive = false
                    eligibility.updation_username = login.username
                    eligibility.updation_date = new java.util.Date()
                    eligibility.updation_ip_address = request.getRemoteAddr()
                    eligibility.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    eligibility.isactive = true
                    eligibility.updation_username = login.username
                    eligibility.updation_date = new java.util.Date()
                    eligibility.updation_ip_address = request.getRemoteAddr()
                    eligibility.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Eligibility Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEligibility")
            return
        }
    }
    def deleteEligibility(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEligibility:: " + params)
            Eligibility eligibility = Eligibility.findById(params.eligibilityId)
            if (eligibility) {
                try {
                    eligibility.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEligibility")
            return
        }
    }
    def editEligibility(){
        println(" in editEligibility :: "+params)
        if (session.loginid == null){
            println"user == null"
            redirect(action: 'erplogin')
        }
        else{
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            ProgramType programtype = ProgramType.findByName(params.programType)
            Eligibility eligibility1 = Eligibility.findByNameAndProgramtype(params.eligibilityName,programtype)
            Eligibility eligibility = Eligibility.findById(params.eligibilityId)
            if(!eligibility1){
                if(params.isactive=='on')
                {
                    eligibility.isactive=true
                }
                else
                {
                    eligibility.isactive=false
                }
                eligibility.name = params.eligibilityName
                eligibility.programtype = programtype
                eligibility.sort_order = Integer.parseInt(params.sortOrder)
                eligibility.organization = org
                eligibility.creation_username = login.username
                eligibility.updation_username = login.username
                eligibility.updation_ip_address = request.getRemoteAddr()
                eligibility.creation_ip_address = request.getRemoteAddr()
                eligibility.updation_date = new java.util.Date()
                eligibility.creation_date = new java.util.Date()
                eligibility.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
            }else{
                flash.error = "Eligibility Already Exist..."
            }
            redirect(action: 'addEligibility')
        }
    }

    //EntranceVersion
    def addEntranceVersion(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceVersion : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceVersion_list = EntranceVersion.findAllByOrganization(org)
            def programTypeList = ProgramType.findAllByOrganization(org)
            def academicYearList = AcademicYear.findAllByIsactive(true).sort{it.ay}

            [entranceVersion_list:entranceVersion_list,programTypeList:programTypeList,academicYearList:academicYearList]
        }
    }
    def saveEntranceVersion(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceVersion : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            println("params.exam_start_time :: " + params.exam_start_time)

            if( params.programType == 'null' || params.academicYear == null) {
                flash.error = "Please add a All Fields..!"
                redirect(action: 'addEntranceVersion')
            }
            else {
                ProgramType programtype = ProgramType.findByName(params.programType)
                AcademicYear academicYear = AcademicYear.findByAy(params.academicYear)
                EntranceVersion entranceVersion = EntranceVersion.findByEntrance_nameAndProgramtype(params.entranceName,programtype)
                if(entranceVersion==null) {
                    entranceVersion= new EntranceVersion()
                    entranceVersion.entrance_name = params.entranceName
                    entranceVersion.programtype = programtype
                    entranceVersion.academicyear = academicYear
                    entranceVersion.version_number = Integer.parseInt(params.version_number)
                    entranceVersion.application_end_date = params.application_end_date
                    entranceVersion.application_start_date = params.application_start_date
                    entranceVersion.applicationtrack = Integer.parseInt(params.applicationtrack)
                    entranceVersion.receipttrack = Integer.parseInt(params.receipttrack)
                    entranceVersion.version_date = params.version_date
                    entranceVersion.exam_date= params.exam_date
                    entranceVersion.exam_start_time =  (params.exam_start_time).toString()
                    entranceVersion.exam_end_time = (params.exam_end_time).toString()
                    entranceVersion.exam_link = params.exam_link
                    if(params.isactive == 'on') {entranceVersion.isactive = true} else {entranceVersion.isactive = false}
                    entranceVersion.organization = org
                    entranceVersion.creation_username = instructor.uid
                    entranceVersion.updation_username = instructor.uid
                    entranceVersion.updation_ip_address = request.getRemoteAddr()
                    entranceVersion.creation_ip_address = request.getRemoteAddr()
                    entranceVersion.updation_date = new java.util.Date()
                    entranceVersion.creation_date = new java.util.Date()
                    entranceVersion.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEntranceVersion')
                }
                else {
                    flash.error = "Entrance Version Already Exists..!"
                    redirect(action:'addEntranceVersion')
                }
            }

        }
    }
    def activateEntranceVersion(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEntranceVersion :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersionId)
            if(entranceVersion) {
                if(entranceVersion.isactive) {
                    entranceVersion.isactive = false
                    entranceVersion.updation_username = login.username
                    entranceVersion.updation_date = new java.util.Date()
                    entranceVersion.updation_ip_address = request.getRemoteAddr()
                    entranceVersion.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceVersion.isactive = true
                    entranceVersion.updation_username = login.username
                    entranceVersion.updation_date = new java.util.Date()
                    entranceVersion.updation_ip_address = request.getRemoteAddr()
                    entranceVersion.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Version Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceVersion")
            return
        }
    }
    def deleteEntranceVersion(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceVersion:: " + params)
            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersionId)
            if (entranceVersion) {
                try {
                    entranceVersion.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceVersion")
            return
        }
    }
    def editEntranceVersion(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }
        else {
            println("In editEntranceVersion : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if( params.programType == 'null' || params.academicYear == null) {
                flash.error = "Please add All Fields While Updating..!"
                redirect(action: 'addEntranceVersion')
            }
            else {
                ProgramType programtype = ProgramType.findByName(params.programType)
                AcademicYear academicYear = AcademicYear.findById(params.academicYear)
                EntranceVersion entranceVersion1 = EntranceVersion.findByEntrance_name(params.entranceName)
                EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersionId)
                if(entranceVersion?.id != entranceVersion1?.id && entranceVersion1) {
                    flash.error = "EntranceVersion - " + entranceVersion1?.entrance_name + "  already exists..!"
                    redirect(action: 'addEntranceVersion')
                }
                else {
                    entranceVersion.entrance_name = params.entranceName
                    entranceVersion.version_number = params.version_number
                    entranceVersion.programtype = programtype
                    entranceVersion.academicyear = academicYear
                    entranceVersion.application_end_date = params.application_end_date
                    entranceVersion.application_start_date = params.application_start_date
                    entranceVersion.version_number = Integer.parseInt(params.version_number)
                    entranceVersion.applicationtrack = Integer.parseInt(params.applicationtrack)
                    entranceVersion.receipttrack = Integer.parseInt(params.receipttrack)
                    entranceVersion.version_date = params.version_date
                    entranceVersion.exam_date= params.exam_date
                    entranceVersion.exam_start_time =   params.exam_start_time
                    entranceVersion.exam_end_time = params.exam_end_time
                    entranceVersion.exam_link = params.exam_link
                    entranceVersion.organization = org
                    entranceVersion.updation_username = instructor.uid
                    entranceVersion.updation_ip_address = request.getRemoteAddr()
                    entranceVersion.updation_date = new java.util.Date()
                    entranceVersion.save(flush: true, failOnError: true)
                    flash.message = "Updated Successfully"
                    redirect(action: 'addEntranceVersion')
                }
            }
        }
    }

    //EntranceBranch
    def addEntranceBranch(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceBranch : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceVersion_list = EntranceVersion.findAllByOrganization(org)
            def program_list = Program.findAllByOrganization(org)
            def entranceBranch_list = EntranceBranch.findAllByOrganization(org)

            [entranceVersion_list:entranceVersion_list,entranceBranch_list:entranceBranch_list,program_list:program_list]
        }
    }
    def saveEntranceBranch(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceBranch : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if( params.program == 'null' || params.entranceVersion == null) {
                flash.error = "Please add a Program  ,Entrance Version...."
            }
            else{
                Program program = Program.findByName(params.program)
                EntranceVersion entranceVersion = EntranceVersion.findByEntrance_name(params.entranceVersion)
                //EntranceBranch entranceBranch1 = EntranceBranch.findByName(params.entranceBranchName)
                EntranceBranch entranceBranch = EntranceBranch.findByNameAndProgramAndEntranceversion(params.entranceBranchName,program,entranceVersion)
                if(entranceBranch==null)
                {
                    entranceBranch=new EntranceBranch()
                    entranceBranch.name = params.entranceBranchName
                    entranceBranch.program = program
                    entranceBranch.entranceversion = entranceVersion
                    if(params.isactive == 'on') {entranceBranch.isactive = true} else {entranceBranch.isactive = false}
                    entranceBranch.organization = org
                    entranceBranch.creation_username = instructor.uid
                    entranceBranch.updation_username = instructor.uid
                    entranceBranch.updation_ip_address = request.getRemoteAddr()
                    entranceBranch.creation_ip_address = request.getRemoteAddr()
                    entranceBranch.updation_date = new java.util.Date()
                    entranceBranch.creation_date = new java.util.Date()
                    entranceBranch.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEntranceBranch')
                }else {

                    flash.error = "Entrance Branch Already Exists..."
                    redirect(action:'addEntranceBranch')
                }
            }

        }
    }
    def activateEntranceBranch(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEntranceBranch :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceBranch entranceBranch = EntranceBranch.findById(params.entranceBranchId)
            if(entranceBranch) {
                if(entranceBranch.isactive) {
                    entranceBranch.isactive = false
                    entranceBranch.updation_username = login.username
                    entranceBranch.updation_date = new java.util.Date()
                    entranceBranch.updation_ip_address = request.getRemoteAddr()
                    entranceBranch.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceBranch.isactive = true
                    entranceBranch.updation_username = login.username
                    entranceBranch.updation_date = new java.util.Date()
                    entranceBranch.updation_ip_address = request.getRemoteAddr()
                    entranceBranch.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Branch Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceBranch")
            return
        }
    }
    def deleteEntranceBranch(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceBranch:: " + params)
            EntranceBranch entranceBranch = EntranceBranch.findById(params.entranceBranchId)
            if (entranceBranch) {
                try {
                    entranceBranch.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceBranch")
            return
        }
    }
    def editEntranceBranch(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In editEntranceBranch : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if( params.program == 'null' || params.entranceVersion == null) {
                flash.error = "Please add a Program  ,Entrance Version...."
            }
            else{
                Program program = Program.findByName(params.program)
                EntranceVersion entranceVersion = EntranceVersion.findByEntrance_name(params.entranceVersion)
                EntranceBranch entranceBranch = EntranceBranch.findById(params.entranceBranchId)
                EntranceBranch entranceBranch1 = EntranceBranch.findByNameAndProgramAndEntranceversion(params.entranceBranchName,program,entranceVersion)
                if(entranceBranch1==null)
                {
                    entranceBranch.name = params.entranceBranchName
                    entranceBranch.program = program
                    entranceBranch.entranceversion = entranceVersion
                    entranceBranch.organization = org
                    entranceBranch.updation_username = instructor.uid
                    entranceBranch.updation_ip_address = request.getRemoteAddr()
                    entranceBranch.updation_date = new java.util.Date()
                    entranceBranch.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEntranceBranch')
                }else {

                    flash.error = "Entrance Branch Already Exists..."
                    redirect(action:'addEntranceBranch')
                }
            }

        }
    }

    //EntranceAuthorityLevel
    def addEntranceAuthorityLevel(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceAuthorityLevel : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceAuthorityLevel_list = EntranceAuthorityLevel.findAllByOrganization(org)
            def entranceAuthorityType_list = EntranceAuthorityType.findAllByOrganization(org)
            //def entranceVersion_list = EntranceVersion.findAllByOrganization(org)
            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersionId)
            [entranceAuthorityLevel_list:entranceAuthorityLevel_list,entranceAuthorityType_list:entranceAuthorityType_list,entranceVersion:entranceVersion]
        }
    }
    def saveEntranceAuthorityLevel(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceAuthorityLevel : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if( params.entranceAuthorityType == 'null' || params.entranceVersionId == null) {
                flash.error = "Please add a Program  ,Entrance Version...."
            }
            else{
                EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(params.entranceAuthorityType)
                EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersionId)
                EntranceAuthorityLevel entranceAuthorityLevel = EntranceAuthorityLevel.findByLevel(params.entranceAuthLevel)
                EntranceAuthorityLevel entranceAuthorityLevel1 = EntranceAuthorityLevel.findByEntranceauthoritytypeAndLevel(entranceAuthorityType,params.entranceAuthLevel)
                //EntranceAuthorityLevel entranceAuthorityLevel2 = EntranceAuthorityLevel.findByLevel()
                if(entranceAuthorityLevel1 == null && entranceAuthorityLevel == null)
                {
                    entranceAuthorityLevel=new EntranceAuthorityLevel()
                    entranceAuthorityLevel.level = Integer.parseInt(params.entranceAuthLevel)
                    entranceAuthorityLevel.entranceauthoritytype = entranceAuthorityType
                    entranceAuthorityLevel.entranceversion = entranceVersion
                    if(params.islastauthority == 'on') {entranceAuthorityLevel.islastauthority = true} else {entranceAuthorityLevel.islastauthority = false}
                    entranceAuthorityLevel.organization = org
                    entranceAuthorityLevel.creation_username = instructor.uid
                    entranceAuthorityLevel.updation_username = instructor.uid
                    entranceAuthorityLevel.updation_ip_address = request.getRemoteAddr()
                    entranceAuthorityLevel.creation_ip_address = request.getRemoteAddr()
                    entranceAuthorityLevel.updation_date = new java.util.Date()
                    entranceAuthorityLevel.creation_date = new java.util.Date()
                    entranceAuthorityLevel.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEntranceAuthorityLevel',params:[entranceVersionId:entranceVersion?.id])
                }else {

                    flash.error = "Entrance Authority Level Already Exists..."
                    redirect(action:'addEntranceAuthorityLevel')
                }
            }

        }
    }
    def activateIsLast_EntranceAuthLevel(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateIsLast_EntranceAuthLevel :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            EntranceAuthorityLevel entranceAuthorityLevel = EntranceAuthorityLevel.findById(params.entranceAuthorityLevelId)
            if(entranceAuthorityLevel) {
                if(entranceAuthorityLevel.islastauthority) {
                    entranceAuthorityLevel.islastauthority = false
                    entranceAuthorityLevel.updation_username = login.username
                    entranceAuthorityLevel.updation_date = new java.util.Date()
                    entranceAuthorityLevel.updation_ip_address = request.getRemoteAddr()
                    entranceAuthorityLevel.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceAuthorityLevel.islastauthority = true
                    entranceAuthorityLevel.updation_username = login.username
                    entranceAuthorityLevel.updation_date = new java.util.Date()
                    entranceAuthorityLevel.updation_ip_address = request.getRemoteAddr()
                    entranceAuthorityLevel.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Authority Level Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceAuthorityLevel")
            return
        }
    }
    def deleteEntranceAuthorityLevel(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceAuthorityLevel:: " + params)
            EntranceAuthorityLevel entranceAuthorityLevel = EntranceAuthorityLevel.findById(params.entranceAuthorityLevelId)
            if (entranceAuthorityLevel) {
                try {
                    entranceAuthorityLevel.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceAuthorityLevel",params:[entranceVersionId:params.entranceVersionId])
            return
        }
    }
    def editEntranceAuthorityLevel(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In editEntranceAuthorityLevel : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if( params.entranceAuthorityType == 'null' || params.entranceVersion == null) {
                flash.error = "Please add a Program  ,Entrance Version...."
            }
            else{
                EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(params.entranceAuthorityType)
                EntranceVersion entranceVersion = EntranceVersion.findByEntrance_name(params.entranceVersion)
                EntranceAuthorityLevel entranceAuthorityLevel = EntranceAuthorityLevel.findById(params.entranceAuthorityLevelId)
                EntranceAuthorityLevel entranceAuthorityLevel1 = EntranceAuthorityLevel.findByEntranceauthoritytypeAndEntranceversion(entranceAuthorityType,entranceVersion)
                EntranceAuthorityLevel entranceAuthorityLevel2 = EntranceAuthorityLevel.findByLevelAndEntranceversion(params.entranceAuthLevel,entranceVersion)

                if(entranceAuthorityLevel1 == null || entranceAuthorityLevel2 == null)
                {
                    entranceAuthorityLevel.level = Integer.parseInt(params.entranceAuthLevel)
                    entranceAuthorityLevel.entranceauthoritytype = entranceAuthorityType
                    entranceAuthorityLevel.entranceversion = entranceVersion
                    entranceAuthorityLevel.organization = org
                    entranceAuthorityLevel.updation_username = instructor.uid
                    entranceAuthorityLevel.updation_ip_address = request.getRemoteAddr()
                    entranceAuthorityLevel.updation_date = new java.util.Date()
                    entranceAuthorityLevel.save(flush: true, failOnError: true)
                    flash.message = "Updated Successfully"
                    redirect(action: 'addEntranceAuthorityLevel', params:[entranceVersionId:entranceVersion?.id])
                }else {

                    flash.error = "Entrance Authority Level Already Exists..."
                    redirect(action:'addEntranceAuthorityLevel')
                }
            }

        }
    }

    //EntranceFees
    def addEntranceFees(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addEntranceFees : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceFees_list = EntranceFees.findAllByOrganization(org)
            def exemption_list = Exemption.findAllByOrganization(org)
            def programType_list = ProgramType.findAllByOrganization(org)
            def entranceCategoryType_list = EntranceCategoryType.findAllByOrganization(org)
            def entranceVersion_list = EntranceVersion.findAllByOrganization(org)

            [entranceFees_list:entranceFees_list,entranceCategoryType_list:entranceCategoryType_list,entranceVersion_list:entranceVersion_list,
             exemption_list:exemption_list,programType_list:programType_list]
        }

    }
    def saveEntranceFees(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveEntranceFees : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if( params.entranceCategoryType == 'null' || params.entranceVersion == null || params.exemption == null || params.programType == null)
            {
                flash.error = "Please Fill All Details..."
                redirect(action: 'addEntranceFees')
            }
            else{
                Exemption exemption = Exemption.findByName(params.exemption)
                ProgramType programType = ProgramType.findByName(params.programType)
                EntranceCategoryType entranceCategoryType = EntranceCategoryType.findByName(params.entranceCategoryType)
                EntranceVersion entranceVersion = EntranceVersion.findByEntrance_name(params.entranceVersion)
                EntranceFees entranceFees = EntranceFees.findByExemptionAndProgramtypeAndEntrancecategorytypeAndEntranceversion(exemption,programType,entranceCategoryType,entranceVersion)
                //println("entranceFees : "+entranceFees)
                if(entranceFees == null )
                {
                    entranceFees=new EntranceFees()
                    entranceFees.fees = Double.parseDouble(params.fees)
                    entranceFees.exemption = exemption
                    entranceFees.programtype = programType
                    entranceFees.entrancecategorytype = entranceCategoryType
                    entranceFees.entranceversion = entranceVersion
                    entranceFees.organization = org
                    entranceFees.creation_username = instructor.uid
                    entranceFees.updation_username = instructor.uid
                    entranceFees.updation_ip_address = request.getRemoteAddr()
                    entranceFees.creation_ip_address = request.getRemoteAddr()
                    entranceFees.updation_date = new java.util.Date()
                    entranceFees.creation_date = new java.util.Date()
                    entranceFees.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addEntranceFees')
                }else {

                    flash.error = "Entrance Fees Already Exists..."
                    redirect(action:'addEntranceFees')
                }
            }

        }
    }
    def deleteEntranceFees(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteEntranceFees:: " + params)
            EntranceFees entranceFees = EntranceFees.findById(params.entranceFeesId)
            if (entranceFees) {
                try {
                    entranceFees.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceFees")
            return
        }
    }
    def editEntranceFees(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In editEntranceFees : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if( params.entranceCategoryType == 'null' || params.entranceVersion == null || params.exemption == null || params.programType == null)
            {
                flash.error = "Please Fill All Details..."
                redirect(action: 'addEntranceFees')
            }
            else{
                Exemption exemption = Exemption.findByName(params.exemption)
                ProgramType programType = ProgramType.findByName(params.programType)
                EntranceCategoryType entranceCategoryType = EntranceCategoryType.findByName(params.entranceCategoryType)
                EntranceVersion entranceVersion = EntranceVersion.findByEntrance_name(params.entranceVersion)
                EntranceFees entranceFees1 = EntranceFees.findByExemptionAndProgramtypeAndEntrancecategorytypeAndEntranceversionAndFees(exemption,programType,entranceCategoryType,entranceVersion,params.fees)
                EntranceFees entranceFees = EntranceFees.findById(params.entranceFeesId)
                if(entranceFees1 == null )
                {
                    entranceFees.fees = Double.parseDouble(params.fees)
                    entranceFees.exemption = exemption
                    entranceFees.programtype = programType
                    entranceFees.entrancecategorytype = entranceCategoryType
                    entranceFees.entranceversion = entranceVersion
                    entranceFees.organization = org
                    entranceFees.updation_username = instructor.uid
                    entranceFees.updation_ip_address = request.getRemoteAddr()
                    entranceFees.updation_date = new java.util.Date()
                    entranceFees.save(flush: true, failOnError: true)
                    flash.message = "Updated Successfully"
                    redirect(action: 'addEntranceFees')
                }else {
                    flash.error = "Entrance Fees Already Exists..."
                    redirect(action:'addEntranceFees')
                }
            }

        }
    }
    /* def activateEntranceFees(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateEntranceBranch :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceFees entranceFees = EntranceFees.findById(params.entranceFeesId)
            if(entranceFees) {
                if(entranceFees.isactive) {
                    entranceFees.isactive = false
                    entranceFees.updation_username = login.username
                    entranceFees.updation_date = new java.util.Date()
                    entranceFees.updation_ip_address = request.getRemoteAddr()
                    entranceFees.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    entranceFees.isactive = true
                    entranceFees.updation_username = login.username
                    entranceFees.updation_date = new java.util.Date()
                    entranceFees.updation_ip_address = request.getRemoteAddr()
                    entranceFees.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Entrance Fees Not found"
            }
            redirect(controller: "entranceAdmissionMaster", action: "addEntranceFees")
            return
        }
    }*/

    //Nationality
    def addERPNationality(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addERPNationality : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def erp_Nationality_list = ERPNationality.findAll()
            //println("erp_Nationality_list : "+erp_Nationality_list)
            [erp_Nationality_list:erp_Nationality_list]
        }
    }
    def saveERPNationality(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In save ERP Nationality : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            ERPNationality erp_Nationality = ERPNationality.findByType(params.type)
            if(erp_Nationality == null ) {
                erp_Nationality=new ERPNationality()
                erp_Nationality.type = params.type
                erp_Nationality.username = login.username
                erp_Nationality.updation_ip_address = request.getRemoteAddr()
                erp_Nationality.creation_ip_address = request.getRemoteAddr()
                erp_Nationality.updation_date = new java.util.Date()
                erp_Nationality.creation_date = new java.util.Date()
                erp_Nationality.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addERPNationality')
            }
            else {
                flash.error = "Nationality Type Already Exists..!"
                redirect(action:'addERPNationality')
            }
        }
    }
    def deleteERPNationality(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteERPNationality:: " + params)
            ERPNationality erp_Nationality = ERPNationality.findById(params.erp_NationalityId)
            if (erp_Nationality) {
                try {
                    erp_Nationality.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be Deleted .."
                }
            } else {
                flash.error = "  Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "entranceAdmissionMaster", action: "addERPNationality")
            return
        }
    }
    def editERPNationality(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In edit ERP Nationality : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            ERPNationality erp_Nationality = ERPNationality.findById(params.erp_NationalityId)
            ERPNationality erp_Nationality1= ERPNationality.findByType(params.type)
            println("erp_Nationality1 : "+erp_Nationality1)
            if(erp_Nationality?.id != erp_Nationality1?.id && erp_Nationality1){
                flash.error = "Nationality - " + erp_Nationality1?.type + "  already exists..!"
                redirect(action: 'addERPNationality')
            }
            else {
                erp_Nationality.type = params.type
                erp_Nationality.username = login.username
                erp_Nationality.updation_ip_address = request.getRemoteAddr()
                erp_Nationality.updation_date = new java.util.Date()
                erp_Nationality.save(flush: true, failOnError: true)
                flash.message = "Updated Successfully"
                redirect(action: 'addERPNationality')
            }
        }
    }

    //Set EntranceAuthorityLevel
    def setEntranceAuthorityLevel(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("setEntranceAuthorityLevel : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def programTypeList = ProgramType.findAllByOrganization(org)
            def academicYearList = AcademicYear.findAllByIsactive(true).sort { it.ay }
            ProgramType programType = ProgramType.findByName(params.programType)
            EntranceVersion entranceVersion = EntranceVersion.findByProgramtype(programType)

            [programTypeList: programTypeList, academicYearList: academicYearList , entranceVersion:entranceVersion]
        }
    }
    def showEntranceVersionDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("showEntranceVersionDetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            if(params.ay == null || params.programType == null) {
                flash.message = "Select Academic Year and Program Type."
                redirect(action: 'setEntranceAuthorityLevel')
            }
            else{
                ProgramType programtype = ProgramType.findByName(params.programType)
                EntranceVersion entranceVersion = EntranceVersion.findByProgramtype(programtype)
                if(entranceVersion == null){
                    flash.message = "Version Details Not Found for Selected Program Type."
                    redirect(action: 'setEntranceAuthorityLevel')
                }else{
                    [entranceVersion: entranceVersion]
                    redirect(action:'setEntranceAuthorityLevel',params:[programType:params.programType])
                }
            }
        }
    }

    // Mark Authority
    def addMarkAuthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addMarkAuthority : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def instructor_list = Instructor.findAllByOrganization(org)
            def entranceAuthorityType_list = EntranceAuthorityType.findAllByOrganization(org)

            def final_list = [];
            /* for (inst in instructor_list) {
                 def sublist = [];
                 def authority_list = inst.entranceauthoritytype
                 if (authority_list.size() > 0) {
                     sublist.add(authority_list: authority_list, inst: inst)
                     final_list.add(sublist)
                 }
             }*/

            for (inst in instructor_list){
                if(inst?.entranceauthoritytype){
                    final_list.add(inst)
                }
            }

            [instructor_list: instructor_list, entranceAuthorityType_list: entranceAuthorityType_list, final_list: final_list]
        }
    }
    def saveMarkedEntranceAuthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In saveMarkedEntranceAuthority : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            Instructor instructor1 = Instructor.findById(params.instructorId)

            def entranceAuthority_list = []
            if (params.entranceAuthority.getClass().isArray()) {
                for (item in params.entranceAuthority) {
                    println("item : " + item)
                    EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(item)
                    println("entranceAuthorityType : " + entranceAuthorityType)
                    if (entranceAuthorityType) {
                        entranceAuthority_list.add(entranceAuthorityType)
                    }
                }
            } else {
                EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(params.entranceAuthority)
                println("entranceAuthorityType : " + entranceAuthorityType)
                if (entranceAuthorityType) {
                    entranceAuthority_list.add(entranceAuthorityType)
                }
            }
            println("entranceAuthority_list : " + entranceAuthority_list)
            if (instructor1) {
                for (items in entranceAuthority_list) {
                    instructor1.addToEntranceauthoritytype(items)
                }
                instructor1.username = instructor.uid
                instructor1.updation_ip_address = request.getRemoteAddr()
                instructor1.updation_date = new java.util.Date()
                instructor1.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addMarkAuthority')
            } else {
                flash.error = "Already Exists..."
                redirect(action: 'addMarkAuthority')
            }
        }
    }
    def deleteMarkEntranceAuthority(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteMarkEntranceAuthority:: " + params)
            Instructor instructor1 = Instructor.findById( params.instructorId)
            def entranceAuthority_list = []

            if (instructor1) {
                if (params.authority.getClass().isArray()) {
                    for (items in params.authority) {
                        println("item : " + items)
                        EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(items)
                        println("entranceAuthorityType : " + entranceAuthorityType)
                        if (entranceAuthorityType) {
                            entranceAuthority_list.add(entranceAuthorityType)
                        }
                    }
                } else {
                    EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(params.authority)
                    println("entranceAuthorityType : " + entranceAuthorityType)
                    if (entranceAuthorityType) {
                        entranceAuthority_list.add(entranceAuthorityType)
                    }
                }
                println("entranceAuthority_list : " + entranceAuthority_list)
                for (auth in entranceAuthority_list) {
                    println("auth : " + auth)
                    instructor1.removeFromEntranceauthoritytype(auth)
                }
                instructor1.save(flush: true, failOnError: true)
                flash.message = "Deleted successfully..."
                redirect(controller: "entranceAdmissionMaster", action: "addMarkAuthority")
                return
            }else{
                flash.message = "Not Found..."
                redirect(controller: "entranceAdmissionMaster", action: "addMarkAuthority")
                return
            }
        }
    }
    /*def editMarkEntranceAuthority(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In editMarkEntranceAuthority : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            Instructor instructor1 = Instructor.findById( params.instructorId)
            def entranceAuthority_list = []
            if(params.entranceAuthority.getClass().isArray()){
                for(items in params.entranceAuthority){
                    println("item : " + items)
                    EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(items)
                    if (entranceAuthorityType) {
                        entranceAuthority_list.add(entranceAuthorityType)
                    }
                }
            }else {
                EntranceAuthorityType entranceAuthorityType = EntranceAuthorityType.findByName(params.entranceAuthority)
                if (entranceAuthorityType) {
                    entranceAuthority_list.add(entranceAuthorityType)
                }
            }
            println("entranceAuthority_list : "+entranceAuthority_list)
            for (auth in entranceAuthority_list) {
                Instructor inst = Instructor.findByEntranceauthoritytype(auth)
                if(!inst){
                    instructor1.addToEntranceauthoritytype(auth)
                }
            }
        }
    }*/

    // Application Details
    def showApplicationDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("showApplicationDetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceApplication_list = EntranceApplication.findAllByOrganization(org)
            [entranceApplication_list:entranceApplication_list]
        }
    }
    def showVersionDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("showVersionDetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceApplication entranceApplication = EntranceApplication.findById(params.entranceApplicationId)
            def version = entranceApplication?.entranceversion
            [version:version]
        }
    }
    def showApplicantDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("showApplicantDetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceApplication entranceApplication = EntranceApplication.findById(params.entranceApplicationId)
            def entranceapplicant = entranceApplication?.entranceapplicant

            [entranceapplicant:entranceapplicant]
        }
    }
    def showQualificationDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("showQualificationDetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceApplication entranceApplication = EntranceApplication.findById(params.entranceApplicationId)
            def entranceapplicant = entranceApplication?.entranceapplicant
            EntranceApplicantAcademics entranceApplicantAcademics = EntranceApplicantAcademics.findByEntranceapplicant(entranceapplicant)

            [entranceApplicantAcademics:entranceApplicantAcademics]
        }
    }
    def showExperienceDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("showApplicantDetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceApplication entranceApplication = EntranceApplication.findById(params.entranceApplicationId)
            def entranceapplicant = entranceApplication?.entranceapplicant
            EntranceApplicantExperience entranceApplicantExperience = EntranceApplicantExperience.findByEntranceapplicant(entranceapplicant)
            if(entranceApplicantExperience){
                [entranceApplicantExperience:entranceApplicantExperience]
            }
            else{
                flash.error = "Applicant Does not have Experience"
                redirect(action: 'showApplicationDetails')
            }
        }
    }
    def showApplicantDocument(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("showVersionDetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceApplication entranceApplication = EntranceApplication.findById(params.entranceApplicationId)
            [entranceApplication:entranceApplication]
        }
    }
    def getExemptionTypedocumenturl() {
        println("getExemptionTypedocumenturl : " + params)
        EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
        ExemptionType exemptionType = ExemptionType.findById(params.exemptionTypeId)
        ExemptionDocument exemptionDocument = ExemptionDocument.findByEntranceapplicantAndExemptiontype(entranceApplication?.entranceapplicant,exemptionType)

        String path = exemptionDocument.file_path + exemptionDocument.file_name
        println("path :: " + path)
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSBucketService awsBucketService1 = new AWSBucketService()
        String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

        render "<a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue' aria-hidden='true'></i></a>"
    }
    def getPhysicallyHandicappeddocumenturl() {
        println("getPhysicallyHandicappeddocumenturl : " + params)

        EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
        String path = entranceApplication.is_physically_handicapped_file_path + entranceApplication.is_physically_handicapped_file_name
        println("path :: " + path)
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSBucketService awsBucketService1 = new AWSBucketService()
        String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

        render "<a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue' aria-hidden='true'></i></a>"
    }
    def getVisuallyHandicappeddocumenturl(){
        println("getVisuallyHandicappeddocumenturl : " + params)

        EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
        String path = entranceApplication.is_visually_handicapped_file_path + entranceApplication.is_visually_handicapped_file_name
        println("path :: " + path)
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSBucketService awsBucketService1 = new AWSBucketService()
        String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

        render "<a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue' aria-hidden='true'></i></a>"
    }
    def getEntranceApplicantDocumenturl(){
        println("getEntranceApplicantDocumenturl : " + params)
        EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
        def entranceApplicant = entranceApplication?.entranceapplicant
        def entranceApplicantDocument = EntranceApplicantDocument.findAllByEntranceapplicant(entranceApplicant)

        //def documentlist =[]
        for(doc in entranceApplicantDocument){
            String path = doc.filepath + doc.filename
            println("path :: " + path)
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSBucketService awsBucketService1 = new AWSBucketService()
            String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            String document = doc?.entrancedocumenttype?.name
            // documentlist.add(url:url,document:document)
            render document+"<a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue;margin-bottom:10px;margin-left:7px' aria-hidden='true'></i></a><br><br>"
        }
        // [documentlist : documentlist]
    }

}

