package volp

import grails.converters.JSON

class InventoryManageController {
   //Add Inventory form
   def addInventory() {
      println(" i am addInventory Controller")
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization
      def invoiceList = Invoice.findAllWhere(organization: org)
      [invoiceList:invoiceList]
   }
   def getRecieptsByDepartment(){
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization
      println"in getRecieptsByDepartment---------->"+params
      def dept = Department.findById(params.dept)
      //println"dept-->"+dept
      def invPR = InvPurchaseRequisition.findByDepartmentAndIsapprovedAndOrganization(dept,true,org)
      //println"invPR-->"+invPR
      def invQuotation = InvQuotation.findAllByInvpurchaserequisitionAndIsapprovedAndOrganization(invPR,true,org)
      //println"invQuotation-->"+invQuotation
      def invPO = InvPurchaseOrder.findAllByInvquotationAndIsapprovedAndOrganization(invQuotation,true,org)
      //println"invPO-->"+invPO
      def invoice = Invoice.findAllByInvpurchaseorderAndIsapprovedAndOrganization(invPO,true,org)
      //println"invoice-->"+invoice
      ArrayList recieptsList = new ArrayList()
      for(items in invoice){
            //def rec = InvReceipt.findByInvoiceAndIsactive(items,true)
            def rec = InvReceipt.findByInvoice(items)
            recieptsList.push(rec)
      }
      //println"recieptsList-->"+recieptsList
      [recieptsList:recieptsList]

   }
   def getMaterialByReciept() {
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization
      println("i am getMaterialByReciept params-->"+ params)
      def reciept = InvReceipt.findById(params.reciept)
      def invoice = reciept?.invoice
      def materialList = InvoiceDetails.findAllByInvoiceAndOrganization(invoice,org)?.invmaterial
      [materialList:materialList]
   }
   def getInventoryDetailsByInvoice(){
      println(" i am getInventoryDetailsByInvoice" + params)
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      if(params.invoice == null){
         flash.error  = "Please Select Invoice"
         return
      }else{
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         def org = instructor.organization
         def invoice=Invoice.findById(params.invoice)
         def invoiceDetails = InvoiceDetails.findAllByInvoice(invoice)
         def departmentList = Department.findAllWhere(organization: org)
         def materialList = []
         for(item in invoiceDetails){
            def invMaterial  = InvMaterial.findById(item?.invmaterial?.id)
            if(invMaterial){
               materialList.push(invMaterial)
            }
         }
         [materialList:materialList,departmentList:departmentList,invoice:invoice]
      }
   }
   def getMaterialDetailsByMaterial(){
      println(" i am getMaterialDetailsByMaterial Controller" + params)
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization
      def invMaterial = InvMaterial.findById(params.materialId)
      def materialPartList = InvMaterialPart.findAllWhere(invmaterial:invMaterial,isactive: true,organization: org)
      def invoice = Invoice.findById(params.invoiceId)
      def invoiceDetails = InvoiceDetails.findByInvoiceAndInvmaterial(invoice,invMaterial)
      def materialQty = invoiceDetails?.available_qty
      def materialCost = invoiceDetails?.cost_per_unit
      def depreciation = InvDepreciationPercentage.findByInvmaterialAndOrganization(invMaterial,org)
      [materialPartList:materialPartList,materialQty:materialQty,materialCost:materialCost,depreciation:depreciation]
   }
   def getRoomListByDept(){
      println(" i am getRoomListByDept Controller" + params)
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization
      def dept = Department.findById(params.departmentId)
      def invRoomList = InvRoom.findByDepartmentAndOrganization(dept,org)
      [invRoomList:invRoomList]
   }
   def saveInventoryDetails() {
      println(" i am getInventoryDetailsByMaterialName Controller")
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      if(params.department ==null)
      {
         flash.error="Please Select a department"
         redirect(action:'addInventory')
      }
      if(params.materialPart ==null)
      {
         flash.error="Please Select a material part"
         redirect(action:'addInventory')
      }
      if(params.material ==null)
      {
         flash.error="Please Select a material"
         redirect(action:'addInventory')
      }
      if(params.inventoryRoom ==null)
      {
         flash.error="Please Select a room"
         redirect(action:'addInventory')
      }
      if(params.inwardDate ==null)
      {
         flash.error="Please Select a date"
         redirect(action:'addInventory')
      }
      if(params.purchaseCost =="null")
      {
         flash.error="Please Select a date"
         redirect(action:'addInventory')
      }
      if(params.qty =="null")
      {
         flash.error="Please Add Quantity"
         redirect(action:'addInventory')
      }

      InventoryManageService invManageService=new InventoryManageService()
      invManageService.saveInventoryDetails(params,request,session,flash)
      redirect(action:'addInventory')
   }

   def viewInventoryDetails() {
      println(" i am viewInventoryDetails Controller")
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization
      def inventoryList=Inventory.findAllByOrganization(org)
      //def transferLevelList=InvTransferLevel.findAllByOrganizationAndIsactive(org,true)
      def deptList = Department.findAllByOrganization(org)
      [inventoryList:inventoryList,deptList:deptList]
   }
    def activateInventory(){
        println(" i am activateInventory Controller")
        if (session.user == null) {flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization
        Inventory inventory = Inventory.findById(params.InventoryId)
        if(inventory) {
            if(inventory.isactive) {
                inventory.isactive = false
                inventory.updation_username = login.username
                inventory.updation_date = new java.util.Date()
                inventory.updation_ip_address = request.getRemoteAddr()
                inventory.save(flush: true, failOnError: true)
                flash.message = "In-Active Successfully.."
            } else {
                inventory.isactive = true
                inventory.updation_username = login.username
                inventory.updation_date = new java.util.Date()
                inventory.updation_ip_address = request.getRemoteAddr()
                inventory.save(flush: true, failOnError: true)
                flash.message = "Active Successfully.."
            }
        } else {
            flash.error = "Inventory Not found"
        }
        redirect(controller: "inventoryManage", action: "viewInventoryDetails")
        return
    }
    def createTransferRequest(){
        if (session.user == null) {flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }
        if(params.dept || params.transferOrg){
            InventoryManageService invManageService=new InventoryManageService()
            invManageService.createTransferRequest(params,request,session,flash)
            redirect(action:'viewInventoryDetails')
        }
    }
    //not in use
   def getTransferTo(){
       if (session.user == null)
           redirect(controller: "login", action: 'erplogin')
       println"in getTransferTo->"+params
       Login login = Login.findById(session.loginid)
       Instructor instructor = Instructor.findByUid(login.username)
       def org = instructor.organization
       def transferLevel = InvTransferLevel.findById(params.transfer)
       //println"--->"+transferLevel?.name+"<-----------"
       boolean deptFlag = []
       def deptList
       if(transferLevel?.name == "Department"){
           deptFlag = true
           deptList = Department.findAllByOrganization(org)
       }else if(transferLevel?.name == "Institute"){
           deptFlag == false
       }
       [deptList:deptList,deptFlag:deptFlag]
   }

    //approve-inventory transfer flow
    def getInstPostInvTransferApprove(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getInstInvtransferApprove : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor, true, org)
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Inventory", true, org)
            [invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority, instructor:instructor, invFinanceApprovingCategory:invFinanceApprovingCategory]
        }
    }
    def getallInvTransferdetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getallInvTransferdetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Inventory", true, org)
            InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.post)
            InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByInvfinanceapprovingcategoryAndInvfinanceapprovingauthorityAndOrganizationAndIsactive(invFinanceApprovingCategory, invInstructorFinanceApprovingAuthority.invfinanceapprovingauthority, org, true)

            if (!invFinanceApprovingAuthorityLevel) {
                render "<div clas='alert alert-danger'>Please fill master of InvFinanceApprovingAuthorityLevel for category : " + invFinanceApprovingCategory.name + "</div>"
                return
            }
            def invTransferEscalationList = InventoryTransferEscalation.findAllByOrganizationAndInvfinanceapprovingauthoritylevelAndIsactive(org, invFinanceApprovingAuthorityLevel,true)
            println"----.invTransferEscalationList"+invTransferEscalationList
            if (invTransferEscalationList.size() == 0) {
                render "<div class='alert alert-danger'>Inventory Transfer Request not found for Approval</div>"
                return
            }
            [invTransferEscalationList     : invTransferEscalationList,
             invFinanceApprovingCategory           : invFinanceApprovingCategory,
             invFinanceApprovingAuthorityLevel     : invFinanceApprovingAuthorityLevel,
             invInstructorFinanceApprovingAuthority: invInstructorFinanceApprovingAuthority]
        }
    }
    def getInvTransferEscalationforApprove(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In getInvTransferEscalationforApprove : "+params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invapprovalstatus_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org, true)
            def next_level_approved = false

            def invTransferEscalation = InventoryTransferEscalation.findById(params.invTransferEscalationId)
            def invTransferEscalationList = InventoryTransferEscalation.findAllByInventoryAndIsactive(invTransferEscalation?.inventory,true)
            for (ITE in invTransferEscalationList){
                //println("ITE?.invfinanceapprovingauthoritylevel?.islast  : "+ITE?.invfinanceapprovingauthoritylevel?.islast )
                if(ITE?.invfinanceapprovingauthoritylevel?.islast == true) {
                    //println("ITE?.invapprovalstatus?.name == "+ITE?.invapprovalstatus?.name)
                    if (ITE?.invapprovalstatus?.name == 'Approved') {
                        next_level_approved = true
                        println("invTransferEscalation : "+invTransferEscalation)
                        println("ITE : "+ITE)
                        if(ITE == invTransferEscalation){
                            next_level_approved = false
                        }
                    }
                }
            }
            //println("next_level_approved :: " + next_level_approved)
            if(invTransferEscalation) {
                def invTransferEscalation_List = InventoryTransferEscalation.findAllByOrganizationAndInventoryAndIsactive(org, invTransferEscalation?.inventory,true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [invTransferEscalation_List:invTransferEscalation_List, invTransferEscalation : invTransferEscalation, invapprovalstatus_list:invapprovalstatus_list, next_level_approved:next_level_approved]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }
    def saveApproveInvTransfer(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveApproveInvTransfer : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvApprovalStatus invApprovalStatus =  InvApprovalStatus.findById(params.approvingstatus)
            def invTransferEscalation = InventoryTransferEscalation.findById(params.iteId)
            if(invTransferEscalation) {
                invTransferEscalation.action_date = new Date()
                invTransferEscalation.remark = params.remark
                invTransferEscalation.actionby = instructor
                invTransferEscalation.updation_username = login.username
                invTransferEscalation.updation_date = new Date()
                invTransferEscalation.updation_ip_address = request.getRemoteAddr()
                invTransferEscalation.invapprovalstatus = invApprovalStatus
                invTransferEscalation.save(flush: true, failOnError: true)
                if(invTransferEscalation?.invfinanceapprovingauthoritylevel?.islast) {
                    if(invApprovalStatus?.name == "Approved"){
                        def inventory = invTransferEscalation?.inventory
                        inventory.department = invTransferEscalation?.department
                        inventory.updation_username = login.username
                        inventory.updation_date = new Date()
                        inventory.updation_ip_address = request.getRemoteAddr()
                        inventory.save(flush: true, failOnError: true)

                        for(item in InventoryTransferEscalation.findAllByInventory(inventory)){
                            item.isactive= false
                            item.save(flush: true,failOnError: true)
                        }
                    }
                }
                def invTransferEscalation_List = InventoryTransferEscalation.findAllByOrganizationAndInventoryAndIsactive(org, invTransferEscalation?.inventory,true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [invTransferEscalation_List:invTransferEscalation_List, invTransferEscalation : invTransferEscalation]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }

   //Dead Stock Register
   def deadStockRegister(){
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      }
      else{
         println(" I am in deadStockRegister")
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         Organization org  = instructor.organization
         def invDepartmentList=Department.findAllByOrganization(org)
         [invDepartmentList:invDepartmentList]
      }

   }
   def getRoomNumbers() {
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      }
      else{
         println(" I am in getRoomNumbers"+ params)
        def department=Department.findById(params.department)
         def invRoomList=InvRoom.findAllByDepartment(department)
         [invRoomList:invRoomList]
      }
   }
   def fetchInventory(){
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      }
      else{
         println(" I am in fetchInventory"+params)
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         Organization org  = instructor.organization
         if(params.department == 'null' || params.department == null || params.department == '' ||
            params.roomNumber == 'null' || params.roomNumber == null || params.roomNumber == ''){
            flash.error ="Please Fill All Details"
            return
         }else{
            def department=Department.findById(params.department)
            def invRoom=InvRoom.findById(params.roomNumber)
            def inventoryList=Inventory.findAllWhere(department:department,invroom:invRoom,organization: org)
            //println("inventoryList-->"+ inventoryList)
            [inventoryList:inventoryList]
         }

      }
   }

   //Dead Stock Verification
   def deadStockVerification(){
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      }
      else
      {
         println("I am in deadStockVerification"+ params)
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         Organization org  = instructor.organization
         def inventoryList=Inventory.list()
         def deadstockstatuslist=InvDeadstockStatus.findAllByOrganizationAndIsactive(org,true)
         [inventoryList:inventoryList,deadstockstatuslist:deadstockstatuslist]
      }

   }
   def getDeadStockVerificationForm(){
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      }
      else
      {
         println("I am in getDeadStockVerificationForm"+ params)
         if(params.accessionNumber != 'null'){
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         Organization org  = instructor.organization
             def accNo = params.accessionNumber.trim()
         Inventory inventory=Inventory.findByAccession_number(accNo)
         def deadStockStatusList=InvDeadstockStatus.findAllByOrganizationAndIsactive(org,true)
         [inventory:inventory,deadStockStatusList:deadStockStatusList]
         }else{
            flash.error = "Please input Accession Number"
            return
         }
      }
   }
   def saveDeadStockVerification() {
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      }
      else {
         println("I am in saveDeadStockVerification"+ params)
         InventoryManageService inventoryManageService=new InventoryManageService()
         inventoryManageService.saveDeadStockVerification(params,request,session,flash)
         redirect(action:'deadStockVerification')
      }
   }
   def activateDeadStockInventory(){
      println(" i am activateDeadStockInventory Controller")
      if (session.user == null) {flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      }
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      Organization org = instructor.organization
      Inventory inventory = Inventory.findById(params.InventoryId)
      if(inventory) {
         if(inventory.isactive) {
            inventory.isactive = false
            inventory.updation_username = login.username
            inventory.updation_date = new java.util.Date()
            inventory.updation_ip_address = request.getRemoteAddr()
            inventory.save(flush: true, failOnError: true)
            flash.message = "In-Active Successfully.."
         } else {
            inventory.isactive = true
            inventory.updation_username = login.username
            inventory.updation_date = new java.util.Date()
            inventory.updation_ip_address = request.getRemoteAddr()
            inventory.save(flush: true, failOnError: true)
            flash.message = "Active Successfully.."
         }
      } else {
         flash.error = "Inventory Not found"
      }
      redirect(controller: "inventoryManage", action: "deadStockVerification")
      return
   }
   def editDeadStockVerification(){
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      }
      else {
         InventoryManageService inventoryManageService=new InventoryManageService()
         inventoryManageService.editDeadStockVerification(params,request,session,flash)
         redirect(action:'deadStockVerification')
      }
   }


   //write-off request
   def addInvWriteoffRequest(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization

      println("addInvWriteoffRequest " + params)
      def invdeadstockstatusList = InvDeadstockStatus.findAllWhere(organization: org)?.status
      def inventoryAccNoList = Inventory.findAllWhere(organization: org,isactive:true)
      Inventory inventory = Inventory.findByAccession_number(params.accession_number)
      def invWriteOffRequest = InvWriteOffRequest.findByRequestby(instructor)

      InvWriteOffRequestEscalation invWriteOffRequestEscalation = InvWriteOffRequestEscalation.findByInvwriteoffrequest(invWriteOffRequest)
      //println("invWriteOffRequestEscalation : "+invWriteOffRequestEscalation?.invapprovalstatus?.name)

      [inventoryAccNoList:inventoryAccNoList,invdeadstockstatusList: invdeadstockstatusList,inventory:inventory,
       invWriteOffRequest:invWriteOffRequest,invWriteOffRequestEscalation:invWriteOffRequestEscalation]
   }
   def getValuesByAccesionNumber(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization

      println("getValuesByAccesionNumber params : "+params)
      Inventory inventory = Inventory.findByIdAndOrganization(params.invId,org)
      def materialid = inventory?.invmaterial?.id
      def materialname = InvMaterial.findById(materialid)?.name
      def materialtype= InvMaterialType.findById(materialid)?.name
      def invmaterialcategory = InvMaterialCategory.findById(materialid)?.name
       def invdeadstockstatusList = InvDeadstockStatus.findAllWhere(organization: org)

      [invdeadstockstatusList:invdeadstockstatusList,invmaterialcategory:invmaterialcategory,materialid:materialid,materialname:materialname,materialtype:materialtype,inventory:inventory]

   }
   def saveInvWriteoffRequestfetchdata(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else{
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         def org = instructor.organization

         if (params.status== 'null' || params.status== null || params.status== ''){
            flash.message = "Kindly Select Deadstock Status"
            redirect(action: 'addInvWriteoffRequest')
         }else{
            //println(" saveInvWriteoffRequestfetchdata control;er  "+params)
            InventoryManageService inventoryManageService = new InventoryManageService()
            inventoryManageService.saveInvWriteoffRequestfetchdata(params, session, request, flash)
            redirect(action: 'addInvWriteoffRequest')
         }
      }
   }
   def activatewriteoffReq(){
      println(" i am activatewriteoffReq"+params)
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else{
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         def org = instructor.organization
         InvWriteOffRequest invWriteOffRequest = InvWriteOffRequest.findById(params.writeoffReqId)
         def invWriteOffRequestEscalation_list = InvWriteOffRequestEscalation.findAllByInvwriteoffrequest(invWriteOffRequest)

         if(invWriteOffRequest) {
            if(invWriteOffRequest.isactive) {
               invWriteOffRequest.isactive = false
               invWriteOffRequest.updation_username = login.username
               invWriteOffRequest.updation_date = new Date()
               invWriteOffRequest.updation_ip_address = request.getRemoteAddr()
               invWriteOffRequest.save(flush: true, failOnError: true)
               if(invWriteOffRequestEscalation_list){
                  for(WRE in invWriteOffRequestEscalation_list){
                     WRE.isactive = false
                     WRE.save(flush: true, failOnError: true)
                  }
               }
               redirect(action: 'addInvWriteoffRequest')
               flash.message = "In-Active Successfully.."
            } else {
               invWriteOffRequest.isactive = true
               invWriteOffRequest.updation_username = login.username
               invWriteOffRequest.updation_date = new Date()
               invWriteOffRequest.updation_ip_address = request.getRemoteAddr()
               invWriteOffRequest.save(flush: true, failOnError: true)
               if(invWriteOffRequestEscalation_list){
                  for(WRE in invWriteOffRequestEscalation_list){
                     WRE.isactive = true
                     WRE.save(flush: true, failOnError: true)
                  }
               }
               redirect(action: 'addInvWriteoffRequest')
               flash.message = "Active Successfully.."

            }
         } else {

            redirect(action: 'addInvWriteoffRequest')
            flash.error = "WriteOff Request Not found"
         }
         return
      }
   }

   //approve-writeoff request
  /* def approveWriteoffRequest(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else {
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         def org = instructor.organization

         println("approveWriteoffRequest params : "+params)

         def invApprovalStatusList = InvApprovalStatus.findAllByOrganization(org)
         def department = Department.findAllWhere(organization: org)
         def roomNo = InvRoom.findAllWhere(organization: org)
         InvApprovalStatus invApprovalStatus = InvApprovalStatus.findByName(params.approvestatus)
         def invWriteOffRequestEscalation = InvWriteOffRequestEscalation.findAllByOrganizationAndInvapprovalstatusAndActionbyAndIsactive(org,invApprovalStatus,instructor,true)
          println("invWriteOffRequestEscalation : "+invWriteOffRequestEscalation)
         def invwriteoffrequest = invWriteOffRequestEscalation?.invwriteoffrequest
         def inventory = invwriteoffrequest?.inventory

         [department:department,roomNo:roomNo,invApprovalStatusList:invApprovalStatusList,invWriteOffRequestEscalation:invWriteOffRequestEscalation,
          invwriteoffrequest:invwriteoffrequest,inventory:inventory]
      }
   }
   def inventoryDetailsAwo(){
      println(" in inventoryDetailsAwo")
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else {
         println("inventoryDetailsAwo params : " + params)
         InvWriteOffRequestEscalation invWriteOffRequestEscalation = InvWriteOffRequestEscalation.findById(params.approveWOId)
         def inventoryApWoR = invWriteOffRequestEscalation.invwriteoffrequest?.inventory
         println("inventoryApWoR : "+inventoryApWoR)
         [inventoryApWoR:inventoryApWoR,invWriteOffRequestEscalation:invWriteOffRequestEscalation]
      }
   }
   def getRoomNobyDepartment(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization

      println("getRoomNobyDepartment params : "+params)
      Department department= Department.findByName(params.dept)
      println("department : "+department)
      def invRoom = InvRoom.findByDepartment(department)
      println("invRoom : "+invRoom)

      [invRoom:invRoom]
   }
   def writeOffRequestApproval(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else {
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         def org = instructor.organization
         println("writeOffRequestApproval params : " + params)

         InventoryManageService inventoryManageService = new InventoryManageService()
         inventoryManageService.writeOffRequestApproval(params, session, request, flash)
         redirect(action: 'approveWriteoffRequest')
      }
   }
   def writeOffApprovalhistory(){
      println(" in writeOffApprovalhistory "+params)
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization
      def invwriteoffrequest=InvWriteOffRequest.findById(params.invwriteoffrequestId)
      def invWriteOffRequestEscalationlist=InvWriteOffRequestEscalation.findAllByInvwriteoffrequestAndOrganization(invwriteoffrequest,org)
      [invWriteOffRequestEscalationlist:invWriteOffRequestEscalationlist,invwriteoffrequest:invwriteoffrequest]
   }*/

   //approve-writeoff request updated flow
   def getInstwriteoffApprove(){
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      } else {
         println("getInstpost : " + params)
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         Organization org = instructor.organization

         def invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor, true, org)
         InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Write Off", true, org)
         [invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority, instructor:instructor, invFinanceApprovingCategory:invFinanceApprovingCategory]
      }
   }
   def getallwriteoffdetails(){
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      } else {
         println("getallwriteoffdetails : " + params)
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         Organization org = instructor.organization

         InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Write Off", true, org)
         //println"invFinanceApprovingCategory----->"+invFinanceApprovingCategory
         InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.post)
         InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByInvfinanceapprovingcategoryAndInvfinanceapprovingauthorityAndOrganizationAndIsactive(invFinanceApprovingCategory, invInstructorFinanceApprovingAuthority.invfinanceapprovingauthority, org, true)

         if (!invFinanceApprovingAuthorityLevel) {
            render "<div clas='alert alert-danger'>Please fill master of InvFinanceApprovingAuthorityLevel for category : " + invFinanceApprovingCategory.name + "</div>"
            return
         }
         def invWriteOffRequestEscalation_list = InvWriteOffRequestEscalation.findAllByOrganizationAndInvfinanceapprovingauthoritylevelAndIsactive(org, invFinanceApprovingAuthorityLevel, true)
         if (invWriteOffRequestEscalation_list.size() == 0) {
            render "<div class='alert alert-danger'>Write off Request not found for Approval</div>"
            return
         }
         [invWriteOffRequestEscalation_list     : invWriteOffRequestEscalation_list,
          invFinanceApprovingCategory           : invFinanceApprovingCategory,
          invFinanceApprovingAuthorityLevel     : invFinanceApprovingAuthorityLevel,
          invInstructorFinanceApprovingAuthority: invInstructorFinanceApprovingAuthority]
      }
   }
   def getWOEscalationforApprove(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In getWOEscalationforApprove : "+params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invapprovalstatus_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org, true)
            def next_level_approved = false

            InvWriteOffRequestEscalation invWriteOffRequestEscalation = InvWriteOffRequestEscalation.findById(params.woId)

            def writeOffEscalations = InvWriteOffRequestEscalation.findAllByInvwriteoffrequest(invWriteOffRequestEscalation?.invwriteoffrequest)
            for (WOE in writeOffEscalations){

                println("WOE?.invfinanceapprovingauthoritylevel?.islast  : "+WOE?.invfinanceapprovingauthoritylevel?.islast )
                if(WOE?.invfinanceapprovingauthoritylevel?.islast == true) {
                    println("WOE?.invapprovalstatus?.name == "+WOE?.invapprovalstatus?.name)
                    if (WOE?.invapprovalstatus?.name == 'Approved') {
                        next_level_approved = true
                        println("invWriteOffRequestEscalation : "+invWriteOffRequestEscalation)
                        println("WOE : "+WOE)
                        if(WOE == invWriteOffRequestEscalation){
                            next_level_approved = false
                        }
                    }
                }
            }
            println("next_level_approved :: " + next_level_approved)
            if(invWriteOffRequestEscalation) {
                def writeoffEscalation_list = InvWriteOffRequestEscalation.findAllByOrganizationAndInvwriteoffrequestAndIsactive(org, invWriteOffRequestEscalation?.invwriteoffrequest, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [writeoffEscalation_list:writeoffEscalation_list, invWriteOffRequestEscalation : invWriteOffRequestEscalation, invapprovalstatus_list:invapprovalstatus_list, next_level_approved:next_level_approved]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }
   def saveApproveWriteOff(){
      if (session.loginid == null) {
         flash.message = "Your are logged out! Please login again. Thank you."
         redirect(controller: "login", action: "erplogin")
         return
      } else {
         println("saveApproveWriteOff : " + params)
         Login login = Login.findById(session.loginid)
         Instructor instructor = Instructor.findByUid(login.username)
         Organization org = instructor.organization

         InvApprovalStatus invApprovalStatus =  InvApprovalStatus.findById(params.approvingstatus)
         InvWriteOffRequestEscalation invWriteOffRequestEscalation = InvWriteOffRequestEscalation.findById(params.woId)
         if(invWriteOffRequestEscalation) {
            invWriteOffRequestEscalation.action_date = new Date()
            invWriteOffRequestEscalation.remark = params.remark
            invWriteOffRequestEscalation.isactive = true
            invWriteOffRequestEscalation.actionby = instructor
            invWriteOffRequestEscalation.updation_username = login.username
            invWriteOffRequestEscalation.updation_date = new Date()
            invWriteOffRequestEscalation.updation_ip_address = request.getRemoteAddr()
            invWriteOffRequestEscalation.invapprovalstatus = invApprovalStatus
            invWriteOffRequestEscalation.save(flush: true, failOnError: true)

            if(invWriteOffRequestEscalation?.invfinanceapprovingauthoritylevel?.islast) {
               InvWriteOffRequest invWriteOffRequest = invWriteOffRequestEscalation.invwriteoffrequest
              // invWriteOffRequest.invapprovalstatus = invApprovalStatus
               invWriteOffRequest.inventory.writeoff_date = new Date()
               invWriteOffRequest.inventory.iswriteoff = true
               invWriteOffRequest.isapproved = true
               invWriteOffRequest.updation_username = login.username
               invWriteOffRequest.updation_date = new Date()
               invWriteOffRequest.updation_ip_address = request.getRemoteAddr()
               invWriteOffRequest.save(flush: true, failOnError: true)
            }

            def writeOffEscalation_list = InvWriteOffRequestEscalation.findAllByOrganizationAndInvwriteoffrequestAndIsactive(org, invWriteOffRequestEscalation?.invwriteoffrequest, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
            [writeOffEscalation_list:writeOffEscalation_list, invWriteOffRequestEscalation : invWriteOffRequestEscalation]
         } else {
            render "<div class='alert alert-danger'>Not Found....</div>"
            return
         }
      }
   }


   //material inspection
   def addmaterialInspection(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization

      def invPurchaseOrderList = InvPurchaseOrder.findAllWhere(organization: org)?.pon
      def academicYearList = AcademicYear.findAllByIsactive(true).sort{it.ay}
      def invVendorList = InvVendor.findAllWhere(organization: org)?.company_name
      println" in addmaterialInspection"+params
      if(params.poNo == null && params.vendor == null){
         flash.error = "Please Fill All Details"
      }
      def invpo = InvPurchaseOrder.findByPon(params.poNo)
      def vendor = InvVendor.findByCompany_name(params.vendor)
      def invoicelist = Invoice.findAllByInvpurchaseorderAndInvvendor(invpo,vendor)
      def academicyear = params.ay

      [academicYearList:academicYearList,invPurchaseOrderList:invPurchaseOrderList,invVendorList:invVendorList,invoicelist:invoicelist,academicyear:academicyear]
   }
   def viewInvoice(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else{
         println("viewInvoice in materialInspection : "+params)
         def invoice1 = Invoice.findById(params.invoiceId)
         Inventory inventory = Inventory.findByInvoice(invoice1)
         /*def invmaterial = inventory?.invmaterial
         def invPurchaseRequisitionDetails = InvPurchaseRequisitionDetails.findByInvmaterial(invmaterial)
         def invPurchaseOrder = invoice1?.invpurchaseorder
         def invPurchaseOrderDetails = InvPurchaseOrderDetails.findByInvpurchaseorder(invPurchaseOrder)*/
         def invoiceDetailsList = InvoiceDetails.findAllByInvoice(invoice1)

         [invoice1:invoice1,invoiceDetailsList:invoiceDetailsList]
      }
   }
   def invoicematerialInspection() {
      if (session.user == null)
         redirect(controller: "login", action: 'erplogin')
      else {
          println(" i am invoicematerialInspection" + params)
         def invoice1 = Invoice.findById(params.invoiceId)
         [invoice1:invoice1]
      }
   }
   def saveMaterialInspection(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else{
         InventoryManageService inventoryManageService = new InventoryManageService()
         inventoryManageService.saveMaterialInspection(params, session, request, flash)
         redirect(action: 'addmaterialInspection')
      }
   }
   def activateMaterialInspection(){
      println(" i am activateInvBudget")
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else{
         InventoryManageService inventoryManageService = new InventoryManageService()
         inventoryManageService.activateMaterialInspection(params,session,request,flash)
         redirect(action: 'addmaterialInspection')
      }
   }
   def activatePoDetailsInspMt(){
      println(" i am activatePoDetailsInspMt")
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      else{
         InventoryManageService inventoryManageService = new InventoryManageService()
         inventoryManageService.activatePoDetailsInspMt(params,session,request,flash)
         redirect(action: 'viewInvoice')
      }
   }
   def getValuesByAcademicyear(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization

      println("getValuesByAcademicyear params : "+params)
      AcademicYear academicYear = AcademicYear.findByAy(params.ay)
      def invPRList = InvPurchaseRequisition.findAllByAcademicyear(academicYear)
      def QuotationList = []
      for(item in invPRList){
         def invQuotation = InvQuotation.findByInvpurchaserequisition(item)
         if(invQuotation != null){
            QuotationList.push(invQuotation)
         }
      }
      def invPOList = []
      for(items in QuotationList){
         def invPurchaseOrder = InvPurchaseOrder.findByInvquotation(items)
         if(invPurchaseOrder != null){
            invPOList.push(invPurchaseOrder)
         }
      }
      InvVendor invVendor = invPOList[0]?.invvendor
      def vendorCompanyList = invVendor?.company_name

      [invPOList:invPOList,invVendor:invVendor,vendorCompanyList:vendorCompanyList]
   }
   def getVendorByPoNumber(){
      if (session.user == null)
         redirect(controller:"login", action: 'erplogin')
      Login login = Login.findById(session.loginid)
      Instructor instructor = Instructor.findByUid(login.username)
      def org = instructor.organization

      println("getVendorByPoNumber "+params)
      InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findByPon(params.poNo)
      def invVendor = invPurchaseOrder?.invvendor

      [invVendor:invVendor]
   }

}
