package volp

class InventoryMastersController {

    def index() {}

    def mastertables() {
        println("mastertables :: " + params)
    }

    //master -FormConfiguration
    def addFormConfiguration() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addFormConfiguration: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def formconfigurationlist = FormConfiguration.list()
            def formName_list = FormName.list()
            [formconfigurationlist: formconfigurationlist, formName_list: formName_list]
        }
    }

    def saveFormConfiguration() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveFormConfiguration : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.formname == 'null') {
                flash.error = "Please select Form Name ...."
            }

            FormName fname =FormName.findById(params.formname)
            if (fname) {
                    FormConfiguration formConfiguration = FormConfiguration.findByOrganizationAndField_nameAndFormname(org, params.fieldname, fname)
                    if (formConfiguration) {
                        flash.error = "Already Exists ...."
                    }
                    else {
                        formConfiguration = new FormConfiguration()
                        formConfiguration.field_name = params.fieldname
                        formConfiguration.display_name = params.displayname
                        formConfiguration.isActive = true
                        formConfiguration.organization = org
                        formConfiguration.formname = fname
                        formConfiguration.creation_username = login.username
                        formConfiguration.updation_username = login.username
                        formConfiguration.creation_date = new Date()
                        formConfiguration.updation_date = new Date()
                        formConfiguration.creation_ip_address = request.getRemoteAddr()
                        formConfiguration.updation_ip_address = request.getRemoteAddr()
                        formConfiguration.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully.."
                   }

            } else {
                flash.error = "Mandatory fields are missing..."
            }
            redirect(controller: "inventoryMasters", action: "addFormConfiguration")
            return
        }
    }

    def editFormConfiguration(){
        println(" i am editFormConfiguration" + params)
        if (session.loginid == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization
        if(params.instfinanceapprovingauthority != 'null' && params.instfinanceapprovingauthority != 'null'){
            FormName fname =FormName.findById(params.formname)
            println("fname ->"+fname)
             FormConfiguration formConfiguration = FormConfiguration.findById(params.configurationid)
             FormConfiguration fieldNameDuplicate = FormConfiguration.findByOrganizationAndField_nameAndFormname(org, params.editfieldname, fname)
            println("fieldNameDuplicate ->"+fieldNameDuplicate)
            if (fieldNameDuplicate == null) {
                formConfiguration.field_name = params.editfieldname
                formConfiguration.display_name = params.editdisplayname
                formConfiguration.isActive = true
                formConfiguration.organization = org
                formConfiguration.formname= fname
                formConfiguration.creation_username = login.username
                formConfiguration.updation_username = login.username
                formConfiguration.creation_date = new Date()
                formConfiguration.updation_date = new Date()
                formConfiguration.updation_ip_address = request.getRemoteAddr()
                formConfiguration.creation_ip_address = request.getRemoteAddr()
                formConfiguration.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
            } else {
                flash.error = "Already Present"
            }
        }else{
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addFormConfiguration')
    }

    def deleteFormConfiguration() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteFormConfiguration :: " + params)
            FormConfiguration instapprovingAuthority = FormConfiguration.findById(params.instfinanceapprovingauthority)
            if (instapprovingAuthority) {
                try {
                    instapprovingAuthority.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Form can not delete .."
                }
            } else {
                flash.error = "Form Not found"
            }
            flash.message = "Deleted successfully..."
            redirect(controller: "inventoryMasters", action: "addFormConfiguration")
            return
        }
    }

    def activationFormConfiguration() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationFormConfiguration :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            FormConfiguration formConfiguration = FormConfiguration.findById(params.fieldactive)
            FormConfiguration f1 = FormConfiguration.findById(params.fieldrequired)
            FormConfiguration f2 = FormConfiguration.findById(params.fieldedit)

            if (formConfiguration) {
                if (formConfiguration.isActive) {
                    formConfiguration.isActive = false
                    formConfiguration.updation_username = login.username
                    formConfiguration.updation_date = new Date()
                    formConfiguration.updation_ip_address = request.getRemoteAddr()
                    formConfiguration.save(flush: true, failOnError: true)
                    flash.message = "In-Active Successfully.."
                } else {
                    formConfiguration.isActive = true
                    formConfiguration.updation_username = login.username
                    formConfiguration.updation_date = new Date()
                    formConfiguration.updation_ip_address = request.getRemoteAddr()
                    formConfiguration.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            }

            if (f1) {
                if (f1.isRequired) {
                    f1.isRequired = false
                    f1.updation_username = login.username
                    f1.updation_date = new Date()
                    f1.updation_ip_address = request.getRemoteAddr()
                    f1.save(flush: true, failOnError: true)
                    flash.message = "Not Required.."
                } else {
                    f1.isRequired = true
                    f1.updation_username = login.username
                    f1.updation_date = new Date()
                    f1.updation_ip_address = request.getRemoteAddr()
                    f1.save(flush: true, failOnError: true)
                    flash.message = "Required.."
                }
            }

            if (f2) {
                if (f2.isEditable) {
                    f2.isEditable = false
                    f2.updation_username = login.username
                    f2.updation_date = new Date()
                    f2.updation_ip_address = request.getRemoteAddr()
                    f2.save(flush: true, failOnError: true)
                    flash.message = "Not Editable.."
                } else {
                    f2.isEditable = true
                    f2.updation_username = login.username
                    f2.updation_date = new Date()
                    f2.updation_ip_address = request.getRemoteAddr()
                    f2.save(flush: true, failOnError: true)
                    flash.message = "Editable Required.."
                }
            }
            redirect(controller: "inventoryMasters", action: "addFormConfiguration")
            return
         }

    }


    //master -InvInstructorFinanceApprovingAuthority
    def addinvinstructorfinanceapprovingauthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addinvinstructorfinanceapprovingauthority : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invinstructorfinanceapprovingauthoritylist = InvInstructorFinanceApprovingAuthority.findAllByOrganization(org)
            def inst_list = Instructor.findAllByOrganizationAndIscurrentlyworking(org, true)
            def financeauthority_list = InvFinanceApprovingAuthority.findAllByOrganizationAndIsactive(org, true)
            [invinstructorfinanceapprovingauthoritylist: invinstructorfinanceapprovingauthoritylist, inst_list: inst_list,
             financeauthority_list                     : financeauthority_list]
        }
    }

    def saveinvinstructorfinanceapprovingauthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveinvinstructorfinanceapprovingauthority : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.financeapprovingauthority == 'null') {
                flash.error = "Please select Finance approving authority ...."
            }
            if (params.instructor == 'null') {
                flash.error = "Please select Instructor ...."
            }
            Instructor inst = Instructor.findById(params.instructor)
            InvFinanceApprovingAuthority approvingAuthority = InvFinanceApprovingAuthority.findById(params.financeapprovingauthority)
            if (approvingAuthority && inst) {
                if (params.instfinanceapprovingauthority != null) {
                    InvInstructorFinanceApprovingAuthority instapprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.instfinanceapprovingauthority)
                    if (params.isactive == 'on') {
                        instapprovingAuthority.isactive = true
                    } else {
                        instapprovingAuthority.isactive = false
                    }
                    instapprovingAuthority.instructor = inst
                    instapprovingAuthority.invfinanceapprovingauthority = approvingAuthority
                    instapprovingAuthority.updation_username = login.username
                    instapprovingAuthority.updation_date = new Date()
                    instapprovingAuthority.updation_ip_address = request.getRemoteAddr()
                    instapprovingAuthority.save(flush: true, failOnError: true)
                    flash.message = "Updation Successfully.."
                } else {
                    InvInstructorFinanceApprovingAuthority instapprovingAuthority = InvInstructorFinanceApprovingAuthority.findByOrganizationAndinstructorAndInvfinanceapprovingauthority(org, inst, approvingAuthority)
                    if (instapprovingAuthority) {
                        flash.error = "Already Exists ...."
                    } else {
                        instapprovingAuthority = new InvInstructorFinanceApprovingAuthority()
                        if (params.isactive == 'on') {
                            instapprovingAuthority.isactive = true
                        } else {
                            instapprovingAuthority.isactive = false
                        }
                        instapprovingAuthority.instructor = inst
                        instapprovingAuthority.invfinanceapprovingauthority = approvingAuthority
                        instapprovingAuthority.organization = org
                        instapprovingAuthority.creation_username = login.username
                        instapprovingAuthority.updation_username = login.username
                        instapprovingAuthority.creation_date = new Date()
                        instapprovingAuthority.updation_date = new Date()
                        instapprovingAuthority.creation_ip_address = request.getRemoteAddr()
                        instapprovingAuthority.updation_ip_address = request.getRemoteAddr()
                        instapprovingAuthority.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully.."
                    }
                }
            } else {
                flash.error = "Mandatory fields are missing..."
            }
            redirect(controller: "inventoryMasters", action: "addinvinstructorfinanceapprovingauthority")
            return
        }
    }

    def deleteinvinstructorfinanceapprovingauthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleteinvinstructorfinanceapprovingauthority :: " + params)
            InvInstructorFinanceApprovingAuthority instapprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.instfinanceapprovingauthority)
            if (instapprovingAuthority) {
                try {
                    instapprovingAuthority.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Instructor Finance Approving Authority can not delete .."
                }
            } else {
                flash.error = "Instructor Finance Approving Authority Not found"
            }
            flash.message = "Delected successfully..."
            redirect(controller: "inventoryMasters", action: "addinvinstructorfinanceapprovingauthority")
            return
        }
    }

    def activationinvinstructorfinanceapprovingauthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvinstructorfinanceapprovingauthority :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvInstructorFinanceApprovingAuthority instapprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.instfinanceapprovingauthority)
            if (instapprovingAuthority) {
                if (instapprovingAuthority.isactive) {
                    instapprovingAuthority.isactive = false
                    instapprovingAuthority.updation_username = login.username
                    instapprovingAuthority.updation_date = new Date()
                    instapprovingAuthority.updation_ip_address = request.getRemoteAddr()
                    instapprovingAuthority.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                } else {
                    instapprovingAuthority.isactive = true
                    instapprovingAuthority.updation_username = login.username
                    instapprovingAuthority.updation_date = new Date()
                    instapprovingAuthority.updation_ip_address = request.getRemoteAddr()
                    instapprovingAuthority.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                }
            } else {
                flash.error = "Instructor Finance Approving Authority Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvinstructorfinanceapprovingauthority")
            return
        }
    }

    //master-FormName
    def addNewFormName() {
        println(" i am save add New Form Name")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def formNameList = FormName.list()
        [formNameList: formNameList]
    }

    def saveFormName() {
        println(" i am save saveFormName")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.saveForm != 'null') {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            def formName = FormName.findByNameAndIsActive(params.saveForm.trim(), true)
            if (formName == null) {
                formName = new FormName()
                formName.name = params.saveForm.trim()
                formName.isActive = true
                formName.creation_username = instructor.uid
                formName.updation_username = instructor.uid
                formName.updation_ip_address = request.getRemoteAddr()
                formName.creation_ip_address = request.getRemoteAddr()
                formName.updation_date = new java.util.Date()
                formName.creation_date = new java.util.Date()

                formName.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
            } else {
                flash.error = "Already Present"
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewFormName')
    }

    def editFormName() {
        println(" i am editFormName" + params)
        if (session.loginid == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.saveForm != 'null' && params.editform != 'null') {
            FormName formName = FormName.findById(params.editform)
            FormName formName1 = FormName.findByName(params.saveForm)
            if (formName1 == null) {
                formName.name = params.saveForm.trim()
                formName.updation_ip_address = request.getRemoteAddr()
                formName.updation_date = new java.util.Date()
                formName.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
            } else {
                flash.error = "Already Present"
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewFormName')
    }

    def deleteFormName() {
        println(" i am deleteFormName")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        FormName formName = FormName.findById(params.deleteform)
        if (formName == null) {
            render 'Form Name Not Found'
        } else {
            try {
                formName.delete(flush: true, failOnError: true)
                flash.message = "Deleted successfully..."
            } catch (Exception e) {
                flash.error = "Foreign Key Constraint,Cannot Delete.."
            }
        }
        redirect(action: 'addNewFormName')
    }

    def activateFormName() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateFormName :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            FormName formName = FormName.findById(params.formNameId)
            if (formName) {
                if (formName.isActive) {
                    formName.isActive = false
                    formName.updation_username = login.username
                    formName.updation_date = new Date()
                    formName.updation_ip_address = request.getRemoteAddr()
                    formName.save(flush: true, failOnError: true)
                    flash.message = "In-Active Successfully.."
                } else {
                    formName.isActive = true
                    formName.updation_username = login.username
                    formName.updation_date = new Date()
                    formName.updation_ip_address = request.getRemoteAddr()
                    formName.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Form Name Not Found"
            }
            redirect(controller: "inventoryMasters", action: "addNewFormName")
            return
        }
    }

    //master-InvBudgetLevel
    def addNewInvBudgetLevel() {
        println(" i am save add New Inv budget Level")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invBudgetLevelList = InvBudgetLevel.findAllWhere(organization: org)
        [invBudgetLevelList: invBudgetLevelList]
    }

    def saveInvBudgetLevel() {
        println(" i am save saveInvBudgetLevel")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.budgetLevelName != 'null') {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            def invbudgetlevel = InvBudgetLevel.findByNameAndOrganization(params.budgetLevelName.trim(), org)
            if (invbudgetlevel == null) {
                invbudgetlevel = new InvBudgetLevel()
                invbudgetlevel.name = params.budgetLevelName.trim()
                invbudgetlevel.isactive = true
                invbudgetlevel.organization = org
                invbudgetlevel.creation_username = instructor.uid
                invbudgetlevel.updation_username = instructor.uid
                invbudgetlevel.updation_ip_address = request.getRemoteAddr()
                invbudgetlevel.creation_ip_address = request.getRemoteAddr()
                invbudgetlevel.updation_date = new java.util.Date()
                invbudgetlevel.creation_date = new java.util.Date()

                invbudgetlevel.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
            } else {
                flash.error = "Already Present"
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewInvBudgetLevel')
    }

    def editInvBudgetLevel() {
        println(" i am editInvBudgetLevel" + params)
        if (session.loginid == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.budgetLevelName != 'null' && params.budgetLevelId != 'null') {
            InvBudgetLevel invBudgetLevel = InvBudgetLevel.findById(params.budgetLevelId)
            InvBudgetLevel invBudgetLevel1 = InvBudgetLevel.findByName(params.budgetLevelName)
            if (invBudgetLevel1 == null) {
                invBudgetLevel.name = params.budgetLevelName.trim()
                invBudgetLevel.updation_ip_address = request.getRemoteAddr()
                invBudgetLevel.updation_date = new java.util.Date()
                invBudgetLevel.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
            } else {
                flash.error = "Already Present"
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewInvBudgetLevel')
    }

    def deleteInvBudgetLevel() {
        println(" i am deleteInvBudgetLevel")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvBudgetLevel invBudgetLevel = InvBudgetLevel.findById(params.budgetLevelId)
        if (invBudgetLevel == null) {
            render 'Budget Level Not Found'
        } else {
            try {
                invBudgetLevel.delete(flush: true, failOnError: true)
                flash.message = "Deleted successfully..."
            } catch (Exception e) {
                flash.error = "Foreign Key Constraint,Cannot Delete.."
            }
        }
        redirect(action: 'addNewInvBudgetLevel')
    }

    def activateInvBudgetLevel() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateInvBudgetLevel :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvBudgetLevel invBudgetLevel = InvBudgetLevel.findById(params.invBudgetLevelId)
            if (invBudgetLevel) {
                if (invBudgetLevel.isactive) {
                    invBudgetLevel.isactive = false
                    invBudgetLevel.updation_username = login.username
                    invBudgetLevel.updation_date = new Date()
                    invBudgetLevel.updation_ip_address = request.getRemoteAddr()
                    invBudgetLevel.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    invBudgetLevel.isactive = true
                    invBudgetLevel.updation_username = login.username
                    invBudgetLevel.updation_date = new Date()
                    invBudgetLevel.updation_ip_address = request.getRemoteAddr()
                    invBudgetLevel.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Budget Level Not found"
            }
            redirect(controller: "inventoryMasters", action: "addNewInvBudgetLevel")
            return
        }
    }

    //master-InvTransferLevel
    def addNewInvTransferLevel() {
        println(" i am add New Inv transfer Level")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invTransferLevelList = InvTransferLevel.findAllWhere(organization: org)
        [invTransferLevelList: invTransferLevelList]
    }

    def saveInvTransferLevel() {
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        println " i am save saveInvTransferLevel" + params
        if (params.transferLevelName != 'null') {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            def invtransferlevel = InvTransferLevel.findByNameAndOrganization(params.transferLevelName.trim(), org)
            if (invtransferlevel == null) {
                invtransferlevel = new InvTransferLevel()
                invtransferlevel.name = params.transferLevelName.trim()
                invtransferlevel.isactive = true
                invtransferlevel.organization = org
                invtransferlevel.creation_username = instructor.uid
                invtransferlevel.updation_username = instructor.uid
                invtransferlevel.updation_ip_address = request.getRemoteAddr()
                invtransferlevel.creation_ip_address = request.getRemoteAddr()
                invtransferlevel.updation_date = new java.util.Date()
                invtransferlevel.creation_date = new java.util.Date()

                invtransferlevel.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
            } else {
                flash.error = "Already Present"
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewInvTransferLevel')
    }

    def editInvTransferLevel() {
        println(" i am editInvTransferLevel" + params)
        if (session.loginid == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.budgetLevelName != 'null' && params.budgetLevelId != 'null') {
            def invTransferLevel = InvTransferLevel.findById(params.transferLevelId)
            def invTransferLevel1 = InvTransferLevel.findByName(params.transferLevelName)
            if (invTransferLevel1 == null) {
                invTransferLevel.name = params.transferLevelName.trim()
                invTransferLevel.updation_ip_address = request.getRemoteAddr()
                invTransferLevel.updation_date = new java.util.Date()
                invTransferLevel.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
            } else {
                flash.error = "Already Present"
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewInvTransferLevel')
    }

    def deleteInvTransferLevel() {
        println(" i am deleteInvTransferLevel")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        def invTransferLevel = InvTransferLevel.findById(params.transferLevelId)
        if (invTransferLevel == null) {
            render 'Transfer Level Not Found'
        } else {
            try {
                invTransferLevel.delete(flush: true, failOnError: true)
                flash.message = "Deleted successfully..."
            } catch (Exception e) {
                flash.error = "Foreign Key Constraint,Cannot Delete.."
            }
        }
        redirect(action: 'addNewInvTransferLevel')
    }

    def activateInvTransferLevel() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateInvTransferLevel :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invTransferLevel = InvTransferLevel.findById(params.invTransferLevelId)
            if (invTransferLevel) {
                if (invTransferLevel.isactive) {
                    invTransferLevel.isactive = false
                    invTransferLevel.updation_username = login.username
                    invTransferLevel.updation_date = new Date()
                    invTransferLevel.updation_ip_address = request.getRemoteAddr()
                    invTransferLevel.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                } else {
                    invTransferLevel.isactive = true
                    invTransferLevel.updation_username = login.username
                    invTransferLevel.updation_date = new Date()
                    invTransferLevel.updation_ip_address = request.getRemoteAddr()
                    invTransferLevel.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Budget Level Not found"
            }
            redirect(controller: "inventoryMasters", action: "addNewInvTransferLevel")
            return
        }
    }

    //master-InvBudgetType
    def addBudgetType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addBudgetType : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def budgettypeList = InvBudgetType.findAllByOrganization(org)

            [budgettypelist: budgettypeList]
        }
    }

    def saveBudgetType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In save Budget Type : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.budgetType == 'null') {
                flash.error = "Please add a budget type...."
            }

            InvBudgetType invBudgetType = InvBudgetType.findByName(params.budgetTypeName)
            if (invBudgetType == null) {
                invBudgetType = new InvBudgetType()
                invBudgetType.name = params.budgetTypeName
                if (params.isactive == 'on') {
                    invBudgetType.isactive = true
                } else {
                    invBudgetType.isactive = false
                }
                invBudgetType.organization = org

                invBudgetType.creation_username = instructor.uid
                invBudgetType.updation_username = instructor.uid
                invBudgetType.updation_ip_address = request.getRemoteAddr()
                invBudgetType.creation_ip_address = request.getRemoteAddr()

                invBudgetType.updation_date = new java.util.Date()
                invBudgetType.creation_date = new java.util.Date()

                invBudgetType.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addBudgetType')
            } else {

                flash.error = "Budget Type already Exists..."
                redirect(action: 'addBudgetType')
            }
        }

    }

    def editBudgetType() {
        println(" i am editBudgetType")
        if (session.user == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization
        InvBudgetType invBudgetTypeDuplicate = InvBudgetType.findByName(params.budgetTypeName)
        InvBudgetType invBudgetType = InvBudgetType.findById(params.budgetType)
        if (invBudgetTypeDuplicate == null) {
            /*
            if(params.isactive=='on')
            {
                invBudgetType.isactive=true
            }
            else
            {
                invBudgetType.isactive=false
            }*/
            invBudgetType.name = params.budgetTypeName
            invBudgetType.organization = org
            invBudgetType.creation_username = login.username
            invBudgetType.updation_username = login.username
            invBudgetType.updation_ip_address = request.getRemoteAddr()
            invBudgetType.creation_ip_address = request.getRemoteAddr()

            invBudgetType.updation_date = new java.util.Date()
            invBudgetType.creation_date = new java.util.Date()

            invBudgetType.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"

        } else {
            flash.error = "Budget Type Already Present..."
        }
        redirect(action: 'addBudgetType')
    }

    def activateBudgetType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvinstructorfinanceapprovingauthority :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvBudgetType invBudgetType = InvBudgetType.findById(params.budgetType)
            if (invBudgetType) {
                if (invBudgetType.isactive) {
                    invBudgetType.isactive = false
                    invBudgetType.updation_username = login.username
                    invBudgetType.updation_date = new java.util.Date()
                    invBudgetType.updation_ip_address = request.getRemoteAddr()
                    invBudgetType.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                } else {
                    invBudgetType.isactive = true
                    invBudgetType.updation_username = login.username
                    invBudgetType.updation_date = new java.util.Date()
                    invBudgetType.updation_ip_address = request.getRemoteAddr()
                    invBudgetType.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully.."
                }
            } else {
                flash.error = "Inventory Budget type  Not found"
            }
            redirect(controller: "inventoryMasters", action: "addBudgetType")
            return
        }
    }

    def deleteBudgetType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("delete Budget Type:: " + params)
            InvBudgetType invBudgetType = InvBudgetType.findById(params.budgetType)
            if (invBudgetType) {
                try {
                    invBudgetType.delete(flush: true, failOnError: true)
                    flash.message = "Deleted successfully..."
                } catch (Exception e) {
                    flash.error = "Foreign Key Constraint,Cannot Delete.."
                }
            } else {
                flash.error = "Budget Type Not found"
            }
            redirect(controller: "inventoryMasters", action: "addBudgetType")
            return
        }
    }

    //master-InvApprovalStatus
    def addNewInvApprovalStatus() {
        println(" i am save add New Inv ApprovalStatus")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invApprovalStatusList = InvApprovalStatus.findAllWhere(organization: org)
        [invApprovalStatusList: invApprovalStatusList]
    }

    def saveInvApprovalStatus() {
        println(" i am save InvApprovalStatus" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.approvalStatusName != 'null') {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            def invApprovalStatus = InvApprovalStatus.findByName(params.approvalStatusName)
            if (invApprovalStatus == null) {
                invApprovalStatus = new InvApprovalStatus()
                invApprovalStatus.name = params.approvalStatusName
                if (params.isactive == 'on') {
                    invApprovalStatus.isactive = true
                } else {
                    invApprovalStatus.isactive = false
                }
                invApprovalStatus.organization = org
                invApprovalStatus.creation_username = instructor.uid
                invApprovalStatus.updation_username = instructor.uid
                invApprovalStatus.updation_ip_address = request.getRemoteAddr()
                invApprovalStatus.creation_ip_address = request.getRemoteAddr()
                invApprovalStatus.updation_date = new java.util.Date()
                invApprovalStatus.creation_date = new java.util.Date()

                invApprovalStatus.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
            } else {
                flash.error = "Approval Status already Present"
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewInvApprovalStatus')
    }

    def editInvApprovalStatus() {
        println(" i am editInvApprovalStatus  " + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.approvalStatusName != 'null') {
            InvApprovalStatus invApprovalStatus = InvApprovalStatus.findById(params.approvalStatusId)
            InvApprovalStatus invApprovalStatus1 = InvApprovalStatus.findByName(params.approvalStatusName)
            if (invApprovalStatus1 == null) {
                invApprovalStatus.name = params.approvalStatusName.trim()
                if (params.isactive == 'on') {
                    invApprovalStatus.isactive = true
                } else {
                    invApprovalStatus.isactive = false
                }
                invApprovalStatus.updation_ip_address = request.getRemoteAddr()
                invApprovalStatus.updation_date = new java.util.Date()
                invApprovalStatus.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
            } else {
                flash.error = 'Duplicate Entry'
            }
        } else {
            flash.error = "Please fill all details"
        }
        redirect(action: 'addNewInvApprovalStatus')
    }

    def deleteInvApprovalStatus() {
        println(" i am deleteInvApprovalStatus")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvApprovalStatus invApprovalStatus = InvApprovalStatus.findById(params.approvalstatusid)
        if (invApprovalStatus == null) {
            render 'Approval Status Not Found'
        } else {
            try {
                invApprovalStatus.delete(flush: true, failOnError: true)
                flash.message = "Deleted Successfully"
            } catch (Exception e) {
                flash.error = "Foreign Key Constraint,Cannot Delete.."
            }
        }
        redirect(action: 'addNewInvApprovalStatus')
    }

    def activateInvApprovalStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateInvApprovalStatus :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvApprovalStatus invApprovalStatus = InvApprovalStatus.findById(params.invApprovalStatusId)
            if (invApprovalStatus) {
                if (invApprovalStatus.isactive) {
                    invApprovalStatus.isactive = false
                    invApprovalStatus.updation_username = login.username
                    invApprovalStatus.updation_date = new Date()
                    invApprovalStatus.updation_ip_address = request.getRemoteAddr()
                    invApprovalStatus.save(flush: true, failOnError: true)
                    flash.message = "Successful.."
                } else {
                    invApprovalStatus.isactive = true
                    invApprovalStatus.updation_username = login.username
                    invApprovalStatus.updation_date = new Date()
                    invApprovalStatus.updation_ip_address = request.getRemoteAddr()
                    invApprovalStatus.save(flush: true, failOnError: true)
                    flash.message = "Successful.."
                }
            } else {
                flash.error = "Approval Status Not found"
            }
            redirect(controller: "inventoryMasters", action: "addNewInvApprovalStatus")
            return
        }
    }

    //master-InvMaterialPart
    def addNewInvMaterialPart() {
        println(" i am addNewInvMaterialPart")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invMaterialPartList = InvMaterialPart.findAllWhere(organization: org)
        def invMaterialList = InvMaterial.findAllWhere(organization: org, isactive: true)
        [invMaterialPartList: invMaterialPartList, invMaterialList: invMaterialList]
    }

    def saveInvMaterialPart() {
        println(" i am save InvMaterialPart" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

        InvMaterial invMaterial1 = InvMaterial.findById(params.invMaterialId)
        InvMaterialPart invMaterialPart = InvMaterialPart.findByNameAndInvmaterial(params.materialPartName.trim(), invMaterial1)
        println("invMaterialPart : " + invMaterialPart)
        if (invMaterialPart == null) {
            invMaterialPart = new InvMaterialPart()
            invMaterialPart.name = params.materialPartName.trim()
            if (params.isactive == 'on') {
                invMaterialPart.isactive = true
            } else {
                invMaterialPart.isactive = false
            }
            invMaterialPart.specification = params.specifications.trim()
            invMaterialPart.code = params.materialcode
            def invMaterial = InvMaterial.findById(params.invMaterialId)
            invMaterialPart.invmaterial = invMaterial
            invMaterialPart.organization = org
            invMaterialPart.creation_username = instructor.uid
            invMaterialPart.updation_username = instructor.uid
            invMaterialPart.updation_ip_address = request.getRemoteAddr()
            invMaterialPart.creation_ip_address = request.getRemoteAddr()
            invMaterialPart.updation_date = new java.util.Date()
            invMaterialPart.creation_date = new java.util.Date()

            invMaterialPart.save(flush: true, failOnError: true)
            flash.message = "Saved Successfully"
            redirect(action: 'addNewInvMaterialPart')
        } else {
            /* render invMaterialPart +
                    "     alert(\"Material part Already Exists... Edit Name Only!!\")\n" +
                    "history.back();\n" +
                    "  </script>"*/
            flash.message = "Material part Already Exists... Edit Name Only!!"
            redirect(action: 'addNewInvMaterialPart')
        }
    }

    def editInvMaterialPart() {
        println(" i am editInvMaterialPart" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvMaterial invMaterial1 = InvMaterial.findById(params.invMaterialId)
        InvMaterialPart invMaterialPart1 = InvMaterialPart.findByNameAndInvmaterialAndCodeAndSpecification(params.materialPartName.trim(), invMaterial1, params.materialcode, params.specifications)
        InvMaterialPart invMaterialPart = InvMaterialPart.findById(params.materialPartId)
        if (invMaterialPart1 == null) {
            invMaterialPart.name = params.materialPartName.trim()
            invMaterialPart.specification = params.specifications.trim()
            invMaterialPart.code = params.materialcode.trim()
            def invMaterial = InvMaterial.findById(params.invMaterialId)
            if (invMaterial) {
                invMaterialPart.invmaterial = invMaterial
            }
            invMaterialPart.updation_ip_address = request.getRemoteAddr()
            invMaterialPart.updation_date = new java.util.Date()
            invMaterialPart.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
            redirect(action: 'addNewInvMaterialPart')
        } else {
            flash.message = "Already Exists"
            redirect(action: 'addNewInvMaterialPart')
        }
    }

    def deleteInvMaterialPart() {
        println(" i am deleteInvMaterialPart" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvMaterialPart invMaterialPart = InvMaterialPart.findById(params.materialPartId)
        if (invMaterialPart == null) {
            flash.error = "Material Part Not Found"
        } else {
            try {
                invMaterialPart.delete(flush: true, failOnError: true)
                flash.message = "Deleted Successfully"
            } catch (Exception e) {
                flash.error = "Foreign Key Constraint,Cannot Delete.."
            }

        }
        redirect(action: 'addNewInvMaterialPart')
    }

    def activateInvMaterialPart() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateInvMaterialPart :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvMaterialPart invMaterialPart = InvMaterialPart.findById(params.invMaterialPartId)
            if (invMaterialPart) {
                if (invMaterialPart.isactive) {
                    invMaterialPart.isactive = false
                    invMaterialPart.updation_username = login.username
                    invMaterialPart.updation_date = new Date()
                    invMaterialPart.updation_ip_address = request.getRemoteAddr()
                    invMaterialPart.save(flush: true, failOnError: true)
                    flash.message = "In-Active Successfully.."
                } else {
                    invMaterialPart.isactive = true
                    invMaterialPart.updation_username = login.username
                    invMaterialPart.updation_date = new Date()
                    invMaterialPart.updation_ip_address = request.getRemoteAddr()
                    invMaterialPart.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Material Part Not found"
            }
            redirect(controller: "inventoryMasters", action: "addNewInvMaterialPart")
        }
    }

    //master-InvMaterialType
    def addNewInvMaterialType() {
        println(" i am addNewMaterialType")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invMaterialTypeList = InvMaterialType.findAllWhere(organization: org)
        [invMaterialTypeList: invMaterialTypeList]
    }

    def saveInvMaterialType() {
        println(" i am save saveInvMaterialType" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.materialTypeName != 'null') {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            InvMaterialType invMaterialType = InvMaterialType.findByName(params.materialTypeName.trim())
            if (invMaterialType == null) {
                invMaterialType = new InvMaterialType()
                invMaterialType.name = params.materialTypeName.trim()
                invMaterialType.isactive = true
                invMaterialType.organization = org
                if (params.isactive == 'on') {
                    invMaterialType.isactive = true
                } else {
                    invMaterialType.isactive = false
                }
                invMaterialType.creation_username = instructor.uid
                invMaterialType.updation_username = instructor.uid
                invMaterialType.updation_ip_address = request.getRemoteAddr()
                invMaterialType.creation_ip_address = request.getRemoteAddr()
                invMaterialType.updation_date = new java.util.Date()
                invMaterialType.creation_date = new java.util.Date()
                invMaterialType.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
            } else {
                flash.error = "Already Present"
            }
        } else {
            flash.error = "Please fill all details"
        }
        redirect(action: 'addNewInvMaterialType')
    }

    def editInvMaterialType() {
        println(" i am editInvMaterialType")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.materialTypeName != 'null' && params.materialtypeId != 'null') {
            InvMaterialType invMaterialType = InvMaterialType.findById(params.materialtypeId)
            InvMaterialType invMaterialType1 = InvMaterialType.findByName(params.materialTypeName)
            if (invMaterialType1 == null) {
                invMaterialType.name = params.materialTypeName.trim()
                if (params.isactive == 'on') {
                    invMaterialType.isactive = true
                } else {
                    invMaterialType.isactive = false
                }
                invMaterialType.updation_ip_address = request.getRemoteAddr()
                invMaterialType.updation_date = new java.util.Date()
                invMaterialType.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
            } else {
                flash.error = 'Duplicate Entry'
            }
        } else {
            flash.error = "Please fill all details"
        }
        redirect(action: 'addNewInvMaterialType')
    }

    def deleteInvMaterialType() {
        println(" i am deleteInvMaterialType" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvMaterialType invMaterialType = InvMaterialType.findById(params.materialTypeId)
        if (invMaterialType == null) {
            flash.error = "Material Type Not Found"
        } else {
            try {
                invMaterialType.delete(flush: true, failOnError: true)
                flash.message = "Deleted Successfully"
            } catch (Exception e) {
                flash.error = "Foreign Key Constraint,Cannot Delete.."
            }

        }
        redirect(action: 'addNewInvMaterialType')
    }

    def activateInvMaterialType() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateInvMaterialType :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvMaterialType invMaterialType = InvMaterialType.findById(params.invMaterialTypeId)
            if (invMaterialType) {
                if (invMaterialType.isactive) {
                    invMaterialType.isactive = false
                    invMaterialType.updation_username = login.username
                    invMaterialType.updation_date = new Date()
                    invMaterialType.updation_ip_address = request.getRemoteAddr()
                    invMaterialType.save(flush: true, failOnError: true)
                    flash.message = "Successful.."
                } else {
                    invMaterialType.isactive = true
                    invMaterialType.updation_username = login.username
                    invMaterialType.updation_date = new Date()
                    invMaterialType.updation_ip_address = request.getRemoteAddr()
                    invMaterialType.save(flush: true, failOnError: true)
                    flash.message = "Successful.."
                }
            } else {
                flash.error = "Material Type Not found"
            }
            redirect(controller: "inventoryMasters", action: "addNewInvMaterialType")
            return
        }
    }

    //master-InvMaterialSet
    def addNewInvMaterialSet() {
        println(" i am addNewInvMaterialSet" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invMaterialSetList = InvMaterialSet.findAllWhere(organization: org)
        println("invMaterialSetList :: " + invMaterialSetList)
        [invMaterialSetList: invMaterialSetList]
    }

    def saveInvMaterialSet() {
        println(" i am save saveInvMaterialSet" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invMaterialSet = InvMaterialSet.findByName(params.materialSetName.trim())
        if (invMaterialSet == null) {
            invMaterialSet = new InvMaterialSet()
            invMaterialSet.name = params.materialSetName.trim()
            invMaterialSet.isactive = true
            invMaterialSet.organization = org
            invMaterialSet.creation_username = instructor.uid
            invMaterialSet.updation_username = instructor.uid
            invMaterialSet.updation_ip_address = request.getRemoteAddr()
            invMaterialSet.creation_ip_address = request.getRemoteAddr()
            invMaterialSet.updation_date = new java.util.Date()
            invMaterialSet.creation_date = new java.util.Date()

            invMaterialSet.save(flush: true, failOnError: true)
            flash.message = "Saved Successfully"
            redirect(action: 'addNewInvMaterialSet')
        } else {
            flash.error = "Material-Set with the name you provided already exists"
            redirect(action: 'addNewInvMaterialSet')
        }
    }

    def editInvMaterialSet() {
        println(" i am editInvMaterialSet" + params)
        if (session.loginid == null)
            redirect(controller: "login", action: 'erplogin')
        InvMaterialSet invMaterialSet = InvMaterialSet.findById(params.materialSetId)
        //println"invMaterialSet------>"+invMaterialSet
        if (invMaterialSet != null) {
            InvMaterialSet invMaterialSameSet = InvMaterialSet.findByName(params.materialSetName)
            println("invMaterialSameSet" + invMaterialSameSet)
            if (invMaterialSameSet != null) {
                flash.error = "Material-Set with same name already exists,please select a different name while editing"
            } else {
                invMaterialSet.name = params.materialSetName.trim()
                /*if(params.isactive == 'on') {
                    invMaterialSet.isactive = true
                } else {
                    invMaterialSet.isactive = false
                }*/
                invMaterialSet.updation_ip_address = request.getRemoteAddr()
                invMaterialSet.updation_date = new java.util.Date()
                invMaterialSet.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
            }

        } else {
            flash.error = "Record Not Found"
        }
        redirect(action: 'addNewInvMaterialSet')
    }

    def deleteInvMaterialSet() {
        println(" i am deleteInvMaterialSet" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvMaterialSet invMaterialSet = InvMaterialSet.findById(params.materialSetId)
        if (invMaterialSet == null) {
            flash.error = "Material Set Not Found"
        } else {
            try {
                invMaterialSet.delete(flush: true, failOnError: true)
                flash.message = "Deleted Successfully"
            } catch (Exception e) {
                flash.error = "Foreign Key Constraint,Cannot Delete."
            }
        }
        redirect(action: 'addNewInvMaterialSet')
    }

    def activateInvMaterialSet() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activate Inventory Material Set :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvMaterialSet invMaterialSet = InvMaterialSet.findById(params.invMaterialSetParam)
            if (invMaterialSet) {
                if (invMaterialSet.isactive) {
                    invMaterialSet.isactive = false
                    invMaterialSet.updation_username = login.username
                    invMaterialSet.updation_date = new java.util.Date()
                    invMaterialSet.updation_ip_address = request.getRemoteAddr()
                    invMaterialSet.save(flush: true, failOnError: true)
                    flash.message = "In-Active Successfully.."
                } else {
                    invMaterialSet.isactive = true
                    invMaterialSet.updation_username = login.username
                    invMaterialSet.updation_date = new java.util.Date()
                    invMaterialSet.updation_ip_address = request.getRemoteAddr()
                    invMaterialSet.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Inventory Material Set  Not found"
            }
            redirect(controller: "inventoryMasters", action: "addNewInvMaterialSet")
            return
        }
    }

    //master-InvFinanceApprovingCategory
    def addInventoryFinanceApprovingCategoy() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("I am in addInventoryFinanceAprrovingCategoy : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invFinanceApprovingCategory = InvFinanceApprovingCategory.findAllByOrganization(org)

            [invFinanceApprovingCategory: invFinanceApprovingCategory]
        }
    }

    def saveInventoryFinanceApprovingCategoy() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In save Inventory Finance Approving Category : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (!params.invfinanceapprovingcatname.isEmpty()) {
                InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByName(params.invfinanceapprovingcatname)
                if (invFinanceApprovingCategory == null) {
                    invFinanceApprovingCategory = new InvFinanceApprovingCategory()
                    invFinanceApprovingCategory.name = params.invfinanceapprovingcatname
                    invFinanceApprovingCategory.isactive = true
                    invFinanceApprovingCategory.organization = org
                    invFinanceApprovingCategory.creation_username = instructor.uid
                    invFinanceApprovingCategory.updation_username = instructor.uid
                    invFinanceApprovingCategory.updation_ip_address = request.getRemoteAddr()
                    invFinanceApprovingCategory.creation_ip_address = request.getRemoteAddr()
                    invFinanceApprovingCategory.updation_date = new java.util.Date()
                    invFinanceApprovingCategory.creation_date = new java.util.Date()
                    invFinanceApprovingCategory.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addInventoryFinanceApprovingCategoy')
                } else {

                    flash.error = "Category with the name you provided already exists"
                    redirect(action: 'addInventoryFinanceApprovingCategoy')
                }
            } else {
                flash.error = "Please add a Category name...."
                redirect(action: 'addInventoryFinanceApprovingCategoy')
            }
        }

    }

    def editInventoryFinanceApprovingCategoy() {
        println(" i am editInventoryFinanceApprovingCategoy" + params)
        if (session.user == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization
        InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findById(params.invfinanceapprovingcat)
        if (invFinanceApprovingCategory != null) {
            InvFinanceApprovingCategory invFinanceApprovingSameCategory = InvFinanceApprovingCategory.findByName(params.invfinanceapprovingcatname)
            if (invFinanceApprovingSameCategory?.name == params.invfinanceapprovingcatname) {
                flash.error = "Category with same name already exists,please select a different name while editing"
                redirect(action: 'addInventoryFinanceApprovingCategoy')
            } else {
                invFinanceApprovingCategory.name = params.invfinanceapprovingcatname
                invFinanceApprovingCategory.organization = org
                invFinanceApprovingCategory.creation_date = new java.util.Date()
                invFinanceApprovingCategory.updation_date = new java.util.Date()
                invFinanceApprovingCategory.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
                redirect(action: 'addInventoryFinanceApprovingCategoy')
            }

        } else {

            flash.error = "Mandatory fields are missing..."
            redirect(action: 'addInventoryFinanceApprovingCategoy')
        }
    }

    def activateInventoryFinanceApprovingCategoy() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateInventoryFinanceApprovingCategoy :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findById(params.invfinanceapprovingcat)
            if (invFinanceApprovingCategory) {
                if (invFinanceApprovingCategory.isactive) {
                    invFinanceApprovingCategory.isactive = false
                    invFinanceApprovingCategory.updation_username = login.username
                    invFinanceApprovingCategory.updation_date = new java.util.Date()
                    invFinanceApprovingCategory.updation_ip_address = request.getRemoteAddr()
                    invFinanceApprovingCategory.save(flush: true, failOnError: true)
                    flash.message = "In-Active Successfully.."
                } else {
                    invFinanceApprovingCategory.isactive = true
                    invFinanceApprovingCategory.updation_username = login.username
                    invFinanceApprovingCategory.updation_date = new java.util.Date()
                    invFinanceApprovingCategory.updation_ip_address = request.getRemoteAddr()
                    invFinanceApprovingCategory.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Inventory Finance Approving Category Not found"
            }
            redirect(controller: "inventoryMasters", action: "addInventoryFinanceApprovingCategoy")
            return
        }
    }

    def deleteInventoryFinanceApprovingCategoy() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("delete deleteInventoryFinanceApprovingCategoy :: " + params)
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findById(params.invfinanceapprovingcat)
            if (invFinanceApprovingCategory) {
                try {
                    invFinanceApprovingCategory.delete(flush: true, failOnError: true)
                    flash.message = "Deleted successfully..."
                } catch (Exception e) {
                    flash.error = "Foriegn Key Constraint,Cannot Delete .."
                }
            } else {
                flash.error = "Inventory Finance Aprroving Categor Type Not found"
            }

            redirect(controller: "inventoryMasters", action: "addInventoryFinanceApprovingCategoy")
            return
        }
    }

    //master-InvFinanceApprovingAuthority
    def addinvfinanceapprovingauthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addinvfinanceapprovingauthority : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invfinanceapprovingauthoritylist = InvFinanceApprovingAuthority.findAllByOrganization(org)
            println("invfinanceapprovingauthoritylist" + invfinanceapprovingauthoritylist)
            [invfinanceapprovingauthoritylist: invfinanceapprovingauthoritylist]
        }
    }

    def saveinvfinanceapprovingauthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveinvfinanceapprovingauthority : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.name == 'null') {
                flash.error = "Please Add Finance approving authority..!"
            }

            if (params.name != null) {
                InvFinanceApprovingAuthority approvingAuthority = InvFinanceApprovingAuthority.findById(params.invFinanceApprovingAuthority)
                if (approvingAuthority != null) {
                    InvFinanceApprovingAuthority editFAA = InvFinanceApprovingAuthority.findByOrganizationAndName(org, params.name)
                    if (editFAA) {
                        flash.error = "Already Exists..!"
                    } else {
                        if (params.isactive == 'on') {
                            approvingAuthority.isactive = true
                        } else {
                            approvingAuthority.isactive = false
                        }
                        approvingAuthority.name = params.name
                        approvingAuthority.updation_username = login.username
                        approvingAuthority.updation_date = new Date()
                        approvingAuthority.updation_ip_address = request.getRemoteAddr()
                        approvingAuthority.save(flush: true, failOnError: true)
                        flash.message = "Updation Successfully..!"
                    }
                } else {
                    InvFinanceApprovingAuthority invFinanceApprovingAuthority1 = InvFinanceApprovingAuthority.findByOrganizationAndName(org, params.name)
                    if (invFinanceApprovingAuthority1) {
                        flash.error = "Already Exists ..!"
                    } else {
                        invFinanceApprovingAuthority1 = new InvFinanceApprovingAuthority()
                        if (params.isactive == 'on') {
                            invFinanceApprovingAuthority1.isactive = true
                        } else {
                            invFinanceApprovingAuthority1.isactive = false
                        }
                        invFinanceApprovingAuthority1.name = params.name
                        invFinanceApprovingAuthority1.organization = org
                        invFinanceApprovingAuthority1.creation_username = login.username
                        invFinanceApprovingAuthority1.updation_username = login.username
                        invFinanceApprovingAuthority1.creation_date = new Date()
                        invFinanceApprovingAuthority1.updation_date = new Date()
                        invFinanceApprovingAuthority1.creation_ip_address = request.getRemoteAddr()
                        invFinanceApprovingAuthority1.updation_ip_address = request.getRemoteAddr()
                        invFinanceApprovingAuthority1.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully..!"
                    }
                }
            } else {
                flash.error = "Mandatory fields are missing..!"
            }
            redirect(controller: "inventoryMasters", action: "addinvfinanceapprovingauthority")
            return
        }
    }

    def deleinvfinanceapprovingauthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleinvfinanceapprovingauthority :: " + params)
            InvFinanceApprovingAuthority invFinanceApprovingAuthority = InvFinanceApprovingAuthority.findById(params.invFinanceApprovingAuthority)
            if (invFinanceApprovingAuthority) {
                try {
                    invFinanceApprovingAuthority.delete(flush: true, failOnError: true)
                    flash.message = "Delected successfully..!"
                } catch (Exception e) {
                    flash.error = "Finance Approving Authority can not delete..!"
                }
            } else {
                flash.error = "Instructor Finance Approving Authority Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvfinanceapprovingauthority")
            return
        }
    }

    def activationinvfinanceapprovingauthority() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvfinanceapprovingauthority :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvFinanceApprovingAuthority invFinanceApprovingAuthority = InvFinanceApprovingAuthority.findById(params.invFinanceApprovingAuthority)
            if (invFinanceApprovingAuthority) {
                if (invFinanceApprovingAuthority.isactive) {
                    invFinanceApprovingAuthority.isactive = false
                    invFinanceApprovingAuthority.updation_username = login.username
                    invFinanceApprovingAuthority.updation_date = new Date()
                    invFinanceApprovingAuthority.updation_ip_address = request.getRemoteAddr()
                    invFinanceApprovingAuthority.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully..!"
                } else {
                    invFinanceApprovingAuthority.isactive = true
                    invFinanceApprovingAuthority.updation_username = login.username
                    invFinanceApprovingAuthority.updation_date = new Date()
                    invFinanceApprovingAuthority.updation_ip_address = request.getRemoteAddr()
                    invFinanceApprovingAuthority.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Finance Approving Authority Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvfinanceapprovingauthority")
            return
        }
    }

    //master-InvPurchaseType
    def addinvpurchasetype() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addinvpurchasetype : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invpurchasetype = InvPurchaseType.findAllByOrganization(org)
            [invpurchasetype: invpurchasetype]
        }
    }

    def saveinvpurchasetype() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveinvpurchasetype : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.name == 'null') {
                flash.error = "Please Add Purchase Type..!"
            }

            if (params.name != 'null') {
                InvPurchaseType purchaseType = InvPurchaseType.findById(params.purchaseType)
                if (purchaseType != null) {
                    InvPurchaseType invPurchaseType = InvPurchaseType.findByOrganizationAndName(org, params.name)
                    if (invPurchaseType) {
                        flash.error = "Already Exists ..!"
                    } else {
                        /*if(params.isactive == 'on') {
                            purchaseType.isactive = true
                        } else {
                            purchaseType.isactive = false
                        }*/
                        purchaseType.name = params.name
                        purchaseType.updation_username = login.username
                        purchaseType.updation_date = new Date()
                        purchaseType.updation_ip_address = request.getRemoteAddr()
                        purchaseType.save(flush: true, failOnError: true)
                        flash.message = "Updation Successfully..!"
                    }
                } else {
                    InvPurchaseType invPurchaseType = InvPurchaseType.findByOrganizationAndName(org, params.name)
                    if (invPurchaseType) {
                        flash.error = "Already Exists ..!"
                    } else {
                        invPurchaseType = new InvPurchaseType()
                        if (params.isactive == 'on') {
                            invPurchaseType.isactive = true
                        } else {
                            invPurchaseType.isactive = false
                        }
                        invPurchaseType.name = params.name
                        invPurchaseType.organization = org
                        invPurchaseType.creation_username = login.username
                        invPurchaseType.updation_username = login.username
                        invPurchaseType.creation_date = new Date()
                        invPurchaseType.updation_date = new Date()
                        invPurchaseType.creation_ip_address = request.getRemoteAddr()
                        invPurchaseType.updation_ip_address = request.getRemoteAddr()
                        invPurchaseType.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully..!"
                    }
                }
            } else {
                flash.error = "Mandatory fields are missing..!"
            }
            redirect(controller: "inventoryMasters", action: "addinvpurchasetype")
            return
        }
    }

    def deleinvpurchasetype() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleinvpurchasetype :: " + params)
            InvPurchaseType invPurchaseType = InvPurchaseType.findById(params.invPurchaseType)
            if (invPurchaseType) {
                try {
                    invPurchaseType.delete(flush: true, failOnError: true)
                    flash.message = "Delected successfully..!"
                } catch (Exception e) {
                    flash.error = "Foreign Key Constraint,Cannot Delete.."
                }
            } else {
                flash.error = "Purchase Type Not found"
            }

            redirect(controller: "inventoryMasters", action: "addinvpurchasetype")
            return
        }
    }

    def activationinvpurchasetype() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvpurchasetype :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvPurchaseType invPurchaseType = InvPurchaseType.findById(params.invPurchaseType)
            if (invPurchaseType) {
                if (invPurchaseType.isactive) {
                    invPurchaseType.isactive = false
                    invPurchaseType.updation_username = login.username
                    invPurchaseType.updation_date = new Date()
                    invPurchaseType.updation_ip_address = request.getRemoteAddr()
                    invPurchaseType.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully..!"
                } else {
                    invPurchaseType.isactive = true
                    invPurchaseType.updation_username = login.username
                    invPurchaseType.updation_date = new Date()
                    invPurchaseType.updation_ip_address = request.getRemoteAddr()
                    invPurchaseType.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Purchase Type Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvpurchasetype")
            return
        }
    }

    //master-InvMaterialCategory
    def addinvmaterialcategory() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addinvmaterialcategory : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invmaterialcategory = InvMaterialCategory.findAllByOrganization(org)
            [invmaterialcategory: invmaterialcategory]
        }
    }

    def saveinvmaterialcategory() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveinvmaterialcategory : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.name == 'null') {
                flash.error = "Please Add Material Category..!"
            }

            if (params.name != 'null') {
                InvMaterialCategory invMaterialCategory = InvMaterialCategory.findById(params.invMaterialCategory)
                if (invMaterialCategory != null) {
                    InvMaterialCategory invMaterialCategory1 = InvMaterialCategory.findByOrganizationAndName(org, params.name)
                    if (invMaterialCategory1) {
                        flash.error = "Already Exists ..!"
                    } else {
                        invMaterialCategory.name = params.name
                        invMaterialCategory.updation_username = login.username
                        invMaterialCategory.updation_date = new Date()
                        invMaterialCategory.updation_ip_address = request.getRemoteAddr()
                        invMaterialCategory.save(flush: true, failOnError: true)
                        flash.message = "Updation Successfully..!"
                    }
                } else {
                    InvMaterialCategory invMaterialCategory1 = InvMaterialCategory.findByOrganizationAndName(org, params.name)
                    if (invMaterialCategory1) {
                        flash.error = "Already Exists ..!"
                    } else {
                        invMaterialCategory1 = new InvMaterialCategory()
                        invMaterialCategory1.isactive = true
                        invMaterialCategory1.name = params.name
                        invMaterialCategory1.organization = org
                        invMaterialCategory1.creation_username = login.username
                        invMaterialCategory1.updation_username = login.username
                        invMaterialCategory1.creation_date = new Date()
                        invMaterialCategory1.updation_date = new Date()
                        invMaterialCategory1.creation_ip_address = request.getRemoteAddr()
                        invMaterialCategory1.updation_ip_address = request.getRemoteAddr()
                        invMaterialCategory1.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully..!"
                    }
                }
            } else {
                flash.error = "Mandatory fields are missing..!"
            }
            redirect(controller: "inventoryMasters", action: "addinvmaterialcategory")
            return
        }
    }

    def deleinvmaterialcategory() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleinvmaterialcategory :: " + params)
            InvMaterialCategory invMaterialCategory = InvMaterialCategory.findById(params.invMaterialCategory)
            if (invMaterialCategory) {
                try {
                    invMaterialCategory.delete(flush: true, failOnError: true)
                    flash.message = "Delected successfully..!"
                } catch (Exception e) {
                    flash.error = "Material Category can not delete..!"
                }
            } else {
                flash.error = "Material Category Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvmaterialcategory")
            return
        }
    }

    def activationinvmaterialcategory() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvmaterialcategory :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvMaterialCategory invMaterialCategory = InvMaterialCategory.findById(params.invMaterialCategory)
            if (invMaterialCategory) {
                if (invMaterialCategory.isactive) {
                    invMaterialCategory.isactive = false
                    invMaterialCategory.updation_username = login.username
                    invMaterialCategory.updation_date = new Date()
                    invMaterialCategory.updation_ip_address = request.getRemoteAddr()
                    invMaterialCategory.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully..!"
                } else {
                    invMaterialCategory.isactive = true
                    invMaterialCategory.updation_username = login.username
                    invMaterialCategory.updation_date = new Date()
                    invMaterialCategory.updation_ip_address = request.getRemoteAddr()
                    invMaterialCategory.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Material Category Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvmaterialcategory")
            return
        }
    }

    //master-InvMaterial
    def addinvmaterial() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addinvmaterial : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invmaterial = InvMaterial.findAllByOrganization(org)
            //println("invmaterial"+invmaterial)
            def invmaterialtype = InvMaterialType.findAllByOrganizationAndIsactive(org, true)
            //println("invmaterialtype"+invmaterialtype)
            def invmaterialcategory = InvMaterialCategory.findAllByOrganizationAndIsactive(org, true)
            //println("invmaterialcategory"+invmaterialcategory)
            def invmaterialset = InvMaterialSet.findAllByOrganizationAndIsactive(org, true)
            //println("invmaterialset"+invmaterialset)
            [invmaterial: invmaterial, invmaterialtype: invmaterialtype, invmaterialcategory: invmaterialcategory, invmaterialset: invmaterialset]
        }
    }

    def saveinvmaterial() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveinvmaterial : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.invmaterialtype == null) {
                flash.error = "Please Select Material Type..!"
            }
            if (params.invmaterialcategory == null) {
                flash.error = "Please Select Material Category..!"
            }
            if (params.invmaterialset == null) {
                flash.error = "Please Select Material Set..!"
            }

            if (params.invmaterialtype && params.invmaterialcategory && params.invmaterialset != 'null') {
                InvMaterialType invMaterialType = InvMaterialType.findById(params.invmaterialtype)
                InvMaterialCategory invMaterialCategory = InvMaterialCategory.findById(params.invmaterialcategory)
                InvMaterialSet invMaterialSet = InvMaterialSet.findById(params.invmaterialset)
                InvMaterial invMaterial = InvMaterial.findById(params.invmaterial)
                if (invMaterial != null) {
                    InvMaterial invMaterial1 = InvMaterial.findByOrganizationAndNameAndCodeAndInvmaterialtypeAndInvmaterialcategoryAndInvmaterialset(org, params.name, params.code, invMaterialType, invMaterialCategory, invMaterialSet)
                    if (invMaterial1) {
                        flash.error = "Already Exists ..!"
                    } else {
                        /*if(params.isactive == 'on') {
                            invMaterial.isactive = true
                        } else {
                            invMaterial.isactive = false
                        }*/
                        invMaterial.name = params.name
                        invMaterial.specification = params.spec
                        invMaterial.code = params.code
                        invMaterial.invmaterialtype = invMaterialType
                        invMaterial.invmaterialcategory = invMaterialCategory
                        invMaterial.invmaterialset = invMaterialSet
                        invMaterial.organization = org
                        invMaterial.updation_username = login.username
                        invMaterial.updation_date = new Date()
                        invMaterial.updation_ip_address = request.getRemoteAddr()
                        invMaterial.save(flush: true, failOnError: true)
                        flash.message = "Updation Successfully..!"
                    }
                } else {
                    InvMaterial invMaterial1 = InvMaterial.findByOrganizationAndNameAndCodeAndInvmaterialtypeAndInvmaterialcategoryAndInvmaterialset(org, params.name, params.code, invMaterialType, invMaterialCategory, invMaterialSet)
                    if (invMaterial1) {
                        flash.error = "Already Exists ..!"
                    } else {
                        invMaterial1 = new InvMaterial()
                        if (params.isactive == 'on') {
                            invMaterial1.isactive = true
                        } else {
                            invMaterial1.isactive = false
                        }
                        invMaterial1.name = params.name
                        invMaterial1.specification = params.spec
                        invMaterial1.code = params.code
                        invMaterial1.invmaterialtype = invMaterialType
                        invMaterial1.invmaterialcategory = invMaterialCategory
                        invMaterial1.invmaterialset = invMaterialSet
                        invMaterial1.organization = org
                        invMaterial1.creation_username = login.username
                        invMaterial1.updation_username = login.username
                        invMaterial1.creation_date = new Date()
                        invMaterial1.updation_date = new Date()
                        invMaterial1.creation_ip_address = request.getRemoteAddr()
                        invMaterial1.updation_ip_address = request.getRemoteAddr()
                        invMaterial1.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully..!"
                    }
                }
            } else {
                flash.error = "Mandatory fields are missing..!"
            }
            redirect(controller: "inventoryMasters", action: "addinvmaterial")
            return
        }
    }

    def deleinvmaterial() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleinvmaterial :: " + params)
            InvMaterial invMaterial = InvMaterial.findById(params.invMaterial)
            if (invMaterial) {
                try {
                    invMaterial.delete(flush: true, failOnError: true)
                    flash.message = "Delected successfully..!"
                } catch (Exception e) {
                    flash.error = "Foreign Key Constraint,Cannot Delete.."
                }
            } else {
                flash.error = "Material Not found"
            }

            redirect(controller: "inventoryMasters", action: "addinvmaterial")
            return
        }
    }

    def activationinvmaterial() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvmaterial :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvMaterial invMaterial = InvMaterial.findById(params.invMaterial)
            if (invMaterial) {
                if (invMaterial.isactive) {
                    invMaterial.isactive = false
                    invMaterial.updation_username = login.username
                    invMaterial.updation_date = new Date()
                    invMaterial.updation_ip_address = request.getRemoteAddr()
                    invMaterial.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully..!"
                } else {
                    invMaterial.isactive = true
                    invMaterial.updation_username = login.username
                    invMaterial.updation_date = new Date()
                    invMaterial.updation_ip_address = request.getRemoteAddr()
                    invMaterial.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Material Category Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvmaterial")
            return
        }
    }

    //master-InvPaymentMethod
    def addNewInvPaymentMethod() {
        println(" i am addNewInvPaymentMethod")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invPaymentMethodList = InvPaymentMethod.findAllWhere(organization: org)
        [invPaymentMethodList: invPaymentMethodList]
    }

    def saveInvPaymentMethod() {
        println(" i am save saveInvPaymentMethod" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        InvPaymentMethod invPaymentMethod = InvPaymentMethod.findByName(params.paymentMethodName.trim())
        if (invPaymentMethod == null) {
            invPaymentMethod = new InvPaymentMethod()
            invPaymentMethod.name = params.paymentMethodName.trim()
            invPaymentMethod.isactive = true
            invPaymentMethod.organization = org
            if (params.isactive == 'on') {
                invPaymentMethod.isactive = true
            } else {
                invPaymentMethod.isactive = false
            }
            invPaymentMethod.creation_username = instructor.uid
            invPaymentMethod.updation_username = instructor.uid
            invPaymentMethod.updation_ip_address = request.getRemoteAddr()
            invPaymentMethod.creation_ip_address = request.getRemoteAddr()
            invPaymentMethod.updation_date = new java.util.Date()
            invPaymentMethod.creation_date = new java.util.Date()

            invPaymentMethod.save(flush: true, failOnError: true)
            flash.message = "Saved Successfully"
        } else {
            flash.error = "Payment method already present!!"
        }
        redirect(action: 'addNewInvPaymentMethod')
    }

    def editInvPaymentMethod() {
        println(" i am editInvPaymentMethod" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvPaymentMethod invPaymentMethod = InvPaymentMethod.findById(params.paymentMethodId)
        InvPaymentMethod invPaymentMethod1 = InvPaymentMethod.findByName(params.paymentMethodName)
        if (invPaymentMethod1 == null) {
            invPaymentMethod.name = params.paymentMethodName.trim()
            /*if(params.isactive == 'on') {
                invPaymentMethod.isactive = true
            } else {
                invPaymentMethod.isactive = false
            }*/
            invPaymentMethod.updation_ip_address = request.getRemoteAddr()
            invPaymentMethod.updation_date = new java.util.Date()
            invPaymentMethod.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
        } else {
            flash.error = 'Duplicate Entry'
        }
        redirect(action: 'addNewInvPaymentMethod')
    }

    def deleteInvPaymentMethod() {
        println(" i am deleteInvPaymentMethod" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvPaymentMethod invPaymentMethod = InvPaymentMethod.findById(params.paymentMethodId)
        if (invPaymentMethod == null) {
            flash.error = "Payment Method Not Found"
        } else {
            invPaymentMethod.delete(flush: true, failOnError: true)
            flash.message = "Deleted Successfully"
        }
        redirect(action: 'addNewInvPaymentMethod')
    }

    def activateInvPaymentMethod() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateInvPaymentMethod :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvPaymentMethod invPaymentMethod = InvPaymentMethod.findById(params.invPaymentMethodId)
            if (invPaymentMethod) {
                if (invPaymentMethod.isactive) {
                    invPaymentMethod.isactive = false
                    invPaymentMethod.updation_username = login.username
                    invPaymentMethod.updation_date = new Date()
                    invPaymentMethod.updation_ip_address = request.getRemoteAddr()
                    invPaymentMethod.save(flush: true, failOnError: true)
                    flash.message = "Successful.."
                } else {
                    invPaymentMethod.isactive = true
                    invPaymentMethod.updation_username = login.username
                    invPaymentMethod.updation_date = new Date()
                    invPaymentMethod.updation_ip_address = request.getRemoteAddr()
                    invPaymentMethod.save(flush: true, failOnError: true)
                    flash.message = "Successful.."
                }
            } else {
                flash.error = "Payment Method Not found"
            }
            redirect(controller: "inventoryMasters", action: "addNewInvPaymentMethod")
            return
        }
    }

    //master-SerialNumber
    def addNewInvSerialNumber() {
        println(" i am addNewInvSerialNumber")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invSerialNumberList = InvSerialNumber.findAllWhere(organization: org)
        def academicYearList = AcademicYear.findAllByIsactive(true).sort { it.ay }
        def departmentList = Department.findAllWhere(organization: org)
        [invSerialNumberList: invSerialNumberList, academicYearList: academicYearList, departmentList: departmentList]
    }

    def editInvSerialNumber() {
        println(" i am editInvSerialNumber" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.departmentId != 'null' && params.academicYearId != 'null' && params.serialNumberName != 'null') {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            def invDepatment = Department.findById(params.departmentId)
            def invAcademicYear = AcademicYear.findById(params.academicYearId)
            int serialnumber = Integer.parseInt(params.serialNumberName.trim())
            InvSerialNumber invSerialNumber = InvSerialNumber.findById(params.serialNumberId)
            InvSerialNumber invSerialNumber1 = InvSerialNumber.findByNumberAndDepartmentAndAcademicyear(params.serialNumberName, invDepatment, invAcademicYear)
            if (invSerialNumber1 == null) {
                invSerialNumber.number = serialnumber
                invSerialNumber.department = invDepatment
                invSerialNumber.academicyear = invAcademicYear
                invSerialNumber.updation_username = instructor.uid
                invSerialNumber.updation_ip_address = request.getRemoteAddr()
                invSerialNumber.updation_date = new java.util.Date()
                invSerialNumber.save(flush: true, failOnError: true)
                flash.message = "Updated Successfully"
            } else {
                flash.error = 'Duplicate Entry'
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewInvSerialNumber')
    }

    def saveInvSerialNumber() {
        println(" i am saveInvSerialNumber" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if (params.departmentId != 'null' && params.academicYearId != 'null' && params.serialNumberName != 'null') {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            def dept = Department.findByIdAndOrganization(params.departmentId, org)
            def ay = AcademicYear.findById(params.academicYearId)
            int serialnumber = 0
            try {
                serialnumber = Integer.parseInt(params.serialNumberName.trim())
            } catch (Exception e) {
                flash.error = "Failed...! Please enter Serial Number in Number format only"
                redirect(action: 'addNewInvSerialNumber')
                return
            }
            InvSerialNumber invSerialNumber = InvSerialNumber.findByNumberAndDepartmentAndAcademicyear(serialnumber, dept, ay)
            if (invSerialNumber == null) {
                invSerialNumber = new InvSerialNumber()
                invSerialNumber.number = serialnumber
                def invDepatment = Department.findById(params.departmentId)
                def invAcademicYear = AcademicYear.findById(params.academicYearId)
                invSerialNumber.department = invDepatment
                invSerialNumber.academicyear = invAcademicYear
                invSerialNumber.organization = org
                invSerialNumber.creation_username = instructor.uid
                invSerialNumber.updation_username = instructor.uid
                invSerialNumber.updation_ip_address = request.getRemoteAddr()
                invSerialNumber.creation_ip_address = request.getRemoteAddr()
                invSerialNumber.updation_date = new java.util.Date()
                invSerialNumber.creation_date = new java.util.Date()
                invSerialNumber.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
            } else {
                flash.error = "Data Already Exists"
            }
        } else {
            flash.error = "Please fill all fields"
        }
        redirect(action: 'addNewInvSerialNumber')
    }

    def deleteInvSerialNumber() {
        println(" i am deleteInvSerialNumber" + params)
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        InvSerialNumber invSerialNumber = InvSerialNumber.findById(params.serialNumberId)
        if (invSerialNumber == null) {
            flash.error = "Record Not Found"
        } else {
            invSerialNumber.delete(flush: true, failOnError: true)
            flash.message = "Deleted Successfully"
        }
        redirect(action: 'addNewInvSerialNumber')
    }

    //master-InvFinanceApprovingAuthorityLevel
    def addinvfinanceapprovingauthoritylevel() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addinvfinanceapprovingauthoritylevel : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def InvFinanceApprovingAuthority = InvFinanceApprovingAuthority.findAllByOrganization(org)
            //println("InvFinanceApprovingAuthority"+InvFinanceApprovingAuthority)
            def InvFinanceApprovingCategory = InvFinanceApprovingCategory.findAllByOrganization(org)
            //println("InvFinanceApprovingCategory"+InvFinanceApprovingCategory)
            def InvFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findAllByOrganization(org)
            //println("InvFinanceApprovingAuthorityLevel"+InvFinanceApprovingAuthorityLevel)
            [InvFinanceApprovingAuthorityLevel: InvFinanceApprovingAuthorityLevel, InvFinanceApprovingCategory: InvFinanceApprovingCategory, InvFinanceApprovingAuthority: InvFinanceApprovingAuthority]
        }
    }

    def saveinvfinanceapprovingauthoritylevel() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveinvfinanceapprovingauthoritylevel : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.level_no == 'null') {
                flash.error = "Please Add Level_No..!"
            }
            if (params.invfinanceapprovingauthority == 'null') {
                flash.error = "Please Select Finance Approving Authority..!"
            }
            if (params.invfinanceapprovingcategory == 'null') {
                flash.error = "Please Select Finance Approving Category..!"
            }

            if (params.level_no && params.invfinanceapprovingauthority && params.invfinanceapprovingcategory != 'null') {
                InvFinanceApprovingAuthority invFinanceApprovingAuthority = InvFinanceApprovingAuthority.findById(params.invfinanceapprovingauthority)
                InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findById(params.invfinanceapprovingcategory)
                InvFinanceApprovingAuthorityLevel invFAAL = InvFinanceApprovingAuthorityLevel.findById(params.ifaal)
                if (invFAAL != null) {
                    InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByOrganizationAndlevel_noAndInvfinanceapprovingauthorityAndInvfinanceapprovingcategory(org, params.level_no, invFinanceApprovingAuthority, invFinanceApprovingCategory)
                    if (invFinanceApprovingAuthorityLevel) {
                        flash.error = "Already Exists ..!"
                    } else {
                        if (params.islast == 'on') {
                            invFAAL.islast = true
                        } else {
                            invFAAL.islast = false
                        }
                        invFAAL.level_no = params.level_no.toInteger()
                        invFAAL.invfinanceapprovingauthority = invFinanceApprovingAuthority
                        invFAAL.invfinanceapprovingcategory = invFinanceApprovingCategory
                        invFAAL.organization = org
                        invFAAL.updation_username = login.username
                        invFAAL.updation_date = new Date()
                        invFAAL.updation_ip_address = request.getRemoteAddr()
                        invFAAL.save(flush: true, failOnError: true)
                        flash.message = "Updation Successfully..!"
                    }
                } else {
                    InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByOrganizationAndlevel_noAndInvfinanceapprovingauthorityAndInvfinanceapprovingcategory(org, params.level_no, invFinanceApprovingAuthority, invFinanceApprovingCategory)
                    if (invFinanceApprovingAuthorityLevel) {
                        flash.error = "Already Exists ..!"
                    } else {
                        invFinanceApprovingAuthorityLevel = new InvFinanceApprovingAuthorityLevel()
                        if (params.isactive == 'on') {
                            invFinanceApprovingAuthorityLevel.isactive = true
                        } else {
                            invFinanceApprovingAuthorityLevel.isactive = false
                        }
                        if (params.islast == 'on') {
                            invFinanceApprovingAuthorityLevel.islast = true
                        } else {
                            invFinanceApprovingAuthorityLevel.islast = false
                        }
                        invFinanceApprovingAuthorityLevel.level_no = params.level_no.toInteger()
                        invFinanceApprovingAuthorityLevel.invfinanceapprovingauthority = invFinanceApprovingAuthority
                        invFinanceApprovingAuthorityLevel.invfinanceapprovingcategory = invFinanceApprovingCategory
                        invFinanceApprovingAuthorityLevel.organization = org

                        invFinanceApprovingAuthorityLevel.creation_username = login.username
                        invFinanceApprovingAuthorityLevel.updation_username = login.username
                        invFinanceApprovingAuthorityLevel.creation_date = new Date()
                        invFinanceApprovingAuthorityLevel.updation_date = new Date()
                        invFinanceApprovingAuthorityLevel.creation_ip_address = request.getRemoteAddr()
                        invFinanceApprovingAuthorityLevel.updation_ip_address = request.getRemoteAddr()
                        invFinanceApprovingAuthorityLevel.save(flush: true, failOnError: true)
                        flash.message = "Added Successfully..!"
                    }
                }
            } else {
                flash.error = "Mandatory fields are missing..!"
            }
            redirect(controller: "inventoryMasters", action: "addinvfinanceapprovingauthoritylevel")
            return
        }
    }

    def deleinvfinanceapprovingauthoritylevel() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleinvfinanceapprovingauthoritylevel :: " + params)
            InvFinanceApprovingAuthorityLevel ifaal = InvFinanceApprovingAuthorityLevel.findById(params.ifaal)
            if (ifaal) {
                try {
                    ifaal.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Approving Authority Level can not delete..!"
                }
            } else {
                flash.error = "Approving Authority Level Not found"
            }
            flash.message = "Delected successfully..!"
            redirect(controller: "inventoryMasters", action: "addinvfinanceapprovingauthoritylevel")
            return
        }
    }

    def activationinvfinanceapprovingauthoritylevel() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvfinanceapprovingauthoritylevel :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvFinanceApprovingAuthorityLevel ifaal = InvFinanceApprovingAuthorityLevel.findById(params.ifaal)
            if (ifaal) {
                if (ifaal.isactive) {
                    ifaal.isactive = false
                    ifaal.updation_username = login.username
                    ifaal.updation_date = new Date()
                    ifaal.updation_ip_address = request.getRemoteAddr()
                    ifaal.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully..!"
                } else {
                    ifaal.isactive = true
                    ifaal.updation_username = login.username
                    ifaal.updation_date = new Date()
                    ifaal.updation_ip_address = request.getRemoteAddr()
                    ifaal.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Approving Authority Level Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvfinanceapprovingauthoritylevel")
            return
        }
    }

    //master-InvInvitation Status
    def addInvInivitationStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("I am in addInvInivitationStatus : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invInvitationStatusList = InvInvitationStatus.findAllByOrganization(org)

            [invInvitationStatusList: invInvitationStatusList]
        }
    }

    def saveInvInivitationStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In save Inventory Invitation Status : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.InvitationStatusName.isEmpty()) {

                flash.error = "Please add a Invitation Status...."
                redirect(action: 'addInvInivitationStatus')
            }
            if (!params.InvitationStatusName.isEmpty()) {
                InvInvitationStatus invInvitationStatus = InvInvitationStatus.findByName(params.InvitationStatusName.trim())
                if (invInvitationStatus != null) {
                    flash.error = "Status already exists"
                    redirect(action: 'addInvInivitationStatus')
                } else {

                    invInvitationStatus = new InvInvitationStatus()
                    invInvitationStatus.name = params.InvitationStatusName
                    if (params.isactive == 'on') {
                        invInvitationStatus.isactive = true
                    } else {
                        invInvitationStatus.isactive = false
                    }
                    invInvitationStatus.organization = org

                    invInvitationStatus.creation_username = instructor.uid
                    invInvitationStatus.updation_username = instructor.uid
                    invInvitationStatus.updation_ip_address = request.getRemoteAddr()
                    invInvitationStatus.creation_ip_address = request.getRemoteAddr()

                    invInvitationStatus.updation_date = new java.util.Date()
                    invInvitationStatus.creation_date = new java.util.Date()

                    invInvitationStatus.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addInvInivitationStatus')


                }
            }


        }

    }

    def editInvInivitationStatus() {
        println(" i am editInvInivitationStatus" + params)
        if (session.user == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization
        InvInvitationStatus invInvitationStatus = InvInvitationStatus.findById(params.InvitationStatusId)
        if (invInvitationStatus != null) {
            InvInvitationStatus invInvitationSameStatus = InvInvitationStatus.findByName(params.InvitationStatusName)
            if (invInvitationStatus?.name == params.InvitationStatusName) {
                flash.error = "Invitation Status name already exists, please a provide a different name while editing"
                redirect(action: 'addInvInivitationStatus')
            } else {
                /*if(params.isactive=='on')
                {
                    invInvitationStatus.isactive=true
                }
                else
                {
                    invInvitationStatus.isactive=false
                }*/
                invInvitationStatus.name = params.InvitationStatusName
                invInvitationStatus.organization = org
                invInvitationStatus.creation_username = login.username
                invInvitationStatus.updation_username = login.username
                invInvitationStatus.updation_ip_address = request.getRemoteAddr()
                invInvitationStatus.creation_ip_address = request.getRemoteAddr()

                invInvitationStatus.updation_date = new java.util.Date()
                invInvitationStatus.creation_date = new java.util.Date()

                invInvitationStatus.save(flush: true, failOnError: true)
                flash.message = "Updated  Successfully"
                redirect(action: 'addInvInivitationStatus')
            }

        } else {
            flash.error = "Mandatory fields are missing..."
        }
    }

    def activateInvInivitationStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvinstructorfinanceapprovingauthority :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvInvitationStatus invInvitationStatus = InvInvitationStatus.findById(params.InvitationStatus)
            if (invInvitationStatus) {
                if (invInvitationStatus.isactive) {
                    invInvitationStatus.isactive = false
                    invInvitationStatus.updation_username = login.username
                    invInvitationStatus.updation_date = new java.util.Date()
                    invInvitationStatus.updation_ip_address = request.getRemoteAddr()
                    invInvitationStatus.save(flush: true, failOnError: true)
                    flash.message = "In-Active Successfully.."
                } else {
                    invInvitationStatus.isactive = true
                    invInvitationStatus.updation_username = login.username
                    invInvitationStatus.updation_date = new java.util.Date()
                    invInvitationStatus.updation_ip_address = request.getRemoteAddr()
                    invInvitationStatus.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Inventory Invitation Status  Not found"
            }
            redirect(controller: "inventoryMasters", action: "addInvInivitationStatus")
            return
        }
    }

    def deleteInvInivitationStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("I am in deleteInvInivitationStatus:: " + params)
            InvInvitationStatus invInvitationStatus = InvInvitationStatus.findById(params.InvitationStatus)
            if (invInvitationStatus) {
                try {
                    invInvitationStatus.delete(flush: true, failOnError: true)
                    flash.message = "Deleted successfully..."
                } catch (Exception e) {
                    flash.error = "Invitation Status Cannot be Deleted .."
                }
            } else {
                flash.error = "Invitation Status Not found"
            }

            redirect(controller: "inventoryMasters", action: "addInvInivitationStatus")
            return
        }
    }

    //master-InvDeadStockStatus
    def addInvDeadStockStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("I am in addInvDeadStockStatus : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invDeadStockStatusList = InvDeadstockStatus.findAllByOrganization(org)

            [invDeadStockStatusList: invDeadStockStatusList]
        }
    }

    def saveInvDeadStockStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In save Inventory Invitation Status : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (!params.DeadStockStatusName.isEmpty()) {
                InvDeadstockStatus invDeadStockStatus = InvDeadstockStatus.findByStatus(params.DeadStockStatusName.trim())
                println("Dead Stock Status Object-->" + invDeadStockStatus?.status)
                if (invDeadStockStatus == null) {
                    invDeadStockStatus = new InvDeadstockStatus()
                    invDeadStockStatus.status = params.DeadStockStatusName
                    if (params.isactive == 'on') {
                        invDeadStockStatus.isactive = true
                    } else {
                        invDeadStockStatus.isactive = false
                    }
                    invDeadStockStatus.organization = org

                    invDeadStockStatus.creation_username = instructor.uid
                    invDeadStockStatus.updation_username = instructor.uid
                    invDeadStockStatus.updation_ip_address = request.getRemoteAddr()
                    invDeadStockStatus.creation_ip_address = request.getRemoteAddr()

                    invDeadStockStatus.updation_date = new java.util.Date()
                    invDeadStockStatus.creation_date = new java.util.Date()

                    invDeadStockStatus.save(flush: true, failOnError: true)
                    flash.message = "Saved Successfully"
                    redirect(action: 'addInvDeadStockStatus')
                } else {
                    flash.error = "Dead Stock Status with the name you provided already exists"
                    redirect(action: 'addInvDeadStockStatus')
                }
            } else {
                flash.error = "Name is empty,please provide a name"
                redirect(action: 'addInvDeadStockStatus')
            }

        }

    }

    def editInvDeadStockStatus() {
        println(" i am editInvDeadStockStatus" + params)
        if (session.user == null)
            redirect(action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization
        def invDeadStockStatus = InvDeadstockStatus.findById(params.DeadStockStatus)
        def duplicateEntry = InvDeadstockStatus.findByStatus(params.DeadStockStatusName)
        if (duplicateEntry == null) {
            invDeadStockStatus.status = (params.DeadStockStatusName).toString().trim()
            invDeadStockStatus.organization = org
            invDeadStockStatus.creation_username = login.username
            invDeadStockStatus.updation_username = login.username
            invDeadStockStatus.updation_ip_address = request.getRemoteAddr()
            invDeadStockStatus.creation_ip_address = request.getRemoteAddr()

            invDeadStockStatus.updation_date = new java.util.Date()
            invDeadStockStatus.creation_date = new java.util.Date()

            invDeadStockStatus.save(flush: true, failOnError: true)
            flash.message = "Updated  Successfully"
        } else {
            flash.error = "Already Present.."
        }
        redirect(action: 'addInvDeadStockStatus')
    }

    def activateInvDeadStockStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activateInvDeadStockStatus :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvDeadstockStatus invDeadStockStatus = InvDeadstockStatus.findById(params.DeadStockStatus)
            if (invDeadStockStatus) {
                if (invDeadStockStatus.isactive) {
                    invDeadStockStatus.isactive = false
                    invDeadStockStatus.updation_username = login.username
                    invDeadStockStatus.updation_date = new java.util.Date()
                    invDeadStockStatus.updation_ip_address = request.getRemoteAddr()
                    invDeadStockStatus.save(flush: true, failOnError: true)
                    flash.message = "In-Active Successfully.."
                } else {
                    invDeadStockStatus.isactive = true
                    invDeadStockStatus.updation_username = login.username
                    invDeadStockStatus.updation_date = new java.util.Date()
                    invDeadStockStatus.updation_ip_address = request.getRemoteAddr()
                    invDeadStockStatus.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully.."
                }
            } else {
                flash.error = "Inventory Dead Stock Status  Not found"
            }
            redirect(controller: "inventoryMasters", action: "addInvDeadStockStatus")
            return
        }
    }

    def deleteInvDeadStockStatus() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("I am in deleteInvDeadStockStatus:: " + params)
            InvDeadstockStatus invDeadStockStatus = InvDeadstockStatus.findById(params.DeadStockStatus)
            if (invDeadStockStatus) {
                try {
                    invDeadStockStatus.delete(flush: true, failOnError: true)
                    flash.message = "Deleted successfully..."
                } catch (Exception e) {
                    flash.error = "Foreign Key Constraint,Cannot Delete."
                }
            } else {
                flash.error = "Dead Stock status Not found"
            }

            redirect(controller: "inventoryMasters", action: "addInvDeadStockStatus")
            return
        }
    }

    //master-InvDepreciationPercentage
    def addinvdepreciationpercentage() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addinvdepreciationpercentage : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def inventoryDepreciationPercentage = InvDepreciationPercentage.findAllWhere(organization: org)
            def invmaterialList = InvMaterial.findAllByOrganization(org)
            def invmaterialNameList = InvMaterial.findAllByOrganization(org)?.name
            def academicYearList = AcademicYear.findAllByIsactive(true).sort { it.ay }
            def invmaterialpartList = InvMaterialPart.findAllWhere(organization: org)
            def invmaterialpartNameList = InvMaterialPart.findAllWhere(organization: org)?.name
            def invMaterialSetNameList = InvMaterialSet.findAllWhere(organization: org)?.name
            def entryby = instructor
            [entryby                        : entryby, invmaterialpartNameList: invmaterialpartNameList, invmaterialNameList: invmaterialNameList, academicYearList: academicYearList,
             inventoryDepreciationPercentage: inventoryDepreciationPercentage, invMaterialSetNameList: invMaterialSetNameList]
        }
    }

    def saveinvdepreciationpercentage() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveinvdepreciationpercentage : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.invmaterial == 'null' || params.invmaterialpart == 'null' || params.academicYearId == 'null') {
                flash.error = "Please Add All Fields..!"
                redirect(controller: "inventoryMasters", action: "addinvdepreciationpercentage")
            } else {
                InvMaterial invMaterial = InvMaterial.findByName(params.invmaterial)
                def invMaterialSet = invMaterial?.invmaterialset
                def academicYear = AcademicYear.findById(params.academicYearId)
                InvMaterialPart invMaterialPart = InvMaterialPart.findByName(params.invmaterialpart)
                def entryBy = instructor
                def percentage = Double.parseDouble(params.percentage)

                InvDepreciationPercentage inventoryDepreciationPercentage = InvDepreciationPercentage.findByOrganizationAndInvmaterialAndInvmaterialpartAndEntryby(org, invMaterial, invMaterialPart, instructor)
                println("inventoryDepreciationPercentage1  -" + inventoryDepreciationPercentage)
                if (inventoryDepreciationPercentage) {
                    flash.error = "Already Exists ..!"
                } else {
                    inventoryDepreciationPercentage = new InvDepreciationPercentage()
                    if (params.isactive == 'on') {
                        inventoryDepreciationPercentage.isactive = true
                    } else {
                        inventoryDepreciationPercentage.isactive = false
                    }
                    inventoryDepreciationPercentage.percentage = percentage
                    inventoryDepreciationPercentage.entry_date = new Date()
                    inventoryDepreciationPercentage.invmaterial = invMaterial
                    inventoryDepreciationPercentage.invmaterial.invmaterialset = invMaterialSet
                    inventoryDepreciationPercentage.academicyear = academicYear
                    inventoryDepreciationPercentage.invmaterialpart = invMaterialPart
                    inventoryDepreciationPercentage.entryby = instructor
                    inventoryDepreciationPercentage.organization = org
                    inventoryDepreciationPercentage.creation_username = login.username
                    inventoryDepreciationPercentage.updation_username = login.username
                    inventoryDepreciationPercentage.creation_date = new Date()
                    inventoryDepreciationPercentage.updation_date = new Date()
                    inventoryDepreciationPercentage.creation_ip_address = request.getRemoteAddr()
                    inventoryDepreciationPercentage.updation_ip_address = request.getRemoteAddr()
                    inventoryDepreciationPercentage.save(flush: true, failOnError: true)
                    flash.message = "Added Successfully..!"
                }
                redirect(controller: "inventoryMasters", action: "addinvdepreciationpercentage")
                return
            }
        }
    }

    def editinvdepreciationpercentage() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            if (params.invmaterial == 'null' || params.invmaterialpart == 'null' || params.academicYearId == 'null') {
                flash.error = "Please Add All Fields While Updating.!"
                redirect(controller: "inventoryMasters", action: "addinvdepreciationpercentage")
            } else {
                Login login = Login.findById(session.loginid)
                Instructor instructor = Instructor.findByUid(login.username)
                Organization org = instructor.organization
                println("editinvdepreciationpercentage :: " + params)
                InvMaterial invMaterial = InvMaterial.findByName(params.invmaterial)
                def invMaterialSet = invMaterial?.invmaterialset
                def academicYear = AcademicYear.findById(params.academicYearId)
                InvMaterialPart invMaterialPart = InvMaterialPart.findByName(params.invmaterialpart)

                InvDepreciationPercentage inventoryDepreciationPercentage = InvDepreciationPercentage.findById(params.inventorydepreciationpercentage)
                println("inventoryDepreciationPercentage--" + inventoryDepreciationPercentage)
                InvDepreciationPercentage inventoryDepreciationPercentage1 = InvDepreciationPercentage.findByInvmaterialAndInvmaterialpartAndEntryby(invMaterial, invMaterialPart, instructor)

                if (inventoryDepreciationPercentage1 == null) {
                    inventoryDepreciationPercentage.percentage = Double.parseDouble(params.percentage)
                    inventoryDepreciationPercentage.entry_date = new Date()
                    inventoryDepreciationPercentage.invmaterial = invMaterial
                    inventoryDepreciationPercentage.academicyear = academicYear
                    inventoryDepreciationPercentage.invmaterialpart = invMaterialPart
                    inventoryDepreciationPercentage.invmaterial.invmaterialset = invMaterialSet
                    inventoryDepreciationPercentage.entryby = instructor
                    inventoryDepreciationPercentage.organization = org
                    inventoryDepreciationPercentage.updation_username = login.username
                    inventoryDepreciationPercentage.updation_date = new Date()
                    inventoryDepreciationPercentage.updation_ip_address = request.getRemoteAddr()
                    inventoryDepreciationPercentage.save(flush: true, failOnError: true)
                    flash.message = "Updation Successfully..!"
                    redirect(controller: "inventoryMasters", action: "addinvdepreciationpercentage")
                    return
                } else {
                    flash.error = 'Already Exists, Update Percentage'
                    redirect(controller: "inventoryMasters", action: "addinvdepreciationpercentage")
                }
            }
        }
    }

    def deleinvdepreciationpercentage() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleinvdepreciationpercentage :: " + params)


            InvDepreciationPercentage inventoryDepreciationPercentage = InvDepreciationPercentage.findById(params.inventoryDepreciationPercentage)
            if (inventoryDepreciationPercentage) {
                try {
                    inventoryDepreciationPercentage.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Depreciation Percentagecan can not be deleted..!"
                }
            } else {
                flash.error = "Depreciation Percentage Not found"
            }
            flash.message = "Delected successfully..!"
            redirect(controller: "inventoryMasters", action: "addinvdepreciationpercentage")
            return
        }
    }

    def activationinvdepreciationpercentage() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvdepreciationpercentage :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvDepreciationPercentage inventoryDepreciationPercentage = InvDepreciationPercentage.findById(params.inventoryDepreciationPercentage)
            if (inventoryDepreciationPercentage != null) {
                if (inventoryDepreciationPercentage.isactive) {
                    inventoryDepreciationPercentage.isactive = false
                    inventoryDepreciationPercentage.updation_username = login.username
                    inventoryDepreciationPercentage.updation_date = new Date()
                    inventoryDepreciationPercentage.updation_ip_address = request.getRemoteAddr()
                    inventoryDepreciationPercentage.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully..!"
                } else {
                    inventoryDepreciationPercentage.isactive = true
                    inventoryDepreciationPercentage.updation_username = login.username
                    inventoryDepreciationPercentage.updation_date = new Date()
                    inventoryDepreciationPercentage.updation_ip_address = request.getRemoteAddr()
                    inventoryDepreciationPercentage.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Record Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvdepreciationpercentage")
            return
        }
    }


    //master-InvRoom
    def addinvroom() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addinvroom : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invroom = InvRoom.findAllByOrganization(org)
            def department = Department.findAllByOrganization(org)
            [invroom: invroom, department: department]
        }
    }

    def saveinvroom() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveinvroom : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            if (params.department == 'null') {
                flash.error = "Please Add Department..!"
                redirect(controller: "inventoryMasters", action: "addinvroom")
            } else {
                def department = Department.findById(params.department)
                def number = params.number
                def name = params.name
                InvRoom invRoom1 = InvRoom.findByOrganizationAndNumber(org, params.number)
                InvRoom invRoom2 = InvRoom.findByOrganizationAndNameAndDepartment(org, name, department)
                if (invRoom1) {
                    flash.error = "Room Number Already Exists ..!"
                    redirect(controller: "inventoryMasters", action: "addinvroom")
                } else if (invRoom2) {
                    flash.error = "Room Already Assigned to Department..!"
                    redirect(controller: "inventoryMasters", action: "addinvroom")
                } else if (invRoom1 == null || invRoom2 == null) {
                    InvRoom invRoom = new InvRoom()
                    if (params.isactive == 'on') {
                        invRoom.isactive = true
                    } else {
                        invRoom.isactive = false
                    }
                    invRoom.name = name
                    invRoom.number = number
                    invRoom.department = department
                    invRoom.organization = org
                    invRoom.creation_username = login.username
                    invRoom.updation_username = login.username
                    invRoom.creation_date = new Date()
                    invRoom.updation_date = new Date()
                    invRoom.creation_ip_address = request.getRemoteAddr()
                    invRoom.updation_ip_address = request.getRemoteAddr()
                    invRoom.save(flush: true, failOnError: true)
                    flash.message = "Added Successfully..!"
                    redirect(controller: "inventoryMasters", action: "addinvroom")
                }
            }
        }
    }

    def deleinvroom() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleinvroom :: " + params)
            InvRoom invRoom = InvRoom.findById(params.invRoom)
            if (invRoom) {
                try {
                    invRoom.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Room Cannot be deleted.!"
                }
            } else {
                flash.error = "InvRoom Not found"
            }
            flash.message = "Delected successfully..!"
            redirect(controller: "inventoryMasters", action: "addinvroom")
            return
        }
    }

    def activationinvroom() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("activationinvroom :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvRoom invRoom = InvRoom.findById(params.invRoom)
            if (invRoom) {
                if (invRoom.isactive) {
                    invRoom.isactive = false
                    invRoom.updation_username = login.username
                    invRoom.updation_date = new Date()
                    invRoom.updation_ip_address = request.getRemoteAddr()
                    invRoom.save(flush: true, failOnError: true)
                    flash.message = "In-active Successfully..!"
                } else {
                    invRoom.isactive = true
                    invRoom.updation_username = login.username
                    invRoom.updation_date = new Date()
                    invRoom.updation_ip_address = request.getRemoteAddr()
                    invRoom.save(flush: true, failOnError: true)
                    flash.message = "Active Successfully..!"
                }
            } else {
                flash.error = "Room Category Not found"
            }
            redirect(controller: "inventoryMasters", action: "addinvroom")
            return
        }
    }

    def editinvroom() {
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

        println("editinvroom :: " + params)
        if (params.department == 'null') {
            flash.error = "Please Add Department..!"
            redirect(controller: "inventoryMasters", action: "addinvroom")
        } else {
            def department = Department.findById(params.department)
            def name = params.name
            def number = params.number
            InvRoom invRoom1 = InvRoom.findById(params.invroom)
            InvRoom invRoom = InvRoom.findByNameAndDepartmentAndNumber(name, department, params.number)
            if (invRoom == null) {
                invRoom1.name = name
                invRoom1.number = number
                invRoom1.department = department
                invRoom1.updation_username = instructor.uid
                invRoom1.updation_ip_address = request.getRemoteAddr()
                invRoom1.updation_date = new java.util.Date()
                invRoom1.save(flush: true, failOnError: true)
                flash.message = "Updated Successfully"
                redirect(controller: "inventoryMasters", action: "addinvroom")
            } else {
                flash.error = 'Room Already Exists'
                redirect(controller: "inventoryMasters", action: "addinvroom")
            }
        }
    }


    // master-InvInstructorFinanceApprovingAuthority
    // bulk post assign form
    def addApprvovingAuth() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return

        } else {
            println("addApprvovingAuth : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def invinstructorfinanceapprovingauthoritylist = InvInstructorFinanceApprovingAuthority.findAllByOrganization(org)
            def invInstructorList = Instructor.findAllWhere(organization: org)
            // def invInstructor = Instructor.findByOrganizationAndIscurrentlyworking(org,true)
            def invFinanceApprovingAuthorityList = InvFinanceApprovingAuthority.findAllWhere(organization: org)
            [invInstructorList: invInstructorList, invFinanceApprovingAuthorityList: invFinanceApprovingAuthorityList, invinstructorfinanceapprovingauthoritylist: invinstructorfinanceapprovingauthoritylist]
        }
    }

    def saveApprvovingAuth() {
        println("saveApprvovingAuth---->" + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

        Instructor invInstructor = Instructor.findById(params.instructorId)
        def invFinanceApprovingAuthorityList = []
        if (params.invFinanceApprovingAuthority.getClass().isArray()) {
            for (items in params.invFinanceApprovingAuthority) {
                InvFinanceApprovingAuthority invFinanceApprovingAuthority = InvFinanceApprovingAuthority.findById(items)
                invFinanceApprovingAuthorityList.add(invFinanceApprovingAuthority)
            }
        } else {
            InvFinanceApprovingAuthority invFinanceApprovingAuthority = InvFinanceApprovingAuthority.findById(params.invFinanceApprovingAuthority)
            invFinanceApprovingAuthorityList.add(invFinanceApprovingAuthority)
        }
        for (items in invFinanceApprovingAuthorityList) {
            InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findByOrganizationAndInstructorAndInvfinanceapprovingauthority(org, invInstructor, items)
            if (!invInstructorFinanceApprovingAuthority) {
                invInstructorFinanceApprovingAuthority = new InvInstructorFinanceApprovingAuthority()
                invInstructorFinanceApprovingAuthority.isactive = true
                invInstructorFinanceApprovingAuthority.instructor = invInstructor
                invInstructorFinanceApprovingAuthority.invfinanceapprovingauthority = items
                invInstructorFinanceApprovingAuthority.organization = org
                invInstructorFinanceApprovingAuthority.creation_username = instructor.uid
                invInstructorFinanceApprovingAuthority.updation_username = instructor.uid
                invInstructorFinanceApprovingAuthority.updation_ip_address = request.getRemoteAddr()
                invInstructorFinanceApprovingAuthority.creation_ip_address = request.getRemoteAddr()
                invInstructorFinanceApprovingAuthority.updation_date = new java.util.Date()
                invInstructorFinanceApprovingAuthority.creation_date = new java.util.Date()
                invInstructorFinanceApprovingAuthority.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"

            } else {
                flash.message = "Already Present"
            }
        }

        redirect(controller: "inventoryMasters", action: "addApprvovingAuth")
        return
    }

    def deleApprvovingAuth() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("deleApprvovingAuth :: " + params)
            InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.instructorId)
            if (invInstructorFinanceApprovingAuthority) {
                try {
                    invInstructorFinanceApprovingAuthority.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = " Cannot be deleted.!"
                }
            } else {
                flash.error = " Not found"
            }
            flash.message = "Delected successfully..!"
            redirect(controller: "inventoryMasters", action: "addApprvovingAuth")
            return
        }
    }


}