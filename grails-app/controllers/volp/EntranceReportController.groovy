package volp

class EntranceReportController {

    def index() {}

    def entrancereport() {

        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def ay = AcademicYear.list().sort { it.ay }.reverse(true)
            ay.reverse(true)

            ApplicationType at = ApplicationType.findByApplication_type("ERP")
            RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Examination", org)
            ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, org)
            def ayid = aay?.academicyear?.id

            def programtypelist = ProgramType.findAllByOrganization(instructor.organization)

            [aylist: ay, ayid: ayid, programtypelist: programtypelist]
        }


    }

    def getEntranceversionAll() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            println("getEntranceversionAll :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization organization = instructor.organization
            if (params.programtypeId != '0' && params.programtypeId != 'null') {
                def programType = ProgramType.findById(params.programtypeId)
                AcademicYear academicYear=AcademicYear.findById(params.ay)
                def entranceversion=EntranceVersion.findAllByOrganizationAndAcademicyearAndProgramtype(instructor.organization,academicYear,programType)

                def program = Program.findAllByProgramtypeAndOrganization(programType,instructor.organization)
                def year = 0
//                render g.select(id: 'entrance', class: 'form-control', from: entranceversion, optionKey:"id" ,optionValue:"entrance_name", name: "entrance", onChange: "${remoteFunction(action: 'getProgramAll', update: 'program', params:'programtypeid='+params.programtypeId)};")
                render g.select(id: 'entrance', class: 'form-control', from: entranceversion, optionKey:"id" ,optionValue:"entrance_name", name: "entrance", onChange: "programall()")
            }
        }
    }

    def getProgramAll() {
        println("getProgramAll " + params)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization organization = instructor.organization

            if (params.programtypeId != '0' && params.programtypeId != 'null') {
                def programType = ProgramType.findById(params.programtypeId)


                def programlist = Program.findAllByProgramtypeAndOrganization(programType,instructor.organization)

                def year = 0
                render g.select(id: 'program', class: 'form-control',optionKey:"id" ,optionValue:"name", from: programlist, name: "program" ,noSelection:['All':'All'])
            }
        }
    }

    def getreporttabs() {
        [ay:params.ay,program:params.program,programtype:params.programType,entrance:params.entrance]
    }

    def All_Applied_Candidates() {
        println("All_Applied_Candidates " + params)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization organization = instructor.organization

            AcademicYear academicYear =AcademicYear.findById(params.ay)
            ProgramType programType =ProgramType.findById(params.programtype)
            EntranceVersion entranceVersion=EntranceVersion.findById(params.entrance)
           def program=null
            if(params.program=='All')
            {
                program=Program.findByProgramtypeAndOrganization(programType,instructor.organization)
            }
            else {
                program=Program.findById(params.program)
            }
          def reportdata = EntranceApplication.createCriteria().list {
                'in'('entranceapplicant', entranceVersion)
                and {
                    'in'('organization', instructor.organization)
                }
            }
            [reportdata:reportdata]
        }
    }

    def Not_Eligible_Candidates() {
        println("All_Applied_Candidates " + params)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization organization = instructor.organization

            AcademicYear academicYear =AcademicYear.findById(params.ay)
            ProgramType programType =ProgramType.findById(params.programtype)
            EntranceVersion entranceVersion=EntranceVersion.findById(params.entrance)
            def program=null
            if(params.program=='All')
            {
                program=Program.findByProgramtypeAndOrganization(programType,instructor.organization)
            }
            else {
                program=Program.findById(params.program)
            }
            def reportdata = EntranceApplication.createCriteria().list {
                'in'('entranceapplicant', entranceVersion)
                and {
                    'in'('organization', instructor.organization)
                }
            }
            [reportdata:reportdata]
        }
    }

    def Candidates_Appearing_for_Entrance_Test() {
        println("All_Applied_Candidates " + params)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization organization = instructor.organization

            AcademicYear academicYear =AcademicYear.findById(params.ay)
            ProgramType programType =ProgramType.findById(params.programtype)
            EntranceVersion entranceVersion=EntranceVersion.findById(params.entrance)
            def program=null
            if(params.program=='All')
            {
                program=Program.findByProgramtypeAndOrganization(programType,instructor.organization)
            }
            else {
                program=Program.findById(params.program)
            }
            def reportdata = EntranceApplication.createCriteria().list {
                'in'('entranceapplicant', entranceVersion)
                and {
                    'in'('organization', instructor.organization)
                }
            }
            [reportdata:reportdata]
        }
    }

    def Exempted_for_Entrance_Test() {
        println("All_Applied_Candidates " + params)
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization organization = instructor.organization

            AcademicYear academicYear =AcademicYear.findById(params.ay)
            ProgramType programType =ProgramType.findById(params.programtype)
            EntranceVersion entranceVersion=EntranceVersion.findById(params.entrance)
            def program=null
            if(params.program=='All')
            {
                program=Program.findByProgramtypeAndOrganization(programType,instructor.organization)
            }
            else {
                program=Program.findById(params.program)
            }
            def reportdata = EntranceApplication.createCriteria().list {
                'in'('entranceapplicant', entranceVersion)
                and {
                    'in'('organization', instructor.organization)
                }
            }
            [reportdata:reportdata]
        }
    }

    def Eligible_Candidate() {
       println("All_Applied_Candidates " + params)
       if (session.loginid == null) {
           flash.message = "Your are logged out! Please login again. Thank you."
           redirect(controller: "login", action: "erplogin")
       } else {
           Login login = Login.findById(session.loginid)
           Instructor instructor = Instructor.findByUid(login.username)
           Organization organization = instructor.organization

           AcademicYear academicYear =AcademicYear.findById(params.ay)
           ProgramType programType =ProgramType.findById(params.programtype)
           EntranceVersion entranceVersion=EntranceVersion.findById(params.entrance)
           def program=null
           if(params.program=='All')
           {
               program=Program.findByProgramtypeAndOrganization(programType,instructor.organization)
           }
           else {
               program=Program.findById(params.program)
           }
           def reportdata = EntranceApplication.createCriteria().list {
               'in'('entranceapplicant', entranceVersion)
               and {
                   'in'('organization', instructor.organization)
               }
           }
           [reportdata:reportdata]
       }
   }

    // Pratik - 17/6/2021
    def getFullReportFilter() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getFullReportFilter :: " + params)
            Login login=Login.findById(session.loginid)
            Instructor instructor=Instructor.findByEmployee_code(login.grno_empid)
            Organization org = instructor.organization

            ApplicationType at = ApplicationType.findByApplication_type("ERP")
            RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Admission", org)
            ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, org)

            def academicyear_list = AcademicYear.findAllByIsactive(true)
            def programtype_list = ProgramType.findAllByOrganization(org)
            def entrance_version_list = []
            if(academicyear_list.size() > 0 && programtype_list.size()> 0) {
                entrance_version_list = EntranceVersion.findAllByAcademicyearAndOrganizationAndProgramtype(aay?.academicyear, org, programtype_list[0])
            }

            [academicyear_list : academicyear_list, programtype_list : programtype_list, entrance_version_list : entrance_version_list, aay : aay]
        }
    }

    def getFullReport() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getFullReport :: " + params)
            Login login=Login.findById(session.loginid)
            Instructor instructor=Instructor.findByEmployee_code(login.grno_empid)
            Organization org = instructor.organization

            AcademicYear ay = AcademicYear.findById(params.ay)
            ProgramType programtype = ProgramType.findById(params.programtype)
            EntranceVersion entranceVersion = EntranceVersion.findById(params.entrance)
            if(!entranceVersion) {
                render "<div class='alert alert-danger'>Please select Entrance Version..</div>"
                return
            }

            def entranceapplication_list = EntranceApplication.createCriteria().list() {
                'in'('organization', org)
                and {
                    'in'('entranceversion', entranceVersion)
                }
            }
            println("entranceapplication_list :: " + entranceapplication_list)

            def exemptiontype_list = ExemptionType.findAllByIsactiveAndOrganization(true, org).sort{it.name}
            def entrancebranch_list = EntranceBranch.findAllByIsactiveAndEntranceversionAndOrganization(true, entranceVersion, org).sort{it.name}
            def entrancedegree_list = EntranceDegree.findAllByIsactive(true).sort{it.name}
            def recexperience_list = RecExperienceType.findAllByIsactive(true).sort{it.type}
            def entrancedocumenttype_list = EntranceDocumentType.findAllByIsactive(true).sort{it.name}

            [entranceapplication_list : entranceapplication_list, exemptiontype_list : exemptiontype_list,
             entrancebranch_list : entrancebranch_list, entrancedegree_list : entrancedegree_list,
             recexperience_list : recexperience_list, entrancedocumenttype_list : entrancedocumenttype_list]
        }
    }

    def getApplicantBranchDetails() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getApplicantBranchDetails :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByEmployee_code(login.grno_empid)
            Organization org = instructor.organization
            def id = params.applicationid.trim()
            println("id :: " + id)
            EntranceApplication entranceApplication = EntranceApplication.findById(id)
            println("entranceApplication :: " + entranceApplication)
            EntranceBranch entranceBranch = EntranceBranch.findById(params.branch.trim())
            println("entranceBranch :: " + entranceBranch)
            def branch_list = entranceApplication?.entrancebranch
            if(branch_list.size() > 0) {
                for(branch in branch_list) {
                    if(branch.id == entranceBranch.id) {
                        render "<i class='fa fa-check fa-2x' style='color:green'></i>"
                        return
                    } else {
                        continue
                    }
                }
            }
            render ""
            return
        }
    }

    def getApplicantExemptionTypeDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getApplicantExemptionTypeDetails :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByEmployee_code(login.grno_empid)
            Organization org = instructor.organization
            def id = params.applicationid.trim()
            println("id :: " + id)
            EntranceApplication entranceApplication = EntranceApplication.findById(id)
            println("entranceApplication :: " + entranceApplication)
            ExemptionType exemptiontype = ExemptionType.findById(params.exemptionType)
            def exemptionType_list1 = entranceApplication?.exemptiontype
            println("exemptiontype :: " + exemptiontype?.id)
            println("exemptionType_list1 : "+exemptionType_list1)
            if(exemptionType_list1) {
                for(exemption in exemptionType_list1) {
                    println("exemption : "+exemption)
                    if(exemption.id == exemptiontype.id) {
                        ExemptionDocument exemptionDocument = ExemptionDocument.findByExemptiontype(exemptiontype)
                        String path = exemptionDocument.file_path + exemptionDocument.file_name
                        println("path :: " + path)
                        AWSBucket awsBucket = AWSBucket.findByContent("documents")
                        AWSBucketService awsBucketService1 = new AWSBucketService()
                        String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

                        render "<i class='fa fa-check fa-2x' style='color:green'></i><br><a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue;margin-left :200px' aria-hidden='true'></i></a>"

                        return
                    } else {
                        continue
                    }
                }
            }
            render "-"
            return
        }
    }

    def getApplicantEntranceDocumentType(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getApplicantEntranceDocumentType :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByEmployee_code(login.grno_empid)
            Organization org = instructor.organization
            def id = params.applicationid.trim()
            println("id :: " + id)
            EntranceApplication entranceApplication = EntranceApplication.findById(id)
            EntranceDocumentType entranceDocumentType = EntranceDocumentType.findById(params.entrancedocumenttype)
            def entranceApplicantDocument1 = EntranceApplicantDocument.findAllByEntranceapplicant(entranceApplication?.entranceapplicant)
            def entrancedocumenttype1 = EntranceDocumentType.findAllByOrganizationAndIsactive(org, true)
            /*if(entrancedocumenttype1.size() != 0){
                for(doc in entrancedocumenttype1){
                    println("doc : "+doc)
                    if(entranceDocumentType?.id == doc?.id)
                    {
                        EntranceApplicantDocument entranceApplicantDocument = EntranceApplicantDocument.findByEntrancedocumenttypeAndEntranceapplicant(entranceDocumentType,entranceApplication?.entranceapplicant)
                        println("entranceApplicantDocument : "+entranceApplicantDocument)
                        if(entranceApplicantDocument != null){
                            String path = entranceApplicantDocument.filepath + entranceApplicantDocument.filename
                            println("path :: " + path)
                            AWSBucket awsBucket = AWSBucket.findByContent("documents")
                            AWSBucketService awsBucketService1 = new AWSBucketService()
                            String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

                            render "<i class='fa fa-check fa-2x' style='color:green'></i><br><a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue;margin-left :70px' aria-hidden='true'></i></a>"
                            return
                        }
                    }
                    else{
                        continue
                    }
                }
            }*/
            if (entranceDocumentType) {
                EntranceApplicantDocument entranceApplicantDocument = EntranceApplicantDocument.findByEntrancedocumenttypeAndEntranceapplicant(entranceDocumentType, entranceApplication?.entranceapplicant)
                println("entranceApplicantDocument : " + entranceApplicantDocument)
                if (entranceApplicantDocument != null) {
                    String path = entranceApplicantDocument.filepath + entranceApplicantDocument.filename
                    println("path :: " + path)
                    AWSBucket awsBucket = AWSBucket.findByContent("documents")
                    AWSBucketService awsBucketService1 = new AWSBucketService()
                    String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

                    render "<i class='fa fa-check fa-2x' style='color:green'></i><br><a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue;margin-left :130px' aria-hidden='true'></i></a>"
                    return
                } else {
                    render ""
                    return
                }
            }
        }
    }

    def getApplicantEntranceDegreeDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getApplicantEntranceDegreeDetails :: " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByEmployee_code(login.grno_empid)
            Organization org = instructor.organization
            def id = params.applicationid.trim()
            EntranceApplication entranceApplication = EntranceApplication.findById(id)
            EntranceDegree entranceDegree = EntranceDegree.findById(params.entrancedegree)
            if(entranceDegree){
                EntranceApplicantAcademics entranceApplicantAcademics = EntranceApplicantAcademics.findByEntrancedegree(entranceDegree)
                println("entranceApplicantAcademics : "+entranceApplicantAcademics)
                render "<div><table ><thead>" +
                        "                                                                               <tr>" +
                        "                                                                                   <th class=\"mdl-data-table__cell--non-numeric\">Class/Grade </th>" +
                        "                                                                                   <th class=\"mdl-data-table__cell--non-numeric\">Percentage/CPI </th>" +
                        "                                                                                   <th class=\"mdl-data-table__cell--non-numeric\">Year Of Passing </th>" +
                        "                                                                                   <th class=\"mdl-data-table__cell--non-numeric\">University/Board</th>" +
                        "                                                                                   <th class=\"mdl-data-table__cell--non-numeric\">Branch</th>" +
                        "                                                                                   <th class=\"mdl-data-table__cell--non-numeric\">Specialization</th>" +
                        "                                                                               </tr>" +
                        "                                                                           </thead> <tbody>\n" +
                        "                                                                                   <tr>\n" +
                        "                                                                                       <td class=\"mdl-data-table__cell--non-numeric\">${entranceApplicantAcademics?.recclass}</td>\n" +
                        "                                                                                       <td class=\"mdl-data-table__cell--non-numeric\">${entranceApplicantAcademics?.cpi_marks}</td>\n" +
                        "                                                                                       <td class=\"mdl-data-table__cell--non-numeric\">${entranceApplicantAcademics?.yearofpassing}</td>\n" +
                        "                                                                                       <td class=\"mdl-data-table__cell--non-numeric\">${entranceApplicantAcademics?.university}</td>\n" +
                        "                                                                                       <td class=\"mdl-data-table__cell--non-numeric\">${entranceApplicantAcademics?.branch}</td>\n" +
                        "                                                                                        <td class=\"mdl-data-table__cell--non-numeric\">${entranceApplicantAcademics?.specialization}</td>\n" +
                        "                                                                                   </tr>\n" +
                        "                                                                           </tbody></table></div> "
                return
            }
            else{
                render ""
                return
            }

        }

    }

    def applicationPreviewInReport(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            def array=[]
            println("In applicationPreviewInReport"+ params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersion)
            EntranceApplicant entranceApplicant= EntranceApplicant.findById(params.applicantId)
            EntranceApplication entranceApplication = EntranceApplication.findByEntranceapplicantAndEntranceversion(entranceApplicant,entranceVersion)
            def entranceApplicantAcademics = EntranceApplicantAcademics.findAllByEntranceapplicantAndOrganization(entranceApplicant,org)
            def entranceApplicantExperience = EntranceApplicantExperience.findAllByEntranceapplicant(entranceApplicant)
            def entranceApplicantDocument = EntranceApplicantDocument.findAllByEntranceapplicantAndOrganization(entranceApplicant,org)
            AWSFolderPath awsFolderPath = AWSFolderPath.findById('5')
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSBucketService awsBucketService = new AWSBucketService()
            for(doc in entranceApplicantDocument)
            {
                HashMap hm=new HashMap()
                def awsimagelink = doc?.filepath + doc?.filename
                String file = awsBucketService.getPresignedUrl(awsBucket.bucketname,awsimagelink, awsBucket.region)
                hm.put("url",file)
                hm.put("document",doc)
                array.add(hm)
            }
            String file = "https://vierp-test.s3.ap-south-1.amazonaws.com/img_avatar.png";
            if(entranceApplicant && entranceApplicant?.photopath && entranceApplicant.photoname) {
                def awsimagelink = entranceApplicant.photopath + entranceApplicant.photoname
                file = awsBucketService.getPresignedUrl(awsBucket.bucketname, awsimagelink, awsBucket.region)
            }
            [Photo_URL:file,EA:entranceApplicant,entranceApplication: entranceApplication,entranceApplicantAcademics:entranceApplicantAcademics,
             entranceApplicantExperience:entranceApplicantExperience,entranceApplicantDocument:array]
        }
    }
}