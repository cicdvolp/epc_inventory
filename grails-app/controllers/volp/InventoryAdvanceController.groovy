package volp

class InventoryAdvanceController {
    //InvAdvanceReqeust CRUD
    def addInvAdvanceRequest() {
        println(" i am addInvAdvanceRequest Controller")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invAdvanceRequestList = InvAdvanceRequest.findAllByRequestbyAndOrganization(instructor, org)
        def invPaymentMethodList = InvPaymentMethod.findAllWhere(organization: org, isactive: true)
        [invAdvanceRequestList: invAdvanceRequestList, invPaymentMethodList: invPaymentMethodList]
    }
    def saveInvAdvanceRequest() {
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        else {
            if (params.advanceRequestPaymentMethod == 'null') {
                flash.error = "Please select a payment method"
                redirect(action: 'addInvAdvanceRequest')
            } else {
                //println(" i am saveInvAdvanceRequest controler" + params)
                InvAdvanceService invAdvanceService = new InvAdvanceService()
                invAdvanceService.saveInvAdvanceRequest(params, session, request, flash)
                redirect(action: 'addInvAdvanceRequest')
            }

        }

    }
    def editInvAdvanceRequest() {
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        else {
            if (params.advanceRequestName == 'null' || params.advanceRequestPurpose == 'null') {
                flash.error = "Kindly fill all required Details While Editing"
                redirect(action: 'addInvAdvanceRequest')
                //println"Department is null";
            }
            else if(params.advanceRequestPaymentMethod == 'null' )
                {
                    flash.error = "Please select a payment method while editing"
                    redirect(action: 'addInvAdvanceRequest')
                }
             else {
                //println(" i am editInvAdvanceRequest controler" + params)
                InvAdvanceService invAdvanceService = new InvAdvanceService()
                invAdvanceService.editInvAdvanceRequest(params, session, request, flash)
                redirect(action: 'addInvAdvanceRequest')
            }

        }
    }
    def deleteInvAdvanceRequest() {
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        else {
                println(" i am deleteInvAdvanceRequest controller")
                InvAdvanceService invAdvanceService = new InvAdvanceService()
                invAdvanceService.deleteInvAdvanceRequest(params, session, request, flash)
                redirect(action: 'addInvAdvanceRequest')
            }

        }
    def activateInvAdvanceRequest() {
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        else {
            println(" i am activateInvAdvanceRequest controller")
            InvAdvanceService invAdvanceService = new InvAdvanceService()
            invAdvanceService.activateInvAdvanceRequest(params, session, request, flash)
            redirect(action: 'addInvAdvanceRequest')
        }
    }

//    //For Required Authority e.g:- HOD/Registrar/Bract etc
//    def approveInvAdvanceRequest() {
//        println(" i am approveInvAdvanceRequest Controller")
//        if (session.user == null)
//            redirect(controller: "login", action: 'erplogin')
//        Login login = Login.findById(session.loginid)
//        Instructor instructor = Instructor.findByUid(login.username)
//        def org = instructor.organization
//        def login_int_dept=instructor.department.name
//        //def invAdvanceRequestEscalationList=InvAdvanceRequestEscalation.findByActionbyAndOrganization(instructor,org)
//        def invApprovalStatusList=InvApprovalStatus.findAllByOrganization(org)
//        def invAdvanceRequestEscalationList = new ArrayList()
//        def invInstructoryFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactive(instructor,true)
//        for(auths in invInstructoryFinanceApprovingAuthority)
//        {
//                       /* println"------>"+auths?.instructor?.department.name
//                         println"------>"+auths?.instructor?.department.getClass()
//                         println"------>"+login_int_dept*/
//
//            if(auths?.instructor?.department.name == login_int_dept)
//            {
//                def invFinanceApprovingAuthority = auths?.invfinanceapprovingauthority
//                //println ("invFinanceApprovingAuthority"+ invFinanceApprovingAuthority)
//                def invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findAllWhere(invfinanceapprovingauthority:invFinanceApprovingAuthority,isactive:true)
//                for(level in invFinanceApprovingAuthorityLevel){
//                  //  println("Level-->"+ level)
//                    def invAdvanceRequestEscalation = InvAdvanceRequestEscalation.findByInvfinanceapprovingauthoritylevelAndIsactive(level,true)
//                  println("invAdvanceRequestEscalation-->"+ invAdvanceRequestEscalation)
//                    if(invAdvanceRequestEscalation != null){
//                        invAdvanceRequestEscalationList.push(invAdvanceRequestEscalation)
//                    }
//                }
//            }
//        }
//       // println("invAdvanceRequestEscalationList" +invAdvanceRequestEscalationList)
//
//        [invAdvanceRequestEscalationList:invAdvanceRequestEscalationList,invApprovalStatusList:invApprovalStatusList]
//    }
//    def updateAdvanceRequestApproval() {
//        println(" i am updateAdvanceRequestApproval Controller")
//        if (session.user == null)
//            redirect(controller: "login", action: 'erplogin')
//        InvAdvanceService invAdvanceService=new InvAdvanceService()
//        invAdvanceService.updateAdvanceRequestApproval(params,session,request,flash)
//        redirect(action: 'approveInvAdvanceRequest')
//    }


    //Updated flow-Advance Request approval
    def getInstAdvanceRequestApprove(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getInstAdvanceRequestApprove : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor, true, org)
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Advance Request", true, org)
            def invAdvanceRequestList = InvAdvanceRequest.findAllByOrganization(org).sort{it.isapproved}
            [invAdvanceRequestList:invAdvanceRequestList,invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority, instructor:instructor, invFinanceApprovingCategory:invFinanceApprovingCategory]
        }
    }
    def getallAdvanceRequestdetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getallAdvanceRequestdetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Advance Request", true, org)
            InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.post)
           InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByInvfinanceapprovingcategoryAndInvfinanceapprovingauthorityAndOrganizationAndIsactive(invFinanceApprovingCategory, invInstructorFinanceApprovingAuthority.invfinanceapprovingauthority, org, true)
            if (!invFinanceApprovingAuthorityLevel) {
                render "<div clas='alert alert-danger'>Please fill master of InvFinanceApprovingAuthorityLevel for category : " + invFinanceApprovingCategory.name + "</div>"
                return
            }
            //def invAdvanceRequestEscalation_List = InvAdvanceRequestEscalation.findAllByOrganizationAndInvfinanceapprovingauthoritylevelAndIsactive(org, invFinanceApprovingAuthorityLevel, true)
            def invAdvanceRequest = InvAdvanceRequest.findById(params.invAdvanceId)
            def invAdvanceRequestEscalation_List = InvAdvanceRequestEscalation.findAllByOrganizationAndInvfinanceapprovingauthoritylevelAndIsactiveAndInvadvancerequest(org, invFinanceApprovingAuthorityLevel, true,invAdvanceRequest)
            if (invAdvanceRequestEscalation_List.size() == 0) {
                render "<div class='alert alert-danger'>Advance Request not found for Approval</div>"
                return
            }

            [invAdvanceRequestEscalation_List     : invAdvanceRequestEscalation_List,
             invFinanceApprovingCategory           : invFinanceApprovingCategory,
             invFinanceApprovingAuthorityLevel     : invFinanceApprovingAuthorityLevel,
             invInstructorFinanceApprovingAuthority: invInstructorFinanceApprovingAuthority]
        }
    }
    def getAdvanceRequestEscalationforApprove(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In getAdvanceRequestEscalationforApprove : "+params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invapprovalstatus_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org, true)
            def next_level_approved = false
            InvAdvanceRequestEscalation invAdvanceRequestEscalation = InvAdvanceRequestEscalation.findById(params.advanceReqId)
            def advanceReqEscalations = InvAdvanceRequestEscalation.findAllByInvadvancerequest(invAdvanceRequestEscalation?.invadvancerequest)
            for (ARE in advanceReqEscalations){
                if(ARE?.invfinanceapprovingauthoritylevel?.islast == true) {
                    if (ARE?.invapprovalstatus?.name == 'Approved') {
                        next_level_approved = true
                        if(ARE == invAdvanceRequestEscalation){
                            next_level_approved = false
                        }
                    }
                }
            }
            println("next_level_approved :: " + next_level_approved)
            if(invAdvanceRequestEscalation) {
                def advanceReqEscalations_List = InvAdvanceRequestEscalation.findAllByOrganizationAndInvadvancerequestAndIsactive(org, invAdvanceRequestEscalation?.invadvancerequest, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [advanceReqEscalations_List:advanceReqEscalations_List, invAdvanceRequestEscalation : invAdvanceRequestEscalation, invapprovalstatus_list:invapprovalstatus_list, next_level_approved:next_level_approved]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }
    def saveApproveAdvanceRequest(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveApproveAdvanceRequest : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            InvApprovalStatus invApprovalStatus =  InvApprovalStatus.findById(params.approvingstatus)
            Double sanctionedAmount = 0
            if(params.approvedamount == 'null' || params.approvedamount == null || params.approvedamount == '')
            {
                return
            }else{
                sanctionedAmount = Double.parseDouble(params.approvedamount)
            }

            InvAdvanceRequestEscalation invAdvanceRequestEscalation = InvAdvanceRequestEscalation.findById(params.advanceReqId)
            if(invAdvanceRequestEscalation) {
                invAdvanceRequestEscalation.action_date = new java.util.Date()
                invAdvanceRequestEscalation.remark = params.remark
                invAdvanceRequestEscalation.sanctioned_amount = sanctionedAmount
                invAdvanceRequestEscalation.isactive = true
                invAdvanceRequestEscalation.actionby = instructor
                invAdvanceRequestEscalation.updation_username = login.username
                invAdvanceRequestEscalation.updation_date = new java.util.Date()
                invAdvanceRequestEscalation.updation_ip_address = request.getRemoteAddr()
                invAdvanceRequestEscalation.invapprovalstatus = invApprovalStatus
                invAdvanceRequestEscalation.save(flush: true, failOnError: true)
                if(invAdvanceRequestEscalation?.invfinanceapprovingauthoritylevel?.islast) {
                    InvAdvanceRequest invAdvanceRequest = invAdvanceRequestEscalation.invadvancerequest

                    if(invApprovalStatus?.name == "Rejected")
                    {
                        invAdvanceRequest.isapproved = false
                        invAdvanceRequest.sanctioned_amount = 0
                        invAdvanceRequest.invapprovalstatus = invApprovalStatus

                    }
                    else if(invApprovalStatus?.name == "Approved")
                    {
                        invAdvanceRequest.sanctioned_amount = sanctionedAmount
                        invAdvanceRequest.isapproved = true
                        invAdvanceRequest.invapprovalstatus = invApprovalStatus
                    }
                    else if(invApprovalStatus?.name == "In-Process")
                    {
                        invAdvanceRequest.isapproved = false
                        invAdvanceRequest.sanctioned_amount = 0
                        invAdvanceRequest.invapprovalstatus = invApprovalStatus
                    }

                    invAdvanceRequest.updation_username = login.username
                    invAdvanceRequest.updation_date = new Date()
                    invAdvanceRequest.updation_ip_address = request.getRemoteAddr()
                    invAdvanceRequest.save(flush: true, failOnError: true)
                }
                def advanceReqEscalations_List = InvAdvanceRequestEscalation.findAllByOrganizationAndInvadvancerequestAndIsactive(org, invAdvanceRequestEscalation.invadvancerequest, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [advanceReqEscalations_List:advanceReqEscalations_List, invAdvanceRequestEscalation : invAdvanceRequestEscalation]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }

    //Approval History
    def advanceRequestApprovalHistory() {
        println(" i am advanceRequestApprovalHistory "+params.id)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def inAdvanceRequest=InvAdvanceRequest.findById(params.id)
        def invAdvanceRequestEscalationList=InvAdvanceRequestEscalation.findAllByInvadvancerequestAndOrganization(inAdvanceRequest,org)
        [invAdvanceRequestEscalationList:invAdvanceRequestEscalationList,inAdvanceRequest:inAdvanceRequest]
    }

    //Advance receipt
    def addInvAdvanceReceipt(){
        println(" i am addInvAdvanceReceipt Controller")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def instructorList=Instructor.findAllByOrganization(org)
        def advanceInstructor=Instructor.findById(params.advanceInstructor)
        def advancesList=InvAdvanceRequest.findAllWhere(organization: org,isactive: true,requestby: advanceInstructor)
        
        ArrayList invAdvanceArray =new ArrayList()
        for(advanceRequest in advancesList)
        {
            HashMap hm = new HashMap<>()
            def invAdvanceReceipt=InvAdvanceRequestReceipt.findByOrganizationAndInvadvancerequestAndIsactive(org,advanceRequest,true)
            if(invAdvanceReceipt != null){
                hm.put("reciept",invAdvanceReceipt)
            }else{
                hm.put("reciept",null)
            }
            hm.put("data",advanceRequest)
            invAdvanceArray.push(hm)
        }
        //println"----->"+invAdvanceArray
        [instructorList:instructorList,invAdvanceRequestList:invAdvanceArray]
    }
    def generateAdvanceReceipt() {
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        println" i am generateAdvanceReceipt Controller"+params
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invAdvanceRequest=InvAdvanceRequest.findById(params.id)
        def invAdvanceReceipt=InvAdvanceRequestReceipt.findByInvadvancerequest(invAdvanceRequest)
        def invAdvancePaymentMethodList=InvPaymentMethod.findAllWhere(organization: org,isactive: true)
        [invAdvanceReceipt:invAdvanceReceipt,invAdvanceRequest:invAdvanceRequest,invAdvancePaymentMethodList:invAdvancePaymentMethodList]
    }
    def saveAdvanceReceipt(){
        //String fromdate = params.fromdate
        //Date date = new SimpleDateFormat("yyyy-MM-dd").parse(fromdate);
        //guardianMeetingScheduleMaster.fromdate = date
        println" i am generateAdvanceReceipt Controller"+params
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        if(params.receiptTransactionDate==null || params.receiptTransactionDate== 'null' || params.receiptTransactionDate=='' ||
          params.receiptamountTransferred == null || params.receiptamountTransferred == 'null' || params.receiptamountTransferred == '' ||
          params.receiptPaymentMethod == null || params.receiptPaymentMethod == 'null' || params.receiptPaymentMethod == '')
        {
            flash.error="please fill the required details"
            redirect(action: 'generateAdvanceReceipt',id:params.advanceRequestId)
        }
        else{
            InvAdvanceService invAdvanceService=new InvAdvanceService()
            invAdvanceService.saveAdvanceReceipt(params,request,session,flash)
            redirect(action: 'addInvAdvanceReceipt')
        }
    }
    def viewAdvanceReceipt(){
        println" i am viewAdvanceReceipt Controller"+params
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        InvAdvanceRequest invAdvanceRequest=InvAdvanceRequest.findById(params.id)
        def invAdvanceReceipt=InvAdvanceRequestReceipt.findByInvadvancerequest(invAdvanceRequest)
        [invAdvanceReceipt:invAdvanceReceipt]
    }

    //Advance Settlement
    def addInvAdvanceSettlement(){
        println(" i am addInvAdvanceSettlement Controller")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def instructorList=Instructor.findAllByOrganization(org)
        //def advanceInstructor=Instructor.findById(params.advanceInstructor)
        Instructor advanceInstructor=Instructor.findById(instructor?.id)
        def instName = advanceInstructor?.employee_code +" - "+advanceInstructor?.person?.firstName + " " + advanceInstructor?.person?.lastName
        def invAdvanceRequestList=InvAdvanceRequest.findAllByOrganizationAndRequestby(org,advanceInstructor)
        def paymentModeList=InvPaymentMethod.findAllByOrganizationAndIsactive(org,true)
        ArrayList InvAdvanceArray =new ArrayList()
        for(advanceRequest in invAdvanceRequestList)
        {
            HashMap hm = new HashMap()
            def invAdvanceReceipt=InvAdvanceRequestReceipt.findByOrganizationAndInvadvancerequestAndIsactive(org,advanceRequest,true)
            println"invAdvanceReceipt------->"+invAdvanceReceipt
            if(invAdvanceReceipt != null){
                hm.put("reciept",invAdvanceReceipt)
            }
            else{
                hm.put("reciept",null)
            }
            hm.put("data",advanceRequest)

            InvAdvanceArray.push(hm)
        }
        //println"Array------>"+InvAdvanceArray
       [instName:instName,instructorList:instructorList,InvAdvanceArray:InvAdvanceArray,paymentModeList:paymentModeList]
    }
    def addInvAdvanceProof(){
        println" i am addInvAdvanceProof Controller"+params
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        //def invAdvanceReceipt=InvAdvanceRequestReceipt.findAllByOrganization(org)
        def paymentModeList=InvPaymentMethod.findAllByOrganizationAndIsactive(org,true)
        def invAdvnaceRequestList=InvAdvanceRequest.findAllByOrganizationAndIsapprovedAndRequestby(org,true,instructor)
        [invAdvnaceRequestList:invAdvnaceRequestList,paymentModeList:paymentModeList]
    }
    def saveInvAdvanceProof(){
        println(" i am saveInvAdvanceProof Controller")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        else{
            InvAdvanceService invAdvanceService=new InvAdvanceService()
            invAdvanceService.saveInvAdvanceProof(params,session,request,flash)
            def id=InvAdvanceRequestReceipt.findById(params.advanceReceiptId)?.id
            redirect(action: 'addInvAdvanceProof',id:id)
        }

    }
    def editInvAdvanceProof(){
        println(" i am editInvAdvanceProof Controller")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        else{
            InvAdvanceService invAdvanceService=new InvAdvanceService()
            invAdvanceService.editInvAdvanceProof(params,session,request,flash)
            def id=InvAdvanceRequestReceipt.findById(params.advanceReceiptId)?.id
            println("receipt id"+ id)
            redirect(action: 'addInvAdvanceProof',id:id)
        }

    }
    def activateInvAdvanceProof() {
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        else {
            println(" i am activateInvAdvanceProof controller")
            InvAdvanceService invAdvanceService = new InvAdvanceService()
            invAdvanceService.activateInvAdvanceProof(params, session, request, flash)
            def id=InvAdvanceRequestReceipt.findById(params.advanceReceiptId)?.id
            redirect(action: 'addInvAdvanceProof',id:id)
        }
    }
    def viewProofUsingAdvanceRequest(){
        println(" i am viewProofUsingAdvanceRequest Controller")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        def invAdvanceRequest=InvAdvanceRequest.findById(params.id)
        println("invAdvanceRequest--> "+invAdvanceRequest )
        //def invAdvanceRequestReceipt=InvAdvanceRequestReceipt.findByInvadvancerequest(invAdvanceRequest)
        def id=invAdvanceRequest?.id
        //println("invAdvanceRequestReceipt--> "+invAdvanceRequestReceipt+ "id --> "+id)
        redirect(action: 'viewProofs',id: id)
    }
    def viewProofs(){
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        else
        {
            println" i am viewProofs Controller"+params
            InvAdvanceRequest invAdvanceRequest=InvAdvanceRequest.findById(params.id)
            def invAdvanceSettlementList=InvAdvanceSettlement.findAllWhere(invadvancerequest:invAdvanceRequest)
            ArrayList array = new ArrayList()
            AWSBucketService awsBucketService = new AWSBucketService()
            AWSBucket aws = AWSBucket.findByContent("documents")
            for(i in invAdvanceSettlementList){
                HashMap hm=new HashMap()
                def awsimagelink = i.file_path
                String file = awsBucketService.getPresignedUrl(aws.bucketname,awsimagelink, aws.region)
                hm.put("url",file)
                hm.put("data",i)
                array.add(hm)
                //println"array------>"+array
            }
            [invAdvanceSettlementList:array,invAdvanceRequest:invAdvanceRequest]
        }
    }

    //Advance Closing
    def addAdvanceClosing(){
        println(" i am addAdvanceClosing Controller")
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def instructorList=Instructor.findAllWhere(organization: org)
        def advanceInstructor=Instructor.findById(params.advanceInstructor)
        def invAdvanceRequestList=InvAdvanceRequest.findAllWhere(organization: org,isactive: true,requestby: advanceInstructor)
        ArrayList advanceReceiptList=new ArrayList()
        for(advancerequest in invAdvanceRequestList)
        {
            HashMap hm = new HashMap()
           InvAdvanceRequestReceipt invAdvanceRequestReceipt=InvAdvanceRequestReceipt.findByInvadvancerequest(advancerequest)
            if(invAdvanceRequestReceipt!=null)
            {
                hm.put("data",invAdvanceRequestReceipt)
            }
            hm.put("advancerequest",advancerequest)
            advanceReceiptList.push(hm)
        }
        //println"------>"+advanceReceiptList
        [instructorList:instructorList,advanceReceiptList:advanceReceiptList]
    }


}