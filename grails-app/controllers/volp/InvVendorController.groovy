package volp

class InvVendorController {

    def index() { }

   /* def addInvVendorRating(){
        println(" i am addInvVendorRating")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

        def invVendorRating = InvVendorRating.findAllWhere(organization: org)
        def invVendorList = InvVendor.findAllWhere(organization: org)
        def invVendorCompanyList = invVendorList.company_number
        [invVendorRating:invVendorRating,invVendorList:invVendorList,invVendorCompanyList:invVendorCompanyList]
    }
    def saveInvVendorRating(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
            if (params.company_number == 'null') {
                flash.message = "Kindly fill all required Details"
                redirect(action: 'addInvVendorRating')
              } else {
                    InvVendorService invVendorService = new InvVendorService()
                    invVendorService.saveInvVendorRating(params, session, request, flash)
                    redirect(action: 'addInvVendorRating')
                }
        }
    }
    def showInvVendorRating(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

            if (params.company_number == 'null') {
                flash.message = "Kindly Select Vendor"
                redirect(action: 'addInvVendorRating')
            }else{
                def invVendorRating = InvVendorRating.findAllWhere(organization: org)
                InvVendorService invVendorService1 = new InvVendorService()
                invVendorService1.showInvVendorRating(session,request,flash,params)
            }

        }
    }*/


    //Vendor Rating
    def addInvVendorRating(){
        println(" i am addInvVendorRating")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

        def invVendorRating = InvVendorRating.findAllWhere(organization: org)
        def invVendorList = InvVendor.findAllWhere(organization: org)
        def invVendorCompanyList = invVendorList.company_name
        [invVendorRating:invVendorRating,invVendorList:invVendorList,invVendorCompanyList:invVendorCompanyList]
    }
    def saveInvVendorRating(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
            if (params.company_name == 'null') {
                flash.message = "Kindly Select Company Name"
                redirect(action: 'addInvVendorRating')
            } else {
                InvVendorService invVendorService = new InvVendorService()
                invVendorService.saveInvVendorRating(params, session, request, flash)
                redirect(action: 'addInvVendorRating')
            }
        }
    }
    def showInvVendorRating(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization

            def vendorlist = InvVendor.findAllByIsactiveAndOrganization(true, org)

            def vendorratinglist = InvVendorRating.findAllByOrganizationAndInvvendorInList(org, vendorlist)

            [vendorlist:vendorlist, vendorratinglist:vendorratinglist]
            /*if (params.company_name == 'null') {
                flash.message = "Kindly Select Company Name"
                redirect(action: 'showInvVendorRating')
            }else{
                def invVendorRating = InvVendorRating.findAllWhere(organization: org)
                InvVendorService invVendorService1 = new InvVendorService()
                invVendorService1.showInvVendorRating(session,request,flash,params)
            }*/
        }
    }

    def fetchvendorratinglist() {
        println("fetchvendorratinglist :: " + params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            if (params.company_name == "" || params.company_name == 'null') {
                render "<div class='alert alert-danger'>Please select Vendor...</div>"
                return
            }
            InvVendor invVendor = InvVendor.findByCompany_name(params.company_name)
            //InvVendor invVendor = InvVendor.findById(params.company_name)
            if (!invVendor) {
                render "<div class='alert alert-danger'>Please select Vendor...</div>"
                return
            }
            def vendorratinglist = InvVendorRating.findAllByOrganizationAndInvvendor(org, invVendor)
            if (vendorratinglist.size() == 0) {
                render "<div class='alert alert-danger'>No Feedback available for Vendor..." + invVendor.company_name + "</div>"
                return
            }
            [vendorratinglist: vendorratinglist]
        }
    }

    //Import Vendor
    def importInvVendor(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')

        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
//        def invvendor = InvVendor.findAllByOrganizationAndCreation_username(org,instructor.uid)
//        println("invvendor :"+invvendor)
//        [invvendor:invvendor]
        def invMaterialCategoryList = InvMaterialCategory.findAllWhere(organization:org)
        [params:params, invMaterialCategoryList:invMaterialCategoryList]
    }
    def saveImportedInvVendor(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
            Login login = Login.findById(session.loginid)
            println("login in controller---"+login)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            println("saveImportedInvVendor controller ::"+params)

            InvVendorService invVendorService = new InvVendorService()
            def final_list = invVendorService.saveImportedInvVendor(params, session, request, flash)

            if(final_list == -1) {
                flash.error = "File is empty."
                redirect(action: 'importInvVendor')
                return
            }
            [failedlist:final_list?.failedlist, successlist:final_list?.successlist]
        }
    }
    def activateImportedInvVendor(){
        println(" i am activateImportedInvVendor")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            InvVendorService invVendorService = new InvVendorService()
            invVendorService.activateImportedInvVendor(params,session,request,flash)
            redirect(action: 'showInvVendors')
        }
    }
    def showInvVendors(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            def org = instructor.organization
            def invVendorList = InvVendor.findAllWhere(organization:org)
            [invVendorList:invVendorList]
        }
    }

    def getVendorCategories() {
        println("getVendorCategories :: " + params)
        InvVendor invVendor = InvVendor.findById(params.id)
        if(invVendor) {
            def list = invVendor.invmaterialcategory
            if(list.size() > 0) {
                render list.name.join(', ')
                return
            } else {
                render "-"
                return
            }
        } else {
            render "-"
            return
        }
    }
    def saveSingleVendor(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
            println("saveSingleVendor controller ::"+params)
            InvVendorService invVendorService = new InvVendorService()
            invVendorService.saveSingleVendor(params, session, request, flash)
            redirect(action: 'importInvVendor')
        }
    }

    def viewPR(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        println(" i am viewPR"+params)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = login.organization
        //def invPRList = InvPurchaseRequisition.findAllWhere(organization: org,isapproved: true)
        def invPRList = InvPurchaseRequisition.findAllWhere(organization: org,isapproved: true,invvendor: null)
        def PT = InvPurchaseType.findByName("Direct")
        def directPRList = InvPurchaseRequisition.findAllWhere(invvendor:vendor,isapproved: true,invpurchasetype:PT)

        //Directly pusing Direct invites to whole PRList
        for(items in directPRList){
            invPRList.push(items)
        }
        //Comparing PR objects then push in main list
        /*
        if(directPRList.size() > 0){
            for(IPR in invPRList){
                for(DPR in directPRList){
                    if( IPR?.equals(DPR) ){
                        println"Duplicate Found-->" + DPR
                    }else{
                        println"Unique Found-->" + DPR
                        //invPRList.push(DPR)
                    }
                }
            }
        }else{
            //Do nothing
        }*/

        [invPRList:invPRList]
    }

    //add quotations
    def addQuotations(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            redirect(action: 'addSinglePRQuotation',params:params)
        }
    }//just to redirect
    def addSinglePRQuotation(params){
        println(" i am addSinglePRQuotation"+params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        //println"sessin-->"+session
        def invPR = InvPurchaseRequisition.findById(params.invPRId)
        def vendor = InvVendor.findByContact_person_email(session.user)
        def invQuotations = InvQuotation.findAllWhere(invpurchaserequisition:invPR,invvendor:vendor)
        ArrayList quotationList = new ArrayList()
        HashMap hm = new HashMap()
        for(i in invQuotations)
        {
            def quoEscalations = InvQuotationEscalation.findAllByInvquotation(i)
            hm.put("data",i)
            hm.put("escalation",quoEscalations)
            quotationList.add(hm)
        }
        //println"quotationList------->"+quotationList
        [invQuotations:quotationList,invPRId:params.invPRId]
    }
    def savePRQuotation(){
        println"savePRQuotation params"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.invPRId == 'null' || params.tax == 'null')
            {
                flash.error = "Kindly fill all required Details"
            }
            else
            {
                Login login = Login.findById(session.loginid)
                InvVendor vendor = InvVendor.findByContact_person_email(login.username)
                def org = login.organization
                def invApprovalStatus = InvApprovalStatus.findByNameAndIsactive("In-Process",true)
                def invPR = InvPurchaseRequisition.findById(params.invPRId)
                def amt = java.lang.Double.parseDouble(params.amount)
                def tax = java.lang.Double.parseDouble(params.tax)
                def invQuote = InvQuotation.findByInvpurchaserequisitionAndInvvendorAndOrganization(invPR,vendor,org)
                if(invQuote == null) {
                    invQuote = new InvQuotation()
                    invQuote.amount = amt
                    invQuote.tax = tax
                    //invQuote.total = java.lang.Double.parseDouble(params.total)
                    invQuote.total = (amt + (amt * tax)/100)
                    invQuote.invvendor = vendor
                    invQuote.invpurchaserequisition = invPR
                    invQuote.quotation_date = new java.util.Date()
                    invQuote.isapproved = false
                    invQuote.isactive = true
                    invQuote.organization = org

                    invQuote.creation_username = vendor.contact_person_name
                    invQuote.updation_username = vendor.contact_person_name
                    invQuote.updation_ip_address = request.getRemoteAddr()
                    invQuote.creation_ip_address = request.getRemoteAddr()
                    invQuote.updation_date = new java.util.Date()
                    invQuote.creation_date = new java.util.Date()
                    invQuote.save(flush: true, failOnError: true)
                    flash.message = "Succesfully Added,Please Add Quotation Details"

                    def invFinanceApprovingCategory = InvFinanceApprovingCategory.findByOrganizationAndNameAndIsactive(org, "Quotation", true)
                    def invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findAllWhere(organization: org, isactive: true, invfinanceapprovingcategory: invFinanceApprovingCategory)
                    //println"invFinanceApprovingAuthorityLevel-------->"+invFinanceApprovingAuthorityLevel
                    for (authLevels in invFinanceApprovingAuthorityLevel) {
                        InvQuotationEscalation invQuotationEscalation = InvQuotationEscalation.findByInvquotationAndInvfinanceapprovingauthoritylevel(invQuote,authLevels)
                        if (invQuotationEscalation == null) {
                            invQuotationEscalation = new InvQuotationEscalation()

                            invQuotationEscalation.action_date = new java.util.Date()
                            invQuotationEscalation.organization = org
                            invQuotationEscalation.invquotation = invQuote
                            invQuotationEscalation.invpurchaserequisition = invPR
                            invQuotationEscalation.invfinanceapprovingauthoritylevel = authLevels
                            invQuotationEscalation.invapprovalstatus = invApprovalStatus
                            //invQuotationEscalation.actionby = vendor
                            invQuotationEscalation.isactive = true

                            invQuotationEscalation.creation_username = vendor?.contact_person_name
                            invQuotationEscalation.updation_username = vendor?.contact_person_name
                            invQuotationEscalation.updation_ip_address = request.getRemoteAddr()
                            invQuotationEscalation.creation_ip_address = request.getRemoteAddr()
                            invQuotationEscalation.updation_date = new java.util.Date()
                            invQuotationEscalation.creation_date = new java.util.Date()

                            invQuotationEscalation.save(flush: true, failOnError: true)
                            println "saving invQuotationEscalation for Authority Level " + authLevels?.level_no
                        } else {
                            continue
                        }
                    }
                }else
                {
                    flash.error = "Quotation Already Submitted,Please Edit"
                }
            }
        }
        redirect(action: 'addSinglePRQuotation',params:[invPRId: params.invPRId])
    }
    def activatePRQuotation(){
        println"params"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.quotationId == 'null')
            {
                flash.error = "Please Re-try"
            }
            else
            {
                def invVendor = InvVendor.findById("1")
                InvQuotation invQuotation = InvQuotation.findById(params.invQuotationId)
                def quotationEscalations = InvQuotationEscalation.findAllWhere(invquotation:invQuotation)
            if(invQuotation) {
                if(invQuotation.isactive) {
                    invQuotation.isactive = false
                    invQuotation.updation_username = invVendor.contact_person_name
                    invQuotation.updation_date = new Date()
                    invQuotation.updation_ip_address = request.getRemoteAddr()
                    invQuotation.save(flush: true, failOnError: true)
                    for(qe in quotationEscalations){
                        qe.isactive = false
                        qe.save(flush: true,failOnError: true)
                    }
                    flash.message = "Successful.."
                } else {
                    invQuotation.isactive = true
                    invQuotation.updation_username = invVendor.contact_person_name
                    invQuotation.updation_date = new Date()
                    invQuotation.updation_ip_address = request.getRemoteAddr()
                    invQuotation.save(flush: true, failOnError: true)
                    for(qe in quotationEscalations){
                        qe.isactive = true
                        qe.save(flush: true,failOnError: true)
                    }
                    flash.message = "Successful.."
                }
            } else {
                flash.error = "Quotation Not found"
            }
            }
        }
        redirect(action: 'viewPR')
    }

    //add quotation details
    def addQuotationsDetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            redirect(action: 'addPRQuotationDetails',params:params)
        }
    }//just to redirect
    def addPRQuotationDetails(params){
        println"i am addPRQuotationDetails"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            def invQuotation = InvQuotation.findById(params.InvQuotationId)
            def isapproved = invQuotation?.isapproved
            def quotationDetailsList = InvQuotationDetails.findAllWhere(invquotation:invQuotation)
            def materialList = InvMaterial.findAllWhere(organization: invQuotation?.organization,isactive: true)
            [isapproved:isapproved,quotationDetailsList:quotationDetailsList,materialList:materialList,invQuotation:invQuotation.id]
        }
    }
    def saveQuotationDetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.materialId == 'null' || params.costPerUnit == 'null' || params.quantity == 'null' || params.invQuotationId == 'null')
            {
                flash.error = "Please Fill all details"
            }
            else{
                println"i am saveQuotationDetails" + params
                Login login = Login.findById(session.loginid)
                //InvVendor vendor = InvVendor.findByContact_person_email(login.username)
                InvQuotation invQuotation = InvQuotation.findById(params.invQuotationId)
                def invMaterial = InvMaterial.findById(params.materialId)
                def invQuotationDetails = InvQuotationDetails.findByInvmaterialAndInvquotationAndIsactive(invMaterial,invQuotation,true)
                //println"-->invQuotationDetails"+invQuotationDetails
                if(invQuotationDetails == null){
                    invQuotationDetails = new InvQuotationDetails()
                    invQuotationDetails.quantity = Double.parseDouble(params.quantity)
                    invQuotationDetails.cost_per_unit = Double.parseDouble(params.costPerUnit)
                    invQuotationDetails.total_cost = Double.parseDouble(params.quantity) * Double.parseDouble(params.costPerUnit)
                    invQuotationDetails.isactive = true
                    invQuotationDetails.organization = login?.organization
                    invQuotationDetails.invquotation = invQuotation
                    invQuotationDetails.invmaterial = invMaterial

                    invQuotationDetails.creation_username = login.username
                    invQuotationDetails.updation_username = login.username
                    invQuotationDetails.updation_ip_address = request.getRemoteAddr()
                    invQuotationDetails.creation_ip_address = request.getRemoteAddr()
                    invQuotationDetails.updation_date = new java.util.Date()
                    invQuotationDetails.creation_date = new java.util.Date()
                    invQuotationDetails.save(flush: true,failOnError: true)
                    flash.message = "Succesfully Added"
                }else{
                    flash.error = "Already Present,Please Edit"
                }
            }
        }
        redirect(action: "addPRQuotationDetails",params:[InvQuotationId: params.invQuotationId])
    }
    def activateQuotationDetails(){
        println"params"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.quotationId == 'null')
            {
                flash.error = "Please Re-try"
            }
            else
            {
                Login login = Login.findById(session.loginid)
                //InvVendor invVendor = InvVendor.findByContact_person_email(login.username)
                InvQuotationDetails invQuotationDetails = InvQuotationDetails.findById(params.invQuotationDetailsId)
                if(invQuotationDetails) {
                    if(invQuotationDetails.isactive) {
                        invQuotationDetails.isactive = false
                        invQuotationDetails.updation_username = login.username
                        invQuotationDetails.updation_date = new Date()
                        invQuotationDetails.updation_ip_address = request.getRemoteAddr()
                        invQuotationDetails.save(flush: true, failOnError: true)
                        flash.message = "Successful.."
                    } else {
                        invQuotationDetails.isactive = true
                        invQuotationDetails.updation_username = login.username
                        invQuotationDetails.updation_date = new Date()
                        invQuotationDetails.updation_ip_address = request.getRemoteAddr()
                        invQuotationDetails.save(flush: true, failOnError: true)
                        flash.message = "Successful.."
                    }
                } else {
                    flash.error = "Quotation Details Not found"
                }
            }
        }
        redirect(action: "addPRQuotationDetails",params:[InvQuotationId: params.invQuotationId])
        //redirect(action: 'viewPR')
    }
    def editPRQuotationDetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.materialId == 'null' || params.costPerUnit == 'null' || params.quantity == 'null' || params.invQuotationId == 'null')
            {
                flash.error = "Please Fill all details"
            }
            else{
                println"i am editPRQuotationDetails" + params
                Login login = Login.findById(session.loginid)
                //InvVendor vendor = InvVendor.findByContact_person_email(login.username)
                //InvQuotation invQuotation = InvQuotation.findById(params.invQuotationId)
                def invMaterial = InvMaterial.findById(params.materialId)
                def invQuotationDetails = InvQuotationDetails.findById(params.invQuotationDetailsId)
                if(invQuotationDetails != null){
                    invQuotationDetails.quantity = Double.parseDouble(params.quantity)
                    invQuotationDetails.cost_per_unit = Double.parseDouble(params.costPerUnit)
                    invQuotationDetails.total_cost = Double.parseDouble(params.quantity) * Double.parseDouble(params.costPerUnit)
                    if(params.isactive){
                        invQuotationDetails.isactive = true
                    }else
                        invQuotationDetails.isactive = false

                    invQuotationDetails.invmaterial = invMaterial

                    invQuotationDetails.updation_username = login.username
                    invQuotationDetails.updation_ip_address = request.getRemoteAddr()
                    invQuotationDetails.updation_date = new java.util.Date()

                    invQuotationDetails.save(flush: true,failOnError: true)
                    flash.message = "Succesfully Edited"
                }else{
                    flash.error = "Not Found"
                }
            }
        }
        redirect(action: "addPRQuotationDetails",params:[InvQuotationId: params.invQuotationId])
    }

    def viewQuotationHistory(){
        println(" i am viewQuotationHistory")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def invQuotationsList = InvQuotation.findAllWhere(invvendor:vendor)
        [invQuotationsList:invQuotationsList]
    }

    //add invoice
    def addInvoice(){
        println(" in addInvoice in InvVendor Controller")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = login.organization
        println"vendor---->"+vendor
        def invoice = Invoice.findAllWhere(organization: org,invvendor:vendor)
        def invPurchaseOrderList = InvPurchaseOrder.findAllWhere(organization: org,invvendor:vendor)
        def academicYearList = AcademicYear.findAllByIsactive(true).sort{it.ay}

        [academicYearList:academicYearList,invPurchaseOrderList:invPurchaseOrderList,invoice:invoice]

    }
    def getValuesByAcademicyearaddInvoice() {
        if (session.user == null)
            redirect(controller: "login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = login.organization
        println("getValuesByAcademicyearaddInvoice params : " + params)
        AcademicYear academicYear = AcademicYear.findByAy(params.ay)
        def invPurchaseRequisition = InvPurchaseRequisition.findAllByAcademicyearAndOrganization(academicYear,org)
        def invQuotationList = []
        for(PRItems in invPurchaseRequisition){
            def invQuotation= InvQuotation.findByInvpurchaserequisitionAndOrganization(PRItems,org)
            if(invQuotation != null){
                invQuotationList.push(invQuotation)
            }
        }
        def invPOList = []
        for(QuoItems in invQuotationList){
            def invPurchaseOrder = InvPurchaseOrder.findByInvquotationAndOrganizationAndInvvendor(QuoItems,org,vendor)
            if(invPurchaseOrder != null){
                invPOList.push(invPurchaseOrder)
            }
        }
        //println"invPOList--->"+invPOList
        [invPOList:invPOList]
    }
    def getValuesByPurchaseOrderNo(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = vendor.organization
        println("getValuesByPurchaseOrderNo : "+params)
        InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findById(params.poNo)
        def purpose = invPurchaseOrder?.purpose
        def pon = invPurchaseOrder?.pon
        def purchase_order_date = invPurchaseOrder?.purchase_order_date
        def amount = invPurchaseOrder?.amount
        //println"amount-->"+amount
        def tax = invPurchaseOrder?.tax
        def total = invPurchaseOrder?.total
        //println"total-->"+total
        def invoice = Invoice.findAllByOrganizationAndInvvendor(org,invPurchaseOrder?.invvendor)
        def bal_amount = total
        for(invItems in invoice){
            //println"in for loop"
            bal_amount = bal_amount - invItems.amount
        }
        //println"bal_amount-->"+bal_amount
        [bal_amount:bal_amount,purpose:purpose,purchase_order_date:purchase_order_date,amount:amount,tax:tax,total:total,pon:pon,invoice:invoice]
    }
    def saveInvoice(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
                println("save invoice controler :")
                if(params.poNo[0] == 'null' || params.ay == 'null'){
                    flash.message="Please select Academic Year Purchase Order Number "
                    redirect(action: 'addInvoice')
                }else{
                    InvVendorService invVendorService = new InvVendorService()
                    invVendorService.saveInvoice(params, session, request, flash)
                    redirect(action: 'addInvoice')
                }
        }
    }
    def activateInvoice(){
        println(" i am activateInvoice")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            InvVendorService invVendorService = new InvVendorService()
            invVendorService.activateInvoice(params,session,request,flash)
            redirect(action: 'addInvoice')
        }
    }
    def showInvoiceinAddInvoice(){
        println(" i am viewInvoice")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            println("params : "+params)
            def invoice1 = Invoice.findById(params.invoiceId)
            [invoice1:invoice1]
        }
    }
    def getInvoicelist(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = vendor.organization
        println("getInvoicelist : "+params)
        InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findById(params.poNo)
        def invoice = Invoice.findAllWhere(organization: org,invvendor: invPurchaseOrder?.invvendor)
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive: true)
        //println"invpoice-->"+invoice
        [invoice:invoice,invMaterialList:invMaterialList]
    }
    def saveInvoiceDetails(){
        println"saveInvoiceDetails--> " + params
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = vendor.organization
        InvMaterial invMaterial = InvMaterial.findById(params.materialId)
        def invoice = Invoice.findById(params.invoiceId)
        int qty = Integer.parseInt(params.quantity)
        double acpu = Double.parseDouble(params.costPerUnit)
        InvoiceDetails invoiceDetails = InvoiceDetails.findByInvoiceAndInvmaterialAndOrganization(invoice,invMaterial,org)
        if(invoiceDetails == null){
            invoiceDetails = new InvoiceDetails()
            invoiceDetails.quantity = qty
            invoiceDetails.available_qty = qty
            invoiceDetails.cost_per_unit = acpu
            invoiceDetails.total_cost = qty * acpu
            //invoiceDetails.remark = params.remarks
            invoiceDetails.invoice = invoice
            invoiceDetails.invmaterial = invMaterial
            invoiceDetails.organization = org
            invoiceDetails.isactive = true
            invoiceDetails.creation_username = vendor?.contact_person_name
            invoiceDetails.updation_username = vendor?.contact_person_name
            invoiceDetails.updation_ip_address = request.getRemoteAddr()
            invoiceDetails.creation_ip_address = request.getRemoteAddr()
            invoiceDetails.updation_date = new java.util.Date()
            invoiceDetails.creation_date = new java.util.Date()

            invoiceDetails.save(flush: true, failOnError: true)
            flash.message = "Saved Successfully"
        }else{
            flash.error = "Duplicate Entry"
        }
        redirect(action: 'addInvoice')
    }
    def viewInvoiceDetailsByInvoice(){
        println"viewInvoiceDetailsByInvoice-->"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invoice = Invoice.findById(params.invoiceId)
        println"invoice-->"+invoice
        def invoiceDetailsList = InvoiceDetails.findAllByInvoice(invoice)
        println"invoiceDetails-->"+invoiceDetailsList
        [invoiceDetailsList:invoiceDetailsList]
    }

    //approve Vendor
    def approveVendor(){
        println(" in approveVendor in InvVendor Controller")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def vendorList = InvVendor.findAllWhere(organization: org)
        [vendorList:vendorList]
    }
    def activateVendor(){
        println"activateVendor--->"+params
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        //def org = instructor.organization
        def vendor = InvVendor.findById(params.vendorId)
        if(vendor) {
            if(vendor.isactive) {
                vendor.isactive = false
                vendor.updation_username = instructor.uid
                vendor.updation_date = new java.util.Date()
                vendor.updation_ip_address = request.getRemoteAddr()
                vendor.save(flush: true, failOnError: true)
                flash.message = "Successfully Updated"
            } else {
                vendor.isactive = true
                vendor.updation_username = instructor.uid
                vendor.updation_date = new java.util.Date()
                vendor.updation_ip_address = request.getRemoteAddr()
                vendor.save(flush: true, failOnError: true)
                flash.message = "Successfully Updated"
            }
        } else {
            flash.error = "Vendor type  Not found"
        }
        redirect(controller: "invVendor", action: "approveVendor")

    }


}
