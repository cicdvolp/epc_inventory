package volp
import javax.servlet.http.Part
import java.nio.file.Paths
import java.time.format.DateTimeFormatter
import java.text.SimpleDateFormat

class EntranceApplicantController {

    def index() {
        println("index :: " + params)
        println("index :: " + session)

        EntranceApplicant login = EntranceApplicant.findById(session.loginid)
        if(!login) {
            flash.error = "Your session is logged out, Please login again."
            redirect (controller: "entranceApplicantLogin", action: "login")
            return
        }

        EntranceStudentProfileTabMaster applicant_profile_master = EntranceStudentProfileTabMaster.findByName("applicant_profile")
        EntranceStudentProfileTabMaster application_form_master = EntranceStudentProfileTabMaster.findByName("applicant_form")
        EntranceStudentProfileTabMaster application_form_fees_master = EntranceStudentProfileTabMaster.findByName("applicant_form_fees")
        EntranceStudentProfileTabMaster application_track_master = EntranceStudentProfileTabMaster.findByName("application_track")
        EntranceStudentProfileTabMaster application_exam_master = EntranceStudentProfileTabMaster.findByName("entrance_exam")
        EntranceStudentProfileTabMaster applicant_profile_personal_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_personal_tab")
        EntranceStudentProfileTabMaster applicant_profile_contact_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_contact_tab")
        EntranceStudentProfileTabMaster applicant_profile_education_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_education_tab")
        EntranceStudentProfileTabMaster applicant_profile_experience_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_experience_tab")
        EntranceStudentProfileTabMaster applicant_profile_photo_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_photo_tab")
        EntranceStudentProfileTabMaster applicant_profile_sign_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_sign_tab")
        EntranceStudentProfileTabMaster applicant_profile_documents_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_document_tab")

        EntranceStudentProfileStatus applicant_profile_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_master, login)
        EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_personal_master, login)
        EntranceStudentProfileStatus applicant_profile_contact_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_contact_master, login)
        EntranceStudentProfileStatus applicant_profile_experience_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_experience_master, login)
        EntranceStudentProfileStatus applicant_profile_education_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_education_master, login)
        EntranceStudentProfileStatus applicant_profile_documents_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_documents_master, login)
        EntranceStudentProfileStatus applicant_profile_photo_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_photo_master, login)
        EntranceStudentProfileStatus applicant_profile_sign_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_sign_master, login)

        EntranceStudentProfileStatus application_form_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(application_form_master, login)
        EntranceStudentProfileStatus application_form_fees_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(application_form_fees_master, login)
        EntranceStudentProfileStatus application_track_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(application_track_master, login)
        EntranceStudentProfileStatus application_exam_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(application_exam_master, login)

        [applicant_profile_status:applicant_profile_status, applicant_profile_personal_status:applicant_profile_personal_status,
         applicant_profile_contact_status:applicant_profile_contact_status, applicant_profile_experience_status:applicant_profile_experience_status,
         applicant_profile_education_status:applicant_profile_education_status, applicant_profile_documents_status:applicant_profile_documents_status,
         applicant_profile_photo_status:applicant_profile_photo_status, applicant_profile_sign_status:applicant_profile_sign_status,
         application_form_status:application_form_status, application_form_fees_status:application_form_fees_status,
         application_track_status:application_track_status, application_exam_status:application_exam_status]
    }

    def eadashboard(){
        println"In eadashboard in EntranceApplicant Controler"
        EntranceApplicant login = EntranceApplicant.findById(session.loginid)
        if (login == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "EntranceAdmissionLogin", action: "login")
        } else {
            EntranceStudentProfileTabMaster applicant_profile_master = EntranceStudentProfileTabMaster.findByName("applicant_profile")
            EntranceStudentProfileTabMaster application_form_master = EntranceStudentProfileTabMaster.findByName("applicant_form")
            EntranceStudentProfileTabMaster application_form_fees_master = EntranceStudentProfileTabMaster.findByName("applicant_form_fees")
            EntranceStudentProfileTabMaster application_track_master = EntranceStudentProfileTabMaster.findByName("application_track")
            EntranceStudentProfileTabMaster application_exam_master = EntranceStudentProfileTabMaster.findByName("entrance_exam")
            EntranceStudentProfileTabMaster applicant_profile_personal_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_personal_tab")
            EntranceStudentProfileTabMaster applicant_profile_contact_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_contact_tab")
            EntranceStudentProfileTabMaster applicant_profile_education_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_education_tab")
            EntranceStudentProfileTabMaster applicant_profile_experience_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_experience_tab")
            EntranceStudentProfileTabMaster applicant_profile_photo_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_photo_tab")
            EntranceStudentProfileTabMaster applicant_profile_sign_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_sign_tab")
            EntranceStudentProfileTabMaster applicant_profile_documents_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_document_tab")

            EntranceStudentProfileStatus applicant_profile_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_master, login)
            EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_personal_master, login)
            EntranceStudentProfileStatus applicant_profile_contact_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_contact_master, login)
            EntranceStudentProfileStatus applicant_profile_experience_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_experience_master, login)
            EntranceStudentProfileStatus applicant_profile_education_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_education_master, login)
            EntranceStudentProfileStatus applicant_profile_documents_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_documents_master, login)
            EntranceStudentProfileStatus applicant_profile_photo_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_photo_master, login)
            EntranceStudentProfileStatus applicant_profile_sign_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_sign_master, login)

            EntranceStudentProfileStatus application_form_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(application_form_master, login)
            EntranceStudentProfileStatus application_form_fees_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(application_form_fees_master, login)
            EntranceStudentProfileStatus application_track_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(application_track_master, login)
            EntranceStudentProfileStatus application_exam_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(application_exam_master, login)

            [EA:login,
             applicant_profile_status:applicant_profile_status, applicant_profile_personal_status:applicant_profile_personal_status,
             applicant_profile_contact_status:applicant_profile_contact_status, applicant_profile_experience_status:applicant_profile_experience_status,
             applicant_profile_education_status:applicant_profile_education_status, applicant_profile_documents_status:applicant_profile_documents_status,
             applicant_profile_photo_status:applicant_profile_photo_status, applicant_profile_sign_status:applicant_profile_sign_status,
             application_form_status:application_form_status, application_form_fees_status:application_form_fees_status,
             application_track_status:application_track_status, application_exam_status:application_exam_status]
        }
    }

    def fillPersonalInformation(){
        println"In fillPersonalInformation in EntranceApplicant Controler"
        def EA = EntranceApplicant.findByEmail(session.email)
        if(EA == null){
            redirect(action: "login",controller: "EntranceAdmissionLogin")
        }
        else{
            def entranceCategoryList = EntranceCategory.findAllWhere(isactive:true)
            def maritalStatusList = MaritalStatus.findAllByIsactive(true)
            def employmentstatusList = EmploymentStatus.findAllWhere(isactive:true)
            def exemptionList = Exemption.findAllWhere(isactive:true)
            def exemptiontypeList = ExemptionType.findAllWhere(isactive:true)
            def nationalityList = ERPNationality.list()
            def erpdomacileList = ERPDomacile.list()
            def genderList = Gender.list()
            [EA:EA,entranceCategoryList:entranceCategoryList,maritalStatusList:maritalStatusList,genderList:genderList,
             nationalityList:nationalityList,erpdomacileList:erpdomacileList,employmentstatusList:employmentstatusList,
             exemptionList:exemptionList,exemptiontypeList:exemptiontypeList]
        }
    }
    def saveEAPersonalDetails(){
        println"In saveEAPersonalDetails"+params
        if(params.employmentstatus == 'null' || params.erpdomacile == 'null' || params.erpnationality == 'null' || params.EAID == 'null' || params.gender == 'null' || params.maritalstatus == 'null' || params.entrancecategory == 'null'){
            flash.error = "Please fill all details"
            redirect(action: 'eadashboard')
        }
        else{
            EntranceApplicant EA = EntranceApplicant.findById(params.EAID)
            if(EA != null){
                def gender = Gender.findById(params.gender)
                def MS = MaritalStatus.findById(params.maritalstatus)
                def EC = EntranceCategory.findById(params.entrancecategory)
                def ES = EmploymentStatus.findById(params.employmentstatus)
                def ED = ERPDomacile.findById(params.erpdomacile)
                def EN = ERPNationality.findById(params.erpnationality)
                def dob = new Date(Integer.parseInt(params.dateofbirth_year),Integer.parseInt(params.dateofbirth_month),Integer.parseInt(params.dateofbirth_day))
                EA.dateofbirth = params.date('dateofbirth','dd-MM-yyyy')
                //println("Date >>"+ params.date('dateofbirth','dd-MM-yyyy'))
                if(params.physical_handi){
                    EA.is_physically_handicapped = true
                }else {
                    EA.is_physically_handicapped = false
                }
                if(params.visually_handi){
                    EA.is_visually_handicapped = true
                }else {
                    EA.is_visually_handicapped = false
                }
                EA.gender = gender
                EA.maritalstatus = MS
                EA.entrancecategory = EC
                EA.employmentstatus = ES
                EA.erpdomacile = ED
                EA.erpnationality = EN

                EA.updation_username = EA?.email
                EA.updation_date = new java.util.Date()
                EA.updation_ip_address = request.getRemoteAddr()
                EA.save(flush:true,failOnError:true)

                EntranceStudentProfileTabMaster applicant_profile_personal_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_personal_tab")
                EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_personal_master, EA)
                if(!applicant_profile_personal_status) {
                    applicant_profile_personal_status = new EntranceStudentProfileStatus()
                    applicant_profile_personal_status.entranceapplicant = EA
                    applicant_profile_personal_status.entrancestudentprofiletabmaster = applicant_profile_personal_master
                    applicant_profile_personal_status.creation_username = EA.email
                    applicant_profile_personal_status.updation_username = EA.email
                    applicant_profile_personal_status.creation_date = new Date()
                    applicant_profile_personal_status.updation_date = new Date()
                    applicant_profile_personal_status.creation_ip_address = request.getRemoteAddr()
                    applicant_profile_personal_status.updation_ip_address = request.getRemoteAddr()
                    applicant_profile_personal_status.save(flush:true,failOnError:true)
                }
                flash.success = "Personal Details Updated Successfully..!"
                redirect(action: 'eadashboard')
            }
            else {
                flash.error = "session closed, please Login Again..!"
                redirect(action: 'login', controller: "entranceAdmissionLogin")
            }
        }
    }

    def fillContactInformation(){
        println"In fillContactInformation in EntranceApplicant Controller"
        def EA = EntranceApplicant.findByEmail(session.email)
        if(EA == null){
            redirect(action: "login",controller: "EntranceAdmissionLogin")
            return
        }else{
            [EA:EA]
        }
    }
    def saveEAContactDetails(){
        println"In saveEAContactDetails"+params
        if(params.EAID == 'null' || params.local_address == 'null' || params.permanent_address == 'null'){
            flash.error = "Please fill all details"
            redirect(action: 'eadashboard')
        }else{
            if(params.aadhar_number.size() != 12){
                flash.error = "Aadhaar-card number should be 12 Digits..!"
                redirect(action: 'eadashboard')
                return
            }
            EntranceApplicant EA = EntranceApplicant.findById(params.EAID)
            if(EA != null){

                EA.aadhar_number = params.aadhar_number
                EA.local_address = params.local_address
                EA.permanent_address = params.permanent_address

                EA.updation_username = EA?.email
                EA.updation_date = new java.util.Date()
                EA.updation_ip_address = request.getRemoteAddr()
                EA.save(flush:true,failOnError:true)

                EntranceStudentProfileTabMaster applicant_profile_contact_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_contact_tab")
                EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_contact_master, EA)
                if(!applicant_profile_personal_status) {
                    applicant_profile_personal_status = new EntranceStudentProfileStatus()
                    applicant_profile_personal_status.entranceapplicant = EA
                    applicant_profile_personal_status.entrancestudentprofiletabmaster = applicant_profile_contact_master
                    applicant_profile_personal_status.creation_username = EA.email
                    applicant_profile_personal_status.updation_username = EA.email
                    applicant_profile_personal_status.creation_date = new Date()
                    applicant_profile_personal_status.updation_date = new Date()
                    applicant_profile_personal_status.creation_ip_address = request.getRemoteAddr()
                    applicant_profile_personal_status.updation_ip_address = request.getRemoteAddr()
                    applicant_profile_personal_status.save(flush:true,failOnError:true)
                }

                flash.success = "Contact details updated successfully"
                redirect(action: 'eadashboard')
            }else{
                flash.error = "session closed, please Login Again...."
                redirect(action: 'login', controller: "entranceAdmissionLogin")
            }
        }
    }

    def fillQualificationDetails(){
        println"In fill Qualification Details in EntranceApplicant Controller"
        def EA = EntranceApplicant.findById(session.loginid)
        if(EA == null){
            redirect(action: "login",controller: "EntranceAdmissionLogin")
            return
        }else{
            def entranceDegree = EntranceDegree.findAllByIsactive(true)
            def entranceClass = RecClass.createCriteria().list() {
//                projections {
//                    distinct('name')
//                }
                'in'('isactive', true)
            }
//            Organization org = Organization.findById(EA.entrancecategory.organization?.id)
//            def EntranceApplicantAcademics = EntranceApplicantAcademics.findAllByEntranceapplicantAndOrganization(EAA,org)
            def EntranceApplicantAcademics = EntranceApplicantAcademics.findAllByEntranceapplicant(EA)

            [EA:EA,entranceDegree:entranceDegree,entranceClass:entranceClass,EntranceApplicantAcademics:EntranceApplicantAcademics]
        }
    }
    def saveQualificationDetails(){
        println"In save Qualification Details :: " + params
        if(params.entranceClass == 'null' && params.entranceDegree == 'null'){
            flash.error = "Please fill all details"
            EntranceApplicant EA = EntranceApplicant.findById(params.EAID)
            redirect(action: 'eadashboard')
            return
        } else {
            EntranceApplicant EA = EntranceApplicant.findById(params.EAID)
            if(EA != null){
                EntranceDegree ED = EntranceDegree.findById(params.entranceDegree)
                RecClass RC = RecClass.findById(params.entranceClass)
                EntranceApplicantAcademics EAA = EntranceApplicantAcademics.findByEntrancedegreeAndEntranceapplicant(ED,EA)
                if(EAA){
                    flash.error = "The Qualification details Already Present..! Please edit..!"
                    redirect(action: 'eadashboard')
                    return
                }else {
                    EAA = new EntranceApplicantAcademics()
                    EAA.yearofpassing = params.yrofpass
                    EAA.university = params.board
                    EAA.branch = params.branch
                    EAA.specialization = params.specialization
                    EAA.cpi_marks = params.cpi.toDouble()
                    EAA.entranceapplicant = EA
                    EAA.entrancedegree = ED
                    EAA.recclass = RC
                    // EAA.organization = ED.organization

                    EAA.creation_username = EA?.email
                    EAA.updation_username = EA?.email
                    EAA.creation_date = new java.util.Date()
                    EAA.updation_date = new java.util.Date()
                    EAA.creation_ip_address = request.getRemoteAddr()
                    EAA.updation_ip_address = request.getRemoteAddr()
                    EAA.save(flush:true,failOnError:true)

                    def entrancedegree_list = EntranceDegree.findAllByIscompulsoryAndIsactive(true, true)
                    def flag = true
                    if(entrancedegree_list.size() > 0) {
                        for(deg in entrancedegree_list) {
                            EntranceApplicantAcademics academics = EntranceApplicantAcademics.findByEntranceapplicantAndEntrancedegree(EA, deg)
                            if(!academics) {
                                flag = false
                                break
                            }
                        }
                    }

                    if(flag) {
                        EntranceStudentProfileTabMaster applicant_profile_education_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_education_tab")
                        EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_education_master, EA)
                        if (!applicant_profile_personal_status) {
                            applicant_profile_personal_status = new EntranceStudentProfileStatus()
                            applicant_profile_personal_status.entranceapplicant = EA
                            applicant_profile_personal_status.entrancestudentprofiletabmaster = applicant_profile_education_master
                            applicant_profile_personal_status.creation_username = EA.email
                            applicant_profile_personal_status.updation_username = EA.email
                            applicant_profile_personal_status.creation_date = new Date()
                            applicant_profile_personal_status.updation_date = new Date()
                            applicant_profile_personal_status.creation_ip_address = request.getRemoteAddr()
                            applicant_profile_personal_status.updation_ip_address = request.getRemoteAddr()
                            applicant_profile_personal_status.save(flush: true, failOnError: true)
                        }
                    }
                    flash.success = "Qualification Details Added Successfully.."
                    redirect(action: 'eadashboard')
                    return
                }
            }else{
                flash.error = "session closed, please Login Again...."
                redirect(action: 'login', controller: "entranceAdmissionLogin")
            }
        }
    }
    def editQualificationDetails(){
        def EA = EntranceApplicant.findById(session.loginid)
        if(EA){
            println("In Edit Qualification Details"+params)
            if(params.entranceClass == 'null' && params.entranceDegree == 'null'){
                flash.error = "Please fill all details"
                redirect(action: 'eadashboard')
                return
            }
            else {
                EntranceDegree ED = EntranceDegree.findById(params.entranceDegree)
                RecClass RC = RecClass.findById(params.entranceClass)
                Organization org = Organization.findById(ED.organization?.id)
                EntranceApplicantAcademics EAA = EntranceApplicantAcademics.findById(params.entranceApplicantAcademics)
                if (EAA) {
                    EAA.yearofpassing = params.yrofpass
                    EAA.university = params.board
                    EAA.branch = params.branch
                    EAA.specialization = params.specialization
                    EAA.cpi_marks = params.cpi.toDouble()
                    EAA.entranceapplicant = EA
                    EAA.entrancedegree = ED
                    EAA.recclass = RC
                    EAA.organization = ED.organization

                    EAA.updation_username = EA?.email
                    EAA.updation_date = new java.util.Date()
                    EAA.updation_ip_address = request.getRemoteAddr()
                    EAA.save(flush: true, failOnError: true)

                    def entrancedegree_list = EntranceDegree.findAllByIscompulsoryAndIsactive(true, true)
                    def qualification_list = EntranceApplicantAcademics.findAllByEntranceapplicant(EA)
                    def flag = true
                    if(entrancedegree_list.size() > 0) {
                        for(deg in entrancedegree_list) {
                            EntranceApplicantAcademics academics = EntranceApplicantAcademics.findByEntranceapplicantAndEntrancedegree(EA, deg)
                            if(!academics) {
                                flag = false
                                break
                            }
                        }
                    }

                    if(flag) {
                        EntranceStudentProfileTabMaster applicant_profile_education_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_education_tab")
                        EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_education_master, EA)
                        if (!applicant_profile_personal_status) {
                            applicant_profile_personal_status = new EntranceStudentProfileStatus()
                            applicant_profile_personal_status.entranceapplicant = EA
                            applicant_profile_personal_status.entrancestudentprofiletabmaster = applicant_profile_education_master
                            applicant_profile_personal_status.creation_username = EA.email
                            applicant_profile_personal_status.updation_username = EA.email
                            applicant_profile_personal_status.creation_date = new Date()
                            applicant_profile_personal_status.updation_date = new Date()
                            applicant_profile_personal_status.creation_ip_address = request.getRemoteAddr()
                            applicant_profile_personal_status.updation_ip_address = request.getRemoteAddr()
                            applicant_profile_personal_status.save(flush: true, failOnError: true)
                        }
                    }
                    flash.success = "Qualification details updated Successfully"
                    redirect(action: 'eadashboard')
                }
            }
        }
        else {
            flash.error = "session closed, please Login Again...."
            redirect(action: 'login', controller: "entranceAdmissionLogin")
            return
        }
    }
    def deleteQualificationDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "EntranceAdmissionLogin", action: "login")
            return
        } else {
            println("delete Qualification Details :: " + params)
            EntranceApplicantAcademics EAA = EntranceApplicantAcademics.findById(params.EAA)
            if (EAA) {
                try {
                    EAA.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Qualification Details can not delete..!"
                    redirect(action: 'eadashboard')
                    return
                }
            } else {
                flash.error = "Qualification Details Not found.."
                redirect(action: 'eadashboard')
                return
            }
            flash.message = "Qualification details deleted successfully..!"
            redirect(action: 'eadashboard')
            return
        }

    }

    def fillExperienceDetails(){
        println"In fill Qualification Details in EntranceApplicant Controller"
        def EA = EntranceApplicant.findById(session.loginid)
        if(EA == null){
            redirect(action: "login",controller: "EntranceAdmissionLogin")
            return
        }else{
            def RecExperienceType = RecExperienceType.findAllByIsactive(true)
            def EntranceApplicantExperience = EntranceApplicantExperience.findAllByEntranceapplicant(EA)
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd")
            [EA:EA,RecExperienceType:RecExperienceType,EntranceApplicantExperience:EntranceApplicantExperience,sdf:sdf]
        }
    }
    def saveExperienceDetails(){
        println"In save Experience Details"+params
        if(params.RecExperienceType == 'null' && params.organization == 'null' && params.designation == 'null'){
            flash.error = "Please fill all details"
            EntranceApplicant EA = EntranceApplicant.findById(session.loginid)
            redirect(action: 'eadashboard')
            return
        }else{
            EntranceApplicant EA = EntranceApplicant.findById(params.EAID)
            if(EA != null){
                RecExperienceType RET = RecExperienceType.findById(params.RecExperienceType)
                Organization org = Organization.findById(RET.organization?.id)
                EntranceApplicantExperience EAE = new EntranceApplicantExperience()
                EAE.organization_name = params.organization
                EAE.from_date = params.fromdate
                if(params.todate) {
                    EAE.to_date = params.todate
                } else {
                    EAE.to_date = null
                }
                EAE.designation = params.designation
                if(params.cuurent_working){
                    EAE.is_current_job = true
                } else {
                    EAE.is_current_job = false
                }
                EAE.recexperiencetype = RET
                EAE.entranceapplicant = EA
                EAE.organization = org

                EAE.creation_username = EA?.email
                EAE.updation_username = EA?.email
                EAE.creation_date = new java.util.Date()
                EAE.updation_date = new java.util.Date()
                EAE.creation_ip_address = request.getRemoteAddr()
                EAE.updation_ip_address = request.getRemoteAddr()
                EAE.save(flush:true,failOnError:true)
                flash.success = "Experience details Saved Successfully"
                redirect(action: 'eadashboard')
                return
            }else{
                flash.error = "session closed, please Login Again...."
                redirect(action: 'login', controller: "entranceAdmissionLogin")
                return
            }
        }
    }
    def deleteExperienceDetails(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "EntranceAdmissionLogin", action: "login")
            return
        } else {
            println("delete Experience Details :: " + params)
            EntranceApplicantExperience EAE = EntranceApplicantExperience.findById(params.EAA)
            if (EAE) {
                try {
                    EAE.delete(flush: true, failOnError: true)
                } catch (Exception e) {
                    flash.error = "Experience Details can not delete..!"
                    redirect(action: 'eadashboard')
                    return
                }
            } else {
                flash.error = "Experience Details Not found"
                redirect(action: 'eadashboard')
                return
            }
            flash.message = "Experience details deleted successfully..!"
            redirect(action: 'eadashboard')
            return
        }

    }

    def fillDocumentDetails(){
        println("In Upload Document in EntranceApplicant Controller" + params)
        def EA = EntranceApplicant.findById(session.loginid)
        if(EA == null){
            redirect(action: "login",controller: "EntranceAdmissionLogin")
        }else{
            def compulsarydocumentlist = EntranceDocumentType.findAllByIsactiveAndIscompulsory(true, true)
            def entranceDocumentType = EntranceDocumentType.findAllByIsactive(true)
            //def documentlist = EntranceApplicantDocument.findAllByEntranceapplicant(EA)
            def uplodeddocumnettypelist = EntranceApplicantDocument.findAllByEntranceapplicant(EA).entrancedocumenttype
            def new_documnet_type_list = []
            for(type in entranceDocumentType) {
                //println("type =="+type)
                HashMap hm = new HashMap()
                def document = EntranceApplicantDocument.findByEntranceapplicantAndEntrancedocumenttype(EA,type)
                hm.put("type",type)
                hm.put("document",document)
                new_documnet_type_list.add(hm)
            }
            //println("type >>" + new_documnet_type_list)
            [EA:EA,EntranceDocumentType:new_documnet_type_list, compulsarydocumentlist:compulsarydocumentlist]
        }
    }
    def saveDocumentDetails(){
        println("In save Document Details"+ params)
        EntranceApplicant EA = EntranceApplicant.findByEmail(session.email)
        if(params.EntranceDocumentType == 'null'){
            flash.error = "Please fill all details"
            redirect(action: 'eadashboard')
            return
        } else {
            if(EA){
                Organization org = Organization.findById(EA.entrancecategory.organization?.id)
                EntranceDocumentType EDT = EntranceDocumentType.findById(params.EntranceDocumentType)
                EntranceApplicantDocument EAD = EntranceApplicantDocument.findByEntrancedocumenttypeAndEntranceapplicant(EDT, EA)
                if(!EAD) {
                    EAD = new EntranceApplicantDocument()
                }
                EAD.entrancedocumenttype = EDT
                EAD.entranceapplicant = EA
                EAD.organization = org
                AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()

                Part filePart = request.getPart("newFile")
                def fileobj = request.getFile("newFile")
                if (!fileobj.empty) {
                    String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                    int indexOfDot = filePartName.lastIndexOf('.')
                    def fileExtention
                    if (indexOfDot > 0) {
                        fileExtention = filePartName.substring(indexOfDot + 1)
                    }

                    String name1 = EA?.fullname.replaceAll("\\s", "")
                    String name = name1.toLowerCase()
                    String orgName1 = org?.organization_name.replaceAll("\\s", "")
                    String orgName = orgName1.toLowerCase()
                    String DocumentType1 = EDT?.name.replaceAll("\\s", "")
                    String DocumentType = DocumentType1.toLowerCase()
                    String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                    def folderPath = awsFolderPath.path + awsBucket?.content + "/" + orgName + "/" + "EntranceApplicant" + "/" + name
                    def fileName = "Doc_" + name + "_" + date + "_" + DocumentType + "." + fileExtention
                    String existsFilePath = ""

                    def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                    if (isUpload) {
                        EAD.filepath = folderPath
                        EAD.filename = fileName
                        EAD.creation_username = EA?.email
                        EAD.updation_username = EA?.email
                        EAD.creation_date = new java.util.Date()
                        EAD.updation_date = new java.util.Date()
                        EAD.creation_ip_address = request.getRemoteAddr()
                        EAD.updation_ip_address = request.getRemoteAddr()
                        EAD.save(flush: true, failOnError: true)


                        def document_list = EntranceDocumentType.findAllByIscompulsoryAndIsactive(true, true)
                        def flag = true
                        if(document_list.size() > 0) {
                            for(deg in document_list) {
                                EntranceApplicantDocument document = EntranceApplicantDocument.findByEntranceapplicantAndEntrancedocumenttype(EA, deg)
                                if(!document) {
                                    flag = false
                                    break
                                }
                            }
                        }

                        if(flag) {
                            EntranceStudentProfileTabMaster applicant_profile_documents_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_document_tab")
                            EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_documents_master, EA)
                            if (!applicant_profile_personal_status) {
                                applicant_profile_personal_status = new EntranceStudentProfileStatus()
                                applicant_profile_personal_status.entranceapplicant = EA
                                applicant_profile_personal_status.entrancestudentprofiletabmaster = applicant_profile_documents_master
                                applicant_profile_personal_status.creation_username = EA.email
                                applicant_profile_personal_status.updation_username = EA.email
                                applicant_profile_personal_status.creation_date = new Date()
                                applicant_profile_personal_status.updation_date = new Date()
                                applicant_profile_personal_status.creation_ip_address = request.getRemoteAddr()
                                applicant_profile_personal_status.updation_ip_address = request.getRemoteAddr()
                                applicant_profile_personal_status.save(flush: true, failOnError: true)
                            }
                        }

                        println "File uploaded"
                        flash.success = "File Uploaded Successfully..!"
                        redirect(action: 'eadashboard')
                        return
                    } else {
                        flash.error = "File Uploading failed.. Please try again!"
                        redirect(action: 'eadashboard')
                        return
                    }
                }
                if (fileobj.empty) {
                    flash.error = "File is empty..!"
                    redirect(action: 'eadashboard')
                    return
                }
            }
        }
    }

    def getdocumenturl() {
        println("getdocumenturl : " + params)

        String path = params.filepath + params.filename
        println("path :: " + path)
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSBucketService awsBucketService1 = new AWSBucketService()
        String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

        render "<a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue' aria-hidden='true'></i></a>"
    }

    def fillPhotoDetails(){
        println"In fill Qualification Details in EntranceApplicant Controller"
        def EA = EntranceApplicant.findByEmail(session.email)
        if(EA == null){
            redirect(action: "login",controller: "EntranceAdmissionLogin")
        }else{
            String url = ""
            if(EA.photopath && EA.photoname) {
                String path = EA.photopath + EA.photoname
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSBucketService awsBucketService1 = new AWSBucketService()
                url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            }
            [EA:EA, url:url]
        }
    }
    def savePhotoDetails(){
        println("In save Photo Details"+ params)
        EntranceApplicant EA = EntranceApplicant.findByEmail(session.email)
        if(EA){
            Organization org = Organization.findById(EA.entrancecategory.organization?.id)
            AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()

            Part filePart = request.getPart("newFile")
            def fileobj = request.getFile("newFile")
            if(!fileobj.empty){
                String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                int indexOfDot = filePartName.lastIndexOf('.')
                def fileExtention
                if (indexOfDot > 0) {
                    fileExtention = filePartName.substring(indexOfDot + 1)
                    println("fileExtention :: " + fileExtention)
                }

                String name1 = EA?.fullname.replaceAll("\\s","")
                String name = name1.toLowerCase()
                String orgName1 = org?.organization_name.replaceAll("\\s","")
                String orgName = orgName1.toLowerCase()
                String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                def folderPath = awsFolderPath.path + awsBucket?.content +"/"+ orgName +"/"+"EntranceApplicant"+"/"+name
                def fileName = "Photo_" + name +"_"+ date +"."+ fileExtention
                String existsFilePath = ""
                //println("ExistsFilePath :"+existsFilePath)

                def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                if(isUpload){
                    EA.photopath = folderPath
                    EA.photoname = fileName
                    EA.updation_username = EA?.email
                    EA.updation_date = new java.util.Date()
                    EA.updation_ip_address = request.getRemoteAddr()
                    EA.save(flush:true,failOnError:true)

                    EntranceStudentProfileTabMaster applicant_profile_photo_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_photo_tab")
                    EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_photo_master, EA)
                    if (!applicant_profile_personal_status) {
                        applicant_profile_personal_status = new EntranceStudentProfileStatus()
                        applicant_profile_personal_status.entranceapplicant = EA
                        applicant_profile_personal_status.entrancestudentprofiletabmaster = applicant_profile_photo_master
                        applicant_profile_personal_status.creation_username = EA.email
                        applicant_profile_personal_status.updation_username = EA.email
                        applicant_profile_personal_status.creation_date = new Date()
                        applicant_profile_personal_status.updation_date = new Date()
                        applicant_profile_personal_status.creation_ip_address = request.getRemoteAddr()
                        applicant_profile_personal_status.updation_ip_address = request.getRemoteAddr()
                        applicant_profile_personal_status.save(flush: true, failOnError: true)
                    }

                    println "File uploaded"
                    flash.success = "Photo Uploaded Successfully..!"
                    redirect(action: 'eadashboard')
                }else {
                    flash.error = "Something went wrong while uploading file..!"
                    println "Something went wrong while uploading file..!"
                    redirect(action: 'eadashboard')
                }
            }
            if (fileobj.empty) {
                flash.error = "File is empty..!"
                redirect(action: 'eadashboard')
            }
        }
    }

    def fillSignDetails(){
        println"In fill Qualification Details in EntranceApplicant Controller"
        def EA = EntranceApplicant.findByEmail(session.email)
        if(EA == null){

            redirect(action: "login",controller: "EntranceAdmissionLogin")
        }else{
            String url = ""
            if(EA.signpath && EA.signname) {
                String path = EA.signpath + EA.signname
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSBucketService awsBucketService1 = new AWSBucketService()
                url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            }
            [EA:EA, url:url]
        }
    }
    def saveSignDetails(){
        println("In save Sign Details"+ params)
        EntranceApplicant EA = EntranceApplicant.findByEmail(session.email)
        if(EA){
            Organization org = Organization.findById(EA.entrancecategory.organization?.id)
            AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()

            Part filePart = request.getPart("newFile")
            def fileobj = request.getFile("newFile")
            if(!fileobj.empty){
                String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                int indexOfDot = filePartName.lastIndexOf('.')
                def fileExtention
                if (indexOfDot > 0) {
                    fileExtention = filePartName.substring(indexOfDot + 1)
                    println("fileExtention :: " + fileExtention)
                }
                String name1 = EA?.fullname.replaceAll("\\s","")
                String name = name1.toLowerCase()
                String orgName1 = org?.organization_name.replaceAll("\\s","")
                String orgName = orgName1.toLowerCase()
                String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                def folderPath = awsFolderPath.path + awsBucket?.content +"/"+ orgName +"/"+"EntranceApplicant"+"/"+name
                def fileName = "Sign_" + name +"_"+ date +"."+ fileExtention
                String existsFilePath = ""
                //println("ExistsFilePath :"+existsFilePath)

                def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                if(isUpload){
                    EA.signpath = folderPath
                    EA.signname = fileName
                    EA.updation_username = EA?.email
                    EA.updation_date = new java.util.Date()
                    EA.updation_ip_address = request.getRemoteAddr()
                    EA.save(flush:true,failOnError:true)

                    EntranceStudentProfileTabMaster applicant_profile_sign_master = EntranceStudentProfileTabMaster.findByName("applicant_profile_sign_tab")
                    EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(applicant_profile_sign_master, EA)
                    if (!applicant_profile_personal_status) {
                        applicant_profile_personal_status = new EntranceStudentProfileStatus()
                        applicant_profile_personal_status.entranceapplicant = EA
                        applicant_profile_personal_status.entrancestudentprofiletabmaster = applicant_profile_sign_master
                        applicant_profile_personal_status.creation_username = EA.email
                        applicant_profile_personal_status.updation_username = EA.email
                        applicant_profile_personal_status.creation_date = new Date()
                        applicant_profile_personal_status.updation_date = new Date()
                        applicant_profile_personal_status.creation_ip_address = request.getRemoteAddr()
                        applicant_profile_personal_status.updation_ip_address = request.getRemoteAddr()
                        applicant_profile_personal_status.save(flush: true, failOnError: true)
                    }

                    println "File uploaded"
                    flash.success = "Sign Uploaded Successfully..!"
                    redirect(action: 'eadashboard')
                }else {
                    flash.error = "Something went wrong while uploading file..!"
                    println "Something went wrong while uploading file..!"
                    redirect(action: 'eadashboard')
                }
            }
            if (fileobj.empty) {
                flash.error = "File is empty..!"
                redirect(action: 'eadashboard')
            }
        }
    }

}
