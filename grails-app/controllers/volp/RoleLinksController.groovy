package volp

import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*


class RoleLinksController {

    RoleLinksService roleLinksService = new RoleLinksService()
    InformationService InformationService

/*    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond roleLinksService.list(params), model:[roleLinksCount: roleLinksService.count()]
    }

    def show(Long id) {
        respond roleLinksService.get(id)
    }

    def create() {
        respond new RoleLinks(params)
    }

    def save(RoleLink  roleLinks) {
        if (roleLinks == null) {
            notFound()
            return
        }

        try {
            roleLinksService.save(roleLinks)
        } catch (ValidationException e) {
            respond roleLinks.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'roleLinks.label', default: 'RoleLinks'), roleLinks.id])
                redirect roleLinks
            }
            '*' { respond roleLinks, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond roleLinksService.get(id)
    }

    def update(RoleLink  roleLinks) {
        if (roleLinks == null) {
            notFound()
            return
        }

        try {
            roleLinksService.save(roleLinks)
        } catch (ValidationException e) {
            respond roleLinks.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'roleLinks.label', default: 'RoleLinks'), roleLinks.id])
                redirect roleLinks
            }
            '*'{ respond roleLinks, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        roleLinksService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'roleLinks.label', default: 'RoleLinks'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'roleLinks.label', default: 'RoleLinks'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }*/

    def rolelinks() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            println("rolelinks :: " + params)
            ApplicationType appType = ApplicationType.findByApplication_type("ERP")
            Organization org = Organization.findById(session.orgid)
            List<RoleLink> roleLinksList = roleLinksService.getAllRoleLinks(org)
            List<RoleType> roleTypeList = roleLinksService.getAllRoleType(appType, org)
            List<Role> roleList = null
            List<UserType> userTypeList = roleLinksService.getAllUserType(appType, org)
            List<SoftwareModule> softwareModuleList = roleLinksService.getAllSoftwareModule(org)
            List<LinkType> linkTypeList = roleLinksService.getAllLinkType(org)
            RoleLink  roleLink = RoleLink.findById(params.roleLinkId)
            def defaultroletype
            def defaultrole
            def defaultuserType
            def defaultlinkType
            def defaultsoftwaremodule
            def count = 0
            def isupdaterolelink = false
            if (params.roletype != null) {
                defaultroletype = RoleType.findById(params.roletype)
            }
//        else
//        {
//            defaultroletype = roleTypeList.get(0)
//        }
            if (params.role != null) {
                RoleType roleType = RoleType.findById(params.roletype)
                UserType userType = UserType.findById(params.usertype)
                roleList = roleLinksService.getRoleByRoleTypeAndUserType(roleType, userType, org)
                defaultrole = Role.findById(params.role)
                roleLinksList = roleLinksService.getLinkListByRole(defaultrole, org)
                isupdaterolelink = true
            }
//        else
//        {
//            defaultrole = roleList.get(0)
//        }
            if (params.usertype != null) {
                defaultuserType = UserType.findById(params.usertype)
            }
//        else
//        {
//            defaultuserType = userTypeList.get(0)
//        }
            if (params.linktype != null) {
                defaultlinkType = LinkType.findById(params.linktype)
            }
//        else
//        {
//            defaultlinkType = linkTypeList.get(0)
//        }
            if (params.softwaremodule != null) {
                defaultsoftwaremodule = SoftwareModule.findById(params.softwaremodule)
            }
//        else
//        {
//            defaultsoftwaremodule = softwareModuleList.get(0)
//        }

            if (params.copy) {
                roleLink.id = 0
            }

            roleTypeList.sort { it.type }
            if (roleList != null) {
                roleList.sort { it.role }
            }
            userTypeList.sort { it.type }
            softwareModuleList.sort { it.name }
            linkTypeList.sort { it.type }

            [count                : count,
             defaultroletype      : defaultroletype,
             defaultrole          : defaultrole,
             defaultuserType      : defaultuserType,
             defaultlinkType      : defaultlinkType,
             defaultsoftwaremodule: defaultsoftwaremodule,
             roleLink             : roleLink,
             roleLinksList        : roleLinksList,
             roleTypeList         : roleTypeList,
             roleList             : roleList,
             userTypeList         : userTypeList,
             softwareModuleList   : softwareModuleList,
             isupdaterolelink     : isupdaterolelink,
             linkTypeList         : linkTypeList]
        }
    }

    def rolelinkslistAll(){

        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            println("rolelinks :: " + params)
            ApplicationType appType = ApplicationType.findByApplication_type("ERP")
            Organization org = Organization.findById(session.orgid)
            List<RoleLink> roleLinksList = roleLinksService.getAllRoleLinks(org)
            List<RoleType> roleTypeList = roleLinksService.getAllRoleType(appType, org)
            List<Role> roleList = null
            List<UserType> userTypeList = roleLinksService.getAllUserType(appType, org)
            List<SoftwareModule> softwareModuleList = roleLinksService.getAllSoftwareModule(org)
            List<LinkType> linkTypeList = roleLinksService.getAllLinkType(org)
            RoleLink  roleLink = RoleLink.findById(params.roleLinkId)
            def defaultroletype
            def defaultrole
            def defaultuserType
            def defaultlinkType
            def defaultsoftwaremodule
            def count = 0
            if (params.roletype != null) {
                defaultroletype = RoleType.findById(params.roletype)
            }
//        else
//        {
//            defaultroletype = roleTypeList.get(0)
//        }
            if (params.role != null) {
                RoleType roleType = RoleType.findById(params.roletype)
                UserType userType = UserType.findById(params.usertype)
                roleList = roleLinksService.getRoleByRoleTypeAndUserType(roleType, userType)
                defaultrole = Role.findById(params.role)
                roleLinksList = roleLinksService.getLinkListByRole(defaultrole, org)
            }
//        else
//        {
//            defaultrole = roleList.get(0)
//        }
            if (params.usertype != null) {
                defaultuserType = UserType.findById(params.usertype)
            }
//        else
//        {
//            defaultuserType = userTypeList.get(0)
//        }
            if (params.linktype != null) {
                defaultlinkType = LinkType.findById(params.linktype)
            }
//        else
//        {
//            defaultlinkType = linkTypeList.get(0)
//        }
            if (params.softwaremodule != null) {
                defaultsoftwaremodule = SoftwareModule.findById(params.softwaremodule)
            }
//        else
//        {
//            defaultsoftwaremodule = softwareModuleList.get(0)
//        }

            if (params.copy) {
                roleLink.id = 0
            }

            roleTypeList.sort { it.type }
            if (roleList != null) {
                roleList.sort { it.role }
            }
            userTypeList.sort { it.type }
            softwareModuleList.sort { it.name }
            linkTypeList.sort { it.type }

            [count                : count,
             defaultroletype      : defaultroletype,
             defaultrole          : defaultrole,
             defaultuserType      : defaultuserType,
             defaultlinkType      : defaultlinkType,
             defaultsoftwaremodule: defaultsoftwaremodule,
             roleLink             : roleLink,
             roleLinksList        : roleLinksList,
             roleTypeList         : roleTypeList,
             roleList             : roleList,
             userTypeList         : userTypeList,
             softwareModuleList   : softwareModuleList,
             linkTypeList         : linkTypeList]
        }

    }


    def addRoleLink() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            println("addRoleLink :: " + params)
            //println("params.quickLink :: " + params.quickLink)
            boolean quickLink = false
            if(params.quickLink == 'true'){
                quickLink = true
            }
            if(quickLink){
                //println("addRoleLink :: is quick link : true")
            }
            RoleLink  roleLink1 = RoleLink.findById(params.roleLinkId)
            //SoftwareModule softwareModule
            //LinkType linkType
            Organization org
            UserType userType
            RoleType roleType
            Role role

//            ---------------------------------------------- Quick link icon upload ----------------------------------------------
//            AWSBucket awsBucket = AWSBucket.findByContent("documents")
//            AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
//            def FolderPath = awsFolderPath.path + "employeeprofile/icard/photo/"
//            def existPath = ""
//            def fileName =
//            def filePath = FolderPath + fileName

            if (roleLink1 == null || params.roleLinkId == 0) {
                /*if (params.softwareModule != "null") {
                    softwareModule = SoftwareModule.findById(params.softwareModule)
                } else {
                    render "Please select software module."
                    return
                }*/
                if (params.role != "null") {
                    role = Role.findById(params.role)
                } else {
                    render "Please select role."
                    return
                }
                /*if (params.linkType != "null") {
                    linkType = LinkType.findById(params.linkType)
                } else {
                    render "Please select linkType."
                    return
                }*/
                if (params.roleType != "null") {
                    roleType = RoleType.findById(params.roleType)
                } else {
                    render "Please select roleType."
                    return
                }
                if (params.userType != "null") {
                    userType = UserType.findById(params.userType)
                } else {
                    render "Please select userType."
                    return
                }
                org = Organization.findById(session.orgid)
                RoleLink  roleLinks = new RoleLink()
                roleLinks.controller_name = params.controllerName
                roleLinks.action_name = params.actionName
                roleLinks.link_name = params.linkName
                roleLinks.link_displayname = params.link_displayname
                roleLinks.vue_js_name = params.vue_js_name
                //println(params.vue_js_name)
                roleLinks.sort_order = params.sortOrder.toInteger()
                roleLinks.role = role
                //roleLinks.softwaremodule = softwareModule
                //roleLinks.linktype = linkType
//                roleLinks.roletype = roleType
//                roleLinks.usertype = userType
                roleLinks.organization = org
                roleLinks.isrolelinkactive = true
                roleLinks.isquicklink = quickLink
//                roleLinks.linkiconimagefilename = quickLink
//                roleLinks.linkiconimagepath = quickLink
                roleLinksService.addRoleLink(roleLinks)
            } else {
                /*if (params.softwareModule != "null") {
                    softwareModule = SoftwareModule.findById(params.softwareModule)
                } else {
                    render "Please select software module."
                    return
                }*/
                if (params.role != "null") {
                    role = Role.findById(params.role)
                } else {
                    render "Please select role."
                    return
                }
                /*if (params.linkType != "null") {
                    linkType = LinkType.findById(params.linkType)
                } else {
                    render "Please select linkType."
                    return
                }*/
                if (params.roleType != "null") {
                    roleType = RoleType.findById(params.roleType)
                } else {
                    render "Please select roleType."
                    return
                }
                if (params.userType != "null") {
                    userType = UserType.findById(params.userType)
                } else {
                    render "Please select userType."
                    return
                }
                org = Organization.findById(session.orgid)
                roleLink1.controller_name = params.controllerName
                roleLink1.action_name = params.actionName
                roleLink1.link_name = params.linkName
                roleLink1.link_displayname = params.link_displayname
                roleLink1.vue_js_name = params.vue_js_name
                roleLink1.sort_order = params.sortOrder.toInteger()
                roleLink1.role = role
                //roleLink1.softwaremodule = softwareModule
                //roleLink1.linktype = linkType
//                roleLink1.roletype = roleType
//                roleLink1.usertype = userType
                roleLink1.organization = org
                roleLink1.isrolelinkactive = true
                roleLink1.isquicklink = quickLink
//                roleLinks.linkiconimagefilename = quickLink
//                roleLinks.linkiconimagepath = quickLink
                roleLink1.save(failOnError: true, flush: true)
            }
            redirect(action: "rolelinks", params: [roletype: params.roleType, role: params.role, usertype: params.userType])
        }
    }

    def deleteRoleLinks(){
        println("deleteRoleLink  :: " + params)
        RoleLink  roleLinks
        if(params.roleLinkId != "null"){
            roleLinks=RoleLink.findById(params.roleLinkId)
            roleLinksService.deleteRoleLinks(roleLinks)
            //println("delete :: " + roleLinks.controller_name )
        }
        redirect(action: "rolelinks", params: [roletype : roleLinks.role?.roletype.id, role : roleLinks.role.id, usertype : roleLinks.role?.usertype.id])
    }

    def activeRoleLinks() {
        println("activeRoleLink  :: " + params)
        RoleLink  roleLinks
        if(params.roleLinkId != "null"){
            roleLinks=RoleLink.findById(params.roleLinkId)
            roleLinksService.activeRoleLniks(roleLinks)
        }
        redirect(action: "rolelinks", params: [roletype : roleLinks.role?.roletype.id, role : roleLinks.role.id, usertype : roleLinks.role?.usertype.id])
    }

    def getRoleByRoleTypeAndUserType() {
        println("getRoleByRoleTypeAndUserType :: " + params)
        RoleType roleType
        UserType userType
        List<Role> roleList
        Organization org = Organization.findById(session.orgid)
        if(params.roletype != "null" && params.usertype != "null")
        {
            roleType = RoleType.findById(params.roletype)
            userType = UserType.findById(params.usertype)
            roleList = roleLinksService.getRoleByRoleTypeAndUserType(roleType, userType, org)
        }else {
            roleList = null
        }
        if(roleList != null){
            roleList.sort { it.role }
        }
        [roleList:roleList]
    }

    def getRoleLinksByRoleType(){
        println("getRoleLinksByRoleType :: " + params)
        Role role
        List<RoleLink> roleLinksList
        Organization org = Organization.findById(session.orgid)
        if(params.role != "null") {
            role = Role.findById(params.role)
            //println("role :: " + role)
            roleLinksList = roleLinksService.getLinkListByRole(role, org)
            //println("roleLinksList :: "+roleLinksList)
        } else {
            roleLinksList = roleLinksService.getAllRoleLinks()
            //println("roleLinksList :: "+roleLinksList)
        }
        def count = 0
        [count:count,
         roleLinksList:roleLinksList]
    }

    def addRole(){
        println("addRole :: " + params)
        String ipAddr = request.getRemoteAddr()
        String username = session.user
        RoleType roletype
        UserType usertype
        Role role
        if(params.roleType != "null"){
            roletype = RoleType.findById(params.roleType)
        } else {
            render "Please select role type."
            return
        }
        if(params.userType != "null"){
            usertype = UserType.findById(params.userType)
        } else {
            render "Please select user type."
            return
        }
        role = Role.findByRoleAndRoletypeAndUsertype(params.role, roletype, usertype)
        if(role){
            render "Role is already available"
        } else {
            roleLinksService.addRole(params.role, username ,roletype, usertype, ipAddr)
        }
        redirect(action: "rolelinks")
    }

    def addLinkType() {
        println("addLinkType :: " + params)
        Organization org = Organization.findById(session.orgid)
        String ipAddr = request.getRemoteAddr()
        String username = session.user
        roleLinksService.addLinkType(params.type , username, org, ipAddr)
        redirect(action: "rolelinks")
    }

    def addSoftwareModule() {
        println("addSoftwareModule :: " + params)
        Organization org = Organization.findById(session.orgid)
        String ipAddr = request.getRemoteAddr()
        String username = session.user
        roleLinksService.addSoftwareModule(params.name , username, org, ipAddr)
        redirect(action: "rolelinks")
    }

    def sm_admission(){
        println("sm_admission :: " + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization organization = instructor.organization

        def instroleList = login.roles

        for (Role role : instroleList) {
            //println "role : " + role
        }

        SoftwareModule sm = SoftwareModule.findByNameLikeAndOrganization("Admission",organization)
        RoleType rt = RoleType.findByType("Admission")

        //println("organization : " + organization.id)
        //println("sm : " + sm.id)
        //println("rt : " + rt.id)

        def rlList =  RoleLink.findAllBySoftwaremoduleAndRoleInListAndRoletypeAndOrganization(sm,instroleList,rt,organization)
        // def rlList =  RoleLink.findAllByOrganization(organization)

        //println("rlList = " + rlList)
        Set linktypelist = []

        for(RoleLink  r : rlList)
        {
            if(r.linktype != null)
            {
                linktypelist.add(r.linktype)
            }
        }
        def sortedlinktypelist=linktypelist.sort{it.id}
        //println("list = " + list + "\n")
        //println("linktypelist = " + linktypelist + "\n")

        def rolelist = []

        for(LinkType l : sortedlinktypelist)
        {
            def rolelinklist =  RoleLink.findAllBySoftwaremoduleAndRoleInListAndRoletypeAndLinktypeAndOrganization(sm,
                    instroleList,rt,l,organization)

            rolelist.add(rolelinklist)
            //println("rolelinklist = " + rolelinklist + "\n")


        }

        //println("rl = " + rolelist + "\n")

        [rolelist:rolelist,ltlist:sortedlinktypelist]
    }

    def sm_lms(){
        println("v :: " + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization organization = instructor.organization
        def instroleList = login.roles
        for (Role role : instroleList) {
            //println "role : " + role
        }
        SoftwareModule sm = SoftwareModule.findByNameLikeAndOrganization("LMS",organization)
        RoleType rt = RoleType.findByType("Establishment Section")
        //println("organization : " + organization.id)
        //println("sm : " + sm.id)
        //println("rt : " + rt.id)
        def rlList =  RoleLink.findAllBySoftwaremoduleAndRoleInListAndRoletypeAndOrganization(sm,instroleList,rt,organization)
        // def rlList =  RoleLink.findAllByOrganization(organization)
        //println("rlList = " + rlList)
        Set linktypelist = []
        for(RoleLink  r : rlList) {
            if(r.linktype != null) {
                linktypelist.add(r.linktype)
            }
        }
        def sortedlinktypelist=linktypelist.sort{it.id}
        //println("list = " + list + "\n")
        //println("linktypelist = " + linktypelist + "\n")
        def rolelist = []
        for(LinkType l : sortedlinktypelist) {
            def rolelinklist =  RoleLink.findAllBySoftwaremoduleAndRoleInListAndRoletypeAndLinktypeAndOrganization(sm,
                    instroleList,rt,l,organization)
            rolelist.add(rolelinklist)
            //println("rolelinklist = " + rolelinklist + "\n")
        }
        //println("rl = " + rolelist + "\n")
        [rolelist:rolelist,ltlist:sortedlinktypelist]
    }

    def sm_recruitment(){
        println("sm_recruitment :: "+params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization organization = instructor.organization
        def instroleList = login.roles
        for (Role role : instroleList) {
            //println "role : " + role
        }
        SoftwareModule sm = SoftwareModule.findByNameLikeAndOrganization("Recruitment",organization)
        RoleType rt = RoleType.findByType("Establishment Section")
        def rlList =  RoleLink.findAllBySoftwaremoduleAndRoleInListAndRoletypeAndOrganization(sm,instroleList,rt,organization)
        // def rlList =  RoleLink.findAllByOrganization(organization)
        // println("rlList = " + rlList)
        Set linktypelist = []
        for(RoleLink  r : rlList) {
            if(r.linktype != null) {
                linktypelist.add(r.linktype)
            }
        }
        def sortedlinktypelist=linktypelist.sort{it.id}
        //println("list = " + list + "\n")
        // println("linktypelist = " + linktypelist + "\n")
        def rolelist = []
        for(LinkType l : sortedlinktypelist) {
            def rolelinklist =  RoleLink.findAllBySoftwaremoduleAndRoleInListAndRoletypeAndLinktypeAndOrganization(sm,
                    instroleList,rt,l,organization)
            rolelist.add(rolelinklist)
            //println("rolelinklist = " + rolelinklist + "\n")
        }
        // println("rl = " + rolelist + "\n")
        [rolelist:rolelist,ltlist:sortedlinktypelist]
    }
   // By NPP 09-07-2019
    //4jul:

    def showEmpAdmission() {
        println("showEmpAdmission :: "+params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization organization = instructor.organization

        def emplist=Instructor.findAllByOrganizationAndIscurrentlyworking(organization,true)
        //println("emplist : "+ emplist )
        // def roleTypelist=RoleType.findAllByOrganization(instructor?.organization)
        def rolelist=Role.findAllByOrganization(instructor?.organization)
        //println("rolelist : "+ rolelist )

        Instructor defaultInstructor = null
        def defInstructorRoleList = []

        if(params.isedit == "1")
        {
            defaultInstructor = Instructor.findById(params.defaultInstructor)
        }
        else
        {
            if(emplist!= null)
            {
                defaultInstructor = emplist.get(0)
                //println("defaultInstructor : "+ defaultInstructor )
            }
        }
        if(defaultInstructor != null)
        {
            Login l = Login.findByUsername(defaultInstructor.uid)
            defInstructorRoleList = l.roles
        }


        //println("defInstructorRoleList : " + defInstructorRoleList)

        ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
        //println("applicationType:" + applicationType)
        RoleType admission=RoleType.findByTypeAndApplicationtype("Admission",applicationType)
        //println("admission :"+admission)
        RoleType reg=RoleType.findByTypeAndApplicationtype("Registration",applicationType)
        //println("reg :"+reg.id)

        def rolelst = []
        for(Role r : defInstructorRoleList)
        {
            //println("... : " + r.roletype)
            if(r.roletype.id == admission.id )
            {
                rolelst.add(r)
            }
            if(r.roletype.id == reg.id )
            {
                rolelst.add(r)
            }
        }
        //println("defInstructorRoleList : " + rolelst)
        //println("defaultInstructor : " + defaultInstructor.id)

        [emplist:emplist,rolelist:rolelist,defaultInstructor:defaultInstructor,defInstructorRoleList:rolelst]
    }

    def proceedclickAdmission()
    {
        println("proceedclickAdmission params : "+params)
        Instructor instructor=Instructor.findById(params.emp)
        //println("instructor  : "+instructor.employee_code)
        Login login=Login.findByUsername(instructor.uid)
        Organization org=instructor.organization
        //println("login : "+login)
        //println("org : "+org)

        def roles=login.roles
        ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
        //println("applicationType:" + applicationType)
        RoleType admission=RoleType.findByTypeAndApplicationtype("Admission",applicationType)
        //println("admission :"+admission)
        RoleType reg=RoleType.findByTypeAndApplicationtype("Registration",applicationType)
        //println("reg :"+reg)


        def rolelist = []
        for(Role r : roles)
        {
            if(r.roletype == admission )
            {
                rolelist.add(r)
            }
            if(r.roletype == reg )
            {
                rolelist.add(r)
            }
        }

        //rolelist=login.roles
        //println(" rolelist : "+rolelist)
        //println("params.instructorid : "+instructor.id)
        [rolelist:rolelist, instructorid :instructor.id]
    }


    def addNewRoleAdmission()
    {
        println(" addNewRoleAdmission params : "+params)
        //println("params.instructorid : "+params.instructorid)
        Instructor instructor=Instructor.findById(params.instructorid)
        //println("instructor  : "+instructor.employee_code)
        Login login=Login.findByUsername(instructor.uid)
        Organization org=instructor.organization
        def roles=login.roles
        ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
        RoleType admission=RoleType.findByTypeAndApplicationtype("Admission",applicationType)
        RoleType reg=RoleType.findByTypeAndApplicationtype("Registration",applicationType)

        def rolelist = []
        for(Role r : roles)
        {
            if(r.roletype == admission )
            {
                rolelist.add(r)
            }
            if(r.roletype == reg )
            {
                rolelist.add(r)
            }
        }
        //println(" rolelist : "+rolelist)

        def rtypelist=[]
        rtypelist.add(admission)
        rtypelist.add(reg)
        //rolelist=login.roles
        [rtypelist:rtypelist,instructorid : params.instructorid,rolelist:rolelist]
    }



    def deleteRoleAdmission()
    {
        println("deleteRole params : "+params)
        Role role=Role.findById(params.id)
        Instructor instructor=Instructor.findById(params.instid)
        Login login=Login.findByUsername(instructor.uid)
        def roles =  login.roles

        roles.remove(role)
        login.roles = roles
        login.save(flush: true, failOnError: true)
        redirect(action:"showEmpAdmission" ,  params: [ isedit: 1 , defaultInstructor: instructor.id ])
    }
    //END By NPP 09-07-2019

    def showfetchAdmission()
    {
        println("  showfetchAdmission params : "+params)
        Instructor instructor=Instructor.findById(params.instructorid)
        Login login=Login.findByUsername(instructor.uid)
        Organization org=instructor.organization

        RoleType selectrtype=RoleType.findById(params.roletype)
        UserType usertype=UserType.findByTypeAndOrganization("Employee", org)

        def rolelist = Role.findAllByRoletypeAndUsertypeAndOrganization(selectrtype,usertype, org)

        def roles=login.roles
        def rolespresent = []
        for(Role r : roles)
        {
            if(r.roletype.id == selectrtype.id )
            {
                rolespresent.add(r)
            }
        }

        //checkboxcode

        def r

        if(params.rolecheckbox.getClass().isArray())
        {
            for (i in params.rolecheckbox)
            {
                //r = Role.findById(i)
                // println("role" + r)
                //login.save(failOnError: true, flush: true)

                def rolespre = []
                for(Role r1 : roles)
                {
                    if(r1.roletype.id == selectrtype.id )
                    {
                        rolespre.add(r1)
                    }
                }
            }
        }
        else {
            r = Role.findById(params.rolecheckbox)
            //println("role" + r)
            // login.save(failOnError: true, flush: true)
        }

//        println(" rolespresent : "+rolespresent)
//        println(" rolelist : "+rolelist)
        rolelist.removeAll(rolespresent)
//        println(" rolelist : "+rolelist)
//        println(" rolelist : "+rolelist)
//        println("params.instructorid : "+params.instructorid)
        [rolelist:rolelist,instructorid : params.instructorid]
    }

    def assignNewRoleAdmission()
    {
        println("assignNewRoleAdmission params : "+params)
        Instructor instructor=Instructor.findById(params.instructorid)
        Login login=Login.findByUsername(instructor.uid)
        def roles =  login.roles

        if (params.rolecheckbox.getClass().isArray()) {
            for(int i=0; i<params.rolecheckbox.size();i++) {
                Role rolecheck= Role.findById(params.rolecheckbox[i])
                //println("rolecheck : "+rolecheck)
                roles.add(rolecheck)
            }
        }
        else
        {
            Role rolecheck= Role.findById(params.rolecheckbox)
            //println("rolecheck : "+rolecheck)
            roles.add(rolecheck)
        }

        login.roles = roles
        //println("roles"+roles)
        login.save(flush: true, failOnError: true)
        RoleType admission=RoleType.findByType("Admission")
        //println("admission :"+admission)
        RoleType reg=RoleType.findByType("Registration")
        //println("reg :"+reg)
        //println(" roles : "+roles)
        def rolelist = []
        for(Role r : roles)
        {
            //println("r :"+r)
            //println("r.roletype:"+r.roletype )
            if(r.roletype.id == admission.id )
            {
                rolelist.add(r)
            }
            if(r.roletype.id == reg.id )
            {
                rolelist.add(r)
            }
        }
        //println(" rolelist : "+rolelist)
        //println(" instructorid : "+params.instructorid)
        redirect(action:"showEmpAdmission" ,  params: [ isedit: 1 , defaultInstructor: params.instructorid ])
        return
    }

    def assignRoletoMultipleIndividuals(){
        if (session.loginid == null) {
            redirect(controller: "login", action: "erplogin")
            return
        }else{
            println("In assignRoletoMultipleIndividuals...")
            flash.message = ""
            flash.error = ""
            Login login = Login.findById(session.loginid)
            Organization organization = Organization.findById(session.orgid)
            ApplicationType applicationType =ApplicationType.findByApplication_type("ERP")
            def programtype =ProgramType.findAllByOrganization(organization)
            //println("programtype "+programtype)

            DepartmentType DepartmentType=DepartmentType.findByName("Academics")
            def departmentList = Department.findAllByOrganizationAndDepartmenttype(organization,DepartmentType)

            def programList = Program.findAllByDepartmentInListAndOrganizationAndIsdeleted(departmentList,organization,false)
            //println"programList "+programList

            def yearList=Year.findAllByOrganization(organization)

            def roletype=RoleType.findAllByApplicationtypeAndOrganization(applicationType, organization)

            def departmentTypeList=DepartmentType.list()
            //println("departmentTypeList "+departmentTypeList)
            def departmentListforInst=Department.findAllByOrganization(organization)
            //println("departmentListforInst "+departmentListforInst)

            def employeeType=EmployeeType.findAllByOrganization(organization)
            //println("employeeType "+employeeType)
            /*def employeeType=EmployeeType.findAllByTypeAndOrganization("Teaching",organization)
            def instructorList =Instructor.findAllByOrganizationAndIscurrentlyworking(organization,true)
            //println("instructorList "+instructorList)*/
            /*if(session.link=='menu1'){
                session.link='home'
            }else{
                session.link='home'
            }*/
            //println session.link

            def academicyear = InformationService.aylist()
            def semester = Semester.findAllByOrganization(organization)
            def aay = InformationService.currentAySem("ERP", "ERP Coordinator", organization)

            if(!aay) {
                render "Application Academic Year Not Set For ERP Coordinator"
                return
            }

            def divisionoffering = DivisionOffering.createCriteria().list(){
                'in'('organization', organization)
                and {
                    'in'('academicyear', aay?.academicyear)
                    'in'('semester', aay?.semester)
                    'in'('program', programList)
                    'in'('year', yearList)
                }
            }

            [programtype            :programtype,
             programList            :programList,
             yearList               :yearList,
             roletype               :roletype,
             departmentTypeList     :departmentTypeList,
             departmentListforInst  :departmentListforInst,
             employeeType           :employeeType,
             academicyear:academicyear,
             semester:semester,
             aay:aay,
             divisionoffering:divisionoffering]
        }
    }

    def getDivisionList(){
        println("In getDivisionList"+params)
        Login login = Login.findById(session.loginid)
        Organization organization = Organization.findById(session.orgid)

        def ay = AcademicYear.findById(params.ay)
        def sem = Semester.findById(params.sem)

        def programTypeList = []
        def programList = []
        def yearList = []
        if(params.programtype=="-1"){
            programTypeList=ProgramType.findAllByOrganization(organization)
        }
        else{
            ProgramType programType=ProgramType.findById(params.programtype)
            programTypeList.add(programType)
        }

        if(params.program=="-1"){
            programList=Program.findAllByOrganizationAndProgramtype(organization, programTypeList)
        }else{
            Program program = Program.findById(params.program)
            programList.add(program)
        }

        if(params.year=="-1"){
            yearList=Year.findAllByOrganization(organization)
        }else{
            Year year=Year.findById(params.year)
            yearList.add(year)
        }

        def divisionoffering = DivisionOffering.createCriteria().list(){
            'in'('organization', organization)
            and {
                'in'('academicyear', ay)
                'in'('semester', sem)
                'in'('program', programList)
                'in'('year', yearList)
            }
        }

        [divisionoffering:divisionoffering]
    }

    def getProgramList(){
        println("In getProgramList"+params)
        Login login = Login.findById(session.loginid)
        Organization organization = Organization.findById(session.orgid)
        ProgramType programType=ProgramType.findByIdAndOrganization(params.programtype,organization)
        //println("programType "+programType)
        def programList=Program.findAllByOrganizationAndProgramtypeAndIsdeleted(organization,programType,false)
        //println("programList "+programList)
        [programList:programList]
    }

    def getCurrentAy(){
        println("In getCurrentAy"+params)
        Login login = Login.findById(session.loginid)
        Organization organization = Organization.findById(session.orgid)

        def roleType = RoleType.findById(params.roletype)
        def aay = InformationService.currentAySem("ERP", roleType?.type, organization)
        def academicyear = InformationService.aylist()
        def semester = Semester.findAllByOrganization(organization)

        [aay:aay, academicyear:academicyear, semester:semester]
    }

    def getRolesList(){
        println("In getRolesList"+params)
        Login login = Login.findById(session.loginid)
        Organization organization = Organization.findById(session.orgid)
        ApplicationType applicationType =ApplicationType.findByApplication_type("ERP")
        UserType userType=UserType.findByTypeAndApplication_typeAndOrganization("Student",applicationType, organization)
        //println("userType "+userType)
        RoleType roleType
        if(params.roletype==null){
            flash.message="Please select role!"
            return
        }
        else{
            roleType=RoleType.findById(params.roletype)
            //println("roleType "+roleType)
        }
        def role =Role.findAllByRoletypeAndUsertypeAndOrganization(roleType,userType, organization)
       //println("role "+role)
        [role:role]
    }

    def showCompleteList(){
        if (session.loginid == null) {
        redirect(controller: "login", action: "erplogin")
        return
    }else{
            println("In showCompleteList..."+params)
            Login login = Login.findById(session.loginid)
            Organization organization = Organization.findById(session.orgid)
            ApplicationType at=ApplicationType.findByApplication_type("ERP")
            RoleType rt=RoleType.findByApplicationtypeAndTypeAndOrganization(at,"ERP Coordinator", organization)
            ApplicationAcademicYear aay=ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt,true,organization)
            if(aay==null){
                render "<p style='color:red;font-size:14px;text-align:center;font-weight:bold;'>Please Set ApplicationAcademicYear of ERP Coordinator Module"
                return
            }

            flash.message = ""
            flash.error = ""

            def ay = AcademicYear.findById(params.ay)
            def sem = Semester.findById(params.sem)

            def programTypeList = []
            def divisionoffering = []
            def programList = []
            def yearList = []
            if(params.programtype=="-1"){
                programTypeList=ProgramType.findAllByOrganization(organization)
                //println("programType "+programTypeList)
            }
            else{
                ProgramType programType=ProgramType.findById(params.programtype)
                programTypeList.add(programType)
                //println("\nprogramType "+programTypeList)
            }

            if(params.programid=="-1"){
                programList=Program.findAllByOrganizationAndProgramtype(organization, programTypeList)
                //println("program "+programList)
            }
            else{
                println("params.programid :: " + params.programid)
                Program program = Program.findById(params.programid)
                programList.add(program)
                //println("\nprogram: "+programList)
            }

            if(params.year=="-1"){
                yearList=Year.findAllByOrganization(organization)
                //println("year "+yearList)
            }
            else{
                Year year=Year.findById(params.year)
                yearList.add(year)
                //println("\nyearList: "+yearList)
            }

            if(params.division=="-1"){
                divisionoffering = DivisionOffering.createCriteria().list(){
                    'in'('organization', organization)
                    and {
                        'in'('academicyear', ay)
                        'in'('semester', sem)
                        'in'('program', programList)
                        'in'('year', yearList)
                    }
                }
            }
            else{
                DivisionOffering dv = DivisionOffering.findById(params.division)
                divisionoffering.add(dv)
            }

            RoleType roleType = null
            if(params.roletype==null){
                flash.message="Please select Role Type!"
                return
            }
            else{
                roleType=RoleType.findById(params.roletype)
                //println("\nroleType: "+roleType.id)
            }

            Role role = null
            if(params.role==null){
                flash.message="Please select role!"
            }
            else{
                role=Role.findById(params.role)
                //println("\nrole: "+role.id)
            }

            def learnerCurrentYear = []
            def learnerTemp = null
            def learner = null
            def learner2=[]
            def learnerDiv=[]
            //ApplicationAcademicYear aay=ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(roleType,true,organization)

            if(divisionoffering) {
                learnerDiv = LearnerDivision.createCriteria().list {
                    'in'('year', yearList)
                    and {
                        'in'('program', programList)
                    } and {
                        'in'('organization', organization)
                    } and {
                        'in'('academicyear', ay)
                    } and {
                        'in'('semester', sem)
                    } and {
                        'in'('divisionoffering', divisionoffering)
                    }
                }
            }

            //learnerDiv=LearnerDivision.findAllByOrganizationAndYearInListAndProgramInListAndAcademicyearAndSemester(organization,yearList,programList,aay.academicyear,aay.semester)
            //learnerCurrentYear=LearnerCurrentYear.findAllByAcademicyearAndOrganizationAndCurrentyearInList(aay.academicyear,organization,yearList).learner
            //println("\nlearnerDiv: "+learnerDiv)
            int count=learnerDiv.size()
            //println"count "+count

            /*        learnerTemp=Learner.findAllByOrganizationAndProgramInList(organization,programList)
            //println("\nlearnerTemp: "+learnerTemp)
            int count2=learnerTemp.size()
            println"count2 "+count2*/
            /*for(Learner learner1:learnerTemp){
                learner=Learner.findAllByCurrent_yearInList(yearList)
                println("1138 learner "+learner)
            }*/

            /*for(y in yearList){
                learner=Learner.findAllByCurrent_year(y)
                println("1143 learner "+learner)
            }*/

            /*learnerDiv.retainAll(learnerTemp)
            println("learnerDiv "+learnerDiv)
            if(learnerDiv==null){
                flash.message="No students available!"
                return
            }*/
            def login1
            def roles
            def learnerList = []
            def finalList=[]
            for(LearnerDivision learner1:learnerDiv){
                if(learner1 != null){
                    login1=Login.findByUsername(learner1.learner.uid)
                    def roles_list = login1?.roles?.id
/*                    roles=login1.roles.id
                    roles = roles.flatten()*/
                    finalList.add(login:login1,learnerdivision:learner1, roles_list:roles_list)
                    //println "finalList "+finalList.login.roles.id
                    //println "roles:"+ roles
                    //println "role:"+ role.id
                    /*if(roles.contains(role.id))
                    {
                        learnerList.add(learner1)
                    }*/
                }
                /*else{
                    flash.message="Students in "+ programList +" doesn't have "+role +" role of "+ roleType +" !"
                    return
                }*/
            }

            //println("learnerList :"+learnerList)
            //println("roles "+roles)
            //println("role "+role.id)
            //println("finalList "+finalList)
            /*println("roleType "+roleType)
            //println("params.role "+params.role.getClass().getName()+" "+params.role)*/

            int roleInt=Integer.parseInt(params.role)
           //println("roleInt "+roleInt.getClass().getName()+" "+roleInt)
            //println"flist.login.roles "+finalList.login.roles.id

            [finalList:finalList,role:role,roleInt:roleInt,
             roleType:roleType,
             roleid:params.role,
             roletypeid:params.roletype,
             yearid:params.year,
             programid:params.programid,
             ay:params.ay,
             sem:params.sem,
             division:params.division,
             programtypeid:params.programtype]
        }
    }

    def saveAssignRoles(){
        println("In saveAssignRoles "+params)

        flash.message = ""
        flash.error = ""

        Login login = Login.findById(session.loginid)

        Organization organization = Organization.findById(session.orgid)

        ApplicationType at=ApplicationType.findByApplication_type("ERP")

        RoleType rt=RoleType.findByApplicationtypeAndType(at,"ERP Coordinator")

        ApplicationAcademicYear aay=ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt,true,organization)
        def ay = AcademicYear.findById(params.ay)
        def sem = Semester.findById(params.sem)

        def programTypeList = []
        def divisionoffering = []
        def programList = []
        def yearList = []

        if(params.programtype=="-1"){
            programTypeList=ProgramType.findAllByOrganization(organization)
            //println("programTypeList "+programTypeList)
        }
        else{
            ProgramType programType=ProgramType.findById(params.programtype)
            programTypeList.add(programType)
            //println("\nprogramTypeList "+programTypeList)
        }

        if(params.programid=="-1"){
            programList=Program.findAllByOrganizationAndProgramtype(organization, programTypeList)
            //println("programList "+programList)
        }
        else{
            Program program=Program.findById(params.programid)
            programList.add(program)
            //println("\nprogramList: "+programList)
        }

        if(params.year=="-1"){
            yearList=Year.findAllByOrganization(organization)
            //println("yearList "+yearList)
        }
        else{
            Year year=Year.findById(params.year)
            yearList.add(year)
            //println("\nyearList: "+yearList)
        }

        if(params.division=="-1"){
            divisionoffering = DivisionOffering.createCriteria().list(){
                'in'('organization', organization)
                and {
                    'in'('academicyear', ay)
                    'in'('semester', sem)
                    'in'('program', programList)
                    'in'('year', yearList)
                }
            }
        }
        else{
            DivisionOffering dv = DivisionOffering.findById(params.division)
            divisionoffering.add(dv)
        }


        RoleType roleType = null
        if(params.roletype==null){
            flash.message="Please select Role type!"
            return
        }
        else{
            roleType=RoleType.findById(params.roletype)
            //println("\nroleType: "+roleType)
        }

        Role role = null
        if(params.role==null){
            flash.message="Please select Role!"
            return
        }
        else{
            role=Role.findById(params.role)
            //println("\nrole: "+role)
        }

        def learnerCurrentYear = []
        def learnerDiv=[]

        if(divisionoffering)
             learnerDiv=LearnerDivision.findAllByOrganizationAndYearInListAndProgramInListAndAcademicyearAndSemesterAndDivisionofferingInList(organization,yearList,programList, ay, sem, divisionoffering)
        //println("\nlearnerDiv: "+learnerDiv)

        for(LearnerDivision learner1:learnerDiv){
            //println"1069:learner1 "+learner1
            Login login2 = Login.findByUsername(learner1.learner.uid)
            if(login2) {
                def roles = login2.roles
                if (!roles.contains(role)) {
                    roles.add(role)
                    login2.roles = roles
                    login2.save(failOnError: true, flush: true)
                }
            }
        }

        flash.message = "Roll assigning completed!"
        [organization : organization]
    }

    def revokeRoles(){
        println("In revokeRoles "+params)

        Login login = Login.findById(session.loginid)

        Organization organization = Organization.findById(session.orgid)

        ApplicationType at=ApplicationType.findByApplication_type("ERP")

        RoleType rt=RoleType.findByApplicationtypeAndTypeAndOrganization(at,"Registration", organization)
        flash.message = ""
        flash.error = ""
        ApplicationAcademicYear aay=ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt,true,organization)

        if(aay==null){
            render "<p style='color:red;font-size:14px;text-align:center;font-weight:bold;'>Please Set ApplicationAcademicYear of Registration Module"
            return
        }

        def ay = AcademicYear.findById(params.ay)
        def sem = Semester.findById(params.sem)

        def programTypeList = []
        def divisionoffering = []
        def programList = []
        def yearList = []

        if(params.programtype=="-1"){
            programTypeList=ProgramType.findAllByOrganization(organization)
            println("programType "+programTypeList)
        }
        else{
            ProgramType programType=ProgramType.findById(params.programtype)
            programTypeList.add(programType)
            println("\nprogramType "+programTypeList)
        }

        if(params.program=="-1"){
            programList=Program.findAllByOrganizationAndProgramtype(organization, programTypeList)
            println("program "+programList)
        }
        else{
            Program program=Program.findById(params.program)
            programList.add(program)
            println("\nprogram: "+programList)
        }

        if(params.year=="-1"){
            yearList=Year.findAllByOrganization(organization)
            println("yearList "+yearList)
        }
        else{
            Year year=Year.findById(params.year)
            yearList.add(year)
            println("\nyearList: "+yearList)
        }

        println("params.division : "+params.division)

        if(params.division=="-1"){
            divisionoffering = DivisionOffering.createCriteria().list(){
                'in'('organization', organization)
                and {
                    'in'('academicyear', ay)
                    'in'('semester', sem)
                    'in'('program', programList)
                    'in'('year', yearList)
                }
            }
        }
        else{
            DivisionOffering dv = DivisionOffering.findById(params.division)
            divisionoffering.add(dv)
        }

        println("divisionoffering : "+divisionoffering)



        RoleType roleType = null
        if(params.roletype==null){
            flash.message="Please select Role Type!"
            return
        }
        else{
            roleType=RoleType.findById(params.roletype)
            println("\nroleType: "+roleType.id)
        }

        Role role = null
        if(params.role==null){
            flash.message="Please select role!"
        }
        else{
            role=Role.findById(params.role)
            println("\nrole: "+role.id)
        }

        def learner = []
        def learnerCurrentYear = []
        def learner2=[]
        def learnerDiv=[]

        if(divisionoffering)
           learnerDiv = LearnerDivision.findAllByOrganizationAndYearInListAndProgramInListAndAcademicyearAndSemesterAndDivisionofferingInList(organization, yearList, programList, ay, sem, divisionoffering)
        //learnerCurrentYear=LearnerCurrentYear.findAllByOrganizationAndCurrentyear(organization,yearList).learner
        println("learnerDiv: "+learnerDiv.learner.registration_number)


        learner = Learner.createCriteria().list {
                'in'('program', programList)
            and {
                'in'('current_year', yearList)
            }
            and{
                'in'('organization', organization)
            }
        }

        //learner=Learner.findAllByOrganizationAndProgramInListAndCurrent_year(organization,programList,yearList)
        println("\nlearner: "+learner)

        for(Learner learner1:learner){
            println("learner1 : " + learner1)
            if(learnerDiv.contains(learner1.id))
            {
                println"In learnerDiv.contains(learner1)"+learner1
                learner2.add(learner1.id)
                //println("learner2 : " + learner2)
            }
        }
        println("learner2 "+learner2)

        for(LearnerDivision learner1:learnerDiv){
            Login login2=Login.findByUsername(learner1.learner.uid)
            //println("login2 "+login2)
            login2.removeFromRoles(role)
            login2.save(flush: true, failOnError: true)
            println("Role Removed!")
        }

        flash.message = "Roll Revoking completed!"
        [organization : organization]
    }

    def getStudentList(){
        println("getStudentList "+params)

        flash.message = ""
        flash.error = ""

        Login login = Login.findById(session.loginid)
        //println("login "+login)

        Organization organization = Organization.findById(session.orgid)
        //println("organization "+organization)

        ApplicationType at=ApplicationType.findByApplication_type("ERP")

        RoleType rt=RoleType.findByApplicationtypeAndTypeAndOrganization(at,"ERP Coordinator", organization)

        ApplicationAcademicYear aay=ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt,true,organization)

        if(aay==null){
            render "<p style='color:red;font-size:14px;text-align:center;font-weight:bold;'>Please Set ApplicationAcademicYear of ERP Coordinator Module"
            return
        }


        def ay = AcademicYear.findById(params.ay)
        def sem = Semester.findById(params.sem)

        def programTypeList = []
        def divisionoffering = []
        def programList = []
        def yearList = []

        if(params.programtype=="-1"){
            programTypeList=ProgramType.findAllByOrganization(organization)
            println("programType "+programTypeList)
        }
        else{
            ProgramType programType=ProgramType.findById(params.programtype)
            programTypeList.add(programType)
            println("\nprogramType "+programTypeList)
        }

        if(params.programid=="-1"){
            programList=Program.findAllByOrganizationAndProgramtype(organization, programTypeList)
            println("program "+programList)
        }
        else{
            Program program=Program.findById(params.programid)
            programList.add(program)
            println("\nprogram: "+programList)
        }

        if(params.year=="-1"){
            yearList=Year.findAllByOrganization(organization)
            println("year "+yearList)
        }
        else{
            Year year=Year.findById(params.year)
            yearList.add(year)
            println("\nyearList: "+yearList)
        }

        if(params.division=="-1"){
            divisionoffering = DivisionOffering.createCriteria().list(){
                'in'('organization', organization)
                and {
                    'in'('academicyear', ay)
                    'in'('semester', sem)
                    'in'('program', programList)
                    'in'('year', yearList)
                }
            }
        }
        else{
            DivisionOffering dv = DivisionOffering.findById(params.division)
            divisionoffering.add(dv)
        }


        RoleType roleType = null
        if(params.roletype==null){
            flash.message="Please select Role Type!"
            return
        }
        else{
            roleType=RoleType.findById(params.roletype)
            println("\nroleType: "+roleType.id)
        }

        Role role = null
        if(params.role==null){
            flash.message="Please select role!"
        }
        else{
            role=Role.findById(params.role)
            println("\nrole: "+role.id)
        }

        def learnerCurrentYear = []
        def learnerTemp = null
        def learner = null
        def learner2=[]
        def learnerDiv=[]
        //ApplicationAcademicYear aay=ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(roleType,true,organization)

        if(divisionoffering) {
            learnerDiv = LearnerDivision.createCriteria().list {
                'in'('year', yearList)
                and {
                    'in'('program', programList)
                } and {
                    'in'('organization', organization)
                } and {
                    'in'('academicyear', ay)
                } and {
                    'in'('semester', sem)
                } and {
                    'in'('divisionoffering', divisionoffering)
                }
            }
        }
        //learnerDiv=LearnerDivision.findAllByOrganizationAndYearInListAndProgramInListAndAcademicyearAndSemester(organization,yearList,programList,aay.academicyear,aay.semester)
        //learnerCurrentYear=LearnerCurrentYear.findAllByAcademicyearAndOrganizationAndCurrentyearInList(aay.academicyear,organization,yearList).learner
        //println("\nlearnerDiv: "+learnerDiv)
        int count=learnerDiv.size()
        println"count "+count

        /*        learnerTemp=Learner.findAllByOrganizationAndProgramInList(organization,programList)
        //println("\nlearnerTemp: "+learnerTemp)
        int count2=learnerTemp.size()
        println"count2 "+count2*/
        /*for(Learner learner1:learnerTemp){
            learner=Learner.findAllByCurrent_yearInList(yearList)
            println("1138 learner "+learner)
        }*/

        /*for(y in yearList){
            learner=Learner.findAllByCurrent_year(y)
            println("1143 learner "+learner)
        }*/

        /*learnerDiv.retainAll(learnerTemp)
        println("learnerDiv "+learnerDiv)
        if(learnerDiv==null){
            flash.message="No students available!"
            return
        }*/
        def login1
        def roles

        def learnerList = []
        for(LearnerDivision learner1:learnerDiv){
            if(learner1 != null){
                login1=Login.findAllByUsername(learner1.learner.uid)
                roles=login1.roles.id
                roles = roles.flatten()
                //println "roles:"+ roles
                //println "role:"+ role.id
                if(roles.contains(role.id))
                {
                    learnerList.add(learner1)
                }
            }
            /*else{
                flash.message="Students in "+ programList +" doesn't have "+role +" role of "+ roleType +" !"
                return
            }*/
        }
        //println("learnerList "+learnerList)
        //println("roles "+roles)
        //println("role "+role)
        //println("roleType "+roleType)
        [learner:learnerList,role:role,roleType:roleType,
         roleid:params.role,
         roletypeid:params.roletype,
         yearid:params.year,
         programid:params.programid,
         ay:params.ay,
         sem:params.sem,
         division:params.division,
         programtypeid:params.programtype]
    }

    def getRolesforFaculty(){
        flash.message = ""
        flash.error = ""
        println("In getRolesforFaculty"+params)

        Login login = Login.findById(session.loginid)

        Organization organization = Organization.findById(session.orgid)

        ApplicationType applicationType =ApplicationType.findByApplication_type("ERP")

        UserType userType=UserType.findByTypeAndApplication_typeAndOrganization("Employee",applicationType, organization)
        println("userType "+userType)

        RoleType roleType
        if(params.roletypefaculty==null){
            flash.message="Please select role for faculty!"
            return
        }
        else{
            roleType=RoleType.findById(params.roletypefaculty)
            println("roleType "+roleType)
        }

        def role =Role.findAllByRoletypeAndUsertypeAndOrganization(roleType,userType, organization)
        println("role "+role)
        [role:role]
    }

    def getDepartmentforFaculty(){
        println("in getDepartmentforFaculty..."+params)

        Login login = Login.findById(session.loginid)

        Organization organization = Organization.findById(session.orgid)

        def departmentType = []
        def departmentList = null
        if(params.departmentType=="-1"){
            departmentType=DepartmentType.list()
            println("departmentType "+departmentType)

            departmentList=Department.findAllByOrganizationAndDepartmenttypeInList(organization,departmentType)
            println("departmentList "+departmentList)

        }else{
            departmentType=DepartmentType.findById(params.departmentType)
            println("departmentType "+departmentType)

            departmentList=Department.findAllByOrganizationAndDepartmenttype(organization,departmentType)
            println("departmentList "+departmentList)
        }

        [departmentList:departmentList]
    }

    def saveRolesofFaculties() {
        println("In saveRolesofFaculties" + params)

        flash.message = ""
        flash.error = ""

        Login login = Login.findById(session.loginid)

        Organization organization = Organization.findById(session.orgid)

        if(params.checkedValue) {
            def checkedValue
            if (params.checkedValue.class.isArray())
                checkedValue = params.checkedValue
            else {
                checkedValue = []
                checkedValue.add(params.checkedValue)
            }

//            def departmentType = []
            def departmentList = []
            def employeeType = []

//            if (params.departmentType == "-1"){
//                departmentType = DepartmentType.list()
//            }else{
//                departmentType=DepartmentType.findById(params.departmentType)
//            }

            if (params.departmentList == "-1"){
                departmentList = Department.createCriteria().list {
//                    projections{
//                        'in'('departmenttype', departmentType)
//                    }
//                    and {
                        'in'('organization', organization)
//                    }
                }
            }
            else{
                departmentList=Department.findById(params.departmentList)
            }

            if(params.empType=="-1"){
                employeeType=EmployeeType.findAllByOrganization(organization)
            }
            else{
                employeeType=EmployeeType.findById(params.empType)
            }

            RoleType roleType = null
            if(params.roletypefaculty==null){
                flash.message="Please select Role type for Faculty!"
                return
            }else{
                roleType=RoleType.findById(params.roletypefaculty)
            }

            Role role = null
            if(params.roleidfaculty==null){
                flash.message="Please select Role!"
                return
            }else{
                role=Role.findById(params.roleidfaculty)
            }

            for(ch in checkedValue) {
                def i = Instructor.findById(ch)
                if(i) {
                    Login login1 = Login.findByUsername(i.uid)
                    if (login1) {
                        def roles = login1.roles
                        if (!roles.contains(role)) {
                            roles.add(role)
                            login1.roles = roles
                            login1.save(failOnError: true, flush: true)
                        }
                    }
                }
            }
            flash.message = "Roles Assigning for Faculty completed!"
            [organization : organization]
        }else{
            flash.error = 'Please Select atleast One Faculty.'
            [organization : organization]
        }
    }

    def revokeRoleforFaculty(){
        println("In revokeRoleforFaculty" + params)

        flash.message = ""
        flash.error = ""

        Login login = Login.findById(session.loginid)

        Organization organization = Organization.findById(session.orgid)

        if(params.checkedValue) {
            def checkedValue
            if (params.checkedValue.class.isArray())
                checkedValue = params.checkedValue
            else {
                checkedValue = []
                checkedValue.add(params.checkedValue)
            }
    //        def departmentType = []
            def departmentList = []
            def employeeType = []

    //        if (params.departmentType == "-1"){
    //            departmentType = DepartmentType.list()
    //            println("-1 departmentType " + departmentType)
    //        }
    //        else{
    //            departmentType=DepartmentType.findById(params.departmentType)
    //            println("departmentType "+departmentType)
    //        }

            if (params.departmentList == "-1"){
                departmentList = Department.createCriteria().list {
    //                projections{
    //                    'in'('departmenttype', departmentType)
    //                }
    //                and {
                        'in'('organization', organization)
    //                }
                }
                println("-1 departmentList " + departmentList)
            }
            else{
                departmentList=Department.findById(params.departmentList)
                println("departmentList "+departmentList)
            }

            if(params.empType=="-1"){
                employeeType=EmployeeType.findAllByOrganization(organization)
                println("-1 employeeType "+employeeType)
            }
            else{
                employeeType=EmployeeType.findById(params.empType)
                println("employeeType "+employeeType)
            }

            RoleType roleType = null
            if(params.roletypefaculty==null){
                flash.message="Please select Role type for Faculty!"
                return
            }
            else{
                roleType=RoleType.findById(params.roletypefaculty)
                println("roleType: "+roleType)
            }

            Role role = null
            if(params.roleidfaculty==null){
                flash.message="Please select Role!"
                return
            }
            else{
                role=Role.findById(params.roleidfaculty)
                println("role: "+role)
            }

            for(ch in checkedValue) {
                def instructor = Instructor.findById(ch)
                if(instructor) {
                    Login login2 = Login.findByUsername(instructor.uid)
                    //println("login2 "+login2)
                    if (login2) {
                        login2.removeFromRoles(role)
                        login2.save(flush: true, failOnError: true)
                        println("Role Removed!")
                    }
                }
            }

            flash.message = "Roll Revoking completed!"
            [organization : organization]
        }else{
            flash.error = 'Please Select atleast One Faculty.'
            [organization : organization]
        }
    }

    def getCompleteFacultyList(){
        println("In getFacultyList" + params)

        flash.message = ""
        flash.error = ""

        Login login = Login.findById(session.loginid)

        Organization organization = Organization.findById(session.orgid)

//        def departmentType = []
        def departmentList = []
        def employeeType = []
        def instructorlist = null

//        if (params.departmentType == "-1"){
//            departmentType = DepartmentType.list()
//            println("-1 departmentType " + departmentType)
//        }
//        else{
//            departmentType=DepartmentType.findById(params.departmentType)
//            println("departmentType "+departmentType)
//        }

        if (params.departmentList == "-1"){
            departmentList = Department.createCriteria().list {
//                projections{
//                    'in'('departmenttype', departmentType)
//                }
//                and {
                    'in'('organization', organization)
//                }
            }
            println("-1 departmentList " + departmentList)
        }
        else{
            departmentList=Department.findById(params.departmentList)
            println("departmentList "+departmentList)
        }

        if(params.empType=="-1"){
            employeeType=EmployeeType.findAllByOrganization(organization)
            println("-1 employeeType "+employeeType)
        }
        else{
            employeeType=EmployeeType.findById(params.empType)
            println("employeeType "+employeeType)
        }

        RoleType roleType = null
        if(params.roletypefaculty==null){
            flash.message="Please select Role type for Faculty!"
            return
        }
        else{
            roleType=RoleType.findById(params.roletypefaculty)
        }

        Role role = null
        if(params.roleidfaculty==null){
            flash.message="Please select Role!"
            return
        }
        else{
            role=Role.findById(params.roleidfaculty)
            println("role: "+role)
        }

        instructorlist = Instructor.createCriteria().list {
            projections{
                'in'('department', departmentList)
            }
            and {
                eq('iscurrentlyworking', true)
                'in'('organization', organization)
            }
            and{
                'in'('employeetype',employeeType)
            }
        }
        println("instructorlist " + instructorlist)

        [instructorList1:instructorlist, role:role, roleType:roleType,
//         depttypeid : params.departmentType,
         deptid : params.departmentList,
         emptypeid : params.empType]
    }

    def getFacultyList(){
        println("In getFacultyList" + params)

        flash.message = ""
        flash.error = ""

        Login login = Login.findById(session.loginid)

        Organization organization = Organization.findById(session.orgid)

//        def departmentType = []
        def departmentList = []
        def employeeType = []
        def instructorlist = null

//        if (params.departmentType == "-1"){
//            departmentType = DepartmentType.list()
//            println("-1 departmentType " + departmentType)
//        }
//        else{
//            departmentType=DepartmentType.findById(params.departmentType)
//            println("departmentType "+departmentType)
//        }

        if (params.departmentList == "-1"){
            departmentList = Department.createCriteria().list {
//                projections{
//                    'in'('departmenttype', departmentType)
//                }
//                and {
                    'in'('organization', organization)
//                }
            }
            println("-1 departmentList " + departmentList)
        }
        else{
            departmentList=Department.findById(params.departmentList)
            println("departmentList "+departmentList)
        }

        if(params.empType=="-1"){
            employeeType=EmployeeType.findAllByOrganization(organization)
            println("-1 employeeType "+employeeType)
        }
        else{
            employeeType=EmployeeType.findById(params.empType)
            println("employeeType "+employeeType)
        }

        RoleType roleType = null
        if(params.roletypefaculty==null){
            flash.message="Please select Role type for Faculty!"
            return
        }
        else{
            roleType=RoleType.findById(params.roletypefaculty)
            println("roleType: "+roleType)
        }

        Role role = null
        if(params.roleidfaculty==null){
            flash.message="Please select Role!"
            return
        }
        else{
            role=Role.findById(params.roleidfaculty)
            println("role: "+role)
        }

        instructorlist = Instructor.createCriteria().list {
            projections{
                'in'('department', departmentList)
            }
            and {
                eq('iscurrentlyworking', true)
                'in'('organization', organization)
            }
            and{
                'in'('employeetype',employeeType)
            }
        }
        println("instructorlist " + instructorlist)

        def login1 = null
        def instructorList1 = []
        def roles = null

        for(Instructor instructor:instructorlist){
            if(instructor){
                login1=Login.findByUsername(instructor.uid)
                if(login1){
                    roles=login1.roles.id
                    //roles = roles.flatten()
                    //println "roles:"+ roles
                    //println "role:"+ role.id
                    if(roles.contains(role.id)){
                        instructorList1.add(instructor)
                    }
                }
            }
            /*else{
                flash.message="Students in "+ programList +" doesn't have "+role +" role of "+ roleType +" !"
                return
            }*/
        }
        //println("instructorList1 "+instructorList1)
        [instructorList1:instructorList1,role:role,roleType:roleType,
//         depttypeid : params.departmentType,
         deptid : params.departmentList,
         emptypeid : params.empType]
    }
    //nandita(17-7-19)

    def assigntoastudent() {
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization organization = instructor.organization
        def student = Learner.findAllByOrganization(organization)
        String temp = ""
        ArrayList stdlist = new ArrayList()
        for (Learner l : student) {
            temp = ""
            if (l.registration_number) {
                temp = temp + l.registration_number + ":"
                if (l.person.fullname_as_per_previous_marksheet) {
                    temp = temp + l.person.fullname_as_per_previous_marksheet
                }
            }
            stdlist.add(temp)
        }
        session.organization = organization
        if(params.cid!=null)
        {
            if (params.cid.isNumber())
            {
                [stdlist: stdlist, abc:params.cid]
            }
            else
            {
                [stdlist: stdlist]

            }
        }
        else
        {[stdlist: stdlist,abc: null]}
    }
    def Tableforrole(){
        //println("I'm in Tablecode")

        println("params :"+params.learner)
        def arr

        String code
        if(params.learner)
        { arr = params.learner.split(":")
            code = arr[0]
            println("code : "+code)
        }
        else
        {
            code = session.code
            println("code : "+code)
        }
        Organization organization = Organization.findById(session.orgid)
        def learner = Learner.findByRegistration_numberAndOrganization(code, organization)
        Login login=Login.findByUsername(learner?.uid)
        def roles = login.roles
        ArrayList rolelist= new ArrayList()

        for(Role r:roles)
        {

            rolelist.add(r)
        }
        rolelist.sort{it.roletype.type}
        println("rolelist:"+rolelist)
        session.code=code
        session.rolelist = rolelist.id
        [rolelist:rolelist,code:code]
    }
    def deletefield(){
        Role role=Role.findById(params.id)

        Organization org = Organization.findById(session.orgid)
        def user = Instructor.findByEmployee_codeAndOrganization(session.code, org)
        if(!user)
            user = Learner.findByRegistration_numberAndOrganization(session.code, org)
        Login login=Login.findByUsername(user?.uid)

        login.removeFromRoles(role)
        login.save(flush: true, failOnError: true)
        def roles=login.roles
        ArrayList rolelist= new ArrayList()
        for(Role r:roles)
        {
            rolelist.add(r)
        }
        rolelist.sort{it.roletype.type}
        redirect(action: 'assigntoastudent', params: [cid :session.code])
    }
    def addroles(){

        println("In getmodule..." +params)
        ApplicationType ApplicationType=ApplicationType.findByApplication_type("ERP")
        println("ApplicationType "+ApplicationType)
        Organization orgn = Organization.findById(session.orgid)
        def list = RoleType.findAllByApplicationtypeAndOrganization(ApplicationType, orgn)

        [RoleType:list,ApplicationType:params.ApplicationType]
    }

    def newaddedfield()///////////saving fields on the same page
    {
        println("params in save:"+params)
        println("Role in save multiple:"+params.Role)
        Organization org = Organization.findById(session.orgid)
        def user = Instructor.findByEmployee_codeAndOrganization(session.code, org)
        if(!user)
            user = Learner.findByRegistration_numberAndOrganization(session.code, org)
        Login login=Login.findByUsername(user?.uid)
        println("login.grnumber: "+login)

        if(params.Role!=null)
        {
            if(params.Role.getClass().isArray())
            {
                def rolelist = login.roles

                for(e in params.Role)
                {
                    println("e :"+e)
                    println("login=" +login)
                    Role role=Role.findById(e)
                    println("Role:"+role)
                    //login.addToRoles(role)
                    rolelist.add(role)

                }
                login.roles = rolelist
                login.save(flush:true,failOnError:true)
            }
            else
            {
                def rolelist = login.roles
                println("Params.role :"+params.Role)
                println("login=" +login)
                Role role=Role.findById(params.Role)
                println("Role:"+role)
                rolelist.add(role)
                //login.addToRoles(role)
                login.roles = rolelist
                login.save(flush:true,failOnError:true)

            }
        }
        ////render "Role Added Successfully.."

        flash.message="Role Added Successfully.."
        redirect(action: 'assigntoastudent', params: [cid :session.code])

    }
    def newrole()/////////// calling new role
    {

        //println("In getrole..." +params)

        Organization orgn = Organization.findById(session.orgid)
        UserType typeuser = UserType.findByTypeAndOrganization("Student", orgn)
        RoleType roleType=RoleType.findById(params.RoleType)
        println("RoleType "+roleType)
        def list = Role.findAllByRoletypeAndUsertypeAndOrganization( roleType,typeuser, orgn).id
        println("List "+list)

        println("session.rolelist : " + session.rolelist )

        int n = list.size()
        println("count :"+n)
        // println("count :"+n)

        def listin = Role.findAllByRoletypeAndOrganizationAndUsertypeAndIdNotInList( roleType, orgn, typeuser,session.rolelist)
        println("listin : " + listin )


        //println("rolelist print in newrole: "+session.rolelist)

        [RoleList: listin, RoleType:roleType, ApplicationType: params.ApplicationType]

    }

    //sheetal(18-7-19)
    //Admission Only
    def displayEmpAdmission()
    {
        println("displayEmpAdmission params : "+params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization organization = instructor.organization
        def emplist=Instructor.findAllByOrganizationAndIscurrentlyworking(organization,true)
        println("emplist : "+ emplist )
        def rolelist=Role.findAllByOrganization(instructor?.organization)
        println("rolelist : "+ rolelist )
        Instructor defaultInstructor = null
        def defInstructorRoleList = []

        if(params.isedit == "1")
        {
            defaultInstructor = Instructor.findById(params.defaultInstructor)
        }
        else
        {
            if(emplist!= null)
            {
                defaultInstructor = emplist.get(0)
                println("defaultInstructor : "+ defaultInstructor )
            }
        }
        if(defaultInstructor != null)
        {
            Login l = Login.findByUsername(defaultInstructor.uid)
            defInstructorRoleList = l.roles
        }
        println("defInstructorRoleList : " + defInstructorRoleList)
        ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
        println("applicationType :" + applicationType)
        println("session.module :" + session.module)

        RoleType admission=RoleType.findByTypeAndApplicationtype(session.module,applicationType)
        println("admission :"+admission)

        def rolelst = []
        for(Role r : defInstructorRoleList)
        {
            println("... : " + r.roletype)
            if(r.roletype.id == admission.id )
            {
                rolelst.add(r)
            }
        }
        println("defInstructorRoleList : " + rolelst)
        println("defaultInstructor : " + defaultInstructor.id)

        [emplist:emplist,rolelist:rolelist,defaultInstructor:defaultInstructor,defInstructorRoleList:rolelst]
    }
    def addRoleAdmission()
    {
        println(" addRoleAdmission params : "+params)
        Instructor instructor=Instructor.findById(params.instructorid)
        println("instructor  : "+instructor.employee_code)
        Login login=Login.findByUsername(instructor.uid)
        Organization org=instructor.organization
        ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
        println("applicationType:" + applicationType)
        println("session.module : " + session.module)
        RoleType admission=RoleType.findByTypeAndApplicationtype(session.module,applicationType)
        println("admission :"+admission)
        UserType usertype=UserType.findByTypeAndOrganization("Employee", org)
        def rolelist = Role.findAllByRoletypeAndUsertypeAndOrganization(admission,usertype, org)

        println("login : "+login)
        println("org : "+org)
        def roles=login.roles
        def rolespresent = []
        for(Role r : roles)
        {
            if(r.roletype.id == admission.id )
            {
                rolespresent.add(r)
            }
        }

        //checkboxcode

        def r

        if(params.rolecheckbox.getClass().isArray())
        {
            for (i in params.rolecheckbox)
            {

                def rolespre = []
                for(Role r1 : roles)
                {
                    if(r1.roletype.id == admission.id )
                    {
                        rolespre.add(r1)
                    }
                }
            }
        }
        else {
            r = Role.findById(params.rolecheckbox)
            println("role" + r)

        }

        println(" rolespresent : "+rolespresent)
        println(" rolelist : "+rolelist)
        rolelist.removeAll(rolespresent)
        println(" rolelist : "+rolelist)
        println(" rolelist : "+rolelist)
        println("params.instructorid : "+params.instructorid)
        [rolelist:rolelist,instructorid : params.instructorid]
    }
    def fetchClickAdmission()
    {
        println("fetchclickAdmission params : "+params)
        Instructor instructor=Instructor.findById(params.emp)
        println("instructor  : "+instructor.employee_code)
        Login login=Login.findByUsername(instructor.uid)
        Organization org=instructor.organization
        println("login : "+login)
        println("org : "+org)
        def roles=login.roles
        ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
        println("applicationType :" + applicationType)
        println("session.module :" + session.module)

        RoleType admission=RoleType.findByTypeAndApplicationtype(session.module,applicationType)
        println("admission :"+admission)
        def rolelist = []
        for(Role r : roles)
        {
            if(r.roletype == admission )
            {
                rolelist.add(r)
            }
        }

        println(" rolelist : "+rolelist)
        println("instructorid : "+instructor.id)
        [rolelist:rolelist, instructorid :instructor.id]
    }
    def deleteAdmissionRole()
    {
        println("deleteAdmissionRole params : "+params)
        Role role=Role.findById(params.id)
        println("role :  "+role)
        Instructor instructor=Instructor.findById(params.instid)
        println("instructor : "+instructor)
        Login login=Login.findByUsername(instructor.uid)
        println("login : "+login)
        def roles =  login.roles
        println("roles :" + login.roles)
        roles.remove(role)
        login.roles = roles
        println("roles :"+roles)
        login.save(flush: true, failOnError: true)
        redirect(action:"displayEmpAdmission" ,  params: [ isedit: 1 , defaultInstructor: instructor.id ])
    }
    def displayFetchAdmission()
    {
        println(" displayFetchAdmission params : "+params)
        Instructor instructor=Instructor.findById(params.instructorid)
        println("instructor  : "+instructor.employee_code)
        Login login=Login.findByUsername(instructor.uid)
        Organization org=instructor.organization
        ApplicationType applicationType = ApplicationType.findByApplication_type("ERP")
        println("applicationType:" + applicationType)
        println("session.module :" + session.module)
        RoleType admission=RoleType.findByTypeAndApplicationtype(session.module,applicationType)
        println("admission :"+admission)
        UserType usertype=UserType.findByTypeAndOrganization("Employee", org)
        def rolelist = Role.findAllByRoletypeAndUsertypeAndOrganization(admission,usertype, org)

        println("login : "+login)
        println("org : "+org)
        def roles=login.roles
        def rolespresent = []
        for(Role r : roles)
        {
            if(r.roletype.id == admission.id )
            {
                rolespresent.add(r)
            }
        }

        //checkboxcode

        def r

        if(params.rolecheckbox.getClass().isArray())
        {
            for (i in params.rolecheckbox)
            {

                def rolespre = []
                for(Role r1 : roles)
                {
                    if(r1.roletype.id == admission.id )
                    {
                        rolespre.add(r1)
                    }
                }
            }
        }
        else {
            r = Role.findById(params.rolecheckbox)
            println("role" + r)

        }

        println(" rolespresent : "+rolespresent)
        println(" rolelist : "+rolelist)
        rolelist.removeAll(rolespresent)
        println(" rolelist : "+rolelist)
        println(" rolelist : "+rolelist)
        println("params.instructorid : "+params.instructorid)
        [rolelist:rolelist,instructorid : params.instructorid]

    }
    def assignRoleAdmission()
    {
        println("assignRoleAdmission params : "+params)
        println("params.rolecheckbox : "+params.rolecheckbox)
        Instructor instructor=Instructor.findById(params.instructorid)
        println("instructor : "+instructor)
        Login login=Login.findByUsername(instructor.uid)
        println("login : "+login)
        def roles =  login.roles
        println("roles :" + login.roles)
        println("rolecheckbox:"+params.rolecheckbox.size())


        if (params.rolecheckbox.getClass().isArray()) {
            for(int i=0; i<params.rolecheckbox.size();i++) {
                Role rolecheck= Role.findById(params.rolecheckbox[i])
                println("rolecheck : "+rolecheck)
                roles.add(rolecheck)
            }
        }
        else
        {
            Role rolecheck= Role.findById(params.rolecheckbox)
            println("rolecheck : "+rolecheck)
            roles.add(rolecheck)
        }

        login.roles = roles
        println("roles"+roles)
        login.save(flush: true, failOnError: true)
        println("session.module :" + session.module)
        RoleType admission=RoleType.findByType(session.module)
        println("admission :"+admission)
        println(" roles : "+roles)
        def rolelist = []
        for(Role r : roles)
        {
            println("r :"+r)
            println("r.roletype:"+r.roletype )
            if(r.roletype.id == admission.id )
            {
                rolelist.add(r)
            }
        }
        println(" rolelist : "+rolelist)
        println(" instructorid : "+params.instructorid)
        redirect(action:"displayEmpAdmission" ,  params: [ isedit: 1 , defaultInstructor: params.instructorid ])
        return
    }

    //ARBAJ
    def assignRoletoStudent(){println("In assignRoletoStudent..."+params)}

    def getStudentData(){
        println("In getStudentData..."+params)
        Login login=Login.findById(session.loginid)
        Organization organization=Organization.findById(session.orgid)
        Learner learner = null
      //  ApplicationType applicationType=ApplicationType.findByApplication_type("ERP")
       // RoleType roleType=RoleType.findByApplicationtypeAndTypeAndOrganization(applicationType,"ERP Coordinator", organization)
      //  ApplicationAcademicYear aay=ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(roleType,true,organization)
        /*if(aay==null){
            render "<p style='color:red;font-size:14px;text-align:center;font-weight:bold;'>Please Set ApplicationAcademicYear of ERP Coordinator Module"
            return
        }*/
      //  AcademicYear aayacademicyear=aay.academicyear
      //  Semester aaysemester=aay.semester

        if(!params.grno){
            render "<script>alert('Please enter PRN No before proceeding!');history.back();location.reload();</script>"

        }else{
            learner=Learner.findByRegistration_numberAndOrganization(params.grno,organization)
            println"Learner :"+learner
        }
        if(learner==null){
            render "<script>alert('Student Not Found');history.back();location.reload();</script>"
        }

        /*LearnerDivision learnerdivision=LearnerDivision.findBySemesterAndAcademicyearAndLearnerAndOrganization(aaysemester,aayacademicyear,learner,organization)
        println"learnerdivision :"+learnerdivision

        Year year = null
        if(learnerdivision == null)
        {

          render "<p style='color:red;font-size:16px;text-align:center;font-weight:bold;'>Student " +  learner.registration_number + " not found in Roll Call of Academic Year " + aayacademicyear.ay + " and semester : " + aaysemester.sem

        }
        else
        {
            year = learnerdivision.year
        }*/

        //def learnerDivision=LearnerDivision.findAllByLearnerAndOrganization(learner,organization)

        /*Program program=learner.program
        Department dept =  program.department

        if(program.programtype.name == 'BTech' && year.year == 'FY' && learner.organization.organization_code == 'VIT')
        {
            program = Program.findByNameAndOrganization("DESH-DESH", organization)
            dept = Department.findByNameAndOrganization("Engineering, Science and Humanities", organization)
        }*/

        Login learnerLogin=Login.findByUsername(learner.uid)
        def roles = learnerLogin.roles
        ArrayList rolelist= new ArrayList()

        for(Role r:roles)
        {

            rolelist.add(r)
        }
        rolelist.sort{it.roletype.type}
        println("rolelist:"+rolelist)
        session.rolelist = rolelist.id
        println" session.rolelist "+ session.rolelist
        [learner    :learner,
         rolelist   :rolelist]
    }

    def addRolesNew(){
        println("In addRolesNew..." +params)
        ApplicationType ApplicationType=ApplicationType.findByApplication_type("ERP")
        println("ApplicationType "+ApplicationType)
        Organization orgn = Organization.findById(session.orgid)
        def roleType = RoleType.findAllByApplicationtypeAndOrganization(ApplicationType, orgn)

        [RoleType:roleType,ApplicationType:ApplicationType,gr_no:params.gr_no]
    }

    def LearnerNewRole(){
        println("In LearnerNewRole..." +params)

        Organization orgn = Organization.findById(session.orgid)
        UserType typeuser = UserType.findByTypeAndOrganization("Student", orgn)
        println"typeuser :"+typeuser
        RoleType roleType=RoleType.findById(params.RoleType)
        println("RoleType "+roleType)
        def list = Role.findAllByRoletypeAndUsertypeAndOrganization( roleType,typeuser, orgn).id
        println("List "+list)

        println("session.rolelist : " + session.rolelist )

        int n = list.size()
        println("count :"+n)

        def listin = Role.findAllByRoletypeAndOrganizationAndUsertypeAndIdNotInList(roleType, orgn, typeuser,session.rolelist)
        println("listin : " + listin )

        [RoleList: listin, RoleType:roleType, ApplicationType: params.ApplicationType,gr_no:params.gr_no]
    }

    def newAddedRole(){
        println("In newAddedRole..."+params)
        println("Role in save multiple:"+params.Role)

        Organization org = Organization.findById(session.orgid)
        def user = Instructor.findByEmployee_codeAndOrganization(params.gr_no, org)
        if(!user)
            user = Learner.findByRegistration_numberAndOrganization(params.gr_no, org)
        Login login=Login.findByUsername(user?.uid)

        println("login.grnumber: "+login)

        if(params.Role!=null)
        {
            if(params.Role.getClass().isArray())
            {
                def rolelist = login.roles

                for(e in params.Role)
                {
                    println("e :"+e)
                    println("login=" +login)
                    Role role=Role.findById(e)
                    println("Role:"+role)
                    //login.addToRoles(role)
                    rolelist.add(role)

                }
                login.roles = rolelist
                login.save(flush:true,failOnError:true)
            }
            else
            {
                def rolelist = login.roles
                println("Params.role :"+params.Role)
                println("login=" +login)
                Role role=Role.findById(params.Role)
                println("Role:"+role)
                rolelist.add(role)
                //login.addToRoles(role)
                login.roles = rolelist
                login.save(flush:true,failOnError:true)

            }
        }
        ////render "Role Added Successfully.."

        flash.message="Role Added Successfully.."
        redirect(action: 'assignRoletoStudent', params: [grno :user?.person?.grno])
    }

    def deleteRole(){
        Role role=Role.findById(params.id)
        Organization org = Organization.findById(session.orgid)
        def user = Instructor.findByEmployee_codeAndOrganization(params.gr_no, org)
        if(!user)
            user = Learner.findByRegistration_numberAndOrganization(params.gr_no, org)
        Login login=Login.findByUsername(user?.uid)

        login.removeFromRoles(role)
        login.save(flush: true, failOnError: true)
        if(params.ischange=="1"){
            redirect(action: 'assignRoletoMultipleIndividuals')
        }else if(params.ischange==null){
            def roles=login.roles
            ArrayList rolelist= new ArrayList()
            for(Role r:roles)
            {
                rolelist.add(r)
            }
            rolelist.sort{it.roletype.type}
            redirect(action: 'assignRoletoStudent', params: [grno :user?.person?.grno])
        }
    }

    def loginRolesOfEmployees(){
        println("In loginRolesOfEmployees..."+params)
        Login login=Login.findById(session.loginid)
        Organization organization=Organization.findById(session.orgid)

        //ArrayList instructorList=new ArrayList()
        def finalList = []
        def instructorList=Instructor.findAllByOrganizationAndIscurrentlyworking(organization, true)
        println"instructorList :"+instructorList
        //ArrayList loginList=new ArrayList()
        for(Instructor instructor:instructorList){
            Login login1=Login.findByUsername(instructor.uid)
            //loginList.add(login1)
            finalList.add(instructor:instructor , login:login1)
        }
        /*println"loginList :"+loginList
        println"loginList.size :"+loginList.size()
        println"instructorList.size :"+instructorList.size()*/
        [finalList:finalList]
    }

    def loginIsBlocked(){
        println("In loginIsBlocked..."+params)
        Login login=Login.findById(params.LoginId)
        println("login "+login.isloginblocked)
        if(login.isloginblocked){
            login.isloginblocked=false
            login.updation_ip_address=request.getRemoteAddr()
            login.updation_date=new java.util.Date()
        }else{
            login.isloginblocked=true
            login.updation_ip_address=request.getRemoteAddr()
            login.updation_date=new java.util.Date()
        }
        login.save(flush: true, failOnError: true)
        redirect(action:"loginRolesOfEmployees")
    }

    // PPS - 24-10-2019
    def setQuickLinks() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
        } else {
            println("setQuickLinks :: " + params)
            RoleLink  roleLinks
            if(params.roleLinkId != "null"){
                roleLinks=RoleLink.findById(params.roleLinkId)
                roleLinksService.setQuickLniks(roleLinks)
            }
            redirect(action: "rolelinks", params: [roletype : roleLinks.role?.roletype.id, role : roleLinks.role.id, usertype : roleLinks.role?.usertype.id])
        }
    }
}
