package volp

import com.tp.pg.util.TransactionResponseBean

import javax.servlet.http.Part
import java.nio.file.Paths
import java.text.SimpleDateFormat

class ApplicationController {

    def index() {}

    //Entrance Application
    def entranceApplication(){
        if(session.loginid == null){
            flash.message = "Your are logged out..! Please login again. Thank you..!"
            redirect(controller: "entranceAdmissionLogin", action: "login")
            return
        }else {
            println("I am in entrance Application")
            def entranceVersion = EntranceVersion.findAllByIsactive(true)
            def list = []
            for(version in entranceVersion) {
                EntranceApplicant entranceApplicant = EntranceApplicant.findById(session.loginid)

                def flag = true
                def tabmaster_list = EntranceStudentProfileTabMaster.findAllByIscompulsory(true)
                if(tabmaster_list.size() > 0) {
                    for(tab in tabmaster_list) {
                        EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(tab, entranceApplicant)
                        if(!applicant_profile_personal_status) {
                            flag = false
                            break
                        }
                    }
                }
                if(!flag) {
                    flash.error = "Please complete Profile Mandatory Tabs..."
                    redirect(controller: "entranceApplicant", action: "eadashboard")
                    return
                }
                def entranceApplication = EntranceApplication.findAllByEntranceversionAndEntranceapplicant(version,entranceApplicant)
                if(entranceApplication) {
                    for(item in entranceApplication){
                        list.add(version:version, application:item, isapplied : true, name: entranceApplicant?.fullname, isfeespaid: item?.isfeespaid)
                    }
                } else {
                    list.add(version:version, application:null, isapplied : false, name: entranceApplicant?.fullname, isfeespaid: false)
                }

            }
            //println("List >>"+ list)
            [entranceVersion: entranceVersion,list: list]
        }
    }
    def applyEntranceApplication(){
        if(session.loginid == null){
            flash.message = "Your are logged out..! Please login again. Thank you..!"
            redirect(controller: "entranceAdmissionLogin", action: "login")
            return
        }else {
            EntranceApplicant entranceApplicant = EntranceApplicant.findById(session.loginid)
            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersion)
            ProgramType PT = ProgramType.findById(params.programtype)
            Organization org = Organization.findById(entranceApplicant.entrancecategory.organization?.id)
            //println("entranceVersion >>"+ entranceVersion)
            def entranceFees = EntranceFees.findAllByEntranceversion(entranceVersion)
            def entranceBranch = EntranceBranch.findAllByEntranceversion(entranceVersion)
            def Eligibility = Eligibility.findAllByProgramtypeAndOrganizationAndIsactive(PT,org,true)
            def ExemptionType = ExemptionType.findAllByProgramtypeAndOrganizationAndIsactive(PT,org,true)

            String url = ""
            if(entranceApplicant.photopath && entranceApplicant.photoname) {
                String path = entranceApplicant.photopath + entranceApplicant.photoname
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSBucketService awsBucketService1 = new AWSBucketService()
                url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            }
            String Exemption_url = ""
            if(entranceApplicant.photopath && entranceApplicant.photoname) {
                String path = entranceApplicant.photopath + entranceApplicant.photoname
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSBucketService awsBucketService1 = new AWSBucketService()
                url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            }

            def entranceCategoryList = EntranceCategory.findAllWhere(isactive:true)

            [ProgramType:PT,EA:entranceApplicant,entranceVersion: entranceVersion,entranceFees: entranceFees,entranceBranch: entranceBranch,
             Eligibility:Eligibility,ExemptionType:ExemptionType, url:url, entranceCategoryList:entranceCategoryList]
        }
    }
    def saveEntranceApplication(){
        if(session.loginid == null){
            flash.message = "Your are logged out..! Please login again. Thank you..!"
            redirect(controller: "entranceAdmissionLogin", action: "login")
            return
        }else {
            println("I am in save Entrance Application :: "+ params)
            AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
            EntranceApplicant login = EntranceApplicant.findById(session.loginid)
            EntranceApplicant entranceApplicant = login
            EntranceCategory entranceCategory = EntranceCategory.findById(params.entrancecategory)
            def entranceCategoryType = entranceCategory?.entrancecategorytype
            Exemption exemption = Exemption.findByName("Exempted")
            EntranceVersion entranceVersion = EntranceVersion.findById(params.version)
            Organization org = entranceVersion.organization
            //println("entranceVersion >>"+ entranceVersion)
            //println("Branch >>"+ params.branch)

            if(!params.branch) {
                flash.error = "Please select at least one branch..!"
                redirect(controller:"application", action: "applyEntranceApplication", params: [entranceVersion:params.version, programtype:params.ProgramType])
                return
            }
            if(!params.entrancecategory) {
                flash.error = "Please select Category..!"
                redirect(controller:"application", action: "applyEntranceApplication", params: [entranceVersion:params.version, programtype:params.ProgramType])
                return
            }

            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
            def folderPath = awsFolderPath.path + "pre_admission/" + org.organization_code + "/" + entranceApplicant.id + "/"
            def existsPath = ""
            boolean visual_isUpload = false
            boolean physical_isUpload = false
            boolean isUpload = false
            def visual_fileName = ""
            def physical_fileName = ""
            if(params.is_physically_handicapped == "on") {
                Part physicalPart = request.getPart("physical_file")
                InputStream physical_fs = physicalPart.getInputStream()
                String physical_contentType = physicalPart.getContentType();
                String physical_filePartName = Paths.get(physicalPart.getSubmittedFileName()).getFileName().toString()
                int physical_indexOfDot = physical_filePartName.lastIndexOf('.')
                String physical_fileExtension
                if (physical_indexOfDot > 0) {
                    physical_fileExtension = physical_filePartName.substring(physical_indexOfDot + 1)
                }
                physical_fileName = "physical_" + entranceApplicant.id + "." + physical_fileExtension
                if(physical_fileExtension != null) {
                    physical_isUpload = awsUploadDocumentsService.uploadDocument(physicalPart, folderPath, physical_fileName, existsPath)
                    if (physical_isUpload) {
                        println("physical_isUpload Document Uploded Successfully")
                    }
                }
            }
            if(params.is_visually_handicapped == "on") {
                Part visualPart = request.getPart("visual_file")
                InputStream visual_fs = visualPart.getInputStream()
                String visual_contentType = visualPart.getContentType();
                String visual_filePartName = Paths.get(visualPart.getSubmittedFileName()).getFileName().toString()
                int visual_indexOfDot = visual_filePartName.lastIndexOf('.')
                String visual_fileExtension
                if (visual_indexOfDot > 0) {
                    visual_fileExtension = visual_filePartName.substring(visual_indexOfDot + 1)
                }
                if(visual_fileExtension != null) {
                    visual_fileName = "visual_" + entranceApplicant.id + "." + visual_fileExtension
                    visual_isUpload = awsUploadDocumentsService.uploadDocument(visualPart, folderPath, visual_fileName, existsPath)
                    if (visual_isUpload) {
                        println("visual_isUpload Document Uploded Successfully")
                    }
                }
            }
            def examption_type_list = []
            def exemption_documents_list = []
            if(params.eligible) {
                if (params.ExemptionType.class.isArray()) {
                    for(type in params.ExemptionType) {
                        ExemptionType exemptionType = ExemptionType.findById(type)
                        if(exemptionType) {
                            examption_type_list.add(exemptionType)
                        }
                    }
                } else {
                    examption_type_list.add(ExemptionType.findById(params.ExemptionType))
                }
                for (type in examption_type_list) {
                    Part filePart = request.getPart(type.abbr)
                    InputStream fs = filePart.getInputStream()
                    String contentType = filePart.getContentType();
                    String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                    int indexOfDot = filePartName.lastIndexOf('.')
                    String fileExtension
                    if (indexOfDot > 0) {
                        fileExtension = filePartName.substring(indexOfDot + 1)
                    }
                    def fileName = type.abbr + "_" + entranceApplicant.id + "." + fileExtension
                    if(fileExtension != null) {
                        isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsPath)
                        if (isUpload) {
                            ExemptionDocument document = ExemptionDocument.findByExemptiontypeAndEntranceversionAndEntranceapplicantAndOrganization(type, entranceVersion, entranceApplicant, org)
                            println("document  :: " + document)
                            if (document) {
                                document.file_path = folderPath
                                document.file_name = fileName
                                document.updation_username = login.email
                                document.updation_date = new Date()
                                document.updation_ip_address = request.getRemoteAddr()
                                document.save(flush: true, failOnError: true)

                                exemption_documents_list.add(document)
                            } else {
                                document = new ExemptionDocument()
                                document.file_path = folderPath
                                document.file_name = fileName
                                document.creation_username = login.email
                                document.updation_username = login.email
                                document.creation_date = new Date()
                                document.updation_date = new Date()
                                document.creation_ip_address = request.getRemoteAddr()
                                document.updation_ip_address = request.getRemoteAddr()
                                document.organization = org
                                document.exemptiontype = type
                                document.entranceversion = entranceVersion
                                document.entranceapplicant = entranceApplicant
                                document.save(flush: true, failOnError: true)

                                exemption_documents_list.add(document)
                            }
                            println(type.abbr + " isUpload Document Uploded Successfully")
                        }
                    }
                }
            }
            println("examption_type_list :: " + examption_type_list)
            println("exemption_documents_list :: " + exemption_documents_list)

            EntranceRejectionReason entranceRejectionReason = EntranceRejectionReason.findByNameAndOrganizationAndIsactive("Under Process", entranceVersion?.organization, true)
            EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findByNameAndOrganization("In-Process", entranceVersion?.organization)
            ProgramType PT = ProgramType.findById(params.ProgramType)

            def application_id = entranceVersion.applicationtrack.toString()
            EntranceApplication EA = null
            if(entranceVersion){
                EA = EntranceApplication.findById(params.application)
                if(EA){
                    EA.applicationdate = new Date()
                    EA.place = params.place.trim()
                    if(params.declare){
                        EA.is_declaration_accepted = true
                    }else {
                        EA.is_declaration_accepted = false
                    }
                    if(params.eligible && examption_type_list.size() > 0){
                        println("Yes")
                        EA.is_exemption_from_test = true
                    }else {
                        println("No")
                        EA.is_exemption_from_test = false
                    }
                    if(params.is_physically_handicapped == "on"){
                        EA.is_physically_handicapped = true
                    }else {
                        EA.is_physically_handicapped = false
                    }
                    if(params.is_visually_handicapped == "on"){
                        EA.is_visually_handicapped = true
                    }else {
                        EA.is_visually_handicapped = false
                    }
                    if(physical_isUpload) {
                        EA.is_physically_handicapped_file_path = folderPath
                        EA.is_physically_handicapped_file_name = physical_fileName
                    }
                    if(visual_isUpload) {
                        EA.is_visually_handicapped_file_path = folderPath
                        EA.is_visually_handicapped_file_name = visual_fileName
                    }
                    EA.entrancecategory = entranceCategory
                    EA.entrancecategorytype = entranceCategoryType
                    EA.exemption = exemption
                    EA.organization = entranceVersion?.organization
                    EA.entranceapplicant = entranceApplicant
                    EA.entranceversion = entranceVersion
                    // EA.exemptiontype = ET
                    def pp = EA.entrancebranch.id
                    for(pap in pp){
                        def temp = EntranceBranch.findById(pap)
                        EA.removeFromEntrancebranch(temp)
                    }
                    for(branch in params.branch){
                        EntranceBranch entranceBranch = EntranceBranch.findById(branch)
                        EA.addToEntrancebranch(entranceBranch)
                    }
                    EA.actionby = null
                    EA.entrancerejectionreason = entranceRejectionReason
                    EA.entranceapplicationstatus = entranceApplicationStatus
                    EA.entranceapplicationreceipt = null

                    EA.creation_username = login.email
                    EA.updation_username = login.email
                    EA.creation_date = new Date()
                    EA.updation_date = new Date()
                    EA.creation_ip_address = request.getRemoteAddr()
                    EA.updation_ip_address = request.getRemoteAddr()
                    EA.save(flush: true, failOnError: true)
//                    EA.save(failOnError: true, flush: true)

                    if(examption_type_list.size() > 0) {
                        def exemptiontype_id_list = EA.exemptiontype.id
                        for(pap in exemptiontype_id_list){
                            def temp = ExemptionType.findById(pap)
                            EA.removeFromExemptiontype(temp)
                        }
                        EA.addToExemptiontype(examption_type_list)
                        EA.save(flush: true, failOnError: true)
                    }

                    if(exemption_documents_list.size() > 0) {
                        def exemptiontypedocument_id_list = EA.exemptiondocument.id
                        for(pap in exemptiontypedocument_id_list){
                            def temp = ExemptionDocument.findById(pap)
                            EA.removeFromExemptiondocument(temp)
                        }
                        EA.addToExemptiondocument(exemption_documents_list)
                        EA.save(flush: true, failOnError: true)
                    }
                    flash.message = "Applied Successfully..!"
                } else {
                    println("new Application")
                    EA = new EntranceApplication()
                    EA.applicaitionid = application_id
                    EA.applicationdate = new Date()
                    EA.place = params.place.trim()
                    if(params.declare){
                        EA.is_declaration_accepted = true
                    }else {
                        EA.is_declaration_accepted = false
                    }
                    if(params.eligible && examption_type_list.size() > 0){
                        EA.is_exemption_from_test = true
                    }else {
                        EA.is_exemption_from_test = false
                    }
                    if(params.is_physically_handicapped){
                        EA.is_physically_handicapped = true
                    }else {
                        EA.is_physically_handicapped = false
                    }
                    if(params.is_visually_handicapped){
                        EA.is_visually_handicapped = true
                    }else {
                        EA.is_visually_handicapped = false
                    }
                    if(physical_isUpload) {
                        EA.is_physically_handicapped_file_path = folderPath
                        EA.is_physically_handicapped_file_name = physical_fileName
                    }
                    if(visual_isUpload) {
                        EA.is_visually_handicapped_file_path = folderPath
                        EA.is_visually_handicapped_file_name = visual_fileName
                    }
                    EA.entrancecategory = entranceCategory
                    EA.entrancecategorytype = entranceCategoryType
                    EA.exemption = exemption
                    EA.organization = entranceVersion?.organization
                    EA.entranceapplicant = entranceApplicant
                    EA.entranceversion = entranceVersion
                    //EA.exemptiontype = ET

                    /*for(type in params.ExemptionType){
                        ExemptionType et = ExemptionType.findById(type)
                        EA.addToExemptiontype(et)

//                        Organization org = Organization.findById(entranceVersion.organization?.id)
                        ExemptionDocument exemptionDocument = new ExemptionDocument()
//                        AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
//                        AWSBucket awsBucket = AWSBucket.findByContent("documents")
//                        AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()

                        println("org >>"+ org)
                        Part filePart = request.getPart("newFile")
                        def fileobj = request.getFile("newFile")

                        if (fileobj) {
                            //println("File >>"+ fileobj)
                            String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                            int indexOfDot = filePartName.lastIndexOf('.')
                            def fileExtention
                            if (indexOfDot > 0) {
                                fileExtention = filePartName.substring(indexOfDot + 1)
                                println("fileExtention >>"+ fileExtention)
                            }
                            String name1 = entranceApplicant?.fullname.replaceAll("\\s", "")
                            String name = name1.toLowerCase()
                            String orgName1 = org?.organization_name.replaceAll("\\s", "")
                            String orgName = orgName1.toLowerCase()
                            String DocumentType1 = et?.name.replaceAll("\\s", "")
                            String DocumentType = DocumentType1.toLowerCase()
                            String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
//                            def folderPath = awsFolderPath.path + awsBucket?.content + "/" + orgName + "/" + "Application" + "/" + name
                            def fileName = "Doc_" + name + "_" + date + "_" + DocumentType + "." + fileExtention
                            String existsFilePath = ""

//                            def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                            def isUpload1 = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                            if (isUpload) {
                                exemptionDocument.file_path = folderPath
                                exemptionDocument.file_name = fileName
                                exemptionDocument.organization = org
                                exemptionDocument.exemptiontype = et
                                exemptionDocument.entranceversion = entranceVersion
                                exemptionDocument.entranceapplicant = entranceApplicant
                                exemptionDocument.creation_username = entranceApplicant?.email
                                exemptionDocument.updation_username = entranceApplicant?.email
                                exemptionDocument.creation_date = new java.util.Date()
                                exemptionDocument.updation_date = new java.util.Date()
                                exemptionDocument.creation_ip_address = request.getRemoteAddr()
                                exemptionDocument.updation_ip_address = request.getRemoteAddr()
//                                exemptionDocument.save(flush: true, failOnError: true)
                                println "File uploaded Successfully..!"
                                flash.success = "File Uploaded Successfully..!"
                            }
                        }
                    }*/
                    EA.actionby = null
                    EA.entrancerejectionreason = entranceRejectionReason
                    EA.entranceapplicationstatus = entranceApplicationStatus
                    EA.entranceapplicationreceipt = null

                    EA.creation_username = login.email
                    EA.updation_username = login.email
                    EA.creation_date = new Date()
                    EA.updation_date = new Date()
                    EA.creation_ip_address = request.getRemoteAddr()
                    EA.updation_ip_address = request.getRemoteAddr()
                    EA.save(flush: true, failOnError: true)

                    for(branch in params.branch){
                        EntranceBranch entranceBranch = EntranceBranch.findById(branch)
                        EA.addToEntrancebranch(entranceBranch)
                        EA.save(flush: true, failOnError: true)
                    }
                    if(examption_type_list.size() > 0) {
                        for(type in examption_type_list) {
                            EA.addToExemptiontype(type)
                            EA.save(flush: true, failOnError: true)
                        }
                    }

                    if(exemption_documents_list.size() > 0) {
                        for(type in exemption_documents_list) {
                            EA.addToExemptiondocument(type)
                            EA.save(flush: true, failOnError: true)
                        }
                    }

                    entranceVersion.applicationtrack = Integer.parseInt(application_id) + 1
                    entranceVersion.save(flush: true, failOnError: true)
                    flash.message = "Applied Successfully..!"
                }
            }
            redirect(controller: "Application", action: "entranceadmissionspayfees", params: [applicationid:EA.id, versionid:entranceVersion.id])
        }
    }

    def getDocument() {
        println("getDocument :: " + params)

        EntranceApplicant login = EntranceApplicant.findById(session.loginid)
        EntranceApplication application = EntranceApplication.findById(params.versionid)
        EntranceVersion entranceVersion = application.entranceversion
        ExemptionType exemptionType = ExemptionType.findById(params.exemptiontypeid)

        ExemptionDocument document = ExemptionDocument.findByExemptiontypeAndEntranceversionAndEntranceapplicantAndOrganization(exemptionType, entranceVersion, login, entranceVersion.organization)
        if(document) {
            String path = document.file_path + document.file_name
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSBucketService awsBucketService1 = new AWSBucketService()
            String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

            render "<a href='" + url + "'>" + document.file_name + "</a>"
        } else {
            render "-"
            return
        }
    }

    def editEntranceApplication(){
        if(session.loginid == null){
            flash.message = "Your are logged out..! Please login again. Thank you..!"
            redirect(controller: "entranceAdmissionLogin", action: "login")
            return
        }else {
            println("I am in Edit Entrance Application"+ params)
            EntranceApplicant login = EntranceApplicant.findById(session.loginid)
            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersion)
            EntranceApplicant EA = login
            Organization org = entranceVersion.organization
            ProgramType PT = ProgramType.findById(params.programtype)
            def entranceFees = EntranceFees.findAllByEntranceversion(entranceVersion)
            def entranceBranch = EntranceBranch.findAllByEntranceversion(entranceVersion)
            def Eligibility = Eligibility.findAllByProgramtypeAndOrganizationAndIsactive(PT,org,true)
            def ExemptionType = ExemptionType.findAllByProgramtypeAndOrganizationAndIsactive(PT,org,true)

            def list = []
            def appliedbranchlist = []
            def appliedexamptiontypelist
            def entranceApplication = null
            for(version in entranceVersion) {
                EntranceApplicant entranceApplicant = EntranceApplicant.findById(session.loginid)
                entranceApplication = EntranceApplication.findAllByEntranceversionAndEntranceapplicantAndOrganization(version,entranceApplicant,org)
                if(entranceApplication) {
                    for(item in entranceApplication){
                        list.add(version:version, application:item, isapplied : true, name: entranceApplicant?.fullname, isfeespaid: item?.isfeespaid)
                    }
                    appliedbranchlist = entranceApplication.entrancebranch
                    appliedexamptiontypelist = entranceApplication.exemptiontype
                }
            }

            String url = ""
            if(EA.photopath && EA.photoname) {
                String path = EA.photopath + EA.photoname
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSBucketService awsBucketService1 = new AWSBucketService()
                url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            }
            String physical_url = ""
            if(entranceApplication[0]?.is_physically_handicapped_file_path && entranceApplication[0]?.is_physically_handicapped_file_name) {
                String path = entranceApplication[0]?.is_physically_handicapped_file_path + entranceApplication[0]?.is_physically_handicapped_file_name
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSBucketService awsBucketService1 = new AWSBucketService()
                physical_url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            }
            String visual_url = ""
            if(entranceApplication[0]?.is_visually_handicapped_file_path && entranceApplication[0]?.is_visually_handicapped_file_name) {
                String path = entranceApplication[0]?.is_visually_handicapped_file_path + entranceApplication[0]?.is_visually_handicapped_file_name
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSBucketService awsBucketService1 = new AWSBucketService()
                visual_url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
            }

            def entranceCategoryList = EntranceCategory.findAllWhere(isactive:true)

            [physical_url:physical_url, visual_url:visual_url, entranceApplication:entranceApplication, list: list,EA:EA,entranceFees: entranceFees,entranceBranch: entranceBranch,Eligibility:Eligibility,ExemptionType:ExemptionType,
             appliedbranchlist:appliedbranchlist, appliedexamptiontypelist:appliedexamptiontypelist, url:url, entranceCategoryList:entranceCategoryList]
        }
    }

    /*def saveExemptionDocumentDetails(){
        println("In save Exemption Document Details"+ params)
        if(session.loginid == null){
            flash.message = "Your are logged out..! Please login again. Thank you..!"
            redirect(controller: "entranceAdmissionLogin", action: "login")
            return
        }
        else {
            EntranceApplicant EA = EntranceApplicant.findByEmail(session.email)
            if(params.ExemptionType == 'null'){
                flash.error = "Please select Exemption Type..!"
                redirect(action: 'applyEntranceApplication')
                return
            }
            else {
                if(EA){
                    Organization org = Organization.findById(EA.entrancecategory.organization?.id)
                    ExemptionType exemptionType = ExemptionType.findById(params.ExemptionType)
                    EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersion)
                    ExemptionDocument exemptionDocument = new ExemptionDocument()

                    AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                    AWSBucket awsBucket = AWSBucket.findByContent("documents")
                    AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()

                    Part filePart = request.getPart("newFile")
                    def fileobj = request.getFile("newFile")
                    if (!fileobj.empty) {
                        String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                        int indexOfDot = filePartName.lastIndexOf('.')
                        def fileExtention
                        if (indexOfDot > 0) {
                            fileExtention = filePartName.substring(indexOfDot + 1)
                        }
                        String name1 = EA?.fullname.replaceAll("\\s", "")
                        String name = name1.toLowerCase()
                        String orgName1 = org?.organization_name.replaceAll("\\s", "")
                        String orgName = orgName1.toLowerCase()
                        String DocumentType1 = exemptionType?.name.replaceAll("\\s", "")
                        String DocumentType = DocumentType1.toLowerCase()
                        String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                        def folderPath = awsFolderPath.path + awsBucket?.content + "/" + orgName + "/" + "Application" + "/" + name
                        def fileName = "Doc_" + name + "_" + date + "_" + DocumentType + "." + fileExtention
                        String existsFilePath = ""

                        def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                        if (isUpload) {
                            exemptionDocument.file_path = folderPath
                            exemptionDocument.file_name = fileName

                            exemptionDocument.organization = org
                            exemptionDocument.exemptiontype = exemptionType
                            exemptionDocument.entranceversion = entranceVersion
                            exemptionDocument.entranceapplicant = EA
                            exemptionDocument.creation_username = EA?.email
                            exemptionDocument.updation_username = EA?.email
                            exemptionDocument.creation_date = new java.util.Date()
                            exemptionDocument.updation_date = new java.util.Date()
                            exemptionDocument.creation_ip_address = request.getRemoteAddr()
                            exemptionDocument.updation_ip_address = request.getRemoteAddr()
                            exemptionDocument.save(flush: true, failOnError: true)
                            println "File uploaded"
                            flash.success = "File Uploaded Successfully..!"
                            redirect(action: 'applyEntranceApplication')
                            return
                        } else {
                            flash.error = "File Uploading failed.. Please try again!"
                            redirect(action: 'applyEntranceApplication')
                            return
                        }
                    }
                    if (fileobj.empty) {
                        flash.error = "File is empty..!"
                        redirect(action: 'applyEntranceApplication')
                        return
                    }
                }
            }
        }
    } */

    def trackEntranceApplication(){
        if(session.loginid == null){
            flash.message = "Your are logged out..! Please login again. Thank you..!"
            redirect(controller: "entranceAdmissionLogin", action: "login")
            return
        }else {
            println("I am in Track Entrance Application")
            EntranceApplicant entranceApplicant = EntranceApplicant.findById(session.loginid)
            def entranceApplication = EntranceApplication.findAllByEntranceapplicant(entranceApplicant)
            //println("entranceApplication >>"+ entranceApplication)
            def flag = true
            def tabmaster_list = EntranceStudentProfileTabMaster.findAllByIscompulsory(true)
            if(tabmaster_list.size() > 0) {
                for(tab in tabmaster_list) {
                    EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(tab, entranceApplicant)
                    if(!applicant_profile_personal_status) {
                        flag = false
                        break
                    }
                }
            }
            if(!flag) {
                flash.error = "Please complete Profile Mandatory Tabs first..."
                redirect(controller: "entranceApplicant", action: "eadashboard")
                return
            }
            [entranceApplication: entranceApplication]
        }
    }

    def entranceExamination(){
        if(session.loginid == null){
            flash.message = "Your are logged out..! Please login again. Thank you..!"
            redirect(controller: "entranceAdmissionLogin", action: "login")
            return
        }else {
            println("I am in Entrance Examination")
            EntranceApplicant entranceApplicant = EntranceApplicant.findById(session.loginid)
            def entranceApplication = EntranceApplication.findAllByEntranceapplicant(entranceApplicant)
            //println("entranceApplication >>"+ entranceApplication)
            def flag = true
            def tabmaster_list = EntranceStudentProfileTabMaster.findAllByIscompulsory(true)
            if(tabmaster_list.size() > 0) {
                for(tab in tabmaster_list) {
                    EntranceStudentProfileStatus applicant_profile_personal_status = EntranceStudentProfileStatus.findByEntrancestudentprofiletabmasterAndEntranceapplicant(tab, entranceApplicant)
                    if(!applicant_profile_personal_status) {
                        flag = false
                        break
                    }
                }
            }
            if(!flag) {
                flash.error = "Please complete Profile Mandatory Tabs first..."
                redirect(controller: "entranceApplicant", action: "eadashboard")
                return
            }
            [entranceApplication: entranceApplication]
        }
    }

    def applicationPreview(){
        if(session.loginid == null){
            flash.message = "Your are logged out..! Please login again. Thank you..!"
            redirect(controller: "entranceAdmissionLogin", action: "login")
            return
        }else {
            def array=[]
            println("I am in Application Preview"+ params)
            EntranceApplicant entranceApplicant = EntranceApplicant.findById(session.loginid)
            Organization org = Organization.findById(entranceApplicant.entrancecategory.organization?.id)
            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersion)
            def entranceApplication = EntranceApplication.findByEntranceapplicantAndEntranceversion(entranceApplicant,entranceVersion)
            // def EntranceApplicantAcademics = EntranceApplicantAcademics.findAllByEntranceapplicantAndOrganization(entranceApplicant,org)
            def EntranceApplicantAcademics = EntranceApplicantAcademics.findAllByEntranceapplicant(entranceApplicant)
            //def EntranceApplicantExperience = EntranceApplicantExperience.findAllByEntranceapplicantAndOrganization(entranceApplicant,org)
            def EntranceApplicantExperience = EntranceApplicantExperience.findAllByEntranceapplicant(entranceApplicant)
            def EntranceApplicantDocument = EntranceApplicantDocument.findAllByEntranceapplicantAndOrganization(entranceApplicant,org)
            AWSFolderPath awsFolderPath = AWSFolderPath.findById('5')
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSBucketService awsBucketService = new AWSBucketService()
            for(doc in EntranceApplicantDocument)
            {
                HashMap hm=new HashMap()
                def awsimagelink = doc?.filepath + doc?.filename
                String file = awsBucketService.getPresignedUrl(awsBucket.bucketname,awsimagelink, awsBucket.region)
                hm.put("url",file)
                hm.put("document",doc)
                array.add(hm)
            }
            String file = "https://vierp-test.s3.ap-south-1.amazonaws.com/img_avatar.png";
            if(entranceApplicant && entranceApplicant?.photopath && entranceApplicant.photoname) {
                def awsimagelink = entranceApplicant.photopath + entranceApplicant.photoname
                file = awsBucketService.getPresignedUrl(awsBucket.bucketname, awsimagelink, awsBucket.region)
            }
            println("entranceApplicantExperience :: " + EntranceApplicantAcademics)
            println("entranceApplicantAcademics :: " + EntranceApplicantExperience)
            println("entranceApplicantDocument :: " + array)

            [Photo_URL:file,EA:entranceApplicant,entranceApplication: entranceApplication,
             entranceApplicantAcademics:EntranceApplicantAcademics,entranceApplicantExperience:EntranceApplicantExperience,
             entranceApplicantDocument:array]
        }
    }

    //Pay fees
    def getallformpayfees() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("entranceadmissionspayfees :: " + params)
            EntranceApplicant login = EntranceApplicant.findById(session.loginid)
            EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findByName("In-Process")
            def applicationlist = EntranceApplication.findAllByIsfeespaidAndEntranceapplicantAndEntranceapplicationstatus(false, login, entranceApplicationStatus)
            println("applicationlist :: " + applicationlist)


            [params:params]
        }
    }

    def entranceadmissionspayfees() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "EntranceAdmissionLogin", action: "login")
            return
        } else {
            println("entranceadmissionspayfees :: " + params)
            EntranceApplication application = EntranceApplication.findById(params.applicationid)
            EntranceApplicant entranceApplicant = EntranceApplicant.findById(session.loginid)
            EntranceVersion entranceVersion = EntranceVersion.findById(params.versionid)
            if(!application) {
                flash.error =  "Please Apply Application first..."
                redirect(controller:"application", action: "entranceApplication")
                return
            }
            if(application.is_physically_handicapped || application.is_visually_handicapped) {
                if(!entranceVersion.is_fee_applicable_to_handicap_person) {
                    redirect(controller: "application", action: "trackEntranceApplication")
                    return
                }
            }

            ApplicationType at = ApplicationType.findByApplication_type("ERP")
            RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Accounts", application.organization )
            ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, application.organization)
            AcademicYear currentAcademicYear = aay.academicyear
            Semester currentSemester = aay.semester

            def total = 0
            def apply_branches = application.entrancebranch
//            println("apply_branches :: " + apply_branches)
//            println("application.organization :: " + application.organization)
//            println("application.entranceversion :: " + application.entranceversion)
            for(branch in apply_branches) {
                EntranceFees entranceFees = EntranceFees.findByEntrancecategorytypeAndOrganizationAndProgramtypeAndEntranceversion(entranceApplicant.entrancecategory.entrancecategorytype, application.organization, branch.program.programtype, application.entranceversion)
//                println("branch.program.programtype :: " + branch.program.programtype)
//                println("entranceApplicant.entrancecategory.entrancecategorytype :: " + entranceApplicant.entrancecategory.entrancecategorytype)
//                EntranceFees entranceFees = EntranceFees.findByOrganizationAndProgramtypeAndEntranceversion(application.organization, branch.program.programtype, application.entranceversion)
//                println("entranceFees :: " + entranceFees)
                if(entranceFees) {
                    total = total + entranceFees.fees
                } /*else {
                    flash.error = "Please fill the entrance fees master..."
                    redirect(controller: "application", action: "entranceApplication")
                    return
                }*/
            }

            TransactionRequestType transactionrequesttype = TransactionRequestType.findByIscurrent(true)
            def paymentgatewaymaster = ERPPaymentGatewayMaster.findByName('TECHPROCESS')
            TransactionOrganizationOnlineAccount transactionorganizationonlineaccount = TransactionOrganizationOnlineAccount.findByOrganizationAndErppaymentgatewaymasterAndIsactive(application.organization, paymentgatewaymaster, true)

            Date today = new Date()
            String merchanttxnrefno = "ent_admission_" + today.getTime().toString() + "_" + application.id

            TransactionCurrencyCode transactioncurrencycode = TransactionCurrencyCode.findByIscurrent(true)
            String shopping_cart_detail = "FIRST_" + total + "_0.0"
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy")
            String transaction_date = formatter.format(today)
            TransactionURLType transactionredirecturltype = TransactionURLType.findByType("redirect")
            TransactionURL transactionredirecturl = TransactionURL.findByTransactionurltype(transactionredirecturltype)
            TransactionURLType transactionreturnurltype = TransactionURLType.findByType("ent_admission_return")
            TransactionURL transactionreturnurl = TransactionURL.findByTransactionurltype(transactionreturnurltype)
            String enkey = transactionorganizationonlineaccount.merchant_key
            String eniv = transactionorganizationonlineaccount.merchant_iv

            CommonFeesTypeTransactions comonfeesonlinetransactions = CommonFeesTypeTransactions.findByTypeAndOrganization("Ent_Admission", application.organization)
            TransactionStatus transactionstatus = TransactionStatus.findByStatus("initialization")

            EntranceOnlineTransaction entranceOnlineTransaction = EntranceOnlineTransaction.findByAcademicyearAndOrganizationAndEntranceapplicationAndTransactionorganizationonlineaccountAndErp_transaction_id(currentAcademicYear, application.organization, application, transactionorganizationonlineaccount, merchanttxnrefno)
            if(!entranceOnlineTransaction) {
                entranceOnlineTransaction = new EntranceOnlineTransaction()
                entranceOnlineTransaction.erp_transaction_id = merchanttxnrefno
                entranceOnlineTransaction.transaction_message = ""
                entranceOnlineTransaction.transaction_error_message = ""
                entranceOnlineTransaction.request_details = shopping_cart_detail
                entranceOnlineTransaction.amount = total
                entranceOnlineTransaction.received_amount = 0
                entranceOnlineTransaction.bank_name = ""
                entranceOnlineTransaction.payment_remark = ""
                entranceOnlineTransaction.request_transaction_date = today
                entranceOnlineTransaction.response_transaction_date = ""
                entranceOnlineTransaction.paymentgateway_transaction_id = ""
                entranceOnlineTransaction.transaction_response_entire_url = ""
                entranceOnlineTransaction.bank_transaction_id = ""
                entranceOnlineTransaction.card_id = ""
                entranceOnlineTransaction.customer_id = ""
                entranceOnlineTransaction.customer_name = application?.entranceapplicant?.fullname
                entranceOnlineTransaction.mobile_number = application?.entranceapplicant?.mobilenumber
                entranceOnlineTransaction.account_number = ""

                entranceOnlineTransaction.entranceversion = application?.entranceversion
                entranceOnlineTransaction.organization = application?.organization
                entranceOnlineTransaction.entranceapplicant = application?.entranceapplicant
                entranceOnlineTransaction.entranceapplication = application
                entranceOnlineTransaction.academicyear = currentAcademicYear
                entranceOnlineTransaction.transactionrequesttype = transactionrequesttype
                entranceOnlineTransaction.transactioncurrencycode = transactioncurrencycode
                entranceOnlineTransaction.transactionorganizationonlineaccount = transactionorganizationonlineaccount
                entranceOnlineTransaction.transactionstatus = transactionstatus

                entranceOnlineTransaction.creation_username = entranceApplicant.email
                entranceOnlineTransaction.updation_username = entranceApplicant.email
                entranceOnlineTransaction.creation_date = new Date()
                entranceOnlineTransaction.updation_date = new Date()
                entranceOnlineTransaction.creation_ip_address = request.getRemoteAddr()
                entranceOnlineTransaction.updation_ip_address = request.getRemoteAddr()
                entranceOnlineTransaction.save(flush: true, failOnError: true)
                println("entranceOnlineTransaction :: " + entranceOnlineTransaction)
            }
            [application : application,
            total:total, transactionrequesttype:transactionrequesttype, transactionorganizationonlineaccount:transactionorganizationonlineaccount,
            merchanttxnrefno:merchanttxnrefno,transactioncurrencycode:transactioncurrencycode,shopping_cart_detail:shopping_cart_detail,
            transaction_date:transaction_date,transactionredirecturl:transactionredirecturl, transactionorganizationonlineaccount:transactionorganizationonlineaccount,
            transactionorganizationonlineaccount:transactionorganizationonlineaccount, transactionreturnurl:transactionreturnurl
            ]
        }
    }

    def entranceadmissionspayfeesresponce() {
        println("entranceadmissionspayfeesresponce :: " + params)


        TransactionRequestType transactionrequesttype = TransactionRequestType.findByIscurrent(true)
        def paymentgatewaymaster = ERPPaymentGatewayMaster.findByName('TECHPROCESS')
        TransactionOrganizationOnlineAccount transactionorganizationonlineaccount = TransactionOrganizationOnlineAccount.findByMerchant_codeAndErppaymentgatewaymasterAndIsactive(params.tpsl_mrct_cd, paymentgatewaymaster, true)
        println("transactionorganizationonlineaccount :: " + transactionorganizationonlineaccount)

        Organization org = transactionorganizationonlineaccount.organization
        println("org :: " + org)

        String key = transactionorganizationonlineaccount.merchant_key
        String iv = transactionorganizationonlineaccount.merchant_iv

        String entireresponse = params['msg']
        TransactionResponseBean beanObj = new TransactionResponseBean();
        beanObj.setResponsePayload(entireresponse)
        beanObj.setIv(iv.getBytes());
        beanObj.setKey(key.getBytes());
        String status = beanObj.getResponsePayload()
        List<String> strings = Arrays.asList(status.split("\\|"))   //enire url
        def responseMap = [:]
        if (strings.size() > 0) {
            for (s in strings) {
                def a = s.split("\\=")
                responseMap[a[0]] = a[1]
            }
        }
        println "responseMap:-" + responseMap

        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Accounts", org )
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, org)
        AcademicYear currentAcademicYear = aay.academicyear
        Semester currentSemester = aay.semester

        String erp_transaction_id = responseMap.get("clnt_txn_ref")
        println("erp_transaction_id :: " + erp_transaction_id)
        String status_code = responseMap.get("txn_status")
        println("status_code :: " + status_code)
        TransactionStatus transactionstatus = TransactionStatus.findByCode(status_code)
        println("transactionstatus :: " + transactionstatus)
        String txn_amt = responseMap.get("txn_amt")
        Double double_txn_amt = Double.parseDouble(txn_amt)
        println("double_txn_amt :: " + double_txn_amt)
        String transaction_message = responseMap.get("txn_msg")
        String transaction_error_message = responseMap.get("txn_err_msg")
        String response_transaction_date = responseMap.get("tpsl_txn_time")
        String paymentgateway_transaction_id = responseMap.get("tpsl_txn_id")
        String bank_transaction_id = responseMap.get("BankTransactionID")
        String card_id = responseMap.get("card_ID")
        String bank_name = responseMap.get("tpsl_bank_cd")


        TransactionCurrencyCode transactioncurrencycode = TransactionCurrencyCode.findByIscurrent(true)

        String[] erp_transaction_id_array = erp_transaction_id.split("_")
        println("erp_transaction_id_array :: " + erp_transaction_id_array[3])

        EntranceApplication entranceApplication = EntranceApplication.findById(erp_transaction_id_array[3])
        println("entranceApplication :: " + entranceApplication)

        EntranceVersion entranceVersion = entranceApplication.entranceversion

        NumberToWordService numberToWordService = new NumberToWordService()

        println("currentAcademicYear :: " + currentAcademicYear)
        println("org :: " + org)
        println("erp_transaction_id :: " + erp_transaction_id)
        println("transactionorganizationonlineaccount :: " + transactionorganizationonlineaccount)

        EntranceOnlineTransaction entranceOnlineTransaction = EntranceOnlineTransaction.findByAcademicyearAndOrganizationAndErp_transaction_idAndTransactionorganizationonlineaccount(currentAcademicYear, org, erp_transaction_id, transactionorganizationonlineaccount)
        println("entranceOnlineTransaction :: " + entranceOnlineTransaction)
        if(entranceOnlineTransaction) {

            Double previousamt = entranceOnlineTransaction.amount

            if (previousamt > double_txn_amt) {
                flash.error = "Transaction amount and paid amount Mis Matched..."
            } else {
                if (responseMap.get("txn_msg") == "success") {
                    int receipttrack = entranceVersion.receipttrack
                    String tno
                    if(receipttrack<10) {
                        tno="0000"+receipttrack
                    } else if(receipttrack<100) {
                        tno="000"+receipttrack
                    } else if(receipttrack<1000) {
                        tno="00"+receipttrack
                    } else if(receipttrack<10000) {
                        tno="0"+receipttrack
                    } else {
                        tno=receipttrack
                    }
                    Date todate = new Date()
                    String date = "" + todate.getDate() + todate.getMonth() + todate.getYear()
                    println("date :: " + date)

                    def receipt_no = entranceVersion.organization.organization_number + "ENT-" + date + "-" + tno

                    EntranceApplicationReceipt receipt = new EntranceApplicationReceipt()
                    receipt.feesreceiptid = receipt_no
                    receipt.amount = double_txn_amt
                    receipt.receiptdate = new Date()

                    receipt.organization = org
                    receipt.entranceapplicant = entranceOnlineTransaction.entranceapplicant
                    receipt.entranceversion = entranceOnlineTransaction.entranceversion
                    receipt.entranceonlinetransaction = entranceOnlineTransaction

                    receipt.creation_username = entranceOnlineTransaction.entranceapplicant.email
                    receipt.updation_username = entranceOnlineTransaction.entranceapplicant.email
                    receipt.creation_date = new Date()
                    receipt.updation_date = new Date()
                    receipt.creation_ip_address = request.getRemoteAddr()
                    receipt.updation_ip_address = request.getRemoteAddr()
                    receipt.save(failOnError: true, flush: true)

                    receipt.addToEntranceapplication(entranceOnlineTransaction.entranceapplication)
                    receipt.save(failOnError: true, flush: true)

                    entranceVersion.receipttrack = receipttrack + 1
                    entranceVersion.save(failOnError: true, flush: true)
                } else {
                    flash.error = "Transaction Failed ...! Please try again..."
                }
            }

            entranceOnlineTransaction.erp_transaction_id = erp_transaction_id
            entranceOnlineTransaction.transaction_message = transaction_message
            entranceOnlineTransaction.transaction_error_message = transaction_error_message
            entranceOnlineTransaction.amount = previousamt
            entranceOnlineTransaction.received_amount = double_txn_amt
            entranceOnlineTransaction.bank_name = bank_name
            entranceOnlineTransaction.payment_remark = "Online Payment By Tech Process"
            entranceOnlineTransaction.response_transaction_date = response_transaction_date
            entranceOnlineTransaction.paymentgateway_transaction_id = paymentgateway_transaction_id
            entranceOnlineTransaction.transaction_response_entire_url = strings
            entranceOnlineTransaction.bank_transaction_id = bank_transaction_id
            entranceOnlineTransaction.card_id = card_id

            entranceOnlineTransaction.transactioncurrencycode = transactioncurrencycode
            entranceOnlineTransaction.transactionstatus = transactionstatus

            entranceOnlineTransaction.updation_username = entranceApplication.entranceapplicant.email
            entranceOnlineTransaction.updation_date = new Date()
            entranceOnlineTransaction.updation_ip_address = request.getRemoteAddr()
            entranceOnlineTransaction.save(flush: true, failOnError: true)
            println("entranceOnlineTransaction :: " + entranceOnlineTransaction)

        } else {
            flash.error = "Application Online Transaction Details Not found..."
        }

//        responseMap:-[txn_status:0392, txn_msg:Aborted, txn_err_msg:Cancelled_BY_User, clnt_txn_ref:ent_admission_1623172926454_1, tpsl_bank_cd:NA, tpsl_txn_id:
//                E15975237, txn_amt:2.00, clnt_rqst_meta:NA, tpsl_txn_time:08-06-2021 22:52:21, tpsl_rfnd_id:NA, bal_amt:NA, rqst_token:343fe74b-16a7-446d-bf69-55b57a662
//                      7e9, hash:08ebffb0f0dffe0bc2a9601f69e8ec81b4bf7597]

//        render "OK"
        redirect(controller: "application", action:"trackEntranceApplication" )
    }

    def getreceipt() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "EntranceAdmissionLogin", action: "login")
            return
        } else {
            println("entranceadmissionspayfees :: " + params)
            EntranceApplicant login = EntranceApplicant.findById(session.loginid)
            EntranceApplicationReceipt receipt = EntranceApplicationReceipt.findByFeesreceiptidAndEntranceapplicant(params.receiptno.trim(), login)
            if(receipt) {

                [receipt:receipt]
            } else {
                flash.error = "Receipt Not found..."
                redirect(controller: "application", action:"trackEntranceApplication" )
            }
        }
    }
}
