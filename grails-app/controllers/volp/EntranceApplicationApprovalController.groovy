package volp

class EntranceApplicationApprovalController {

    def index() { }

    def addApplicationApproval(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addApplicationApproval : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceBranch_list = EntranceBranch.findAllByOrganization(org)
            def entranceVersion_list = EntranceVersion.findAllByOrganization(org)

            [entranceBranch_list:entranceBranch_list,entranceVersion_list:entranceVersion_list]
        }
    }
    def getApplicationToapproval(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getApplicationToapproval : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceApplicationStatus_list = EntranceApplicationStatus.findAllByOrganization(org)
            def exemption_list = Exemption.findAllByOrganization(org)
            //EntranceBranch entranceBranch = EntranceBranch.findByName(params.branch)
            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersionId)
            def entranceBranch = EntranceBranch.findByName(params.branch)
            println("entranceBranch : "+entranceBranch)
            def  entranceBranch1 = EntranceBranch.findAllByEntranceversion(entranceVersion)
            println("entranceBranch1: "+entranceBranch1)
            def entranceApplicationStatus = EntranceApplicationStatus.findByName('In-Process')
            def entranceApplication_list = EntranceApplication.findAllByEntranceversionAndEntranceapplicationstatus(entranceVersion,entranceApplicationStatus)
            //def entranceApplication_list = EntranceApplication.findAllByEntranceversionAndEntrancebranch(entranceVersion,entranceBranch)
            println("entranceApplication_list : "+entranceApplication_list)
            def entranceRejectionReason_list = EntranceRejectionReason.findAllByOrganization(org)


            [entranceVersion:entranceVersion,entranceApplication_list:entranceApplication_list,entranceApplicationStatus_list:entranceApplicationStatus_list,
             exemption_list:exemption_list,branch:params.branch,entranceRejectionReason_list:entranceRejectionReason_list]
        }
    }
    def saveApplicationToapproval(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            println("In saveApplicationToapproval : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceApplication entranceApplication = EntranceApplication.findById(params.entranceApplicationId)
            println("entranceApplication : "+entranceApplication)
            EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findByName(params.applicationStatus)
            Exemption exemption = Exemption.findByName(params.exemption)
            EntranceRejectionReason entranceRejectionReason = EntranceRejectionReason.findByName(params.rejectionReason)
            if(entranceApplication)
            {
                entranceApplication.entranceapplicationstatus = entranceApplicationStatus
                entranceApplication.entranceapplicant.exemption = exemption
                entranceApplication.entrancerejectionreason = entranceRejectionReason
                entranceApplication.actionby = instructor
                entranceApplication.updation_username = instructor.uid
                entranceApplication.updation_ip_address = request.getRemoteAddr()
                entranceApplication.updation_date = new java.util.Date()
                entranceApplication.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'getApplicationToapproval')
            }else {
                flash.error = "Already Exists..."
                redirect(action:'getApplicationToapproval')
            }
        }

    }
    def viewApplicationInApproval(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        }else {
            def array=[]
            println("In viewApplicationInApproval"+ params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceVersion entranceVersion = EntranceVersion.findById(params.entranceVersion)
            EntranceApplicant entranceApplicant= EntranceApplicant.findById(params.applicantId)
            EntranceApplication entranceApplication = EntranceApplication.findByEntranceapplicantAndEntranceversion(entranceApplicant,entranceVersion)
            def entranceApplicantAcademics = EntranceApplicantAcademics.findAllByEntranceapplicantAndOrganization(entranceApplicant,org)
            def entranceApplicantExperience = EntranceApplicantExperience.findAllByEntranceapplicantAndOrganization(entranceApplicant,org)
            def entranceApplicantDocument = EntranceApplicantDocument.findAllByEntranceapplicantAndOrganization(entranceApplicant,org)
            AWSFolderPath awsFolderPath = AWSFolderPath.findById('5')
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSBucketService awsBucketService = new AWSBucketService()
            for(doc in entranceApplicantDocument)
            {
                HashMap hm=new HashMap()
                def awsimagelink = doc?.filepath + doc?.filename
                String file = awsBucketService.getPresignedUrl(awsBucket.bucketname,awsimagelink, awsBucket.region)
                hm.put("url",file)
                hm.put("document",doc)
                array.add(hm)
            }
            String file = "https://vierp-test.s3.ap-south-1.amazonaws.com/img_avatar.png";
            if(entranceApplicant && entranceApplicant?.photopath && entranceApplicant.photoname) {
                def awsimagelink = entranceApplicant.photopath + entranceApplicant.photoname
                file = awsBucketService.getPresignedUrl(awsBucket.bucketname, awsimagelink, awsBucket.region)
            }
            [Photo_URL:file,EA:entranceApplicant,entranceApplication: entranceApplication,entranceApplicantAcademics:entranceApplicantAcademics,
             entranceApplicantExperience:entranceApplicantExperience,entranceApplicantDocument:array]
        }
    }
    def getApplicationCount(){
        println("in getApplicationCount "+params)
        EntranceVersion entranceVersion = EntranceVersion.findById(params.versionId)
        def entranceApplication_list = EntranceApplication.findAllByEntranceversion(entranceVersion)
        def entranceApplication_list_count= entranceApplication_list.size()
        render entranceApplication_list_count
        return
    }
    def getShortListedApplicationCount(){
        println("in getApplicationCount "+params)
        EntranceVersion entranceVersion = EntranceVersion.findById(params.versionId)
        EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findByName('Accepted')
        def entranceApplication_list = EntranceApplication.findAllByEntranceversionAndEntranceapplicationstatus(entranceVersion,entranceApplicationStatus)
        def entranceApplication_list_count= entranceApplication_list.size()
        render entranceApplication_list_count
        return
    }


    //Exemption Approval
    def addExemptionApproval(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addExemptionApproval : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceApplication_list = EntranceApplication.findAllByOrganization(org)
            def exemption_list = Exemption.findAllByOrganization(org)

            [entranceApplication_list:entranceApplication_list,exemption_list:exemption_list]
        }
    }
    def saveExemptionApproval(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveExemptionApproval : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
            Exemption exemption = Exemption.findByName(params.exemption)
            if(entranceApplication){
                entranceApplication.exemption = exemption
                if(params.exemption == 'Not-exempted'){
                    entranceApplication.is_exemption_from_test = true
                }else{
                    entranceApplication.is_exemption_from_test = false
                }
                entranceApplication.actionby = instructor
                entranceApplication.updation_username = instructor.uid
                entranceApplication.updation_date = new java.util.Date()
                entranceApplication.updation_ip_address = request.getRemoteAddr()
                entranceApplication.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'addExemptionApproval')
            }else {
                flash.error = "Does not Exists..."
                redirect(action:'addExemptionApproval')
            }
        }
    }
    def getExemptionTypedocumenturl() {
        println("getExemptionTypedocumenturl : " + params)

        EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
        ExemptionType exemptionType = ExemptionType.findById(params.exemptionTypeId)
        ExemptionDocument exemptionDocument = ExemptionDocument.findByEntranceapplicantAndExemptiontype(entranceApplication?.entranceapplicant,exemptionType)

        String path = exemptionDocument.file_path + exemptionDocument.file_name
        println("path :: " + path)
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSBucketService awsBucketService1 = new AWSBucketService()
        String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)


        render "<a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue' aria-hidden='true'></i></a>"
    }

    //Is Handicapped?
    def showIsHandicapped(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("addExemptionApproval : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            def entranceApplicationStatus_list = EntranceApplicationStatus.findAllByOrganization(org)
            def entranceRejectionReason_list = EntranceRejectionReason.findAllByOrganization(org)
            def entranceApplication_list = EntranceApplication.findAllByOrganization(org)
            def handicapped_list=[]
            for(items in entranceApplication_list){
                if(items?.is_physically_handicapped == true || items?.is_visually_handicapped == true){
                    handicapped_list.add(items)
                }
            }
            println("handicapped_list : "+handicapped_list)

            [handicapped_list:handicapped_list,entranceApplicationStatus_list:entranceApplicationStatus_list,entranceRejectionReason_list:entranceRejectionReason_list]
        }
    }
    def saveIsHandicappedStatus(){
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveIsHandicappedStatus : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            EntranceApplicationStatus entranceApplicationStatus = EntranceApplicationStatus.findByName(params.applicationStatus)
            EntranceRejectionReason entranceRejectionReason = EntranceRejectionReason.findByName(params.reason)
            EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
            if(entranceApplication){
                entranceApplication.entrancerejectionreason = entranceRejectionReason
                entranceApplication.entranceapplicationstatus = entranceApplicationStatus
                entranceApplication.actionby = instructor
                entranceApplication.remark = params.remark
                entranceApplication.updation_username = instructor.uid
                entranceApplication.updation_date = new java.util.Date()
                entranceApplication.updation_ip_address = request.getRemoteAddr()
                entranceApplication.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
                redirect(action: 'showIsHandicapped')
            }else{
                flash.message = "Does not Exists..."
                redirect(action: 'showIsHandicapped')
            }
        }
    }
    def getPhysicallyHandicappeddocumenturl() {
        println("getPhysicallyHandicappeddocumenturl : " + params)

        EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
        String url = ""
        if(entranceApplication.is_physically_handicapped_file_path && entranceApplication.is_physically_handicapped_file_name) {
            String path = entranceApplication.is_physically_handicapped_file_path + entranceApplication.is_physically_handicapped_file_name
            println("path :: " + path)
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSBucketService awsBucketService1 = new AWSBucketService()
            url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)
        }
        render "<a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue' aria-hidden='true'></i></a>"
    }
    def getVisuallyHandicappeddocumenturl(){
        println("getVisuallyHandicappeddocumenturl : " + params)

        EntranceApplication entranceApplication = EntranceApplication.findById(params.applicationId)
        String path = entranceApplication.is_visually_handicapped_file_path + entranceApplication.is_visually_handicapped_file_name
        println("path :: " + path)
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        AWSBucketService awsBucketService1 = new AWSBucketService()
        String url = awsBucketService1.getPresignedUrl(awsBucket.bucketname, path, awsBucket.region)

        render "<a href='" + url + "'><i class='fa fa-download fa-2x' style='color:blue' aria-hidden='true'></i></a>"
    }

}
