package volp

class InvBudgetController {

    def index() { }

    //Add Budget
    def addNewInvBudget(){
        println(" i am addNewInvBudget")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at, "Registration", instructor.organization)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true, instructor.organization)

        def invBudgetTypeList = InvBudgetType.findAllWhere(organization: org,isactive:true)
        def invBudgetLevelList = InvBudgetLevel.findAllWhere(organization: org,isactive: true)
        def academicYearList = AcademicYear.findAllByIsactive(true,[sort: "ay", order: "desc"])
        //def departmentList = Department.findAllWhere(organization: org)
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive:true)
        def invBudgetList = InvBudget.findAllWhere(organization: org)
        ArrayList Budgetarray=new ArrayList()
        /*
        def approvalCategory = InvFinanceApprovingCategory.findByName("Budget")
        def approvalLevelList = InvFinanceApprovingAuthorityLevel.findAllByIsactiveAndOrganizationAndInvfinanceapprovingcategory(true,org,approvalCategory)
        */
        def approvalLevelList=[]
        AWSBucketService awsBucketService = new AWSBucketService()
        AWSBucket aws = AWSBucket.findByContent("documents")
        for(i in invBudgetList)
        {
            HashMap hm=new HashMap()
            def awsimagelink = i.file_path
            String file = awsBucketService.getPresignedUrl(aws.bucketname,awsimagelink, aws.region)
            hm.put("url",file)
            hm.put("data",i)

            def budgetEscalations = InvBudgetEscalation.findAllByInvbudget(i)
            hm.put("escalation",budgetEscalations)
            Budgetarray.add(hm)
        }
        //println"Budgetarray"+Budgetarray
        [invBudgetList:Budgetarray,invBudgetTypeList:invBudgetTypeList, aay:aay,invMaterialList:invMaterialList,
         invBudgetLevelList:invBudgetLevelList,academicYearList:academicYearList,approvalLevelList:approvalLevelList]
    }
    def fetchDepts(){
        println"I am in fetch dept by budgettype"+params
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def BL = InvBudgetLevel.findById(params.budgetLevel)
        def departmentList= []
        if(BL?.name == "Department"){
            departmentList = Department.findAllWhere(organization: org)
        }
        [departmentList:departmentList,size:departmentList.size()]
    }
    def saveInvBudget(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.academicYearId == 'null' || params.budgetLevelId == 'null' || params.budgetTypeId == 'null' || params.newFile == 'null')
            {
                flash.error = "Kindly fill all required Details"
            }
            else{
                println(" i am saveInvBudget controler")
                InvBudgetService invBudgetService = new InvBudgetService()
                invBudgetService.saveInvBudget(params,session,request,flash)
            }

        }
        redirect(action: 'addNewInvBudget')
    }
    def editInvBudget(){
        println" i am editInvBudget"
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.invBudgetId == 'null' || params.departmentId == 'null' || params.academicYearId == 'null' || params.budgetLevelId == 'null' || params.budgetTypeId == 'null')
            {
                flash.error = "Kindly fill all required Details"
            }
            else {
                InvBudgetService invBudgetService = new InvBudgetService()
                invBudgetService.editInvBudget(params, session, request, flash)
            }
        }
        redirect(action: 'addNewInvBudget')
    }
    def deleteInvBudget(){
        println(" i am deleteInvBudget")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            InvBudgetService invBudgetService = new InvBudgetService()
            invBudgetService.deleteInvBudget(params,flash)
            redirect(action: 'addNewInvBudget')
        }
    }
    def activateInvBudget(){
        println(" i am activateInvBudget")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            InvBudgetService invBudgetService = new InvBudgetService()
            invBudgetService.activateInvBudget(params,session,request,flash)
            redirect(action: 'addNewInvBudget')
        }
    }

    def addNewBudgetDetails(){
        println(" i am addNewBudgetDetails")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invBudgetDetailsList = InvBudgetDetails.findAllWhere(organization: org)
        def invBudgetList = InvBudget.findAllWhere(organization: org,isactive:true)
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive:true)
        [invBudgetDetailsList:invBudgetDetailsList,invBudgetList:invBudgetList,invMaterialList:invMaterialList]
    }
    def getMaterialPartsByMaterial(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        println" i am getMaterialPartsByMaterial"+params
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        InvMaterial invMaterial = InvMaterial.findById(params.materialId)
        def materialPartList = InvMaterialPart.findAllByInvmaterialAndIsactiveAndOrganization(invMaterial,true,org)
        [materialPartList:materialPartList]
    }
    def saveBudgetDetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            if(params.budgetId == 'null' || params.materialId == 'null')
            {
                flash.message = "Kindly fill all required Details"
            }
            else{
                println(" i am saveBudgetDetails" + params)
                InvBudgetService invBudgetService = new InvBudgetService()
                invBudgetService.saveBudgetDetails(params,session,request,flash)
            }
            redirect(action: 'addNewInvBudget')
        }
    }
    def editBudgetDetails(){
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else {
            println"In editbudgetdetails controller" + params
            if(params.budgetId == 'null' || params.materialId == 'null')
            {
                flash.error = "Kindly fill all required Details"
            }
            else{
                println(" i am editBudgetDetails")
                InvBudgetService invBudgetService = new InvBudgetService()
                invBudgetService.editBudgetDetails(params,session,request,flash)
            }
            redirect(action: 'viewSingleBudgetDetails', params:[invBudgetId: params.invBudgetId])
        }
    }
    def deleteBudgetDetails(){
        println(" i am deleteInvBudget")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            InvBudgetService invBudgetService = new InvBudgetService()
            invBudgetService.deleteBudgetDetails(params,flash)
            redirect(action: 'addNewBudgetDetails')
        }
    }
    def activateBugdetDetails(){
        println(" i am activateBugdetDetails" + params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            InvBudgetService invBudgetService = new InvBudgetService()
            invBudgetService.activateBugdetDetails(params,session,request,flash)
            redirect(action: 'addNewBudgetDetails')
        }
    }

    //Budget Reports
    def viewBudget(){
        println(" i am viewBudget Accounts Dept")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invApprovalStatus = InvApprovalStatus.findByNameAndOrganization("Approved",org)
        //def invBudgetList = InvBudget.findAllWhere(organization: org,invapprovalstatus: invApprovalStatus)
        def invBudgetList = InvBudget.findAllWhere(organization: org,isapprove: true)
        /*def invBudgetTypeList = InvBudgetType.findAllWhere(organization: org)
        def invBudgetLevelList = InvBudgetLevel.findAllWhere(organization: org)
        def academicYearList = AcademicYear.list()
        def departmentList = Department.findAllWhere(organization: org)
        def invStatusList = InvApprovalStatus.findAllWhere(organization: org)*/
        //println"invStatusList"+invStatusList
        [invBudgetList:invBudgetList]
        /*
        invBudgetTypeList:invBudgetTypeList,invStatusList:invStatusList,
         invBudgetLevelList:invBudgetLevelList,academicYearList:academicYearList,departmentList:departmentList]
         */

    }
    def viewBudgetDetails(){
        println(" i am viewBudgetDetails" + params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            redirect(action: 'viewSingleBudgetDetails',params:params)
        }
    }
    def viewSingleBudgetDetails(params){
        println(" i am viewSingleBudgetDetails"+params)
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invBudget = InvBudget.findById(params.invBudgetId)
        def isapprove = invBudget?.isapprove
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invBudgetDetailsList = InvBudgetDetails.findAllByInvbudget(invBudget)//dont add is-active
        def invMaterialList = InvMaterial.findAllWhere(organization: org,isactive:true)
        [isapprove:isapprove,invBudgetDetailsList:invBudgetDetailsList,invMaterialList:invMaterialList,invBudgetId:params.invBudgetId]
    }

    //Not USED
    //By Required Authority--- Employee/HOD/BRACT
    def approvBudget(){
        println(" i am approvBudget")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def login_int_dept = instructor?.department
        def invBudgetEscalationList = new ArrayList()
        def invInstructoryFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor,true,org)
            for(auths in invInstructoryFinanceApprovingAuthority)
             {
                     if(auths?.instructor?.department == login_int_dept)
                     {
                        def invFinanceApprovingAuthority = auths?.invfinanceapprovingauthority
                        def invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findAllWhere(invfinanceapprovingauthority:invFinanceApprovingAuthority,isactive:true,organization:org)
                        for(levels in invFinanceApprovingAuthorityLevel){
                            def invBudgetEscalation = InvBudgetEscalation.findByInvfinanceapprovingauthoritylevelAndIsactiveAndOrganization(levels,true,org)
                            if(invBudgetEscalation != null){
                                invBudgetEscalationList.push(invBudgetEscalation)
                            }
                     }
             }
        }
        def invApprovalStatusList = InvApprovalStatus.findAllWhere(organization:org)
        [invBudgetEscalationList:invBudgetEscalationList,invApprovalStatusList:invApprovalStatusList,instructor:instructor]

    }
    def updateBudgetStatus(){
        println(" i am updateBudgetStatus controller")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            InvBudgetService invBudgetService = new InvBudgetService()
            def par = params
            invBudgetService.updateBudgetStatus(par,session,flash)
            redirect(action: 'approvBudget')
        }
    }
    def viewApprovalHistory(){
        println(" i am viewApprovalHistory")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            redirect(action: 'viewApprovalHistoryDetails',params:params)
        }
    }
    def viewApprovalHistoryDetails(params){
        println(" i am viewApprovalHistoryDetails")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invBudget = InvBudget.findById(params.invBudgetId)
        def invBudgetEscalationList= InvBudgetEscalation.findAllByInvbudget(invBudget)
        [invBudgetEscalationList:invBudgetEscalationList]

    }

    //Not in use
    //By Required Authority--- Employee/HOD/BRACT
    /*def approveQuotation(){
        println(" i am approveQuotation")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def login_int_dept = instructor?.department
        def PRList = InvPurchaseRequisition.findAllWhere(organization: org,isactive: true,department:login_int_dept)
        ArrayList quotationList = new ArrayDeque()
        for(pr in PRList){
            def quotation = InvQuotation.findByInvpurchaserequisitionAndIsactive(pr,true)
            quotationList.push(quotation)
        }
        def invApprovalStatusList = InvApprovalStatus.findAllWhere(organization:org)
        [quotationList:quotationList,invApprovalStatusList:invApprovalStatusList,instructor:instructor]

    }
    def updateQuotationStatus(){
        println(" i am updateQuotationStatus in budget controller")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            Login login = Login.findById(session.loginid)
            if(params.invQuotationId == 'null'){
                flash.error  = "Please Re-try"
            }else{
                def invQuotation = InvQuotation.findById(params.invQuotationId)
                if(invQuotation.isapproved) {
                    invQuotation.isapproved = false
                    invQuotation.updation_username = login.username
                    invQuotation.updation_date = new Date()
                    invQuotation.updation_ip_address = request.getRemoteAddr()
                    invQuotation.save(flush: true, failOnError: true)
                    flash.message = "Successful.."
                } else {
                    invQuotation.isapproved = true
                    invQuotation.updation_username = login.username
                    invQuotation.updation_date = new Date()
                    invQuotation.updation_ip_address = request.getRemoteAddr()
                    invQuotation.save(flush: true, failOnError: true)
                    flash.message = "Successful.."
                }
            }
        }
        redirect(action: "approveQuotation")
    }
    def viewQuotationDetails(){
        println(" i am viewQuotationDetails")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        else{
            redirect(action: 'viewSingleQuotationDetails',params:params)
        }
    }
    def viewSingleQuotationDetails(params){
        println(" i am viewSingleBudgetDetails")
        if (session.user == null)
            redirect(controller:"login", action: 'erplogin')
        def invQuotation = InvQuotation.findById(params.invQuotationId)
        def invQuotationDetailsList = InvQuotationDetails.findAllByInvquotationAndIsactive(invQuotation,true)
        [invQuotationDetailsList:invQuotationDetailsList]
    }*/

    //Approve BUDGET
    def getInstpost() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getInstpost : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findAllByInstructorAndIsactiveAndOrganization(instructor, true, org)
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Budget", true, org)
            def invBudgetList = InvBudget.findAllWhere(organization: org,isactive:true)
            [invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority, instructor:instructor, invFinanceApprovingCategory:invFinanceApprovingCategory,invBudgetList:invBudgetList]
        }
    }
    def getallbudgetdetails() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("getallbudgetdetails : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Budget", true, org)
            InvInstructorFinanceApprovingAuthority invInstructorFinanceApprovingAuthority = InvInstructorFinanceApprovingAuthority.findById(params.post)

            InvFinanceApprovingAuthorityLevel invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findByInvfinanceapprovingcategoryAndInvfinanceapprovingauthorityAndOrganizationAndIsactive(invFinanceApprovingCategory, invInstructorFinanceApprovingAuthority.invfinanceapprovingauthority, org, true)
            if(!InvFinanceApprovingAuthorityLevel) {
                render "<div clas='alert alert-danger'>Please fill master of InvFinanceApprovingAuthorityLevel for category : " + invFinanceApprovingCategory.name + "</div>"
                return
            }
            def invBudget = InvBudget.findById(params.budgetId)
            def invBudgetEscalationList = InvBudgetEscalation.findAllByOrganizationAndInvfinanceapprovingauthoritylevelAndIsactiveAndInvbudget(org, invFinanceApprovingAuthorityLevel, true,invBudget)
            if(invBudgetEscalationList.size() == 0) {
                render "<div class='alert alert-danger'>Budget not found for approval...</div>"
                return
            }
            //println("invBudgetEscalationList :: " + invBudgetEscalationList)
            def budgetEscalationArray= []
            AWSBucketService awsBucketService = new AWSBucketService()
            AWSBucket aws = AWSBucket.findByContent("documents")
            for(i in invBudgetEscalationList)
            {
                HashMap hm=new HashMap()
                def awsimagelink = i.invbudget?.file_path
                String file = awsBucketService.getPresignedUrl(aws.bucketname,awsimagelink, aws.region)
                hm.put("url",file)
                hm.put("data",i)
                budgetEscalationArray.add(hm)
            }
            //println"budgetEscalationArray--->"+budgetEscalationArray
            [invBudgetEscalationList : budgetEscalationArray,
             invFinanceApprovingCategory : invFinanceApprovingCategory,
             invFinanceApprovingAuthorityLevel : invFinanceApprovingAuthorityLevel,
             invInstructorFinanceApprovingAuthority : invInstructorFinanceApprovingAuthority]
        }
    }
    def getBudgetEscalationforApprove() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("In getBudgetEscalationforApprove : ")
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization
            def invapprovalstatus_list = InvApprovalStatus.findAllByOrganizationAndIsactive(org, true)
            def next_level_approved = false

            InvBudgetEscalation invBudgetEscalation = InvBudgetEscalation.findById(params.bde_id)
            def budgetEscalations = InvBudgetEscalation.findAllByInvbudget(invBudgetEscalation?.invbudget)
            for(BE in budgetEscalations){
                    if(BE?.invfinanceapprovingauthoritylevel?.islast == true) {
                        if (BE?.invapprovalstatus?.name == 'Approved') {
                            next_level_approved = true
                            if(BE == invBudgetEscalation){
                                next_level_approved = false
                            }
                        }
                    }
            }
            println("next_level_approved :: " + next_level_approved)
            if(invBudgetEscalation) {
                def list = InvBudgetEscalation.findAllByOrganizationAndInvbudgetAndIsactive(org,invBudgetEscalation?.invbudget,true)
                 [list:list, invBudgetEscalation : invBudgetEscalation, invapprovalstatus_list:invapprovalstatus_list, next_level_approved:next_level_approved]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }
    def saveApproveBudget() {
        if (session.loginid == null) {
            flash.message = "Your are logged out! Please login again. Thank you."
            redirect(controller: "login", action: "erplogin")
            return
        } else {
            println("saveApproveBudget : " + params)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            Organization org = instructor.organization

            InvApprovalStatus invApprovalStatus =  InvApprovalStatus.findById(params.approvingstatus)
            InvBudgetEscalation invBudgetEscalation = InvBudgetEscalation.findById(params.bde_id)
            println"-->invBudgetEscalation"+invBudgetEscalation
            if(invBudgetEscalation) {
                invBudgetEscalation.action_date = new Date()
                invBudgetEscalation.remark = params.remark
                invBudgetEscalation.isactive = true
                invBudgetEscalation.actionby = instructor
                invBudgetEscalation.updation_username = login.username
                invBudgetEscalation.updation_date = new Date()
                invBudgetEscalation.updation_ip_address = request.getRemoteAddr()
                invBudgetEscalation.invapprovalstatus = invApprovalStatus
                invBudgetEscalation.save(flush: true, failOnError: true)

                if(invBudgetEscalation?.invfinanceapprovingauthoritylevel?.islast) {
                     InvBudget invBudget = invBudgetEscalation.invbudget
                    invBudget.invapprovalstatus = invApprovalStatus
                    if(invApprovalStatus?.name == "Rejected")
                    {
                        invBudget.isapprove = false
                    }
                    else if(invApprovalStatus?.name == "Approved")
                    {
                        invBudget.isapprove = true
                    }
                    else if(invApprovalStatus?.name == "In-Process")
                    {
                        invBudget.isapprove = false
                    }
                    invBudget.invapprovalstatus = invApprovalStatus
                    invBudget.updation_username = login.username
                    invBudget.updation_date = new Date()
                    invBudget.updation_ip_address = request.getRemoteAddr()
                    invBudget.save(flush: true, failOnError: true)
                }

                def list = InvBudgetEscalation.findAllByOrganizationAndInvbudgetAndIsactive(org, invBudgetEscalation?.invbudget, true).sort{it?.invfinanceapprovingauthoritylevel?.level_no}
                [list:list, invBudgetEscalation : invBudgetEscalation]
            } else {
                render "<div class='alert alert-danger'>Not Found....</div>"
                return
            }
        }
    }

}
