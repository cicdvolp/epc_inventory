package volp

import grails.gorm.transactions.Transactional
import grails.gorm.services.Service


import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.Multipart;
import javax.mail.BodyPart;
@Transactional
class SendMailService
{
   // boolean transactional = false
    def serviceMethod() {
    }
    // def SendMailService sendMailService
    //        sendMailService.sendmail("volp.vu@gmail.com","volp@108","deepak.pawar@vit.edu","This is final tesing","hi hardworking guy","")
    def sendmail(String frommail,String frommailpassword,String sendto,String subject,String bodymessage,String attachmentfile)
    {
        //d:/trust_office_ip.png
        final String username = frommail
        final String password = frommailpassword;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(frommail));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(sendto));
            message.setSubject(subject);
            if(attachmentfile.equals(""))
            {
                message.setText(bodymessage);
            }
            else
            {
                // Create the message part
                BodyPart messageBodyPart = new MimeBodyPart();

                // Now set the actual message
                messageBodyPart.setText(bodymessage);

                // Create a multipar message
                Multipart multipart = new MimeMultipart();

                // Set text message part
                multipart.addBodyPart(messageBodyPart);

                // Part two is attachment
                messageBodyPart = new MimeBodyPart();
                String filename = attachmentfile
                DataSource source = new FileDataSource(filename);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);
                multipart.addBodyPart(messageBodyPart);
                // Send the complete message parts
                message.setContent(multipart);
            }
            Transport.send(message);
            System.out.println("Mail Sent Successfully.....");

        } catch (MessagingException e) {
           // throw new RuntimeException(e);
            System.out.println("Error::Mail NOT Sent....");
        }
    }


    def sendmailwithcss(String frommail,String frommailpassword,String sendto,String subject,String bodymessage,String attachmentfile)
    {
        //d:/trust_office_ip.png
        final String username = frommail
        final String password = frommailpassword;

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        try {

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(frommail));
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(sendto));
            message.setSubject(subject);
            if(attachmentfile.equals(""))
            {
                message.setContent(bodymessage, "text/html");
            }
            else
            {
                // Create the message part
                BodyPart messageBodyPart = new MimeBodyPart();

                // Now set the actual message
                messageBodyPart.setContent(bodymessage, "text/html");

                // Create a multipar message
                Multipart multipart = new MimeMultipart();

                // Set text message part
                multipart.addBodyPart(messageBodyPart);

                // Part two is attachment
                messageBodyPart = new MimeBodyPart();
                String filename = attachmentfile
                DataSource source = new FileDataSource(filename);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(filename);
                multipart.addBodyPart(messageBodyPart);
                // Send the complete message parts
                message.setContent(multipart);
            }
            Transport.send(message);
            System.out.println("Mail Sent Successfully.....");

        } catch (MessagingException e) {
            // throw new RuntimeException(e);
            System.out.println("Error::Mail NOT Sent...."+e);
        }
    }

    def sesmail(String from,String to,String body,String subject){
        final String FROM = from//"noreply@volp.in";   // This has been verified on the Amazon SES setup
        final String TO = to//to"nandakishor@eduplusnow.com";  // I have production access
        AWSCredential awsc=AWSCredential.findByName("SES")
        final String BODY = body//"This email was sent through the Amazon SES SMTP interface by using Java.";
        final String SUBJECT = subject//"Amazon SES test (SMTP interface accessed using Java)";

        final String SMTP_USERNAME = awsc.accesskey//"AKIAWLBOOLH4YZ2LESU5";
        final String SMTP_PASSWORD = awsc.secretkey//"BAD6/eRL3RFw5AxncD8Oit3c8A9RAg94Ih1Istxfzzyp";

        final String HOST = awsc.host//"email-smtp.us-east-1.amazonaws.com";

        final int PORT = awsc.port//587;

        Properties props = System.getProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.port", PORT);

        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.starttls.required", "true");

        Session session = Session.getDefaultInstance(props);

        MimeMessage msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(FROM));
        msg.setRecipient(Message.RecipientType.TO, new InternetAddress(TO));
        msg.setSubject(SUBJECT);
        msg.setContent(BODY,"text/html");

        Transport transport = session.getTransport();

        try
        {
            //System.out.//println("Attempting to send an email through the Amazon SES SMTP interface...");
            transport.connect(HOST, SMTP_USERNAME, SMTP_PASSWORD);
            transport.sendMessage(msg, msg.getAllRecipients());
            println("Email sent!");
        }
        catch (Exception ex) {
            System.out.println("The email was not sent.");
            System.out.println("Error message: " + ex.getMessage());
        }
        finally
        {
            transport.close();

        }
    }
}
