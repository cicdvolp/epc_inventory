package volp
import grails.gorm.transactions.Transactional
@Transactional
class CommonService {

    InformationService InformationService
    SendMailService sendMailService

    def serviceMethod() {

    }

    def daysBetween(Date d1, Date d2) {
        return (int)( (d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
    }

    def saveDTEInformation(String application_id,String name_of_candidate,String merit_no,String mht_cet_score, ApplicationAcademicYear aay,Gender gender,ERPAdmissionRound erpAdmissionRound,ERPStudentAdmissionMainCategory erpStudentAdmissionMainCategory,ERPShift erpShift,ERPSeatType erpSeatType,Program program,Year year,Organization org,DTEAllotmentList dal,String jee_marks,String diploma_marks, ERPDomacile erpDomacile,ERPAdmissionQuota erpAdmissionQuota,AcademicYear academicYear)

    {
        println("dal :"+dal)
        if(dal==null) {
            dal = new DTEAllotmentList()
            dal.merit_no = merit_no
            dal.mht_cet_score = mht_cet_score
            dal.applicationid = application_id
            dal.name_of_candidate = name_of_candidate
            dal.jee_marks = jee_marks
            dal.diploma_marks = diploma_marks
            dal.erpround = erpAdmissionRound
            dal.category = erpStudentAdmissionMainCategory
            dal.gender = gender
            dal.erpseattype = erpSeatType
            dal.erpshift = erpShift
            dal.program = program
            dal.year = year
            dal.erpdomacile = erpDomacile
            dal.erpadmissionquota = erpAdmissionQuota
            dal.academicyear = academicYear
            dal.organization = org
            dal.save(flush: true, failOnError: true)
            return 1
        }
        else {
//            dal.merit_no = merit_no
//            dal.mht_cet_score = mht_cet_score
//            dal.applicationid = application_id
//            dal.name_of_candidate = name_of_candidate
//            dal.jee_marks = jee_marks
//            dal.diploma_marks = diploma_marks
//            dal.erpround = erpAdmissionRound
//            dal.category = erpStudentAdmissionMainCategory
//            dal.gender = gender
//            dal.erpseattype = erpSeatType
//            dal.erpshift = erpShift
//            dal.program = program
//            dal.year = year
//            dal.erpdomacile = erpDomacile
//            dal.erpadmissionquota = erpAdmissionQuota
//            dal.academicyear = academicYear
//            dal.organization = org
//            dal.save(flush: true, failOnError: true)
//            return 1
             return 0
        }
    }

    def sendAttendanceEmail(def learnerdivision, def attendanace, def user, def ip){
        println("sendAttendanceEmail")
        def learner = learnerdivision?.learner
        def dept = learner?.program?.department
        def org = learner?.organization
        def emailsetting = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Send Email to student if they are marked absent in attendance?', org)?.value
        if(emailsetting == 'true') {
            def msg = "Dear Parent ," +
                      "<br> Your ward " + learner?.person?.firstName + " " + learner?.person?.lastName + " is absent for many classes in the college. You are requested to visit college and meet class teacher immediately or call." +
                      "<br> " + dept?.name + " Dept."
                      "<br> " + org?.organization_name

            ERPRelationType father = ERPRelationType.findByName( "Father" )
            ERPRelationType mother = ERPRelationType.findByName( "Mother" )
            ERPRelativeDetails erprelativedetailsfather = ERPRelativeDetails.findByErprelationtypeAndLearner( father, learner )
            ERPRelativeDetails erprelativedetailsmother = ERPRelativeDetails.findByErprelationtypeAndLearner( mother, learner )

            def email
            if (erprelativedetailsfather)
                email = erprelativedetailsfather?.email
            if (!email)
                email = erprelativedetailsmother?.email

//            email = 'komal@edupluscampus.com'
            if (org?.establishment_email && org?.establishment_email_credentials && email) {
                if(!attendanace?.emailsendonabsent) {
                    sendMailService.sendmailwithcss( org?.establishment_email, org.establishment_email_credentials, email, org?.organization_code + " Student Attendance", msg, "" )
                    attendanace.emailsendonabsent = true
                    attendanace.username = user
                    attendanace.updation_date = new java.util.Date()
                    attendanace.updation_ip_address = ip
                    attendanace.save( flush: true, failOnError: true )
                }
            }
        }
    }

    def sendAttendanceSMS(def learnerdivision, def attendanace, def user, def ip){
        println("sendAttendanceSMS")
        def learner = learnerdivision?.learner
        def dept = learner?.program?.department
        def org = learner?.organization
        def smssetting = ERPRoleTypeSettings.findByNameIlikeAndOrganization('Send SMS to student if they are marked absent in attendance?', org)?.value
        if(smssetting == 'true') {
            def msg = "Dear Parent ," +
                    " Your ward " + learner?.person?.firstName + " " + learner?.person?.lastName + " is absent for many classes in the college. You are requested to visit college and meet class teacher immediately or call." +
                    " " + dept?.name + " Dept."
                    " " + org?.organization_name

            ERPRelationType father = ERPRelationType.findByName( "Father" )
            ERPRelationType mother = ERPRelationType.findByName( "Mother" )
            ERPRelativeDetails erprelativedetailsfather = ERPRelativeDetails.findByErprelationtypeAndLearner( father, learner )
            ERPRelativeDetails erprelativedetailsmother = ERPRelativeDetails.findByErprelationtypeAndLearner( mother, learner )

            def mobile_no
            if (erprelativedetailsfather)
                mobile_no = erprelativedetailsfather?.mobile_no
            if (!mobile_no)
                mobile_no = erprelativedetailsmother?.mobile_no

//            mobile_no = '9922834870'
            if (mobile_no) {
                if(!attendanace?.smssendonabsent) {
                    InformationService.smsCommonService( msg, mobile_no, org, null)
                    attendanace.smssendonabsent = true
                    attendanace.username = user
                    attendanace.updation_date = new java.util.Date()
                    attendanace.updation_ip_address = ip
                    attendanace.save(flush: true, failOnError: true)
                }
            }
        }
    }

    def getRecDepartment(Organization org, RecVersion recver){
        def recdept = RecBranch.findAllByOrganizationAndRecversionAndIsactive(org, recver, true)?.program?.department
        HashSet departmentlist = []
        departmentlist.addAll(recdept)
        departmentlist.sort{it.name}

        return  departmentlist
    }

    def getRecProgram(Organization org, RecVersion recver){
        def recprog = RecBranch.findAllByOrganizationAndRecversionAndIsactive(org, recver, true)?.program
        HashSet programlist = []
        programlist.addAll(recprog)
        programlist.sort{it.name}

        return  programlist
    }
}
