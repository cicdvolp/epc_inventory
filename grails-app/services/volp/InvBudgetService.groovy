package volp

import grails.gorm.transactions.Transactional

import javax.servlet.http.Part
import java.nio.file.Paths
import java.text.SimpleDateFormat

@Transactional
class InvBudgetService {

    def serviceMethod() {

    }

    def saveInvBudget(params,session,request,flash){
        println"in saveInvBudget service...." + params
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

            def ay = AcademicYear.findById(params.academicYearId)
            def activity_name = (params.activityName).trim().toUpperCase()
            println"------>"+activity_name
            def invApprovalStatus = InvApprovalStatus.findByNameAndIsactive("In-Process",true)
            def invBudgetLevel = InvBudgetLevel.findById(params.budgetLevelId)
            InvBudget invBudget
            if(params.departmentId){
                def dept = Department.findById(params.departmentId)
                invBudget = InvBudget.findByOrganizationAndDepartmentAndAcademicyearAndActivity_name(org,dept,ay,activity_name)
            }else{
                invBudget = InvBudget.findByOrganizationAndInvbudgetlevelAndAcademicyearAndActivity_name(org,invBudgetLevel,ay,activity_name)
            }
            if(invBudget == null){
                invBudget = new InvBudget()
                invBudget.activity_name = activity_name
                invBudget.isactive = true
                invBudget.isapprove = false
                invBudget.budget_entry_date = new java.util.Date()
                invBudget.invapprovalstatus = invApprovalStatus
                if(params.departmentId){
                    def dept = Department.findById(params.departmentId)
                    invBudget.department = dept
                }
                invBudget.academicyear = ay
                invBudget.amount = Double.parseDouble(params.amount)
                invBudget.organization = org
                def invBudgetType = InvBudgetType.findById(params.budgetTypeId)
                invBudget.invbudgettype = invBudgetType
                invBudget.invbudgetlevel = invBudgetLevel
                invBudget.enterby = instructor

                AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                Part filePart = request.getPart("newFile")
                def fileobj = request.getFile("newFile")
                if(!fileobj.empty){
                    String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                    int indexOfDot = filePartName.lastIndexOf('.')
                    def fileExtention
                    if (indexOfDot > 0) {
                        fileExtention = filePartName.substring(indexOfDot + 1)
                    }
                    String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                    def folderPath
                    if(params.departmentId){
                        def dept = Department.findById(params.departmentId)
                        folderPath = awsFolderPath.path + awsBucket?.content +"/"+"inventory/BUDGET"+"/"+ay?.ay+"/"+dept?.name
                    }else{
                        folderPath = awsFolderPath.path + awsBucket?.content +"/"+"inventory/BUDGET"+"/"+ay?.ay+"/"+invBudgetLevel?.name
                    }
                    def fileName = activity_name +"_BUDGET_"+ date +"."+ fileExtention
                    String existsFilePath = ""
                    def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                    if(isUpload){
                        invBudget.file_path = folderPath
                        invBudget.file_name = fileName
                    }else {
                        println "Something went wrong while uploading file..!"
                    }
                }

                invBudget.creation_username = instructor.uid
                invBudget.updation_username = instructor.uid
                invBudget.updation_ip_address = request.getRemoteAddr()
                invBudget.creation_ip_address = request.getRemoteAddr()
                invBudget.updation_date = new java.util.Date()
                invBudget.creation_date = new java.util.Date()
                invBudget.save(flush: true, failOnError: true)

                def invFinanceApprovingCategory = InvFinanceApprovingCategory.findByOrganizationAndNameAndIsactive(org,"Budget",true)
                def invFinanceApprovingAuthorityLevel = InvFinanceApprovingAuthorityLevel.findAllWhere(organization:org,isactive:true,invfinanceapprovingcategory:invFinanceApprovingCategory)
                for(authLevels in invFinanceApprovingAuthorityLevel){
                    InvBudgetEscalation invBudgetEscalation = InvBudgetEscalation.findByInvbudgetAndInvfinanceapprovingauthoritylevel(invBudget,authLevels)
                    if(invBudgetEscalation == null)
                    {
                        invBudgetEscalation = new InvBudgetEscalation()
                        invBudgetEscalation.remark = activity_name
                        invBudgetEscalation.action_date = new java.util.Date()
                        invBudgetEscalation.organization = org
                        invBudgetEscalation.invbudget = invBudget
                        invBudgetEscalation.invfinanceapprovingauthoritylevel = authLevels
                        invBudgetEscalation.invapprovalstatus = invApprovalStatus
                        invBudgetEscalation.actionby = instructor
                        invBudgetEscalation.isactive = true
                        invBudgetEscalation.creation_username = instructor.uid
                        invBudgetEscalation.updation_username = instructor.uid
                        invBudgetEscalation.updation_ip_address = request.getRemoteAddr()
                        invBudgetEscalation.creation_ip_address = request.getRemoteAddr()
                        invBudgetEscalation.updation_date = new java.util.Date()
                        invBudgetEscalation.creation_date = new java.util.Date()
                        invBudgetEscalation.save(flush: true, failOnError: true)
                        println"saving invBudgetEscalation for Authority Level " + authLevels?.level_no
                    }else{
                        continue
                    }
                }

                flash.message = "Saved Successfully"
            }else{
                flash.error = "Duplicate Entry"
            }
    }
    def editInvBudget(params,session,request,flash){
        println"Params in editinvBudget" + params + "----->" + request
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def dept = Department.findById(params.departmentId)
        def ay = AcademicYear.findById(params.academicYearId)
        def invBudgetType = InvBudgetType.findById(params.budgetTypeId)
        def invBudgetLevel = InvBudgetLevel.findById(params.budgetLevelId)
        def activity_name = params.activityName
        InvBudget invBudget = InvBudget.findById(params.invBudget)
        InvBudget invBudget1 = InvBudget.findByActivity_nameAndIsactiveAndOrganizationAndDepartmentAndAcademicyearAndInvbudgettypeAndInvbudgetlevelAndAmount(activity_name,true,org,dept,ay,invBudgetType,invBudgetLevel,params.amount)
        if(invBudget1 == null){
            invBudget.activity_name = activity_name
            invBudget.budget_entry_date = new java.util.Date()
            invBudget.department = dept
            invBudget.academicyear = ay
            invBudget.organization = org
            invBudget.invbudgettype = invBudgetType
            invBudget.invbudgetlevel = invBudgetLevel
            invBudget.enterby = instructor
            invBudget.amount = Double.parseDouble(params.amount)

            AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
            AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
            Part filePart = request.getPart("newFile")
            def fileobj = request.getFile("newFile")
            if(!fileobj.empty){
                String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                int indexOfDot = filePartName.lastIndexOf('.')
                def fileExtention
                if (indexOfDot > 0) {
                    fileExtention = filePartName.substring(indexOfDot + 1)
                }
                String date = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                def folderPath = awsFolderPath.path + awsBucket?.content +"/"+"inventory/BUDGET"+"/"+ay?.ay+"/"+dept?.name
                def fileName = activity_name +"_BUDGET_"+ date +"."+ fileExtention
                String existsFilePath = ""
                def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                if(isUpload){
                    invBudget.file_path = folderPath
                    invBudget.file_name = fileName
                }else {
                    println "Something went wrong while uploading file..!"
                }
            }

            invBudget.updation_username = instructor.uid
            invBudget.updation_ip_address = request.getRemoteAddr()
            invBudget.updation_date = new java.util.Date()
            invBudget.save(flush: true, failOnError: true)
            flash.message = "Updated Successfully"
        }else{
            flash.error = 'Already Present,Please Edit'
        }
    }
    //deleteInvBudget NOT IN USE
    def deleteInvBudget(params,flash){
        InvBudget invBudget = InvBudget.findById(params.invBudgetId)
        if (invBudget == null)
        {
            flash.error = "Record Not Found"
        }
        else {
            //Delete Budget Details and Budget Escalation if BUdget is DELETED
            /*
            def invBudgetEscalation = InvBudgetEscalation.findAllWhere(invbudget: invBudget)
            for(buds in invBudgetEscalation){
                buds.delete(flush: true, failOnError: true)
            }
            def invBudgetDetails = InvBudgetDetails.findAllWhere(invbudget: invBudget)
            for(details in invBudgetDetails){
                details.delete(flush: true, failOnError: true)
            }
*/
            invBudget.delete(flush: true, failOnError: true)
            flash.message = "Deleted Successfully"
        }
    }
    def activateInvBudget(params,session,request,flash){
        Login login = Login.findById(session.loginid)
        InvBudget invBudget = InvBudget.findById(params.invBudgetId)
        def budgetEscalations = InvBudgetEscalation.findAllWhere(invbudget:invBudget)
        if(invBudget) {
            if(invBudget.isactive) {
                invBudget.isactive = false
                invBudget.updation_username = login.username
                invBudget.updation_date = new Date()
                invBudget.updation_ip_address = request.getRemoteAddr()
                invBudget.save(flush: true, failOnError: true)
                for(be in budgetEscalations){
                    be.isactive = false
                    be.save(flush: true,failOnError: true)
                }
                flash.message = "Successfully.."
            } else {
                invBudget.isactive = true
                invBudget.updation_username = login.username
                invBudget.updation_date = new Date()
                invBudget.updation_ip_address = request.getRemoteAddr()
                invBudget.save(flush: true, failOnError: true)
                for(be in budgetEscalations){
                    be.isactive = true
                    be.save(flush: true,failOnError: true)
                }
                flash.message = "Successfully.."
            }
        } else {
            flash.error = "Budget Not found"
        }
        return
    }

    def saveBudgetDetails(params,session,request,flash){
        println"Params in service--> " + params
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        InvBudget invBudget = InvBudget.findById(params.budgetId)
        InvMaterial invMaterial = InvMaterial.findById(params.materialId)
        int qty = Integer.parseInt(params.quantity)
        double acpu = Double.parseDouble(params.costPerUnit)
        InvBudgetDetails invBudgetDetails = InvBudgetDetails.findByInvbudgetAndInvmaterialAndOrganization(invBudget,invMaterial,org)
        if(invBudgetDetails == null){
            invBudgetDetails = new InvBudgetDetails()
            invBudgetDetails.quantity_required = qty
            invBudgetDetails.approx_cost_per_unit = acpu
            invBudgetDetails.total_cost = qty * acpu
            invBudgetDetails.remark = params.remarks
            invBudgetDetails.invbudget = invBudget
            invBudgetDetails.invmaterial = invMaterial
            invBudgetDetails.organization = org
            invBudgetDetails.isactive = true
            /if(params.isactive == 'on') {
                invBudgetDetails.isactive = true
            } else {
                invBudgetDetails.isactive = false
            }*/
            invBudgetDetails.creation_username = instructor.uid
            invBudgetDetails.updation_username = instructor.uid
            invBudgetDetails.updation_ip_address = request.getRemoteAddr()
            invBudgetDetails.creation_ip_address = request.getRemoteAddr()
            invBudgetDetails.updation_date = new java.util.Date()
            invBudgetDetails.creation_date = new java.util.Date()

            invBudgetDetails.save(flush: true, failOnError: true)
            flash.message = "Saved Successfully"
        }else{
            flash.error = "Duplicate Entry"
        }
    }
    def deleteBudgetDetails(params,flash){
        InvBudgetDetails invBudgetDetails = InvBudgetDetails.findById(params.budgetDetailsId)
        if (invBudgetDetails == null)
        {
            flash.error = "Record Not Found"
        }
        else {
            invBudgetDetails.delete(flush: true, failOnError: true)
            flash.message = "Deleted Successfully"
        }
    }
    def editBudgetDetails(params,session,request,flash){
        println"params editBudgetDetails service-->"+params
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invBudget = InvBudget.findById(params.budgetId)
        InvMaterial invMaterial = InvMaterial.findById(params.materialId)
        int qty = Integer.parseInt(params.quantity)
        double acpu = Double.parseDouble(params.costPerUnit)
        InvBudgetDetails invBudgetDetails = InvBudgetDetails.findById(params.budgetDetailsId)
        InvBudgetDetails invBudgetDetails1 = InvBudgetDetails.findByInvmaterialAndInvbudget(invMaterial,invBudget)
        println"deuplicate"+invBudgetDetails1
        if(invBudgetDetails1 == null){
            invBudgetDetails.quantity_required = qty
            invBudgetDetails.approx_cost_per_unit = acpu
            invBudgetDetails.total_cost = qty * acpu
            invBudgetDetails.remark = params.remarks
            //invBudgetDetails.invbudget = invBudget
            invBudgetDetails.invmaterial = invMaterial
            invBudgetDetails.organization = org
            if(params.isactive == 'on') {
                invBudgetDetails.isactive = true
            } else {
                invBudgetDetails.isactive = false
            }
            invBudgetDetails.updation_username = instructor.uid
            invBudgetDetails.updation_ip_address = request.getRemoteAddr()
            invBudgetDetails.updation_date = new java.util.Date()
            invBudgetDetails.save(flush: true, failOnError: true)
            flash.message = "Updated Successfully"
        }else{
            flash.error = 'Already Present...'
        }
    }
    def activateBugdetDetails(params,session,request,flash){
        Login login = Login.findById(session.loginid)
        InvBudgetDetails invBudgetDetails = InvBudgetDetails.findById(params.budgetDetailsId)
        if(invBudgetDetails) {
            if(invBudgetDetails.isactive) {
                invBudgetDetails.isactive = false
                invBudgetDetails.updation_username = login.username
                invBudgetDetails.updation_date = new Date()
                invBudgetDetails.updation_ip_address = request.getRemoteAddr()
                invBudgetDetails.save(flush: true, failOnError: true)
                flash.message = "In-Active Successfully.."
            } else {
                invBudgetDetails.isactive = true
                invBudgetDetails.updation_username = login.username
                invBudgetDetails.updation_date = new Date()
                invBudgetDetails.updation_ip_address = request.getRemoteAddr()
                invBudgetDetails.save(flush: true, failOnError: true)
                flash.message = "Active Successfully.."
            }
        } else {
            flash.error = "Budget Details Not found"
        }
        return
    }

    //Used to update invBudgetEscalation Domain
    def updateBudgetStatus(params,session,flash){
        println"i am in updateBudgetEscalationsStatus in service"
        if(params.invBudgetId != 'null' && params.approvalStatusId != 'null'){
            InvBudget invBudget = InvBudget.findById(params.invBudgetId)
            Login login = Login.findById(session.loginid)
            Instructor instructor = Instructor.findByUid(login.username)
            InvApprovalStatus invApprovalStatus = InvApprovalStatus.findById(params.approvalStatusId)
            if(invBudget != null && invApprovalStatus != null){
                def escalations = InvBudgetEscalation.findAllWhere(invbudget:invBudget)
                def singleEsc = InvBudgetEscalation.findById(params.budgetEscalationId)
                for(esc in escalations)
                {
                    if(esc?.invfinanceapprovingauthoritylevel?.level_no == singleEsc?.invfinanceapprovingauthoritylevel?.level_no)
                    {

                        if(invApprovalStatus?.name == "In-Process"){
                            esc.remark = params.remark
                            esc.actionby = instructor
                            esc.save(flush: true,failOnError: true)
                        }
                        else if(invApprovalStatus?.name == "Approved")
                        {
                            esc.invapprovalstatus = invApprovalStatus
                            esc.isactive =false
                            esc.remark = params.remark
                            esc.actionby = instructor
                            esc.action_date = new java.util.Date()
                            esc.save(flush: true,failOnError: true)
                            if(esc?.invfinanceapprovingauthoritylevel?.islast == true){
                                invBudget.isapprove = true
                                invBudget.invapprovalstatus = invApprovalStatus
                                invBudget.save(flush: true,failOnError: true)
                            }

                        }else if(invApprovalStatus.name == "Rejected")
                        {
                            invBudget.isactive = false;
                            invBudget.save(flush: true,failOnError: true)

                            //Reject All Escalations ----Isactive made false
                            for(esc1 in escalations)
                            {
                                esc1.isactive = false;
                                esc1.action_date = new java.util.Date()
                                esc1.save(flush: true,failOnError: true)
                            }

                            //Reject All Budget Details ----Isactive made false
                            def budgetDetails = InvBudgetDetails.findAllWhere(invbudget:invBudget)
                            for(dets in budgetDetails)
                            {
                                dets.isactive = false
                                dets.save(flush: true,failOnError: true)
                            }
                            break
                        }
                        else{
                            flash.error = "Status Updation Error"
                        }
                    }else if(esc?.invfinanceapprovingauthoritylevel?.level_no > singleEsc?.invfinanceapprovingauthoritylevel?.level_no)
                    {
                        esc.isactive =true;
                        esc.action_date = new java.util.Date()
                        esc.save(flush: true,failOnError: true)
                        break;
                    }
                }
                flash.message = "Saved Successfully"
            }else
            {
                flash.error = "Record Not Found"
            }
        }else{
            flash.error = "Values Missing"
        }
    }

    //Currently Not In use
    def inviteQuotations(params,session,request,flash){
        println"in inviteQuotations in budget service...."
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        InvBudget invBudget = InvBudget.findById(params.invBudgetId)
        InvPurchaseRequisition invPR = InvPurchaseRequisition.findByInvbudget(invBudget)
        def invBudgetDetails = InvBudgetDetails.findAllWhere(invbudget: invBudget,isactive: true,organization: org)
        for(details in invBudgetDetails){
            def invMaterialCategory = InvMaterialCategory.findById(details?.invmaterial?.invmaterialcategory?.id)
            def invVendor = InvVendor.findAll(organization: org,invMaterialCategory:invMaterialCategory)
            for(vendor in invVendor){
                InvQuotation invQuotation = InvQuotation.findByVendorAndInvpurchaserequisition(vendor,invPR)
                if(invQuotation == null){
                    invQuotation = new InvQuotation()
                    invQuotation.invvendor = vendor
                    invQuotation.invpurchaserequisition = invPR

                    invQuotation.amount = 0
                    invQuotation.tax = 0
                    invQuotation.total = 0
                    /*
                    invQuotation.file_path = invPR
                    invQuotation.file_name = invPR
                    */
                    invQuotation.quotation_date = new java.util.Date()
                    invQuotation.isapproved = false
                    invQuotation.isactive = true

                    invQuotation.creation_username = vendor.contact_person_name
                    invQuotation.updation_username = vendor.contact_person_name
                    invQuotation.updation_ip_address = request.getRemoteAddr()
                    invQuotation.creation_ip_address = request.getRemoteAddr()
                    invQuotation.updation_date = new java.util.Date()
                    invQuotation.creation_date = new java.util.Date()
                    invQuotation.save(flush: true, failOnError: true)

                }else{
                    flash.error = "Quotation Already Raised"
                }
            }
        }
        return true
    }
}
