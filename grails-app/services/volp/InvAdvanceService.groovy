package volp

import grails.gorm.transactions.Transactional

import javax.servlet.http.Part
import java.nio.file.Paths
import java.text.SimpleDateFormat

@Transactional
class InvAdvanceService {

    def serviceMethod() {

    }

    def saveInvAdvanceRequest(params, session, request, flash) {
        println("in saveInvAdvanceRequest service...." + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invApprovalStatus = InvApprovalStatus.findByNameAndIsactive("In-Process", true)
        def purpose = params.advanceRequestPurpose.trim().toUpperCase()
        InvAdvanceRequest invAdvanceRequest = InvAdvanceRequest.findByPurposeAndOrganizationAndIsapproved(purpose,org,false)

        if (invAdvanceRequest == null) {
            invAdvanceRequest = new InvAdvanceRequest()
            invAdvanceRequest.amount = Double.parseDouble(params.advanceRequestAmount)
            invAdvanceRequest.purpose = purpose

            invAdvanceRequest.request_date = new java.util.Date()
            invAdvanceRequest.account_number = params.advanceRequestAccountNumber
            invAdvanceRequest.bank_name = params.advanceRequestBankName
            invAdvanceRequest.bank_branch = params.advanceRequestBankAddress
            invAdvanceRequest.ifsc_code = params.advanceRequestIFSC
            invAdvanceRequest.sanctioned_amount = 0
            invAdvanceRequest.isactive = true
            invAdvanceRequest.isapproved = false
            invAdvanceRequest.requestby = instructor
            invAdvanceRequest.invapprovalstatus = invApprovalStatus
            def invPaymentMethod = InvPaymentMethod.findById(params.advanceRequestPaymentMethod)
            invAdvanceRequest.invpaymentmethod = invPaymentMethod
            invAdvanceRequest.organization = org
            invAdvanceRequest.creation_username = instructor.uid
            invAdvanceRequest.updation_username = instructor.uid
            invAdvanceRequest.creation_ip_address = request.getRemoteAddr()
            invAdvanceRequest.updation_ip_address = request.getRemoteAddr()
            invAdvanceRequest.updation_date = new java.util.Date()
            invAdvanceRequest.creation_date = new java.util.Date()
            invAdvanceRequest.save(flush: true, failOnError: true)

            //Save In Advance Request Escalation As Well to use it in approve advance request
            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByOrganizationAndNameAndIsactive(org, "Advance Request", true)
            def invApprovingAuthorityLevels = InvFinanceApprovingAuthorityLevel.findAllWhere(organization:org,isactive:true, invfinanceapprovingcategory:invFinanceApprovingCategory)
            for(authority_level in invApprovingAuthorityLevels)
            {
                InvAdvanceRequestEscalation invAdvanceRequestEscalation=InvAdvanceRequestEscalation.findByInvadvancerequestAndInvfinanceapprovingauthoritylevel(invAdvanceRequest,authority_level)
                if(invAdvanceRequestEscalation == null)
                {
                    invAdvanceRequestEscalation=new InvAdvanceRequestEscalation()
                    invAdvanceRequestEscalation.action_date=new Date()
                    invAdvanceRequestEscalation.sanctioned_amount = 0
                    //invAdvanceRequestEscalation.remark=params.advanceRequestPurpose
                    invAdvanceRequestEscalation.organization=org
                    invAdvanceRequestEscalation.invadvancerequest=invAdvanceRequest
                    invAdvanceRequestEscalation.invfinanceapprovingauthoritylevel=authority_level
                    invAdvanceRequestEscalation.invapprovalstatus=invApprovalStatus
                    invAdvanceRequestEscalation.actionby=instructor
                    invAdvanceRequestEscalation.isactive = true
                    invAdvanceRequestEscalation.creation_username = instructor.uid
                    invAdvanceRequestEscalation.updation_username = instructor.uid
                    invAdvanceRequestEscalation.creation_ip_address = request.getRemoteAddr()
                    invAdvanceRequestEscalation.updation_ip_address = request.getRemoteAddr()
                    invAdvanceRequestEscalation.updation_date = new java.util.Date()
                    invAdvanceRequestEscalation.creation_date = new java.util.Date()
                    invAdvanceRequestEscalation.save(flush:true,failOnError:true)
                    //println("Saving Escalation with authority level-->"+authority_level?.level_no)
                }
            }
            flash.message="Advance Requested Successfully"
        } else {
            flash.error = "Advance Request Already Exists For Purpose: " + purpose
        }
    }
    def editInvAdvanceRequest(params, session, request, flash) {
        println("in editInvAdvanceRequest service...." + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def purpose = params.advanceRequestPurpose.trim().toUpperCase()
        InvAdvanceRequest invAdvanceRequest = InvAdvanceRequest.findById(params.invAdvanceRequestID)
        if (invAdvanceRequest != null) {
            invAdvanceRequest.amount = Double.parseDouble(params.advanceRequestAmountEdit)
            invAdvanceRequest.purpose = purpose
            invAdvanceRequest.account_number = params.advanceRequestAccountNumber
            invAdvanceRequest.bank_name = params.advanceRequestBankName
            invAdvanceRequest.bank_branch = params.advanceRequestBankAddress
            invAdvanceRequest.ifsc_code = params.advanceRequestIFSC
            invAdvanceRequest.isactive = true
            invAdvanceRequest.requestby = instructor
            def invPaymentMethod = InvPaymentMethod.findById(params.advanceRequestPaymentMethod)
            invAdvanceRequest.invpaymentmethod = invPaymentMethod
            invAdvanceRequest.organization = org
            invAdvanceRequest.updation_username = instructor.uid
            invAdvanceRequest.updation_ip_address = request.getRemoteAddr()
            invAdvanceRequest.updation_date = new java.util.Date()
            invAdvanceRequest.save(flush: true, failOnError: true)
            flash.message="Record Updated Successfully"
        } else {
            flash.error = "Record Not Found"
        }

    }
    def deleteInvAdvanceRequest(params,flash) {
        InvAdvanceRequest invAdvanceRequest = InvAdvanceRequest.findById(params.advanceRequestId)
        if (invAdvanceRequest == null) {
            flash.error = "Record Not Found.Deletion Unsuccesful"
        } else {
            invAdvanceRequest.delete(flush: true, failOnError: true)
            flash.message = "Record Deleted Successfully"
        }
    }
    def activateInvAdvanceRequest(params, session, request, flash) {
        println("in activateInvAdvanceRequest service...."+params)
        Login login = Login.findById(session.loginid)
        InvAdvanceRequest invAdvanceRequest = InvAdvanceRequest.findById(params.advanceRequestId)
        def invAdvanceRequestEscalation_list = InvAdvanceRequestEscalation.findAllByInvadvancerequest(invAdvanceRequest)
        if (invAdvanceRequest)
        {
            if(invAdvanceRequest.isactive) {
                invAdvanceRequest.isactive = false
                invAdvanceRequest.updation_username = login.username
                invAdvanceRequest.updation_date = new java.util.Date()
                invAdvanceRequest.updation_ip_address = request.getRemoteAddr()
                invAdvanceRequest.save(flush: true, failOnError: true)
                if(invAdvanceRequestEscalation_list){
                    for (ARE in invAdvanceRequestEscalation_list){
                        ARE.isactive= false
                        ARE.save(flush: true, failOnError: true)
                    }
                }
                flash.message = "In-Active Successfully.."
            } else {
                invAdvanceRequest.isactive = true
                invAdvanceRequest.updation_username = login.username
                invAdvanceRequest.updation_date = new java.util.Date()
                invAdvanceRequest.updation_ip_address = request.getRemoteAddr()
                invAdvanceRequest.save(flush: true, failOnError: true)
                if(invAdvanceRequestEscalation_list){
                    for (ARE in invAdvanceRequestEscalation_list){
                        ARE.isactive= true
                        ARE.save(flush: true, failOnError: true)
                    }
                }
                flash.message = "Active Successfully.."
            }
        }
        else
        {
            flash.error= "Advance Request Not Found"
        }
    }

    //Authority NOT IN USE
    def updateAdvanceRequestApproval(params,session,request,flash) {
        println("I am In updateAdvanceRequestApproval Service"+ params)
        InvAdvanceRequest invAdvanceRequest = InvAdvanceRequest.findById(params.invadvancerequestId)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        if(invAdvanceRequest != null){
            def escalations = InvAdvanceRequestEscalation.findAllWhere(invadvancerequest:invAdvanceRequest)
            def singleEsc = InvAdvanceRequestEscalation.findById(params.advanceRequestEscalationId)
            for(esc in escalations)
            {
                if(esc?.invfinanceapprovingauthoritylevel?.level_no == singleEsc?.invfinanceapprovingauthoritylevel?.level_no)
                {
                    InvApprovalStatus invApprovalStatus = InvApprovalStatus.findById(params.approvalStatus)
                    if(invApprovalStatus?.name == "In-Process"){
                        esc.remark = params.advanceRequestRemark
                        esc.actionby = instructor
                        esc.sanctioned_amount=Double.parseDouble(params.approvedamount)
                        esc.action_date=new java.util.Date()
                        esc.updation_username=instructor.uid
                        esc.updation_date=new java.util.Date()
                        esc.updation_ip_address=request.getRemoteAddr()
                        esc.save(flush: true,failOnError: true)
                        break;
                    }
                    else if(invApprovalStatus?.name == "Approved")
                    {
                        esc.invapprovalstatus =invApprovalStatus
                        esc.isactive =false
                        esc.remark = params.advanceRequestRemark
                        esc.sanctioned_amount=Double.parseDouble(params.approvedamount)
                        esc.actionby = instructor
                        esc.action_date=new java.util.Date()
                        esc.updation_username=instructor.uid
                        esc.updation_date=new java.util.Date()
                        esc.updation_ip_address=request.getRemoteAddr()
                        esc.save(flush: true,failOnError: true)
                        if(esc?.invfinanceapprovingauthoritylevel?.islast == true){
                            invAdvanceRequest.isapproved = true
                            invAdvanceRequest.invapprovalstatus = invApprovalStatus
                            invAdvanceRequest.sanctioned_amount=Double.parseDouble(params.approvedamount)
                            invAdvanceRequest.save(flush: true,failOnError: true)

                        }

                    }else if(invApprovalStatus.name == "Rejected")
                    {
                        invAdvanceRequest.isactive = false
                        invAdvanceRequest.invapprovalstatus=invApprovalStatus
                        invAdvanceRequest.updation_username=instructor.uid
                        invAdvanceRequest.updation_date=new java.util.Date()
                        invAdvanceRequest.updation_ip_address=request.getRemoteAddr()
                        invAdvanceRequest.save(flush: true,failOnError: true)

                        //Reject All Escalations ----Isactive made false
                        for(esc1 in escalations)
                        {
                            esc1.isactive = false;
                            esc1.invapprovalstatus=invApprovalStatus
                            esc1.actionby = instructor
                            esc1.action_date=new java.util.Date()
                            esc1.updation_username=instructor.uid
                            esc1.updation_date=new java.util.Date()
                            esc1.updation_ip_address=request.getRemoteAddr()
                            esc1.save(flush: true,failOnError: true)
                        }
                        break;
                    }
                    else{
                        flash.error = "Status Updation Error"
                    }
                }else if(esc?.invfinanceapprovingauthoritylevel?.level_no > singleEsc?.invfinanceapprovingauthoritylevel?.level_no)
                {
                    esc.isactive =true;
                    esc.save(flush: true,failOnError: true)
                    break;
                }
            }
            flash.message = "Saved Successfully"
        }else
        {
            flash.error = "Record Not Found"
        }

    }

    //Advance Proofs And Settlement
    def saveInvAdvanceProof(params,session,request,flash){
        println("in saveInvAdvanceProof service...." + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def file = request.getFile('expenditureProofFile')
        InvAdvanceRequest invAdvanceRequest=InvAdvanceRequest.findById(params.advRequest)
        def expenditurePurpose = params.expenditurePurpose.trim()
        InvAdvanceRequestReceipt invAdvanceRequestReceipt=InvAdvanceRequestReceipt.findByInvadvancerequest(invAdvanceRequest)
        InvAdvanceSettlement invAdvanceSettlement=InvAdvanceSettlement.findWhere(submissionby:instructor,organization:  org,invadvancerequest:invAdvanceRequest,purpose:expenditurePurpose)
        if(invAdvanceSettlement == null)
        {
            invAdvanceSettlement=new InvAdvanceSettlement()
            invAdvanceSettlement.amount_spent_date=params.expenditureDate
            invAdvanceSettlement.amount_spent=Double.parseDouble(params.expenditureAmount)
            invAdvanceSettlement.amount_returned=Double.parseDouble(params.amountReturned)
            invAdvanceSettlement.purpose=expenditurePurpose
            invAdvanceSettlement.organization=org
            invAdvanceSettlement.invadvancerequestreceipt=invAdvanceRequestReceipt
            invAdvanceSettlement.invadvancerequest=invAdvanceRequest
            invAdvanceSettlement.submissionby=instructor
            if(file!=null)
            {
                AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                Part filePart = request.getPart("expenditureProofFile")
                def fileobj = request.getFile("expenditureProofFile")
                if(!fileobj.empty){
                    String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                    int indexOfDot = filePartName.lastIndexOf('.')
                    def fileExtention
                    if (indexOfDot > 0) {
                        fileExtention = filePartName.substring(indexOfDot + 1)
                        println("fileExtention :: " + fileExtention)
                    }
                    String date1= new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                    def folderPath = awsFolderPath.path + awsBucket?.content +"/"+"inventory/advanced"+"/" + org +"/" + invAdvanceSettlement?.id
                    def fileName = instructor?.employee_code +"_"+ date1 +"_"+invAdvanceRequest?.purpose+"."+ fileExtention
                    String existsFilePath = ""
                      def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                    if(isUpload){
                        invAdvanceSettlement.file_path = folderPath
                        invAdvanceSettlement.file_name = fileName
                        println "File uploaded"
                      }
                    else {
                        println "Something went wrong while uploading file..!"
                      }

                }
            }
            invAdvanceSettlement.isactive=true
            invAdvanceSettlement.creation_username=instructor.uid
            invAdvanceSettlement.updation_username=instructor.uid
            invAdvanceSettlement.creation_date=new java.util.Date()
            invAdvanceSettlement.updation_date=new java.util.Date()
            invAdvanceSettlement.creation_ip_address=request.getRemoteAddr()
            invAdvanceSettlement.updation_ip_address=request.getRemoteAddr()
            invAdvanceSettlement.save(flush: true, failOnError: true)
            flash.message="Proof Saved Successfully"
        }
        else{
            flash.error="Proof Already Submitted,Cannot Add More"
        }
    }
    def editInvAdvanceProof(params,session,request,flash){
        println("in editInvAdvanceProof service...." + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def file = request.getFile('expenditureProofFile')
        InvAdvanceSettlement invAdvanceSettlement=InvAdvanceSettlement.findById(params.advanceSettlementId)
        if(invAdvanceSettlement)
        {
            //invAdvanceSettlement=new InvAdvanceSettlement()
            //String expenditureDate = params.expenditureDate
            //Date date = new SimpleDateFormat("yyyy-MM-dd").parse(expenditureDate);
            invAdvanceSettlement.amount_spent_date=params.expenditureDate
            invAdvanceSettlement.amount_spent=Double.parseDouble(params.expenditureAmount)
            invAdvanceSettlement.purpose=params.expenditurePurpose
            invAdvanceSettlement.organization=org
            invAdvanceSettlement.submissionby=instructor
            if(file!=null)
            {
                AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                AWSBucket awsBucket = AWSBucket.findByContent("documents")
                AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                Part filePart = request.getPart("expenditureProofFile")
                def fileobj = request.getFile("expenditureProofFile")
                if(!fileobj.empty){
                    String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                    int indexOfDot = filePartName.lastIndexOf('.')
                    def fileExtention
                    if (indexOfDot > 0) {
                        fileExtention = filePartName.substring(indexOfDot + 1)
                        println("fileExtention :: " + fileExtention)
                    }

                    String date1= new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                    def folderPath = awsFolderPath.path + awsBucket?.content +"/"+"inventory/advanced"+"/" + org +"/" + invAdvanceSettlement?.id
                    def fileName = instructor?.employee_code +"_"+ date1 +"_"+invAdvanceSettlement?.invadvancerequestreceipt?.invadvancerequest?.purpose+"."+ fileExtention
                    String existsFilePath = ""
                    //println("ExistsFilePath :"+existsFilePath)
                    //awsFolderPath.path + "inventory/advanced/" + organization +"/" + id of InvAdvanceSettlement + "/" +
                    def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                    if(isUpload){
                        invAdvanceSettlement.file_path = folderPath
                        invAdvanceSettlement.file_name = fileName
                        println "File uploaded"
                    }
                    else {
                        println "Something went wrong while uploading file..!"
                    }

                }
            }
            invAdvanceSettlement.updation_username=instructor.uid
            invAdvanceSettlement.updation_date=new java.util.Date()
            invAdvanceSettlement.updation_ip_address=request.getRemoteAddr()
            invAdvanceSettlement.save(flush: true, failOnError: true)
            flash.message="Proof Updated Successfully"
        }
        else{
            flash.error= "Record Not Found"
        }

    }
    def activateInvAdvanceProof(params, session, request, flash){
        println("in activateInvAdvanceProof service...."+params)
        Login login = Login.findById(session.loginid)
        InvAdvanceSettlement invAdvanceSettlement = InvAdvanceSettlement.findById(params.advanceSettlementId)
        if (invAdvanceSettlement)
        {
            if(invAdvanceSettlement.isactive) {
                invAdvanceSettlement.isactive = false
                invAdvanceSettlement.updation_username = login.username
                invAdvanceSettlement.updation_date = new java.util.Date()
                invAdvanceSettlement.updation_ip_address = request.getRemoteAddr()
                invAdvanceSettlement.save(flush: true, failOnError: true)
                flash.message = "In-Active Successfully.."
            } else {
                invAdvanceSettlement.isactive = true
                invAdvanceSettlement.updation_username = login.username
                invAdvanceSettlement.updation_date = new java.util.Date()
                invAdvanceSettlement.updation_ip_address = request.getRemoteAddr()
                invAdvanceSettlement.save(flush: true, failOnError: true)
                flash.message = "Active Successfully.."
            }
        }
        else
        {
            flash.error= "Proof Not Found"
        }
    }

    //Advance Receipt
    def saveAdvanceReceipt(params,request,session,flash){
        println("in saveAdvanceReceipt service...." + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = instructor.organization
        def invAdvanceRequest=InvAdvanceRequest.findById(params.advanceRequestId)
        def invPaymentMethod=InvPaymentMethod.findById(params.receiptPaymentMethod)
        InvPaymentMethod returnPaymentMethod
        if(params.returnMethod == 'null' || params.returnMethod == null || params.returnMethod == ''){
            println"Return payment is Null"
        }
        else{
            returnPaymentMethod = InvPaymentMethod.findById(params.returnMethod)
        }

        InvAdvanceRequestReceipt invAdvanceRequestReceipt=InvAdvanceRequestReceipt.findByInvadvancerequestAndOrganization(invAdvanceRequest,org)
        if(invAdvanceRequestReceipt==null)
        {
            invAdvanceRequestReceipt=new InvAdvanceRequestReceipt()
            def PRPO = PRPOTracking.findByOrganization(org)
            if(PRPO == null){
                PRPO = new PRPOTracking()
                PRPO.PRN = 00001
                PRPO.PON = 00001
                PRPO.invoiceNo = 00001
                PRPO.recieptNo = 00001
                PRPO.advanceRecieptNo = 00001
                PRPO.organization = org
                PRPO.creation_username = login.username
                PRPO.updation_username = login.username
                PRPO.creation_date = new Date()
                PRPO.updation_date = new Date()
                PRPO.creation_ip_address = request.getRemoteAddr()
                PRPO.updation_ip_address = request.getRemoteAddr()
                PRPO.save(flush: true,failOnError: true)
                invAdvanceRequestReceipt.receipt_no=PRPO?.advanceRecieptNo
                PRPO?.advanceRecieptNo++
                println"PRPOTracking for " + org?.trust_name + " created succesfuly"
            }else{
                invAdvanceRequestReceipt.receipt_no=PRPO?.advanceRecieptNo
                PRPO?.advanceRecieptNo++
            }


            invAdvanceRequestReceipt.return_details=params.returnDetails
            invAdvanceRequestReceipt.returnpaymentmethod=returnPaymentMethod
            invAdvanceRequestReceipt.receipt_date=params.receiptTransactionDate
            invAdvanceRequestReceipt.amount_transferred=Double.parseDouble(params.receiptamountTransferred)
            invAdvanceRequestReceipt.account_number=params.receiptBankAccountNo
            invAdvanceRequestReceipt.bank_name=params.receiptBankName
            invAdvanceRequestReceipt.bank_branch=params.receiptBankAddress
            invAdvanceRequestReceipt.ifsc_code=params.receiptIFSC
            invAdvanceRequestReceipt.transaction_number=params.receiptTransactionNo
            invAdvanceRequestReceipt.isactive=true

            invAdvanceRequestReceipt.organization=org
            invAdvanceRequestReceipt.invadvancerequest=invAdvanceRequest
            invAdvanceRequestReceipt.invpaymentmethod=invPaymentMethod
            invAdvanceRequestReceipt.paymentby=instructor

            invAdvanceRequestReceipt.creation_username=instructor.uid
            invAdvanceRequestReceipt.updation_username=instructor.uid
            invAdvanceRequestReceipt.creation_date=new java.util.Date()
            invAdvanceRequestReceipt.updation_date=new java.util.Date()
            invAdvanceRequestReceipt.creation_ip_address=request.getRemoteAddr()
            invAdvanceRequestReceipt.updation_ip_address=request.getRemoteAddr()
            invAdvanceRequestReceipt.save(flush: true,failOnError: true)
            flash.message="Receipt Saved Successfully"
        }
        else {
            flash.error="Receipt Already Exists"
        }
    }
}

