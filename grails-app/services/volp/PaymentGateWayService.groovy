package volp

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import com.sun.org.apache.xml.internal.security.utils.Base64;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.*;
import java.security.MessageDigest;

import grails.gorm.transactions.Transactional

@Transactional
class PaymentGateWayService {

    //Checksum  Test Account
    def hashValue(String message, def key) {
//        def key = 'G3eAmyVkAzKp8jFq0fqPEqxF4agynvtJ'
        byte[] hash = toHmacSHA256(message, key);
        String hashHexed = toHex(hash);
        return hashHexed;
    }

    def toHex(def value) {
        String hexed = String.format("%040x", new BigInteger(1, value));
        return hexed;
    }

    def toHmacSHA256(String value, def key) {
        byte[] hash = null;
        SecretKeySpec secretKey = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(secretKey);
        hash = mac.doFinal(value.getBytes("UTF-8"));

        return hash;
    }


    //Checksum Production Account
    def checkSumSHA256(String plaintext, def checksum)  {
        plaintext = plaintext+"|"+checksum
        MessageDigest md = null;
        md = MessageDigest.getInstance("SHA-256");
        md.update(plaintext.getBytes("UTF-8"));

        StringBuffer ls_sb=new StringBuffer();
        byte[] raw = md.digest();
        for(int i=0;i<raw.length;i++)
            ls_sb.append(char2hex(raw[i]));
        return ls_sb.toString();
    }

    def char2hex(byte x)
    {
        char[] arr =[

                '0','1','2','3',
                '4','5','6','7',
                '8','9','A','B',
                'C','D','E','F'
        ];

        char[] c= [arr[(x & 0xF0)>>4],arr[x & 0x0F]];
        return (new String(c));
    }




//    -------------------------------------------------------------
//    Not In Use
//    def checkcall() {
//         def checksum = hashValue('HmacSHA256', str, 'HZ1o7dCRgY3o');
//    }
//    def hash_hmac(String algorithm, String message, String key){
//        Mac sha256_hmac = Mac.getInstance(algorithm);
//        SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), algorithm);
//        sha256_hmac.init(secret_key);
//        String hash = Base64.encode(sha256_hmac.doFinal(message.getBytes("UTF-8")));
//        return hash.trim();
//    }
//   -------------------------------------------------------------


}
