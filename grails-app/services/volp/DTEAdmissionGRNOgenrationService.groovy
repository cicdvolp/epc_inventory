package volp

import grails.gorm.transactions.Transactional

@Transactional
class DTEAdmissionGRNOgenrationService {

    def serviceMethod() {}

    def grgenration(Organization organization,AcademicYear admissionyear,Year year,Program program,def ip, def user){

         String grno=""
        if(organization.organization_code=='PCCOE')
        {
            ERPGRNumberInstituteCode erpgrnumberinstitutecode=ERPGRNumberInstituteCode.findByOrganization(organization)
            grno=grno+erpgrnumberinstitutecode.institute_code
            grno=grno+admissionyear.shortcut
            ERPGRNumberProgramCode erpgrnumberprogramcode=ERPGRNumberProgramCode.findByProgramAndOrganization(program,organization)
            if(erpgrnumberprogramcode == null)
            {
                return "error404"

            }
            if(erpgrnumberprogramcode.program_code=="M"|| erpgrnumberprogramcode.program_code=="B" || erpgrnumberprogramcode.program_code=="C" || erpgrnumberprogramcode.program_code=="P")
            {
                grno=grno+erpgrnumberprogramcode.program_code
            }

            if(year?.year=='FY')
            {
                grno=grno+"1"
            }
            if (year?.year=='SY' || year?.year == 'SE')
            {
                grno=grno+'2'
            }
            ERPGRNumberTrack egnt=ERPGRNumberTrack.findByErpgrnumberinstitutecodeAndAcademicYearAndProgramtypeAndYear(erpgrnumberinstitutecode,admissionyear,program?.programtype,year)

            int flag=0
            if(egnt==null)
            {
                egnt=new ERPGRNumberTrack()
                egnt.username= user
                egnt.creation_date=new java.util.Date()
                egnt.updation_date=new java.util.Date()
                egnt.creation_ip_address=ip
                egnt.updation_ip_address=ip
                egnt.erpgrnumberinstitutecode=erpgrnumberinstitutecode
                egnt.erpgrnumberprogramcode=erpgrnumberprogramcode
                egnt.academicYear=admissionyear
                egnt.programtype=program.programtype
                egnt.organization=organization
                egnt.year=year
                grno=grno+"0001"
            }
            else
            {
                flag=1
                egnt.updation_date=new java.util.Date()
                egnt.updation_ip_address=ip
                egnt.organization=organization
                int no=egnt.number
                int d=0
                while(no>0)
                {
                    no=no/10
                    d++
                }
                int prefix=4-d
                for(int i=0;i<prefix;i++)
                {
                    grno=grno+"0"
                }
                grno=grno+egnt.number
            }
            if(flag==0)
            {
                egnt.number=2
                egnt.save(failOnError: true, flush: true)
            }
            if(flag==1)
            {
                egnt.number = egnt.number + 1
                egnt.save(failOnError: true, flush: true)
            }
            println("GRNO genrated "+grno)

        }
        else {
            ERPGRNumberInstituteCode erpgrnumberinstitutecode=ERPGRNumberInstituteCode.findByOrganization(organization)
            grno=grno+erpgrnumberinstitutecode.institute_code

            grno=grno+admissionyear.shortcut

            ERPGRNumberProgramCode erpgrnumberprogramcode=ERPGRNumberProgramCode.findByProgramAndOrganization(program,organization)
            if(erpgrnumberprogramcode == null)
            {
                return "error404"

            }
            if(erpgrnumberprogramcode.program_code=="M" || erpgrnumberprogramcode.program_code=="C" || erpgrnumberprogramcode.program_code=="P")
            {
                grno=grno+erpgrnumberprogramcode.program_code
            }
            else {
                if(year.year=='FY')
                {
                    grno=grno+"1"
                }
                if (year.year=='SY')
                {
                    grno=grno+'2'
                }
            }
            ERPGRNumberTrack egnt=ERPGRNumberTrack.findByErpgrnumberinstitutecodeAndAcademicYearAndProgramtypeAndYear(erpgrnumberinstitutecode,admissionyear,program?.programtype,year)

            int flag=0
            if(egnt==null)
            {
                egnt=new ERPGRNumberTrack()
                egnt.username= user
                egnt.creation_date=new java.util.Date()
                egnt.updation_date=new java.util.Date()
                egnt.creation_ip_address=ip
                egnt.updation_ip_address=ip
                egnt.erpgrnumberinstitutecode=erpgrnumberinstitutecode
                egnt.erpgrnumberprogramcode=erpgrnumberprogramcode
                egnt.academicYear=admissionyear
                egnt.programtype=program.programtype
                egnt.organization=organization
                egnt.year=year
                grno=grno+"0001"
            }
            else
            {
                flag=1
                egnt.updation_date=new java.util.Date()
                egnt.updation_ip_address=ip
                egnt.organization=organization
                int no=egnt.number
                int d=0
                while(no>0)
                {
                    no=no/10
                    d++
                }
                int prefix=4-d
                for(int i=0;i<prefix;i++)
                {
                    grno=grno+"0"
                }
                grno=grno+egnt.number
            }
            if(flag==0)
            {
                egnt.number=2
                egnt.save(failOnError: true, flush: true)
            }
            if(flag==1)
            {
                egnt.number = egnt.number + 1
                egnt.save(failOnError: true, flush: true)
            }
            println("GRNO genrated "+grno)
        }

        return grno
    }

    def emailgeration(Organization organization,String firstname,String lastname,AcademicYear ay,def grno)
    {

        def shortcut = ""
        if(ay != null)
            shortcut = ay?.shortcut

        println("shortcut  :"+shortcut)
        String uid=""
        if(organization.organization_code == "VIT")
        {
            uid=firstname.trim()+"."+lastname.trim()+""+shortcut+"@vit.edu"
            int track=1
            while(true)
            {
                Learner learnerobj=Learner.findByOrganizationAndUid(organization,uid)
                Login login=Login.findByOrganizationAndUsername(organization,uid)
                if(learnerobj==null&&login==null)
                    break
                uid=firstname.trim()+"."+lastname.trim()+""+shortcut+""+track+"@vit.edu"
                track++
            }
        }
        else if(organization.organization_code == "PCCOE")
        {
            uid=firstname.trim()+"."+lastname.trim()+""+shortcut+"@pccoepune.org"
            int track=1
            while(true)
            {
                Learner learnerobj=Learner.findByOrganizationAndUid(organization,uid)
                Login login=Login.findByOrganizationAndUsername(organization,uid)
                if(learnerobj==null&&login==null)
                    break
                uid=firstname.trim()+"."+lastname.trim()+""+shortcut+""+track+"@pccoepune.org"
                track++
            }
        }
        else if(organization.organization_code=="VIIT")
        {
            uid=firstname.trim()+"."+grno.trim()+"@viit.ac.in"
        }
        uid = uid.toLowerCase()
        println("Email "+uid)
        return uid
    }

    def facultygmail(Organization organization,String firstname,String lastname)
    {

        String uid=""
        if(organization.organization_code == "VIT")
        {
            uid=firstname.trim()+"."+lastname.trim()+"@vit.edu"
            int track=1
            while(true)
            {
                Learner learnerobj=Learner.findByOrganizationAndUid(organization,uid)
                Login login=Login.findByOrganizationAndUsername(organization,uid)
                if(learnerobj==null&&login==null)
                    break
                uid=firstname.trim()+"."+lastname.trim()+track+"@vit.edu"
                track++
            }
        }
        else if(organization.organization_code == "PCCOE")
        {
            uid=firstname.trim()+"."+lastname.trim()+"@pccoepune.org"
            int track=1
            while(true)
            {
                Learner learnerobj=Learner.findByOrganizationAndUid(organization,uid)
                Login login=Login.findByOrganizationAndUsername(organization,uid)
                if(learnerobj==null&&login==null)
                    break
                uid=firstname.trim()+"."+lastname.trim()+""+track+"@pccoepune.org"
                track++
            }
        }
        else if(organization.organization_code=="VIIT")
        {
            uid=firstname.trim()+"."+lastname.trim()+"@vit.edu"
            int track=1
            while(true)
            {
                Learner learnerobj=Learner.findByOrganizationAndUid(organization,uid)
                Login login=Login.findByOrganizationAndUsername(organization,uid)
                if(learnerobj==null&&login==null)
                    break
                uid=firstname.trim()+"."+lastname.trim()+""+track+"@vit.edu"
                track++
            }
        }
        uid = uid.toLowerCase()
        println("Email "+uid)
        return uid
    }

    def StudentEntry(String user,
                     String applicationid,
                     String email,
                     String grno,
                     Double income,
                     Year year,
                     String fullname,
                     String firstname,
                     String middlename,
                     String lastname,
                     Gender gender,
                     String fathername,
                     String mothername,
                     Date date_of_birth,
                     Organization organization,
                     AcademicYear admissionyear,
                     ERPSeatType erpSeatType,
                     Program program,
                     ERPAdmissionQuota erpadmissionquota,
                     ERPShift erpshift,
                     ERPAdmissionRound erpround,
                     ERPStudentAdmissionMainCategory category,
                     ERPDomacile erpdomacile,
                     ERPCountry erpnationality,
                     ERPCast cast,
                     ERPSubCast subcast,
                     String otherscast,
                     String othersubCast,
                     String adharname,
                     def ip,
                     String personalemail,
                     def mobileno
    )
    {
        Learner learner=Learner.findByDte_application_numberAndOrganization(applicationid,organization)
        println("learner entry")


        println(learner)
        if(learner==null || applicationid=="") {
            learner = new Learner()
            learner.uid = email
            learner.registration_number = grno
            learner.ispassout = false
            learner.dte_application_number = applicationid
            learner.admission_form_no = applicationid.trim().replaceAll(" +", " ")
            learner.admission_date = new java.util.Date()
            if (income != "")
                learner.family_income = income
            learner.isadmissioncancelled = false
            learner.current_year = year
            learner.username = user
            learner.creation_date = new java.util.Date()
            learner.updation_date = new java.util.Date()
            learner.creation_ip_address = ip
            learner.updation_ip_address = ip

            //Let us create person object
            Person person = Person.findByGrno(grno)
            if (person == null) {
                person = new Person()
                person.email = email
                person.grno = grno
                person.organization = organization
                person.nameasperaadhar=adharname
                person.institute_email = email
                person.fullname_as_per_previous_marksheet = fullname.trim().replaceAll(" +", " ")
                person.firstName = firstname.trim().replaceAll(" +", " ")
                person.middleName = middlename.trim().replaceAll(" +", " ")
                person.lastName = lastname.trim().replaceAll(" +", " ")
                person.gender = gender
                person.father_first_name = fathername.trim().replaceAll(" +", " ")
                person.mother_first_name = mothername.trim().replaceAll(" +", " ")
                person.date_of_birth = date_of_birth
                person.username = user
                person.creation_date = new java.util.Date()
                person.updation_date = new java.util.Date()
                person.creation_ip_address = ip
                person.updation_ip_address = ip
                person.save(failOnError: true, flush: true)
            } else {
                person.email = email
                person.grno = grno
                person.organization = organization
                person.institute_email = email
                person.fullname_as_per_previous_marksheet = fullname.trim().replaceAll(" +", " ")
                person.firstName = firstname.trim().replaceAll(" +", " ")
                person.middleName = middlename.trim().replaceAll(" +", " ")
                person.lastName = lastname.trim().replaceAll(" +", " ")
                person.gender = gender
                person.father_first_name = fathername.trim().replaceAll(" +", " ")
                person.mother_first_name = mothername.trim().replaceAll(" +", " ")
                person.date_of_birth = date_of_birth
                person.username = user
                person.creation_date = new java.util.Date()
                person.updation_date = new java.util.Date()
                person.creation_ip_address = ip
                person.updation_ip_address = ip
                person.save(failOnError: true, flush: true)
            }

            def contact = Contact.findByPerson(person)
            if(mobileno !='null' && personalemail !='null') {
                if (contact) {
                    contact.email = personalemail
                    contact.mobile_no = mobileno

                    contact.username = user
                    contact.updation_date = new java.util.Date()
                    contact.updation_ip_address = ip
                    contact.save(failOnError: true, flush: true)
                } else {
                    contact = new Contact()
                    contact.person = person
                    contact.organization = organization
                    contact.email = personalemail
                    contact.mobile_no = mobileno

                    contact.username = user
                    contact.creation_date = new java.util.Date()
                    contact.updation_date = new java.util.Date()
                    contact.creation_ip_address = ip
                    contact.updation_ip_address = ip
                    contact.save(failOnError: true, flush: true)
                }
            }


            println("person "+person)
            learner.person = person
            learner.organization = organization
            learner.gender = gender

            learner.admissionyear = admissionyear
            learner.erpseattype = erpSeatType

            //learner.current_year=year
            learner.program = program
            learner.erpadmissionquota = erpadmissionquota
            ERPAdmissionType erpadmissiontype = null
            if (year.year == "FY")
                erpadmissiontype = ERPAdmissionType.findByType("Regular")
            else if (year.year == "SY")
                erpadmissiontype = ERPAdmissionType.findByType("SEDA")
            learner.erpadmissiontype = erpadmissiontype
            learner.erpshift = erpshift
            learner.erpadmissionround = erpround
            learner.allotederpstudentadmissionmaincategory = category
            learner.erpdomacile = erpdomacile
            learner.erpnationality = erpnationality
            if (cast == "Other"||cast == "Others"||cast == "others"||cast == "other") {
                ERPCast erpcast = ERPCast.findByCast(otherscast)
                if (erpcast == null) {
                    erpcast = new ERPCast()
                    erpcast.cast = otherscast
                    erpcast.username = user
                    erpcast.creation_date = new java.util.Date()
                    erpcast.updation_date = new java.util.Date()
                    erpcast.creation_ip_address = ip
                    erpcast.updation_ip_address = ip
                    erpcast.save(failOnError: true, flush: true)
                }
                learner.erpcast = erpcast
            } else {
                learner.erpcast = cast
            }
            if (subcast == "Other"||subcast == "Others"||subcast == "others"||subcast == "other")  {
                ERPSubCast erpsubcast = ERPSubCast.findBySubcast(othersubCast)
                if (erpsubcast == null) {
                    erpsubcast = new ERPSubCast()
                    erpsubcast.subcast = othersubCast
                    erpsubcast.username = user
                    erpsubcast.creation_date = new java.util.Date()
                    erpsubcast.updation_date = new java.util.Date()
                    erpsubcast.creation_ip_address = ip
                    erpsubcast.updation_ip_address = ip
                    erpsubcast.save(failOnError: true, flush: true)
                }
                learner.erpsubcast = erpsubcast
            } else {
                learner.erpsubcast = subcast
            }

            learner.save(failOnError: true, flush: true)

            LearnerStatus learnerStatus = LearnerStatus.findByStatusAndOrganization("REGULAR", organization)
            LearnerHistory learnerHistory = new LearnerHistory()
            learnerHistory.year = year
            learnerHistory.isregular = true
            learnerHistory.remark = "Admission"
            learnerHistory.learner = learner
            learnerHistory.learnerstatus = learnerStatus
            learnerHistory.academicyear = admissionyear
            learnerHistory.organization = organization
            learnerHistory.username = user
            learnerHistory.creation_date = new java.util.Date()
            learnerHistory.updation_date = new java.util.Date()
            learnerHistory.creation_ip_address = ip
            learnerHistory.updation_ip_address = ip
            learnerHistory.save(failOnError: true, flush: true)
            def fpath = FolderPath.list()
            FolderPath fp = fpath[0]
            new File(fp.path + "/studentprofile/documents/" + grno).mkdirs()
        }
        else {
            learner.username=email
            learner.registration_number=grno
            learner.save(failOnError: true, flush: true)
            Person person = Person.findById(learner.person.id)
            if (person!=null)
            {
                person.grno=grno
                person.email=email
                person.save(failOnError: true, flush: true)
            }

        }
        println("learner?.uid")
        Login l=Login.findByUsername(email)
        println("learner "+l)
        if(l==null)
        {
            l=new Login()
            l.username=email
            l.password=grno
            l.creation_date=new java.util.Date()
            l.updation_date=new java.util.Date()
            l.creation_ip_address=ip
            l.updation_ip_address=ip
            l.isloginblocked=false
            l.organization=organization
            ApplicationType aterp=ApplicationType.findByApplication_type("ERP")
            UserType uterp=UserType.findByTypeAndApplication_typeAndOrganization("Student",aterp, organization)
            l.addToUsertype(uterp)

            RoleType rt_erp_admission=RoleType.findByTypeAndApplicationtypeAndOrganization("Admission",aterp, organization)
            RoleType rt_erp_registration=RoleType.findByTypeAndApplicationtypeAndOrganization("Registration",aterp, organization)
            //RoleType rt_volp=RoleType.findByTypeAndApplicationtypeAndOrganization("VOLP",atvolp, organization)
            RoleType rt_erp_accounts=RoleType.findByTypeAndApplicationtypeAndOrganization("Accounts",aterp, organization)
            RoleType rt_erp_tt=RoleType.findByTypeAndApplicationtypeAndOrganization("TimeTable",aterp, organization)
            RoleType rt_erp_facultyappriasal=RoleType.findByTypeAndApplicationtypeAndOrganization("Faculty Appraisal",aterp, organization)
            RoleType rt_erp_examination=RoleType.findByTypeAndApplicationtypeAndOrganization("Examination",aterp, organization)
            RoleType rt_erp_tpo=RoleType.findByTypeAndApplicationtypeAndOrganization("TPO",aterp, organization)
            RoleType rt_erp_cert=RoleType.findByTypeAndApplicationtypeAndOrganization("Certificates",aterp, organization)
            RoleType rt_erp_asportal=RoleType.findByTypeAndApplicationtypeAndOrganization("ASPORTAL",aterp, organization)
            RoleType rt_erp_quiz=RoleType.findByTypeAndApplicationtypeAndOrganization("Quiz",aterp, organization)
            RoleType rt_erp_online_examination=RoleType.findByTypeAndApplicationtypeAndOrganization("Online Examination",aterp, organization)
            RoleType rt_erp_academics=RoleType.findByTypeAndApplicationtypeAndOrganization("Academics",aterp, organization)

            Role role_erp_admission=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_admission, organization)
            Role role_erp_registration=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_registration, organization)
            //  Role role_volp=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Learner",true,rt_volp, organization)
            Role role_erp_accounts=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_accounts, organization)
            Role role_erp_tt=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_tt, organization)
            Role role_erp_facultyappriasal=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_facultyappriasal, organization)
            Role role_erp_examination=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_examination, organization)
            Role role_erp_cert=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_cert, organization)
            Role role_erp_asportal=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_asportal, organization)
            Role role_erp_quiz=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_quiz, organization)
            Role role_erp_online_examination=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_online_examination, organization)
            Role role_erp_academics=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("Student",true,rt_erp_academics, organization)

            Role role_erp_tpo=Role.findByRoleAndIsRoleSetAndRoletypeAndOrganization("TPO_STUDENT",true,rt_erp_tpo, organization)

            if(role_erp_admission)
                l.addToRoles(role_erp_admission)
            if(role_erp_registration)
                l.addToRoles(role_erp_registration)
//            if(role_volp)
//            l.addToRoles(role_volp)
            if(role_erp_accounts)
                l.addToRoles(role_erp_accounts)
            if(role_erp_tt)
                l.addToRoles(role_erp_tt)
            if(role_erp_facultyappriasal)
                l.addToRoles(role_erp_facultyappriasal)
            if(role_erp_examination)
                l.addToRoles(role_erp_examination)
            if(role_erp_cert)
                l.addToRoles(role_erp_cert)
            if(role_erp_asportal)
                l.addToRoles(role_erp_asportal)
            if(role_erp_quiz)
                l.addToRoles(role_erp_quiz)
            if(role_erp_online_examination)
                l.addToRoles(role_erp_online_examination)
            if(role_erp_academics)
                l.addToRoles(role_erp_academics)

            l.save(failOnError: true, flush: true)
            return true
        }
    }

    def offerlearnerdivision(def learner, def program, def academicyear, def semester, def year, def username, def ip){
        println("offerlearnerdivision : ")
        def allotdiv
        if(learner?.organization?.organization_code == 'VIT')
            allotdiv = getDivisionByRoundRobinVIT(learner, program, academicyear, semester, year)
        else
            allotdiv = getDivisionByRoundRobinVIIT(learner, program, academicyear, semester, year)

        if(allotdiv == -1){
            return -1
        }else {
            if (allotdiv) {
                def divoff
                if (program?.programtype?.name == "BTech" && year.year == "FY") {
                    def programlist = Program.findAllByProgramtypeAndOrganization(program?.programtype, learner.organization)
                    if (programlist)
                        divoff = DivisionOffering.findAllByIsregularAndAcademicyearAndSemesterAndYearAndShiftAndOrganizationAndProgramInList(true, academicyear, semester, year, learner.erpshift, learner.organization, programlist)
                } else {
                    divoff = DivisionOffering.findAllByIsregularAndAcademicyearAndSemesterAndYearAndProgramAndShiftAndOrganization(true, academicyear, semester, year, program, learner.erpshift, learner.organization)
                }
                def allotlearnerdivision
                if (divoff)
                    allotlearnerdivision = LearnerDivision.findByLearnerAndDivisionofferingInList(learner, divoff)
                if (!allotlearnerdivision) {
                    def rollno = LearnerDivision.createCriteria().list() {
                        projections {
                            max('rollno')
                        }
                        'in'('divisionoffering', allotdiv)
                        and {
                            eq('isdeleted', false)
                        }
                    }[0]

                    if (!rollno)
                        rollno = 1
                    else
                        rollno = rollno + 1
                    allotlearnerdivision = new LearnerDivision()
                    allotlearnerdivision.rollno = rollno
                    allotlearnerdivision.academicyear = academicyear
                    allotlearnerdivision.semester = semester
                    allotlearnerdivision.year = year
                    allotlearnerdivision.divisionoffering = allotdiv
                    allotlearnerdivision.rollnumbersuffix = allotdiv?.rollnumbersuffix
                    allotlearnerdivision.learner = learner
                    allotlearnerdivision.program = program
                    allotlearnerdivision.shift = learner?.erpshift
                    allotlearnerdivision.organization = learner?.organization

                    allotlearnerdivision.username = username
                    allotlearnerdivision.creation_date = new java.util.Date()
                    allotlearnerdivision.updation_date = new java.util.Date()
                    allotlearnerdivision.creation_ip_address = ip
                    allotlearnerdivision.updation_ip_address = ip
                    allotlearnerdivision.save(failOnError: true, flush: true)
                }

                def sub1
                def sub2
                if(allotlearnerdivision) {
                    sub1 = subject_registration_sem1(learner?.organization, learner, program, year, allotlearnerdivision, username, ip)
                    if(sub1 == -3)
                        return sub1

                    def allotlearnerdivisiontwo
                    if ((program?.programtype?.name == "BTech" && year.year == "FY" && learner?.organization?.organization_code == 'VIT') || (program?.programtype?.name == "BTech" && year.year == "SY" && learner?.organization?.organization_code == 'VIIT')) {
                        Semester semtwo = Semester.findBySemAndOrganization("2", learner?.organization)
                        def divoffsemtwo = DivisionOffering.findByIsregularAndDivisionAndAcademicyearAndSemesterAndYearAndProgramAndShiftAndOrganization(true, allotdiv.division, allotdiv.academicyear, semtwo, allotdiv.year, allotdiv.program, allotdiv.shift, allotdiv.organization)
                        if(divoffsemtwo) {
                            allotlearnerdivisiontwo = LearnerDivision.findByLearnerAndDivisionoffering(learner, divoffsemtwo)
                            if (allotlearnerdivisiontwo == null) {
                                allotlearnerdivisiontwo = new LearnerDivision()
                                allotlearnerdivisiontwo.rollno = allotlearnerdivision.rollno
                                allotlearnerdivisiontwo.academicyear = divoffsemtwo.academicyear
                                allotlearnerdivisiontwo.semester = semtwo
                                allotlearnerdivisiontwo.year = divoffsemtwo.year
                                allotlearnerdivisiontwo.learner = learner
                                allotlearnerdivisiontwo.program = learner?.program
                                allotlearnerdivisiontwo.shift = learner?.erpshift
                                allotlearnerdivisiontwo.divisionoffering = divoffsemtwo
                                allotlearnerdivisiontwo.rollnumbersuffix = divoffsemtwo?.rollnumbersuffix
                                allotlearnerdivisiontwo.organization = learner?.organization

                                allotlearnerdivisiontwo.username = username
                                allotlearnerdivisiontwo.creation_date = new java.util.Date()
                                allotlearnerdivisiontwo.updation_date = new java.util.Date()
                                allotlearnerdivisiontwo.creation_ip_address = ip
                                allotlearnerdivisiontwo.updation_ip_address = ip
                                allotlearnerdivisiontwo.save(failOnError: true, flush: true)
                            }
                            sub2 = subject_registration_sem2(learner?.organization, learner, program, year, allotlearnerdivisiontwo, username, ip)
                            if(sub2 == -4)
                                return sub2
                        }else
                            return -2
                    }
                }
                return allotlearnerdivision
            } else
                return 0
        }
    }

    def getDivisionByRoundRobinVIIT(def learner, def program, def academicyear, def semester, def year){
        println("getDivisionByRoundRobinVIIT")
        def division
        def divoff
        if(program?.programtype?.name == "BTech" && year.year == "FY") {
            def programlist = Program.findAllByProgramtypeAndOrganization(program?.programtype, learner.organization)
            if(programlist)
               divoff = DivisionOffering.findAllByIsregularAndAcademicyearAndSemesterAndYearAndOrganizationAndProgramInList(true, academicyear, semester, year, learner.organization, programlist)
        }else if(program?.programtype?.name == "BTech" && year.year == "SY") {
            divoff = DivisionOffering.findAllByIsregularAndAcademicyearAndSemesterAndYearAndProgramAndShiftAndOrganization(true, academicyear, semester, year, program, learner.erpshift, learner.organization)
        }else{
            divoff = DivisionOffering.findAllByIsregularAndAcademicyearAndSemesterAndYearAndProgramAndOrganization(true, academicyear, semester, year, program, learner.organization)
        }
        if(!divoff){
             return -1
        }else {
            divoff?.sort{it?.division?.name}
            if (divoff?.size() == 1) {
                def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[0], false).size()
                if (divoff[0]?.capacity > current)
                    division = divoff[0]
            } else {
                divoff.sort { it?.division?.name }
                def i = 0
                for (div in divoff) {
                    def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(div, false).size()

                    def k = i + 1
                    if (divoff.size() > k)
                        k = 0
                    def next = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[k], false).size()
                    def j = i - 1
                    if (j < 0)
                        j = divoff.size()
                    def previous = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[j], false).size()

                    i++
                    if (div?.capacity <= current) {
                        continue
                    } else if (current > next) {
                        continue
                    } else if (current == next && current < previous || current < next) {
                        division = div
                        break;
                    } else if (current == next == previous) {
                        division = div
                        break;
                    }
                }
                if (!division) {
                    def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[0], false).size()
                    if (divoff[0]?.capacity > current)
                        division = divoff[0]
                }
            }
            println("division : " + division)
            return division
        }
    }


    def getDivisionByRoundRobinVIT(def learner, def program, def academicyear, def semester, def year){
        println("getDivisionByRoundRobinVIT")
        def division
        def divoff
        if(program?.programtype?.name == "BTech" && year.year == "FY") {
            def programlist = Program.findAllByProgramtypeAndOrganization(program?.programtype, learner.organization)
            if(programlist)
                divoff = DivisionOffering.findAllByIsregularAndAcademicyearAndSemesterAndYearAndShiftAndOrganizationAndProgramInList(true, academicyear, semester, year, learner.erpshift, learner.organization, programlist)
        }else{
            divoff = DivisionOffering.findAllByIsregularAndAcademicyearAndSemesterAndYearAndProgramAndShiftAndOrganization(true, academicyear, semester, year, program, learner.erpshift, learner.organization)
        }

        if(!divoff){
            return -1
        }else {
            divoff?.sort{it?.division?.name}
            if (learner?.erpadmissionquota?.isForeignNational && program?.programtype?.name == "BTech" && year.year=="FY") {
                if (divoff?.size() == 1 && divoff[0]?.division?.name == 'N') {
                    def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[0], false).size()
                    if (divoff[0]?.capacity > current)
                        division = divoff[0]
                } else {
                    for (dv in divoff) {
                        if (dv?.division?.name == 'N') {
                            def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(dv, false).size()
                            if (dv?.capacity > current)
                                division = dv
                        }
                    }
                }
            } else {
                if (program?.programtype?.name == "BTech" && year.year=="FY") {
                    if (divoff?.size() == 1 && divoff[0]?.division?.name != 'N' && divoff[0]?.division?.name != 'K' && divoff[0]?.division?.name != 'L' && divoff[0]?.division?.name != 'M') {
                        def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[0], false).size()
                        if (divoff[0]?.capacity > current)
                            division = divoff[0]
                    } else if (learner?.erpshift?.type == "Second Shift" && (program?.abbrivation == 'B.Tech-Comp' || program?.abbrivation == 'B.Tech-Mech' || program?.abbrivation == 'B.Tech-ENTC')) {
                        def name
                        if (program?.abbrivation == 'B.Tech-Comp') {
                            name = 'K'
                        } else if (program?.abbrivation == 'B.Tech-Mech') {
                            name = 'M'
                        } else if (program?.abbrivation == 'B.Tech-ENTC') {
                            name = 'L'
                        }
                        for (dv in divoff) {
                            if (dv?.division?.name == name) {
                                def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(dv, false).size()
                                if (dv?.capacity > current)
                                    division = dv
                            }
                        }
                    } else {
                        divoff.sort { it?.division?.name }
                        def i = 0
                        for (div in divoff) {
                            if (div?.division?.name != 'N' && div?.division?.name != 'K' && div?.division?.name != 'L' && div?.division?.name != 'M') {
                                def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(div, false).size()

                                def k = i + 1
                                if (divoff.size() > k)
                                    k = 0
                                def next = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[k], false).size()
                                def j = i - 1
                                if (j < 0)
                                    j = divoff.size()
                                def previous = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[j], false).size()

                                i++
                                if (div?.capacity <= current) {
                                    continue
                                } else if (current > next) {
                                    continue
                                } else if (current == next && current < previous || current < next) {
                                    division = div
                                    break;
                                } else if (current == next == previous) {
                                    division = div
                                    break;
                                }
                            }
                        }
                        if (!division && divoff[0]?.division?.name != 'N' && divoff[0]?.division?.name != 'K' && divoff[0]?.division?.name != 'L' && divoff[0]?.division?.name != 'M') {
                            def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[0], false).size()
                            if (divoff[0]?.capacity > current)
                                division = divoff[0]
                        }
                    }
                }else{
                    if (divoff?.size() == 1) {
                        def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[0], false).size()
                        if (divoff[0]?.capacity > current)
                            division = divoff[0]
                    } else {
                        divoff.sort { it?.division?.name }
                        def i = 0
                        for (div in divoff) {
                            def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(div, false).size()

                            def k = i + 1
                            if (divoff.size() > k)
                                k = 0
                            def next = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[k], false).size()
                            def j = i - 1
                            if (j < 0)
                                j = divoff.size()
                            def previous = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[j], false).size()

                            i++
                            if (div?.capacity <= current) {
                                continue
                            } else if (current > next) {
                                continue
                            } else if (current == next && current < previous || current < next) {
                                division = div
                                break;
                            } else if (current == next == previous) {
                                division = div
                                break;
                            }
                        }
                        if (!division) {
                            def current = LearnerDivision.findAllByDivisionofferingAndIsdeleted(divoff[0], false).size()
                            if (divoff[0]?.capacity > current)
                                division = divoff[0]
                        }
                    }
                }
            }

            println("division : " + division)
            return division
        }
    }

    def subject_registration_sem1(def organization, def learner, def program, def year, def allotlearnerdivision, def username, def ip){
        def allotdiv = allotlearnerdivision?.divisionoffering

        ERPSubjectRegistrationType erpsubjectregistrationtype=ERPSubjectRegistrationType.findByTypeAndOrganization("auto",organization)
        ERPSubjectRegistrationConfiguration erpsubjectregistrationconfiguration=ERPSubjectRegistrationConfiguration.findByOrganizationAndErpsubjectregistrationtypeAndYearAndProgramtype(organization, erpsubjectregistrationtype, year, program.programtype)
        if(erpsubjectregistrationconfiguration!=null && program.programtype.name=="BTech" && year.year=="FY" && organization.organization_code=="VIT")
        {
            def divstrut = DivisionStructure.findByDivisionofferingAndOrganization(allotdiv, organization)
            if(!divstrut)
                return -3
            for(coff in divstrut.erpcourseoffering)
            {
                ERPExamConductType erpexamconducttype=ERPExamConductType.findByTypeAndOrganization("Regular", organization)
                ERPCourseOfferingLearner erpcourseofferinglearner = ERPCourseOfferingLearner.findByErpcourseofferingAndLearnerdivisionAndIscancelled(coff, allotlearnerdivision,false)
                
                if (erpcourseofferinglearner == null)
                {
                    erpcourseofferinglearner = new ERPCourseOfferingLearner()
                    erpcourseofferinglearner.isfeespaid=true
                    erpcourseofferinglearner.iscancelled=false
                    erpcourseofferinglearner.erpcourseoffering = coff
                    erpcourseofferinglearner.learnerdivision = allotlearnerdivision
                    erpcourseofferinglearner.erpexamconducttype = erpexamconducttype
                    erpcourseofferinglearner.erpcourseofferinglearner = null
                    erpcourseofferinglearner.program = program
                    erpcourseofferinglearner.academicyear = allotlearnerdivision.divisionoffering.academicyear
                    erpcourseofferinglearner.semester = allotlearnerdivision.divisionoffering.semester
                    erpcourseofferinglearner.module = allotlearnerdivision.divisionoffering.module
                    erpcourseofferinglearner.learner = learner
                    erpcourseofferinglearner.organization = organization

                    erpcourseofferinglearner.username = username
                    erpcourseofferinglearner.creation_date = new java.util.Date()
                    erpcourseofferinglearner.updation_date = new java.util.Date()
                    erpcourseofferinglearner.creation_ip_address = ip
                    erpcourseofferinglearner.updation_ip_address = ip
                    erpcourseofferinglearner.save(failOnError: true, flush: true)

                    allotdiv.hasjoined = true
                    allotdiv.joining_date = new java.util.Date()
                    allotdiv.username = username
                    allotdiv.updation_date = new java.util.Date()
                    allotdiv.updation_ip_address = ip
                    allotdiv.save(failOnError: true, flush: true)
                }
            }
        }else if(erpsubjectregistrationconfiguration!=null && year.year=="FY"){

            ERPExamConductType erpexamconducttype=ERPExamConductType.findByTypeAndOrganization("Regular",organization)

            ERPCourseOfferingLearner erpcourseofferinglearnerobj= ERPCourseOfferingLearner.findByErpexamconducttypeAndLearnerdivisionAndProgramAndAcademicyearAndSemesterAndLearnerAndOrganization(erpexamconducttype, allotlearnerdivision, program, allotdiv.academicyear, allotdiv.semester, learner, organization)

            if(!erpcourseofferinglearnerobj) {

                ArrayList subject_list = new ArrayList()
                def total_credits = 0

                def erpcourseoffering

                if (organization.organization_code == "VIIT" && program.programtype.name == "BTech" && year.year == "FY") {
                    def programVIIT = Program.findByNameAndOrganization("Engineering and Applied Science-Engineering and Applied Science", organization)
                    erpcourseoffering = ERPCourseOffering.findAllByYearAndProgramAndAcademicyearAndSemesterAndModuleAndOrganization(year, programVIIT, allotdiv.academicyear, allotdiv.semester, allotdiv.module, organization)
                } else {
                    erpcourseoffering = ERPCourseOffering.findAllByYearAndProgramAndAcademicyearAndSemesterAndModuleAndOrganization(year, program, allotdiv.academicyear, allotdiv.semester, allotdiv.module, organization)
                }

                for (ERPCourseOffering e : erpcourseoffering) {
                    if (e.templateoffering.programtype != learner.program.programtype)
                        continue
                    if (e.erpprogramgroup != null) {
                        if (learner.program.erpprogramgroup == e.erpprogramgroup) {
                            subject_list.add(e)
                            total_credits = total_credits + e.credit
                        }
                    } else {
                        subject_list.add(e)
                        total_credits = total_credits + e.credit
                    }
                }

                for (ERPCourseOffering e : subject_list) {
                    ERPCourseOfferingLearner erpcourseofferinglearner = ERPCourseOfferingLearner.findByErpcourseofferingAndLearnerdivisionAndIscancelled(e, allotlearnerdivision, false)

                    if (erpcourseofferinglearner == null) {
                        erpcourseofferinglearner = new ERPCourseOfferingLearner()
                        erpcourseofferinglearner.isfeespaid = true
                        erpcourseofferinglearner.iscancelled = false
                        erpcourseofferinglearner.erpcourseoffering = e
                        erpcourseofferinglearner.learnerdivision = allotlearnerdivision
                        erpcourseofferinglearner.erpexamconducttype = erpexamconducttype
                        erpcourseofferinglearner.erpcourseofferinglearner = null
                        erpcourseofferinglearner.program = program
                        erpcourseofferinglearner.academicyear = allotlearnerdivision.divisionoffering.academicyear
                        erpcourseofferinglearner.semester = allotlearnerdivision.divisionoffering.semester
                        erpcourseofferinglearner.module = allotlearnerdivision.divisionoffering.module
                        erpcourseofferinglearner.learner = learner
                        erpcourseofferinglearner.organization = organization

                        erpcourseofferinglearner.username = username
                        erpcourseofferinglearner.creation_date = new java.util.Date()
                        erpcourseofferinglearner.updation_date = new java.util.Date()
                        erpcourseofferinglearner.creation_ip_address = ip
                        erpcourseofferinglearner.updation_ip_address = ip
                        erpcourseofferinglearner.save(failOnError: true, flush: true)


                        allotdiv.hasjoined = true
                        allotdiv.joining_date = new java.util.Date()
                        allotdiv.username = username
                        allotdiv.updation_date = new java.util.Date()
                        allotdiv.updation_ip_address = ip
                        allotdiv.save(failOnError: true, flush: true)
                    }
                }
            }
        }
    }

    def subject_registration_sem2(def organization, def learner, def program, def year, def allotlearnerdivisiontwo, def username, def ip){
        def divoffsemtwo = allotlearnerdivisiontwo?.divisionoffering

        ERPSubjectRegistrationType erpsubjectregistrationtype=ERPSubjectRegistrationType.findByTypeAndOrganization("auto",organization)
        ERPSubjectRegistrationConfiguration erpsubjectregistrationconfiguration=ERPSubjectRegistrationConfiguration.findByOrganizationAndErpsubjectregistrationtypeAndYearAndProgramtype(organization, erpsubjectregistrationtype, year, program.programtype)
        if(erpsubjectregistrationconfiguration!=null && program.programtype.name=="BTech" && year.year=="FY" && organization.organization_code=="VIT")
        {
            def divstrut = DivisionStructure.findByDivisionofferingAndOrganization(divoffsemtwo, organization)
            if(!divstrut)
                return -4
            for(coff in divstrut.erpcourseoffering)
            {
                ERPExamConductType erpexamconducttype=ERPExamConductType.findByTypeAndOrganization("Regular", organization)
                ERPCourseOfferingLearner erpcourseofferinglearner = ERPCourseOfferingLearner.findByErpcourseofferingAndLearnerdivisionAndIscancelled(coff, allotlearnerdivisiontwo, false)
                if (erpcourseofferinglearner == null)
                {
                    erpcourseofferinglearner = new ERPCourseOfferingLearner()
                    erpcourseofferinglearner.isfeespaid=true
                    erpcourseofferinglearner.iscancelled=false
                    erpcourseofferinglearner.erpcourseoffering = coff
                    erpcourseofferinglearner.learnerdivision = allotlearnerdivisiontwo
                    erpcourseofferinglearner.erpexamconducttype=erpexamconducttype
                    erpcourseofferinglearner.erpcourseofferinglearner=null
                    erpcourseofferinglearner.program = program
                    erpcourseofferinglearner.academicyear = allotlearnerdivisiontwo.divisionoffering.academicyear
                    erpcourseofferinglearner.semester = allotlearnerdivisiontwo.divisionoffering.semester
                    erpcourseofferinglearner.module = allotlearnerdivisiontwo.divisionoffering.module
                    erpcourseofferinglearner.learner = learner
                    erpcourseofferinglearner.organization = organization

                    erpcourseofferinglearner.username = username
                    erpcourseofferinglearner.creation_date = new java.util.Date()
                    erpcourseofferinglearner.updation_date = new java.util.Date()
                    erpcourseofferinglearner.creation_ip_address = ip
                    erpcourseofferinglearner.updation_ip_address = ip
                    erpcourseofferinglearner.save(failOnError: true, flush: true)

                    divoffsemtwo.hasjoined = true
                    divoffsemtwo.joining_date = new java.util.Date()
                    divoffsemtwo.username = username
                    divoffsemtwo.updation_date = new java.util.Date()
                    divoffsemtwo.updation_ip_address = ip
                    divoffsemtwo.save(failOnError: true, flush: true)
                }
            }
        }else if(erpsubjectregistrationconfiguration!=null && year.year=="FY"){

            ERPExamConductType erpexamconducttype=ERPExamConductType.findByTypeAndOrganization("Regular",organization)

            ERPCourseOfferingLearner erpcourseofferinglearnerobj= ERPCourseOfferingLearner.findByErpexamconducttypeAndLearnerdivisionAndProgramAndAcademicyearAndSemesterAndLearnerAndOrganization(erpexamconducttype, allotlearnerdivisiontwo, program, divoffsemtwo.academicyear, divoffsemtwo.semester, learner, organization)

            if(!erpcourseofferinglearnerobj) {

                ArrayList subject_list = new ArrayList()
                def total_credits = 0

                def erpcourseoffering

                if (organization.organization_code == "VIIT" && program.programtype.name == "BTech" && year.year == "FY") {
                    def programVIIT = Program.findByNameAndOrganization("Engineering and Applied Science-Engineering and Applied Science", organization)
                    erpcourseoffering = ERPCourseOffering.findAllByYearAndProgramAndAcademicyearAndSemesterAndModuleAndOrganization(year, programVIIT, divoffsemtwo.academicyear, divoffsemtwo.semester, divoffsemtwo.module, organization)
                } else {
                    erpcourseoffering = ERPCourseOffering.findAllByYearAndProgramAndAcademicyearAndSemesterAndModuleAndOrganization(year, program, divoffsemtwo.academicyear, divoffsemtwo.semester, divoffsemtwo.module, organization)
                }

                for (ERPCourseOffering e : erpcourseoffering) {
                    if (e.templateoffering.programtype != learner.program.programtype)
                        continue
                    if (e.erpprogramgroup != null) {
                        if (learner.program.erpprogramgroup == e.erpprogramgroup) {
                            subject_list.add(e)
                            total_credits = total_credits + e.credit
                        }
                    } else {
                        subject_list.add(e)
                        total_credits = total_credits + e.credit
                    }
                }

                for (ERPCourseOffering e : subject_list) {
                    ERPCourseOfferingLearner erpcourseofferinglearner = ERPCourseOfferingLearner.findByErpcourseofferingAndLearnerdivisionAndIscancelled(e, allotlearnerdivisiontwo, false)

                    if (erpcourseofferinglearner == null) {
                        erpcourseofferinglearner = new ERPCourseOfferingLearner()
                        erpcourseofferinglearner.isfeespaid = true
                        erpcourseofferinglearner.iscancelled = false
                        erpcourseofferinglearner.erpcourseoffering = e
                        erpcourseofferinglearner.learnerdivision = allotlearnerdivisiontwo
                        erpcourseofferinglearner.erpexamconducttype = erpexamconducttype
                        erpcourseofferinglearner.erpcourseofferinglearner = null
                        erpcourseofferinglearner.program = program
                        erpcourseofferinglearner.academicyear = allotlearnerdivisiontwo.divisionoffering.academicyear
                        erpcourseofferinglearner.semester = allotlearnerdivisiontwo.divisionoffering.semester
                        erpcourseofferinglearner.module = allotlearnerdivisiontwo.divisionoffering.module
                        erpcourseofferinglearner.learner = learner
                        erpcourseofferinglearner.organization = organization

                        erpcourseofferinglearner.username = username
                        erpcourseofferinglearner.creation_date = new java.util.Date()
                        erpcourseofferinglearner.updation_date = new java.util.Date()
                        erpcourseofferinglearner.creation_ip_address = ip
                        erpcourseofferinglearner.updation_ip_address = ip
                        erpcourseofferinglearner.save(failOnError: true, flush: true)

                        divoffsemtwo.hasjoined = true
                        divoffsemtwo.joining_date = new java.util.Date()
                        divoffsemtwo.username = username
                        divoffsemtwo.updation_date = new java.util.Date()
                        divoffsemtwo.updation_ip_address = ip
                        divoffsemtwo.save(failOnError: true, flush: true)
                    }
                }
            }
        }
    }

    def setFeesCategoryLinking(Learner learner, def category, def ay, def year, def program, String user, String ip){

        println("setFeesCategoryLinking : ")
        AccountsService accounts_service = new AccountsService()

        ERPStudentFeesCategoryLinking link = ERPStudentFeesCategoryLinking.findByLearnerAndAcademicyear(learner, ay)
        ERPStudentFeesCategory erpStudentFeesCategory = link?.erpstudentfeescategory
        if(!link){
            ERPStudentAdmissionFeesCategoryLinking admissioncate = ERPStudentAdmissionFeesCategoryLinking.findByYearAndProgramtypeAndOrganizationAndErpstudentadmissionmaincategory(year, program?.programtype, learner?.organization, category)
            if(admissioncate?.erpstudentfeescategory) {
                link = new ERPStudentFeesCategoryLinking()
                link.learner = learner
                link.organization = learner?.organization
                link.erpstudentfeescategory = admissioncate?.erpstudentfeescategory
                link.erpscholarshiptype = learner?.erpscholarshiptype
                link.academicyear = ay
                link.year = year
//                link.programtype = program?.programtype
                link.username = user
                link.creation_date = new Date()
                link.updation_date = new Date()
                link.creation_ip_address = ip
                link.updation_ip_address = ip
                link.save(flush: true, failOnError: true)
            }
            erpStudentFeesCategory = link?.erpstudentfeescategory
        }

        if(link) {
            def isforeignstudent = false
            if (learner.erpadmissionquota.isForeignNational) {
                isforeignstudent = true
            }

            if (!isforeignstudent) {
                ERPFeeStudentMaster erpFeeStudentMaster = ERPFeeStudentMaster.findByLearnerAndErpstudentfeescategorylinking(learner, link)
                if (erpFeeStudentMaster) {
                    def feesStructureMaster = accounts_service.getFeesMasterbyDepositetype(learner.admissionyear,
                            erpFeeStudentMaster.erpfeesstructuremaster.organization,
                            erpStudentFeesCategory,
                            erpFeeStudentMaster.erpfeesstructuremaster.feesprogramtype,
                            erpFeeStudentMaster.erpfeesstructuremaster.year,
                            ay,
                            program,
                            erpFeeStudentMaster.erpfeesstructuremaster.feescurrencytype,
                            erpFeeStudentMaster.erpfeesstructuremaster.erpfeesdepositetype,
                            erpFeeStudentMaster.erpfeesstructuremaster.erpstudenttype,
                            erpFeeStudentMaster.erpfeesstructuremaster.erpcountry)

                    if (feesStructureMaster) {
                        erpFeeStudentMaster.erpfeesstructuremaster = feesStructureMaster
                        erpFeeStudentMaster.username = user
                        erpFeeStudentMaster.updation_date = new Date()
                        erpFeeStudentMaster.updation_ip_address = ip
                    }
                } else {
                    FeesCurrencyType feescurrancytype = FeesCurrencyType.findByType("RUPEES")
                    ERPFeesDepositeType erpfeesdepositetype = ERPFeesDepositeType.findByType("Addmission Fees")
                    FeesProgramType feesprogramtype = FeesProgramType.findByType(program?.programtype.name)
                    ERPStudentType erpstudenttype = ERPStudentType.findByType("Regular")
                    def feesStructureMaster = accounts_service.getFeesMasterbyDepositetypewithoutCountry(learner.admissionyear,
                            learner.organization,
                            erpStudentFeesCategory,
                            feesprogramtype,
                            year,
                            ay,
                            program,
                            feescurrancytype,
                            erpfeesdepositetype,
                            erpstudenttype)

                    if(feesStructureMaster) {
                        erpFeeStudentMaster = new ERPFeeStudentMaster()
                        erpFeeStudentMaster.paid_till_now = 0.0
                        erpFeeStudentMaster.from_social_welfare = 0.0
                        erpFeeStudentMaster.fees_receivable_due = 0.0
                        erpFeeStudentMaster.fees_receivable_paid = 0.0
                        erpFeeStudentMaster.learner = learner
                        erpFeeStudentMaster.erpstudentfeescategorylinking = link
                        erpFeeStudentMaster.erpfeesstructuremaster = feesStructureMaster
                        erpFeeStudentMaster.organization = learner.organization
                        erpFeeStudentMaster.username = user
                        erpFeeStudentMaster.creation_date = new Date()
                        erpFeeStudentMaster.updation_date = new Date()
                        erpFeeStudentMaster.creation_ip_address = ip
                        erpFeeStudentMaster.updation_ip_address = ip
                        erpFeeStudentMaster.save( flush: true, failOnError: true )
                    }
                }
            } else {
                ERPForeignNationalFeesStudentMaster student = new ERPForeignNationalFeesStudentMaster()
                FeesCurrencyType feescurrencytype = FeesCurrencyType.findByType('RUPEES')
                ERPFeesDepositeType erpfeesdepositetype = ERPFeesDepositeType.findByType("Addmission Fees")
                ERPStudentType erpstudenttype = ERPStudentType.findByType('Regular')
                student.total_fees = 0.0
                student.paid_til_now = 0.0
                student.username = user
                student.creation_date = new java.util.Date()
                student.updation_date = new java.util.Date()
                student.creation_ip_address = ip
                student.updation_ip_address = ip
                student.feescurrencytype = feescurrencytype
                student.erpstudentfeescategorylinking = link
                student.learner = learner
                student.organization = learner?.organization
                student.financialyear = ay
                student.erpfeesdepositetype = erpfeesdepositetype
                student.erpstudentfeescategory = erpStudentFeesCategory
                def feesprogramtype = FeesProgramType.list()
                for (prog in feesprogramtype) {
                    String ftype = prog.type.replace(".", "")
                    if (ftype == learner.program.programtype.name) {
                        student.feesprogramtype = prog
                        break
                    }
                }
                student.year = year
                student.erpstudenttype = erpstudenttype
                student.erpcountry = learner?.erpnationality
                student.save(flush: true, failOnError: true)

            }
        }
    }

    def marklearner_dte_doc(Learner learner, def admission_applicant, String user, String ip){
        def documents = []
        def doc = ERPDocuments.findAllByAdmissionapplicantAndOrganization(admission_applicant, admission_applicant?.organization)
        if(doc)
            documents.addAll(doc)
        doc = ERPDocuments.findAllByLearnerAndOrganization(learner, learner?.organization)
        documents.addAll(doc)

        for(docs in documents){
            docs.learner = learner
            docs.admissionapplicant = admission_applicant
            docs.username = user
            docs.updation_date = new java.util.Date()
            docs.updation_ip_address = ip
            docs.save(failOnError: true, flush: true)
        }
    }
}
