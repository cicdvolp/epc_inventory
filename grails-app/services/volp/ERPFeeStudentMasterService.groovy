package volp

import grails.gorm.transactions.Transactional

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.temporal.ChronoField

@Transactional
class ERPFeeStudentMasterService {

    def learnerPartialfeesPaidChecking(Learner learner, Organization organization, AcademicYear ay, Year year){
        def erpstudentfeescategorylinking = ERPStudentFeesCategoryLinking.findByLearnerAndYearAndAcademicyearAndOrganization(learner, year, ay, organization)
        def  erpfeestudentmaster
        if(!learner?.erpadmissionquota?.isForeignNational){

            erpfeestudentmaster = ERPFeeStudentMaster.findByLearnerAndErpstudentfeescategorylinking(learner, erpstudentfeescategorylinking)
            def paid_till_now = 0
            if(erpfeestudentmaster)
                paid_till_now = get_paid_fees_total_from_receipt(erpfeestudentmaster, learner, false, null)
            if (paid_till_now > 0)
                return 1
            else
                return 0
        }else{
            def feesprogramtype = FeesProgramType.findByTypeIlike(learner?.program?.programtype?.name)
            erpfeestudentmaster = ERPForeignNationalFeesStudentMaster.findByLearnerAndOrganizationAndErpstudentfeescategorylinkingAndErpstudentfeescategoryAndFeesprogramtypeAndYearAndFinancialyear(learner, organization, erpstudentfeescategorylinking, erpstudentfeescategorylinking?.erpstudentfeescategory, feesprogramtype, year, ay)
            if(erpfeestudentmaster) {
                def paid_till_now = get_paid_fees_total_from_receipt(null, learner, true, erpfeestudentmaster)
                if (paid_till_now > 0)
                    return 1
                else
                    return 0
            }
        }
        return 0
    }

    def learnerFullfeesPaidChecking(Learner learner, Organization organization, AcademicYear ay, Year year){

        def erpstudentfeescategorylinking = ERPStudentFeesCategoryLinking.findByLearnerAndYearAndAcademicyearAndOrganization(learner, year, ay, organization)

        def erpfeestudentmaster
        if(!learner?.erpadmissionquota?.isForeignNational){
            erpfeestudentmaster = ERPFeeStudentMaster.findByLearnerAndErpstudentfeescategorylinking(learner, erpstudentfeescategorylinking)

            def feedue = 0
            def erpfeesstructuremaster = erpfeestudentmaster?.erpfeesstructuremaster
            if (erpfeesstructuremaster) {
                def feesamount = erpfeesstructuremaster?.total_fees_amount - erpfeesstructuremaster?.fees_receivable_from_sw - erpfeestudentmaster?.feesconcession

                def paid_till_now = 0
                if(erpfeestudentmaster)
                    paid_till_now = get_paid_fees_total_from_receipt(erpfeestudentmaster, learner, false, null)

//                def totpalpaidamt = erpfeestudentmaster?.paid_till_now + erpfeestudentmaster?.fees_receivable_paid
                def totalfee = feesamount + erpfeestudentmaster?.fees_receivable_due
                if (paid_till_now >= totalfee)
                    return 1
                else
                    return 0
            }
        }else{
            def feesprogramtype = FeesProgramType.findByTypeIlike(learner?.program?.programtype?.name)
            erpfeestudentmaster = ERPForeignNationalFeesStudentMaster.findByLearnerAndOrganizationAndErpstudentfeescategorylinkingAndErpstudentfeescategoryAndFeesprogramtypeAndYearAndFinancialyear(learner, organization, erpstudentfeescategorylinking, erpstudentfeescategorylinking?.erpstudentfeescategory, feesprogramtype, year, ay)
            if(erpfeestudentmaster) {
                def feesamount = erpfeestudentmaster?.total_fees - erpfeestudentmaster?.feesconcession
                def paid_till_now = get_paid_fees_total_from_receipt(null, learner, true, erpfeestudentmaster)

//                def totpalpaidamt = erpfeestudentmaster?.paid_til_now + erpfeestudentmaster?.fees_receivable_paid
                def totalfee = feesamount + erpfeestudentmaster?.fees_receivable_due
                if (paid_till_now >= totalfee)
                    return 1
                else
                    return 0
            }
        }
        return 0
    }

    def get_sw_fees_total_from_receipt(ERPFeeStudentMaster erpfeestudentmaster, Learner learner){
        def amount = ERPSocialWelfareReceipt.createCriteria().list(){
            projections{
                sum('amount')
            }
            'in'('erpfeestudentmaster', erpfeestudentmaster)
            and{
                'in'('learner', learner)
                'in'('organization', learner.organization)
                eq('iscancelled', false)
            }
        }
        if(amount[0]!= null)
            amount = amount[0]
        else
            amount = 0

        return amount
    }

    def get_paid_fees_total_from_receipt(ERPFeeStudentMaster erpfeestudentmaster, Learner learner, boolean foreign, ERPForeignNationalFeesStudentMaster erpforeignnationalfeesstudentmaster){
        def amount
        def refundamount
        if(foreign){
            amount = ERPStudentReceipt.createCriteria().list(){
               projections{
                   sum('amount')
               }
               'in'('erpforeignnationalfeesstudentmaster', erpforeignnationalfeesstudentmaster)
               and{
                   'in'('learner', learner)
                   'in'('organization', learner.organization)
                   eq('iscancelled', false)
               }
            }

            refundamount = ERPRefundReceipt.createCriteria().list(){
               projections{
                   sum('amount')
               }
               'in'('erpforeignnationalfeesstudentmaster', erpforeignnationalfeesstudentmaster)
               and{
                   'in'('learner', learner)
                   'in'('organization', learner.organization)
               }
           }
        }else{
            amount = ERPStudentReceipt.createCriteria().list(){
               projections{
                   sum('amount')
               }
               'in'('erpfeestudentmaster', erpfeestudentmaster)
               and{
                   'in'('learner', learner)
                   'in'('organization', learner.organization)
                   eq('iscancelled', false)
               }
            }

            refundamount = ERPRefundReceipt.createCriteria().list(){
               projections{
                   sum('amount')
               }
               'in'('erpfeestudentmaster', erpfeestudentmaster)
               and{
                   'in'('learner', learner)
                   'in'('organization', learner.organization)
               }
           }
        }

        if(amount[0]!= null)
            amount = amount[0]
        else
            amount = 0

        if(refundamount[0]!= null)
            refundamount = refundamount[0]
        else
            refundamount = 0

        return amount-refundamount
    }

    def get_paid_fees_total_from_receipt_hostel(HostelFeeStudentMaster hostelfeestudentmaster, Learner learner){
        println("get_paid_fees_total_from_receipt_hostel")

        def receipt = HostelStudentReceipt.createCriteria().list(){
           'in'('hostelfeestudentmaster', hostelfeestudentmaster)
           and{
               'in'('learner', learner)
           }
        }

        def status = HostelFeesStatus.findByNameIlikeAndOrganizationgroup("approved", learner?.organization?.organizationgroup)

        def amount
        if(receipt) {
            amount = HostelReceiptTransaction.createCriteria().list() {
                projections{
                    sum('amount')
                }
                'in'('learner', learner)
                and{
                    'in'('hostelfeestudentmaster', hostelfeestudentmaster)
                    'in'('hostelstudentreceipt', receipt)
                    'in'('hostelfeesstatus', status)
                }
            }[0]
        }

        if (amount == 'null' || amount == null)
            amount = 0

//            def refundamount
//            refundamount = ERPRefundReceipt.createCriteria().list(){
//               projections{
//                   sum('amount')
//               }
//               'in'('erpfeestudentmaster', erpfeestudentmaster)
//               and{
//                   'in'('learner', learner)
//                   'in'('organization', learner.organization)
//               }
//           }


//        if(refundamount[0]!= null)
//            refundamount = refundamount[0]
//        else
//            refundamount = 0

//        return amount-refundamount

        if(amount)
            amount?.round( 2 )
        return amount
    }

    def get_receivable_paid_fees_total_from_receipt(ERPFeeStudentMaster erpfeestudentmaster, Learner learner, boolean foreign, ERPForeignNationalFeesStudentMaster erpforeignnationalfeesstudentmaster){
        def amount
        if(foreign){
            amount = ERPStudentReceipt.createCriteria().list(){
                projections{
                    sum('fees_receivable_paid')
                }
                'in'('erpforeignnationalfeesstudentmaster', erpforeignnationalfeesstudentmaster)
                and{
                    'in'('learner', learner)
                    'in'('organization', learner.organization)
                    eq('iscancelled', false)
                }
            }
        }else{
            amount = ERPStudentReceipt.createCriteria().list(){
                projections{
                    sum('fees_receivable_paid')
                }
                'in'('erpfeestudentmaster', erpfeestudentmaster)
                and{
                    'in'('learner', learner)
                    'in'('organization', learner.organization)
                    eq('iscancelled', false)
                }
            }
        }

        if(amount[0]!= null)
            amount = amount[0]
        else
            amount = 0

        return amount
    }


//    def checkaccesstolink(String rolename, String actionname, String controllername, Double loginid, Double orgid){
//        Login login = Login.findById(loginid)
//        Organization organization=Organization.findById(orgid)
//
//        def roles=login.roles
//
//        ArrayList rolelinkslist=new ArrayList<RoleLink>()
//        for(Role r:roles)
//        {
//            if(r.roletype.type==rolename)
//            {
//                def rolelinks = RoleLink.findAllByRoleAndOrganization(r,organization)
//                for(RoleLink rl:rolelinks)
//                {
//                    rolelinkslist.add(rl)
//                }
//            }
//        }
//        if(roles.roletype.type.contains(rolename)){
//            if(rolelinkslist?.controller_name.contains(controllername) && rolelinkslist?.action_name.contains(actionname)){
//                for(s in rolelinkslist){
//                    if(s.controller_name == controllername && s.action_name == actionname && s.isrolelinkactive)
//                        return 1
//                }
//            }
//        }
//        return 0
//    }


    def checkaccesstolink(String actionname, String controllername, Double loginid, Double orgid){
        Login login = Login.findById(loginid)
        Organization organization=Organization.findById(orgid)

        def roles=login.roles

        def rolelist = RoleLink.findAllByOrganizationAndController_nameAndAction_nameAndIsrolelinkactive(organization, controllername, actionname, true)
        if(roles) {
            for (r in rolelist) {
                if (roles.id.contains(r.role.id)) {
                    return 1
                }
            }
        }
        return 0
    }

    def checkaccesstolinklearner(String pagename, Double loginid, Double orgid){
        Login login = Login.findById(loginid)
        Organization organization=Organization.findById(orgid)

        def roles=login.roles

        def rolelist = RoleLink.findAllByOrganizationAndvue_js_nameAndIsrolelinkactive(organization, pagename, true)
        if(roles) {
            for (r in rolelist) {
                if (roles.id.contains(r.role.id)) {
                    return 1
                }
            }
        }
        return 0
    }

}