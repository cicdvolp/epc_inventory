package volp

import grails.gorm.transactions.Transactional

@Transactional
class RoleLinksService {
    def serviceMethod() {

    }

    /*RoleLink  get(Serializable id) {}

    List<RoleLink> list(Map args) {}

    Long count() {}

    void delete(Serializable id) {}*/

    void addRoleLink(RoleLink roleLinks) {
        RoleLink roleLinksObj = new RoleLink()
        roleLinksObj.controller_name = roleLinks.controller_name
        roleLinksObj.action_name = roleLinks.action_name
        roleLinksObj.link_name = roleLinks.link_name
        roleLinksObj.link_displayname = roleLinks.link_displayname
        roleLinksObj.link_description = roleLinks.link_description
        roleLinksObj.sort_order = roleLinks.sort_order
        roleLinksObj.role = roleLinks.role
        roleLinksObj.vue_js_name = roleLinks.vue_js_name
        roleLinksObj.organization = roleLinks.organization
//        roleLinksObj.softwaremodule = roleLinks.softwaremodule
//        roleLinksObj.linktype = roleLinks.linktype
//        roleLinksObj.roletype = roleLinks.roletype
//        roleLinksObj.usertype = roleLinks.usertype
        roleLinksObj.isrolelinkactive = roleLinks.isrolelinkactive
        roleLinksObj.isquicklink = roleLinks.isquicklink
        roleLinksObj.save(failOnError:true,flush:true)
    }

    List<RoleLink> getAllRoleLinks(Organization org) {
        List<RoleLink> roleLinksList = RoleLink.findAllByOrganization(org);
        return roleLinksList;
    }

    List<RoleType> getAllRoleType(ApplicationType appType, Organization org) {
        List<RoleType> roleTypeList = RoleType.findAllByApplicationtypeAndOrganizationAndIsactive(appType, org, true);
        return roleTypeList
    }

    List<Role> getAllRoles() {
        List<Role> roleList = Role.findAll();
        return roleList
    }

    List<UserType> getAllUserType(ApplicationType appType, Organization org) {
        List<UserType> userTypeList = UserType.findAllByApplication_typeAndOrganization(appType, org)
        return userTypeList
    }

    List<SoftwareModule> getAllSoftwareModule(Organization org) {
        List<SoftwareModule> softwareModuleList = SoftwareModule.findAllByOrganization(org);
        return softwareModuleList
    }

    List<LinkType> getAllLinkType(Organization org) {
        List<LinkType> linkTypeList = LinkType.findAllByOrganization(org);
        return linkTypeList
    }

    void deleteRoleLinks(RoleLink roleLinks){
       roleLinks.delete(failOnError:true,flush:true)
       return
    }

    void activeRoleLniks(RoleLink roleLinks) {
        if(roleLinks.isrolelinkactive == true){
            roleLinks.isrolelinkactive = false
        } else {
            roleLinks.isrolelinkactive = true
        }
        roleLinks.save(failOnError:true, flush:true)
        return
    }

    // PPS - 24-10-2019
    void setQuickLniks(RoleLink roleLinks) {
        if(roleLinks.isquicklink == true){
            roleLinks.isquicklink = false
        } else {
            roleLinks.isquicklink = true
        }
        roleLinks.save(failOnError:true, flush:true)
        return
    }

    List<Role> getRoleByRoleTypeAndUserType(RoleType roleType, UserType userType, Organization org) {
        List<Role> roleList = Role.findAllByRoletypeAndUsertypeAndOrganization(roleType, userType, org)
        return roleList
    }

    List<RoleLink>  getLinkListByRole(Role role, Organization org) {
        List<RoleLink> roleLinksList = RoleLink.findAllByOrganizationAndRole( org, role)
        return roleLinksList
    }

    void addRole(String role, String username ,RoleType roleType, UserType userType, String ipAddr ){
        Role roleObj = new Role()
        roleObj.role = role
        roleObj.username = username
        roleObj.roletype = roleType
        roleObj.usertype = userType
        roleObj.creation_date = new java.util.Date()
        roleObj.updation_date =new java.util.Date()
        roleObj.creation_ip_address=ipAddr
        roleObj.updation_ip_address=ipAddr
        roleObj.isRoleSet=true
        roleObj.save(failOnError:true,flush:true)
        return
    }

    void addLinkType(String type, String username, Organization org, String ipAddr  ) {
        LinkType linkType = new LinkType()
        linkType.type = type
        linkType.username = username
        linkType.creation_date = new java.util.Date()
        linkType.updation_date = new java.util.Date()
        linkType.creation_ip_address = ipAddr
        linkType.updation_ip_address = ipAddr
        linkType.organization = org
        linkType.save(failOnError:true,flush:true)
        return
    }

    void addSoftwareModule(String name, String username, Organization org, String ipAddr  ) {
        SoftwareModule softwareModule = new SoftwareModule()
        softwareModule.name = name
        softwareModule.username = username
        softwareModule.creation_date = new java.util.Date()
        softwareModule.updation_date = new java.util.Date()
        softwareModule.creation_ip_address = ipAddr
        softwareModule.updation_ip_address = ipAddr
        softwareModule.organization = org
        softwareModule.save(failOnError:true,flush:true)
        return
    }
}