package volp

import grails.gorm.transactions.Transactional

@Transactional
class NumberToWordService {

    def serviceMethod() {

    }
    public static String[] units = ["", "One", "Two", "Three", "Four",
                                    "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve",
                                    "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen",
                                    "Eighteen", "Nineteen" ];

    public static String[] tens = [
            "", 		// 0
            "",		// 1
            "Twenty", 	// 2
            "Thirty", 	// 3
            "Forty", 	// 4
            "Fifty", 	// 5
            "Sixty", 	// 6
            "Seventy",	// 7
            "Eighty", 	// 8
            "Ninety" 	// 9
    ]

    public static String convert(int n)
    {
        // ////println("Enter a number to convert into word format"+n.getClass().getName());
        if (n < 0) {
            return "Minus " + convert(-n);
        }

        if (n < 20) {
            return units[n];
        }

        if (n < 100) {
            return tens[(int)(n / 10)] + ((n % 10 != 0) ? " " : "") + units[(int)(n % 10)];
        }

        if (n < 1000) {
            return units[(int)(n / 100)] + " Hundred" + ((n % 100 != 0) ? " " : "") + convert((int)(n % 100));
        }

        if (n < 100000) {
            return convert((int)(n / 1000)) + " Thousand" + ((n % 10000 != 0) ? " " : "") + convert((int)(n % 1000));
        }

        if (n < 10000000) {
            return convert((int)(n / 100000)) + " Lakh" + (((int)(n % 100000) != 0) ? " " : "") + convert((int)(n % 100000));
        }

        return convert((int)(n / 10000000)) + " Crore" + (((int)(n % 10000000) != 0) ? " " : "") + convert((int)(n % 10000000));
    }
}
