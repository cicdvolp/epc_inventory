package volp

import grails.gorm.transactions.Transactional

class ReceiptNumberGeneratorService {

    String getReceiptNumber(long orgid,String AY,String year,String Progtype ) {
        println( "getReceiptNumber : " )
        //println("year...:"+year)
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        //println("yr:"+yr)
        //def ay=ReceiptNumberTracking.findByYearAndOrganization(yr[0],org)

        def ay = ReceiptNumberTracking.findByOrganizationAndIsactive(org, true)
//        AY = ay?.academicyear?.ay
        String[] yr = AY.split("-")
        //println("AY:"+AY)

        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber
        if(no<10) {
            tno="000"+no
        } else if(no<100) {
            tno="00"+no
        } else if(no<1000) {
            tno="0"+no
        } else {
            tno=no
        }
        if(ay==null) {
            return "Please verify Organization code or Academic year"
        } else {
            if(Progtype=="MCA") {
                receiptNumber=code.institute_code+"MCA-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno
            } else if(Progtype=="MTech") {
                receiptNumber = code.institute_code + "MTECH-" + year[0] + "-" + yr[0][2] + yr[0][3] + tno
            } else {
                receiptNumber = code.institute_code + year[0] + "-" + yr[0][2] + yr[0][3] + tno
            }
            return receiptNumber
        }
    }


    String getReceiptNumberCurrentYear(long orgid,String AY,String year,String Progtype ) {
        println( "getReceiptNumber : " )
        //println("year...:"+year)
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        //println("yr:"+yr)
        //def ay=ReceiptNumberTracking.findByYearAndOrganization(yr[0],org)

        def ay = ReceiptNumberTracking.findByOrganizationAndIsactive(org, true)
        AY = ay?.academicyear?.ay
        String[] yr = AY.split("-")
        //println("AY:"+AY)

        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber
        if(no<10) {
            tno="000"+no
        } else if(no<100) {
            tno="00"+no
        } else if(no<1000) {
            tno="0"+no
        } else {
            tno=no
        }
        if(ay==null) {
            return "Please verify Organization code or Academic year"
        } else {
            if(Progtype=="MCA") {
                receiptNumber=code.institute_code+"MCA-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno
            } else if(Progtype=="MTech") {
                receiptNumber = code.institute_code + "MTECH-" + year[0] + "-" + yr[0][2] + yr[0][3] + tno
            } else {
                receiptNumber = code.institute_code + year[0] + "-" + yr[0][2] + yr[0][3] + tno
            }
            return receiptNumber
        }
    }


    String getReceiptNumberPreviousYears(long orgid, def academicyear,String year,String Progtype ) {
        println( "getReceiptNumberPreviousYears : " )
        //println("year...:"+year)
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)

        def AY = academicyear?.ay

        String[] yr = AY.split("-")

        def ay = ReceiptNumberTracking.findByOrganizationAndAcademicyear(org, academicyear)

        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber
        if(no<10) {
            tno="000"+no
        } else if(no<100) {
            tno="00"+no
        } else if(no<1000) {
            tno="0"+no
        } else {
            tno=no
        }
        if(ay==null) {
            return "Please verify Organization code or Academic year"
        } else {
            if(Progtype=="MCA") {
                receiptNumber=code.institute_code+"MCA-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno
            } else if(Progtype=="MTech") {
                receiptNumber = code.institute_code + "MTECH-" + year[0] + "-" + yr[0][2] + yr[0][3] + tno
            } else {
                receiptNumber = code.institute_code + year[0] + "-" + yr[0][2] + yr[0][3] + tno
            }
            return receiptNumber
        }
    }


    String getDummyReceiptNumber(long orgid,String AY,String year,String Progtype ) {
        println("getDummyReceiptNumber : ")
        //println("year...:"+year)
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        String[] yr = AY.split("-")
        //println("AY:"+AY)
        //println("yr:"+yr)
        //def ay=DummyReceiptNumberTracking.findByYearAndOrganization(yr[0],org)
        def ay = DummyReceiptNumberTracking.findByOrganizationAndIsactive(org, true)
        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber
        if(no<10) {
            tno="000"+no
        } else if(no<100) {
            tno="00"+no
        } else if(no<1000) {
            tno="0"+no
        } else {
            tno=no
        }
        if(ay==null) {
            return "Please verify Organization code or Academic year"
        } else {
            if(Progtype=="MCA") {
                receiptNumber=code.institute_code+"MCA-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno
            } else if(Progtype=="MTech") {
                receiptNumber = code.institute_code + "MTECH-" + year[0] + "-" + yr[0][2] + yr[0][3] + tno
            } else {
                receiptNumber = code.institute_code + year[0] + "-" + yr[0][2] + yr[0][3] + tno
            }
            return receiptNumber
        }
    }

    String getRefundReceiptNumber(long orgid,String AY,String year,String Progtype ) {
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        String[] yr = AY.split("-")
        ////println("yr:"+yr)
        def ay=RefundReceiptNumberTracking.findByYearAndOrganization(yr[0],org)
        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber

        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        if(ay==null)
            return "Please verify Organization code or Academic year"
        else
        {
            if(Progtype=="MCA")
            {receiptNumber=code.institute_code+"R-MCA-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno}
            else if(Progtype=="MTech")
                receiptNumber=code.institute_code+"R-MTECH-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno
            else
                receiptNumber=code.institute_code+"R-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno

            return receiptNumber
        }


    }

    String getSWReceiptNumber(long orgid,String AY,String year,String Progtype ) {
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        String[] yr = AY.split("-")
        ////println("yr:"+yr)
        def ay=SocialWelfareReceiptNumberTracking.findByYearAndOrganization(yr[0],org)
        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber

        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        if(ay==null)
            return "Please verify Organization code or Academic year"
        else
        {
            if(Progtype=="MCA")
            {receiptNumber=code.institute_code+"SW-MCA-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno}
            else if(Progtype=="MTech")
                receiptNumber=code.institute_code+"SW-MTECH-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno
            else
                receiptNumber=code.institute_code+"SW-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno

            return receiptNumber
        }


    }

    String getReRegReceiptNumber(long orgid,String AY){
        println("AY :: " + AY + ", orgid :: " + orgid)
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        //def ay=ReRegReceiptNumberTracking.findByOrganizationAndYear(org,yr[0])
        def ay=ReRegReceiptNumberTracking.findByOrganizationAndIsactive(org,true)
        String[] yr = ay.academicyear.ay.split("-")
        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber
        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        if(ay==null)
            return "Please verify Organization code"
        else
        {
            receiptNumber=code.institute_code+"RR-"+yr[0][2]+yr[0][3]+tno
            return receiptNumber
        }
    }

    String getLateFineReceiptNumber(long orgid,String AY,String year){
        //println("ay:"+AY)
        String[] yr = AY.split("-")
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        def ay=LateFeesNumberTracking.findByOrganizationAndYear(org,yr[0])
        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber
        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        if(ay==null)
            return "Please verify Organization code"
        else
        {
            receiptNumber=code.institute_code+"AL-"+year[0]+yr[0][2]+yr[0][3]+tno
            return receiptNumber
        }
    }

    String getRevaluationReceiptNumber(long orgid,String AY,String year, def programtyperecepitchar){
        //println("ay:"+AY)
        String[] yr = AY.split("-")
        Organization org = Organization.findById(orgid)
        //println("org  :: " + org )
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        //println("code :: " + code)
        ERPReceiptTypeMaster erpReceiptTypeMaster = ERPReceiptTypeMaster.findByTypeAndOrganization("Revaluation",org)
        //println("erpReceiptTypeMaster  :: " + erpReceiptTypeMaster )
        def ay = ERPCommonReceiptNumberTracker.findByOrganizationAndErpreceipttypemasterAndIsactive(org, erpReceiptTypeMaster, true)
        println("ay :: " + ay)
        int no = Integer.parseInt(ay.number)
        //println("nno :: " + no)
        String tno
        String receiptNumber
        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        //println("tno :: " + tno)
        if(ay==null)
            return "Please verify Organization code"
        else {
            receiptNumber=code.institute_code+"Rev-"+programtyperecepitchar+year[0]+yr[0][2]+yr[0][3]+tno
            //println("receiptNumber :: " + receiptNumber)

            // increment receipt number no
            no = no + 1
            ay.number = no.toString()
            ay.save(failOnError: true, flush: true)

            return receiptNumber
        }
    }

    String getReexamReceiptNumber(long orgid,String AY,String year, def programtyperecepitchar){
        //println("ay:"+AY)
        String[] yr = AY.split("-")
        Organization org = Organization.findById(orgid)
        //println("org  :: " + org )
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        //println("code :: " + code)
        ERPReceiptTypeMaster erpReceiptTypeMaster = ERPReceiptTypeMaster.findByTypeAndOrganization("Reexam",org)
        //println("erpReceiptTypeMaster  :: " + erpReceiptTypeMaster )
        def ay = ERPCommonReceiptNumberTracker.findByOrganizationAndErpreceipttypemaster(org, erpReceiptTypeMaster)
        println("ay :: " + ay)
        int no = Integer.parseInt(ay.number)
        //println("nno :: " + no)
        String tno
        String receiptNumber
        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        //println("tno :: " + tno)
        if(ay==null)
            return "Please verify Organization code"
        else {
            receiptNumber=code.institute_code+"Rex-"+programtyperecepitchar+year[0]+yr[0][2]+yr[0][3]+tno
            //println("receiptNumber :: " + receiptNumber)

            // increment receipt number no
            no = no + 1
            ay.number = no.toString()
            ay.save(failOnError: true, flush: true)

            return receiptNumber
        }
    }

    String getBacklogReceiptNumber(long orgid,String AY,String sem, def programtyperecepitchar){
        //println("ay:"+AY)
        String[] yr = AY.split("-")
        Organization org = Organization.findById(orgid)
        //println("org  :: " + org )
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        //println("code :: " + code)
        ERPReceiptTypeMaster erpReceiptTypeMaster = ERPReceiptTypeMaster.findByTypeIlikeAndOrganization("Backlog",org)
        //println("erpReceiptTypeMaster  :: " + erpReceiptTypeMaster )
        def ay = ERPCommonReceiptNumberTracker.findByOrganizationAndErpreceipttypemaster(org, erpReceiptTypeMaster)
        println("ay :: " + ay)
        int no = Integer.parseInt(ay.number)
        //println("nno :: " + no)
        String tno
        String receiptNumber
        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        //println("tno :: " + tno)
        if(ay==null)
            return "Please verify Organization code"
        else {
            receiptNumber=code.institute_code+"Bck-"+programtyperecepitchar+sem[0]+yr[0][2]+yr[0][3]+tno
            //println("receiptNumber :: " + receiptNumber)

            // increment receipt number no
            no = no + 1
            ay.number = no.toString()
            ay.save(failOnError: true, flush: true)

            return receiptNumber
        }
    }

    String getRefundRevalReexamReceiptNumber(long orgid,String AY,String year, def programtyperecepitchar, def learnerexamtype){
        //println("ay:"+AY)
        String[] yr = AY.split("-")
        Organization org = Organization.findById(orgid)
        //println("org  :: " + org )
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        //println("code :: " + code)

        def abbrivation = ""
        ERPReceiptTypeMaster erpReceiptTypeMaster
        if(learnerexamtype.name == "Refund - reval") {
            abbrivation = "R-Rev"
            erpReceiptTypeMaster = ERPReceiptTypeMaster.findByTypeAndOrganization("Refund - Revaluation",org)
        } else if(learnerexamtype.name == "Refund - reexam") {
            abbrivation = "R-Rex"
            erpReceiptTypeMaster = ERPReceiptTypeMaster.findByTypeAndOrganization("Refund - Reexam",org)
        }
        def ay = ERPCommonReceiptNumberTracker.findByOrganizationAndErpreceipttypemaster(org, erpReceiptTypeMaster)
        println("ay :: " + ay)
        int no = Integer.parseInt(ay.number)
        //println("nno :: " + no)
        String tno
        String receiptNumber
        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        //println("tno :: " + tno)
        if(ay==null)
            return "Please verify Organization code"
        else {
            receiptNumber=code.institute_code+abbrivation+programtyperecepitchar+year[0]+yr[0][2]+yr[0][3]+tno
            //println("receiptNumber :: " + receiptNumber)

            // increment receipt number no
            no = no + 1
            ay.number = no.toString()
            ay.save(failOnError: true, flush: true)

            return receiptNumber
        }
    }

    String getAdhockReceiptNumber(long orgid,String AY,String year,String Progtype ) {
        println("year...:"+year)
        Organization org = Organization.findById(orgid)
        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)
        String[] yr = AY.split("-")
        println("AY:"+AY)
        println("yr:"+yr)
        ERPReceiptTypeMaster erpReceiptTypeMaster = ERPReceiptTypeMaster.findByTypeAndOrganization('Adhock', org)
        println("erpReceiptTypeMaster :: " + erpReceiptTypeMaster )
        //def ay=ReceiptNumberTracking.findByYearAndOrganization(yr[0],org)
        def ay = ERPCommonReceiptNumberTracker.findByOrganizationAndIsactiveAndErpreceipttypemaster(org, true, erpReceiptTypeMaster)
        int no = Integer.parseInt(ay.number)
        String tno
        String receiptNumber
        if(no<10) {
            tno="000"+no
        } else if(no<100) {
            tno="00"+no
        } else if(no<1000) {
            tno="0"+no
        } else {
            tno=no
        }
        if(ay==null) {
            return "Please verify Organization code or Academic year"
        } else {
            receiptNumber = code.institute_code + "AD-" + yr[0][2] + yr[0][3] + tno
//            if(Progtype=="MCA") {
//                receiptNumber=code.institute_code+"MCA-"+year[0]+"-"+yr[0][2]+yr[0][3]+tno
//            } else if(Progtype=="MTech") {
//                receiptNumber = code.institute_code + "MTECH-" + year[0] + "-" + yr[0][2] + yr[0][3] + tno
//            } else {
//                receiptNumber = code.institute_code + year[0] + "-" + yr[0][2] + yr[0][3] + tno
//            }
            return receiptNumber
        }
    }



    String getHostelReceiptNumber(long orgid, AcademicYear academicyear){

        Organization org = Organization.findById(orgid)

        ERPGRNumberInstituteCode code = ERPGRNumberInstituteCode.findByOrganization(org)

        def ay = HostelReceiptTracking.findByOrganizationgroupAndAcademicyear(org?.organizationgroup, academicyear)
        String[] yr = ay.academicyear.ay.split("-")
        int no = ay.number
        String tno
        String receiptNumber
        if(no<10)
        {
            tno="000"+no
        }
        else if(no<100){
            tno="00"+no
        }
        else if(no<1000){
            tno="0"+no
        }
        else{
            tno=no
        }
        if(ay==null)
            return "Please verify Organization code"
        else
        {
            receiptNumber=code.institute_code+"HS-"+yr[0][2]+yr[0][3]+tno
            return receiptNumber
        }
    }

}
