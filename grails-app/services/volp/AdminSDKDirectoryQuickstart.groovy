package volp

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.admin.directory.Directory;
import com.google.api.services.admin.directory.DirectoryScopes;
import com.google.api.services.admin.directory.model.Users
import com.google.api.services.admin.directory.model.User;
import com.google.api.services.admin.directory.model.Group;
import com.google.api.services.admin.directory.model.Member;
import com.google.api.services.admin.directory.model.UserName
import grails.converters.JSON;
import grails.gorm.transactions.Transactional
import org.apache.http.entity.StringEntity
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.Collections;
import java.util.List;
import org.json.simple.JSONArray;


@Transactional
class AdminSDKDirectoryQuickstart {
    def APPLICATION_NAME = "Google Admin SDK Directory API Java Quickstart";
    def JSON_FACTORY = JacksonFactory.getDefaultInstance();
    def SCOPES = [];

    private static LocalServerReceiver receiver = null;

    def TOKENS_DIRECTORY_PATH
    def CREDENTIALS_FILE_PATH

    def creategmailaccount(def firstname, def lastname, def email, def password, def org) throws IOException, GeneralSecurityException {
        email = email.replaceAll(" ", "")
        println("creategmailaccount :: ")
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Directory service = new Directory.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT, org, 'user'))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            def present = false
            try {
               present = service.users().get(email).setProjection("full").execute();
            } catch(Exception e) {}

            println("present : "+present)

            if(!present) {
                User user = new User();
                UserName name = new UserName();
                name.setFamilyName(firstname);
                name.setGivenName(lastname);
                user.setName(name);
                user.setPassword(password);
                user.setPrimaryEmail(email);
                user = service.users().insert(user).execute();

                return 1;
            }else{
                return 2;
            }
        } catch(Exception e) {
            return 0
        }
    }
    def resetemailpassword(def email,def password, def org) throws IOException, GeneralSecurityException {
        email = email.replaceAll(" ", "")
        println("resetemailpassword :: ")
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Directory service = new Directory.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT, org, 'user'))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            def present = false
            try {
                present = service.users().get(email).setProjection("full").execute();
            } catch(Exception e) {}

            println("present : "+present)

            if(present) {

                present.setPassword(password);

                present = service.users().update(email,present).execute();

                return 1;
            }else{
                return 2;
            }
        } catch(Exception e) {
            println(e)
            return 0
        }
    }

    def creategroup(def groupname, def groupemail, def useremail, def org) throws IOException, GeneralSecurityException {
        groupemail = groupemail.replaceAll(" ", "")
        useremail = useremail.replaceAll(" ", "")
        println("creategroup :: ")
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Directory service = new Directory.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT, org, 'group'))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            def presentgroup = false
            try {
                presentgroup = service.groups().get(groupemail).execute();
            } catch(Exception e) {}

            println("presentgroup : "+presentgroup)

            if(!presentgroup) {
                def group = new Group();
                group.setEmail(groupemail);
                group.setName(groupname);
                Group googleGroup = service.groups().insert(group).execute();
                println("googleGroup :"+googleGroup)
            }

            return 1
        } catch(Exception e) {
            return 0
        }
    }

    def addGoogleGroupMember(def userEmail, def groupEmail, def groupname, def role, def org) throws IOException, GeneralSecurityException {
        groupEmail = groupEmail.replaceAll(" ", "")
        userEmail = userEmail.replaceAll(" ", "")
        println("addGoogleGroupMember :: ")
        try {
            final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
            Directory service = new Directory.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT, org, 'group'))
                    .setApplicationName(APPLICATION_NAME)
                    .build();

            def presentmember = false
            try {
                presentmember = service.members().get(groupEmail, userEmail).execute();
            } catch(Exception e) {}

            println("presentmember : "+presentmember)

            if(!presentmember) {
                Member member = new Member();
                member.setEmail(userEmail);
                member.setRole(role);
                member.setType("USER");
                Member memberadded =  service.members().insert(groupEmail, member).execute()
                println("memberadded :" + memberadded)
            }

            return 1
        } catch(Exception e) {
            return 0
        }
    }


    def getGmailList() throws IOException, GeneralSecurityException {
        println("getGmailList :: ")
        final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
        Directory service = new Directory.Builder(HTTP_TRANSPORT, JSON_FACTORY, getCredentials(HTTP_TRANSPORT, org, 'user'))
                .setApplicationName(APPLICATION_NAME)
                .build();

        println("service :"+service)

        Users result = service.users().list()
                .setCustomer("my_customer")
                .setMaxResults(10)
                .setOrderBy("email")
                .execute();

        println("result : "+result)

        List<User> users = result.getUsers();
        if (users == null || users.size() == 0) {
            System.out.println("No users found.");
        } else {
            System.out.println("Users:");
            for (User user : users) {
                System.out.println(user.getName().getFullName());
            }
        }
    }

    def getCredentials(final NetHttpTransport HTTP_TRANSPORT, def org, def type) throws IOException {
        // Load client secrets.
        CREDENTIALS_FILE_PATH = org?.gsuit_credentials_file_name
//        println("CREDENTIALS_FILE_PATH : "+CREDENTIALS_FILE_PATH)
        TOKENS_DIRECTORY_PATH = org?.gsuit_credentials_file_path
//        println("TOKENS_DIRECTORY_PATH : "+TOKENS_DIRECTORY_PATH)
        int PORT_NUMBER = org?.gsuit_port_number
        println("PORT_NUMBER :"+PORT_NUMBER)

        SCOPES.add(DirectoryScopes.ADMIN_DIRECTORY_USER);
        SCOPES.add(DirectoryScopes.ADMIN_DIRECTORY_GROUP);

        java.io.File clientSecretFilePath = new java.io.File(TOKENS_DIRECTORY_PATH, CREDENTIALS_FILE_PATH);
        InputStream inn = new FileInputStream(clientSecretFilePath);
        if (inn == null) {
            throw new FileNotFoundException("Resource not found: " + CREDENTIALS_FILE_PATH);
        }
        GoogleClientSecrets clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(inn));
        FileDataStoreFactory obj = new FileDataStoreFactory(new java.io.File(TOKENS_DIRECTORY_PATH))
        GoogleAuthorizationCodeFlow flow = new GoogleAuthorizationCodeFlow.Builder(
                HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
                .setDataStoreFactory(obj)
                .setAccessType("offline")
                .build();


        if(receiver == null){
            receiver = new LocalServerReceiver.Builder().setPort(PORT_NUMBER).build();
        }else{
            receiver.stop();
            receiver = new LocalServerReceiver.Builder().setPort(PORT_NUMBER).build();
        }

        return new AuthorizationCodeInstalledApp(flow, receiver).authorize(type);
    }
}