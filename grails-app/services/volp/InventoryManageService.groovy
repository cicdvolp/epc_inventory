package volp

import grails.gorm.transactions.Transactional

@Transactional
class InventoryManageService {


    def saveInventoryDetails(params,request,session,flash){
        println("in saveInventoryDetails service...." + params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invMaterial = InvMaterial.findById(params.material)
        def invdepartment = Department.findById(params.department)
        def invSerialNumber = InvSerialNumber.findByDepartmentAndOrganization(invdepartment,org)
        def invoice = Invoice.findById(params.invoiceId)
        def reciept = InvReceipt.findByInvoice(invoice)
        def room = InvRoom.findById(params.inventoryRoom)
        def qty = Integer.parseInt(params.addQty)
        def invoiceDetails = InvoiceDetails.findByInvoiceAndInvmaterialAndOrganization(invoice,invMaterial,org)
        if(invoiceDetails.available_qty == 0){
            flash.error = "All Items are already added,Please Check Inventory"
            return
        }else if(qty > invoiceDetails?.available_qty){
            flash.error = "Cannot add more than avaliable Quantity"
            return
        }
        def materialParts = InvMaterialPart.findAllByInvmaterialAndIsactive(invMaterial,true)
        for (def i = 0; i < qty ; i++)
        {
            //def inv = Inventory.findByInvreceiptAndInvmaterialAndOrganization(reciept,invMaterial,org)
            //def inv = Inventory.findByInvreceiptAndInvmaterialAndAccession_numberAndOrganization(reciept,invMaterial,params.accNo,org)
                if(materialParts.size() == 0)
                {
                        def inv = new Inventory()
                        //println"-->"+invMaterial?.organization?.organization_code + "/"+invdepartment?.name+"/"+invSerialNumber?.number
                        inv.accession_number = invMaterial?.organization?.organization_code + "/" +(invdepartment?.name).replaceAll("\\s","")+"/"+invSerialNumber?.number
                        invSerialNumber.number++
                        inv.inward_date = params.inwardDate
                        inv.purchase_cost = Double.parseDouble(params.purchaseCost)
                        inv.invreceipt = reciept
                        inv.invroom = room
                        inv.department = invdepartment
                        inv.invdeadstockstatus = InvDeadstockStatus.findByStatus("Working-Condition")
                        inv.invmaterial = invMaterial
                        inv.invoice = invoice
                        inv.organization = org
                        inv.isactive = true
                        def depreciation = InvDepreciationPercentage.findByInvmaterialAndOrganization(invMaterial,org)
                        inv.depreciation_rate = depreciation?.percentage
                        inv.depreciation_value = (Double.parseDouble(params.purchaseCost) - (Double.parseDouble(params.purchaseCost) * depreciation?.percentage)/100)
                        inv.creation_date = new Date()
                        inv.updation_date = new Date()
                        inv.creation_ip_address = request.getRemoteAddr()
                        inv.updation_ip_address = request.getRemoteAddr()
                        inv.creation_username = instructor.uid
                        inv.updation_username = instructor.uid
                        inv.save(flush: true,failOnError: true)
                        flash.message ="Inventory Succesfully Saved"

                }
                else
                {
                    for(materialPartsItem in materialParts)
                    {
                        def inv = new Inventory()
                        //println"-->"+invMaterial?.organization?.organization_code + "/"+invdepartment?.name+"/"+invSerialNumber?.number
                        inv.accession_number = invMaterial?.organization?.organization_code + "/" +(invdepartment?.name).replaceAll("\\s","")+"/"+materialPartsItem?.name+"/"+invSerialNumber?.number
                        invSerialNumber.number++
                        inv.invmaterialpart = materialPartsItem
                        inv.inward_date = params.inwardDate
                        inv.purchase_cost = Double.parseDouble(params.purchaseCost)
                        inv.invreceipt = reciept
                        inv.invroom = room
                        inv.department = invdepartment
                        inv.invdeadstockstatus = InvDeadstockStatus.findByStatus("Working-Condition")
                        inv.invmaterial = invMaterial
                        inv.invoice = invoice
                        inv.organization = org
                        inv.isactive = true
                        def depreciation = InvDepreciationPercentage.findByInvmaterialAndInvmaterialpartAndOrganization(invMaterial,materialPartsItem,org)
                        inv.depreciation_rate = depreciation?.percentage
                        inv.depreciation_value = (Double.parseDouble(params.purchaseCost) - (Double.parseDouble(params.purchaseCost) * depreciation?.percentage)/100)

                        inv.creation_date = new Date()
                        inv.updation_date = new Date()
                        inv.creation_ip_address = request.getRemoteAddr()
                        inv.updation_ip_address = request.getRemoteAddr()
                        inv.creation_username = instructor.uid
                        inv.updation_username = instructor.uid
                        inv.save(flush: true,failOnError: true)
                        flash.message ="Inventory Succesfully Saved"

                    }
                }
            invoiceDetails?.available_qty--
        }
    }
    def createTransferRequest(params,request,session,flash){
        println"In createTransferRequest service-->"+params
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org = login.organization
        def inventory = Inventory.findById(params.inventoryId)
        def transferLevel = InvTransferLevel.findById(params.transferLevel)
        def invApprovalStatus = InvApprovalStatus.findByName("In-Process")

        InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByNameAndIsactiveAndOrganization("Inventory", true, org)
        def invApprovingAuthorityLevels = InvFinanceApprovingAuthorityLevel.findAllWhere(organization:org,isactive:true, invfinanceapprovingcategory:invFinanceApprovingCategory)
        for(authority_level in invApprovingAuthorityLevels) {
            InventoryTransferEscalation inventoryTransferEscalation = InventoryTransferEscalation.findByInventoryAndInvfinanceapprovingauthoritylevelAndIsactiveAndOrganization(inventory,authority_level,true,org)
            if(inventoryTransferEscalation) {
                flash.error = "Request For this Inventory already created"
                return
            }
            else {
                inventoryTransferEscalation = new InventoryTransferEscalation()
                inventoryTransferEscalation.action_date = new Date()
                inventoryTransferEscalation.remark = null
                inventoryTransferEscalation.organization = org
                inventoryTransferEscalation.inventory = inventory
                if(params.dept){
                    println"Saving Dept Transfer Reqst"
                    def dept = Department.findById(params.dept)
                    inventoryTransferEscalation.department = dept
                    inventoryTransferEscalation.transfer_to = null
                }
                else{
                    println"Saving Ins Transfer Reqst"
                    inventoryTransferEscalation.department = null
                    inventoryTransferEscalation.transfer_to = params.transferOrg
                }
                //Kept statics until transfer to other ORG flow is set
                inventoryTransferEscalation.invtransferlevel = InvTransferLevel.findByName("Department")

                inventoryTransferEscalation.invfinanceapprovingauthoritylevel = authority_level
                inventoryTransferEscalation.invapprovalstatus = invApprovalStatus
                inventoryTransferEscalation.actionby = instructor
                inventoryTransferEscalation.isactive = true

                inventoryTransferEscalation.creation_username = login.username
                inventoryTransferEscalation.updation_username = login.username
                inventoryTransferEscalation.creation_date = new Date()
                inventoryTransferEscalation.updation_date = new Date()
                inventoryTransferEscalation.creation_ip_address = request.getRemoteAddr()
                inventoryTransferEscalation.updation_ip_address = request.getRemoteAddr()
                inventoryTransferEscalation.save(flush: true, failOnError: true)
                flash.message = "Request to transfer inventory  has been successfully created"
            }
        }
    }

    //write-off request
    def saveInvWriteoffRequestfetchdata(params, session, request, flash){
        println("in saveInvWriteoffRequestfetchdata service : "+params )
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def inventory = Inventory.findById(params.invId)
        def status = InvDeadstockStatus.findById(params.status)

        ApplicationType at = ApplicationType.findByApplication_type("ERP")
        RoleType rt = RoleType.findByApplicationtypeAndTypeAndOrganization(at,"Registration", org)
        ApplicationAcademicYear aay = ApplicationAcademicYear.findByRoletypeAndIsActiveAndOrganization(rt, true,org)
        AcademicYear academicYear = AcademicYear.findById(aay?.id)
        //println("academicYear :"+academicYear)
        def invApprovalStatus = InvApprovalStatus.findByNameAndIsactive("In-Process", true)

        InvWriteOffRequest invWriteOffRequest = InvWriteOffRequest.findByInventory(inventory)
        if (invWriteOffRequest == null){
            invWriteOffRequest = new InvWriteOffRequest()
            invWriteOffRequest.requestby = instructor
            invWriteOffRequest.organization= org
            invWriteOffRequest.inventory = inventory
            invWriteOffRequest.academicyear = academicYear
            invWriteOffRequest.inventory.invdeadstockstatus = status
            invWriteOffRequest.reason = params.reason
            //invWriteOffRequest.inventory.writeoff_date = new Date()
            // invWriteOffRequest.inventory.invoice = params.invoice
            invWriteOffRequest.creation_date = new Date()
            invWriteOffRequest.updation_date = new Date()
            invWriteOffRequest.creation_ip_address = request.getRemoteAddr()
            invWriteOffRequest.updation_ip_address = request.getRemoteAddr()
            invWriteOffRequest.request_date = new Date()
            invWriteOffRequest.creation_username = instructor.uid
            invWriteOffRequest.updation_username = instructor.uid
            invWriteOffRequest.isactive = true
            invWriteOffRequest.save(failOnError: true, flush: true)

            InvFinanceApprovingCategory invFinanceApprovingCategory = InvFinanceApprovingCategory.findByOrganizationAndNameAndIsactive(org, "Write Off", true)
            def invApprovingAuthorityLevels =InvFinanceApprovingAuthorityLevel.findAllWhere(organization:org,isactive:true, invfinanceapprovingcategory:invFinanceApprovingCategory)
            for(authorityLevel in invApprovingAuthorityLevels){
                InvWriteOffRequestEscalation invWriteOffRequestEscalation = InvWriteOffRequestEscalation.findByInvwriteoffrequestAndInvfinanceapprovingauthoritylevel(invWriteOffRequest,authorityLevel)
                if(invWriteOffRequestEscalation == null){
                    invWriteOffRequestEscalation = new InvWriteOffRequestEscalation()
                    invWriteOffRequestEscalation.action_date = new Date()
                    invWriteOffRequestEscalation.remark =  params.reason
                    invWriteOffRequestEscalation.actionby = instructor
                    invWriteOffRequestEscalation.organization = org
                    invWriteOffRequestEscalation.invwriteoffrequest = invWriteOffRequest
                    invWriteOffRequestEscalation.invapprovalstatus = invApprovalStatus
                    invWriteOffRequestEscalation.invfinanceapprovingauthoritylevel = authorityLevel
                    invWriteOffRequestEscalation.isactive = true
                    invWriteOffRequestEscalation.creation_username = instructor.uid
                    invWriteOffRequestEscalation.updation_username = instructor.uid
                    invWriteOffRequestEscalation.creation_ip_address = request.getRemoteAddr()
                    invWriteOffRequestEscalation.updation_ip_address = request.getRemoteAddr()
                    invWriteOffRequestEscalation.creation_date = new Date()
                    invWriteOffRequestEscalation.updation_date = new Date()
                    invWriteOffRequestEscalation.save(failOnError: true, flush: true)
                }
            }
            flash.message= "Saved Successfully"

        }else{
            flash.message= "Already present"
        }


    }

    def writeOffRequestApproval(params, session, request, flash){
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization

        println("params writeOffRequestApproval service : "+params)
        InvWriteOffRequest invWriteOffRequest = InvWriteOffRequest.findById(params.invwriteoffrequestId)
        if(invWriteOffRequest != null) {
            def escalations = InvWriteOffRequestEscalation.findAllWhere(invwriteoffrequest: invWriteOffRequest)
            println("escalations : " + escalations)
            def singleEsc = InvWriteOffRequestEscalation.findById(params.approveWOId)
            println("singleEsc : " + singleEsc)
            for(esc in escalations)
            {
                if(esc?.invfinanceapprovingauthoritylevel?.level_no == singleEsc?.invfinanceapprovingauthoritylevel?.level_no)
                {
                    InvApprovalStatus invApprovalStatus = InvApprovalStatus.findById(params.approvalStatus)
                    if(invApprovalStatus?.name == "In-Process"){
                        esc.remark = params.approveWORemark
                        esc.actionby = instructor
                        esc.action_date=new java.util.Date()
                        esc.updation_username=instructor.uid
                        esc.updation_date=new java.util.Date()
                        esc.updation_ip_address=request.getRemoteAddr()
                        esc.save(flush: true,failOnError: true)
                        break;
                    }
                    else if(invApprovalStatus?.name == "Approved")
                    {
                        esc.invapprovalstatus =invApprovalStatus
                        esc.isactive =false
                        esc.remark = params.approveWORemark
                        esc.actionby = instructor
                        esc.action_date=new java.util.Date()
                        esc.updation_username=instructor.uid
                        esc.updation_date=new java.util.Date()
                        esc.updation_ip_address=request.getRemoteAddr()
                        esc.save(flush: true,failOnError: true)
                        if(esc?.invfinanceapprovingauthoritylevel?.islast == true){
                            invWriteOffRequest.isapproved = true
                           // invWriteOffRequest.invapprovalstatus = invApprovalStatus
                            invWriteOffRequest.save(flush: true,failOnError: true)
                        }

                    }else if(invApprovalStatus.name == "Rejected")
                    {
                        invWriteOffRequest.isactive = false
                        invWriteOffRequest.invapprovalstatus=invApprovalStatus
                        invWriteOffRequest.updation_username=instructor.uid
                        invWriteOffRequest.updation_date=new java.util.Date()
                        invWriteOffRequest.updation_ip_address=request.getRemoteAddr()
                        invWriteOffRequest.save(flush: true,failOnError: true)

                        //Reject All Escalations ----Isactive made false
                        for(esc1 in escalations)
                        {
                            esc1.isactive = false;
                            esc1.invapprovalstatus=invApprovalStatus
                            esc1.actionby = instructor
                            esc1.action_date=new java.util.Date()
                            esc1.updation_username=instructor.uid
                            esc1.updation_date=new java.util.Date()
                            esc1.updation_ip_address=request.getRemoteAddr()
                            esc1.save(flush: true,failOnError: true)
                        }
                        break;
                    }
                    else{
                        flash.error = "Status Updation Error"
                    }
                }else if(esc?.invfinanceapprovingauthoritylevel?.level_no > singleEsc?.invfinanceapprovingauthoritylevel?.level_no)
                {
                    esc.isactive =true;
                    esc.save(flush: true,failOnError: true)
                    break;
                }
            }
            flash.message = "Saved Successfully"
        }else
        {
            flash.error = "Record Not Found"
        }

    }

    //material-inspection
    def saveMaterialInspection(params, session, request, flash){
        println"in saveMaterialInspection service...."+params
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        def org = instructor.organization
        def invoice = Invoice.findById(params.invoiceId)
        InvMaterialInspection invMaterialInspection = InvMaterialInspection.findByInvoiceAndOrganization(invoice,org)
        if(invMaterialInspection == null){
            invMaterialInspection = new InvMaterialInspection()
            if(params.is_inspected == 'on'){
                println(" is_inspected : "+params.is_inspected)
                invMaterialInspection.is_inspected= true
            }
            if(params.is_training_given == 'on'){
                println(" is_training_given : "+params.is_training_given)
                invMaterialInspection.is_training_given= true
            }
            if(params.is_material_inspected_tested_installed == 'on'){
                println("is_material_inspected_tested_installed : "+params.is_material_inspected_tested_installed)
                invMaterialInspection.is_material_inspected_tested_installed= true
            }
            if(params.is_original_copies_of_invoice_collected == 'on'){
                println("is_original_copies_of_invoice_collected : "+params.is_original_copies_of_invoice_collected)
                invMaterialInspection.is_original_copies_of_invoice_collected= true
            }
            if(params.is_delivery_challen_collected == 'on'){
                println("is_delivery_challen_collected : "+params.is_delivery_challen_collected)
                invMaterialInspection.is_delivery_challen_collected = true
            }
            if(params.is_test_certificate_received == 'on'){
                println("is_test_certificate_received : "+params.is_test_certificate_received)
                invMaterialInspection.is_test_certificate_received= true
            }
            if(params.is_warranty_card_supplied == 'on'){
                println("is_warranty_card_supplied : "+params.is_warranty_card_supplied)
                invMaterialInspection.is_warranty_card_supplied= true
            }
            if(params.is_amc_certificate_provided == 'on'){
                println("is_amc_certificate_provided : "+params.is_amc_certificate_provided)
                invMaterialInspection.is_amc_certificate_provided = true
            }
            if(params.is_technical_demonstration_done == 'on'){
                println("is_technical_demonstration_done : "+params.is_technical_demonstration_done)
                invMaterialInspection.is_technical_demonstration_done= true
            }
            if(params.is_user_manual_provided == 'on'){
                println("is_user_manual_provided : "+params.is_user_manual_provided)
                invMaterialInspection.is_user_manual_provided= true
            }
            if(params.is_installation_manual_provided == 'on'){
                println("is_installation_manual_provided : "+params.is_installation_manual_provided)
                invMaterialInspection.is_installation_manual_provided= true
            }
            if(params.is_maintainance_manual_provided == 'on'){
                println("is_maintainance_manual_provided : "+params.is_maintainance_manual_provided)
                invMaterialInspection.is_maintainance_manual_provided= true
            }
            invMaterialInspection.isactive = true
            invMaterialInspection.organization= org
            invMaterialInspection.invoice = invoice
            invMaterialInspection.inspectionby = instructor
            invMaterialInspection.remark = params.remark
            println("inspectionby : "+invMaterialInspection.inspectionby)
            invMaterialInspection.creation_ip_address = request.getRemoteAddr()
            invMaterialInspection.updation_ip_address = request.getRemoteAddr()
            invMaterialInspection.updation_username = instructor.uid
            invMaterialInspection.creation_username = instructor.uid
            invMaterialInspection.creation_date = new Date()
            invMaterialInspection.updation_date = new Date()
            invMaterialInspection.save(flush: true, failOnError: true)
            flash.message = "Saved Successfully"
            if(params.is_inspected == "on" && params.is_material_inspected_tested_installed== "on" &&
              params.is_warranty_card_supplied== "on" && params.is_amc_certificate_provided == 'on'){
                invoice.isapproved = true
                println"Invoice approved in save"
            }else{
                invoice.isapproved = false
                println"Invoice not approved in save"
            }
            invoice.save(failOnError: true,flush: true)
        }
        else{
            if(params.is_inspected == 'on'){
                println(" is_inspected : "+params.is_inspected)
                invMaterialInspection.is_inspected= true
            }
            if(params.is_training_given == 'on'){
                println(" is_training_given : "+params.is_training_given)
                invMaterialInspection.is_training_given= true
            }
            if(params.is_material_inspected_tested_installed == 'on'){
                println("is_material_inspected_tested_installed : "+params.is_material_inspected_tested_installed)
                invMaterialInspection.is_material_inspected_tested_installed= true
            }
            if(params.is_original_copies_of_invoice_collected == 'on'){
                println("is_original_copies_of_invoice_collected : "+params.is_original_copies_of_invoice_collected)
                invMaterialInspection.is_original_copies_of_invoice_collected= true
            }
            if(params.is_delivery_challen_collected == 'on'){
                println("is_delivery_challen_collected : "+params.is_delivery_challen_collected)
                invMaterialInspection.is_delivery_challen_collected = true
            }
            if(params.is_test_certificate_received == 'on'){
                println("is_test_certificate_received : "+params.is_test_certificate_received)
                invMaterialInspection.is_test_certificate_received= true
            }
            if(params.is_warranty_card_supplied == 'on'){
                println("is_warranty_card_supplied : "+params.is_warranty_card_supplied)
                invMaterialInspection.is_warranty_card_supplied= true
            }
            if(params.is_amc_certificate_provided == 'on'){
                println("is_amc_certificate_provided : "+params.is_amc_certificate_provided)
                invMaterialInspection.is_amc_certificate_provided = true
            }
            if(params.is_technical_demonstration_done == 'on'){
                println("is_technical_demonstration_done : "+params.is_technical_demonstration_done)
                invMaterialInspection.is_technical_demonstration_done= true
            }
            if(params.is_user_manual_provided == 'on'){
                println("is_user_manual_provided : "+params.is_user_manual_provided)
                invMaterialInspection.is_user_manual_provided= true
            }
            if(params.is_installation_manual_provided == 'on'){
                println("is_installation_manual_provided : "+params.is_installation_manual_provided)
                invMaterialInspection.is_installation_manual_provided= true
            }
            if(params.is_maintainance_manual_provided == 'on'){
                println("is_maintainance_manual_provided : "+params.is_maintainance_manual_provided)
                invMaterialInspection.is_maintainance_manual_provided= true
            }
            invMaterialInspection.isactive = true
            invMaterialInspection.organization= org
            invMaterialInspection.invoice = invoice
            invMaterialInspection.remark = params.remark
            invMaterialInspection.updation_ip_address = request.getRemoteAddr()
            invMaterialInspection.updation_username = instructor.uid
            invMaterialInspection.updation_date = new Date()
            invoice.save(flush: true, failOnError: true)

            if(params.is_inspected == "on" && params.is_material_inspected_tested_installed== "on" &&
             params.is_warranty_card_supplied== "on" && params.is_amc_certificate_provided == 'on'){
                invoice.isapproved = true
                println"Invoice approved in edit"
            }else{
                invoice.isapproved = false
                println"Invoice not approved in edit"
            }
            invoice.save(failOnError: true,flush: true)

            flash.message = "Updated Successfully"
        }

    }
    def activateMaterialInspection(params,session,request,flash){
        Login login = Login.findById(session.loginid)
        Invoice invoice = Invoice.findById(params.invoiceId)

        if(invoice) {
            if(invoice.isactive) {
                invoice.isactive = false
                invoice.updation_username = login.username
                invoice.updation_date = new Date()
                invoice.updation_ip_address = request.getRemoteAddr()
                invoice.save(flush: true, failOnError: true)
                flash.message = "In-Active Successfully.."
            } else {
                invoice.isactive = true
                invoice.updation_username = login.username
                invoice.updation_date = new Date()
                invoice.updation_ip_address = request.getRemoteAddr()
                invoice.save(flush: true, failOnError: true)
                flash.message = "Active Successfully.."
            }
        } else {
            flash.error = "Invoice Not found"
        }
        return
    }
    def activatePoDetailsInspMt(params,session,request,flash) {
        Login login = Login.findById(session.loginid)
        InvPurchaseOrderDetails invPurchaseOrderDetails = InvPurchaseOrderDetails.findById(params.podetailID)
        if (invPurchaseOrderDetails) {
            if (invPurchaseOrderDetails.isactive) {
                invPurchaseOrderDetails.isactive = false
                invPurchaseOrderDetails.updation_username = login.username
                invPurchaseOrderDetails.updation_date = new Date()
                invPurchaseOrderDetails.updation_ip_address = request.getRemoteAddr()
                invPurchaseOrderDetails.save(flush: true, failOnError: true)
                flash.message = "In-Active Successfully.."
            } else {
                invPurchaseOrderDetails.isactive = true
                invPurchaseOrderDetails.updation_username = login.username
                invPurchaseOrderDetails.updation_date = new Date()
                invPurchaseOrderDetails.updation_ip_address = request.getRemoteAddr()
                invPurchaseOrderDetails.save(flush: true, failOnError: true)
                flash.message = "Active Successfully.."
            }
        } else {
            flash.error = "Invoice Not found"
        }
        return
    }


    //DeadStock Verification
    def saveDeadStockVerification(params,request,session,flash){
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        Organization org  = instructor.organization
        def inventory=Inventory.findByAccession_number(params.accessionNumber)
        def deadStockStatus=InvDeadstockStatus.findById(params.deadStockStatus)
        if(inventory)
        {
            if(params.isWriteOff=='on')
            {
                inventory.iswriteoff=true
            }
            else{
                inventory.iswriteoff=false
            }

            inventory.writeoff_date=params.writeOffDate
            inventory.invdeadstockstatus=deadStockStatus
            inventory.updation_username=instructor.uid
            inventory.updation_date=new java.util.Date()
            inventory.updation_ip_address=request.getRemoteAddr()
            inventory.save(flush: true,failOnError: true)
            flash.message = "Saved Successfully"
        }
        else
        {
            flash.error = "Something Went Wrong"
        }
    }

    def editDeadStockVerification(params,request,session,flash){
        println("I am in editDeadStockVerification service"+ params)
        Login login = Login.findById(session.loginid)
        Instructor instructor = Instructor.findByUid(login.username)
        if(params.deadStockStatus == null || params.deadStockStatus == 'null' || params.deadStockStatus == ''){
            flash.error = "Please select Deadstock Status"
            return
        }
        def inventory=Inventory.findById(params.inventoryId)
        def deadStockStatus=InvDeadstockStatus.findById(params.deadStockStatus)
        if(inventory)
        {
            if(params.isWriteOff=='on')
            {
                inventory.iswriteoff=true
                inventory.writeoff_date=params.writeOffDate
            }
            else{
                inventory.iswriteoff=false
                inventory.writeoff_date=null
            }
            inventory.invdeadstockstatus=deadStockStatus
            inventory.updation_username=instructor.uid
            inventory.updation_date=new java.util.Date()
            inventory.updation_ip_address=request.getRemoteAddr()
            inventory.save(flush: true,failOnError: true)
            flash.message = "Updated Successfully"
        }
        else
        {
            flash.error ="Record Not Found"
        }

    }
}
