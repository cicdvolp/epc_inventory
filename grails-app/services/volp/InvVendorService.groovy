package volp

import javax.servlet.http.Part
import java.nio.file.Paths
import java.text.SimpleDateFormat
import javax.servlet.http.Part
import java.nio.file.Paths
import java.text.DateFormat
import java.text.SimpleDateFormat
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import grails.gorm.transactions.Transactional

@Transactional
class InvVendorService {

    def serviceMethod() {

    }
    //vendor rating
    def saveInvVendorRating(params,session,request,flash){
        Login login = Login.findById(session.loginid)
        def org = login.organization
        println("saveInvVendorRating :"+params)
        def invVendor = InvVendor.findByCompany_name(params.company_name)
        def rating = Double.parseDouble(params.rating)
        def comment = params.comment
        def ratingOutof = 10
        InvVendorRating invVendorRating = InvVendorRating.findByOrganizationAndInvvendor(org,invVendor)
        if(!invVendorRating){
            invVendorRating = new InvVendorRating()
            invVendorRating.invvendor=invVendor
            invVendorRating.rating=rating
            invVendorRating.comment=comment
            invVendorRating.ratingby=instructor
            invVendorRating.rating_date= new java.util.Date()
            invVendorRating.rating_outof=ratingOutof
            invVendorRating.organization= org
            invVendorRating.isactive= true
            invVendorRating.creation_username = login.username
            invVendorRating.updation_username = login.username
            invVendorRating.updation_ip_address = request.getRemoteAddr()
            invVendorRating.creation_ip_address = request.getRemoteAddr()
            invVendorRating.updation_date = new java.util.Date()
            invVendorRating.creation_date = new java.util.Date()
            invVendorRating.save(flush: true, failOnError: true)
            flash.message = "Vendor Feedback saved Successfully"
        }else{
            flash.error = "Already Present"
        }
    }
    def showInvVendorRating(session,request,flash,params){
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = login.organization
        println("showInvVendorRating "+params)
        def invVendorList = InvVendor.findAllWhere(organization: org)
        def invVendorCompanyList = invVendorList.company_name
        def invVendor = InvVendor.findByCompany_name(params.company_name)
        def invVendorRatingList = InvVendorRating.findAllWhere(invvendor: invVendor)
        [invVendorRatingList:invVendorRatingList,invVendorCompanyList:invVendorCompanyList,invVendor:invVendor]

    }

    //importvendor
    def saveImportedInvVendor(params, session, request, flash){
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = login.organization
        println("saveImportedInvVendor service " +params)
        def ipaddress = request.getRemoteAddr()
        def failedlist = []
        def successlist = []
        def file_incorrect_flag = false
        if(params.save == 'Upload') {
            def file = request.getFile("importvendorfile")
            println("file : "+file)
            if (!file.empty) {
                def sheetheader = []
                def values = []
                def workbook = new XSSFWorkbook(file.getInputStream())
                def sheet = workbook.getSheetAt(0)
                for (cell in sheet.getRow(0).cellIterator()) {
                    sheetheader << cell.stringCellValue
                }
                def headerFlag = true
                for (row in sheet.rowIterator()) {
                    if (headerFlag) {
                        headerFlag = false
                        continue
                    }
                    def value = ''
                    def map = [:]
                    for (cell in row.cellIterator()) {
                        switch (cell.cellType) {
                            case 1:
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 0:
                                cell.setCellType(cell.CELL_TYPE_STRING)
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            case 2:
                                cell.setCellType(cell.CELL_TYPE_STRING)
                                value = cell.stringCellValue
                                map["${sheetheader[cell.columnIndex]}"] = value
                                break
                            default:
                                value = ''
                        }
                    }
                    if (values.size() > 0) {
                        if (values[0].keySet().size() == map.size())
                            values.add(map)
                    } else {
                        values.add(map)
                    }
                }
                def colnamelist = values[0].keySet()
                for (int i = 0; i < values.size(); i++) {

                    def company_name = values[i].get('Company Name')
                    def company_registration_number = values[i].get('Company Registration Number')
                    def address = values[i].get('Address')
                    def rating = values[i].get('Rating')
                    def gst_no = values[i].get('GST Number')
                    def tan_no = values[i].get('TAN Number')
                    def contact_person_name = values[i].get('Contact Person Name')
                    def contact_person_contact = values[i].get('Contact Person Contact')
                    def contact_person_email = values[i].get('Contact Person Email')
                    def invmaterialcategory = values[i].get('Material Category')
                    def client_list = values[i].get('Client List')
                    def date_of_establishment1 = values[i].get('Date of Establishment')
                    def month_of_establishment = values[i].get('Month of Establishment')
                    def year_of_establishment = values[i].get('Year of Establishment')

                    def row = [
                            company_name :company_name,
                            company_registration_number: company_registration_number,
                            address : address,
                            rating: rating,
                            gst_no : gst_no,
                            tan_no : tan_no,
                            contact_person_name : contact_person_name,
                            contact_person_contact : contact_person_contact,
                            contact_person_email : contact_person_email,
                            client_list : client_list,
                            date_of_establishment1 : date_of_establishment1,
                            month_of_establishment : month_of_establishment,
                            year_of_establishment : year_of_establishment,
                            invmaterialcategory : invmaterialcategory,
                    ]
                    println("row " +row)

                    if(company_name == null) {
                        failedlist.add(row:row,  msg:"Company Name is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(company_registration_number == null) {
                        failedlist.add(row:row,  msg:"Company Registration Number is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(address == null) {
                        failedlist.add(row:row,  msg:"Company Address is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(gst_no == null) {
                        failedlist.add(row:row,  msg:"Company GST No is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(tan_no == null) {
                        failedlist.add(row:row,  msg:"Company TAN No is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(contact_person_name == null) {
                        failedlist.add(row:row,  msg:"Company Contact Person Name is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(contact_person_contact == null) {
                        failedlist.add(row:row,  msg:"Company Contact Person Telephone/Mobile No is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(contact_person_email == null) {
                        failedlist.add(row:row,  msg:"Company Contact Person's Email IS is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(client_list == null) {
                        failedlist.add(row:row,  msg:"Company Client List is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    /*if(date_of_establishment1 == null) {
                        flash.message= "Add Date of Establishment & Upload."
                        failedlist.add(row:row,  msg:"Company Establishment Date is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(month_of_establishment == null) {
                        flash.message= "Add Month of Establishment & Upload."
                        failedlist.add(row:row,  msg:"Incorrect File, Please download sample file first and edit file as per instructions and then upload file")
                        file_incorrect_flag = true
                        break
                    }
                    if(year_of_establishment == null) {
                        flash.message= "Add Year of Establishment & Upload."
                        failedlist.add(row:row,  msg:"Incorrect File, Please download sample file first and edit file as per instructions and then upload file")
                        file_incorrect_flag = true
                        break
                    }*/
                    Date establishment_full_date
                    if (date_of_establishment1 != "NA" && month_of_establishment != "NA" && year_of_establishment != "NA" && date_of_establishment1 != "-" && month_of_establishment != "-" && year_of_establishment != "-") {
                        String dateOfEstablishment = date_of_establishment1 + "/" + month_of_establishment + "/" + year_of_establishment
                        establishment_full_date = new SimpleDateFormat("dd/MM/yyyy").parse(dateOfEstablishment);
                    } else {
                        establishment_full_date = null
                    }
                    if(establishment_full_date == null) {
                        failedlist.add(row:row,  msg:"Company Establishment Date is mandatory field.")
                        file_incorrect_flag = true
                        break
                    }
                    if(invmaterialcategory == null) {
                        failedlist.add(row:row,  msg:"Atleast 1 Material Catgory required.")
                        file_incorrect_flag = true
                        break
                    }

                    def category = invmaterialcategory.split(',')
                    def invMaterialCategory_list = []
                    for(item in category) {
                        if(item != "") {
                            InvMaterialCategory materialCategory = InvMaterialCategory.findByNameAndOrganizationAndIsactive(item.trim(), org, true)
                            if (materialCategory) {
                                invMaterialCategory_list.add(materialCategory)
                            }
                        }
                    }
                    /*def invMaterialCategory = InvMaterialCategory.findAllWhere(organization : org)?.name
                    println("invMaterialCategory :"+invMaterialCategory)
                    def category= invmaterialcategory.split(',')
                    def newinvMaterialCategoryList= new ArrayList()
                    for(item in category){
                        if(invMaterialCategory.contains(item)){
                            newinvMaterialCategoryList.add(item)
                        }
                    }*/
                    InvVendor invvendor = InvVendor.findByCompany_registration_numberAndOrganization(company_registration_number, org)
                    if (invvendor != null) {
                        failedlist.add(row:row,  msg:"Vendor Already Present.")
                        file_incorrect_flag = true
                        break
                    } else {
                        invvendor = new InvVendor()
                        invvendor.company_name = company_name
                        invvendor.contact_person_contact= contact_person_contact
                        invvendor.company_registration_number= company_registration_number
                        invvendor.rating= rating
                        invvendor.contact_person_email = contact_person_email
                        invvendor.contact_person_name = contact_person_name
                        invvendor.address = address
                        invvendor.client_list = client_list
                        invvendor.date_of_establishment = establishment_full_date
                        invvendor.gst_no = gst_no
                        invvendor.tan_no = tan_no
                        invvendor.organization = org
                        invvendor.isactive = true
                        invvendor.creation_username = login.username
                        invvendor.updation_username = login.username
                        invvendor.creation_date = new Date()
                        invvendor.updation_date = new Date()
                        invvendor.creation_ip_address = request.getRemoteAddr()
                        invvendor.updation_ip_address = request.getRemoteAddr()
                        invvendor.addToInvmaterialcategory(invMaterialCategory_list)

                        //Create Login
                        Login log = Login.findByUsername(params.contactPersonEmail)
                        if(log == null){
                            log = new Login()
                            log.username = contact_person_email
                            log.password = contact_person_email
                            log.organization = instructor?.organization
                            log.isloginblocked = false
                            UserType ut = UserType.findByType("Vendor")
                            if(ut == null){
                                ut = new UserType()
                                ut.type= "Vendor"
                                ut.istypeset = true
                                ut.istypeset = "Vendor"
                                ut.organization = org
                                def at = ApplicationType.findByApplication_type('ERP')
                                ut.application_type = at

                                ut.creation_date = new Date()
                                ut.updation_date = new Date()
                                ut.updation_ip_address = request.getRemoteAddr()
                                ut.creation_ip_address = request.getRemoteAddr()
                                ut.save(flush: true,failOnError: true)
                                log.addToUsertype(ut)
                            }else{
                                log.addToUsertype(ut)
                            }
                            def roles = Role.findAllByUsertype(ut)
                            def roleslist = []
                            for(item in roles) {
                                roleslist.add(item)
                            }
                            log.addToRoles(roleslist)
                            log.creation_date = new Date()
                            log.updation_date = new Date()
                            log.updation_ip_address = request.getRemoteAddr()
                            log.creation_ip_address = request.getRemoteAddr()
                            log.save(flush: true,failOnError: true)
                            println"Login for Vendor Created"
                        }else{
                            failedlist.add(row:row,  msg:"Vendor Already Present.")
                        }

                        //Create Person
                        Person person = Person.findByEmailAndOrganization(params.contactPersonEmail,org)
                        if(person == null){
                            person = new Person()
                            person.email = contact_person_email
                            person.username = contact_person_email
                            person.organization = org
                            person.creation_date = new Date()
                            person.updation_date = new Date()
                            person.updation_ip_address = request.getRemoteAddr()
                            person.creation_ip_address = request.getRemoteAddr()
                            person.save(flush: true,failOnError: true)
                            println"Person Entry for Vendor Created"
                        }else{
                            failedlist.add(row:row,  msg:"Vendor Already Present.")
                        }

                        invvendor.save(failOnError: true, flush: true)
                        successlist.add(row: row)
                        flash.message= "File uploaded."
                    }
                }
            }
            else {
                return -1
            }
        } else {
            println("save != upload")
            flash.error = "Select file to upload"
        }
        def final_list = []
        final_list.add(failedlist:failedlist, successlist:successlist)

        return final_list
    }
    def activateImportedInvVendor(params,session,request,flash){
        Login login = Login.findById(session.loginid)
        InvVendor invVendor = InvVendor.findById(params.vendorId)

        if(invVendor) {
            if(invVendor.isactive) {
                invVendor.isactive = false
                invVendor.updation_username = login.username
                invVendor.updation_date = new Date()
                invVendor.updation_ip_address = request.getRemoteAddr()
                invVendor.save(flush: true, failOnError: true)
                flash.message = "In-Active Successfully.."
            } else {
                invVendor.isactive = true
                invVendor.updation_username = login.username
                invVendor.updation_date = new Date()
                invVendor.updation_ip_address = request.getRemoteAddr()
                invVendor.save(flush: true, failOnError: true)
                flash.message = "Active Successfully.."
            }
        } else {
            flash.error = "Vendor Not found"
        }
        return


    }

    //save invoice
    def saveInvoice(params, session, request, flash){
        println("in saveInvoice service...." + params)
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = login.organization
        def ay = params.ay
        def file = request.getFile('invoicefile')
        //println("file : "+file)
        InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findByPon(params.PON)
        def invvendor = invPurchaseOrder?.invvendor
        double bal_amount = invPurchaseOrder?.total
        double prePaidAmount = 0
        def PRPO =PRPOTracking.findByOrganization(org)

        def invoice = Invoice.findAllByInvvendorAndInvpurchaseorderAndOrganization(invvendor,invPurchaseOrder,org)
        //println("invoice :"+invoice)
        for(invItems in invoice){
            bal_amount = bal_amount - invItems.amount
            prePaidAmount = prePaidAmount + invItems.amount
        }
        if(bal_amount > 0){
            double invoiceAmount = Double.parseDouble(params.invoiceAmt)
            if(invoiceAmount > bal_amount){
                flash.error = "Amount Cannot be Greater than Balance Amount"
            }else{
                invoice = new Invoice()
                invoice.amount = invoiceAmount
                invoice.purpose = params.purpose
                invoice.tax = invPurchaseOrder?.tax
                invoice.invoice_number = PRPO?.invoiceNo
                PRPO.invoiceNo++                
                invoice.invoice_date = params.invoiceDate
                invoice.total = invPurchaseOrder?.total
                invoice.organization = org
                invoice.invvendor = invvendor
                invoice.isactive = true
                invoice.invpurchaseorder = invPurchaseOrder
                invoice.isapproved = false
                if(file!=null)
                {
                    AWSFolderPath awsFolderPath = AWSFolderPath.findByPath("cloud/")
                    AWSBucket awsBucket = AWSBucket.findByContent("documents")
                    AWSUploadDocumentsService awsUploadDocumentsService = new AWSUploadDocumentsService()
                    Part filePart = request.getPart("invoicefile")
                    def fileobj = request.getFile("invoicefile")
                    if(!fileobj.empty){
                        String filePartName = Paths.get(filePart.getSubmittedFileName()).getFileName().toString()
                        int indexOfDot = filePartName.lastIndexOf('.')
                        def fileExtention
                        if (indexOfDot > 0) {
                            fileExtention = filePartName.substring(indexOfDot + 1)
                            //println("fileExtention :: " + fileExtention)
                        }

                        //Invoice document store path ->
                        //awsFolderPath.path + "inventory/invoice/" + organization +"/" + id of purchase order+ "/" +
                        String date1 = new SimpleDateFormat("dd-MM-YYYY").format(new java.util.Date())
                        def folderPath = awsFolderPath.path + awsBucket?.content +"/"+"inventory/invoice"+"/" + org +"/" + invPurchaseOrder?.id
                        def fileName = vendor?.company_name +"_"+ date1 +"_"+invoice?.purpose+"."+ fileExtention
                        String existsFilePath = ""
                        //println("ExistsFilePath :"+existsFilePath)
                        //awsFolderPath.path + "inventory/advanced/" + organization +"/" + id of InvAdvanceSettlement + "/" +
                        def isUpload = awsUploadDocumentsService.uploadDocument(filePart, folderPath, fileName, existsFilePath)
                        if(isUpload){
                            invoice.file_path = folderPath
                            invoice.file_name = fileName
                            println "File uploaded"
                        }
                        else {
                            println "Something went wrong while uploading file..!"
                        }
                    }
                }
                invoice.creation_username = login.username
                invoice.updation_username = login.username
                invoice.creation_date = new Date()
                invoice.updation_date = new Date()
                invoice.updation_ip_address = request.getRemoteAddr()
                invoice.creation_ip_address = request.getRemoteAddr()
                invoice.save(flush: true, failOnError: true)
                flash.message = "Saved Successfully"
            }
        }else{
            flash.message="Cannot Add:- Full Payment Cleared"
        }

    }
    def activateInvoice(params,session,request,flash){
        Login login = Login.findById(session.loginid)
        Invoice invoice = Invoice.findById(params.invoiceId)

        if(invoice) {
            if(invoice.isactive) {
                invoice.isactive = false
                invoice.updation_username = login.username
                invoice.updation_date = new Date()
                invoice.updation_ip_address = request.getRemoteAddr()
                invoice.save(flush: true, failOnError: true)
                flash.message = "In-Active Successfully.."
            } else {
                invoice.isactive = true
                invoice.updation_username = login.username
                invoice.updation_date = new Date()
                invoice.updation_ip_address = request.getRemoteAddr()
                invoice.save(flush: true, failOnError: true)
                flash.message = "Active Successfully.."
            }
        } else {
            flash.error = "Invoice Not found"
        }
        return

    }
    def editInvoice(params, session, request, flash){
        println("in updateMaterialInspectionInvoice service...." + params)
        Login login = Login.findById(session.loginid)
        InvVendor vendor = InvVendor.findByContact_person_email(login.username)
        def org = vendor.organization

        // def file = request.getFile('invoicefile')
        InvPurchaseOrder invPurchaseOrder = InvPurchaseOrder.findByPon(params.poNo)
        def invvendor = invPurchaseOrder?.invvendor
        def poid= invPurchaseOrder?.id
        // def amount = Double.parseDouble(params.amount)
        // def tax= Double.parseDouble(params.tax)
        Invoice invoice = Invoice.findById(params.invoiceId)
        println("invoice :"+invoice)
        if(invoice != null){
            invoice.invoice_date = params.invoicedate
            invoice.total = invPurchaseOrder?.total
            invoice.organization = org
            invoice.isactive = true
            invoice.invpurchaseorder = invPurchaseOrder
            invoice.creation_username = login.username
            invoice.updation_username = login.username
            invoice.creation_date = new Date()
            invoice.updation_date = new Date()
            invoice.updation_ip_address = request.getRemoteAddr()
            invoice.creation_ip_address = request.getRemoteAddr()
            invoice.save(flush: true, failOnError: true)
            flash.message = "Updated Successfully"
        }else{
            flash.message = "Does not Exist"
        }
    }

    def saveSingleVendor(params, session, request, flash){
        println("in saveSingleVendor service....")
        Login login = Login.findById(session.loginid)
        def org = login.organization
        def invMaterialCategory_list = []
        for(item in params.invMaterailCategoryId) {
            if(item != "") {
                InvMaterialCategory materialCategory = InvMaterialCategory.findByIdAndOrganizationAndIsactive(item, org, true)
                if (materialCategory) {
                    invMaterialCategory_list.add(materialCategory)
                }
            }
        }
        //println"Mateiral Catetgory List---->"+invMaterialCategory_list

        def invVendor = InvVendor.findByCompany_registration_numberAndOrganization(params.companyRegistrationNumber,org)
        //def invVendor = InvVendor.findByCompany_nameAndInvmaterialcategory(params.company_name,invMaterialCategory)
        if(invVendor != null){
            flash.message = "Vendor With Company Registration Number Already Present"
        }else{
            invVendor = new InvVendor()
            invVendor.company_registration_number = params.companyRegistrationNumber
            invVendor.company_name = params.companyName
            invVendor.address = params.address
            invVendor.gst_no = params.gstNo
            invVendor.tan_no = params.tanNo
            invVendor.contact_person_name = params.contactPersonName
            invVendor.contact_person_contact = params.contactPersonContact
            invVendor.contact_person_email = params.contactPersonEmail
            invVendor.date_of_establishment = params.dateofEstablishment
            invVendor.rating = java.lang.Double.parseDouble(params.rating)
            invVendor.isactive = true
            invVendor.organization = org
            invVendor.addToInvmaterialcategory(invMaterialCategory_list)

            invVendor.creation_username = login.username
            invVendor.updation_username = login.username
            invVendor.creation_date = new Date()
            invVendor.updation_date = new Date()
            invVendor.updation_ip_address = request.getRemoteAddr()
            invVendor.creation_ip_address = request.getRemoteAddr()

            //Create Login
            Login log = Login.findByUsername(params.contactPersonEmail)
            if(log == null){
                log = new Login()
                log.username = params.contactPersonEmail
                log.password = params.contactPersonEmail
                log.organization = org
                log.isloginblocked = false
                UserType ut = UserType.findByType("Vendor")
                if(ut == null){
                    ut = new UserType()
                    ut.type= "Vendor"
                    ut.istypeset = true
                    ut.username = "Vendor"
                    ut.organization = org
                    def at = ApplicationType.findByApplication_type('ERP')
                    ut.application_type = at

                    ut.creation_date = new Date()
                    ut.updation_date = new Date()
                    ut.updation_ip_address = request.getRemoteAddr()
                    ut.creation_ip_address = request.getRemoteAddr()
                    ut.save(flush: true,failOnError: true)
                    log.addToUsertype(ut)
                }else{
                    log.addToUsertype(ut)
                }
                def roles = Role.findAllByUsertype(ut)
                def roleslist = []
                for(item in roles) {
                    roleslist.add(item)
                }
                log.addToRoles(roleslist)
                log.creation_date = new Date()
                log.updation_date = new Date()
                log.updation_ip_address = request.getRemoteAddr()
                log.creation_ip_address = request.getRemoteAddr()
                log.save(flush: true,failOnError: true)
                println"Login for Vendor Created"
            }else{
                flash.error  = "Vendor with " + params.contactPersonEmail + "already exist"
                return
            }

            //Create Person
            Person person = Person.findByEmailAndOrganization(params.contactPersonEmail,org)
            if(person == null){
                person = new Person()
                person.email = params.contactPersonEmail
                person.username = params.contactPersonEmail
                person.organization = org
                person.creation_date = new Date()
                person.updation_date = new Date()
                person.updation_ip_address = request.getRemoteAddr()
                person.creation_ip_address = request.getRemoteAddr()
                person.save(flush: true,failOnError: true)
                println"Person Entry for Vendor Created"
            }else{
                flash.error = "Person with" + params.contactPersonEmail + "Already Exist"
            }

            invVendor.save(flush: true,failOnError: true)
            println"Vendor Created"
            flash.message = "Vendor Succcesfully Added"
        }
        return
    }

}
