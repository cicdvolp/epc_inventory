package volp

import com.amazonaws.HttpMethod

import com.amazonaws.AmazonClientException
//import com.amazonaws.AmazonServiceException
import com.amazonaws.ClientConfiguration
import com.amazonaws.SdkClientException
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3

import com.amazonaws.auth.profile.ProfileCredentialsProvider
import com.amazonaws.regions.Region
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.amazonaws.services.s3.model.*
import com.amazonaws.services.s3.transfer.MultipleFileDownload
import com.amazonaws.services.s3.transfer.TransferManager
import com.amazonaws.services.s3.transfer.TransferManagerBuilder

import java.io.InputStream

import com.amazonaws.AmazonServiceException
import com.amazonaws.services.s3.transfer.Download
import com.amazonaws.services.s3.transfer.MultipleFileDownload
import com.amazonaws.services.s3.transfer.TransferManager
import com.amazonaws.services.s3.transfer.TransferManagerBuilder

import java.io.File

import com.amazonaws.services.s3.model.ListObjectsRequest
import com.amazonaws.services.s3.model.ObjectListing
import com.amazonaws.services.s3.model.S3ObjectSummary
import com.amazonaws.services.s3.model.ListObjectsV2Request;
import com.amazonaws.services.s3.model.ListObjectsV2Result;

//import com.amazonaws.services.cloudfront.CloudFrontUrlSigner;
//import com.amazonaws.services.cloudfront.util.SignerUtils;

class AWSBucketService {

    def serviceMethod() {}

    // AWS s3 credentials
    AmazonS3 getCredential(String clientRegion){
        AmazonS3 s3
            AWSBucket awsBucket = AWSBucket.findByContent("documents")
//        String access_key = "AKIASPWL33DPWBJ6VCFV"
//        String secret_key = "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"

        String access_key = awsBucket.accesskey
        String secret_key = awsBucket.screatekey

        try {
            ClientConfiguration clientConf = new ClientConfiguration();
            clientConf.setConnectionTimeout(60 * 10000);
            //credentials = new ProfileCredentialsProvider().getCredentials()
            BasicAWSCredentials creds = new BasicAWSCredentials(access_key, secret_key);
            s3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion("ap-south-1").withClientConfiguration(clientConf).build();
//            s3 = new AmazonS3Client(credentials);
//            AWSStaticCredentialsProvider provider = new AWSStaticCredentialsProvider(creds);

//            s3 =AmazonSQSClientBuilder.standard()
//                    .withCredentials(provider)
//                    .withRegion(Regions.ap-south-1)
//                    .build();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e)
        }
        return s3;
    }
    // creating new AWS bucket
    def createBucket(String bucket,String region){
        final BUCKET_NAME = bucket
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        /* AWSCredentials credentials = null
         try {
             //credentials = new ProfileCredentialsProvider().getCredentials()
         } catch (Exception e) {
             throw new AmazonClientException(
                     "Cannot load the credentials from the credential profiles file. " +
                             "Please make sure that your credentials file is at the correct " +
                             "location (~/.aws/credentials), and is in valid format.",
                     e)
         }*/
        //AmazonS3 s3 = AmazonS3ClientBuilder.standard()
        //        .withCredentials(new AWSStaticCredentialsProvider(credentials))
        //        .withRegion("ap-south-1")
        //        .build()
        AmazonS3 s3=getCredential(region)
        String bucketName = BUCKET_NAME + UUID.randomUUID()
        String key = "MyObjectKey"

//        System.out.println("===========================================")
//        System.out.println("Getting Started with Amazon S3")
//        System.out.println("===========================================\n")
        try {
            /*
             * Create a new S3 bucket - Amazon S3 bucket names are globally unique,
             * so once a bucket name has been taken by any user, you can't create
             * another bucket with that same name.
             *
             * You can optionally specify a location for your bucket if you want to
             * keep your data closer to your applications or users.
             */
//            System.out.println("Creating bucket " + bucketName + "\n")
            s3.createBucket(bucketName)
        }
        catch (AmazonServiceException ase) {
//            System.out.println("Caught an AmazonServiceException, which means your request made it "
//                    + "to Amazon S3, but was rejected with an error response for some reason.")
//            System.out.println("Error Message:    " + ase.getMessage())
//            System.out.println("HTTP Status Code: " + ase.getStatusCode())
//            System.out.println("AWS Error Code:   " + ase.getErrorCode())
//            System.out.println("Error Type:       " + ase.getErrorType())
//            System.out.println("Request ID:       " + ase.getRequestId())
        } catch (AmazonClientException ace) {
//            System.out.println("Caught an AmazonClientException, which means the client encountered "
//                    + "a serious internal problem while trying to communicate with S3, "
//                    + "such as not being able to access the network.")
//            System.out.println("Error Message: " + ace.getMessage())
        }
    }
    // creating folder
    boolean createFolder(String bucket,String region,String key){
        final BUCKET_NAME = bucket
        boolean created=false
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        //AWSCredentials credentials = null
        AmazonS3 s3=getCredential(region)
        /*try {
            //credentials = new ProfileCredentialsProvider().getCredentials()
            BasicAWSCredentials creds = new BasicAWSCredentials("AKIAIAB4H4AWAOQANQMQ", "UIDo4G/ZjeNU2AFlIYgPzT+zhnb20uBO6jmslDKJ");
            s3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion(clientRegion).build();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e)
        }

        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion("ap-south-1")
                .build()*/

        String bucketName = BUCKET_NAME
        //String key = key

//        System.out.println("===========================================")
//        System.out.println("Getting Started with Amazon S3")
//        System.out.println("===========================================\n")
        try {
            /*
             * Create a new S3 bucket - Amazon S3 bucket names are globally unique,
             * so once a bucket name has been taken by any user, you can't create
             * another bucket with that same name.
             *
             * You can optionally specify a location for your bucket if you want to
             * keep your data closer to your applications or users.
             */
            InputStream input = new ByteArrayInputStream(new byte[0]);
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength(0);
//            System.out.println("Creating Folder in Bucket " + bucketName + "\n")
            s3.putObject(new PutObjectRequest(bucketName, key, input, metadata));
            s3.setObjectAcl(bucketName, key, CannedAccessControlList.Private);
            created=true
        }
        catch (AmazonServiceException ase) {
//            System.out.println("Caught an AmazonServiceException, which means your request made it "
//                    + "to Amazon S3, but was rejected with an error response for some reason.")
//            System.out.println("Error Message:    " + ase.getMessage())
//            System.out.println("HTTP Status Code: " + ase.getStatusCode())
//            System.out.println("AWS Error Code:   " + ase.getErrorCode())
//            System.out.println("Error Type:       " + ase.getErrorType())
//            System.out.println("Request ID:       " + ase.getRequestId())
        } catch (AmazonClientException ace) {
//            System.out.println("Caught an AmazonClientException, which means the client encountered "
//                    + "a serious internal problem while trying to communicate with S3, "
//                    + "such as not being able to access the network.")
//            System.out.println("Error Message: " + ace.getMessage())
        }
        return created
    }
    // upload one file
    boolean putObjectToBucket(String bucket,String region,String key,InputStream is,String contentType){
//        String clientRegion = "ap-south-1";
        String bucketName = bucket;
        // String stringObjKeyName = "*** String object key name ***";
        String fileObjKeyName =key;
        //String fileName = file+key;
        boolean putobj=false


        try {
            /*AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                    .withRegion(clientRegion)
                    .withCredentials(new ProfileCredentialsProvider())
                    .build();*/
            AmazonS3 s3Client=getCredential(region)

            // Upload a file as a new object with ContentType and title specified.
//            println("PATH"+fileObjKeyName)
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType(contentType);
            metadata.addUserMetadata("x-amz-meta-title", "eduplusnow");

            PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName,is,metadata);
            s3Client.putObject(request);
            s3Client.setObjectAcl(bucketName, fileObjKeyName, CannedAccessControlList.Private);
            putobj=true
        }
        catch(AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }
        return putobj
    }
    // listing all buckets list
    def listBuckets(String region){
        /*AWSCredentials credentials = null
        try {
            credentials = new ProfileCredentialsProvider().getCredentials()
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e)
        }

        AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(credentials))
                .withRegion("us-west-2")
                .build()*/
        /*
             * List the buckets in your account
             */
        AmazonS3 s3=getCredential(region)
//        System.out.println("Listing buckets")
        for (Bucket bucket : s3.listBuckets()) {
            System.out.println(" - " + bucket.getName())
        }
//        System.out.println()
    }
    // to check whether folder or file is exist
    boolean doesObjectExist(String bucket,String region,String key){
        //final BUCKET_NAME = 'eduplusnow-video-on-demand-destination-v0i283tt37ix'
        String clientRegion = region;
        final BUCKET_NAME = bucket
        boolean exist=false
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        //AWSCredentials credentials = null
        AmazonS3 s3=getCredential(region)
        /* try {
             //credentials = new ProfileCredentialsProvider().getCredentials()
             BasicAWSCredentials creds = new BasicAWSCredentials("AKIAIAB4H4AWAOQANQMQ", "UIDo4G/ZjeNU2AFlIYgPzT+zhnb20uBO6jmslDKJ");
             s3 = AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion(clientRegion).build();
         } catch (Exception e) {
             throw new AmazonClientException(
                     "Cannot load the credentials from the credential profiles file. " +
                             "Please make sure that your credentials file is at the correct " +
                             "location (~/.aws/credentials), and is in valid format.",
                     e)
         }*/

        //AmazonS3 s3 = AmazonS3ClientBuilder.standard()
        //      .withCredentials(new AWSStaticCredentialsProvider(credentials))
        //    .withRegion("ap-south-1")
        //  .build()
//us-east-1
        String bucketName = BUCKET_NAME
        //String key = key

//        System.out.println("===========================================")
//        System.out.println("Getting Started with Amazon S3")
//        System.out.println("===========================================\n")
        try {
            /*
             * Check whether the object really exists in bucket or not
             */

//            System.out.println("Checking does object exists in " + bucketName + "\n")
            exist=s3.doesObjectExist(bucketName,key)
//            System.out.println("result of checking " + exist + "\n")
        }
        catch (AmazonServiceException ase) {
//            System.out.println("Caught an AmazonServiceException, which means your request made it "
//                    + "to Amazon S3, but was rejected with an error response for some reason.")
//            System.out.println("Error Message:    " + ase.getMessage())
//            System.out.println("HTTP Status Code: " + ase.getStatusCode())
//            System.out.println("AWS Error Code:   " + ase.getErrorCode())
//            System.out.println("Error Type:       " + ase.getErrorType())
//            System.out.println("Request ID:       " + ase.getRequestId())
        } catch (AmazonClientException ace) {
//            System.out.println("Caught an AmazonClientException, which means the client encountered "
//                    + "a serious internal problem while trying to communicate with S3, "
//                    + "such as not being able to access the network.")
//            System.out.println("Error Message: " + ace.getMessage())
        }
        return exist
    }
    // delete file or folder
    def deleteObject(String bucket,String region,String key){
        final BUCKET_NAME = bucket
        String clientRegion = region
        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        AWSCredentials credentials = null
        AmazonS3 s3=getCredential(region)
        /*try {
            //credentials = new ProfileCredentialsProvider().getCredentials()
            BasicAWSCredentials creds = new BasicAWSCredentials("AKIAIAB4H4AWAOQANQMQ", "UIDo4G/ZjeNU2AFlIYgPzT+zhnb20uBO6jmslDKJ");
             s3= AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion(clientRegion).build();
        } catch (Exception e) {
            throw new AmazonClientException(
                    "Cannot load the credentials from the credential profiles file. " +
                            "Please make sure that your credentials file is at the correct " +
                            "location (~/.aws/credentials), and is in valid format.",
                    e)
        }*/

        //AmazonS3 s3 = AmazonS3ClientBuilder.standard()
        //      .withCredentials(new AWSStaticCredentialsProvider(credentials))
        //    .withRegion("ap-south-1")
        //  .build()

        String bucketName = BUCKET_NAME


//        System.out.println("===========================================")
//        System.out.println("Getting Started with Amazon S3")
//        System.out.println("===========================================\n")
        try {
            /*
             * Create a new S3 bucket - Amazon S3 bucket names are globally unique,
             * so once a bucket name has been taken by any user, you can't create
             * another bucket with that same name.
             *
             * You can optionally specify a location for your bucket if you want to
             * keep your data closer to your applications or users.
             */
//            System.out.println("Deleting an object\n");
            s3.deleteObject(bucketName, key);
        }
        catch (AmazonServiceException ase) {
//            System.out.println("Caught an AmazonServiceException, which means your request made it "
//                    + "to Amazon S3, but was rejected with an error response for some reason.")
//            System.out.println("Error Message:    " + ase.getMessage())
//            System.out.println("HTTP Status Code: " + ase.getStatusCode())
//            System.out.println("AWS Error Code:   " + ase.getErrorCode())
//            System.out.println("Error Type:       " + ase.getErrorType())
//            System.out.println("Request ID:       " + ase.getRequestId())
        } catch (AmazonClientException ace) {
//            System.out.println("Caught an AmazonClientException, which means the client encountered "
//                    + "a serious internal problem while trying to communicate with S3, "
//                    + "such as not being able to access the network.")
//            System.out.println("Error Message: " + ace.getMessage())
        }
    }
    // downloading file from AWS
    InputStream downloadContentFromBucket(String bucket,String region,String key){
        final BUCKET_NAME = bucket

        /*
         * The ProfileCredentialsProvider will return your [default]
         * credential profile by reading from the credentials file located at
         * (~/.aws/credentials).
         */
        /*  AWSCredentials credentials = null
          try {
              credentials = new ProfileCredentialsProvider().getCredentials()
          } catch (Exception e) {
              throw new AmazonClientException(
                      "Cannot load the credentials from the credential profiles file. " +
                              "Please make sure that your credentials file is at the correct " +
                              "location (~/.aws/credentials), and is in valid format.",
                      e)
          }

          AmazonS3 s3 = AmazonS3ClientBuilder.standard()
                  .withCredentials(new AWSStaticCredentialsProvider(credentials))
                  .withRegion("ap-south-1")
                  .build()*/

        AmazonS3 s3=getCredential(region)
        String bucketName = BUCKET_NAME
//        System.out.println("Downloading an object")
        S3Object object = s3.getObject(new GetObjectRequest(bucketName, key))
//        System.out.println("Content-Type: "  + object.getObjectMetadata().getContentType())
        return object.getObjectContent()
        //displayTextInputStream(object.getObjectContent())

    }

    boolean putPDFToBucket(String bucket,String region,String key,String file,File video,String contentType)
    {
        String clientRegion = region;
        String bucketName = bucket;
        // String stringObjKeyName = "*** String object key name ***";
        String fileObjKeyName = key+file;
        String fileName = key+file;
        boolean putobj=false

        AmazonS3 s3Client
        try {
            //AWSCredentials credentials = new BasicAWSCredentials("AKIAIAB4H4AWAOQANQMQ", "UIDo4G/ZjeNU2AFlIYgPzT+zhnb20uBO6jmslDKJ");
            // credentials = new ProfileCredentialsProvider().getCredentials();
//            AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
//                    .withRegion(clientRegion)
//                    .withCredentials(new ProfileCredentialsProvider(credentials))//(new ProfileCredentialsProvider())
//                    .build();
            //BasicAWSCredentials creds = new BasicAWSCredentials("AKIAIAB4H4AWAOQANQMQ", "UIDo4G/ZjeNU2AFlIYgPzT+zhnb20uBO6jmslDKJ");
            //s3Client= AmazonS3ClientBuilder.standard().withCredentials(new AWSStaticCredentialsProvider(creds)).withRegion(clientRegion).build();
            // Upload a file as a new object with ContentType and title specified.
            s3Client=getCredential(region)
            println("PATH"+fileName)
            ObjectMetadata metadata = new ObjectMetadata()
            metadata.setContentType(contentType)
            metadata.addUserMetadata("x-amz-meta-title", "eduplusnow");

            PutObjectRequest request = new PutObjectRequest(bucketName, fileObjKeyName,video)

            s3Client.putObject(request)
            s3Client.setObjectAcl(bucketName, fileObjKeyName, CannedAccessControlList.Private)
            putobj=true
        }
        catch(AmazonServiceException e) {
            // The call was transmitted successfully, but Amazon S3 couldn't process
            // it, so it returned an error response.
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            // Amazon S3 couldn't be contacted for a response, or the client
            // couldn't parse the response from Amazon S3.
            e.printStackTrace();
        }



        return putobj

    }

//    def s3downloaddirectory(string dir_path) {
//        AmazonS3 s3=getCredential(region)
//        s3.downloaddirectory(dir_path)
//    }


    def downloaddirectory(String key, String dir_path) {

        TransferManager xfer_mgr = TransferManagerBuilder.standard().withS3Client(getCredential("ap-south-1")).build();
        AWSBucket awsBucket = AWSBucket.findByContent("documents")
        try {
            MultipleFileDownload xfer = xfer_mgr.downloadDirectory(
                    awsBucket.bucketname, key, new File(dir_path));
            // loop with Transfer.isDone()
            XferMgrProgress.showTransferProgress(xfer);
            // or block with Transfer.waitForCompletion()
            XferMgrProgress.waitForCompletion(xfer);
        } catch (AmazonServiceException e) {
            System.err.println(e.getErrorMessage());
            System.exit(1);
        }
        xfer_mgr.shutdownNow();
    }


    /*String testCloudFront(String key)
    {
        AWSBucket awsBucket=AWSBucket.findByContent("videourl")
        String distributionDomainName=awsBucket.distributiondomainname
        def url = PrivateKey.list().path
        // the private key you created in the AWS Management Console
        File cloudFrontPrivateKeyFile =new File(url[1]);
        // the unique ID assigned to your CloudFront key pair in the console
        Awscredentials awsc = Awscredentials.findByName("cloudfront")
        String cloudFrontKeyPairId = awsc.keyid
        Date expirationDate = new Date(System.currentTimeMillis() + 60 * 1000);
        // Protocol protocol = SignerUtils.Protocol.https;

        String signedUrl = CloudFrontUrlSigner.getSignedURLWithCannedPolicy(
                SignerUtils.Protocol.https,
                distributionDomainName,
                cloudFrontPrivateKeyFile,
                key, // the resource path to our content
                cloudFrontKeyPairId,
                expirationDate);
        return signedUrl;
    }*/


    String getPresignedUrl(String bucket, String key,String region) {
//        println("getPresignedUrl :: " + bucket + ", " + key + ", " + region)
          AmazonS3 s3=getCredential(region)
        def bucketName = bucket
          try {
              AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                      .withRegion(region)
                      .withCredentials(new ProfileCredentialsProvider())
                      .build();

              //s3Client.setObjectAcl(bucketName, key, CannedAccessControlList.Private)

              // Set the presigned URL to expire after one hour.
              java.util.Date expiration = new java.util.Date();
              long expTimeMillis = expiration.getTime();
              expTimeMillis += 1000 * 60 * 60;
              expiration.setTime(expTimeMillis);

              // Generate the presigned URL.
              //System.out.println("Generating pre-signed URL.");
              GeneratePresignedUrlRequest generatePresignedUrlRequest =
                      new GeneratePresignedUrlRequest(bucketName, key)
                              .withMethod(HttpMethod.GET)
                              .withExpiration(expiration);
              URL url = s3.generatePresignedUrl(generatePresignedUrlRequest);
//              System.out.println("Pre-Signed URL: " + url.toString());
              return url
          } catch (AmazonServiceException e) {
              // The call was transmitted successfully, but Amazon S3 couldn't process
              // it, so it returned an error response.
              e.printStackTrace();
          } catch (SdkClientException e) {
              // Amazon S3 couldn't be contacted for a response, or the client
              // couldn't parse the response from Amazon S3.
              e.printStackTrace();
          }
      }


    boolean CopyObjectToBucket(String bucket,String region, String keynew, String keyold){
        String bucketName = bucket;
        boolean putobj=false
        try {
            AmazonS3 s3Client=getCredential(region)
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.addUserMetadata("x-amz-meta-title", "eduplusnow");
            s3Client.copyObject(bucketName, keyold, bucketName, keynew);
            s3Client.setObjectAcl(bucketName, keynew, CannedAccessControlList.Private);
            putobj=true
        }
        catch(AmazonServiceException e) {
            e.printStackTrace();
        }
        catch(SdkClientException e) {
            e.printStackTrace();
        }
        return putobj
    }
}