<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
              <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">
                                <div class="card card-topline-purple">
                                   <div class="card-head">
                                   <header class="erp-table-header">PO Details List</header>
                                     </div>
                                     <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">PON</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Qty</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Cost/Unit</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invPODetailsList}" var="PODetails" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PODetails?.invpurchaseorder?.pon}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PODetails?.invmaterial?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PODetails?.quantity}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PODetails?.cost_per_unit}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PODetails?.total_cost}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <g:if test="${isapproved}">
                                                            <label for="pwd">PO Already Approved,Cannot Edit</label>
                                                         </g:if>
                                                         <g:else>
                                                           <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editPODetails${i}"></i>
                                                         </g:else>

                                                        <!--Modal Edit-->
                                                                                                        <div class="modal fade" id="editPODetails${i}" role="dialog">
                                                                                                                   <div class="modal-dialog">
                                                                                                                       <div class="modal-content">
                                                                                                                           <div class="modal-header">
                                                                                                                               <h4 class="modal-title">Edit PO Details</h4>
                                                                                                                               <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                                                                           </div>
                                                                                                                           <g:form action="editPODetails">
                                                                                                                               <div class="modal-body">

                                                                                                                                           <div class="form-group">
                                                                                                                                           <input type="hidden" name="POID" value="${POID}">
                                                                                                                                           <input type="hidden" name="PODetailsId" value="${PODetails?.id}">
                                                                                                                                                   <label for="pwd">Material: </label>
                                                                                                                                                   <span style="color:red; font-size: 16px">*</span> <br>
                                                                                                                                                   <g:select name="materialId"  optionValue="name" from="${invMaterialList}" style="width: 100% !important;"  class="select2" required="true" value="${PODetails?.invmaterial?.id}" optionKey="id"/><br>
                                                                                                                                           </div>
                                                                                                                                           <div class="form-group">
                                                                                                                                                   <label for="pwd">Cost Per Unit:</label>
                                                                                                                                                   <span style="color:red; font-size: 16px">*</span>
                                                                                                                                                   <input type="number" class="form-control" name="costPerUnit" value="${PODetails?.cost_per_unit}" required="true">
                                                                                                                                           </div>
                                                                                                                                           <div class="form-group">
                                                                                                                                                   <label for="pwd">Quantity:</label>
                                                                                                                                                   <span style="color:red; font-size: 16px">*</span>
                                                                                                                                                   <input type="number" class="form-control" name="quantity" value="${PODetails?.quantity}" required="true" optionValue="quantity">
                                                                                                                                           </div>
                                                                                                                                           <!--
                                                                                                                                           <div class="form-group">
                                                                                                                                                   <g:if test="${PODetails?.isactive}">
                                                                                                                                                       <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                                                                                   </g:if>
                                                                                                                                                   <g:else>
                                                                                                                                                       <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                                                                                   </g:else>
                                                                                                                                           </div>
                                                                                                                                           -->
                                                                                                                                           <div class="modal-footer">
                                                                                                                                                   <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                                                                                           </div>
                                                                                                                               </div>
                                                                                                                           </g:form>
                                                                                                                       </div>
                                                                                                                   </div>
                                                                                                           </div>
                                                        </td>

                                                    </tr>
                                                </g:each>
                                                </tbody>
                                               </table>


      </div>
    </div>
  </div>
  </div>
</body>
</html>



