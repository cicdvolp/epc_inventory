<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-purple">
            <div class="card-head">
                <header class="erp-table-header">
                    Approving History for Vendor Quotation's : ${invQuotationEscalation?.invpurchaserequisition?.prn} - ${invQuotationEscalation?.invpurchaserequisition?.purpose}
                </header>
            </div>

                <div class="table-responsive">
                    <table class="mdl-data-table ml-table-bordered">
                        <thead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric">Level No</th>
                                <th class="mdl-data-table__cell--non-numeric">Approving Authority</th>
                                <th class="mdl-data-table__cell--non-numeric">Approving Status</th>
                                <th class="mdl-data-table__cell--non-numeric">Action Date</th>
                                <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                <th class="mdl-data-table__cell--non-numeric">Approved By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${list}" var="pr" status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">${pr?.invfinanceapprovingauthoritylevel?.level_no}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${pr?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</td>
                                    <g:if test="${pr?.invapprovalstatus?.name != 'In-Process'}">
                                        <td class="mdl-data-table__cell--non-numeric">${pr?.invapprovalstatus?.name}</td>
                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd/MM/yyyy" date="${pr?.action_date}"/></td>
                                        <td class="mdl-data-table__cell--non-numeric">${pr?.remark}</td>
                                        <td class="mdl-data-table__cell--non-numeric">${pr?.actionby}</td>
                                    </g:if>
                                    <g:else>
                                        <td class="mdl-data-table__cell--non-numeric">Not Approved</td>
                                        <td class="mdl-data-table__cell--non-numeric">-</td>
                                        <td class="mdl-data-table__cell--non-numeric">-</td>
                                        <td class="mdl-data-table__cell--non-numeric">-</td>
                                    </g:else>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
                </div>

        </div>
    </div>
</div>