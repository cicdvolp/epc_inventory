<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
         <script>
            function fetchPODetails(){
                         var PO= document.getElementById("poNo").value;
                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("PODetails").innerHTML = this.responseText;
                            }
                        };
                        xmlhttp.open("GET", "${request.contextPath}/InventoryPurchase/getValuesByPO?PONO=" + PO);
                        xmlhttp.send();

                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("Recieptlist").innerHTML = this.responseText;
                            }
                        };
                        xmlhttp.open("GET", "${request.contextPath}/InventoryPurchase/fetchAllRecieptByPO?PONO=" + PO);
                        xmlhttp.send();

                        var xmlhttp = new XMLHttpRequest();
                        xmlhttp.onreadystatechange = function() {
                            if (this.readyState == 4 && this.status == 200) {
                                document.getElementById("invoicediv").innerHTML = this.responseText;
                            }
                        };
                        xmlhttp.open("GET", "${request.contextPath}/InventoryPurchase/getInvoiceByPO?PONO=" + PO);
                        xmlhttp.send();
            }
            function call() {
                                var vendor= document.getElementById("vendor").value;
                                var xmlhttp = new XMLHttpRequest();
                                xmlhttp.onreadystatechange = function() {
                                    if (this.readyState == 4 && this.status == 200) {
                                        document.getElementById("podiv").innerHTML = this.responseText;
                                    }
                                };
                                xmlhttp.open("GET", "${request.contextPath}/InventoryPurchase/getPOValuesByVendor?vendor=" + vendor);
                                xmlhttp.send();
                           }
            function getValuesByInvoice(){
                            var invoice= document.getElementById("invoice").value;
                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function() {
                                if (this.readyState == 4 && this.status == 200) {
                                    document.getElementById("invoiceDetailsdiv").innerHTML = this.responseText;
                                }
                            };
                            xmlhttp.open("GET", "${request.contextPath}/InventoryPurchase/getValuesByInvoice?invoice=" + invoice);
                            xmlhttp.send();
            }


         </script>

    </head>

     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <g:if test="${flash.error}">
                             <div class="alert alert-danger" >${flash.error}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12 mobile-table-responsive">
                            <div class="card card-topline-purple ">
                                <div class="card-head">
                                    <header class="erp-table-header">Add Reciept</header>
                                </div>
                                <div class="card-body" id="bar-parent">
                                       <g:form action="saveReciept" name="form" enctype="multipart/form-data" >
                                            <div class="row">
                                               <div class="col-sm-5">
                                               <label for="pwd">Vendor: </label>
                                               <span style="color:red; font-size: 16px">*</span><br>
                                               <g:select name="vendor" id="vendor"  from="${invVendorList?.company_name}"
                                                class="form-control select2"
                                               optionValue="${company_name}"
                                               noSelection="['null':'Please Select Vendor']" onchange="call()"
                                               required="true"
                                               />
                                               </div>
                                               <div class="col-sm-4" id="podiv">
                                               </div>
                                               <div class="col-sm-3" id="invoicediv">
                                                </div>
                                            </div>

                                               <div class="form-group" id="PODetails">
                                               </div>
                                               <div class="form-group" id=invoiceDetailsdiv>
                                               </div>
<br>
                                               <div class="row" >
                                                  <div class="col-sm-12">
                                                      <center><b>Enter Following Details</b></center>
                                                   </div>
                                               </div>


                                               <div class="row">
                                                    <div class="col-sm-4">
                                                            <label for="rating">Amount : </label>
                                                            <span style="color:red; font-size: 16px">*</span>
                                                            <input class="form-control" id="recieptAmt" name="recieptAmt" type="number" min="0" required="true" value="${e?.type}">

                                                     </div>
                                                    <div class="col-sm-4">
                                                            <label for="inventory">Date: </label> <br>
                                                            <span style="color:red; font-size: 16px">*</span>
                                                            <g:datePicker name="recieptDate" precision="day" class="form-control" value="${recieptDate}" required="true"/>
                                                    </div>
                                                     <div class="col-sm-4">
                                                                 <label for="rating">Payment Method : </label>
                                                                 <span style="color:red; font-size: 16px">*</span>
                                                                 <g:select name="paymentMethod" id="paymentMethod"  from="${paymentMethodList?.name}"
                                                               class="form-control select2"
                                                                optionValue="${name}"
                                                                noSelection="['null':'Please Select Payment Method']"
                                                                required="true"
                                                                />
                                                     </div>
                                               </div>

                                               <div class="row">
                                                                    <div class="col-sm-4">
                                                                       <label for="rating">Transaction Id : </label>
                                                                       <span style="color:red; font-size: 16px">*</span>
                                                                       <g:field id="recieptTransId" class="form-control" name="recieptTransId" type="number" min="0"  required="true"/>
                                                                    </div>
                                                                     <div class="col-sm-4">
                                                                                       <label for="rating">Bank Name : </label>
                                                                                       <span style="color:red; font-size: 16px">*</span>
                                                                                       <g:field id="bankName" name="bankName"  class="form-control"  type="text" min="0" required="true"/>
                                                                                    </div>
                                                                                     <div class="col-sm-4">
                                                                                       <label for="rating">IFSC Code : </label>
                                                                                       <span style="color:red; font-size: 16px">*</span>
                                                                                       <g:field id="IFSC" name="IFSC" type="text" min="0"  class="form-control"  required="true"/>
                                                                                    </div>
                                                   </div>

                                                   <div class="row">

                                                          <div class="col-sm-4">
                                                            <label for="rating">Bank Address : </label>
                                                            <span style="color:red; font-size: 16px">*</span>
                                                            <g:field id="bankAdd" name="bankAdd" type="text" min="0" class="form-control" required="true"/>
                                                          </div>
                                                          <div class="col-sm-4">
                                                             <label for="rating">Account Number : </label>
                                                             <span style="color:red; font-size: 16px">*</span>
                                                             <g:field id="bankAccNo" name="bankAccNo" type="number" class="form-control" min="0" required="true"/>
                                                          </div>
                                                           <div class="col-sm-4">
                                                               <label for="rating">Remarks : </label>
                                                                 <span style="color:red; font-size: 16px">*</span>
                                                                 <g:field id="remark" name="remark" type="text" min="0" class="form-control"/>
                                                          </div>
                                                    </div>
<br>


                                                <div class="row" style="margin-top:20px;margin-left:20px">
                                                    <div class="form-group row mx-auto">
                                                         <button type="submit" class="btn btn-primary"  > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                    </div>
                                                </div>
                                     </g:form>
                                </div>
                            </div>
                        </div>

                                             <div class="card-body" id="Recieptlist">

                                             </div>

                    </div>

                     <!--END OF INVOICE LIST-->
                </div>
            </div>
      </body>
</html>


