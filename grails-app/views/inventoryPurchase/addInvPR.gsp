<!doctype html>
<html>
   <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="pratik"/>
         <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
        <script>
        function fetchBudgetInfo() {
                            var budgetid= document.getElementById("inv_budget").value;
                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function() {
                                if (this.readyState == 4 && this.status == 200) {
                                    document.getElementById("budgetAY").innerHTML = this.responseText;
                                }
                            };
                            xmlhttp.open("GET", "${request.contextPath}/InventoryPurchase/fetchBudgetInfo?budgetId=" + budgetid);
                            xmlhttp.send();
                       }
        </script>
    </head>
<body>
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Add New Purchase Requisition</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li>
                            <i class="fa fa-home"></i>
                            &nbsp;
                            <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                            &nbsp;
                            <i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active">Add New Purchase Requisition</li>
                    </ol>
                </div>
            </div>
            <g:if test="${flash.message}">
                <div class="alert alert-success" >${flash.message}</div>
            </g:if>
            <g:if test="${flash.error}">
                <div class="alert alert-danger" >${flash.error}</div>
            </g:if>
            <!------------------ Add New Purchase Requisition --------------------->
            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-body">
                            <g:form action="saveInvPR" enctype="multipart/form-data">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label for="pwd">Budget List <span style="color:red; font-size: 16px">*</span>:</label><br>
                                        <g:select  name="inv_budget"  optionValue="activity_name" from="${inv_budget_list}" style="width: 100% !important;"  class="form-control select2"  required="true"  value=" "  noSelection="['null':'Select Budget']" optionKey="id" onchange="fetchBudgetInfo()"/><br>
                                    </div>
                                    <div class="col-sm-6">
                                         <label for="pwd">Purpose <span style="color:red; font-size: 16px">*</span> :</label><br>
                                         <g:field  id="purpose" name="purpose" value=""  class="form-control" required="true"/><br>
                                     </div>
                                   </div>
                                   <div class="row" id="budgetAY">
                                           <div class="col-sm-6">
                                                            <!--DIV Will be shown after budget is selected-->
                                           </div>
                                   </div>
                                <div class="row">
                                <!--
                                     <div class="col-sm-6">
                                         <label for="pwd">Purchase Type <span style="color:red; font-size: 16px">*</span> :</label><br>
                                         <g:select name="inv_purchase_type"  optionValue="name" from="${inv_purchase_type_list}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Purchase Type']" optionKey="id"/><br>
                                     </div>                                     -->

                                </div>
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>File <span style="color:red; font-size: 16px">*</span> :</label>
                                        <input type="file" class="form-control btn-primary" name="newFile">
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                </div>
                            </g:form>
                        </div>
                    </div>
                </div>
            </div>
            <!------------------ End Add New Purchase Requisition ------------------>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card card-topline-purple">
                        <div class="card-head">
                            <header class="erp-table-header">Purchase Requisition List</header>
                        </div>
                        <div class="card-body " id="bar-parent">
                            <div class="table-responsive">
                                <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                            <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                            <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                            <th class="mdl-data-table__cell--non-numeric">PR Date</th>
                                            <th class="mdl-data-table__cell--non-numeric">PRN</th>
                                            <th class="mdl-data-table__cell--non-numeric">Level</th>
                                            <th class="mdl-data-table__cell--non-numeric">Purchase Type</th>
                                            <th class="mdl-data-table__cell--non-numeric">Deparment</th>
                                            <th class="mdl-data-table__cell--non-numeric">Total Cost in Rs</th>
                                            <th class="mdl-data-table__cell--non-numeric">PRN File</th>
                                            <th class="mdl-data-table__cell--non-numeric">PR Status</th>
                                            <th class="mdl-data-table__cell--non-numeric">Budget</th>
                                            <th class="mdl-data-table__cell--non-numeric">View Budget Details</th>
                                            <th class="mdl-data-table__cell--non-numeric">Add PR Item</th>
                                            <th class="mdl-data-table__cell--non-numeric">View PR Item</th>
                                            <th class="mdl-data-table__cell--non-numeric">Is Active?</th>
                                            <th class="mdl-data-table__cell--non-numeric">Is-approved?</th>
                                            <th class="mdl-data-table__cell--non-numeric">Approved By</th>
                                            <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                            <!--
                                            <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <g:each in="${InvPurchaseRequisition}" var="name" status="i">
                                            <tr>
                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.purpose}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.academicyear?.ay}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.request_date}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.prn}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.invbudget?.invbudgetlevel?.name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.invpurchasetype?.name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.invbudget?.department?.name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.amount}</td>
                                                <td class="mdl-data-table__cell--non-numeric"><a href="${name.url}" target="_blank">${name?.data?.file_name}</a></td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.invapprovalstatus?.name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.data?.invbudget?.activity_name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">
                                                   <center>  <g:link controller="inventoryPurchase" action="viewSingleBudgetDetails" params="[invBudgetId : name?.data?.invbudget?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link> </center>
                                                    <!-- View budget Modal -->
                                                    <div class="modal fade" id="addModal1" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="card-body " id="bar-parent">
                                                                    <div class="table-responsive">
                                                                        <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                                    <th class="mdl-data-table__cell--non-numeric">Material</th>
                                                                                    <th class="mdl-data-table__cell--non-numeric">Quantity</th>
                                                                                    <th class="mdl-data-table__cell--non-numeric">Cost Per Unit</th>
                                                                                    <th class="mdl-data-table__cell--non-numeric">Total Cost</th>
                                                                                    <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                            <<g:if test="${invBudgetDetailsList}">
                                                                               <g:each in="${invBudgetDetailsList}" var="budgetDetails" status="g">
                                                                                    <tr>
                                                                                        <td class="mdl-data-table__cell--non-numeric">${g+1}</td>
                                                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.invmaterial?.name}</td>
                                                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.quantity_required}</td>
                                                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.approx_cost_per_unit}</td>
                                                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.total_cost}</td>
                                                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.remark}</td>
                                                                                    </tr>
                                                                                </g:each>
                                                                            </g:if>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </div> 
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- End view budget Modal -->
                                                </td>
                                                <td class="mdl-data-table__cell--non-numeric">
                                                     <center> <g:if test="${name?.data?.isapproved}">
                                                        <label for="pwd">PR Already Approved,Cannot Add</label>
                                                     </g:if>
                                                     <g:else>
                                                        <i class="fa fa-plus  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#addPRDetails${i}"></i>
                                                     </g:else> </center>

                                                       <!-- ADD PR Modal-->
                                                       <div class="modal fade" id="addPRDetails${i}" role="dialog">
                                                              <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                  <div class="modal-header">
                                                                    <h4 class="modal-title">Add New PR Details</h4>
                                                                    <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                  </div>
                                                                 <g:form action="savePRItem">
                                                                  <div class="modal-body">
                                                                    <div class="form-group">
                                                                        <label for="pwd">PRN:</label>
                                                                        <input type="hidden" value="${name?.data?.id}" name="PRID">
                                                                        <input type="text" class="form-control" name="prn" required="true" value="${name?.data?.prn}" disabled optionKey="id">
                                                                    </div>
                                                                   <div class="form-group">
                                                                    <label for="pwd">Material:</label>
                                                                    <span style="color:red; font-size: 16px">*</span>
                                                                    <br>
                                                                        <g:select name="materialId"  optionValue="name" from="${inv_material_list}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                                                  </div>
                                                                   <div class="form-group">
                                                                        <label for="pwd">Cost Per Unit:</label>
                                                                        <span style="color:red; font-size: 16px">*</span>
                                                                        <input type="number" min=0 class="form-control" name="costPerUnit" required="true">
                                                                    </div>
                                                                    <div class="form-group">
                                                                      <label for="pwd">Quantity:</label>
                                                                      <span style="color:red; font-size: 16px">*</span>
                                                                      <input type="number" min=0 class="form-control" name="quantity" required="true">
                                                                  </div>
                                                                <div class="form-group">
                                                                      <label for="pwd">Remarks:</label>
                                                                      <span style="color:red; font-size: 16px">*</span>
                                                                      <input type="text" class="form-control" name="remarks" required="true">
                                                                  </div>
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                                  </div>
                                                                 </g:form>
                                                       <!--END ADD PR Modal-->
                                                 </td>
                                                 <td> <center>
                                                  <g:link controller="inventoryPurchase" action="viewSinglePRDetails" params="[PRID : name?.data?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                  </center></td>
                                                <td class="mdl-data-table__cell--non-numeric">
                                                     <center><g:link controller="inventoryPurchase" action="activationInvPR" params="[inv_purchase_requisition : name?.data?.id]">
                                                        <g:if test="${name?.data?.isactive}">
                                                            <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                        </g:if>
                                                        <g:else>
                                                            <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                        </g:else>
                                                    </g:link> </center>
                                                </td>
                                                <td class="mdl-data-table__cell--non-numeric">
                                                     <center><g:if test="${name?.data?.isapproved}">
                                                        <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                    </g:if>
                                                    <g:else>
                                                        <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                    </g:else> </center>
                                                </td>
                                                <td>
                                                      <center>   <g:each in="${name?.escalation}" var="app" status="a">
                                                            <g:if test="${app?.invapprovalstatus?.name == 'Approved'}">
                                                                <!--  <i class="fa fa-user fa-2x" style="color:green" aria-hidden="true" title="${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}"></i>-->
                                                                  <strong>${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</strong>:-
                                                                  ${app?.invapprovalstatus?.name}
                                                            </g:if>
                                                            <g:else>
                                                              <!--  <i class="fa fa-user fa-2x" style="color:red" aria-hidden="true" title="${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}"></i>-->
                                                                <strong>${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</strong>:-
                                                                ${app?.invapprovalstatus?.name}
                                                            </g:else>
                                                              <br>
                                                        </g:each> </center>
                                                </td>
                                                <td class="mdl-data-table__cell--non-numeric">
                                                     <g:if test="${name?.data?.isapproved}">
                                                        <label for="pwd">PR Already Approved,Cannot Edit</label>
                                                     </g:if>
                                                     <g:else>
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>
                                                    </g:else>
                                                    </td>
                                                       <!-- Modal edit -->
                                                    <div class="modal fade" id="editModel${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                            <div class="modal-content">
                                                                <div class="col-sm-12">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Purchase Requisition</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="saveInvPR" enctype="multipart/form-data">
                                                                        <div class="modal-body">
                                                                            <div class="row">
                                                                                <div class="col-sm-6">
                                                                                    <label for="pwd">Academic Year <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                    <input type="text" class="form-control" name="academic_year" required="true" value="${name?.data?.academicyear}" readonly>
                                                                                 </div>
                                                                                <div class="col-sm-6">
                                                                                    <label for="pwd">PRN <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                    <input type="number" class="form-control" name="prn" required="true" value="${name?.data?.prn}" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Purpose <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="text" class="form-control" name="purpose" required="true" value="${name?.data?.purpose}">
                                                                            </div>
                                                                            <div class="row"> 
                                                                                <div class="col-sm-6">
                                                                                    <label for="pwd">Budget Level <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                    <input type="text" class="form-control" name="inv_budget_level" required="true" value="${name?.data?.invbudget?.invbudgetlevel?.name}" readonly>

                                                                                </div>
                                                                                <div class="col-sm-6">
                                                                                    <label for="pwd">Department :</label><br>
                                                                                    <input type="text" class="form-control" name="inv_budget_level" required="true" value="${name?.data?.invbudget?.department?.name}" readonly>
                                                                                    </div>
                                                                            </div>
                                                                            <div class="row"> 
                                                                                <div class="col-sm-6">
                                                                                    <label for="pwd">Budget Type <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                    <input type="text" class="form-control" name="inv_budget_level" required="true" value="${name?.data?.invbudget?.invbudgettype?.name}" readonly>
                                                                                    </div>
                                                                                <div class="col-sm-6">
                                                                                    <label for="pwd">Activity <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                    <input type="text" class="form-control" name="inv_budget_level" required="true" value="${name?.data?.invbudget?.activity_name}" readonly>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>File <span style="color:red; font-size: 16px">*</span> :</label>
                                                                                <input type="file" class="form-control btn-primary" name="newFile">
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="invpr" id="invpr" value="${name?.data?.id}" />
                                                                            <input type="hidden" name="inv_budget" id="inv_budget" value="${name?.data?.invbudget?.id}">
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--End Modal edit -->
                                                </td>
                                                <!--
                                                <td class="mdl-data-table__cell--non-numeric">
                                                    <g:link controller="inventoryPurchase" action="deleteInvPR" params="[inv_purchase_requisition : name?.data?.id]">
                                                        <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                    </g:link>
                                                </td>
                                                -->
                                            </tr>
                                        </g:each>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </div>
</body>
</html>