<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-purple">
            <div class="card-head">
                <header class="erp-table-header">
                    Category : ${invFinanceApprovingCategory?.name}, Approving Authority Post : ${invInstructorFinanceApprovingAuthority?.invfinanceapprovingauthority?.name}
                </header>
            </div>
            <!--<div class="card-body">-->
                <div class="table-responsive">
                    <table class="mdl-data-table ml-table-bordered">
                        <thead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                <th class="mdl-data-table__cell--non-numeric">Purchase Order Details</th>
                                <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${invPOEscalation}" var="po" status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <span>Remark : </span><span class="erp-table-header" style="font-weight:900;">${po?.remark}</span>
                                        <br>
                                        <span>Request Date : </span><span class="erp-table-header" style="font-weight:900;">${po?.action_date}</span>
                                        <br>
                                        <span>PON : </span><span class="erp-table-header" style="font-weight:900;">${po?.invpurchaseorder?.pon}</span>
                                        <br>
                                        <span>Vendor : </span><span class="erp-table-header" style="font-weight:900;">${po?.invvendor?.company_name}</span>
                                        <br>
                                        <span>Action By : </span><span class="erp-table-header" style="font-weight:900;">${po?.actionby}</span>
                                        <br>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:link target="blank" controller="inventoryPurchase" action="getPOEscalationforApprove" params="[poe_id:po?.id]" class="btn btn-primary">Proceed</g:link>
                                    </td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
            <!--</div>-->
        </div>
    </div>
</div>