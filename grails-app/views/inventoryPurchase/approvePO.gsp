<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">
                                <div class="card card-topline-purple">
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Action Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Level No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval Status</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Remarks</th>
                                                    <!--
                                                    <th class="mdl-data-table__cell--non-numeric">View Budget Details</th>
                                                    -->
                                                    <th class="mdl-data-table__cell--non-numeric">Approve PO</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View History</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invPOEscalationList}" var="PO" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.updation_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.invfinanceapprovingauthoritylevel?.level_no}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.invapprovalstatus?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.remark}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#updatePOStatus${i}"></i>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="viewApprovalHistory">
                                                                  <input type="hidden" value="${PO?.invpurchaseorder?.id}" name="invPOId">
                                                                        <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                            </g:form>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="updatePOStatus${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title">Update Approval Status</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="updatePOStatus">
                                                                <div class="modal-body">
                                                                      <div class="form-group">
                                                                          <input type="hidden" value="${PO?.invpurchaseorder?.id}" name="invPOId">
                                                                              <input type="hidden" value="${PO?.id}" name="POEId">
                                                                       </div>
                                                                      <div>
                                                                        <label for="pwd">Approval Status:</label>
                                                                            <g:select name="approvalStatusId"  optionValue="name" from="${invApprovalStatusList}" style="width: 100% !important;"  class="select2" required="true"  value="${PO?.id}" optionKey="id"/><br>
                                                                      </div>
                                                                      <div>
                                                                      <label for="pwd">Remarks:-</label>
                                                                      <input type="text" value="${PO?.remark}" class="form-control" name="remark" required="true">
                                                                      </div>
                                                                 </div>
                                                                <div class="modal-footer">
                                                                  <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



