<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">
                    <!--
                    <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple full-scroll-table">
                                -->
                                <div class="card card-topline-purple">
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Action Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Authority Level</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval Status</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invPOEscalation}" var="POEscalation" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${POEscalation?.action_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${POEscalation?.remark}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${POEscalation?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${POEscalation?.invapprovalstatus?.name}</td>

                                                    </tr>
                                                </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



