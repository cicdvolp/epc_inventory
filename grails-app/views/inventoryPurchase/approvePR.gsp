<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="pratik"/>
         <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Approve Purchase Requisition</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Approve Purchase Requisition</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                 <!------------------ Fetch Purchase Requisition --------------------->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <g:form action="fetchApprovePR">
                                    <div class="row">
                                    <div class="col-sm-6">
                                            <label for="pwd">Academic Year <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <g:select name="academic_year"  optionValue="ay" from="${academic_year_list}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="pwd">Purchase Type <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <g:select name="inv_purchase_type"  optionValue="name" from="${inv_purchase_type_list}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Purchase Type']" optionKey="id"/><br>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="pwd">Department :</label><br>
                                            <g:select name="department"  optionValue="name" from="${department_list}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Department']" optionKey="id"/><br>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="pwd">Approval Status <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <g:select name="inv_approval_status"  optionValue="name" from="${inv_approval_status_list}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Approval Status']" optionKey="id"/><br>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Fetch</button>
                                    </div>
                                </g:form>
                            </div>
                        </div>
                    </div>
                </div>
                <!------------------ End Fetch Purchase Requisition ------------------>

                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                <th class="mdl-data-table__cell--non-numeric">PR Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">PRN</th>
                                                <th class="mdl-data-table__cell--non-numeric">Level</th>
                                                <th class="mdl-data-table__cell--non-numeric">Deparment</th>
                                                <th class="mdl-data-table__cell--non-numeric">Budget Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">Activity</th>
                                                <th class="mdl-data-table__cell--non-numeric">Total Cost in Rs</th>
                                                <th class="mdl-data-table__cell--non-numeric">PRN File</th>
                                                <th class="mdl-data-table__cell--non-numeric">Details</th>
                                                <th class="mdl-data-table__cell--non-numeric">View Budget</th>
                                                <th class="mdl-data-table__cell--non-numeric">PRN Status</th>
                                                <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                <th class="mdl-data-table__cell--non-numeric">Purchase Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">Vendor</th>
                                                <th class="mdl-data-table__cell--non-numeric">Approval History</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Quotation Invited?</th>
                                                <th class="mdl-data-table__cell--non-numeric">Quotation</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active?</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${InvPurchaseRequisitionList}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.academicyear?.ay}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.request_date}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.prn}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invbudget?.invbudgetlevel?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.department?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invbudget?.invbudgettype?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invbudget?.activity_name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.amount}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.file_name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link  data-toggle="modal" data-target="#addModal1"><button type="" class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>View PR Details</button></g:link>
                                                        <!-- PR item Modal -->
                                                        <div class="modal fade" id="addModal1" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Add New PR Item</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="savePRItem" params="[inv_purchase_requisition : name?.id]">
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="pwd">Material <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <g:select name="invmaterial"  optionValue="name" from="${inv_material_list}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Approx. Cost Per unit <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="approx_cost_per_unit" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Quantity <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="quantity_required" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Total Cost <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="total_cost" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Remark <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="text" class="form-control" name="remark" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End PR item Modal -->
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="InvBudget" action="addNewBudgetDetails"><button type="" class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>View Budget</button></g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invapprovalstatus?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link  data-toggle="modal" data-target="#addModal1"><button type="" class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Add Remark</button></g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invpurchasetype?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invvendor?.company_registration_number}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="InvBudget" action="addNewBudgetDetails"><button type="" class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Approve History</button></g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryPurchase" action="activationInvPR" params="[inv_purchase_requisition : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>   
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link  data-toggle="modal" data-target="#addModal1"><button type="" class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Invite Vendor</button></g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryPurchase" action="activationInvPR" params="[inv_purchase_requisition : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>   
                                                    </td>                                                       
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryPurchase" action="deleteInvPR" params="[inv_purchase_requisition : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




