<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-purple">
            <div class="card-head">
                <header class="erp-table-header">
                    Category : ${invFinanceApprovingCategory?.name}, Approving Authority Post : ${invInstructorFinanceApprovingAuthority?.invfinanceapprovingauthority?.name}
                </header>
            </div>
            <!--<div class="card-body">-->
                <div class="table-responsive">
                    <table class="mdl-data-table ml-table-bordered">
                        <thead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                <th class="mdl-data-table__cell--non-numeric">Quotation Details</th>
                                <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${quotationEscalations}" var="qo" status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <span>Requested By : </span><span class="erp-table-header" style="font-weight:900;">${qo?.actionby}</span>
                                        <br>
                                        <span>Total : </span><span class="erp-table-header" style="font-weight:900;">${qo?.invquotation?.total}</span>
                                        <br>
                                        <span>Quotation Submitted By : </span><span class="erp-table-header" style="font-weight:900;">${qo?.invquotation?.invvendor?.company_name}</span>
                                        <br>
                                        <span>Quotation Submit Date : </span><span class="erp-table-header" style="font-weight:900;">${qo?.creation_date}</span>
                                        <br>
                                        <span>Quotation Approved? : </span><span class="erp-table-header" style="font-weight:900;">
                                            <g:if test="${qo?.invquotation?.isapproved}">
                                                <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                            </g:if>
                                            <g:else>
                                                <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                            </g:else>
                                        </span>
                                         <br>
                                        <span>Quotation Details : </span><span class="erp-table-header" style="font-weight:900;">
                                         <g:link controller="inventoryPurchase" action="viewQuotationDetails" params="[QuotationId : qo?.invquotation?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                        </span>
                                        <br>

                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:link target="blank" controller="inventoryPurchase" action="getQuotationEscalationforApprove" params="[qoe_id:qo?.id , QuotationId:qo?.invquotation?.id]" class="btn btn-primary">Proceed </g:link>
                                    </td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
            <!--</div>-->
        </div>
    </div>
</div>