<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="pratik"/>
         <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Purchase Requisition Escalation</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Purchase Requisition Escalation</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Purchase Requisition Escalation List</header>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Action Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Purchase Requisition</th>
                                                <th class="mdl-data-table__cell--non-numeric">Finance Approving Authority Level</th>
                                                <th class="mdl-data-table__cell--non-numeric">Approval Status</th>
                                                <th class="mdl-data-table__cell--non-numeric">Action By</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active?</th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${InvPurchaseRequisitionEscalationList}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.action_date}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invpurchaserequisition?.prn}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invfinanceapprovingauthoritylevel?.level_no}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invbudget?.invbudgetlevel?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invbudget?.department?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.amount}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.file_name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invapprovalstatus?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="InvBudget" action="addNewBudgetDetails"><button type="" class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>View Budget</button></g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link  data-toggle="modal" data-target="#addModal1"><button type="" class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Add PR Item</button></g:link>
                                                    </td>
                                                    
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryPurchase" action="activationInvPR" params="[inv_purchase_requisition : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                        <!-- PR item Modal -->
                                                        <div class="modal fade" id="addModal1" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Add New PR Item</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="savePRItem" params="[inv_purchase_requisition : name?.id]">
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="pwd">Material <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <g:select name="invmaterial"  optionValue="name" from="${inv_material_list}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Approx. Cost Per unit <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="approx_cost_per_unit" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Quantity <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="quantity_required" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Total Cost <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="total_cost" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Remark <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="text" class="form-control" name="remark" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- End PR item Modal -->
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                        <!-- Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Purchase Requisition</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="saveInvPR" enctype="multipart/form-data">
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="pwd">Academic Year <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <g:select name="academic_year"  optionValue="ay" from="${academic_year_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.academicyear?.id}"  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">PRN <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="text" class="form-control" name="prn" required="true" value="${name?.prn}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Purpose <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="text" class="form-control" name="purpose" required="true" value="${name?.purpose}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Budget Level <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <g:select name="inv_budget_level"  optionValue="name" from="${inv_budget_level_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.academicyear?.id}"  noSelection="['null':'Select Budget Level']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Department :</label><br>
                                                                                <g:select name="department"  optionValue="name" from="${department_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.department?.id}"  noSelection="['null':'Select Department']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Budget Type <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <g:select name="inv_budget_type"  optionValue="name" from="${inv_budget_type_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.academicyear?.id}"  noSelection="['null':'Select Budget Type']" optionKey="id" /><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Activity <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="text" class="form-control" name="activity" required="true" value="">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Budget List <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                                                <g:select  name="inv_budget"  optionValue="activity_name" from="${inv_budget_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invbudget?.id}"  noSelection="['null':'Select Budget']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Vendor List <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                                                <g:select  name="inv_vendor"  optionValue="company_registration_number" from="${inv_vendor_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invvendor?.id}"  noSelection="['null':'Select Vendor']" optionKey="id"/><br>
                                                                            </div>

                                                                            <div class="form-group">
                                                                                <label>File <span style="color:red; font-size: 16px">*</span> :</label>
                                                                                <input type="file" class="form-control btn-primary" name="newFile">
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="invpr" id="invpr" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End Modal edit -->

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryPurchase" action="deleteInvPR" params="[inv_purchase_requisition : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




