<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="pratik"/>
         <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Purchase Requisition Details</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Purchase Requisition Details</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!------------------ Add New Purchase Requisition Details --------------------->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <g:form action="savePRItem">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label for="pwd">Purchase Requisition No <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <g:select name="Inv_Purchase_Requisition" optionValue="prn" from="${Inv_Purchase_Requisition_list}" style="width: 100% !important;"  class="select2" required="true"  value="${inv_purchase_requisition}"  noSelection="['null':'Select Purchase Requisition']" optionKey="id"/><br>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="pwd">Material <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <g:select name="invmaterial"  optionValue="name" from="${Inv_Material_List}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="pwd">Approx. Cost Per unit <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <input type="number" min="0" class="form-control" name="approx_cost_per_unit" required="true" value="">
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="pwd">Quantity <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <input type="number" min="0" class="form-control" name="quantity_required" required="true" value="">
                                        </div>
                                        <!--
                                        <div class="col-sm-6">
                                            <label for="pwd">Total Cost <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <input type="number" class="form-control" name="total_cost" required="true" value="">
                                        </div>-->
                                        <div class="col-sm-6">
                                            <label for="pwd">Remark <span style="color:red; font-size: 16px">*</span> :</label><br>
                                            <input type="text" class="form-control" name="remark" required="true" value="">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                    </div>
                                </g:form>
                            </div>
                        </div>
                    </div>
                </div>
                <!------------------ End Add New Purchase Requisition Details ------------------>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Purchase Requisition Details List</header>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material</th>
                                                <th class="mdl-data-table__cell--non-numeric">Purchase Requisition</th>
                                                <th class="mdl-data-table__cell--non-numeric">Quantity Required</th>
                                                <th class="mdl-data-table__cell--non-numeric">Approx Cost Per Unit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Total Cost</th>
                                                <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active </th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${InvPurchaseRequisitionDetails}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invmaterial?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invpurchaserequisition?.prn}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.quantity_required}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.approx_cost_per_unit}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.total_cost}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.remark}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryPurchase" action="activationPRItem" params="[inv_prd : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                        <!-- Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Purchase Requisition Details</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editPRItem">
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="pwd">Purchase Requisition No <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <g:select name="invpurchaserequisition"  optionValue="prn" from="${Inv_Purchase_Requisition_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invpurchaserequisition?.id}"  noSelection="['null':'Select Purchase Requisition']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Material <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <g:select name="invmaterial"  optionValue="name" from="${Inv_Material_List}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invmaterial?.id}"  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Quantity <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="quantity_required" required="true" value="${name?.quantity_required}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Approx. Cost Per unit <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="approx_cost_per_unit" required="true" value="${name?.approx_cost_per_unit}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Total Cost <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="number" class="form-control" name="total_cost" required="true" value="${name?.total_cost}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Remark <span style="color:red; font-size: 16px">*</span> :</label><br>
                                                                                <input type="text" class="form-control" name="remark" required="true" value="${name?.remark}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <g:if test="${name?.isactive}">
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                </g:else>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="inv_prd" id="inv_prd" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End Modal edit -->
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryPurchase" action="deletePRItem" params="[inv_prd : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




