<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">

                                <div class="card card-topline-purple">
                                <div class="card-head">
                                         <header class="erp-table-header">  Approved Purchase Order List</header>

                                                                           </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">PON</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Tax</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Vendor Company</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Quotation ID</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Released By</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View PO Details</th>

                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invPOList}" var="PO" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.pon}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.tax}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.total}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.invvendor?.company_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.invquotation?.id}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.releasedby?.person?.grno}:${PO?.releasedby?.person?.firstName} ${PO?.releasedby?.person?.lastName}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <center>   <g:form action="viewPODetails">
                                                                  <input type="hidden" value="${PO?.id}" name="POID">
                                                                        <i class="fa fa-eye fa-2x erp-edit-icon-color"></i>
                                                            </g:form> </center>
                                                        </td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



