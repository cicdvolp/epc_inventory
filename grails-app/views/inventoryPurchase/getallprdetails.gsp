<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-purple">
            <div class="card-head">
                <header class="erp-table-header">
                    Category : ${invFinanceApprovingCategory?.name}, Approving Authority Post : ${invInstructorFinanceApprovingAuthority?.invfinanceapprovingauthority?.name}
                </header>
            </div>
            <!--<div class="card-body">-->
                <div class="table-responsive">
                    <table class="mdl-data-table ml-table-bordered">
                        <thead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                <th class="mdl-data-table__cell--non-numeric">Purchase Requisition</th>
                                <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${invPurchaseRequisitionEscalation_list}" var="pr" status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <span>Academic Year : </span><span class="erp-table-header" style="font-weight:900;">${pr?.invpurchaserequisition?.academicyear?.ay}</span>
                                        <br>
                                        <span>Purchase Requisition Number : </span><span class="erp-table-header" style="font-weight:900;">${pr?.invpurchaserequisition?.prn}</span>
                                        <br>
                                        <span>Purpose : </span><span class="erp-table-header" style="font-weight:900;">${pr?.invpurchaserequisition?.purpose}</span>
                                        <br>
                                        <span>Requested Date : </span><span class="erp-table-header" style="font-weight:900;"><g:formatDate format="dd/MM/yyyy" date="${pr?.invpurchaserequisition?.request_date}"/></span>
                                        <br>
                                        <span>Amount : </span><span class="erp-table-header" style="font-weight:900;">${pr?.invpurchaserequisition?.amount}</span>
                                        <br>
                                        <span>Purchase Type : </span><span class="erp-table-header" style="font-weight:900;">${pr?.invpurchaserequisition?.invpurchasetype?.name}</span>
                                        <br>
                                        <span>Department : </span><span class="erp-table-header" style="font-weight:900;">${pr?.invpurchaserequisition?.department?.abbrivation}</span>
                                        <br>
                                        <span>Requested By : </span><span class="erp-table-header" style="font-weight:900;">${pr?.invpurchaserequisition?.requestedby}</span>
                                        <br>
                                        <span>View More Details : </span><span class="erp-table-header" style="font-weight:900;">----</span>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:link target="blank" controller="inventoryPurchase" action="getPREscalationforApprove" params="[pre_id:pr?.id]" class="btn btn-primary">Proceed</g:link>
                                    </td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
            <!--</div>-->
        </div>
    </div>
</div>