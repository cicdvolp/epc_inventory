<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_inst"/>
        <meta name="author" content="rishabh"/>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card card-topline-purple">
                                    <div class="modal-header"> Quotation  Report    </div>
                                        <div class="card-body">
                                            <g:form controller="inventoryPurchase" action="getAllQuotationsByVendor">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Vendor List : </label>
                                                        <g:select name="vendor" optionValue="company_name" from="${vendorList}" class="form-control select2" required="true" optionKey="id"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <br>
                                                        <center>
                                                            <g:submitToRemote url="[action: 'getAllQuotationsByVendor', controller:'inventoryPurchase']" update="invoicelist" value="Fetch All Vendor Quotations" class="btn btn-primary" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                                        </center>
                                                    </div>
                                                </div>
                                            </g:form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    <div id="invoicelist"></div>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>