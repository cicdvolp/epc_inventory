<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_inst"/>
        <meta name="author" content="rishabh"/>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                    <g:if test="${!invFinanceApprovingCategory}">
                        <div class="alert alert-danger">Approving Authority Category not found.</div>
                    </g:if>
                    <g:else>
                        <g:if test="${invInstructorFinanceApprovingAuthority.size() == 0}">
                            <div class="alert alert-danger">You have not assign any approving post.</div>
                        </g:if>
                        <g:else>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card card-topline-purple">
                                        <div class="card-head">
                                            <header class="erp-table-header">
                                                Instructor Name : ${instructor}, Category : ${invFinanceApprovingCategory?.name}
                                        </div>
                                        <div class="card-body">
                                            <g:form controller="inventoryPurchase" action="getallprdetails">
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Approving Authority Post : </label>
                                                        <g:select name="post" optionValue="invfinanceapprovingauthority" from="${invInstructorFinanceApprovingAuthority}" class="form-control select2" required="true" optionKey="id"/>
                                                    </div>
                                                     <div class="col-sm-4">
                                                        <label>Purchase Requisition Number : </label>
                                                        <span style="color:red; font-size: 16px">*</span>
                                                        <g:select name="PRID" optionValue="prn" from="${invPRList}" class="form-control select2" required="true" optionKey="id"/>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <br>
                                                        <center>
                                                            <g:submitToRemote url="[action: 'getallprdetails', controller:'inventoryPurchase']" update="prlist" value="Fetch Purchase Requisitions" class="btn btn-primary" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                                        </center>
                                                    </div>
                                                </div>
                                            </g:form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </g:else>
                    </g:else>
                    <div id="prlist"></div>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>