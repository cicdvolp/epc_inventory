 <div class="card-body">
             <div class="row">
                    <div class="col-sm-6" id="budgetAY">
                    <label for="pwd">Academic Year:</label><br>
                    <g:field  id="budgetAY" name="academic_year" readonly="readonly" value="${budgetAY}" optionkey="id" class="form-control" /><br>
                    </div>

                    <div class="col-sm-6" id="budgetType">
                    <label for="pwd">Budget Type: </label><br>
                    <g:field  id="budgetType" name="budgetType" class="form-control" readonly="readonly" value="${budgetType?.name}" /><br>
                    </div>
            </div>
            <div class="row">
                        <div class="col-sm-6" id="budgetDept">
                        <label for="pwd">Budget Department:</label><br>
                        <g:field id="budgetDept" name="department" readonly="readonly" class="form-control" value="${budgetDept?.name}"/><br>
                        </div>

                         <div class="col-sm-6" id="budgetLevel">
                        <label for="pwd">Budget Level:</label><br>
                        <g:field  id="budgetLevel" name="budgetLevel" readonly="readonly" value="${budgetLevel?.name}" class="form-control"/><br>
                        </div>
            </div>
            <div class="row">
                    <div class="col-sm-6" id="activity_name">
                    <label for="pwd">Activity Name:</label><br>
                    <g:field  id="activity_name" name="activity_name" readonly="readonly" value="${activity_name}" class="form-control"/><br>
                    </div>
             </div>
</div>

