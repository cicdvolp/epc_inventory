<g:if test="${QFound}">
<div class="row">
    <div class="col-sm-4">
            <label for="pwd">Quotation Amount:</label>
            <g:field readonly type="text" value="${quotation?.amount}" class="form-control" name="amount"></g:field>
    </div>
     <div class="col-sm-4">
            <label for="pwd">Quotation Tax:</label>
            <g:field readonly type="text" value="${quotation?.tax}" class="form-control" name="tax" optionValue="quantity"></g:field>
    </div>
    <div class="col-sm-4">
                <label for="pwd">Quotation Total:</label>
                <g:field readonly type="text" value="${quotation?.total}" class="form-control" name="total" optionValue="quantity"></g:field>
        </div>
</div>
</g:if>
<g:else>
<br>
    <div>
                <b style="color:red;"><label for="pwd">No Quotation Approved For This PRN </label></b>
    </div>
</g:else>