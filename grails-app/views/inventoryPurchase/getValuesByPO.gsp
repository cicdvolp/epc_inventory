 <div class="row" >
          <div class="col-sm-12">
              <h4>Purchase Order Details</h4>

           </div>
       </div>

<div class="row" id="PODetails">
   <div class="col-sm-3">
<label for="invoice"> Purchase Order Number :</label>
<g:field  id="PON" name="PON" readonly="readonly" value="${pon}" class="form-control"/>
</div>
<div class="col-sm-3">
<label for="invoice"> Purpose :</label>
<g:field  id="purpose" name="purpose" readonly="readonly" value="${purpose}" class="form-control"/>
</div>
<div class="col-sm-3">
<label for="amount">Total :</label>
<g:field id="total" name="total" readonly="readonly" value="${total}" class="form-control"/>
</div>
<div class="col-sm-3">
<label for="purchase_order_date" >Purchase Order date :</label>
<g:field  id="purchase_order_date" name="purchase_order_date" readonly="readonly" value="${formatDate(format:'dd-MM-yyyy',date:purchase_order_date)}" class="form-control"/>
</div>
