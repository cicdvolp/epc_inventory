

<div class="row">
            <div class="col-md-12">
                                <div class="card card-topline-purple">
                                 <div class="card-head"> <header class="erp-table-header">Quotation Report</header>
                                                                                            </div>

                                          <div class="table-responsive">
 <table class="mdl-data-table ml-table-bordered common">

                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Quotation Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Tax</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Quotation Details</th>

                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${quotationList}" var="quo" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quo?.quotation_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quo?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quo?.tax}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quo?.total}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <center> <g:link controller="inventoryPurchase" action="viewQuotationDetailsByQuotation" params="[quotationId : quo?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                         </center></td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

 </div>
 </div>
 </div>



