

                       <div class="row">
                           <div class="col-sm-12">
                               <div class="card card-topline-purple">
                                   <div class="card-head">
                                       <header class="erp-table-header">Reciept Details</header>
                                   </div>
                                   <div class="table-responsive">
                                       <table class="mdl-data-table ml-table-bordered common">
                                           <thead>

                                                    <tr>
                                                        <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Reciept Number</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Reciept Date</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Amount(Rs).</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Invoice Number</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Invoice Details</th>

                                                    </tr>
                                           </thead>
                                           <tbody>
                                                 <g:each in="${recieptList}" var="rec" status="i">
                                                     <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${rec?.receipt_no}</td>
                                                            <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${rec?.receipt_date}"/></td>
                                                            <td class="mdl-data-table__cell--non-numeric">${rec?.remark}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${rec?.amount}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${rec?.invoice?.invoice_number}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                               <center>
                                                                <g:link controller="inventoryPurchase" action="viewInvoiceDetailsByReciept" params="[invoiceId : rec?.invoice?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                            </center>
                                                            </td>

                                                     </tr>
                                                  </g:each>
                                           </tbody>
                                       </table>
                                   </div>
                               </div>
                           </div>
                       </div>



