<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
            Approved Purchase Requisition List
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->

                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">
                    <!--
                    <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple full-scroll-table">
                                -->
                                <div class="card card-topline-purple">
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">PRN</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View PR Details</th>

                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invPRList}" var="PR" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PR?.prn}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PR?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PR?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="viewPODetails">
                                                                  <input type="hidden" value="${PR?.id}" name="PRID">
                                                                        <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                            </g:form>
                                                        </td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



