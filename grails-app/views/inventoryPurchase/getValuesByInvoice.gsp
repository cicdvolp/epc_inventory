
 <div class="row" >
          <div class="col-sm-12">
              <h4> Invoice Details</h4>

           </div>
       </div>

<div class="row"  id="PODetails">
<div class="col-sm-3">
<label><strong>Invoice Amount :</strong></label>
<g:field  id="amount" name="amount" readonly="readonly" value="${amount}"  class="form-control"/>
</div>
<div class="col-sm-3">
<label> Tax :</label>
<g:field  id="tax" name="tax" readonly="readonly" value="${tax}"  class="form-control"/>
</div>
<div class="col-sm-3">
<label><strong>Total :</strong></label>
<g:field id="total" name="total" readonly="readonly" value="${total}"  class="form-control"/>
</div>
<div class="col-sm-3">
<label>Invoice Date:</label>
<g:field  id="date" name="date" readonly="readonly" value="${formatDate(format:'dd-MM-yyyy',date:date)}"  class="form-control"/>
</div>
