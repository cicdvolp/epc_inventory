<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                 <div class="modal-header">
                                              Reciepts List

                                 </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Reciept Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Reciept Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Invoice</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Account Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IFSC Code</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Address</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Transaction Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Remark</th>

                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${recieptList}" var="rec" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.receipt_no}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.receipt_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <g:link controller="inventoryPurchase" action="viewInvoiceByReciept" params="[recieptId : rec?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.bank_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.account_number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.ifsc_code}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.bank_branch}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.transaction_number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${rec?.remark}</td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



