<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple ">
                                 <div class="card-head">
                                     <header class="erp-table-header">Invoice Details List</header>
                                     </div>
                                      <div class="card-body" id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Quantity</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Cost Per Unit</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total Cost</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invoiceDetailsList}" var="InvoiceDetails" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${InvoiceDetails?.invmaterial?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${InvoiceDetails?.quantity}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${InvoiceDetails?.cost_per_unit}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${InvoiceDetails?.total_cost}</td>
                                                    </tr>
                                                </g:each>
                                                </tbody>
                                               </table>



      </div>
    </div>
  </div>
  </div>
  </div>
</body>
</html>



