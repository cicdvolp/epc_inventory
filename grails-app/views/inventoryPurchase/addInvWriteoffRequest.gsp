<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
         <script>
                function callme() {
                                var accessionNumber= document.getElementById("accession_number").value;
                                var xmlhttp = new XMLHttpRequest();
                                xmlhttp.onreadystatechange = function() {
                                    if (this.readyState == 4 && this.status == 200) {
                                        document.getElementById("roledivid").innerHTML = this.responseText;
                                    }
                                };
                                xmlhttp.open("GET", "${request.contextPath}/InventoryManage/getValuesByAccesionNumber?accession_number=" + accessionNumber);
                                xmlhttp.send();
                           }
         </script>

    </head>

     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12 mobile-table-responsive">
                            <div class="card card-topline-purple">
                                        <div class="card-head">
                                            <header class="erp-table-header">Add Write-off Request</header>
                                        </div>
                                    <div class="card-body " id="bar-parent">
                                     <div>
                                       <g:form action="saveInvWriteoffRequestfetchdata" name="form">
                                           <center>
                                               <div>
                                               <label for="pwd"><strong>Accession Number</strong> </label><br>
                                               <g:select name="accession_number" id="accession_number"  from="${inventoryAccNoList}"
                                               style="width: 30% !important;"  class="select2"
                                               optionValue="${accession_number}"
                                               noSelection="['null':'Please Select Accession Number']" onchange="callme()"
                                                 />
                                             <!--  <button type="submit" class="btn btn-primary" >Fetch</button>-->
                                               </div>
                                           </center>
                                       <div>
                                         <div class="form-group">
                                            <div id="roledivid">
                                               <div class="row" style="margin-top:20px; margin-left:20px" >
                                                       <div class="col-sm-3">
                                                           <label for="invoice">Invoice Number :</label>
                                                           <g:field  id="invoice" name="invoice" readonly="readonly" value="" style="width: 100% !important;"/><br>
                                                       </div>
                                                       <div class="col-sm-3">
                                                           <label for="departmentS" >Department :</label>
                                                           <g:field  id="department" name="department" readonly="readonly" value="${inventory?.department?.name}" style="width: 100% !important;"/>
                                                       </div>
                                                       <div class="col-sm-3">
                                                           <label for="material">Material Name :</label>
                                                           <g:field id="materialname" name="materialname" readonly="readonly" value="${materialname}" style="width: 100% !important;"/><br>
                                                       </div>
                                                       <div class="col-sm-3">
                                                           <label for="invmaterialpart">Material Part :</label><br>
                                                           <g:field  id="materialpart" name="materialpart" readonly="readonly" value="${materialpart}" style="width: 100% !important;"/>
                                                       </div>
                                               </div>
                                               <div class="row" style="margin-top:20px; margin-left:20px" >
                                                         <div class="col-sm-3">
                                                             <label for="rating">Material Type :</label><br>
                                                             <g:field id="materialtype" name="materialtype"  readonly="readonly" value="${materialtype}" style="width: 100% !important;"/><br>
                                                        </div>
                                                        <div class="col-sm-3">
                                                             <label for="rating">Material Category :</label><br>
                                                             <g:field id="materialcategory" name="materialcategory"  readonly="readonly" value="${invmaterialcategory}" style="width: 100% !important;"/><br>
                                                        </div>
                                                        <div class="col-sm-3">
                                                             <label for="invroom">Room Number :</label><br>
                                                             <g:field id="invroom" name="invroom" readonly="readonly" value="${inventory?.invroom?.number}" style="width: 100% !important;"/><br>
                                                         </div>
                                                          <div class="col-sm-3">
                                                              <label for="inventory">Inward date :</label><br>
                                                              <g:field id="inward_date" name="inward_date" readonly="readonly" value="${formatDate(format:'dd-MM-yyyy',date:inventory?.inward_date)}" style="width: 100% !important;"/>
                                                          </div>
                                               <div>
                                            </div>
                                         </div>
                                       </div>
                                    <hr>
                                    <div>
                                        <div class="row" style="margin-top:20px; margin-left:20px">
                                             <div class="col-sm-2">
                                            <label for="comment">Reason</label>
                                            </div>
                                            <div class="col-sm-4">
                                              <textarea id="reason" name="reason" rows="2" cols="47" required="true"></textarea>
                                            </div>
                                            <div class="col-sm-2">
                                              <label for="pwd">Deadstock Status</label>
                                            </div>
                                            <div class="col-sm-4">
                                               <g:select name="status"   from="${invdeadstockstatusList}"
                                                style="width: 100% !important;"  class="select2"
                                                optionValue="${status}"
                                                noSelection="['null':'Select Deadstock Status']" /><br>
                                            </div>
                                        </div><br>
                                            <center>
                                                <div>
                                                     <input type="hidden" name="accession_number" id="accession_number" value="${accession_number}" />
                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                </div>
                                            </center>
                                        </g:form>
                                    </div>
                                </div>
                             </div>
                           </div>
                        </div>
                       </div>
                       </div>
                        <div class="row">
                            <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="table-responsive">
                                     <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                       <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Accession Number</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material Part</th>
                                                <th class="mdl-data-table__cell--non-numeric">Room No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Department</th>
                                                <th class="mdl-data-table__cell--non-numeric">Inward Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Write-off Request Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">DeadStock Status</th>
                                                <th class="mdl-data-table__cell--non-numeric">Write-off Request Status</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active</th>
                                            </tr>
                                        </thead>
                                        </tbody>
                                             <g:each in="${invWriteOffRequest}" var="writeoffReq" status="i">
                                                 <tr>
                                                     <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${writeoffReq?.inventory?.accession_number}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${writeoffReq?.inventory?.invmaterial?.name}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${writeoffReq?.inventory?.invmaterialpart?.name}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${writeoffReq?.inventory?.invroom?.number}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${writeoffReq?.inventory?.department}</td>
                                                     <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${writeoffReq?.inventory?.inward_date}"/></td>
                                                     <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${writeoffReq?.request_date}"/></td>
                                                     <td class="mdl-data-table__cell--non-numeric">${writeoffReq?.inventory?.invdeadstockstatus?.status}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${invWriteOffRequestEscalation?.invapprovalstatus?.name}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">
                                                          <g:link controller="inventoryManage" action="activatewriteoffReq" params="[writeoffReqId : writeoffReq?.id]">
                                                              <g:if test="${writeoffReq?.isactive}">
                                                                  <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                              </g:if>
                                                              <g:else>
                                                                  <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                              </g:else>
                                                          </g:link>
                                                      </td>
                                                </tr>
                                             </g:each>
                                        </tbody>
                                     </table>
                                   </div>
                                 </div>
                               </div>
                             </div>
                          </div>
                    </div>
                </div>
            </div>
      </body>
</html>


