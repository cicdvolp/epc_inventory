<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <div class="row">
                  <div class="col-sm-12">
                         <div class="card">
                               <div class="card-body">
                                        <g:form action="savePODetails">
                                         <div class="row">
                                            <div class="col-sm-4">
                                                <label for="pwd">Purchase Order:</label>
                                                 <g:select name="POId" optionValue="pon" from="${invPOList}" style="width: 100% !important;"  class="select2" required="true"  noSelection="['null':'Select PO']" optionKey="id"/><br>
                                           </div>
                                         <div class="col-sm-4">
                                              <label for="pwd">Material:</label>
                                           <g:select name="materialId"  optionValue="name" from="${invMaterialList}" style="width: 100% !important;"  class="select2" required="true"  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                         </div>
                                         <div class="col-sm-4">
                                             <label for="pwd">Cost Per Unit:</label>
                                           <input type="number" min="0" class="form-control" name="costPerUnit" required="true">
                                       </div>
                                       </div>
                                       <div class="row">
                                            <div class="col-sm-4">
                                                        <label for="pwd">Quantity:</label>
                                                <input type="number" min="0" class="form-control" name="quantity" required="true">
                                            </div>
                                         <div class="col-sm-4">
                                               <label for="pwd">Remarks:</label>
                                           <input type="text" class="form-control" name="remarks" required="true">
                                         </div>
                                         </div>
                                         <div class="row">
                                         <div class="col-sm-12">
                                             <center>
                                                 <br>
                                                 <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                             </center>
                                         </div>
                                         </div>
                                        </g:form>
                               </div>
                         </div>
                  </div>
            </div>
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                 <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Data List Start-->
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">
                        <div class="card card-topline-purple full-scroll-table">
                            <div class="card-head">
                                <header class="erp-table-header">Budget Details List</header>
                            </div>
                            <div class="card card-topline-purple">
                                <div class="card-body" id="bar-parent">
                                    <div class="table-responsive">
                                        <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Material</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Quantity</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Cost Per Unit</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Total Cost</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <g:each in="${invPODetails}" var="PODetails" status="i">
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${PODetails?.invmaterial?.name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${PODetails?.quantity}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${PODetails?.cost_per_unit}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${PODetails?.total_cost}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link controller="inventoryPurchase" action="activatePODetails" params="[PODetailsId : PODetails?.id]">
                                                                    <g:if test="${PODetails?.isactive}">
                                                                        <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                                    </g:if>
                                                                    <g:else>
                                                                        <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                                    </g:else>
                                                                </g:link>
                                                            </td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editPODetails${i}"></i>
                                                            </td>

                                                             <!--Modal Edit-->
                                                                                                             <div class="modal fade" id="editPODetails${i}" role="dialog">
                                                                                                                        <div class="modal-dialog">
                                                                                                                            <div class="modal-content">
                                                                                                                                <div class="modal-header">
                                                                                                                                    <h4 class="modal-title">Edit PO Details</h4>
                                                                                                                                    <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                                                                                </div>
                                                                                                                                <g:form action="editPODetails">
                                                                                                                                    <div class="modal-body">
                                                                                                                                                <div>
                                                                                                                                                        <input type="hidden" value="${PODetails?.id}" name="PODetailsId">
                                                                                                                                                        <label for="pwd">Purchase Order:</label>
                                                                                                                                                        <g:select name="POId"  optionValue="purpose" from="${invPOList}" style="width: 100% !important;"  class="select2" required="true" value="${PODetails?.id}" optionKey="id"/><br>
                                                                                                                                                </div>
                                                                                                                                                <div class="form-group">
                                                                                                                                                        <label for="pwd">Material: </label>
                                                                                                                                                        <g:select name="materialId"  optionValue="name" from="${invMaterialList}" style="width: 100% !important;"  class="select2" required="true" value="${PODetails?.invmaterial?.id}" optionKey="id"/><br>
                                                                                                                                                </div>
                                                                                                                                                <div class="form-group">
                                                                                                                                                        <label for="pwd">Cost Per Unit</label>
                                                                                                                                                        <input type="text" class="form-control" name="costPerUnit" value="${PODetails?.cost_per_unit}" required="true">
                                                                                                                                                </div>
                                                                                                                                                <div class="form-group">
                                                                                                                                                        <label for="pwd">Quantity</label>
                                                                                                                                                        <input type="text" class="form-control" name="quantity" value="${PODetails?.quantity}" required="true" optionValue="quantity">
                                                                                                                                                </div>
                                                                                                                                                <!--
                                                                                                                                                <div class="form-group">
                                                                                                                                                        <g:if test="${PODetails?.isactive}">
                                                                                                                                                            <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                                                                                        </g:if>
                                                                                                                                                        <g:else>
                                                                                                                                                            <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                                                                                        </g:else>
                                                                                                                                                </div>
                                                                                                                                                -->
                                                                                                                                                <div class="modal-footer">
                                                                                                                                                        <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                                                                                                </div>
                                                                                                                                    </div>
                                                                                                                                </g:form>
                                                                                                                            </div>
                                                                                                                        </div>
                                                                                                                    </div>

                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:form action="deletePODetails">
                                                                    <input type="hidden" value="${PODetails?.id}" name="PODetailsId">
                                                                            <button  data-toggle="modal" data-target="#${i}myModal" class="fabutton" type="submit" onclick="return confirm('Do you want to Delete ?')" ><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></button>
                                                                </g:form>
                                                            </td>
                                                        </tr>
                                                    </g:each>
                                                </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                                <!-- Modal New -->
                                  <div class="modal fade" id="addNewBudgetDetails" role="dialog">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h4 class="modal-title">Add New Budget Details</h4>
                                          <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                       <g:form action="saveBudgetDetails">
                                        <div class="modal-body">
                                            <div class="form-group">
                                              <label for="pwd">Budget:</label>
                                                  <g:select name="budgetId"  optionValue="activity_name" from="${invBudgetList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Budget Activity']" optionKey="id"/><br>
                                            </div>
                                             <div class="form-group">
                                              <label for="pwd">Material:</label>
                                                  <g:select name="materialId"  optionValue="name" from="${invMaterialList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                            </div>
                                             <div class="form-group">
                                                  <label for="pwd">Cost Per Unit</label>
                                                  <input type="text" class="form-control" name="costPerUnit" required="true">
                                              </div>
                                              <div class="form-group">
                                                <label for="pwd">Quantity</label>
                                                <input type="text" class="form-control" name="quantity" required="true">
                                            </div>
                                             <div class="form-group">
                                                <label for="pwd">Remarks</label>
                                                <input type="text" class="form-control" name="remarks" required="true">
                                              </div>
                                            <div class="modal-footer">
                                              <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </div>
                                         </div>
                                       </g:form>
                                        </div>
                                    </div>

</body>
</html>



