<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
                <script>
                        function getQuoDetails() {
                                            var PR= document.getElementById("PR").value;
                                            var xmlhttp = new XMLHttpRequest();
                                            xmlhttp.onreadystatechange = function() {
                                                if (this.readyState == 4 && this.status == 200) {
                                                    document.getElementById("quoDetails").innerHTML = this.responseText;
                                                }
                                            };
                                            xmlhttp.open("GET", "${request.contextPath}/InventoryPurchase/fetchQuotationDetails?PR=" + PR);
                                            xmlhttp.send();
                                       }
                        </script>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                            <div class="alert alert-success" >${flash.message}</div>
                        </g:if>
                        <g:if test="${flash.error}">
                            <div class="alert alert-danger" >${flash.error}</div>
                        </g:if>
                <div class="row">
                  <div class="col-sm-12">
                         <div class="card">
                               <div class="card-body">
                                        <g:form action="savePO">
                                         <div class="row">
                                                <div class="col-sm-4">
                                                     <label for="pwd">PRN:</label>
                                                     <span style="color:red; font-size: 16px">*</span>
                                                      <g:select name="PR"  optionValue="prn" from="${invPRList}" style="width: 100% !important;"  class="select2" required="true" noSelection="['null':'Select PRN']" optionKey="id" onChange="getQuoDetails()"/><br>
                                                </div>

                                           </div>
                                           <div id="quoDetails">
                                            </div>
                                           <div class="row">
                                               <div class="col-sm-4">
                                                      <label for="pwd">Amount:</label>
                                                      <span style="color:red; font-size: 16px">*</span>
                                                 <g:field type="number" class="form-control" name="namount" required="true"></g:field>
                                              </div>
                                              <div class="col-sm-4">
                                                     <label for="pwd">Tax:</label>
                                                     <span style="color:red; font-size: 16px">*</span>
                                                 <g:field type="number" class="form-control" name="ntax" required="true"></g:field>
                                              </div>
                                                <div class="col-sm-4">
                                                       <label for="pwd">Purpose:</label>
                                                       <span style="color:red; font-size: 16px">*</span>
                                                   <g:field type="text" class="form-control" name="npurpose" required="true"></g:field>
                                                </div>
                                        </div>
                                         <div class="row>
                                         <div class="modal-footer">
                                         <center>     <br>
                                           <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                             </center>
                                         </div>
                                         </div>
                                   </g:form>
                                   </div>
                  </div>
            </div>
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                <div class="card-head">
                                      <header class="erp-table-header">Purchase Order  List</header>
                                </div>
                                      <div class="card-body" id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Tax</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total</th>
                                                    <th class="mdl-data-table__cell--non-numeric">PO Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Add PO Details</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View PO Details</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Vendor Company</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-active</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-approved?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${POList}" var="PO" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.data?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.data?.tax}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.data?.total}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.data?.pon}</td>
                                                        <td><center>
                                                         <g:if test="${PO?.data?.isapproved}">
                                                            <label for="pwd">PO Already Approved,Cannot Add</label>
                                                         </g:if>
                                                         <g:else>
                                                            <i class="fa fa-plus  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#addPODetails${i}"></i>
                                                         </g:else></center>

                                                         <!-- ADD PR Modal-->
                                                                <div class="modal fade" id="addPODetails${i}" role="dialog">
                                                                       <div class="modal-dialog">
                                                                         <div class="modal-content">
                                                                           <div class="modal-header">
                                                                             <h4 class="modal-title">Add New PO Details</h4>
                                                                             <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                           </div>
                                                                          <g:form action="savePODetails">
                                                                           <div class="modal-body">
                                                                             <div class="form-group">
                                                                                 <label for="pwd">Purchase Order Number:</label>
                                                                                 <input type="hidden" value="${PO?.data?.id}" name="POID">
                                                                                 <input type="text" class="form-control" name="pon" required="true" value="${PO?.data?.pon}" disabled optionKey="id">
                                                                             </div>
                                                                            <div class="form-group">
                                                                             <label for="pwd">Material:</label>
                                                                             <span style="color:red; font-size: 16px">*</span> <br>
                                                                                 <g:select name="materialId"  optionValue="name" from="${invMaterialList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                                                           </div>
                                                                            <div class="form-group">
                                                                                 <label for="pwd">Cost Per Unit:</label>
                                                                                 <span style="color:red; font-size: 16px">*</span>
                                                                                 <input type="number" min=0 class="form-control" name="costPerUnit" required="true">
                                                                             </div>
                                                                             <div class="form-group">
                                                                               <label for="pwd">Quantity:</label>
                                                                               <span style="color:red; font-size: 16px">*</span>
                                                                               <input type="number" min=0 class="form-control" name="quantity" required="true">
                                                                           </div>
                                                                         <div class="form-group">
                                                                               <label for="pwd">Remarks:</label>
                                                                               <span style="color:red; font-size: 16px">*</span>
                                                                               <input type="text" class="form-control" name="remarks" required="true">
                                                                           </div>
                                                                           </div>
                                                                           <div class="modal-footer">
                                                                             <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                                           </div>
                                                                          </g:form>
                                                                <!--END ADD PR Modal-->
                                                        </td>
                                                        <td><center>
                                                        <g:link controller="inventoryPurchase" action="viewSinglePODetails" params="[POID : PO?.data?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                        </center></td>
                                                        <td class="mdl-data-table__cell--non-numeric">${PO?.data?.invvendor?.company_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <center><g:if test="${PO?.data?.isactive}">
                                                            <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                        </g:if>
                                                        <g:else>
                                                            <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                        </g:else></center>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <center><g:if test="${PO?.data?.isapproved}">
                                                                <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else></center>
                                                        </td>
                                                        <td>
                                                          <center>  <g:each in="${PO?.escalation}" var="app" status="a">
                                                                <g:if test="${app?.invapprovalstatus?.name == 'Approved'}">
                                                                      <!--<i class="fa fa-user fa-2x" style="color:green" aria-hidden="true" title="${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}"></i>-->
                                                                      <strong>${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</strong>:-
                                                                      ${app?.invapprovalstatus?.name}
                                                                </g:if>
                                                                <g:else>
                                                                    <!--<i class="fa fa-user fa-2x" style="color:red" aria-hidden="true" title="${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}"></i>-->
                                                                     <strong>${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</strong>:-
                                                                    ${app?.invapprovalstatus?.name}
                                                                </g:else>
                                                                <br>
                                                            </g:each></center>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <center><g:if test="${PO?.data?.isapproved}">
                                                            <label>PO Already Approved,Cannot Edit</label>
                                                        </g:if>
                                                        <g:else>
                                                            <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editPO${i}"></i>
                                                        </g:else></center>
                                                         </td>

                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="editPO${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title">Edit PO</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="editPO">
                                                            <div class="modal-body">
                                                              <div class="form-group">
                                                                  <input type="hidden" value="${PO?.data?.id}" name="POId">
                                                                  <input type="hidden" value="${PO?.data?.invquotation?.id}" name="quotationId">
                                                                  </div>
                                                                   <div class="form-group">
                                                                        <label for="pwd">Amount:</label>
                                                                        <g:field type="number" value="${PO?.data?.amount}" class="form-control" name="amount"></g:field>
                                                                    </div>
                                                                    <div class="form-group">
                                                                      <label for="pwd">Tax:</label>
                                                                      <g:field type="number" value="${PO?.data?.tax}" class="form-control" name="tax" optionValue="quantity"></g:field>
                                                                  </div>
                                                                <div class="form-group">
                                                                      <label for="pwd">Total:</label>
                                                                      <g:field type="number" value="${PO?.data?.total}" class="form-control" name="total"></g:field>
                                                                  </div>
                                                                  <div class="form-group">
                                                                     <g:if test="${PO?.data?.isactive}">
                                                                           <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                     </g:if>
                                                                     <g:else>
                                                                           <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                     </g:else>
                                                               </div>
                                                            <div class="modal-footer">
                                                              <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                            </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </g:each>
                                                </tbody>
                                               </table>

                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->


    </div>
    </div>
  </div>
  </div>
</body>
</html>



