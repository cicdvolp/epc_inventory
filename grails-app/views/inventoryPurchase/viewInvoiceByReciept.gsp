<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                 <div class="modal-header">  Invoice
                                 </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Invoice Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Invoice Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total to Pay</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount Paid</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Tax</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Invoice</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>

                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invoiceList}" var="invoice" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${invoice?.invoice_number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${invoice?.invoice_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${invoice?.total}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${invoice?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${invoice?.tax}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <center> <g:link controller="inventoryPurchase" action="viewInvoiceDetailsByInvoice" params="[invoiceId : invoice?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                        </center> </td>
                                                        <td class="mdl-data-table__cell--non-numeric">${invoice?.purpose}</td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



