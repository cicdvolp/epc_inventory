<head>
     <meta name="layout" content="smart_main_datatable_inst"/>
       <meta name="author" content="aditi"/>
</head>

<body>
      <div class="page-content-wrapper">
           <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <g:if test="${flash.message}">
                     <div class="alert alert-success" role="status">
                           <strong class="font-weightage"> </strong>${flash.message}
                     </div>
                </g:if>

                 <div class="row">
                      <div class="col-sm-12">
                           <div class="card">
                                <div class="card-body">
                                     <g:form controller="EmployeeLeaveMaster" name="myform"  action="getLoadData">
                                          <div class="row">
                                               <div class="col-sm-4">
                                                  <label>From Application Date : </label>
                                                  <input id="fromdate"  class="form-control" type="date" onkeydown="return false" name="fromdate" value="${fromdate}">
                                               </div>
                                                <div class="col-sm-4">
                                                    <label>To Application Date : </label>
                                                    <input id="todate" class="form-control" type="date" onkeydown="return false" name="todate" value="${todate}">
                                                </div>
                                                <div class="col-lg-4" style="padding:35px;">
                                                    <g:submitButton class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" name="Proceed" value="Proceed" />
                                                </div>
                                          </div>
                                     </g:form>
                                </div>
                           </div>
                      </div>
                 </div>

                 <g:if test="${flash.message}">
                     <div class="alert alert-success" role="status">
                         <strong class="font-weightage"> </strong>${flash.message}
                     </div>
                 </g:if>

                 <div class="row">
                     <div class="col-sm-12">
                            <div class="card card-topline-purple">
                                  <div class="card-head">
                                      <header class="erp-table-header">Load Adjustment</header>
                                  </div>
                                  <div class="table-responsive">
                                      <table class="mdl-data-table ml-table-bordered">
                                           <thead>
                                                 <tr>
                                                     <td class="mdl-data-table__cell--non-numeric">S.No</td>
                                                     <td class="mdl-data-table__cell--non-numeric">Employee</td>
                                                     <td class="mdl-data-table__cell--non-numeric">Date</td>
                                                     <td class="mdl-data-table__cell--non-numeric">Task/Load Details</td>
                                                     <td class="mdl-data-table__cell--non-numeric">From Time</td>
                                                     <td class="mdl-data-table__cell--non-numeric">To Time</td>
                                                     <td  class="mdl-data-table__cell--non-numeric">Status</td>
                                                 </tr>
                                           </thead>
                                           <tbody>
                                                <g:each var="d" in="${loadFinalList}" status="i">
                                                   <tr>
                                                         <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                         <td class="mdl-data-table__cell--non-numeric"><label title=${d?.load?.employeeleavetransaction?.instructor?.employee_code}>${d?.load?.employeeleavetransaction?.instructor?.employee_code} : ${d?.load?.employeeleavetransaction?.instructor?.person?.firstName} ${d?.load?.employeeleavetransaction?.instructor.person.lastName}</label></td>
                                                         <td class="mdl-data-table__cell--non-numeric"><g:formatDate date="${d?.load?.leave_date}" format="dd-MMM-yyyy"/></td>
                                                         <td class="mdl-data-table__cell--non-numeric">${d?.load?.load_description}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${d?.load?.start_time}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${d?.load?.end_time}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                              <g:if test="${d?.freezed || d?.load?.isdeleted}">
                                                                  ${d?.load?.remark}
                                                              </g:if>
                                                              <g:else>
                                                                  <g:form controller="EmployeeLeaveMaster" name="myform"  action="approveRejectload">
                                                                        <input type="hidden" name="loadad" value="${d?.load?.id}" />
                                                                        <input type="hidden" name="frontpage" value="true" />
                                                                        <table>
                                                                            <tr>
                                                                               <td style="border: none !important;">
                                                                                   <g:if test="${remarkcompulsary}">
                                                                                       <input class="form-control" name="remark" type="text" value="${d?.load?.remark}" style="width: 200px" required="true">
                                                                                   </g:if>
                                                                                   <g:else>
                                                                                       <input class="form-control" name="remark" type="text" value="${d?.load?.remark}" style="width: 200px">
                                                                                   </g:else>
                                                                               </td>
                                                                               <td style="border: none !important;">
                                                                                   <g:if test="${d?.load?.isloadrejected || !d?.load?.isloadadjusted}">
                                                                                       <g:submitButton class="btn btn-sm btn-success" name="save" value="Adjust" />
                                                                                   </g:if>
                                                                               </td>
                                                                               <td style="border: none !important;">
                                                                                   <g:if test="${d?.load?.isloadadjusted || !d?.load?.isloadrejected }">
                                                                                       <g:submitButton class="btn btn-sm btn-danger" name="save" value="Reject" />
                                                                                   </g:if>
                                                                               </td>
                                                                            </tr>
                                                                        </table>
                                                                  </g:form>
                                                              </g:else>
                                                         </td>
                                                     </tr>
                                                </g:each>
                                           </tbody>
                                      </table>
                                  </div>
                            </div>
                     </div>
                 </div>
                 <script>
                      $('#headerCheckbox').click(function() {
                           var checked = this.checked;
                           $('.columnCheckbox').prop('checked', checked);
                      });
                 </script>
           </div>
      </div>
 </body>
</html>