<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template/meta" />
        <g:render template="/layouts/smart_template/css" />
        <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <title>EduPlusCampus</title>
        <!--select2-->
        <asset:stylesheet src="smart/plugins/select2/css/select2.css"/>
        <asset:stylesheet src="smart/plugins/select2/css/select2-bootstrap.min.css"/>
         <style>
            @media (max-width: 600px) {
                .btn {
                    width:100% !important;
                    text-align:center !important;
                    margin-top: 3px !important;
                    margin-bottom:3px !important;
                }
            }
            .btn {
                width:100% !important;
                text-align:center !important;
                margin-top: 3px !important;
                margin-bottom:3px !important;
            }
            span {
                display: inline-block;
                vertical-align: middle;
                margin-right: 20px;
            }
            img {
                border-radius: 8px;
            }
            h4 {
                color:#FFBE60 !important;
            }
        </style>
        <script>
                    function printDiv() {
                        var divContents = document.getElementById("print").innerHTML;
                        var a = window.open('', '', 'height=500, width=500');
                        a.document.write(divContents);
                        a.document.close();
                        a.print();
                    }
        </script>
    </head>
     <body class="page-header-fixed page-full-width sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
            <div class="page-wrapper">
                <g:render template="/layouts/smart_template/header1" />
                <div class="page-container">
                    <!-- start page content -->
                    <div class="page-content-wrapper">
                        <div class="page-content">
                            <!-- add content here -->
                                <g:if test="${flash.message}">
                                    <div class="alert alert-success" >${flash.message}</div>
                                </g:if>
                                <g:if test="${flash.error}">
                                    <div class="alert alert-danger" >${flash.error}</div>
                                </g:if>
                                <div class="row" id="print">
                                    <div class="col-sm-12">
                                        <div class="card card-topline-purple">
                                            <div class="card-body " id="bar-parent">

                                                    <h4 style="text-align:center; font-weight: bold;color:black !important;">Application Form</h4>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-sm-9">
                                                            <div class="row">
                                                                <div class="col-sm-6">
                                                                    <span>Application ID : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${entranceApplication?.applicaitionid}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Application Date : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;"><g:formatDate date="${entranceApplication?.applicationdate}" format="dd-MM-yyyy"/></span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Name : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.fullname}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Email : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.email}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Date Of Birth : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;"><g:formatDate date="${EA?.dateofbirth}" format="dd-MM-yyyy"/></span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Mobile No. : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.mobilenumber}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Gender : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.gender?.type}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Marital Status : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.maritalstatus?.name}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Nationality : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.erpnationality?.type}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Domacile : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.erpdomacile?.type}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Applied Branch : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${entranceApplication?.entrancebranch?.name.join(', ')}</span>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <g:if test="${entranceApplication?.entrancerejectionreason}">
                                                                        <span>Exemption Reason : </span>
                                                                        <span style="color:#00aeff; font-weight:bold; float:right;">${entranceApplication?.entrancerejectionreason}</span>
                                                                    </g:if>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Are you Physically Handicapped? : </span>
                                                                    <g:if test="${EA?.is_physically_handicapped == true}">
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">Yes</span>
                                                                    </g:if>
                                                                    <g:else>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">No</span>
                                                                    </g:else>
                                                                </div>
                                                                <div class="col-sm-6">
                                                                    <span>Are you Visually Handicapped? : </span>
                                                                    <g:if test="${EA?.is_visually_handicapped == true}">
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">Yes</span>
                                                                    </g:if>
                                                                    <g:else>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">No</span>
                                                                    </g:else>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <span>Local Address : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.local_address}</span>
                                                                </div>
                                                                <div class="col-sm-12">
                                                                    <span>Permanent Address : </span>
                                                                    <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.permanent_address}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-3">
                                                            <center>
                                                                <img style="height:140px;width:140px" src="${Photo_URL}"/>
                                                            </center>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <h4 style="text-align:left; font-weight: bold;">Qualification Details </h4>
                                                        <table class="mdl-data-table ml-table-bordered">
                                                           <thead>
                                                               <tr>
                                                                   <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Degree</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">class/Grade</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Percentage/CPI</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Year of Passing</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">University/Board</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Branch</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Specialization</th>
                                                               </tr>
                                                           </thead>
                                                           <tbody>
                                                               <g:each in="${entranceApplicantAcademics}" var="acad" status="g">
                                                                   <tr>
                                                                       <td class="mdl-data-table__cell--non-numeric">${g+1}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${acad?.entrancedegree?.name}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${acad?.recclass?.name}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${acad?.cpi_marks}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${acad?.yearofpassing}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${acad?.university}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${acad?.branch}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${acad?.specialization}</td>
                                                                   </tr>
                                                               </g:each>
                                                           </tbody>
                                                        </table>
                                                    <hr>
                                                    <h4 style="text-align:left; font-weight: bold;">Experience Details </h4>
                                                        <table class="mdl-data-table ml-table-bordered">
                                                           <thead>
                                                               <tr>
                                                                   <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Enperience Type</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Name Of Oraganization</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">From Date</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">To Date</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Is this current job?</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Designation</th>
                                                               </tr>
                                                           </thead>
                                                           <tbody>
                                                               <g:each in="${entranceApplicantExperience}" var="exper" status="h">
                                                                   <tr>
                                                                       <td class="mdl-data-table__cell--non-numeric">${h+1}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${exper?.recexperiencetype?.type}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${exper?.organization_name}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${exper?.from_date}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${exper?.to_date}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">
                                                                           <g:if test="${exper?.is_current_job}">
                                                                               <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                                           </g:if>
                                                                           <g:else>
                                                                               <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                                           </g:else>
                                                                       </td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${exper?.designation}</td>
                                                                   </tr>
                                                               </g:each>
                                                           </tbody>
                                                        </table>
                                                    <hr>
                                                        <h4 style="text-align:left; font-weight: bold;">Document Details </h4>
                                                            <table class="mdl-data-table ml-table-bordered">
                                                                <thead>
                                                                    <tr>
                                                                        <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                        <th class="mdl-data-table__cell--non-numeric">Document</th>
                                                                        <th class="mdl-data-table__cell--non-numeric">Document File</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <g:each in="${entranceApplicantDocument}" var="doc" status="p">
                                                                        <tr>
                                                                            <td class="mdl-data-table__cell--non-numeric">${p+1}</td>
                                                                            <td class="mdl-data-table__cell--non-numeric">${doc?.document?.entrancedocumenttype?.name}</td>
                                                                            <td class="mdl-data-table__cell--non-numeric"><a href="${doc.url}" target="_blank">${doc?.document?.filename}</a></td>
                                                                        </tr>
                                                                    </g:each>
                                                                </tbody>

                                                            </table>
                                                        <hr>
                                                    <i class="fa fa-check fa-2x" style="color:Green;text-size:5px" aria-hidden="true"></i>&nbsp;&nbsp; I &nbsp;&nbsp;&nbsp;&nbsp;<span class="font-weight-bold">${EA?.fullname}</span> hereby declare that the information filled in profile by me is true and correct as per my knowledge and belief. I understand that if above information or attached document are found false, incorrect or incomplete, I can be debarred from admission proceedings at any stage or after the admission.I further understand that no notice period shall be taken on request for wthdrawal of my application.
                                                    <hr>

                                                    <div class="row">
                                                        <label class="font-weight-bold" style="float:right;">Place : ${entranceApplication?.place}</label>
                                                    </div>
                                                    <div class="row">
                                                       <br>
                                                       <br>
                                                       <label class="font-weight-bold" style="float:right;">Date : <g:formatDate date="${new Date()}" type="datetime" style="MEDIUM"/></label>
                                                   </div>

                                                    <br>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <!-- end content here -->
                             <div>
                               <center> <input type="button" class="btn btn-primary" value="Print Application" onclick="printDiv()"> </center>
                            </div>
                        </div>
                    </div>
                    <!-- end page content -->
                </div>
                <!-- start footer -->
                <div class="page-footer">
                    <div class="page-footer-inner">
                        2020 &copy; EduPlusCampus By
                        <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
                    </div>
                    <div class="scroll-to-top">
                        <i class="icon-arrow-up"></i>
                    </div>
                </div>
                <!-- end footer -->
            </div>
            <g:render template="/layouts/smart_template/js" />
            <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
                <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
                <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
            </div>
            <!--select2-->
            <asset:javascript src="smart/plugins/select2/js/select2.js"/>
            <asset:javascript src="smart/pages/select2/select2-init.js"/>
        </body>
    </html>
