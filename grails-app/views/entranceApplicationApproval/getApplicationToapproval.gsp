<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                            <div class="row">
                               <div class="col-md-12  mobile-table-responsive">
                                   <div class="card card-topline-purple full-scroll-table">
                                       <div class="card-head">
                                           <header class="erp-table-header">Entrance Version Details</header>
                                       </div>
                                        <div class="table-responsive">
                                            <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Branch</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <g:each in="${entranceVersion}" var="name" status="i">
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${name?.academicyear}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${name?.entrance_name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${branch}</td>
                                                        </tr>
                                                    </g:each>
                                                </tbody>
                                            </table>
                                        </div>
                                     </div>
                                </div>
                            </div>

                            <g:if test="${entranceApplication_list == []}">
                               <span style="color:blue;font-size: 30px;">No Applications Avaiable.</span>
                            </g:if>
                            <g:else>

                            <div class="row">
                                <div class="col-sm-12 mobile-table-responsive">
                                    <div class="card card-topline-purple full-scroll-table">
                                        <div class="card-head">
                                            <header class="erp-table-header">Entrance Application List</header>
                                        </div>
                                        <div class="card-body " id="bar-parent">
                                            <div class="table-responsive">
                                                <table style="max-width: 100%;table-layout: auto;" id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                    <colgroup>
                                                           <col span="1" style="width: 15%;">
                                                           <col span="1" style="width: 20%;">
                                                           <col span="1" style="width: 30%;">
                                                    </colgroup>
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application Id</th>
                                                            <th style="margin-left:30px"class="mdl-data-table__cell--non-numeric">Full Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Branch</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exam Exempted?</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Category</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Email Id</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Mobile Number</th>
                                                            <th class="mdl-data-table__cell--non-numeric">View Application</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Fees Paid in(Rs.)</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exam Exemption</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application Status</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Rejection Reason</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Save</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

                                                         <g:each in="${entranceApplication_list}" var="name" status="i">
                                                              <tr>
                                                                  <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.applicaitionid}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicant?.fullname}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entrancebranch?.name.join(', ')}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.is_exemption_from_test}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicant?.entrancecategory}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicant?.email}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicant?.mobilenumber}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">
                                                                     <g:link controller="entranceApplicationApproval" action="viewApplicationInApproval" params="[entranceVersion : name?.entranceversion?.id,applicantId:name?.entranceapplicant?.id]"><i style="cursor: pointer;margin-left:20px" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                                  </td>
                                                                  <td class="mdl-data-table__cell--non-numeric">Fees</td>
                                                                  <g:form action="saveApplicationToapproval">
                                                                      <td class="form-group">
                                                                          <g:select name="exemption" id="exemption"  from="${exemption_list?.name}"
                                                                            class="select2"
                                                                            optionValue="${exemption}" style="width:150px"
                                                                            noSelection="['null':'Please Select Exemption']"/>
                                                                      </td>
                                                                      <td class="form-group">
                                                                           <g:select name="applicationStatus" id="applicationStatus"  from="${entranceApplicationStatus_list?.name}"
                                                                            class="select2"
                                                                           optionValue="${applicationStatus}"style="width:150px"
                                                                           noSelection="['null':'Please Select Application Status']"/>
                                                                      </td>
                                                                      <td>
                                                                          <input class="form-control" type="text" name="remark"style="width:150px">
                                                                      </td>
                                                                      <td class="form-group">
                                                                             <g:select name="rejectionReason" id="rejectionReason"  from="${entranceRejectionReason_list?.name}"
                                                                              class="select2"
                                                                             optionValue="${rejectionReason}" style="width:150px"
                                                                             noSelection="['null':'Please Select Rejection Reason']"/>
                                                                      </td>
                                                                      <td class="mdl-data-table__cell--non-numeric form-group"">
                                                                           <input type="hidden" value="${name?.id}" name="entranceApplicationId">
                                                                           <button type="submit" class="btn btn-primary">Save</button>
                                                                      </td>
                                                                  </g:form>
                                                              </tr>
                                                          </g:each>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                              </g:else>
                            </div>
                        </div>
                    </div>
        </div>
    </body>
</html>




