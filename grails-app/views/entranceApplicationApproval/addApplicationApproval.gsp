<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->

                            <div class="row">
                               <div class="col-md-12  mobile-table-responsive">
                                   <div class="card card-topline-purple">
                                       <div class="card-head">
                                           <header class="erp-table-header">Entrance Version Details</header>
                                       </div>
                                        <div class="table-responsive">
                                            <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Total Applications</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Shortlisted Applications</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Branch</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Proceed To Approval</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <g:each in="${entranceVersion_list}" var="name" status="i">
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${name?.academicyear}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${name?.entrance_name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:include controller="entranceApplicationApproval" action="getApplicationCount" params="[versionId:name?.id]" />
                                                            </td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:include controller="entranceApplicationApproval" action="getShortListedApplicationCount" params="[versionId:name?.id]" />
                                                            </td>

                                                            <g:form action="getApplicationToapproval">
                                                            <td class="form-group">
                                                              <g:select name="branch" id="branch"  from="${entranceBranch_list?.name}"
                                                              style="width: 90% !important;"  class="select2"
                                                              optionValue="${branch}"
                                                              noSelection="['null':'Please Select Branch']"/>
                                                            </td>
                                                            <td class="mdl-data-table__cell--non-numeric form-group"">
                                                                 <input type="hidden" value="${name?.id}" name="entranceVersionId">
                                                                  <button type="submit" class="btn btn-primary">Proceed</button>
                                                            </td>
                                                            </g:form>
                                                        </tr>
                                                    </g:each>
                                                </tbody>
                                            </table>
                                        </div>
                                     </div>
                                </div>
                             </div>
                        </div>
                    </div>
        </div>
    </body>
</html>




