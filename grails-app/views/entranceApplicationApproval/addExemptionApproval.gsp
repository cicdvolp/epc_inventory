<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->

                            <div class="row">
                               <div class="col-md-12  mobile-table-responsive">
                                   <div class="card card-topline-purple">
                                       <div class="card-head">
                                           <header class="erp-table-header">Exemption Approval</header>
                                       </div>
                                            <div class="table-responsive">
                                                <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Applicant Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exemption</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exemption Type Details</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Save</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <g:each in="${entranceApplication_list}" var="name" status="i">
                                                            <tr>
                                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicant?.fullname}</td>
                                                                <g:form action="saveExemptionApproval">
                                                                <td class="mdl-data-table__cell--non-numeric form-group">
                                                                   <g:select name="exemption" from="${exemption_list?.name}" style="width: 100% !important;"  class="select2" required="true"
                                                                   value="${name?.exemption?.name}"  noSelection="['null':'Select Exemption']"/><br>
                                                                </td>
                                                                <td class="mdl-data-table__cell--non-numeric">
                                                                   <div class="table-responsive ">
                                                                       <table id="exportTable" class="display nowrap erp-full-width ml-table-bordered" >
                                                                           <thead>
                                                                               <tr>
                                                                                   <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                                   <th class="mdl-data-table__cell--non-numeric">Exemption Type</th>
                                                                                   <th class="mdl-data-table__cell--non-numeric">Download</th>
                                                                               </tr>
                                                                           </thead>
                                                                           <tbody>
                                                                               <g:each in="${name?.exemptiontype}" var="exemptionType" status="k">
                                                                                   <tr>
                                                                                       <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                                                                       <td class="mdl-data-table__cell--non-numeric">${exemptionType?.name}</td>
                                                                                       <td class="mdl-data-table__cell--non-numeric">
                                                                                           <g:include controller="entranceApplicationApproval" action="getExemptionTypedocumenturl" params="[exemptionTypeId:exemptionType?.id,applicationId:name?.id]" />
                                                                                       </td>
                                                                                   </tr>
                                                                               </g:each>
                                                                           </tbody>
                                                                       </table>
                                                                   </div>
                                                                </td>
                                                                <td class="mdl-data-table__cell--non-numeric form-group">
                                                                    <input type="hidden" name="applicationId" id="applicationId" value="${name?.id}"/>
                                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span>Save</button>
                                                                </td>
                                                                </g:form>
                                                            </tr>
                                                        </g:each>
                                                    </tbody>
                                                </table>
                                            </div>
                                     </div>
                                </div>
                             </div>
                        </div>
                    </div>
        </div>
    </body>
</html>




