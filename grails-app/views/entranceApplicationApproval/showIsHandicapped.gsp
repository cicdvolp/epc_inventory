<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->

                            <div class="row">
                               <div class="col-md-12  mobile-table-responsive">
                                   <div class="card card-topline-purple">
                                       <div class="card-head">
                                           <header class="erp-table-header">Handicapped Applicant List</header>
                                            <div class="tools">
                                               <g:link controller="entranceAdmissionMaster" action="addEntranceRejectionReason"><span aria-hidden="true" class="icon-plus" title="Add New Reason" style="float:right; font-size:20px;"></span></g:link>
                                           </div>
                                       </div>

                                            <div class="table-responsive">
                                                <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Applicant Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Physically Handicapped</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Physically Handicapped Document</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Visually Handicapped</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Visually Handicapped Document</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Satus</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Reason</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Save</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         <g:each in="${handicapped_list}" var="name" status="i">
                                                             <tr>
                                                                 <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicant?.fullname}</td>
                                                                 <g:if test="${name?.is_physically_handicapped == true}">
                                                                    <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-check fa-2x" style="color:Green;text-size:5px" aria-hidden="true"></td>
                                                                 </g:if>
                                                                 <g:else>
                                                                    <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></td>
                                                                 </g:else>
                                                                 <g:if test="${name?.is_physically_handicapped == true}">
                                                                     <td class="mdl-data-table__cell--non-numeric">
                                                                         <g:include controller="entranceApplicationApproval" action="getPhysicallyHandicappeddocumenturl" params="[is_physically_handicapped:name?.is_physically_handicapped, applicationId:name?.id]" />
                                                                     </td>
                                                                 </g:if>
                                                                 <g:else>
                                                                     <td class="mdl-data-table__cell--non-numeric">-</td>
                                                                 </g:else>

                                                                 <g:if test="${name?.is_visually_handicapped == true}">
                                                                     <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-check fa-2x" style="color:Green;text-size:5px" aria-hidden="true"></td>
                                                                 </g:if>
                                                                 <g:else>
                                                                     <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></td>
                                                                 </g:else>
                                                                 <g:if test="${name?.is_visually_handicapped == true}">
                                                                      <td class="mdl-data-table__cell--non-numeric">
                                                                           <g:include controller="entranceApplicationApproval" action="getVisuallyHandicappeddocumenturl" params="[is_visually_handicapped:name?.is_visually_handicapped, applicationId:name?.id]" />
                                                                       </td>
                                                                 </g:if>
                                                                 <g:else>
                                                                      <td class="mdl-data-table__cell--non-numeric">-</td>
                                                                 </g:else>
                                                                 <g:form action="saveIsHandicappedStatus" name="${i}">
                                                                     <td class="mdl-data-table__cell--non-numeric form-group">
                                                                        <g:select name="applicationStatus" from="${entranceApplicationStatus_list?.name}" style="width: 100% !important;"  class="select2" required="true" value="${name?.entranceapplicationstatus?.name}"  noSelection="['null':'Select Satus']"/><br>
                                                                     </td>
                                                                     <td class="mdl-data-table__cell--non-numeric form-group">
                                                                        <g:select name="reason" from="${entranceRejectionReason_list?.name}" style="width: 100% !important;"  class="select2" required="true" value="${name?.entrancerejectionreason?.name}"  noSelection="['null':'Select Reason']"/><br>
                                                                     </td>
                                                                     <td class="mdl-data-table__cell--non-numeric form-group">
                                                                        <input type="text" name="remark" required="true" value="${name?.remark}" />
                                                                     </td>
                                                                     <td class="mdl-data-table__cell--non-numeric form-group">
                                                                        <input type="hidden" name="applicationId" id="applicationId" value="${name?.id}"/>
                                                                        <button type="submit" class="btn btn-primary"> <span class="glyphicon glyphicon-save"></span>Save</button>
                                                                     </td>
                                                                 </g:form>
                                                             </tr>
                                                         </g:each>
                                                    </tbody>
                                                </table>
                                            </div>

                                       </div>
                                   </div>
                            </div>
                </div>
             </div>
        </div>
    </body>
</html>




