<tr>
    <td style="text-align:right" ><label style="font-size:14px">Academic Year&nbsp;&nbsp;&nbsp;</label></td>
    <td>
        <g:select class="form-control" id="ay" name="ay" from="${academicyear}" optionValue="ay" onchange="getDivision();" optionKey="id" value="${aay?.academicyear?.id}" style="width:400px;" required="true" />
    </td>
</tr>
<tr>
    <td style="text-align:right" ><label style="font-size:14px">Semester&nbsp;&nbsp;&nbsp;</label></td>
    <td>
        <g:select class="form-control" id="sem" name="sem" from="${semester}" optionValue="sem" onchange="getDivision();" optionKey="id" value="${aay?.semester?.id}" style="width:400px;" required="true" />
    </td>
</tr>