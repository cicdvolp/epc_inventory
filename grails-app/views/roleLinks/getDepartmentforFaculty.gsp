<g:select class="form-control"
id="departmentList" name="departmentList"
from="${departmentList}"
optionValue="${{it.name}}"
optionKey="id"
noSelection="['-1':'All']"
required="true"
style="width:400px;"/>