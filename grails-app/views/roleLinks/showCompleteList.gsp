<g:if test="${flash.message}">
	<center><span style="color: red;"><div class="message" role="status">${flash.message}</div></span></center>
</g:if>
<div id="newtable">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-topline-purple">
                        <div class="card-head">
                            <header class="erp-table-header">STUDENT LIST</header>
                        </div>
                        <div class="table-responsive" id="bar-parent">
                            <table id="exportTable" class="display nowrap erp-full-width">
                                <thead>
                                    <tr>
                                        <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                        <th class="mdl-data-table__cell--non-numeric">PRN No</th>
                                        <th class="mdl-data-table__cell--non-numeric">Name</th>
                                        <th class="mdl-data-table__cell--non-numeric">Division</th>
                                        <th class="mdl-data-table__cell--non-numeric">Role Type</th>
                                        <th class="mdl-data-table__cell--non-numeric">Role</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <g:each in="${finalList}" var="flist" status="i">
                                        <tr>
                                            <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                            <td class="mdl-data-table__cell--non-numeric">${flist?.learnerdivision.learner.registration_number}</td>
                                            <td class="mdl-data-table__cell--non-numeric">${flist?.learnerdivision.learner}</td>
                                            <td class="mdl-data-table__cell--non-numeric">${flist?.learnerdivision?.divisionoffering}</td>
                                            <g:each in="${roleType}" var="rlt" status="j">
                                                <td class="mdl-data-table__cell--non-numeric">${rlt}</td>
                                            </g:each>
                                            <g:if test="${flist?.roles_list?.contains(role?.id)}">
                                                <td class="mdl-data-table__cell--non-numeric">${role}</td>
                                            </g:if>
                                            <g:else>
                                                <td class="mdl-data-table__cell--non-numeric">-</td>
                                            </g:else>
                                        </tr>
                                    </g:each>
                                </tbody>
                            </table>
                        </div>
                        <div class="form-row col-sm-12">
                            <div class="form-row col-sm-12">
                                <div class="col-sm-6">
                                    <div style="float:right;">
                                        <g:form controller="RoleLinks" action="saveAssignRoles">
                                            <g:hiddenField name="ay" value="${ay}" />
                                            <g:hiddenField name="sem" value="${sem}" />
                                            <g:hiddenField name="division" value="${division}" />
                                            <g:hiddenField name="programtype" value="${programtypeid}" />
                                            <g:hiddenField name="programid" value="${programid}" />
                                            <g:hiddenField name="year" value="${yearid}" />
                                            <g:hiddenField name="roletype" value="${roleType.id}" />
                                            <g:hiddenField name="role" value="${roleid}" />
                                            <g:submitToRemote url="[action: 'saveAssignRoles', controller:'RoleLinks']" update="updatemessage3" value="Assign Selected Role" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" before="if(!confirm('Are You sure!')) return true;document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                        </g:form>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <g:form controller="RoleLinks" action="revokeRoles" >
                                        <g:hiddenField name="ay" value="${ay}" />
                                        <g:hiddenField name="sem" value="${sem}" />
                                        <g:hiddenField name="division" value="${division}" />
                                        <g:hiddenField name="programtype" value="${programtypeid}" />
                                        <g:hiddenField name="program" value="${programid}" />
                                        <g:hiddenField name="year" value="${yearid}" />
                                        <g:hiddenField name="roletype" value="${roleType.id}" />
                                        <g:hiddenField name="role" value="${roleid}" />
                                        <g:submitToRemote url="[action: 'revokeRoles', controller:'RoleLinks']" update="updatemessage3" value="Revoke Role" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-danger" before="if(!confirm('Are You sure!')) return true;document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                    </g:form>
                                 </div>
                            </div>
                        </div>

                        <div id="updatemessage3"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<g:render template="/layouts/smart_template_inst/datatable_js" />