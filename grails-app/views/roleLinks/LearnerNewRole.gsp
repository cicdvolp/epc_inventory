<g:form name="myForm" action="newAddedRole">
<input type="hidden" name="RoleType" value="${RoleType}">
<g:hiddenField name="gr_no" value="${gr_no}" />
    <br />
     <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="card card-box">
                <table width="60%">
            <tr>
            <br>
                <td>&nbsp;&nbsp;<label><b>Roles : </b></label></td>
                <td>&nbsp;</td>
                <td>
                    <table>
                        <tr>
                            <td><input type="checkbox" id="selectall" onClick="selectAll(this)" /></td>
                            <td>Select All</td>
                            <td>
                                <g:each var="a" in="${RoleList}" >
                                    <tr>
                                        <td><input type="checkbox" name="Role" value="${a.id}" ></td>
                                        <td>${a.role_displayname}</td>
                                    </tr>
                               </g:each>
                            </td>
                        </tr>
                    </table>
                <td>&nbsp;</td>
                </td>

                <td align="right">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<g:submitButton id="submitButton" class="btn btn-primary" name="Update" value="Save" /></td>
                <td>&nbsp;</td>
            </tr>

        </table>
        <br>
    </div>
</g:form>


