
<head>
    <meta name="layout" content="smart_main_datatable_inst" />
    <meta name="author" content="poonam" />
    <asset:javascript src="jquery.min.js"/>
</head>
    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- start content here -->
                 <g:if test="${flash.message}">
                    <div class="message" role="status">${flash.message}</div>
                 </g:if>
                 <div class="card">
                    <g:form url="[controller:'RoleLinks',action: 'addRoleAdmission']" >
                        <g:if test="${flash.message}">
                           <div class="message" role="status">${flash.message}</div>
                        </g:if>

                        <g:form action="fetchClickAdmission">
                             <div class="card-body row">
                                 <div class="col-sm-6">
                                     <br>
                                     <label>Employee :</label>
                                     <g:select class="js-example-basic-single class="form-control" id="emp" name="emp" from="${emplist}" value="${defaultInstructor.id}" optionKey="id"/><br>
                                 </div>
                                 <div class="col-sm-2">
                                    <br /><br />
                                     <g:submitToRemote url="[action: 'fetchClickAdmission']"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" update="updateme" value="Fetch Roles"/>
                                 </div>
                             </div>

                        </g:form>
                 </div>

                 <div id="updateme">
                       <g:form action="addRoleAdmission">
                          <input type="hidden" id="instructorid" name="instructorid" value="${defaultInstructor.id}">
                          <br>
                           <div class="row">
                              <div class="col-md-12 col-sm-12">
                                  <div class="card card-box">

                                      <div class="card-body " id="bar-parent">
                                          <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                             <g:set var="x" value="${1}"/>
                                             <thead>
                                                <tr>
                                                   <th>Sr.No</th>
                                                   <th>Role Type</th>
                                                   <th>Role</th>
                                                   <th>Action</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                <g:each in="${defInstructorRoleList}" var="r1" status="i">
                                                   <tr>
                                                      <td>${i+1}</td>
                                                      <td>${r1?.roletype?.type}</td>
                                                      <td>${r1?.role}</td>
                                                      <td>
                                                         <g:link action="deleteAdmissionRole" params="[instid:defaultInstructor.id,id:r1.id]" onclick="return confirm('Do you want to Delete ?')"><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></g:link>
                                                      </td>
                                                   </tr>
                                                </g:each>
                                             </tbody>
                                          </table>
                                          <center><g:submitToRemote url="[action: 'addRoleAdmission']" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary"" update="updaterole" value="Add New Role"/></center>

                                      </div>
                                  </div>
                              </div>
                           </div>
                       </g:form>
                       <div id="updaterole"></div>
                 </div>
            </g:form>

    <script>
         $(document).ready(function() {
            $('.js-example-basic-single').select2();
         });
    </script>
