
<head>
    <meta name="layout" content="ERPmainTemplateT2">
 <style>

 table {
   border-collapse: collapse;
   width: 100%;
 }

 th, td {
   text-align: center;
   padding: 1px;
 }

 tr:nth-child(even){background-color: #f2f2f2}

 th {
   background-color: #4CAF50;
   color: white;
 }
 </style>

</head>
<!-- Page Content  Copy Coding use Only-------------------------------------->
<div id="page-wrapper">
    <div class="container-fluid">
        <g:render template="/layouts/home_back_btn_T2"/>

        <div class="row">
            <div class="col-lg-12">

                <!-- <h3 class="page-header" style="text-align: center;">Create Template</h3> -->

                 <!-- Start coding only in This block ------------------->
            </br></br>

            <table align='center' width='50%'>
             <tr>
                <g:each var="linktypelist" in="${rolelist}" status="i">
                            <td><table  align='center' width='100%'>
                                        <tr  height=40>

                                            <th ><center><big>${linktypelist.linktype.get(1)}</big></center></th>

                                        </tr>
                    <g:each var="link" in="${linktypelist}" status="j">

                        <tr >

                                         <td width='100%'>
                                            <g:link controller="${link.controller_name}" action="${link.action_name}">
                                            <button style="width: 200px;margin:10px;" type="submit" class="btn
                                            btn-primary" >${link
                                            .link_name}</button>
                                            </g:link></td>

                         </tr>
                </g:each></td>  </table>
                </g:each> </tr>
            </table>


 <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>

            </div>
            <!-- /.col-lg-12 -->
        </div> <!-- /.row -->


    </div> <!-- /.container-fluid -->


</div>
<!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->