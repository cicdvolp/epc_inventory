<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_inst"/>
        <meta name="author" content="pratik"/>
        <style>
            form {
                font-size: 12px;
            }
            legend {
                text-align: left;
                width: auto;
                border: 0px solid black;
                font-size: 16px;
                color: blue;
                margin-bottom: 0px;
            }
            table tr td:empty {
                width: 50px;
            }
            table tr td {
                padding-top: 5px;
                padding-bottom: 5px;
            }
            #errmsg {
                color: red;
                font-size: 15px;
            }
            .loader {
                border: 16px solid #f3f3f3;
                border-radius: 50%;
                border-top: 16px solid #3498db;
                width: 120px;
                height: 120px;
                -webkit-animation: spin 2s linear infinite; /* Safari */
                animation: spin 2s linear infinite;
            }
            /* Safari */
            @-webkit-keyframes spin {
                0% { -webkit-transform: rotate(0deg); }
                100% { -webkit-transform: rotate(360deg); }
            }
            @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-topline-purple">
                                <!-- <div class="card-body"> -->
                                    <ul class="nav nav-tabs" style="background:#eaeef3;">
                                        <!--
                                            <li class="${session.link=='home'?'active':''}"><a data-toggle="tab" href="#home" style="color:#70a7ff">Students</a></li>
                                            <li class="${session.link=='menu1'?'active':''}"><a data-toggle="tab" href="#menu1" style="color:#70a7ff">Faculties</a></li>
                                        -->
                                        <li class="active"><a data-toggle="tab" href="#home" style="color:#70a7ff">Students</a></li>
                                        <li><a data-toggle="tab" href="#menu1" style="color:#70a7ff">Faculties</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <div align="center">
                                                <g:if test="${flash.message}">
                                                    <center><h4 style="color: red;"><div class="message" role="status">${flash.message}</div></h4></center>
                                                </g:if>
						                        <g:form action="saveAssignRoles">
                                                    <br/>
                                                    <table width="75%">
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Role Type&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <g:select class="form-control" id="roletype" name="roletype" from="${roletype}" optionValue="${{it.type}}" optionKey="id" noSelection="${['null':'Select Role Type...']}" onchange="getRoles();getCurrentAy();" style="width:400px;" required="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Role&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <div id="role">
                                                                    <input name="role" class="form-control" type="text" id="roleid" style="width:400px;" required />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">
                                                                <table width="95%" id="aychange">
                                                                    <tr>
                                                                        <td style="text-align:right" ><label style="font-size:14px">Academic Year&nbsp;&nbsp;&nbsp;</label></td>
                                                                        <td>
                                                                            <g:select class="form-control" id="ay" name="ay" from="${academicyear}" optionValue="ay" onchange="getDivision();" optionKey="id" value="${aay?.academicyear?.id}" style="width:400px;" required="true" />
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align:right" ><label style="font-size:14px">Semester&nbsp;&nbsp;&nbsp;</label></td>
                                                                        <td>
                                                                            <g:select class="form-control" id="sem" name="sem" from="${semester}" optionValue="sem" onchange="getDivision();" optionKey="id" value="${aay?.semester?.id}" style="width:400px;" required="true" />
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:right" ><label style="font-size:14px">Program Type&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <g:select class="form-control" id="programtype" name="programtype" from="${programtype}" optionValue="${{it.name}}" noSelection="['-1':'All']" onchange="callme();getDivision();" optionKey="id" style="width:400px;" required="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Program&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <div id="program">
                                                                    <g:select class="form-control" id="programid" name="programid" from="${programList}" optionValue="${{it.name}}" optionKey="id" noSelection="['-1':'All']" onchange="getDivision();" style="width:400px;" required="true" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Year&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <g:select class="form-control" id="year" name="year" from="${yearList}" noSelection="['-1':'All']" onchange="getDivision();" optionKey="id" style="width:400px;" required="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Division &nbsp;&nbsp;&nbsp;</label></td>
                                                            <td id="division">
                                                                <g:select class="form-control" id="divisionid" name="division" from="${divisionoffering}" noSelection="['-1':'All']" optionKey="id" style="width:400px;" required="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <table width="50%">
                                                                <tr>
                                                                    <td align="center" width="30%">
                                                                        <g:submitToRemote  url="[action: 'showCompleteList']" value="Fetch Student List" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary"
                                                                        onFailure ="alert('Something went wrong');" update="updateMe" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>

                                                                        <g:submitToRemote  url="[action: 'getStudentList']" value="Fetch Role Assigned List" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary"
                                                                        onFailure ="alert('Something went wrong');" update="updateMe" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>

                                                                        <!--
                                                                            <input type='button' id="btn" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-danger" onclick="callspinner();revokeRole()" value='Revoke Role'/>
                                                                        -->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </tr>
                                                    </table>
                                                </g:form>
                                                <br />
                                                <div id="spinner" class="spinner" style="display:none; text-align: center;">
                                                    <table>
                                                        <center><asset:image src="loading.gif"  alt="Image" width="100px" height="100px"/></center>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <div align="center">
                                                <g:if test="${flash.message}">
                                                    <center><h4 style="color: red;"><div class="message" role="status">${flash.message}</div></h4></center>
                                                </g:if>
                                                <g:form action="getFacultyList">
                                                    <br />
                                                    <table width="75%">
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Role Type&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <g:select class="form-control" id="roletypefaculty" name="roletypefaculty" from="${roletype}" optionValue="${{it.type}}" optionKey="id" noSelection="${['null':'Select Role Type...']}" onchange="getRolesforFaculty()" style="width:400px;" required="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Role&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <div id="rolefaculty">
                                                                    <input name="roleidfaculty" class="form-control" type="text"  id="roleidfaculty" style="width:400px;" required />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <!-- <tr>
                                                            <td style="text-align:right" ><label style="font-size:14px">Department Type&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <g:select class="form-control" id="departmentType" name="departmentType" from="${departmentTypeList}" optionValue="${{it.name}}" noSelection="['-1':'All']" onchange="getDepartment()" optionKey="id" style="width:400px;" required="true" />
                                                            </td>
                                                        </tr> -->
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Department&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <div id="departmentofFaculty">
                                                                    <g:select class="form-control" id="departmentList" name="departmentList" from="${departmentListforInst}" noSelection="['-1':'All']" style="width:400px;" optionKey="id" required="true" />
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="text-align:right"><label style="font-size:14px">Employee Type&nbsp;&nbsp;&nbsp;</label></td>
                                                            <td>
                                                                <g:select class="form-control" id="empType" name="empType" from="${employeeType}" noSelection="['-1':'All']" optionKey="id" style="width:400px;" required="true" />
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <table width="50%">
                                                                <tr>
                                                                    <td align="center" width="30%">
                                                                        <g:submitToRemote  url="[action: 'getCompleteFacultyList']" value="Fetch Faculty List" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary"
                                                                        onFailure ="alert('Something went wrong');" update="updateMe" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>

                                                                        <g:submitToRemote  url="[action: 'getFacultyList']" value="Fetch Role Assigned Faculty List" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary"
                                                                        onFailure ="alert('Something went wrong');" update="updateMe" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>

                                                                        <!--
                                                                            <input type='button' id="btn" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-danger" onclick="callspinner1();revokeRoleforFaculty()" value='Revoke Role'/>
                                                                        -->
                                                                        <!-- <button type="submit" class="btn btn-primary" id="button1" onclick="getStudentList()"><span class="glyphicon glyphicon-refresh"></span> Fetch Student List</button> -->
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </tr>
                                                    </table>
                                                </g:form>
                                            </div>
                                            <br/>
                                            <div id="spinner1" class="spinner" style="display:none; text-align: center;">
                                                <table>
                                                    <center><asset:image src="loading.gif"  alt="Image" width="100px" height="100px"/></center>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                <!-- </div> -->
                            </div>
                        </div>
                    </div>
                    <div id="updateMe"></div>
                    <div id="FacultyTable"></div>
                <!-- end content here -->
            </div>
        </div>
        <script>
            function callme(){
                var programtype = document.getElementById("programtype").value;
                //alert("programtype : " + programtype);
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("program").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET", "${request.getContextPath()}/roleLinks/getProgramList?programtype="+programtype,true);
                xmlhttp.send();
            }

            function getDivision(){
                var ay = document.getElementById("ay").value;
                var sem = document.getElementById("sem").value;
                var programtype = document.getElementById("programtype").value;
                var program = document.getElementById("programid").value;
                var year = document.getElementById("year").value;
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("division").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET", "${request.getContextPath()}/roleLinks/getDivisionList?programtype="+programtype+"&program="+program+"&ay="+ay+"&sem="+sem+"&year="+year,true);
                xmlhttp.send();
            }

            function getCurrentAy(){
                var roletype = document.getElementById("roletype").value;
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("aychange").innerHTML = this.responseText;
                        getDivision();
                    }
                };
                xmlhttp.open("GET", "${request.getContextPath()}/roleLinks/getCurrentAy?roletype="+roletype,true);
                xmlhttp.send();
            }

            function getRoles(){
                var roletype = document.getElementById("roletype").value;
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("role").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET", "${request.getContextPath()}/roleLinks/getRolesList?roletype="+roletype,true);
                xmlhttp.send();


            }
            function revokeRole(){
                var ay = document.getElementById("ay").value;
                var sem = document.getElementById("sem").value;
                var division = document.getElementById("divisionid").value;
                var programtype = document.getElementById("programtype").value;
                var program = document.getElementById("programid").value;
                var year = document.getElementById("year").value;
                var roletype = document.getElementById("roletype").value;
                var role = document.getElementById("roleid").value;
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        location.reload();
                    }
                };
                xmlhttp.open("GET", "revokeRoles?programtype="+programtype +"&program="+program+"&year="+year+"&roletype="+roletype+"&role="+role+"&ay="+ay+"&sem="+sem+"&division="+division,true);
                xmlhttp.send();
            }

            function  revokeRoleforFaculty(){
                var departmentList = document.getElementById("departmentList").value;
                var empType = document.getElementById("empType").value;
                var roletypefaculty = document.getElementById("roletypefaculty").value;
                var roleidfaculty = document.getElementById("roleidfaculty").value;
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("updatemessage2").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET", "${request.getContextPath()}/roleLinks/revokeRoleforFaculty?departmentList="+departmentList+"&empType="+empType+"&roletypefaculty="+roletypefaculty+"&roleidfaculty="+roleidfaculty,true);
                xmlhttp.send();
            }
            
            function callspinner(){
                var role = document.getElementById("roleid").value;
                if(role=="") {
                    alert("Please select Role!");
                    document.getElementById("focus").focus();
                } else {
                    $('#spinner').show();
                }
            }
            function  callspinner1(){
                var role = document.getElementById("roleidfaculty").value;
                if(role=="") {
                    alert("Please select Role for Faculty!");
                    document.getElementById("focus").focus();
                } else {
                    $('#spinner1').show();
                }
            }
            function callspinner2(){
                $('#spinner').show();
            }
            function getDepartment(){
                var departmentType=document.getElementById("departmentType").value;
                //alert("departmentType : " + departmentType);
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("departmentList").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET", "${request.getContextPath()}/roleLinks/getDepartmentforFaculty?departmentType="+departmentType,true);
                xmlhttp.send();
            }
            function  getRolesforFaculty(){
                var roletypefaculty = document.getElementById("roletypefaculty").value;
                //alert("roletypefaculty : " + roletypefaculty);
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("rolefaculty").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("GET", "${request.getContextPath()}/roleLinks/getRolesforFaculty?roletypefaculty="+roletypefaculty,true);
                xmlhttp.send();
            }
            /*
            $(document).ready(function(){
            $(document).ajaxStart(function(){
            $("#wait").css("display", "block");
            });
            $(document).ajaxComplete(function(){
            $("#wait").css("display", "none");
            });
            $("button").click(function(){
            $("#btn").(location.reload(););
            });
            });*/
        </script>
        <!-- end page content -->
    </body>
</html>