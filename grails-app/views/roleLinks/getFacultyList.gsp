<g:if test="${flash.message}">
	<center><h4 style="color: red;"><div class="message" role="status">${flash.message}</div></h4></center>
</g:if>
<div id="newtable">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-topline-purple">
                        <div class="card-head">
                            <header class="erp-table-header">Faculty List</header>
                        </div>
                        <!--<div class="card-body">-->
                        <g:form controller="RoleLinks" action="revokeRoleforFaculty">
                            <g:hiddenField name="roletypefaculty" value="${roleType.id}" />
                            <g:hiddenField name="roleidfaculty" value="${role?.id}" />
                            <!-- <g:hiddenField name="departmentType" value="${depttypeid}" /> -->
                            <g:hiddenField name="departmentList" value="${deptid}" />
                            <g:hiddenField name="empType" value="${emptypeid}" />
                            <div class="table-responsive" id="bar-parent">
                                <table id="exportTable" class="table table-bordered table-condensed">
                                    <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                            <th class="mdl-data-table__cell--non-numeric">
                                               <g:checkBox style="width: 20px;height: 20px;" class="checkboxsize" name="myCheckbox" value="${true}" id="headerCheckbox" />
                                               Select All
                                            </th>
                                            <th class="mdl-data-table__cell--non-numeric">Employee Code</th>
                                            <th class="mdl-data-table__cell--non-numeric">Name</th>
                                            <th class="mdl-data-table__cell--non-numeric">Department</th>
                                            <th class="mdl-data-table__cell--non-numeric">Employee Type</th>
                                            <th class="mdl-data-table__cell--non-numeric">Role Type</th>
                                            <th class="mdl-data-table__cell--non-numeric">Role</th>
                                            <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <g:each in="${instructorList1}" var="inst" status="i">
                                            <tr>
                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                <td class="mdl-data-table__cell--non-numeric">
                                                    <center><input  style="width: 20px;height: 20px;" type="checkbox" name="checkedValue" class="columnCheckbox checkboxsize" value="${inst?.id}" checked/></center>
                                                </td>
                                                <td class="mdl-data-table__cell--non-numeric">${inst.employee_code}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${inst.person.fullname_as_per_previous_marksheet}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${inst.department}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${inst.employeetype}</td>
                                                <g:each in="${roleType}" var="rlt" status="j">
                                                    <td class="mdl-data-table__cell--non-numeric">${rlt}</td>
                                                </g:each>
                                                <g:each in="${role}" var="rl" status="k">
                                                    <td class="mdl-data-table__cell--non-numeric">${rl}</td>
                                                </g:each>
                                                <td class="mdl-data-table__cell--non-numeric"><g:link controller="RoleLinks" action="deleteRole" onclick="return confirm('Do you want to delete ${role} Role of ${instructorList1[i]?.person?.fullname_as_per_previous_marksheet} in ${roleType} Module ?')" params="[ischange:1,id:role?.id,gr_no:instructorList1[i]?.employee_code]" <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true"></i></g:link></td>
                                            </tr>
                                        </g:each>
                                    </tbody>
                                </table>
                            </div>
                            <g:if test="${instructorList1}">
                                <div align="center">
                                    <br><br>
                                    <g:submitToRemote url="[action: 'revokeRoleforFaculty', controller:'RoleLinks']" update="updatemessage2" value="Revoke Role" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-danger" before="if(!confirm('Are You sure!')) return true;document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                </div>
                            </g:if>
                        </g:form>
                        <!-- </div> -->
                    </div>
                    <div id="updatemessage2"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<g:render template="/layouts/smart_template_inst/js_datatable" />
<script>
 $(document).ready(function(){
       $('#headerCheckbox').click(function() {
            var checked = this.checked;
            $('.columnCheckbox').prop('checked', checked);
       });
 });
</script>