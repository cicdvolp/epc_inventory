

<g:form action="proceedclickAdmission">
    <input type="hidden" id="instructorid" name="instructorid" value="${instructorid}">

       <g:if test="${rolelist.size()==0}">
             <div class="row">
                 <div class="col-md-12">
                     <div class="row">
                         <div class="col-md-12">
                             <div class="card card-topline-purple">
                             <br>
                                No Roles are Allocated.
                             <br>
                         </div>
                     </div>
                 </div>
             </div>
       </g:if>
       <g:else>
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="card card-box">

                        <div class="card-body " id="bar-parent">
                            <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                <g:set var="x" value="${1}"/>
                                <thead>
                                    <tr>
                                         <th>Sr.No</th>
                                         <th>Role Type</th>
                                        <th>Role</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     <g:each in="${rolelist}" var="r1" status="i">
                                        <tr>
                                          <td>${i+1}</td>
                                          <td>${r1?.roletype?.type}</td>
                                          <td>${r1?.role}</td>
                                          <td><g:link action="deleteRoleAdmission" params="[instid:instructorid,id:r1.id]"  style="color: blue">Delete  </g:link></td>
                                        </tr>
                                     </g:each>
                                 </tbody>
                            </table>
                        </div>
                        <br>
                       <center> <g:submitToRemote url="[action: 'addNewRoleAdmission']" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" update="updaterole" value="Add New Role"/></center>
                        <br>

                    </div>
                </div>
            </div>
        </g:else>
    </g:form>
  <div id="updaterole"></div>
  <g:render template="/layouts/smart_template_inst/datatable_js" />


