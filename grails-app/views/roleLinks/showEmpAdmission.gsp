<head>
     <meta name="layout" content="smart_main_datatable_inst" />
     <meta name="author" content="poonam" />
     <!-- jQuery -->
         <asset:javascript src="jqueryT1.min.js" />
         <!-- Metis Menu Plugin JavaScript -->
         <asset:javascript src="metisMenuT1.min.js" />
         <!-- Custom Theme JavaScript -->
         <asset:javascript src="sb-adminT1.js" />
</head>

 <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- start content here -->
                    <g:if test="${flash.message}">
                        <div class="message" role="status">${flash.message}</div>
                    </g:if>
                    <div class="card">
                        <g:form url="[controller:'RoleType',action: 'addNewRoleAdmission']" >
                            <g:if test="${flash.message}">
                                <div class="message" role="status">${flash.message}</div>
                             </g:if>
                            <g:form action="proceedclickAdmission">
                                <div class="card-body row">
                                    <div class="col-sm-5">
                                         <label>Employee : </label>
                                          <g:select class="js-example-basic-single class="form-control" id="emp" name="emp" from="${emplist}" value="${defaultInstructor.id}" optionKey="id"/><br>
                                    </div>
                                    <div class="col-sm-5">
                                         <label>&nbsp</label>
                                        <br>
                                        <center> <g:submitToRemote url="[action: 'proceedclickAdmission']"  class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" update="updateme" value="Fetch Roles"/></center>
                                     </div>

                                </div>
                            </g:form>
                    </div>
                            <div id="updateme">
                            <g:form action="addNewRoleAdmission">
                                <input type="hidden" id="instructorid" name="instructorid" value="${defaultInstructor.id}">

                                <div class="row">
                                      <div class="col-md-12 col-sm-12">
                                          <div class="card card-box">

                                              <div class="card-body " id="bar-parent">
                                                  <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                       <g:set var="x" value="${1}"/>
                                                       <thead>
                                                           <tr>
                                                             <th>Sr.No</th>
                                                             <th>Role Type</th>
                                                             <th>Role</th>
                                                             <th>Action</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                             <g:each in="${defInstructorRoleList}" var="r1" status="i">
                                                                <tr>
                                                                  <td>${i+1}</td>
                                                                  <td>${r1?.roletype?.type}</td>
                                                                  <td>${r1?.role}</td>
                                                                  <td><g:link action="deleteRoleAdmission" params="[instid:defaultInstructor.id,id:r1.id]" onclick="return confirm('Do you want to Delete ?')"><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i> </g:link></td>
                                                                </tr>
                                                             </g:each>
                                                      </tbody>
                                                 </table>
                                                 <center><g:submitToRemote url="[action: 'addNewRoleAdmission']" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" update="updaterole" value="Add New Role"/></center>
                                              </div>
                                          </div>
                                      </div>
                                </div>
                            </g:form>
                           <div id="updaterole"></div>
                    </g:form>
                </div>
           </div>
    </div>
</div>

<script>
       $(document).ready(function() {
            $('.js-example-basic-single').select2();
       });

       var year = new Date().getYear()+1900;
       var  month = new Date().getMonth()+1;
       if(month<=9)
       month = 0+''+month
       document.getElementById("month").value = year +'-'+ month;
       function  callme(){
        $('#spinner').show();
             var month = document.getElementById("month").value;
             var xmlhttp = new XMLHttpRequest();
             xmlhttp.onreadystatechange = function() {
                 if (this.readyState == 4 && this.status == 200) {
                     document.getElementById("document").innerHTML = this.responseText;
                     document.getElementById("month").value = month;

                     $('#spinner').hide();
                 }
             };
             xmlhttp.open("GET", "myAttendanceReport?month=" + month, true);
             xmlhttp.send();
       }
</script>





