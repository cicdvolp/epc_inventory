<div class="col-md-12 mobile-table-responsive">
    <div class="card card-topline-purple full-scroll-table">
        <div class="card-head">
            <header class="erp-table-header">Role Links List</header>
        </div>
        <!--<div class="card-body">-->
            <div class="table-responsive" id="bar-parent">
                <table id="exportTable" class="display nowrap erp-full-width mdl-data-table ml-table-bordered">
                    <thead>
                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                            <th class="mdl-data-table__cell--non-numeric">Role Type</th>
                            <th class="mdl-data-table__cell--non-numeric">Role</th>
                            <th class="mdl-data-table__cell--non-numeric">Vue JS Redirect Name</th>
                            <th class="mdl-data-table__cell--non-numeric">Sort Order</th>
                            <th class="mdl-data-table__cell--non-numeric">Link Name</th>
                            <th class="mdl-data-table__cell--non-numeric">Link Display Name</th>
                            <th class="mdl-data-table__cell--non-numeric">User Type</th>
                            <th class="mdl-data-table__cell--non-numeric">Active</th>
                            <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            <th class="mdl-data-table__cell--non-numeric">Is Quick Link</th>
                            <th class="mdl-data-table__cell--non-numeric">Is Icon Set?</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each var="i" in="${roleLinksList}" status="k">
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">${count=count+1}</td>
                                <td class="mdl-data-table__cell--non-numeric">${i.role?.roletype?.type}</td>
                                <td class="mdl-data-table__cell--non-numeric">${i.role?.role}</td>
                                <td class="mdl-data-table__cell--non-numeric">${i?.vue_js_name}</td>
                                <td class="mdl-data-table__cell--non-numeric">${i.sort_order}</td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <g:link action="${i?.action_name}" controller="${i?.controller_name}" id="rediredrolelink" style="color:blue;">
                                        ${i?.link_name}
                                    </g:link>
                                </td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <g:link action="${i?.action_name}" controller="${i?.controller_name}" id="rediredrolelink" style="color:blue;">
                                        ${i?.link_displayname}
                                    </g:link>
                                </td>
                                <td class="mdl-data-table__cell--non-numeric">${i?.role?.usertype?.type}</td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <g:if test="${i.isrolelinkactive}">
                                        <g:link action="activeRoleLinks" onclick="return confirm('Are you sure you want to In-Active?')" params="[roleLinkId:i.id]" >
                                            <center><i class="fa fa-toggle-on fa-2x" style="color:Green" aria-hidden="true"></i></center>
                                        </g:link>
                                    </g:if>
                                    <g:else>
                                        <g:link action="activeRoleLinks" onclick="return confirm('Are you sure you want to Active?')" params="[roleLinkId:i.id]">
                                            <center><i class="fa fa-toggle-off fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                        </g:link>
                                    </g:else>
                                    <% /* <center><input type="checkbox" checked data-toggle="toggle" data-on="Active" data-off="In-Active" data-onstyle="success" data-offstyle="danger"></center>*/ %>
                                </td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <g:link action="rolelinks" params="[roleLinkId:i.id, roletype:i?.role?.roletype?.id, usertype:i?.role?.usertype?.id, role:i.role?.id, roleLinksList:i, isEdit:true]" id="editRoleLink">
                                        <i class="fa fa-pencil-square-o fa-2x" style="color:green" name="edits" title="Edit Role Link"></i>
                                    </g:link>
                                    &nbsp;&nbsp;&nbsp;
                                    <g:link action="rolelinks" params="[roleLinkId:i.id, roletype:i?.role?.roletype?.id, usertype:i?.role?.usertype?.id, role:i.role?.id, copy:'true', roleLinksList:i]" id="copyRoleLink">
                                        <i class="fa fa-copy fa-2x" style="color:black" name="copy" title="Copy Role Link"></i>
                                    </g:link>
                                    &nbsp;&nbsp;&nbsp;
                                    <g:link action="deleteRoleLinks" params="[roleLinkId:i.id]">
                                        <i class="fa fa-trash fa-2x" style="color:red" name="delete" title="Delete Role Link"></i>
                                    </g:link>
                                </td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <g:if test="${i.isquicklink}">
                                        <g:link action="setQuickLinks" onclick="return confirm('Are you sure you want to set as not Quick Link?')" params="[roleLinkId:i.id]" >
                                            <center><i class="fa fa-check fa-2x" style="color:Green" aria-hidden="true"></i></center>
                                        </g:link>
                                    </g:if>
                                    <g:else>
                                        <g:link action="setQuickLinks" onclick="return confirm('Are you sure you want to set as Quick Link?')" params="[roleLinkId:i.id]">
                                            <center><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                        </g:link>
                                    </g:else>
                                </td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <g:if test="${i?.linkiconimagepath}">
                                        <g:if test="${i?.linkiconimagefilename}">
                                            <center><i class="fa fa-check fa-2x" style="color:Green" aria-hidden="true"></i></center>
                                        </g:if>
                                        <g:else>
                                            <center><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                        </g:else>
                                    </g:if>
                                    <g:else>
                                        <center><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                    </g:else>
                                </td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>
            </div>
        <!-- </div> -->
    </div>
</div>
<g:render template="/layouts/smart_template_inst/datatable_js" />