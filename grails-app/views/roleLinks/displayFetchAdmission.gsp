

<head>
<script language="JavaScript">
   function selectAll(source) {
      checkboxes = document.getElementsByName('rolecheckbox[]');
      for(var i in checkboxes)
         checkboxes[i].checked = source.checked;
   }
</script>
</head>

<g:form action="assignRoleAdmission">
<input type="hidden" id="instructorid"  name="instructorid" value="${instructorid}">
<br>
    <g:if test="${rolelist.size()==0}">
                     All Roles are Allocated.<br>
    </g:if>

    <g:else>
    Role:
    <br><br>

          <table class="table table-bordered" id="common" style="width:50%;">
            <g:set var="x" value="${1}"/>
            <thead>
                     <tr>
                       <th><g:checkBox name="checkAll" id ="checkAll" onClick="selectAll(this)" value="" />All</th>
                     </tr>
            </thead>
             <tbody>
             <g:each in="${rolelist}" var="rb" status="i">
             <tr>
                <td><g:checkBox name="rolecheckbox" id ="rolecheckbox" value="${rb.id}" checked="false"/>${rb.role}</td>
             </tr>
             </g:each>
              </tbody>
         </table>

         </g:else>
     <br>
 <input type="submit" class="btn btn-sm btn-success"  value="Assign Role"/>
 </g:form>

