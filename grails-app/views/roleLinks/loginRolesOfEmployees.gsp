<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="smart_main_datatable_inst" />
    <meta name="author" content="poonam" />
</head>

<body>
    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- start content here -->

                <g:if test="${flash.message}">
                       <center><h4 style="color: red;"><div class="alert alert-error">${flash.message}</div></h4></center>
                </g:if>

		        <div class="row">
                    <div class="col-md-12 col-sm-12 mobile-table-responsive">
                        <div class="card card-box full-scroll-table">
                          <div class="table-responsive">

                            <div class="card-body " id="bar-parent">
                                <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Sr No.</td>
                                            <th>Login Status</td>
                                            <th>Employee Code</td>
                                            <th>Employee Name</td>
                                            <th>Department</td>
                                            <th>Employee Type</td>
                                            <th>Designation</td>
                                            <th>Is Currently Working</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <g:each in="${finalList}" var="fList" status="i">
                                        <tr>
                                            <td >${i+1}</td>
                                            <td style="text-align:center">
                                                <g:if test="${fList?.login?.isloginblocked==false}">
                                                    <g:link action="loginIsBlocked" onclick="return confirm('Are you sure you want to de-activate the Login access of ${fList?.instructor?.person?.fullname_as_per_previous_marksheet} to VIERP?')" params="[LoginId:fList?.login?.id]">
                                                        <center><i class="fa fa-toggle-on fa-2x" style="color:Green" title="active" aria-hidden="true"></i></center>
                                                    </g:link>
                                                </g:if>
                                                <g:else>
                                                    <g:link action="loginIsBlocked" onclick="return confirm('Are you sure you want to activate the Login access of ${fList?.instructor?.person?.fullname_as_per_previous_marksheet} to VIERP?')" params="[LoginId:fList?.login?.id]">
                                                        <center><i class="fa fa-toggle-off fa-2x" style="color:Red" title="Inactive" aria-hidden="true"></i></center>
                                                    </g:link>
                                                </g:else>
                                            </td>
                                            <td >${fList?.instructor?.employee_code}</td>
                                            <td>${fList?.instructor?.person?.fullname_as_per_previous_marksheet}</td>
                                            <g:if test="${fList?.instructor?.department}">
                                                <td>
                                                <g:if test="${fList?.instructor?.department?.id==0}">--</g:if>
                                                <g:else>
                                                ${fList?.instructor?.department?.name}
                                                </g:else>
                                                </td>
                                            </g:if>
                                            <g:else>
                                                <td>Department not Set</td>
                                            </g:else>
                                            <td>${fList?.instructor?.employeetype?.type}</td>
                                            <td>${fList?.instructor?.designation?.name}</td>
                                            <g:if test="${fList?.instructor?.iscurrentlyworking==true}">
                                                <td style="text-align:center">YES</td>
                                            </g:if>
                                            <g:else>
                                                <td style="text-align:center">NO</td>
                                            </g:else>
                                        </tr>
                                    </g:each>
                                    </tbody>
                                </table>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>
		</div><!--/.container-fluid-->
	</div><!--/#page-wrapper-->
    <script>
     $(document).ready(function() {
         $('#common').DataTable( {
             dom: 'Bfrtip',
             buttons: [
                 'copyHtml5',
                 'excelHtml5',
                 'csvHtml5',
                 'pdfHtml5'
             ],
              paging: false
         } );
     } );
   </script>
</body>
</html>