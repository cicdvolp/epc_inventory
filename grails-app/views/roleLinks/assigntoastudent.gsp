<head>
    <meta name="layout" content="ERPmainTemplateT2">
                    <asset:stylesheet src="jquery-ui.css"/>
                    <asset:stylesheet src="datatables.min.css"/>
                    <asset:stylesheet src="buttons.dataTables.min.css"/>
                    <asset:javascript src="jquery.min.js"/>
                    <asset:javascript src="datatables.min.js"/>
                    <asset:javascript src="dataTables.buttons.min.js"/>
                    <asset:javascript src="jszip.min.js"/>
                    <asset:javascript src="pdfmake.min.js"/>
                    <asset:javascript src="vfs_fonts.js"/>
                    <asset:javascript src="buttons.html5.min.js"/>
</head>
<!-- Page Content  Copy Coding use Only-------------------------------------->
<div id="page-wrapper">
    <div class="container-fluid">
        <g:render template="/layouts/home_back_btn_T2"/>

        <div class="row">
            <div class="col-lg-12">

                <h3 class="page-header" style="text-align: center;">Assign Roles to students</h3>
                 <!-- Start coding only in This block ------------------->
                                <g:if test="${flash.message}">
                                        <div class="message" role="status">${flash.message}</div>
                                </g:if>
                       <label>Search Student in List</label>
                             <input type="text" id="searchin" value="${abc}" class="form-control" placeholder="enter Student's PRN no" onkeypress="search()">

                             <g:select class="form-control" name="learneri" id="learner" from="${stdlist}" />
                             <br>
                             <g:submitButton name="getdata" class="btn btn-sm btn-success" value="Proceed" onclick="funcall()"/>
            </div>
              <div id="abcd"></div>
              <div id="efgh"></div>
        </div>
    </div>
</div>
<script>
$(document).ready(function() {
     var text = document.getElementById("searchin").value;
     var options = learner.options;
     setTimeout(function(){
                                     funcall();
                              }, 300);
     for (var i = 0; i < options.length; i++) {
         var option = options[i];
         var optionText = option.text;
         var lowerOptionText = optionText.toLowerCase();
         var lowerText = text.toLowerCase();
         var regex = new RegExp("^" + text, "i");
         var match = optionText.match(regex);
         var contains = lowerOptionText.indexOf(lowerText) != -1;
         if (match || contains) {
             option.selected = true;
             return;
         }
         searchin.selectedIndex = 0;
     }
});
searchin.addEventListener("keyup", function (e) {
var text = e.target.value;
 var options = learner.options;
        for (var i = 0; i < options.length; i++) {

            var option = options[i];
            var optionText = option.text;
            var lowerOptionText = optionText.toLowerCase();
            var lowerText = text.toLowerCase();
            var regex = new RegExp("^" + text, "i");
            var match = optionText.match(regex);
            var contains = lowerOptionText.indexOf(lowerText) != -1;
            if (match || contains) {
                option.selected = true;
                return;
            }
            searchin.selectedIndex = 0;
        }
        }
        );

 function funcall(){
    var learner = document.getElementById("learner").value;
                   var xmlhttp = new XMLHttpRequest();
                   xmlhttp.onreadystatechange = function() {
                                       if (this.readyState == 4 && this.status == 200) {
                                           document.getElementById("abcd").innerHTML= this.responseText;
                                       }
                                   };
                   xmlhttp.open("GET", "Tableforrole?learner="+learner, true);
                   xmlhttp.send();
 }
 function funconcall(){
          var xmlhttp = new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function() {
                                                if (this.readyState == 4 && this.status == 200) {
                                                    document.getElementById("efgh").innerHTML= this.responseText;
                                                }
                                            };
                            xmlhttp.open("GET", "addroles", true);
                            xmlhttp.send();
 }
 function selectAll(source) {
 		checkboxes = document.getElementsByName('Role');
 		for(var i in checkboxes)
 			checkboxes[i].checked = source.checked;
 	}

</script>