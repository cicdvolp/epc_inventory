<g:if test="${flash.message}">
	<center><h4 style="color: red;"><div class="message" role="status">${flash.message}</div></h4></center>
</g:if>
<div id="newtable">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-topline-purple">
                        <div class="card-head">
                            <header class="erp-table-header">STUDENT LIST</header>
                        </div>
                        <!--<div class="card-body">-->
                            <div class="table-responsive" id="bar-parent">
                                <table id="exportTable" class="display nowrap erp-full-width">
                                    <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                            <th class="mdl-data-table__cell--non-numeric">PRN No</th>
                                            <th class="mdl-data-table__cell--non-numeric">Name</th>
                                            <th class="mdl-data-table__cell--non-numeric">Division</th>
                                            <th class="mdl-data-table__cell--non-numeric">Role Type</th>
                                            <th class="mdl-data-table__cell--non-numeric">Role</th>
                                            <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <g:each in="${learner}" var="lnr" status="i">
                                            <tr>
                                               <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                               <td class="mdl-data-table__cell--non-numeric">${lnr.learner.registration_number}</td>
                                               <td class="mdl-data-table__cell--non-numeric">${lnr.learner}</td>
                                               <td class="mdl-data-table__cell--non-numeric">${lnr.divisionoffering}</td>
                                                <g:each in="${roleType}" var="rlt" status="j">
                                                    <td class="mdl-data-table__cell--non-numeric">${rlt}</td>
                                                </g:each>
                                                <g:each in="${role}" var="rl" status="k">
                                                    <td class="mdl-data-table__cell--non-numeric">${rl}</td>
                                                </g:each>
                                                <td class="mdl-data-table__cell--non-numeric"><g:link controller="RoleLinks" action="deleteRole" onclick="return confirm('Do you want to delete ${role} Role of ${learner[i]?.learner?.person?.fullname_as_per_previous_marksheet} in ${roleType} Module ?')" params="[ischange:1,id:role?.id,gr_no:learner[i]?.learner?.registration_number]" <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true"></i></g:link></td>
                                            </tr>
                                        </g:each>
                                    </tbody>
                                </table>
                            </div>

                            <g:if test="${learner}">
                                <div align="center">
                                    <g:form controller="RoleLinks" action="revokeRoles">
                                        <g:hiddenField name="ay" value="${ay}" />
                                        <g:hiddenField name="sem" value="${sem}" />
                                        <g:hiddenField name="division" value="${division}" />
                                        <g:hiddenField name="programtype" value="${programtypeid}" />
                                        <g:hiddenField name="program" value="${programid}" />
                                        <g:hiddenField name="year" value="${yearid}" />
                                        <g:hiddenField name="roletype" value="${roleType.id}" />
                                        <g:hiddenField name="role" value="${roleid}" />
                                        <g:submitToRemote url="[action: 'revokeRoles', controller:'RoleLinks']" update="updatemessage3" value="Revoke Role" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-danger" before="if(!confirm('Are You sure!')) return true;document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                    </g:form>
                                </div>

                                <div id="updatemessage3"></div>
                            </g:if>
                        <!-- </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<g:render template="/layouts/smart_template_inst/datatable_js" />