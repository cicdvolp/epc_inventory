<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="smart_main_inst" />
        <meta name="author" content="poonam" />
         <!-- jQuery -->
        <asset:javascript src="jqueryT1.min.js" />
        <!-- Metis Menu Plugin JavaScript -->
        <asset:javascript src="metisMenuT1.min.js" />
        <!-- Custom Theme JavaScript -->
        <asset:javascript src="sb-adminT1.js" />
	</head>

    <body>
       <!-- start page content -->

           <div class="page-content-wrapper">
               <div class="page-content">
                   <g:render template="/layouts/smart_template_inst/breadcrumb" />
                        <g:if test="${flash.message}">
                            <div class="alert alert-success" >${flash.message}</div>
                       </g:if>

                        <div class="row">
                             <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="card">

                                        <g:form controller="RoleLinks">
                                            <div class="card-body row">
                                                <div class="col-sm-3">
                                                    <label>Enter Student PRN No :</label>
                                                    <input name="grno" id="grno" type="text" class="form-control" style="width:200px;" placeholder="PRN. No." required />
                                                </div>
                                                <div class="col-sm-3">
                                                    <label>&nbsp</label>
                                                    <br>
                                                   <!-- <button type="submit" id="btnSubmit" class="btn btn-primary"><i class="fa fa-refresh"></i>&nbsp;Fetch Details</button>-->

                                                    <g:submitToRemote url="[action: 'getStudentData']" value="Fetch Details" id="myBtn" class="btn btn-primary" update="UpdateMe"/>
                                                </div>
                                            </div>
                                        </g:form>
                                     </div>

                            </div>
                         </div>
                        <div id="UpdateMe"></div>
		                <div id="UpdateMe2"></div>
               </div><!--/#page-wrapper-->
           </div><!--/#wrapper-->

        <script>
        var input = document.getElementById("grno");
        input.addEventListener("keydown", function(event) {
          if (event.keyCode === 13) {
           event.preventDefault();
           document.getElementById("myBtn").click();
          }
        });

        function funconcall(){

                    var gr_no = document.getElementById("studGrno").value;
                    //alert("gr_no :"+gr_no);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                                        if (this.readyState == 4 && this.status == 200) {
                                                document.getElementById("UpdateMe2").innerHTML= this.responseText;
                                        }
                                    };
                    xmlhttp.open("GET", "${request.getContextPath()}/roleLinks/addRolesNew?gr_no="+gr_no,true);
                     xmlhttp.send();
        }

         function selectAll(source) {
                checkboxes = document.getElementsByName('Role');
                for(var i in checkboxes)
                    checkboxes[i].checked = source.checked;
            }

        </script>
<!-- jQuery -->
<asset:javascript src="jqueryT1.min.js" />

<!-- Bootstrap Core JavaScript -->
<asset:javascript src="bootstrapT1.min.js" />

<!-- Metis Menu Plugin JavaScript -->
<asset:javascript src="metisMenuT1.min.js" />

<!-- Custom Theme JavaScript -->
<asset:javascript src="sb-adminT1.js" />

	</body>
</html>