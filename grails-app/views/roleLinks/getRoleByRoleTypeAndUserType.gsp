
<label>Role<span class="erp-mandatory-mark">*</span> : </label>
<g:link controller="ERPRoomType" action="addRole">
    <i class="fa fa-plus-square" aria-hidden="true" onclick="document.getElementById('addRoleModal').style.display='block'" style="color:blue; float:right; padding-top:9px;" name="addsoftwaremodel" ></i>
</g:link>
<g:select class="form-control" name='role' id="roleList" value="${defaultrole?.id}" onchange="getRoleLinksByRole();"  from='${roleList}' optionValue="role_displayname"  optionKey="id" required="true"></g:select>
<!--<g:if test="${defaultrole == null}">
    <p style="color:red; display: inline;" id="errorroleid">&nbsp; Please Select Role..</p>
</g:if>
<g:else>
    <p style="color:red; display: inline;" id="errorroleid">&nbsp; </p>
</g:else>-->


<script>
    function roleValidate(){
        var role = document.getElementById("roleList").value;
        if(role> 0){
            document.getElementById("errorroleid").innerHTML = '';
        } else {
            document.getElementById("errorroleid").innerHTML = 'Please Select One..';
        }
    }

    function getRoleLinksByRole() {
        var role = document.getElementById("roleList").value;
        /*alert("role_type : " + role_type);*/
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                document.getElementById("filterdivtableid").innerHTML = this.responseText;
                $('#exportTable').DataTable( {
                    dom: 'Bfrtip',
                    buttons: [
                        'copyHtml5',
                        'excelHtml5',
                        'csvHtml5',
                        'pdfHtml5'
                    ],
                    paging: false
                } );
            }
        };
        xmlhttp.open("GET", "${request.contextPath}/RoleLinks/getRoleLinksByRoleType?role="+ role);
        xmlhttp.send();
    }
</script>