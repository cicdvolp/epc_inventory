<g:if test="${flash.message}">
       <div class="alert alert-error">${flash.message}</center>
</g:if>
<div class="card">

    <div class="row">
        <table class="table">
             <tr class="info">
                <td style="font-size:15px;text-align:right;">PRN Number&nbsp;:</td>
                <td><input type="text" class="form-control" id="studGrno" name="studGrno" value="${learner?.registration_number}" style="width:350px;" disabled ></td>

                <td style="font-size:15px;text-align:right;">Department&nbsp;:</td>
                <td><input type="text" class="form-control" id="dept" name="dept" value="${learner?.program?.department}" style="width:400px;" disabled ></td>
            </tr>

            <tr class="info">
                <td style="font-size:15px;text-align:right;">Student Name&nbsp;:</td>
                <td><input type="text" class="form-control" id="studName" name="studName" value="${learner}" style="width:350px;" disabled ></td>

                <td style="font-size:15px;text-align:right;">Program&nbsp;:</td>
                <td><input type="text" class="form-control" id="program" name="program" value="${learner?.program}" style="width:400px;" disabled ></td>
            </tr>

            <tr class="info">
                <td style="font-size:15px;text-align:right;">Shift&nbsp;:</td>
                <td><input type="text" class="form-control" id="shift" name="shift" value="${learner?.erpshift}" style="width:200px;" disabled ></td>

                <td style="font-size:15px;text-align:right;">Year&nbsp;:</td>
                <td>-</td>
            </tr>
        </table>
    </div>
  </div>

       <div class="row">
           <div class="col-md-12">
               <div class="row">
                   <div class="col-md-12">
                       <div class="card card-topline-purple">
                           <div class="table-responsive">
                               <table class="mdl-data-table ml-table-bordered">
                                   <thead>
                                      <th class="mdl-data-table__cell--non-numeric">Sr. no</th>
                                      <th class="mdl-data-table__cell--non-numeric">Role</th>
                                      <th class="mdl-data-table__cell--non-numeric">Module</th>
                                      <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                   </thead>
                                   <tbody>
                                       <g:set var="x" value="${1}"/>
                                       <g:each var="r" in="${rolelist}" >
                                       <tr>
                                           <td class="mdl-data-table__cell--non-numeric">${x}</td>
                                           <td class="mdl-data-table__cell--non-numeric">${r.role_displayname}</td>
                                           <td class="mdl-data-table__cell--non-numeric">${r.roletype.type_displayname}</td>
                                           <td class="mdl-data-table__cell--non-numeric"><g:link controller="RoleLinks" action="deleteRole" params="[id:r?.id,gr_no:learner?.registration_number]" onclick="return confirm('Do you want to Delete ?')"><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></g:link></td>
                                       </tr>
                                       <g:set var="x" value="${x+1}"/>
                                       </g:each>
                                   </tbody>
                               </table>
                           </div>
                           <br>
                           <center><g:submitButton class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" name="Add New Roles" value="Add New Roles" onclick="funconcall()" /></center>
                           <br>
                       </div>
                   </div>
               </div>
           </div>
       </div>
