<g:form>
    <input type="hidden" id="instructorid" name="instructorid" value="${instructorid}">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-topline-purple">
                        <div class="table-responsive">
                            <table class="mdl-data-table ml-table-bordered">
                                    <tr>
                                    <th class="mdl-data-table__cell--non-numeric">Sr. No</th>
                                    <th class="mdl-data-table__cell--non-numeric">Role Type</th>
                                    <th class="mdl-data-table__cell--non-numeric">Role</th>
                                    <th class="mdl-data-table__cell--non-numeric">Action</th>
                                    </tr>
                                 <g:each in="${roles}" var="r1" status="i">
                                    <tr>
                                      <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                      <td class="mdl-data-table__cell--non-numeric">${r1?.roletype?.type}</td>
                                      <td class="mdl-data-table__cell--non-numeric">${r1?.role}</td>
                                      <td class="mdl-data-table__cell--non-numeric"><g:link action="deleteAdmissionRole" params="[instid:instructorid,id:r1.id]"  style="color: blue">Delete  </g:link></td>
                                    </tr>
                                 </g:each>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</g:form>
