<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-purple">
            <div class="card-head">
                <header class="erp-table-header">
                    Category : ${invFinanceApprovingCategory?.name}, Approving Authority Post : ${invInstructorFinanceApprovingAuthority?.invfinanceapprovingauthority?.name}
                </header>
            </div>
            <!--<div class="card-body">-->
                <div class="table-responsive">
                    <table class="mdl-data-table ml-table-bordered">
                        <thead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                <th class="mdl-data-table__cell--non-numeric">Budget Details</th>
                                <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${invBudgetEscalationList}" var="bd" status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <span>Academic Year : </span><span class="erp-table-header" style="font-weight:900;">${bd?.data?.invbudget?.academicyear?.ay}</span>
                                        <br>
                                        <span>Activity Name : </span><span class="erp-table-header" style="font-weight:900;">${bd?.data?.invbudget?.activity_name}</span>
                                        <br>
                                        <span>Amount : </span><span class="erp-table-header" style="font-weight:900;">${bd?.data?.invbudget?.amount}</span>
                                        <br>
                                        <span>Request Date : </span><span class="erp-table-header" style="font-weight:900;">${bd?.data?.invbudget?.budget_entry_date}</span>
                                        <br>
                                        <span>Budget Level : </span><span class="erp-table-header" style="font-weight:900;">${bd?.data?.invbudget?.invbudgetlevel?.name}</span>
                                        <br>
                                        <span>Department : </span><span class="erp-table-header" style="font-weight:900;">${bd?.data?.invbudget?.department?.name}</span>
                                        <br>
                                        <span>File : </span><span class="erp-table-header" style="font-weight:900;"><a href="${bd?.url}" target="_blank">${bd?.data?.invbudget?.file_name}</a></span>

                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:link target="blank" controller="invBudget" action="getBudgetEscalationforApprove" params="[bde_id:bd?.data?.id]" class="btn btn-primary">Proceed</g:link>
                                    </td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
            <!--</div>-->
        </div>
    </div>
</div>