<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Quotation List</header>
                                    <!--    <div class="tools">
                                            <g:link  data-toggle="modal" data-target="#addNewInvBudget"><span aria-hidden="true" class="icon-plus" title="Add New Budget" style="float:right; font-size:20px;"></span></g:link>
                                        </div>
                                        -->
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Tax</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Quotation Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Vendor Company</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Quotation Details</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approve Quotation</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${quotationList}" var="quotes" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.tax}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.total}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.quotation_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.invvendor?.company_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                         <g:form action="viewQuotationDetails">
                                                              <input type="hidden" value="${quotes?.id}" name="invQuotationId">
                                                                    <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                        </g:form>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="invBudget" action="updateQuotationStatus" params="[invQuotationId : quotes?.id]">
                                                                 <g:if test="${quotes?.isapproved}">
                                                                    <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                                </g:if>
                                                                <g:else>
                                                                    <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                                </g:else>
                                                        </g:link>
                                                        </td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



