<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
        <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
        <script>
        function call(){
                    var materialId= document.getElementById("materialId").value;
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        if (this.readyState == 4 && this.status == 200) {
                            document.getElementById("materialPartDiv").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET", "${request.contextPath}/InvBudget/getMaterialPartsByMaterial?materialId=" + materialId);
                    xmlhttp.send();
        }
        function fetchDepts(){
                            var budgetLevel= document.getElementById("budgetLevelId").value;
                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function() {
                                if (this.readyState == 4 && this.status == 200) {
                                    document.getElementById("deptDiv").innerHTML = this.responseText;
                                }
                            };
                            xmlhttp.open("GET", "${request.contextPath}/InvBudget/fetchDepts?budgetLevel=" + budgetLevel);
                            xmlhttp.send();
                }

        </script>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                    <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                    <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card">
                            <div class="card-body">
                                <g:form action="saveInvBudget" enctype='multipart/form-data'>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="pwd">Academic Year:</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                            <g:select name="academicYearId"  optionValue="ay" from="${academicYearList}" style="width: 100% !important;"  class="select2" required="true"  value="${aay?.academicyear?.id}"  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="pwd">Budget Type:</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                            <g:select name="budgetTypeId"  optionValue="name" from="${invBudgetTypeList}" style="width: 100% !important;"  class="select2" required="true"  noSelection="['null':'Select Budget Type']" optionKey="id"/><br>
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="pwd">Budget Level:</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                            <g:select name="budgetLevelId"  optionValue="name" from="${invBudgetLevelList}" style="width: 100% !important;"  class="select2" required="true"   noSelection="['null':'Select Budget Level']" optionKey="id" onchange="fetchDepts()"/><br>
                                        </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                        <div class="col-sm-4">
                                            <label for="pwd">Budget/Activity Name:</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                            <input type="text" class="form-control" name="activityName" required="true">
                                        </div>
                                        <div class="col-sm-4">
                                            <label for="pwd">Amount:</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                            <input type="number" min="0" class="form-control" name="amount" required="true">
                                         </div>
                                          <div class="col-sm-4" id="deptDiv">

                                          </div>
                                          <div class="col-sm-4">
                                             <label>File: </label>
                                             <input type="file" class="form-control btn-primary" name="newFile">
                                         </div>
                                        <div class="col-sm-12">
                                            <center>
                                                <br>
                                                <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </center>
                                        </div>
                                    </div>
                                </g:form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Budget List</header>
                                        <!--
                                        <div class="tools">
                                            <g:link  data-toggle="modal" data-target="#addNewInvBudget"><span aria-hidden="true" class="icon-plus" title="Add New Budget" style="float:right; font-size:20px;"></span></g:link>
                                        </div>
                                        -->
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                         <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                                 <th class="mdl-data-table__cell--non-numeric">Activity</th>
                                                                 <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Budget Type</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Budget Level</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Department</th>
                                                    <th class="mdl-data-table__cell--non-numeric">File</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Add Details</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Budget Details</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-approved?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval By</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invBudgetList}" var="budget" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${budget?.data?.academicyear}</td>
                                                           <td class="mdl-data-table__cell--non-numeric">${budget?.data?.activity_name}</td>
                                                           <td class="mdl-data-table__cell--non-numeric">${budget?.data?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.data?.invbudgettype?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.data?.invbudgetlevel?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.data?.department?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <a href="${budget.url}" target="_blank">${budget?.data?.file_name}</a>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                           <center> <g:if test="${budget?.data?.isapprove}">
                                                                <label for="pwd">Budget Already Approved,Cannot Add</label>
                                                             </g:if>
                                                             <g:else>
                                                            <i class="fa fa-plus  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#addBudgetDetails${i}"></i>
                                                            </g:else></center>
                                                        </td>

                                                        <td class="mdl-data-table__cell--non-numeric"><center>
                                                            <g:link controller="InvBudget" action="viewSingleBudgetDetails" params="[invBudgetId : budget?.data?.id]"><button type="" class="btn btn-primary"> <span class="glyphicon" title="View Budget Details"></span>View Budget Details</button></g:link>
                                                        </center> </td>

                                                        <td class="mdl-data-table__cell--non-numeric">
                                                       <center>  <g:link controller="invBudget" action="activateInvBudget" params="[invBudgetId : budget?.data?.id]">
                                                            <g:if test="${budget?.data?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link></center>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <center> <g:if test="${budget?.data?.isapprove}">
                                                                <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else></center>
                                                        </td>
                                                        <td>
                                                            <g:each in="${budget?.escalation}" var="app" status="a">
                                                                <g:if test="${app?.invapprovalstatus?.name == 'Approved'}">
                                                                          <!--<i class="fa fa-user fa-2x" style="color:green" aria-hidden="true" title="${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}"></i>-->
                                                                          <strong>${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</strong>:-
                                                                          ${app?.invapprovalstatus?.name}
                                                                </g:if>
                                                                <g:else>
                                                                   <!-- <i class="fa fa-user fa-2x" style="color:red" aria-hidden="true" title="${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}"></i>-->
                                                                     <strong>${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</strong>:-
                                                                     ${app?.invapprovalstatus?.name}
                                                                </g:else>
                                                                <br>
                                                            </g:each>
                                                        </td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                         <center> <g:if test="${budget?.data?.isapprove}">
                                                            <label for="pwd">Budget Already Approved,Cannot Edit</label>
                                                         </g:if>
                                                         <g:else>
                                                             <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editInvBudget${i}"></i>
                                                          </g:else></center>
                                                        </td>
                                                    </tr>

                                                    <!-- Modal Add Budget Details-->
                                                   <div class="modal fade" id="addBudgetDetails${i}" role="dialog">
                                                           <div class="modal-dialog">
                                                             <div class="modal-content">
                                                               <div class="modal-header">
                                                                 <h4 class="modal-title">Add New Budget Details</h4>
                                                                 <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                               </div>
                                                              <g:form action="saveBudgetDetails">
                                                               <div class="modal-body">
                                                                 <div class="form-group">
                                                                     <label for="pwd">Budget:</label>
                                                                     <input type="hidden" value="${budget?.data?.id}" name="budgetId">
                                                                     <input type="text" class="form-control" name="budgetId" required="true" value="${budget?.data?.activity_name}" disabled optionKey="id">
                                                                 </div>
                                                                <div class="form-group">
                                                                 <label for="pwd">Material:</label>
                                                                 <span style="color:red; font-size: 16px">*</span>
                                                                     <g:select name="materialId"  optionValue="name" from="${invMaterialList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Material']" optionKey="id" onchange="call()"/><br>
                                                               </div>
                                                                <div class="form-group" id="materialPartDiv">
                                                                    <label for="pwd">Material Parts</label>
                                                                    <input type="text" class="form-control" name="materialParts" required="true" value="${materialPartList?.name}" disabled optionKey="id">
                                                                </div>
                                                                <div class="form-group">
                                                                     <label for="pwd">Cost Per Unit:</label>
                                                                     <span style="color:red; font-size: 16px">*</span>
                                                                     <input type="number" min=0 class="form-control" name="costPerUnit" required="true">
                                                                 </div>
                                                                 <div class="form-group">
                                                                   <label for="pwd">Quantity:</label>
                                                                   <span style="color:red; font-size: 16px">*</span>
                                                                   <input type="number" min=0 class="form-control" name="quantity" required="true">
                                                               </div>
                                                             <div class="form-group">
                                                                   <label for="pwd">Remarks:</label>
                                                                   <span style="color:red; font-size: 16px">*</span>
                                                                   <input type="text" class="form-control" name="remarks" required="true">
                                                               </div>
                                                               </div>
                                                               <div class="modal-footer">
                                                                 <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                               </div>
                                                              </g:form>
                                                     </div>
                                                   </div>
                                                 </div>

                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="editInvBudget${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title">Edit Budget</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="editInvBudget" enctype='multipart/form-data'>
                                                            <div class="modal-body">
                                                              <div class="form-group">
                                                                  <label for="pwd">Activity Name</label>
                                                                  <input type="hidden" value="${budget?.data?.id}" name="invBudget">
                                                                  <input type="text" class="form-control" name="activityName" required="true" value="${budget?.data?.activity_name}" readonly>
                                                              </div>
                                                              <div class="form-group">
                                                                <label for="pwd">Amount</label>
                                                                <input type="number" class="form-control" name="amount" required="true" value="${budget?.data?.amount}">
                                                            </div>
                                                              <div class="form-group">
                                                                   <label for="pwd">Budget Level:</label>
                                                                       <g:select value="${budget?.data?.invbudgetlevel?.id}" name="budgetLevelId"  optionValue="name" from="${invBudgetLevelList}" style="width: 100% !important;"  class="select2" required="true" optionKey="id"/><br>
                                                                 </div>
                                                                <div class="form-group">
                                                                 <label for="pwd">Department:</label>
                                                                       <g:select value="${budget?.data?.department?.id}" name="departmentId"  optionValue="name" from="${departmentList}" style="width: 100% !important;"  class="select2" required="true" optionKey="id"/><br>
                                                               </div>
                                                               <div class="form-group">
                                                                     <label for="pwd">Academic Year:</label>
                                                                         <g:select value="${budget?.data?.academicyear?.id}" name="academicYearId"  optionValue="ay" from="${academicYearList}" style="width: 100% !important;"  class="select2" required="true" optionKey="id"/><br>
                                                                   </div>
                                                               <div class="form-group">
                                                                      <label for="pwd">Budget Type:</label>
                                                                          <g:select name="budgetTypeId" value="${budget?.data?.invbudgettype?.id}" optionValue="name" from="${invBudgetTypeList}" style="width: 100% !important;"  class="select2" required="true" optionKey="id"/><br>
                                                                    </div>
                                                                <div class="form-group">
                                                                     <label>File: </label>
                                                                     <input type="file" class="form-control btn-primary" name="newFile">
                                                                 </div>
                                                            <div class="modal-footer">
                                                              <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                            </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </g:each>
                                                </tbody>
                                               </table>

                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                                <!-- Modal New -->
                                  <div class="modal fade" id="addNewInvBudget" role="dialog">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h4 class="modal-title">Add New Budget</h4>
                                          <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                       <g:form action="saveInvBudget">
                                        <div class="modal-body">
                                         <div class="form-group">
                                          <label for="pwd">Academic Year:</label>
                                              <g:select name="academicYearId"  optionValue="ay" from="${academicYearList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                        </div>
                                        <div class="form-group">
                                               <label for="pwd">Budget Type:</label>
                                                   <g:select name="budgetTypeId"  optionValue="name" from="${invBudgetTypeList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Budget Type']" optionKey="id"/><br>
                                             </div>
                                         <div class="form-group">
                                              <label for="pwd">Activity Name</label>
                                              <input type="text" class="form-control" name="activityName" required="true">
                                          </div>
                                          <div class="form-group">
                                             <label for="pwd">Budget Level:</label>
                                                 <g:select name="budgetLevelId"  optionValue="name" from="${invBudgetLevelList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Budget Level']" optionKey="id"/><br>
                                           </div>
                                          <div class="form-group">
                                           <label for="pwd">Departments:</label>
                                               <g:select name="departmentId"  optionValue="name" from="${departmentList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Department']" optionKey="id"/><br>
                                         </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                        </div>
                                       </g:form>
      </div>
      </div>
      </div>
      </div>
      </div>
    </div>
  </div>
  </div>
</body>
</html>



