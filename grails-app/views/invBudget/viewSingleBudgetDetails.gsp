<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple ">
                                      <div class="card-head">
                                           <header class="erp-table-header">Budget Details List</header>
                                       </div>
                                      <div class="card-body" id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Quantity</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Cost Per Unit</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total Cost</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    <!--
                                                    <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                                    -->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invBudgetDetailsList}" var="budgetDetails" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.invmaterial?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.quantity_required}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.approx_cost_per_unit}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.total_cost}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budgetDetails?.remark}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                           <center>  <g:if test="${budgetDetails?.isactive}">
                                                                <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else></center>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                           <center>  <g:if test="${isapprove}">
                                                                <label for="pwd">Budget Already Approved,Cannot Add</label>
                                                             </g:if>
                                                             <g:else>
                                                             <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editBudgetDetails${i}"></i>
                                                             </g:else></center>
                                                        </td>
                                                         <!-- Modal edit -->
                                                         <div class="modal fade" id="editBudgetDetails${i}" role="dialog">
                                                           <div class="modal-dialog">
                                                             <div class="modal-content">
                                                               <div class="modal-header">
                                                                 <h4 class="modal-title">Edit Budget Details</h4>
                                                                 <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                               </div>
                                                              <g:form controller="InvBudget" action="editBudgetDetails">
                                                               <div class="modal-body">
                                                                 <div class="form-group">
                                                                     <input type="hidden" value="${budgetDetails?.id}" name="budgetDetailsId">
                                                                     <input type="hidden" value="${invBudgetId}" name="invBudgetId">
                                                                     </div>
                                                                     <div class="form-group">
                                                                         <label for="pwd">Material: </label>
                                                                         <span style="color:red; font-size: 16px">*</span>
                                                                        <g:select name="materialId"  optionValue="name" from="${invMaterialList}" style="width: 100% !important;"  class="select2" required="true"  value="${budgetDetails?.invmaterial.id}"  noSelection="['null':'Select Material']" optionKey="id"/><br><!--${budgetDetails?.invmaterial?.name} -->
                                                                      </div>
                                                                        <div class="form-group">
                                                                           <label for="pwd">Cost Per Unit:</label>
                                                                           <span style="color:red; font-size: 16px">*</span>
                                                                           <input type="text" class="form-control" name="costPerUnit" value="${budgetDetails?.approx_cost_per_unit}" required="true">
                                                                       </div>
                                                                       <div class="form-group">
                                                                         <label for="pwd">Quantity:</label>
                                                                         <span style="color:red; font-size: 16px">*</span>
                                                                         <input type="text" class="form-control" name="quantity" value="${budgetDetails?.quantity_required}" required="true" optionValue="quantity">
                                                                     </div>
                                                                   <div class="form-group">
                                                                         <label for="pwd">Remarks:</label>
                                                                         <span style="color:red; font-size: 16px">*</span>
                                                                         <input type="text" class="form-control" name="remarks" value="${budgetDetails?.remark}" required="true">
                                                                     </div>
                                                                     <div class="form-group">
                                                                        <g:if test="${budgetDetails?.isactive}">
                                                                              <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                        </g:if>
                                                                        <g:else>
                                                                              <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                        </g:else>
                                                                  </div>
                                                               <div class="modal-footer">
                                                                 <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                               </div>
                                                              </g:form>
                                                             </div>
                                                           </div>
                                                         </div>
                                                        <!--
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                              <g:if test="${budgetDetails?.isactive}">
                                                                   <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                               </g:if>
                                                               <g:else>
                                                                   <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                               </g:else>

                                                        </td>
                                                        -->
                                                    </tr>
                                                </g:each>
                                                </tbody>
                                               </table>



      </div>
    </div>
  </div>
  </div>
  </div>
</body>
</html>



