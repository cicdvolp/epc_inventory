<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple">

                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Budget Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Budget Type</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Activity</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Initiated By</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Budget Level</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Department</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Budget Details

                                                    </th>
                                                    <!--
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    -->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invBudgetList}" var="budget" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.updation_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.invbudgettype?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.activity_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.enterby?.person?.grno}:${budget?.enterby?.person?.firstName} ${budget?.enterby?.person?.lastName}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.invbudgetlevel?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.department?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.academicyear?.ay}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <center>
                                                               <g:link controller="InvBudget" action="viewBudgetDetails" params="[invBudgetId : budget?.id]" target="blank"><i style="cursor: pointer;" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                         </center>
                                                        </td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



