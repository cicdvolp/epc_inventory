<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple full-scroll-table">
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Action Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Level No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval Status</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Remarks</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Budget Details</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approve Budget</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View History</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invBudgetEscalationList}" var="budget" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.updation_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.invfinanceapprovingauthoritylevel?.level_no}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.invapprovalstatus?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${budget?.remark}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="viewBudgetDetails">
                                                                  <input type="hidden" value="${budget?.invbudget?.id}" name="invBudgetId">
                                                                        <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                            </g:form>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#updateBudgetStatus${i}"></i>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="viewApprovalHistory">
                                                                  <input type="hidden" value="${budget?.invbudget?.id}" name="invBudgetId">
                                                                        <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                            </g:form>
                                                        </td>
                                                        <!--
                                                        <g:if test="${budget?.invfinanceapprovingauthoritylevel?.islast} == 'false'">
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="inviteBudgetQuotations">
                                                              <input type="hidden" value="${budget?.invbudget?.id}" name="invBudgetId">
                                                                    <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                        </g:form>
                                                        </td>
                                                        -->
                                                        </g:if>
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="updateBudgetStatus${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title">Update Approval Status</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="updateBudgetStatus">
                                                                <div class="modal-body">
                                                                      <div class="form-group">
                                                                          <input type="hidden" value="${budget?.invbudget?.id}" name="invBudgetId">
                                                                          <input type="hidden" value="${budget?.id}" name="budgetEscalationId">
                                                                       </div>
                                                                      <div>
                                                                        <label for="pwd">Approval Status:</label>
                                                                            <g:select name="approvalStatusId"  optionValue="name" from="${invApprovalStatusList}" style="width: 100% !important;"  class="select2" required="true"  value="${budget?.id}" optionKey="id"/><br>
                                                                      </div>
                                                                      <div>
                                                                      <label for="pwd">Remarks:</label>
                                                                      <input type="text" value="${budget?.remark}" class="form-control" name="remark" required="true">
                                                                      </div>
                                                                 </div>
                                                                <div class="modal-footer">
                                                                  <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



