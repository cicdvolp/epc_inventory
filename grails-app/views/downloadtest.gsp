<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
    <% /* <link rel="stylesheet"
     href="https://fonts.googleapis.com/css?family=Tangerine">
    <link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">*/ %>
    <!-- Bootstrap Core CSS -->
    <asset:stylesheet src="bootstrapT1.min.css" />
    <asset:stylesheet src="metisMenuT1.min.css" />
    <asset:stylesheet src="sb-admimT1.min.css" />
    <asset:stylesheet src="font-awesomeT1.min.css" />
    <!-- jQuery -->
    <asset:javascript src="jqueryT1.min.js" />
    <!-- Bootstrap Core JavaScript -->
    <asset:javascript src="bootstrapT1.min.js" />
    <!-- Metis Menu Plugin JavaScript -->
    <asset:javascript src="metisMenuT1.min.js" />
    <!-- Custom Theme JavaScript -->
    <asset:javascript src="sb-adminT1.js" />
    <asset:javascript src="download.js" />

    <!---------------------Table data-------------------------------------------------------------->
    <asset:stylesheet src="jquery-ui.css"/>
    <asset:stylesheet src="datatables.min.css"/>
    <asset:stylesheet src="buttons.dataTables.min.css"/>
    <asset:javascript src="jquery.min.js"/>
    <asset:javascript src="datatables.min.js"/>
    <asset:javascript src="dataTables.buttons.min.js"/>
    <asset:javascript src="jszip.min.js"/>
    <asset:javascript src="pdfmake.min.js"/>
    <asset:javascript src="vfs_fonts.js"/>
    <asset:javascript src="buttons.html5.min.js"/>
    <!--------------------------------------------------------------------------------------------->

    <script>
        //function downloadall() {
            //var downloadUrl = "https://example.org/image.png";

            //browser.runtime.getManifest().version;

            //chrome.downloads.download({
            //      url : downloadUrl,
            //      filename : 'my-image-again.png',
            //      conflictAction : 'uniquify'
            //}).then(function(id) {
            //    console.log(`Started downloading: ${id}`);
            //}, function(error) {
            //    console.log(`Download failed: ${error}`);
            //});

            //$.ajax({
            //    url: "https://vierp.s3.ap-south-1.amazonaws.com/cloud/individualidcardpdf/VIT/DESH/11910186.pdf",
            //    success: download.bind(true, "text/html", "dlAjaxCallback.html")
            //});
        //}

        function downloadall() {
            var ay = document.getElementById("ay").value;
            var sem = document.getElementById("sem").value;
            var progtype = document.getElementById("progtype").value;
            var prog = document.getElementById("prog").value;
            var year = document.getElementById("year").value;
            var xmlhttp = new XMLHttpRequest();
            xmlhttp.open("GET", "http://images.pexels.com/photos/67636/rose-blue-flower-rose-blooms-67636.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" , true);
            xmlhttp.responseType = 'blob';
            xmlhttp.onload=function(e){download(xmlhttp.response, "dlBinAjax.jpeg", "image/gif" ); }
            xmlhttp.send();
        }
    </script>
    <style>
        .custome-button {
             background-color:orange;
             border-color:orange;
             width:125px;
             color:white;
        }
        .custome-button:hover {
             background-color:#F7D698;
             border-color:#F7D698;
             width:125px;
             color:black;
        }
        .custom-form-box {
	    background-color:#F1F1F1;
            border:1px solid;
            border-radius:8px;
            padding:10px;
        }
    </style>
</head>
<body>
    <!-- Navigation -->
    <!----------------------------------------------------------------------------------------------------------------------------->
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <g:link controller="Login" action="erphome" style="background-color:#f0ad4e; color: white; text-decoration: underline;border: none;position: absolute; left:250px" class="btn btn-sm btn-default" ><span class="glyphicon glyphicon-home"> </span>&nbsp;Home</g:link>
            <button style="background-color:#f0ad4e; color: white; text-decoration: underline;border: none;position: absolute; right: 0" class="btn btn-sm btn-default" onclick="history.back(-1)"><span class="glyphicon glyphicon-triangle-left"> </span>Go Back</button>
            <div class="row">
                <div id="document">
                    <% /* =================================================== */ %>
                        <h3 class="page-header"><center><b>Download Test</b></center></h3>
                        <input type="button" class="btn" value="download" onclick="downloadall();"/>
                    <% /* =================================================== */ %>
                </div>
            </div>
        </div>
    </div>
</body>
</html>