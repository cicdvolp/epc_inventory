<!doctype html>
<html>
    <head>
        
       
            <meta name="layout" content="smart_main_inst"/>
      
        <meta name="author" content="rishabh"/>

        <script>
                 function showTotal(){
                     var comp = document.getElementsByClassName("fee");
                     var totalamt = 0.0;
                     for(var i=0;i<comp.length;i++){
                         totalamt += parseFloat(comp[i].innerHTML)
                     }
                     document.getElementById("total").innerHTML = totalamt;
                 }

              </script>
              <asset:javascript src="jquery.min.js"/>
              <style>
              input, textarea { border: none !important; box-shadow: none !important; outline: none !important; }
                 h6{
                 font-size:1.0em;
                 font-family:  "Times New Roman", Times, serif";"
                 }
                 input[type="text"]{
                    padding: 4px;

                    border:none;
                }
                table {
                    border-collapse: collapse;

                    font-size: 12px;
                    width: 60%;
                }

                table, th, td {
                    border: 1px solid black;
                }
                th {

                    height: 30px !important;
                    text-align: center !important;
                }
                th, td {
                    padding: 4px;
                    }
                @media print {

                @page{
                    margin-left: 2.5cm;
                    margin-right: 1cm
                    counter-increment: page;
                    content: counter(page);
                    }
                    body{
                     font-size: 15px !important;
                    }


                    a[href]{ display: none !important; }
                    button{
                        display: none !important;
                    }
                    input[type="submit"],input[type="button"]{display: none !important; }
                    hr {border:none !important;}
                    input[type="text"]{
                        border:none !important;
                    }
                    input, textarea { border: none !important; box-shadow: none !important; outline: none !important; }
             }
              </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                  <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->

                       <div class="row">
                               <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                   <div class="row clearfix">
                                        <div class="col-md-12 col-sm-6 col-12">
                                             <div class="card">
                                                 <div class="panel-body">

                                                          <div class="col-lg-12" id="print">
                                                             <table class="table">
                                                                 <tbody>
                                                                    <tr>
                                                                     <th>Receipt Number</th>
                                                                     <td>${invAdvanceReceipt?.receipt_no}</td>
                                                                   </tr>
                                                                   <tr>
                                                                     <th>Transaction Date</th>
                                                                     <td>${invAdvanceReceipt?.receipt_date}</td>
                                                                   </tr>
                                                                     <tr>
                                                                     <th>Transaction Number</th>
                                                                     <td>${invAdvanceReceipt?.transaction_number}</td>
                                                                   </tr>
                                                                   <tr>
                                                                     <th>Requested By</th>
                                                                     <td>${invAdvanceReceipt?.invadvancerequest?.requestby?.person?.fullname_as_per_previous_marksheet}</td>
                                                                   </tr>
                                                                   <tr>
                                                                     <th>Amount Paid</th>
                                                                     <td>${invAdvanceReceipt?.amount_transferred}</td>
                                                                   </tr>
                                                                   <tr>
                                                                    <th>Amount Returned</th>
                                                                    <td>${invAdvanceReceipt?.amount_returned}</td>
                                                                  </tr>
                                                                   <tr>
                                                                     <th>Purpose</th>
                                                                     <td>${invAdvanceReceipt?.invadvancerequest?.purpose}</td>
                                                                   </tr>
                                                                   <tr>
                                                                     <th>Payment Method</th>
                                                                     <td>${invAdvanceReceipt?.invpaymentmethod?.name}</td>
                                                                   </tr>

                                                                    <tr>
                                                                     <th>Bank Name</th>
                                                                     <td>${invAdvanceReceipt?.bank_name}</td>
                                                                   </tr>
                                                                   <tr>
                                                                     <th>IFSC Code</th>
                                                                     <td>${invAdvanceReceipt?.ifsc_code}</td>
                                                                   </tr>
                                                                   <tr>
                                                                     <th>Bank Account Number</th>
                                                                     <td>${invAdvanceReceipt?.account_number}</td>
                                                                   </tr>
                                                                    <tr>
                                                                     <th>Bank branch and address</th>
                                                                     <td>${invAdvanceReceipt?.bank_branch}</td>
                                                                   </tr>
                                                                 </tbody>
                                                                </table>
                                                         </div>
                                                        
                                                 </div>
                                             </div>
                                        </div>
                                   </div>
                               </div>
                       </div>



                         <script>
                                  window.onload = function(){
                                      ///alert("win");
                                      showTotal();
                                  }
                               </script>
                               <script>
                                  function printElem(divId) {
                                           //alert("hi")
                                           var content = document.getElementById(divId).innerHTML;
                                           var mywindow = document.body.innerHTML;

                                           document.body.innerHTML=content ;
                                           window.print();
                                           document.body.innerHTML=mywindow;
                                                   return true;
                                       }
                                      function reflect(ele){
                                         ele.setAttribute("value",ele.value)
                                      }

                               </script>


                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>
<!--
       