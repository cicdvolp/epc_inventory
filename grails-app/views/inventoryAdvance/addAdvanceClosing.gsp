<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
               <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Instructor Selection-->
                    <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Select Instructor</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                     <g:form action="addAdvanceClosing"> 
                                        <center>
                                         <div>
                                            <label>Instructor</label>
                                            <g:select name="advanceInstructor" optionKey="id" optionValue="${instructor?.person?.name}" from="${instructorList}" class="select2" required="true" value="${id}"  noSelection="['null':'Select Instructor']"/><br><br>
                                             <button type="submit" class="btn btn-primary" >Fetch</button>
                                         </div>
                                        </center>
                                    </g:form>
                                   
                                  </div>  
                        </div>
                  </div>
                </div>
                <!---->
                <!--Table with details-->
                     <div class="row">
                            <div class="col-md-12  mobile-table-responsive">

                                <div class="card card-topline-purple  full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Receipt List</header>
                                   </div>
                                    <div class="card-body " id="bar-parent">
                                        <div class="table-responsive">
                                            <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Request Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount Issued Rs</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Account Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IFSC Code</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Receipt No</th>
                                                  <th class="mdl-data-table__cell--non-numeric">Proof Submissions</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Close Advance</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${advanceReceiptList}" var="advanceReceipt" status="i">
                                                    <tr>
                                                        
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${advanceReceipt?.data?.invadvancerequest?.request_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${advanceReceipt?.data?.invadvancerequest?.sanctioned_amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceReceipt?.data?.invadvancerequest?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceReceipt?.data?.account_number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceReceipt?.data?.bank_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceReceipt?.data?.ifsc_code}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            ${advanceReceipt?.data?.receipt_no}
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <g:link controller="inventoryAdvance" action="viewProofs" id="${advanceReceipt?.advancerequest?.id}" style="text-decoration:underline;">View Proof</g:link>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            
                                                        </td>

                                                    </tr>
                                                   
                                                      
                                                </g:each>
                                                </tbody>
                                               </table>

                                         </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
            </div>
        </div>
                
</body>
</html>



