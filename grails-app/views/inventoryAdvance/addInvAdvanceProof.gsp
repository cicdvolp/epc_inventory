<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
              <!--message and error-->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

               <!--Add a proof-->
                     <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple ">
                                    <div class="card-head">
                                        <header class="erp-table-header">Add a Expenditure Proof</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                    <g:form action="saveInvAdvanceProof" enctype="multipart/form-data">
                                       <div class="row">
                                           <div class="col-sm-4">
                                               <label for="pwd">Advance Request:</label>
                                               <span style="color:red; font-size: 16px">*</span>
                                               <g:select name="advRequest" optionKey="id" optionValue="${{it.purpose}}" from="${invAdvnaceRequestList}" class="select2" required="true" noSelection="['null':'Select Request']"/><br>
                                           </div>
                                          <div class="col-sm-4">
                                            <div class="form-group">
                                                <label >Amount Spent Date:</label>
                                                <span style="color:red; font-size: 16px">*</span><br>
                                                 <g:datePicker class="form-control" required="true "name="expenditureDate" precision="day" value="${new Date()}"/>
                                            </div>
                                         </div>
                                         <div class="col-sm-4">
                                             <div class="form-group row mx-auto">
                                                  <label class="my-2">Purpose:</label>
                                                  <span style="color:red; font-size: 16px">*</span>
                                                 <input type="text" class="form-control my-2" name="expenditurePurpose" required="true">
                                             </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-sm-4">
                                              <div class="form-group row mx-auto">
                                                  <label class="my-2">Amount Spent:</label>
                                                  <span style="color:red; font-size: 16px">*</span>
                                                  <input class="form-control" type="number" min="0" name="expenditureAmount">
                                              </div>
                                           </div>
                                          <div class="col-sm-4">
                                              <div class="form-group row mx-auto">
                                                   <label class="my-2">Amount Returned:</label>
                                                   <span style="color:red; font-size: 16px">*</span>
                                                  <input type="number" min="0" class="form-control my-2" name="amountReturned" required="true">
                                              </div>
                                            </div>
                                           <div class="col-sm-4">
                                              <div class="form-group row mx-auto">
                                              <label class="my-2">Proof:</label>
                                              <span style="color:red; font-size: 16px">*</span>
                                                 <input accept="image/jpeg,image/png,application/pdf" type="file" class="form-control-file my-2" name="expenditureProofFile"/>
                                              </div>
                                           </div>
                                       </div>
                                       <!--
                                       <div class="row">
                                            <div class="col-sm-4">
                                               <label for="pwd">Payment Return Mode: </label>
                                               <g:select name="returnMethod" optionKey="id" optionValue="${{it.name}}" from="${paymentModeList}" style="width: 100% !important;"  class="select2" noSelection="['null':'Select Return Payment Mode']"/><br>
                                            </div>
                                            <div class="col-sm-4">
                                               <label for="pwd">Payment Return Details: </label>
                                               <input type="text" class="form-control my-2" name="returnDetails" value="N/A">
                                            </div>
                                       </div>
                                       -->
                                       <div class="row">
                                        <button id="btnSubmit" type="submit" class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                       </div>
                                    </g:form>
                                    </div>
                        </div>
                  </div>
                </div>
               <!--////-->
                 <!--Table,Edit,Delete -->
                 <!--
                <div class="row">
                    <div class="col-md-12  mobile-table-responsive">

                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Proofs List</header>
                                       
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount Spent Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount Spent in Rs</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Proof File</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IsActive</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    <!--<th class="mdl-data-table__cell--non-numeric">Delete</th>-->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invAdvanceSettlementList}" var="advanceSettlement" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${advanceSettlement?.amount_spent_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${advanceSettlement?.amount_spent}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceSettlement?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceSettlement?.file_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryAdvance" action="activateInvAdvanceProof" params="[advanceSettlementId : advanceSettlement?.id,advanceReceiptId: advanceSettlement?.invadvancerequestreceipt?.id]">
                                                               <g:if test="${advanceSettlement?.isactive}">
                                                                   <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                               </g:if>
                                                               <g:else>
                                                                   <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                               </g:else>
                                                           </g:link>
                                                        </td>
                                                         
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <i class="fa fa-edit fa-2x erp-edit-icon-color mx-auto" data-toggle="modal" data-target="#editadvanceSettlement${i}"></i>
                                                        </td>
                                                        
                                                      
                                                        
                                                      <!----  <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="deleteInvAdvanceRequest">
                                                                  <input type="hidden" value="${advanceRequest?.id}" name="advanceRequestId">
                                                                        <button  data-toggle="modal" data-target="#${i}myModal" class="fabutton" type="submit" onclick="return confirm('Do you want to Delete ?')" ><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></button>
                                                            </g:form>
                                                        </td>-->
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="editadvanceSettlement${i}" role="dialog">
                                                        <div class="modal-dialog modal-lg"">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title ml-3"><strong>Edit Advance Settlement</strong></h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                                 <g:form action="editInvAdvanceProof" enctype="multipart/form-data">
                                                                    <g:hiddenField name="advanceRequestId" value="${inAdvanceRequest?.id}"/>
                                                                  <div class="modal-body mx-3">
                                                                  <div class="form-row">
                                                                     <div class="form-group row mx-4">
                                                                        <label>Amount Spent Date:</label>
                                                                        <g:datePicker class="form-control"  required="true "name="expenditureDate" precision="day" value="${advanceSettlement?.amount_spent_date}" noSelection="['':'-Select a date-']"/>
                                                                    </div>
                                                                  </div>
                                                                    <div class="form-row">
                                                                      <div class="form-group row mx-4">
                                                                        <label>Amount Spent in Rs:</label>
                                                                        <input class="form-control"  type="text" name="expenditureAmount" value="${advanceSettlement?.amount_spent}">
                                                                      </div>
                                                                      <div class="form-group row mx-4">
                                                                         <label>Purpose:</label>
                                                                         <input type="text" class="form-control" name="expenditurePurpose" value="${advanceSettlement?.purpose}" required="true">
                                                                       </div>
                                                                    </div>
                                                                   <div class="form-row">
                                                                     <div class="form-group row mx-4">
                                                                        <label class="my-2">Proof File:</label>&nbsp;&nbsp;
                                                                       <input style="width:80% ! important;" accept="image/jpeg,image/png,application/pdf" type="file" class="form-control-file my-2" name="expenditureProofFile" value="${advanceSettlement?.file_name}"/>
                                                                     </div>
                                                                    </div>   
                                                                </div>
                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="advanceSettlementId" id="advanceSettlementId" value="${advanceSettlement?.id}">
                                                                        <input type="hidden" name="advanceReceiptId" id="advanceReceiptId" value="${advanceSettlement?.invadvancerequestreceipt?.id}">
                                                                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto mx-auto">Update</button>
                                                                    </div>
                                                                 
                                                                </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </g:each>
                                                </tbody>
                                               </table>

                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                                <!-- Modal New NOT IN USE -->
                                 
                          <!--End of add modal-->       
                      </div>
                 </div>
                 -->
            </div>
        </div>
    </div>
  </div>
</div>
               
  
  
</body>
</html>









