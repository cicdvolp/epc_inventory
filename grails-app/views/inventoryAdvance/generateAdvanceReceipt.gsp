<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                  <g:render template="/layouts/smart_template_inst/breadcrumb" />
              <!--message and error-->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
              <!--////-->
              <!--Advance Request Details-->
                 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Requesting Person's Details</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                        <div class="row mx-auto">
                                            <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Request Date:</label>&nbsp;
                                                <g:formatDate format="yyyy-MM-dd" date="${invAdvanceRequest?.request_date}"/>
                                            </div>
                                             <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Requested By:</label>&nbsp;
                                                ${invAdvanceRequest?.requestby?.employee_code}: ${invAdvanceRequest?.requestby?.person?.fullname_as_per_previous_marksheet}
                                            </div>
                                        </div>
                                        <div class="row mx-auto">
                                            <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Amount in Rs:</label>&nbsp;
                                                <i class="fa fa-inr" aria-hidden="true"></i> ${invAdvanceRequest?.sanctioned_amount}
                                            </div>
                                             <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Purpose:</label>&nbsp;
                                                ${invAdvanceRequest?.purpose}
                                            </div>
                                        </div>
                                    </div>
                        </div>
                  </div>
                </div>
               <!--///-->
               <!--Add a proof-->
                     <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Add a receipt</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                        <g:form action="saveAdvanceReceipt">
                                           <g:hiddenField name="advanceRequestId" value="${invAdvanceRequest?.id}"/>
                                       <div class="row">
                                          <div class="col">
                                            <div class="form-group row mx-auto">
                                                <label class="my-2">Amount Paid:</label>
                                                <span style="color:red; font-size: 16px">*</span>
                                                <input class="form-control" style="width: 50% !important;" type="text" name="receiptamountTransferred">
                                            </div>
                                         </div>
                                          <div class="col">
                                            <div class="form-group">
                                                <label class="my-2">Transaction Date</label>
                                                <span style="color:red; font-size: 16px">*</span>
                                                 <g:datePicker required="true" class="form-control" required="true "name="receiptTransactionDate" precision="day" value="${new Date()}" noSelection="['':'-Select a date-']"/>
                                            </div>
                                         </div>
                                       </div>  
                                       <div class="row">
                                          <div class="col">
                                            <div class="form-group row mx-auto">
                                                 <label class="my-2">Payment Method:</label>
                                                 <span style="color:red; font-size: 16px">*</span>
                                                <g:select name="receiptPaymentMethod" optionKey="id" optionValue="name" from="${invAdvancePaymentMethodList}" style="width: 50% !important;"  class="select2 my-2" required="true" value="${id}"  noSelection="['null':'Select Payment Method']"/><br>
                                            </div>
                                          </div> 
                                         
                                           <div class="col">
                                              <div class="form-group row mx-auto">
                                                 <label class="my-2">Transaction Number:</label>
                                                 <span style="color:red; font-size: 16px">*</span>
                                                 <input style="width:50% ! important;"  type="text" class="form-control my-2" name="receiptTransactionNo"/>
                                              </div>
                                           </div>
                                       </div>
                                       <div class="row">
                                          <div class="col">
                                            <div class="form-group row mx-auto">
                                                <label>Bank Name:</label>
                                                <span style="color:red; font-size: 16px">*</span>
                                             <input style="width:40% !important;" type="text" class="form-control" name="receiptBankName">
                                            </div>
                                          </div> 
                                           <div class="col">
                                              <div class="form-group row mx-auto">
                                                  <label>IFSC Code:</label>
                                                  <span style="color:red; font-size: 16px">*</span>
                                                  <input style="width:40% !important;" type="text" class="form-control" name="receiptIFSC">
                                              </div>
                                           </div>
                                        </div>
                                        <div class="row">
                                            <div class="col">
                                              <div class="form-group row mx-auto">
                                                  <label>Bank Account Number:</label>
                                                  <span style="color:red; font-size: 16px">*</span>
                                                  <input style="width:40% !important;" type="text" class="form-control" name="receiptBankAccountNo">
                                              </div>
                                            </div>
                                            <div class="col">
                                              <div class="form-group row mx-auto">
                                                   <label>Bank Branch And Address:</label>
                                                   <span style="color:red; font-size: 16px">*</span>
                                                    <input style="width:50% !important;" type="text" class="form-control" name="receiptBankAddress">
                                              </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="col">
                                               <label>Return Amount: </label>
                                               <span style="color:red; font-size: 16px">*</span>
                                               <input type="number" min="0" class="form-control my-2" name="returnAmount" value="N/A">
                                            </div>
                                            <div class="col">
                                               <label>Payment Return Mode: </label>
                                               <span style="color:red; font-size: 16px">*</span>
                                               <g:select name="returnMethod" optionKey="id" optionValue="${{it.name}}" from="${invAdvancePaymentMethodList}"  class="select2" noSelection="['null':'Select Return Payment Mode']"/><br>
                                            </div>
                                            <div class="col">
                                               <label>Payment Return Details: </label>
                                               <span style="color:red; font-size: 16px">*</span>
                                               <input type="text" style="width:100% !important;" class="form-control my-2" name="returnDetails" value="N/A">
                                            </div>
                                       </div>
                                       <div class="row">
                                        <button id="btnSubmit" type="submit" class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                       </div>
                                    </g:form>
                                    </div>
                        </div>
                  </div>
                </div>
               <!--////-->
                 <!--Table,Edit,Delete 
                        
                 
                 -->


                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                                <!-- Modal New NOT IN USE -->
                                 
                          <!--End of add modal-->       
                  
  </div>
</div>
               
  
  
</body>
</html>









