<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-purple">
            <div class="card-head">
                <header class="erp-table-header">
                    Category : ${invFinanceApprovingCategory?.name}, Approving Authority Post : ${invInstructorFinanceApprovingAuthority?.invfinanceapprovingauthority?.name}
                </header>
            </div>

                <div class="table-responsive">
                    <table class="mdl-data-table ml-table-bordered">
                        <thead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                <th class="mdl-data-table__cell--non-numeric">Advance Request </th>
                                <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${invAdvanceRequestEscalation_List}" var="advanceReq" status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <span>Account Number : </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invadvancerequest?.account_number}</span>
                                        <br>
                                        <span>Purpose : </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invadvancerequest?.purpose}</span>
                                        <br>
                                        <span>Requested Date : </span><span class="erp-table-header" style="font-weight:900;"><g:formatDate format="dd/MM/yyyy" date="${advanceReq?.invadvancerequest?.request_date}"/></span>
                                        <br>
                                        <span>Requested Amount: </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invadvancerequest?.amount}</span>
                                        <br>
                                        <span>Requested By : </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invadvancerequest?.requestby}</span>
                                        <br>
                                        <span>Payment Method : </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invadvancerequest?.invpaymentmethod?.name}</span>
                                        <br>
                                        <span>Bank Name : </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invadvancerequest?.bank_name}</span>
                                        <br>
                                        <span>Bank Branch : </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invadvancerequest?.bank_branch}</span>
                                        <br>
                                        <span>Bank Branch : </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invadvancerequest?.bank_branch}</span>
                                        <br>
                                        <span>Approval Status : </span><span class="erp-table-header" style="font-weight:900;">${advanceReq?.invapprovalstatus?.name}</span>
                                        <br>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:link target="blank" controller="inventoryAdvance" action="getAdvanceRequestEscalationforApprove" params="[advanceReqId:advanceReq?.id]" class="btn btn-primary">Proceed</g:link>
                                    </td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
            <!--</div>-->
        </div>
    </div>
</div>