<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
              <!--message and error-->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
              <!--////-->
              <!--Add form-->
                 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                   <div class="card-body inline-block" id="bar-parent">
                                        <g:form action="saveInvAdvanceRequest">
                                       <div class="row">
                                       <div class="col-sm-4">
                                            <label label>Purpose:</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                            <input type="text" class="form-control" name="advanceRequestPurpose" required="true">
                                          </div>
                                          <div class="col-sm-4">
                                          <label>Amount:</label>
                                          <span style="color:red; font-size: 16px">*</span>
                                          <input type="text" class="form-control cols-sm-4" name="advanceRequestAmount" required="true">
                                          </div>
                                          <div class="col-sm-4">
                                          <label>Payment Method:</label>
                                          <span style="color:red; font-size: 16px">*</span>
                                          <g:select name="advanceRequestPaymentMethod" optionKey="id" optionValue="name" from="${invPaymentMethodList}"  class="select2 form-control" required="true" value="${id}"  noSelection="['null':'Select Payment Method']"/><br>
                                          </div>
                                       </div>
                                       <div class="row">
                                        <div class="col-sm-4">
                                              <label>Account Number</label>
                                              <span style="color:red; font-size: 16px">*</span>
                                              <input type="text" class="form-control" name="advanceRequestAccountNumber">
                                           </div>
                                           <div class="col-sm-4">
                                            <label>Bank Name:</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                             <input type="text" class="form-control" name="advanceRequestBankName">
                                           </div>
                                            <div class="col-sm-4">
                                            <label>Bank Branch :</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                            <input class="form-control" name="advanceRequestBankAddress">
                                          </div>
                                            <div class="col-sm-4">
                                            <label>IFSC Code:</label>
                                            <span style="color:red; font-size: 16px">*</span>
                                            <input type="text" class="form-control" name="advanceRequestIFSC">
                                          </div>
                                       </div>
                                       <div class="row">
                                       <div class="col-sm-12">
                                        <center>
                                            <br>
                                        <button id="btnSubmit" type="submit" class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                        </cemter>
                                       </div>
                                       </div>
                                    </g:form>
                                    </div>
                        </div>
                  </div>
                </div>
               <!--///-->
                 <!--Table,Edit,Delete -->
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Request List</header>
                                       
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Request Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Request Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Payment Method</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Account Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Branch</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IFSC Code</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is Approved?</td>
                                                    <th class="mdl-data-table__cell--non-numeric">Approved Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IsActive</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    <!--<th class="mdl-data-table__cell--non-numeric">Delete</th>-->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invAdvanceRequestList}" var="advanceRequest" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${advanceRequest?.request_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${advanceRequest?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.invpaymentmethod?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.account_number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.bank_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.bank_branch}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.ifsc_code}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                              <center> <g:if test="${advanceRequest?.isapproved}">
                                                              <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                          </g:if>
                                                          <g:else>
                                                              <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                          </g:else></center>
                                                        </td>
                                                        <td>
                                                            ${advanceRequest?.sanctioned_amount}
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric"> <center>
                                                         <g:link controller="inventoryAdvance" action="activateInvAdvanceRequest" params="[advanceRequestId : advanceRequest?.id]">
                                                              <g:if test="${advanceRequest?.isactive}">
                                                                   <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                               </g:if>
                                                               <g:else>
                                                                   <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                               </g:else>
                                                           </g:link> </center>
                                                         </td>
                                                         <td>
                                                         <center>
                                                             <g:if test="${advanceRequest?.isapproved}">
                                                                 <label for="pwd">Request Already Approved,Cannot Edit</label>
                                                              </g:if>
                                                              <g:else>
                                                                  <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editadvanceRequest${i}"></i>
                                                               </g:else>
                                                           </center>
                                                         </td>
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="editadvanceRequest${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title ml-3"><strong>Edit Advance Request</strong></h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                                 <g:form action="editInvAdvanceRequest">
                                                                    <div class="modal-body mx-3">
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Request Amount:</label>
                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                <input type="number" class="form-control " name="advanceRequestAmountEdit" value="${advanceRequest.amount} "required="true">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Payment Method :</label>
                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                <g:select name="advanceRequestPaymentMethod" optionKey="id" optionValue="name" from="${invPaymentMethodList}"  class=" form-control select2" style="width:100% !important;" required="true" value="${advanceRequest?.id}" noSelection="['null':'Select Payment Method']"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Account Number :</label>
                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                <input type="number" class="form-control " name="advanceRequestAccountNumber" value="${advanceRequest.account_number}" required="true">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>IFSC Code :</label>
                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                <input type="text" class="form-control " name="advanceRequestIFSC" value="${advanceRequest.ifsc_code}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Bank Name :</label>
                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                <input type="text" class="form-control " name="advanceRequestBankName" value="${advanceRequest.bank_name}">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Bank Branch :</label>
                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                <input type="text" class="form-control " name="advanceRequestBankAddress" value="${advanceRequest.bank_branch} "required="true">
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-sm-12">
                                                                                <label>Purpose:</label>
                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                <input type="text" class="form-control " name="advanceRequestPurpose" required="true" value="${advanceRequest.purpose}">
                                                                            </div>
                                                                        </div>
                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="invAdvanceRequestID" id="invAdvanceRequestID" value="${advanceRequest?.id}">
                                                                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto mx-auto">Update</button>
                                                                    </div>
                                                                 
                                                                </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </g:each>
                                                </tbody>
                                               </table>

                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                                <!-- Modal New NOT IN USE -->
                                  
                          <!--End of add modal-->       
                      </div>
                 </div>
            </div>
        </div>
    </div>
  </div>
</div>
               
  
  
</body>
</html>









