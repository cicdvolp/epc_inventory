<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12  mobile-table-responsive">

                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Request List</header>
                                    <!--    <div class="tools">
                                            <g:link  data-toggle="modal" data-target="#addNewInvBudget"><span aria-hidden="true" class="icon-plus" title="Add New Budget" style="float:right; font-size:20px;"></span></g:link>
                                        </div>
                                        -->
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Request Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Requested By</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Requested Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Level</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Current Approval Status</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Sanctioned Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval History</th>
                                                     <th class="mdl-data-table__cell--non-numeric">Update Approval</th>
                                                    <!--
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    -->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invAdvanceRequestEscalationList}" var="advanceRequestEscalation" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${advanceRequestEscalation?.invadvancerequest.request_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.actionby?.employee_code}: ${advanceRequestEscalation?.actionby?.person.firstName} ${advanceRequestEscalation?.actionby?.person.lastName}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${advanceRequestEscalation?.invadvancerequest?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.invfinanceapprovingauthoritylevel?.level_no}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.invadvancerequest?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.invapprovalstatus?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${advanceRequestEscalation?.sanctioned_amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.remark}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:link controller="inventoryAdvance" action="advanceRequestApprovalHistory" id="${advanceRequestEscalation?.invadvancerequest?.id}">Approval History</g:link></td>
                                                       
                                                        <!--<td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="viewBudgetDetails">
                                                                  <input type="hidden" value="${budget?.id}" name="invBudgetId">
                                                                        <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                            </g:form>
                                                        </td>-->
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <i class="fa fa-edit fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#updateAdvanceRequestApproval${i}"></i>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="updateAdvanceRequestApproval${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title">Update Approval</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="updateAdvanceRequestApproval">
                                                                <div class="modal-body">
                                                                      <div class="form-group">
                                                                          <input type="hidden" value="${advanceRequestEscalation?.id}" name="advanceRequestEscalationId">
                                                                          <input type="hidden" value="${advanceRequestEscalation?.invadvancerequest?.id}" name="invadvancerequestId">
                                                                       </div>
                                                                      <div>
                                                                        <label>Approval Status:</label>
                                                                            <g:select name="approvalStatus" optionKey="id" optionValue="name" from="${invApprovalStatusList}" style="width: 100% !important;"  class="select2" required="required"  value="${id}"  noSelection="['null':'Select Approval Status']"/><br>
                                                                      </div>
                                                                      <div>
                                                                        <label>Remark:</label>
                                                                            <input type="text" class="form-control" name="advanceRequestRemark" value="${advanceRequestEscalation.remark}"> 
                                                                      </div>
                                                                      <div>
                                                                        <label>Sanctioned Amount:</label>
                                                                            <input type="text" class="form-control" name="advanceRequestSanctionedAmount" value="${advanceRequestEscalation.sanctioned_amount}"> 
                                                                      </div>
                                                                 </div>
                                                                <div class="modal-footer">
                                                                  <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto" >Update</button>
                                                                </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
           </div>
            </div>
        </div>
               
</body>
</html>



