<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                 <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Card-->
                <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Select Instructor</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                    
                                     <g:form action="addInvAdvanceReceipt"> 
                                        <center>
                                         <div>
                                            <label>Instructor:</label>
                                            <g:select name="advanceInstructor" optionKey="id" optionValue="${instructor?.person?.name}" from="${instructorList}" class="select2" required="true" value="${id}"  noSelection="['null':'Select Instructor']"/><br><br>
                                             <button type="submit" class="btn btn-primary" >Fetch</button>
                                         </div>
                                        </center>
                                    </g:form>
                                   
                                  </div>  
                        </div>
                  </div>
                </div>
                <!---->
                <!--Table with details-->
     
                     <div class="row">
                            <div class="col-md-12">

                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Receipt List</header>
                                   </div>
                                    <div class="card-body " id="bar-parent">
                                        <div class="table-responsive">
                                            <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Request Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval Status</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Generate Receipt</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Receipt</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Advance Settlement</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Sanctioned Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Account Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IFSC Code</th>


                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invAdvanceRequestList}" var="advanceRequest" status="i">
                                                    <tr>
                                                        
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${advanceRequest?.data?.request_date}"/></td>

                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            ${advanceRequest?.data?.invapprovalstatus?.name}
                                                        <g:link controller="inventoryAdvance" action="advanceRequestApprovalHistory" id="${advanceRequest?.data?.id}"><i class="fa fa-external-link fa-lg erp-edit-icon-color" aria-hidden="true"></i></g:link>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <g:if test="${advanceRequest?.data?.isapproved==true}">
                                                            <g:if test="${advanceRequest?.reciept == null}">
                                                                <g:link target="new" controller="inventoryAdvance" action="generateAdvanceReceipt" id="${advanceRequest?.data?.id}" style="text-decoration:underline;">Generate Receipt</g:link>
                                                            </g:if>
                                                            <g:else>
                                                                Reciept Generated.
                                                            </g:else>
                                                        </g:if>
                                                        <g:else>
                                                            Advance Not Yet Approved
                                                        </g:else>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <g:if test="${advanceRequest?.reciept != null}">
                                                            <g:link target="new" controller="inventoryAdvance" action="viewAdvanceReceipt" id="${advanceRequest?.data?.id}" style="text-decoration:underline;">View Receipt</g:link>
                                                        </g:if>
                                                        <g:else>
                                                            Reciept Not Yet Generated
                                                        </g:else>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link target="new" controller="inventoryAdvance" action="viewProofUsingAdvanceRequest" id="${advanceRequest?.data?.id}" style="text-decoration:underline;">View Proof</g:link>
                                                        </td>

                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${advanceRequest?.data?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${advanceRequest?.data?.sanctioned_amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.data?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.data?.account_number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.data?.bank_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequest?.data?.ifsc_code}</td>

                                                    </tr>
                                                   
                                                      
                                                </g:each>
                                                </tbody>
                                               </table>

                                         </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
            </div>
        </div>
               
</body>
</html>



