<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                 <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Requesting Instructor Details-->
                <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Requesting Person's Details</header>
                                   </div>
                                   <div class="card-body " id="bar-parent">
                                        <div class="row mx-auto">
                                            <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Request Date:</label>&nbsp;
                                                <g:formatDate format="yyyy-MM-dd" date="${inAdvanceRequest?.request_date}"/>
                                            </div>
                                             <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Requested By:</label>&nbsp;
                                                ${inAdvanceRequest?.requestby?.employee_code}: ${inAdvanceRequest?.requestby?.person?.fullname_as_per_previous_marksheet}
                                            </div>
                                        </div>
                                        <div class="row mx-auto">
                                            <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Amount in Rs:</label>&nbsp;
                                                <i class="fa fa-inr" aria-hidden="true"></i> ${inAdvanceRequest?.amount}
                                            </div>
                                             <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Purpose:</label>&nbsp;
                                                ${inAdvanceRequest?.purpose}
                                            </div>
                                        </div>
                                   </div>
                      </div>
                  </div>
                </div>
                <!--/////-->
                <!--Table with details-->
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Request Approval History</header>
                                   </div>
                                    <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Level No</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approving Authority</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approved Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approving Category</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is Last Level</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Action Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Status</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Action By</th>
                                                     
                                                    <!--
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    -->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invAdvanceRequestEscalationList}" var="advanceRequestEscalation" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.invfinanceapprovingauthoritylevel?.level_no}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.sanctioned_amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.invfinanceapprovingauthoritylevel?.invfinanceapprovingcategory?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <g:if test="${advanceRequestEscalation?.invfinanceapprovingauthoritylevel?.islast}">
                                                        <i class="fa fa-check" aria-hidden="true"></i>
                                                        </g:if>
                                                        <g:else>
                                                          <i class="fa fa-times" aria-hidden="true"></i>
                                                        </g:else>
                                                        
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-mm-yyyy" date="${advanceRequestEscalation?.action_date}"/></td>
                                                      
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.invapprovalstatus?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.remark}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceRequestEscalation?.actionby?.person?.fullname_as_per_previous_marksheet}</td>
                                                       
                                                        <!--<td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="viewBudgetDetails">
                                                                  <input type="hidden" value="${budget?.id}" name="invBudgetId">
                                                                        <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                            </g:form>
                                                        </td>-->
                                                      
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
           </div>
            </div>
        </div>
               
</body>
</html>



