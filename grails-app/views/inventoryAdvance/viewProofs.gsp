<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
              <!--message and error-->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
              <!--////-->
              <!--Advance Request Details-->
                 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Requesting Person's Details</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                        <div class="row mx-auto">
                                            <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Request Date:</label>&nbsp;
                                                <g:formatDate format="yyyy-MM-dd" date="${invAdvanceRequest?.request_date}"/>
                                            </div>
                                             <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Requested By:</label>&nbsp;
                                                ${invAdvanceRequest?.requestby?.employee_code}: ${invAdvanceRequest?.requestby?.person?.fullname_as_per_previous_marksheet}
                                            </div>
                                        </div>
                                        <div class="row mx-auto">
                                            <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Requested Amount:</label>&nbsp;
                                                <i class="fa fa-inr" aria-hidden="true"></i> ${invAdvanceRequest?.amount}
                                            </div>
                                             <div class="col-sm-5 mx-auto px-auto">
                                                <label style="font-weight:bold;text-decoration:underline;">Purpose:</label>&nbsp;
                                                ${invAdvanceRequest?.purpose}
                                            </div>
                                        </div>
                                    </div>
                        </div>
                  </div>
                </div>
               <!--///-->
              
                 <!--Table -->
                <div class="row">
                    <div class="col-md-12  mobile-table-responsive">

                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Proofs List</header>
                                       
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount Spent Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount Spent in Rs</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Proof File</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invAdvanceSettlementList}" var="advanceSettlement" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${advanceSettlement?.data?.amount_spent_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${advanceSettlement?.data?.amount_spent}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${advanceSettlement?.data?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <a href="${data?.url}" target="_blank">${advanceSettlement?.data?.file_name}</a>
                                                        </td>
                                                    </tr>

                                                </g:each>
                                                </tbody>
                                               </table>
                      </div>
                 </div>
            </div>
        </div>
    </div>
  </div>
</div>
               
  
  
</body>
</html>









