<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!---->
                <!--
                <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Select Instructor</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                     <g:form action="addInvAdvanceSettlement"> 
                                        <center>
                                         <div>
                                            <label>Instructor:</label>
                                            <g:select name="advanceInstructor" optionKey="id" optionValue="${instructor?.person?.name}" from="${instructorList}" class="select2" required="true" value="${id}"  noSelection="['null':'Select Instructor']"/><br><br>
                                             <button type="submit" class="btn btn-primary" >Fetch</button>
                                         </div>
                                        </center>
                                    </g:form>
                                  </div>  
                        </div>
                  </div>
                </div>
                -->
                <!---->
                <!--Table with details-->
                     <div class="row">
                            <div class="col-md-12  mobile-table-responsive">

                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Advance Settlement :- ${instName}</header>
                                   </div>
                                    <div class="card-body " id="bar-parent">
                                        <div class="table-responsive">
                                            <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Request Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Account Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Bank Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IFSC Code</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval Status</th>
                                                    <!--
                                                    <th class="mdl-data-table__cell--non-numeric">Is Receipt Generated</th>
                                                    -->
                                                    <th class="mdl-data-table__cell--non-numeric">Proof Submission</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Receipt</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${InvAdvanceArray}" var="requestAndReceipt" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${requestAndReceipt?.data?.request_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i> ${requestAndReceipt?.data?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${requestAndReceipt?.data?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${requestAndReceipt?.data?.account_number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${requestAndReceipt?.data?.bank_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${requestAndReceipt?.data?.ifsc_code}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        ${requestAndReceipt?.data?.invapprovalstatus?.name}
                                                        <g:link controller="inventoryAdvance" action="advanceRequestApprovalHistory" id="${requestAndReceipt?.data?.id}"><i class="fa fa-external-link fa-lg erp-edit-icon-color" aria-hidden="true"></i></g:link>
                                                        </td>
                                                        <!--
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:if test="${requestAndReceipt?.reciept == null}">
                                                                       No
                                                            </g:if>
                                                            <g:else>
                                                                    Yes
                                                            </g:else>
                                                        </td>
                                                        -->
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:link controller="inventoryAdvance" action="addInvAdvanceProof"  id="${requestAndReceipt?.data?.id}" style="text-decoration:underline;">Submit Proof</g:link>
                                                         </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <g:if test="${requestAndReceipt?.reciept != null}">
                                                            <g:link controller="inventoryAdvance" action="viewAdvanceReceipt" id="${requestAndReceipt?.data?.id}" style="text-decoration:underline;">View Receipt</g:link>
                                                        </g:if>
                                                        <g:else>
                                                            Reciept Not Yet Generated
                                                        </g:else>
                                                        </td>
                                                    </tr>
                                                   
                                                      
                                                </g:each>
                                                </tbody>
                                               </table>

                                         </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
            </div>
        </div>
                
</body>
</html>



