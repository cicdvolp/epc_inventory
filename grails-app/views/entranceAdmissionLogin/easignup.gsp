<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Responsive Admin Template" />
        <meta name="author" content="RedstarHospital" />
        <title>EduPlusCampus</title>
        <!-- google font -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- icons -->
        <asset:stylesheet src="smart/fonts/font-awesome/css/font-awesome.min.css" />
        <asset:stylesheet src="smart/plugins/iconic/css/material-design-iconic-font.min.css" />
        <!-- bootstrap -->
        <asset:stylesheet src="smart/plugins/bootstrap/css/bootstrap.min.css" />
        <!-- style -->
        <asset:stylesheet src="smart/pages/extra_pages.css" />

    </head>
    <script>
            function myFunction() {
              document.getElementById("hidebutton").style.display = "none";
              document.getElementById("showDiv").style.display = "block";
              }
    </script>
    <body>
        <div class="limiter">
            <div class="container-login100 page-background">
                <div class="wrap-login100">
                    <div class="tools">
                       <g:link controller="entranceAdmissionLogin" action="login"><i class="fa fa-close fa-2x" style="color:white !important; display: inline-block !important; float:right;margin-top:-25px;margin-right:-30px"></i></g:link>
                    </div>
                    <g:form url="[action:'verifyemail',controller:'EntranceAdmissionLogin']">
                        <p class="text-center txt-small-heading">
                            SIGN UP
                        </p>
                        <div class="wrap-input100 validate-input" data-validate="Enter username">
                            <label style="color:white;font-size:14px;font-style:bold;">Enter Your Email Address</label>
                            <input class="input100" id="email" type="email" name="email" placeholder="Enter Your Email Address" required autocomplete="off" >

                        </div>
                        <div class="container-login100-form-btn" id="hidebutton">
                            <button type="submit" id="btnSubmit" class="login100-form-btn">
                                Verify Email
                            </button>
                        </div>
                        <div class="container-login100-form-btn" id="showDiv" style="display:none">
                            <p class="text-center txt-small-heading">
                                      OTP SENT ON EMAIL
                            </p>
                        </div>
                    </g:form>
                </div>
            </div>
        </div>
        <!-- start js include path -->
        <asset:javascript src="smart/plugins/jquery/jquery.min.js"/>
        <!-- bootstrap -->
        <asset:javascript src="smart/plugins/bootstrap/js/bootstrap.min.js"/>
        <asset:javascript src="smart/pages/extra-pages/pages.js"/>
        <!-- end js include path -->
    </body>
</html>