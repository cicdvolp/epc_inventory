    <!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Responsive Admin Template" />
        <meta name="author" content="RedstarHospital" />
        <title>EduPlusCampus</title>
        <!-- google font -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- icons -->
        <asset:stylesheet src="smart/fonts/font-awesome/css/font-awesome.min.css" />
        <asset:stylesheet src="smart/plugins/iconic/css/material-design-iconic-font.min.css" />
        <!-- bootstrap -->
        <asset:stylesheet src="smart/plugins/bootstrap/css/bootstrap.min.css" />
        <!-- style -->
        <asset:stylesheet src="smart/pages/extra_pages.css" />

    </head>
    <body>
        <div class="limiter">
            <div class="container-login100 page-background">
                <div class="wrap-login100">
                    <g:form url="[action:'verifyotpFP',controller:'EntranceAdmissionLogin']">
                        <p class="text-center txt-small-heading">
                            OTP Verification
                        </p>
                        <div class="wrap-input100 validate-input" data-validate="Enter otp">
                            <input class="input100" id="otp" name="otp" type="password" placeholder="Enter OTP" required autocomplete="new-password">
                            <input class="input100" id="email" name="email" type="hidden" value="${email}">
                            <span class="" data-placeholder="&#xf207;"></span>
                        </div>
                        <div class="container-login100-form-btn">
                            <input class="login100-form-btn" type="submit" value="Verify OTP">
                        </div>
                    </g:form>
                </div>
            </div>
        </div>
        <!-- start js include path -->
        <asset:javascript src="smart/plugins/jquery/jquery.min.js"/>
        <!-- bootstrap -->
        <asset:javascript src="smart/plugins/bootstrap/js/bootstrap.min.js"/>
        <asset:javascript src="smart/pages/extra-pages/pages.js"/>
        <!-- end js include path -->
    </body>
</html>