<!DOCTYPE html>
<html lang="en">
   <head>
      <title>EduPlusCampus</title>
      <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
      <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
      <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
      <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
      <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <asset:stylesheet src="bootstrap.min.css"/>
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <asset:stylesheet src="smart/fonts/font-awesome/css/font-awesome.min.css"/>
      <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <style>
         .center {
         display: block;
         margin-left: auto;
         margin-right: auto;
         margin-top: 7.6rem;
         }
         html, body {
         height: 100vh;
         }
         .first{
         background-color: #e2e2e4;
         display: flex;
         justify-content: center;
         height: 100vh;
         }
         .second{
         display: flex;
         flex-direction: column;
         }
         .txt-blue{
         color: #4647bf;
         }
         .txt-org {
         color: orange;
         }
         .btn {
         background: #4647bf;
         border-radius: 15px;
         margin-top: 5px;
         }
         .flex-center{
         display: flex;
         justify-content: center;
         }
         .cancel {
         display: flex;
         padding-top: 7.5rem;
         justify-content: flex-end;
         padding-bottom: 5rem;
         }
         body{
         overflow-y: scroll;
         overflow-x: hidden;
         }
         .logo-img1 {
             margin: auto;
             height: 100px;
         }
         .sign-in {
         padding-top: 1.1rem;
         font-size: 17px;
         text-align: center;
         }
         .password {
         width: -webkit-fill-available;,
         }
         .form input:focus {
         outline: none;
         }
         .btn {
         width: -webkit-fill-available;
         }
         .btn:focus {
         outline: none;
         }
         input:-webkit-autofill {
         color: #2a2a2a !important;
         }
         .form input {
         border: 0;
         border-bottom: 0.05rem solid black;
         width: -webkit-fill-available;
         background: transparent;
         margin-bottom: 0.4rem;
         }
         .fa-eye {
         position: absolute;
         margin-left: -1.5rem;
         }
         .form p {
         font-size: 11px;
         color: #b22f2f;
         }
         .form {
         padding-bottom:2rem !important
         }
         input:-webkit-autofill {
         background: transparent !important;        }
         .footer {
         text-align: center;
         background-color: #34495e;
         margin: 0px;
         color:white;
         position: fixed;
         left: 0;
         bottom: 0;
         width: 100%;
         }
         .form-input {
         width: -webkit-fill-available;
         padding-left: 1rem;
         }
         input:-webkit-autofill {transition: background-color 5000s ease-in-out 0s;}
         @media only screen and (max-width: 600px) {
         .first{
         display:none;
         }
         .footer{
         bottom: 0;
         }
         }
         #erplogin{
         height:auto;
         }
         .link a {
         color:#4647bf;
         float:right;
         text-decoration: none;
         }
         .field-icon {
         float: right;
         margin-top: -25px;
         position: relative;
         z-index: 2;
         }
         .btn:hover {
         color: #fff;
         background-color: #4647bf;
         }
      </style>
   </head>
   <body>
      <div id="erplogin">
         <div class="row">
            <div class="first  col-lg-6">
                <g:if test="${flash.message}">
                    <span style="color:green; font-weight:900;">${flash.message}</span>
                </g:if>
                <g:if test="${flash.error}">
                    <span style="color:red; font-weight:900;">${flash.error}</span>
                </g:if>

                <span class="logo-img1" style="font-size:18px; font-weight:900;">
                    Pre-Admission Process Steps/Manual :
                    <a href="${url}" target="blank">Click Here</a>
                </span>
                <!--<img class="logo-img1" src="https://www.vierp.in/logo/edu+camp-01.png">-->
            </div>
            <div class="col-lg-6 flex-center">
               <div class="second col-lg-8">
                  <br>
                  <br>
                  <img class="center " src="https://www.vierp.in/logo/edu+camp-01.png" style="height:100px;">
                  <!--<img class="center logo-img1" src="${loginlogo}" style="height:150px;">-->
                  <!-- <marquee behavior="scroll" direction="left" scrollamount="8" bgcolor="#F39C12" onmouseover="this.stop();" onmouseout="this.start();"><b>Use Your Official Email as Username instead of PRN Number or Employee Code</b></marquee> -->
                  <h2 class="sign-in">
                      <span class="txt-blue" style="color:${button_color}">
                          <strong>SIGN</strong>
                      </span>
                      <span class="txt-org" style="color:${text_color}">
                          <strong>IN</strong>
                      </span>
                  </h2>
                  <br>
                  <g:form url="[controller:'EntranceAdmissionLogin',action:'processerplogin']">
                     <div class="form center-block" style="display: flex;">
                        <div><i class="fa fa-user"></i></div>
                        <div class="form-input">
                           <input type="text" class="form-group" name="username" id="username" placeholder="Enter your Email" autofocus"="" value="${emp_code}" style="width:100% !important;" required>
                           <!-- <span style="color:red; font-weight:500;">Please Use Your Official Email As Username</span> -->
                        </div>
                     </div>
                     <div class="form center-block" style="display: flex;">
                        <div><i class="fa fa-lock"></i></div>
                        <div class="form-input" >
                           <input class="form-group" type="password" data-toggle="password"  name="password" id="password"  placeholder="Enter Your Password"  style="width:100% !important;" required>
                           <%--    <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>  --%>
                        </div>
                     </div>
                     <button type="submit" class="btn btn-primary" style="background:${button_color};font-weight:600;">SIGN IN</button>

                     <g:if test="${flash.error}">
                         <br><br>
                         <center><span style="color:red; font-weight:500;">${flash.error}</span></center>
                         <br><br>
                     </g:if>
                  </g:form>
                  <!--<center><b><p style="color:${text_color};font-size:20px;">For any help contact contactus@edupluscampus.com</p></b></center>-->

                  <div class="row">
                       <div class="col-sm-12">
                              <br>
                              <!--<a style="color:#292DC2; text-decoration: none;float:left;" href="https://learner.vierp.in/feedback">Suggestions-Queries-Feedback-Complaints</a>-->
                              <g:link style="color:#292DC2; text-decoration: none;float:left;" controller="entranceAdmissionLogin" action="easignup">Sign Up</g:link>
                              <g:link style="color:#292DC2; text-decoration: none;float:right;" controller="entranceAdmissionLogin" action="forgetPassword">Forgot Password?</g:link>
                       </div>
                  </div>
               </div>
            </div>
         </div>
         <p class="footer">

             <!--  <span class="form-row col-sm-12">
                    <span class="col-sm-3"></span>
                    <span class="col-sm-6">For Any Query, Please Contact Us,</span>
                    <span class="col-sm-3"></span>
              </span>
              <br>
              <span class="form-row col-sm-12">
                  <span class="col-sm-4">
                      Piyush Deshpande<br>
                      Email : piyush.deshpande16@vit.edu
                  </span>

                  <span class="col-sm-4">
                      Khushal Hasjia<br>
                      Email : contactus@edupluscampus.com
                  </span>

                  <span class="col-sm-4">
                        Dipali Mundada<br>
                        Email : dmundada@eduplusnow.com
                  </span>
              </span>
              <br><br><br> -->

              <span class="form-row col-sm-12">
                  <span class="col-sm-3"></span>
                  <span class="col-sm-6">© Copyright VGESPL 2020.All Rights Reserved</span>
                  <span class="col-sm-3"></span>
              </span>
         </p>
      </div>
      <%--
         <div class="card-footer card-foot">
                   <div class="row">
                       <div class="col-sm-4" align="center">
                              <a href="https://classroom.volp.in/" target="blank" >
                                  <img src="https://www.vierp.in/logo/free_1.png" alt="Card image" class="volp-logo-tag"/>
                                  <img src="https://www.vierp.in/logo/volp_logo.png" alt="Card image"  class="volp-logo"/>
                              </a>
                              </br>
                              <a href="https://www.google.com/url?q=https://apps.apple.com/kw/app/volp-classroom/id1475739735&sa=D&source=hangouts&ust=1569302983335000&usg=AFQjCNGqgk4zQ-lD91HzGgXVmm-o6ehriA" target="blank">
                                  <img src="https://www.vierp.in/logo/apple-512.png" alt="Card image"class="applogo"/>
                              </a>
                              <a href="https://play.google.com/store/apps/details?id=com.volpclassroom" target="blank">
                                 <img src="https://www.vierp.in/logo/android-flat.png" alt="Card image" class="applogo"/>
                              </a>
                       </div>
                       <div class="col-sm-4" align="center">
                               <img src="https://www.vierp.in/logo/pri_1.png" alt="Card image" class="epc-pri"/>

                             <a href="https://edupluscampus.com" target="blank" >
                               <img src="${loginlogo}" alt="Card image"  class="ecp-logo" style="height:150px;"/>
                               <h5 style="color:#FFF;"><b>Education Redefined</b></h5>
                              </a>
                       </div>
                       <div class="col-sm-4" align="center" >
                               <a href="https://www.eduplusnow.com/" target="blank">
                                   <img src="https://www.vierp.in/logo/pri_1.png" alt="Card image" class="edu-logo-tag"/>
                                   <img src="https://www.vierp.in/logo/epn_logo.png" alt="Card image" class="edu-logo"/>
                               </a>
                               </br>
                               <a href="#" target="blank">
                                   <img src="https://www.vierp.in/logo/apple-512.png" alt="Card image" class="applogo"/>
                               </a>
                               <a href="https://play.google.com/store/apps/details?id=com.eduplusnow" target="blank">
                                   <img src="https://www.vierp.in/logo/android-flat.png" alt="Card image" class="applogo"/>
                               </a>
                       </div>
                     </div>
               </div>
          --%>
      <script>
         <%-- $(".toggle-password").click(function() {
            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") {
              input.attr("type", "text");
            } else {
              input.attr("type", "password");
            }
            }); --%>


         /*$(document).ready(function() {

                 var remember = $.cookie('remember');
                 if (remember == 'true')
                 {
                     var email = $.cookie('email');
                     var password = $.cookie('password');
                     // autofill the fields
                     $('#username').val(email);
                     $('#password').val(password);
                 }
          $("#login").submit(function() {
                 if ($('#remember').is(':checked')) {
                     var email = $('#username').val();
                     var password = $('#password').val();

                     // set cookies to expire in 14 days
                     $.cookie('email', email, { expires: 14 });
                     $.cookie('password', password, { expires: 14 });
                     $.cookie('remember', true, { expires: 14 });
                 }
                 else
                 {
                     // reset cookies
                     $.cookie('email', null);
                     $.cookie('password', null);
                     $.cookie('remember', null);
                 }
           });
         });*/


      </script>
      <asset:javascript src="jquery.min.js"/>
      <asset:javascript src="jquery.cookie.min.js"/>
      <asset:javascript src="bootstrap.min.js"/>
   </body>
</html>