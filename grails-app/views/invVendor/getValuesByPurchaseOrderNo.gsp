<div class="col-sm-4">
    <label for="invoice"> PON :</label>
    <g:field  id="PON" name="PON" readonly="readonly" value="${pon}"  class="form-control"/>
</div>
<div class="col-sm-4">
    <label for="invoice"> Purpose :</label>
    <g:field  id="purpose" name="purpose" readonly="readonly" value="${purpose}" class="form-control"/>
</div>
<div class="col-sm-4">
    <label for="purchase_order_date" >Purchase Order date :</label>
    <g:field  id="purchase_order_date" name="purchase_order_date" readonly="readonly" value="${formatDate(format:'dd-MM-yyyy',date:purchase_order_date)}" class="form-control"/>
</div>
<div class="col-sm-4">
    <label for="amount">Amount :</label>
    <g:field id="amount" name="amount" readonly="readonly" value="${amount}" class="form-control"/>
</div>
<div class="col-sm-4">
    <label for="tax">Total Tax :</label>
    <g:field  id="tax" name="tax" readonly="readonly" value="${tax}" class="form-control"/>
</div>
<div class="col-sm-4">
    <label for="tax">Balance Amount :</label>
    <g:field id="bal_amt" readonly="readonly" name="bal_amt" type="number" value="${bal_amount}" min="0" class="form-control"/>
</div>