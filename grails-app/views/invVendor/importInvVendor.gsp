<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <asset:javascript src="jquery.min.js"/>
        <asset:javascript src="xlsx.full.min.js" />
        <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
        </style>
        <script>
            function doit(type, fn, dl) {
                var elt = document.getElementById('example');
                var wb = XLSX.utils.table_to_book(elt, {sheet:"Sheet JS"});
                return dl ?
                    XLSX.write(wb, {bookType:type, bookSST:true, type: 'base64'}) :
                    XLSX.writeFile(wb, fn || ('import_vendor.' + (type || 'xlsx')));
            }

        </script>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                    <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">
                        <g:form controller="invVendor" action="saveImportedInvVendor" enctype="multipart/form-data" >
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="card card-topline-purple">
                                        <div class="card-head">
                                            <header class="erp-table-header">Sample File Import Vendor</header>
                                        </div>
                                        <div style="margin-top:10px; margin-left:10px">
                                        <input type="button" class="btn btn-primary" onclick="doit('xlsx');" value="Download Sample File">
                                    </div>
                                    <div class="alert alert-info" style="margin:10px">
                                        <ul>
                                            <li>Download sample file , after mark entry completion save file in .xls format then upload</li>
                                            <li>In Excel File empty field is not accepted.<br/>In empty Field put "NA" (dash).</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Import Vendor</header>
                                        <g:link class="erp-table-header" data-toggle="modal" data-target="#addNewVendor"><span aria-hidden="true" class="icon-plus" title="Add New Vendor" style="float:right; font-size:20px;"></span></g:link>
                                    </div>
                                    <div style="margin-top:10px; margin-left:10px">
                                        <input type="file" name="importvendorfile" id="importvendorfile" class="btn btn-primary" accept=".xlsx">
                                    </div>
                                    <br>
                                    <div style="margin-left:10px; margin-bottom:10px">
                                        <g:submitToRemote url="[action: 'saveImportedInvVendor']"  value="Upload" name="save" class="btn btn-primary"/>
                                    </div>
                                </div>
                            </div>
                        </g:form>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mobile-table-responsive">
                        <div class="card card-topline-purple full-scroll-table" style="margin-left:15px">
                            <div class="card-head">
                               <header class="erp-table-header">Masters List For Reference</header>
                            </div>
                                 <div class="card-body " id="bar-parent">
                                      <div>
                                            <table  class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                  <thead>
                                                       <tr>
                                                            <th style="background:#AAAAAA; color:#FFF; text-align:center; font-weight:900;">Material Category</th>
                                                       </tr>
                                                  </thead>
                                                  <tbody>
                                                      <g:each in="${invMaterialCategoryList}" var="materialCategory" status="i">
                                                              <tr>
                                                                  <td>${materialCategory?.name}</td>
                                                              </tr>
                                                      </g:each>
                                                  </tbody>
                                            </table>
                                      </div>
                                 </div>
                        </div>
                    </div>
                </div>

 </div>
</div>







          <table id="example" style="display:none;">
                    <thead>
                        <tr>
                              <th class="mdl-data-table__cell--non-numeric">Company Name</th>
                              <th class="mdl-data-table__cell--non-numeric">Company Registration Number</th>
                              <th class="mdl-data-table__cell--non-numeric">Address</th>
                              <th class="mdl-data-table__cell--non-numeric">Rating</th>
                              <th class="mdl-data-table__cell--non-numeric">GST Number</th>
                              <th class="mdl-data-table__cell--non-numeric">TAN Number</th>
                              <th class="mdl-data-table__cell--non-numeric">Contact Person Name</th>
                              <th class="mdl-data-table__cell--non-numeric">Contact Person Contact</th>
                              <th class="mdl-data-table__cell--non-numeric">Contact Person Email</th>
                              <th class="mdl-data-table__cell--non-numeric">Material Category</th>
                              <th class="mdl-data-table__cell--non-numeric">Client List</th>
                              <th class="mdl-data-table__cell--non-numeric">Date of Establishment</th>
                              <th class="mdl-data-table__cell--non-numeric">Month of Establishment</th>
                              <th class="mdl-data-table__cell--non-numeric">Year of Establishment</th>
                        </tr>
                    </thead>
         </table>

                        <!-- Modal New -->
                                  <div class="modal fade" id="addNewVendor" role="dialog">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h4 class="modal-title">Add New Vendor</h4>
                                          <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                       <g:form action="saveSingleVendor">
                                        <div class="modal-body">
                                              <div class="form-group">
                                                  <label for="pwd">Company Registration Number:</label>
                                                  <span style="color:red; font-size: 16px">*</span>
                                                  <input type="text" class="form-control" name="companyRegistrationNumber" required="true">
                                              </div>
                                              <div class="form-group">
                                                 <label for="pwd">Company Name:</label>
                                                 <span style="color:red; font-size: 16px">*</span>
                                                    <input type="text" class="form-control" name="companyName" required="true">
                                                </div>
                                                <div class="form-group">
                                                 <label for="pwd">Company Address:</label>
                                                 <span style="color:red; font-size: 16px">*</span>
                                                    <input type="text" class="form-control" name="address" required="true">
                                                </div>
                                                <div class="form-group">
                                                 <label for="pwd">Company GST Number:</label>
                                                 <span style="color:red; font-size: 16px">*</span>
                                                    <input type="text" class="form-control" name="gstNo" required="true">
                                                </div>
                                                 <div class="form-group">
                                                  <label for="pwd">Company TAN Number:</label>
                                                  <span style="color:red; font-size: 16px">*</span>
                                                      <input type="text" class="form-control" name="tanNo" required="true">
                                                 </div>
                                                 <div class="form-group">
                                                   <label for="pwd">Contact Person Name:</label>
                                                   <span style="color:red; font-size: 16px">*</span>
                                                       <input type="text" class="form-control" name="contactPersonName" required="true">
                                                  </div>
                                                  <div class="form-group">
                                                    <label for="pwd">Contact Person Contact:</label>
                                                    <span style="color:red; font-size: 16px">*</span>
                                                        <input type="text" class="form-control" name="contactPersonContact" required="true">
                                                   </div>
                                                   <div class="form-group">
                                                 <label for="pwd">Contact Person Email:</label>
                                                 <span style="color:red; font-size: 16px">*</span>
                                                     <input type="text" class="form-control" name="contactPersonEmail" required="true">
                                                </div>
                                                 <div class="form-group">
                                                 <label for="pwd">Date of Establishment:</label>
                                                 <span style="color:red; font-size: 16px">*</span><br>
                                                     <g:datePicker name="dateofEstablishment" value="${new Date()}" precision="day"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Material Category:</label>
                                                    <span style="color:red; font-size: 16px">*</span>
                                                    <g:select name="invMaterailCategoryId"  optionValue="name" from="${invMaterialCategoryList}" style="width: 100% !important;"  class="select2" required="true"  value="${name}"  noSelection="['null':'Select Material Category']" optionKey="id" multiple="multiple"/><br>
                                                </div>
                                                 <div class="form-group">
                                                 <label for="pwd">Rating:</label>
                                                 <span style="color:red; font-size: 16px">*</span>
                                                     <input type="number" class="form-control" name="rating" required="true">
                                                </div>
                                                 <div class="modal-footer">
                                                  <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                </div>
</div>
                                       </g:form>
      </div>
      </div>
      </div>





    </body>
</html>
