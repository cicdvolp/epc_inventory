<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
    </head>
     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                 <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <g:if test="${flash.error}">
                             <div class="alert alert-success" >${flash.error}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12 mobile-table-responsive">
                            <div class="card card-topline-purple full-scroll-table">
                                <div class="card-head">
                                    <header class="erp-table-header">Vendor List</header>
                                </div>
                                        <div class="card-body " id="bar-parent">
                                                  <div class="table-responsive">
                                                        <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                          <thead>
                                                               <tr>
                                                                   <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Company Name</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Address</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">GST Number</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">TAN Number</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Contact Person Name</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Contact Person Contact</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Contact Person Email</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Material Category</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Client List</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Date Of Establishment</th>
                                                                   <th class="mdl-data-table__cell--non-numeric">Is Active</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                              <g:each in="${invVendorList}" var="vendor" status="i">
                                                                   <tr>
                                                                       <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${vendor?.company_name}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${vendor?.address}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${vendor?.gst_no}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${vendor?.tan_no}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${vendor?.contact_person_name}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${vendor?.contact_person_contact}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${vendor?.contact_person_email}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric">
                                                                            <g:include controller="invVendor" action="getVendorCategories" params="[id:vendor?.id]" />
                                                                       </td>
                                                                       <td class="mdl-data-table__cell--non-numeric">${vendor?.client_list}</td>
                                                                       <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${vendor?.date_of_establishment}"/></td>
                                                                       <td class="mdl-data-table__cell--non-numeric">
                                                                            <g:link controller="invVendor" action="activateImportedInvVendor" params="[vendorId : vendor?.id]">
                                                                                <g:if test="${vendor?.isactive}">
                                                                                    <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                                                </g:else>
                                                                            </g:link>
                                                                        </td>
                                                                  </tr>
                                                               </g:each>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                        </div>
                            </div>
                        </div>
                    </div>
      </body>
</html>
