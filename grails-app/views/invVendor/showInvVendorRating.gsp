<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
        <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                    <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-sm-6">
                        <div class="card card-topline-purple ">
                            <div class="card-head">
                                <header class="erp-table-header">Vendor Rating List</header>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <g:form action="fetchvendorratinglist" name="form">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <label>Vendor Company Name </label>${invVendorCompanyList}
                                            <g:select name="company_name" from="${vendorlist}"
                                                 class="select2 form-control"
                                                    optionValue="company_name" optionKey="id"
                                                    noSelection="['null':'Select Company Name']" />
                                        </div>
                                        <div class="col-sm-12">
                                            <center>
                                                <br>
                                                <g:submitToRemote url="[action: 'fetchvendorratinglist']" update="updateMe" value="Fetch" class="btn btn-success"/>
                                            </center>
                                        </div>
                                    </div>
                                </g:form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12  mobile-table-responsive">
                        <div class="card card-topline-purple full-scroll-table">
                            <div class="card-head">
                                <header class="erp-table-header">Vendor Rating List</header>
                            </div>
                            <div class="table-responsive" id="updateMe">
                                <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                            <th class="mdl-data-table__cell--non-numeric">Company Name</th>
                                            <th class="mdl-data-table__cell--non-numeric">Rating</th>
                                            <th class="mdl-data-table__cell--non-numeric">Out of Rating</th>
                                            <th class="mdl-data-table__cell--non-numeric">Comments</th>
                                            <th class="mdl-data-table__cell--non-numeric">Feedback Date</th>
                                            <th class="mdl-data-table__cell--non-numeric">FeedBack By</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <g:each in="${vendorratinglist}" var="vendor" status="i">
                                            <tr>
                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${vendor.invvendor.company_name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${vendor.rating}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${vendor.rating_outof}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${vendor.comment}</td>
                                                <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${vendor.rating_date}"/></td>
                                                <td class="mdl-data-table__cell--non-numeric">${vendor.ratingby}</td>
                                            </tr>
                                        </g:each>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
