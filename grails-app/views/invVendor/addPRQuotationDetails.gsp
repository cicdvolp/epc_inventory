<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Single Quotation Details</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>

                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Single Quotation Details ${invQuotation}</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->

                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <g:if test="${isapproved}">
                </g:if>
                <g:else>
                <div class="row">
                      <div class="col-sm-12">
                             <div class="card">
                             <div class="card-header">
                                                                           <b>Add New Details </b>
                                                                      </div>
                                   <div class="card-body">

                                            <g:form action="saveQuotationDetails">
                                             <div class="row">
                                             <div class="col-sm-4">
                                                  <label for="pwd">Material:</label>
                                                  <span style="color:red; font-size: 16px">*</span>
                                               <g:select name="materialId"  optionValue="name" from="${materialList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                         </div>
                                          <div class="col-sm-4">
                                                 <label for="pwd">Cost Per Unit</label>
                                                 <span style="color:red; font-size: 16px">*</span>
                                               <input type="number" min=0 class="form-control" name="costPerUnit" required="true">
                                               <input type="hidden" class="form-control" name="invQuotationId" required="true" value="${invQuotation}">
                                          </div>
                                           <div class="col-sm-4">
                                                    <label for="pwd">Quantity</label>
                                                    <span style="color:red; font-size: 16px">*</span>
                                               <input type="number" min=1 class="form-control" name="quantity" required="true">
                                           </div>
                                            </div>
                                             <div class="row>
                                              <div class="col-sm-12">
                                             <center>     <br>
                                               <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                 </center>
                                             </div>

                                            </g:form>
                                   </div>
                             </div>
                      </div>
                </div>
                </g:else>
                <div class="row">
                    <div class="col-md-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Single Quotation Details</header>
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Qty</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Cost Per Unit</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total Cost</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${quotationDetailsList}" var="quotes" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.invmaterial?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.quantity}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.cost_per_unit}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.total_cost}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                           <center>  <g:link controller="invVendor" action="activateQuotationDetails" params="[invQuotationDetailsId : quotes?.id,invQuotationId : quotes?.invquotation?.id]">
                                                              <g:if test="${quotes?.isactive}">
                                                            <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                        </g:if>
                                                        <g:else>
                                                            <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                        </g:else>
                                                        </g:link> </center>
                                                        </td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                         <g:if test="${isapproved}">
                                                            Quotation Approved,Cannot Edit
                                                         </g:if>
                                                         <g:else>
                                                           <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editQuoDetails${i}"></i>
                                                           </g:else>
                                                         </td>
                                                    </tr>

                                                    <!-- Modal edit -->
                                                          <div class="modal fade" id="editQuoDetails${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                              <div class="modal-content">
                                                                <div class="modal-header">
                                                                  <h4 class="modal-title">Edit Quotation Details</h4>
                                                                  <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                </div>
                                                               <g:form action="editPRQuotationDetails">
                                                                <div class="modal-body">
                                                                  <div class="form-group">
                                                                      <input type="hidden" value="${quotes?.id}" name="invQuotationDetailsId">
                                                                      <input type="hidden" value="${quotes?.invquotation?.id}" name="invQuotationId">
                                                                      </div>
                                                                       <div class="form-group">
                                                                        <label for="pwd">Material:</label>
                                                                            <span style="color:red; font-size: 16px">*</span>
                                                                            <g:select name="materialId"  optionValue="name" from="${materialList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Material']" optionKey="id"/><br>
                                                                      </div>
                                                                       <div class="form-group">
                                                                            <label for="pwd">Cost Per Unit:</label>
                                                                            <span style="color:red; font-size: 16px">*</span>
                                                                            <input type="number" min=1 class="form-control" name="costPerUnit" required="true">
                                                                        </div>
                                                                        <div class="form-group">
                                                                          <label for="pwd">Quantity:</label>
                                                                          <span style="color:red; font-size: 16px">*</span>
                                                                          <input type="number" min=1 class="form-control" name="quantity" required="true" optionValue="quantity">
                                                                      </div>
                                                                      <div class="form-group">
                                                                         <g:if test="${quotes?.isactive}">
                                                                               <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                         </g:if>
                                                                         <g:else>
                                                                               <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                         </g:else>
                                                                   </div>

                                                                <div class="modal-footer">
                                                                  <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                </div>
                                                               </g:form>
                                                              </div>
                                                            </div>
                                                          </div>
                                                </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



