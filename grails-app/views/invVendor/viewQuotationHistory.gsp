<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">View Quotation History</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>

                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">View Quotation's</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">View Quotation History </header>
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Tax</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-Approved?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Apply Date</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invQuotationsList}" var="quotes" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.tax}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.total}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                           <center> <g:if test="${quotes?.isapproved}">
                                                                <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else></center>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.quotation_date}</td>
                                                    </tr>
                                                </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



