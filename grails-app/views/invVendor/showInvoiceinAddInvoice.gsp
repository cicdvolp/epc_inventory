<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
    </head>
     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Invoice Details</header>
                                </div>
                                 <div class="card-body " id="bar-parent">
                                     <div class="form-group">
                                         <div class="row" style="margin-top:20px; margin-left:20px">
                                            <div class="col-sm-2">
                                                  <label for="pon">PO Number :</label><br>
                                                  <label for="podate" >PO Date :</label><br>
                                                  <label for="material">Purpose :</label><br>
                                                  <label for="amount">Amount :</label><br>
                                                   <label for="tax">Tax :</label><br>
                                            </div>
                                            <div class="col-sm-4">
                                                 <g:field  id="pon" name="pon" readonly="readonly" value="${invoice1?.invpurchaseorder?.pon}" style="width: 100% !important;"/><br>
                                                 <g:field  id="podate" name="podate" readonly="readonly" value="${formatDate(format:'dd-MM-yyyy',date:invoice1?.invpurchaseorder?.purchase_order_date)}" style="width: 100% !important;"/><br>
                                                 <g:field id="purpose" name="purpose" readonly="readonly" value="${invoice1?.purpose}" style="width: 100% !important;"/><br>
                                                 <g:field  id="amount" name="amount" readonly="readonly" value="${invoice1?.amount}" style="width: 100% !important;"/>
                                                 <g:field id="tax" name="tax"  readonly="readonly" value="${invoice1?.tax}" style="width: 100% !important;"/><br>
                                            </div>
                                            <div class="col-sm-2">
                                                  <label for="Total">Total Amount :</label><br>
                                                  <!--
                                                  <label for="rating">Balance Amount:</label><br>
                                                  <label for="invroom">Balance Tax:</label><br>
                                                  -->
                                                  <label for="inventory">Invoice Number</label><br>
                                                  <label for="inventory">File Name</label><br>
                                            </div>
                                            <div class="col-sm-4">
                                                  <g:field id="total" name="total"  readonly="readonly" value="${invoice1?.total}" style="width: 100% !important;"/><br>
                                                  <!--
                                                  <g:field id="balamount" name="balamount"  readonly="readonly" value="" style="width: 100% !important;"/><br>
                                                  <g:field id="baltax" name="baltax" readonly="readonly" value="" style="width: 100% !important;"/><br>
                                                  -->
                                                  <g:field id="invoicenumber" name="invoicenumber" readonly="readonly" value="${invoice1?.invoice_number}" style="width: 100% !important;"/><br>
                                                  <g:field id="filename" name="filename" readonly="readonly" value="${invoice1?.file_name}" style="width: 100% !important;"/><br>
                                            </div>
                                         <div>
                                     </div>
                                 </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </body>
      </html>


