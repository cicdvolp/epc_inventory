<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Vendor List</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>

                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">View PR</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Vendor List</header>
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Company Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Establishment</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Company Registration Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Rating(Max. 10)</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Address</th>
                                                    <th class="mdl-data-table__cell--non-numeric">GST</th>
                                                    <th class="mdl-data-table__cell--non-numeric">TAN</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Contact Person</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Contact Mobile</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Contact Email</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approve</th>

                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${vendorList}" var="ven" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.company_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.date_of_establishment}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.company_registration_number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.rating}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.address}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.gst_no}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.tan_no}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.contact_person_name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.contact_person_contact}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${ven?.contact_person_email}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                         <g:link controller="invVendor" action="activateVendor" params="[vendorId : ven?.id]">
                                                            <g:if test="${ven?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                        </td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



