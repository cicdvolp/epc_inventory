<div class="row">
			<div class="col-md-12" mobile-table-responsive">
				<div class="card card-topline-purple">
					<div class="card-head">
						<header class="erp-table-header"> Invoice List </header>
					</div>
					<div class="table-responsive">
						<table class="mdl-data-table ml-table-bordered">


                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Invoice Number</th>
                                                <th class="mdl-data-table__cell--non-numeric">Invoice Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Amount(Rs).</th>
                                                <th class="mdl-data-table__cell--non-numeric">Invoice File</th>
                                                <th class="mdl-data-table__cell--non-numeric">View Invoice</th>
                                                <th class="mdl-data-table__cell--non-numeric">Add Details</th>
                                                <th class="mdl-data-table__cell--non-numeric">View Invoice Details</th>
                                                <th class="mdl-data-table__cell--non-numeric">Approved?</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${invoice}" var="inv" status="i">
                                                <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${inv?.invoice_number}</td>
                                                            <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${inv?.invoice_date}"/></td>
                                                            <td class="mdl-data-table__cell--non-numeric">${inv?.amount}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${inv?.file_name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                               <center> <g:form action="showInvoiceinAddInvoice">
                                                                        <input type="hidden" value="${inv?.id}" name="invoiceId">
                                                                       <i class="fa fa-eye fa-2x erp-edit-icon-color"></i>
                                                                </g:form> </center>
                                                            </td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <center> <i class="fa fa-plus  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#addInvoiceDetails${i}"></i> </center>
                                                                <!-- Modal Add Invoice Details-->
                                                                <div class="modal fade" id="addInvoiceDetails${i}" role="dialog">
                                                                    <div class="modal-dialog">
                                                                    <div class="modal-content">
                                                                        <div class="modal-header">
                                                                        <h4 class="modal-title">Add New Invoice Details</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                        </div>
                                                                        <g:form action="saveInvoiceDetails">
                                                                        <div class="modal-body">
                                                                        <div class="form-group">
                                                                            <label for="pwd">Invoice:</label>
                                                                            <input type="hidden" value="${inv?.id}" name="invoiceId">
                                                                            <input type="text" class="form-control" name="invoiceId" required="true" value="${inv?.invoice_number}" disabled optionKey="id">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="pwd">Material: <span style="color:red; font-size: 16px">*</span></label>

                                                                            <g:select name="materialId"  optionValue="name" from="${invMaterialList}" class="form-control select2" required="true"  value="${id}"  noSelection="['null':'Select Material']" optionKey="id"/>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="pwd">Cost Per Unit: <span style="color:red; font-size: 16px">*</span></label>

                                                                            <input type="number" min=1 class="form-control" name="costPerUnit" required="true">
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label for="pwd">Quantity:   <span style="color:red; font-size: 16px">*</span></label>

                                                                            <input type="number" min=1 class="form-control" name="quantity" required="true">
                                                                        </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Remarks:</label>
                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                <input type="text" class="form-control" name="remarks" required="true">
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                                        </div>
                                                                        </g:form>
                                                                    </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                 <center> <g:link controller="invVendor" action="viewInvoiceDetailsByInvoice" params="[invoiceId : inv?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link> </center>
                                                            </td>
                                                            <td>
                                                                  <center>    <g:if test="${inv?.isapproved}">
                                                                        <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                                    </g:if>
                                                                    <g:else>
                                                                        <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                                    </g:else> </center>
                                                            </td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                    <center> <g:link controller="invVendor" action="activateInvoice" params="[invoiceId : inv?.id]">
                                                                        <g:if test="${invoice?.isactive}">
                                                                            <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                                        </g:if>
                                                                        <g:else>
                                                                            <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                                        </g:else>
                                                                    </g:link> </center>
                                                            </td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>



 <div class="table-responsive"> <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
