<table id="exportTable" class="display nowrap erp-full-width  ml-table-bordered">
    <thead>
        <tr>
            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
            <th class="mdl-data-table__cell--non-numeric">Company Name</th>
            <th class="mdl-data-table__cell--non-numeric">Rating</th>
            <th class="mdl-data-table__cell--non-numeric">Out of Rating</th>
            <th class="mdl-data-table__cell--non-numeric">Comments</th>
            <th class="mdl-data-table__cell--non-numeric">Feedback Date</th>
            <th class="mdl-data-table__cell--non-numeric">FeedBack By</th>
        </tr>
    </thead>
    <tbody>
        <g:each in="${vendorratinglist}" var="vendor" status="i">
            <tr>
                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                <td class="mdl-data-table__cell--non-numeric">${vendor.invvendor.company_name}</td>
                <td class="mdl-data-table__cell--non-numeric">${vendor.rating}</td>
                <td class="mdl-data-table__cell--non-numeric">${vendor.rating_outof}</td>
                <td class="mdl-data-table__cell--non-numeric">${vendor.comment}</td>
                <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${vendor.rating_date}"/></td>
                <td class="mdl-data-table__cell--non-numeric">${vendor.ratingby}</td>
            </tr>
        </g:each>
    </tbody>
</table>