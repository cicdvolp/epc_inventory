<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">View Purchase Requisitions</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>

                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">View PR</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Purchase Requisitions List</header>
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Amount</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purpose</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Request Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">PRN</th>
                                                    <!--
                                                    <th class="mdl-data-table__cell--non-numeric">Is Approved?</th>
                                                    -->
                                                    <th class="mdl-data-table__cell--non-numeric">Add Quotation</th>

                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invPRList}" var="pr" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${pr?.academicyear?.ay}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${pr?.amount}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${pr?.purpose}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${pr?.request_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${pr?.prn}</td>
                                                        <!--
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:if test="${pr?.isapproved}">
                                                                <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </td>
                                                        -->
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                          <g:form action="addQuotations">
                                                              <input type="hidden" value="${pr?.id}" name="invPRId">
                                                                   <button type="" class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Add Quotation</button>
                                                        </g:form>
                                                        </td>
                                                    </tr>
                                               </g:each>
                                                </tbody>
                                               </table>

      </div>
    </div>
  </div>
  </div>
</body>
</html>



