<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Add Quotation</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>

                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Add Quotation</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <div class="row">
                      <div class="col-sm-12">
                             <div class="card">
                             <div class="card-header">
                                    <b>Add New Quotation</b>
                              </div>
                                   <div class="card-body">

                                            <g:form action="savePRQuotation">
                                             <div class="row">
                                             <div class="col-sm-4">
                                                <label for="pwd">Amount:</label>
                                                   <span style="color:red; font-size: 16px">*</span>
                                                   <input type="number" min=0 class="form-control" name="amount" required="true">
                                                   <input type="hidden" class="form-control" name="invPRId" required="true" value=${invPRId}>
                                             </div>
                                             <div class="col-sm-4">
                                                    <label for="pwd">Tax:(%)</label>
                                                    <span style="color:red; font-size: 16px">*</span>
                                                   <input type="number" min=0 class="form-control" name="tax" required="true">
                                              </div>
                                              <!--
                                              <div class="col-sm-4">
                                                    <label for="pwd">Total:</label>
                                                   <input type="text" class="form-control" name="total" required="true">
                                               </div>
                                               -->
                                               </div>
                                                <div class="row>
                                                 <div class="col-sm-12">
                                                     <center>
                                                          <br>
                                                           <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                     </center>
                                                 </div>
                                                 </div>
                                             </div>
                                            </g:form>

                      </div>
                </div>
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Quotation List</header>
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total Cost</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Quotation Approved?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Quotation Details</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invQuotations}" var="quotes" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.data?.invpurchaserequisition?.academicyear?.ay}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.data?.updation_date}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${quotes?.data?.total}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <center><g:if test="${quotes?.data?.isapproved}">
                                                                <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else> </center>
                                                        </td>
                                                        <td>
                                                                 <g:each in="${quotes?.escalation}" var="app" status="a">
                                                                    <g:if test="${app?.invapprovalstatus?.name == 'Approved'}">
                                                                        <!--  <i class="fa fa-user fa-2x" style="color:green" aria-hidden="true" title="${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}"></i>-->
                                                                          <strong>${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</strong>:-
                                                                          ${app?.invapprovalstatus?.name}
                                                                    </g:if>
                                                                    <g:else>
                                                                        <!--<i class="fa fa-user fa-2x" style="color:red" aria-hidden="true" title="${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}"></i>-->
                                                                        <strong>${app?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</strong>:-
                                                                        ${app?.invapprovalstatus?.name}
                                                                    </g:else>
                                                                    <br>
                                                                </g:each>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:if test="${quotes?.data?.isapproved}">
                                                                Cannot Add,As Quotation Already Approved
                                                            </g:if>
                                                            <g:else>

                                                                <g:form action="addQuotationsDetails">
                                                                      <input type="hidden" value="${quotes?.data?.id}" name="InvQuotationId">

                                                                           <button type="" class="btn btn-primary"> <span class="glyphicon" title="Add Quotation Details"></span>Add Quotation Details</button>
                                                              </g:form>
                                                            </g:else>
                                                         </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                           <center>  <g:link controller="invVendor" action="activatePRQuotation" params="[invQuotationId : quotes?.data?.id]">
                                                                  <g:if test="${quotes?.data?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                            </g:link> </center>
                                                         </td>
                                                    </tr>
                                                    <!-- Modal edit -->
                                                </g:each>
                                                </tbody>
                                               </table>

                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->

  </div>
  </div>
</body>
</html>



