<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="smart_main_datatable_inst"/>
    <meta name="author" content="reena"/>
    <!---------------------Table data-------------------------------------------------------------->
    <asset:stylesheet src="jquery-ui.css"/>
    <asset:stylesheet src="datatables.min.css"/>
    <asset:stylesheet src="buttons.dataTables.min.css"/>
    <asset:javascript src="jquery.min.js"/>
    <asset:javascript src="datatables.min.js"/>
    <asset:javascript src="dataTables.buttons.min.js"/>
    <asset:javascript src="jszip.min.js"/>
    <asset:javascript src="pdfmake.min.js"/>
    <asset:javascript src="vfs_fonts.js"/>
    <asset:javascript src="buttons.html5.min.js"/>
    <!--------------------------------------------------------------------------------------------->
    <asset:javascript src="jqueryT1.min.js" />
    <script>
        $(document).ready(function() {
            $('#common').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
                paging: false
            } );
            $('#common1').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
                paging: false
            } );
        } );
    </script>
    <style>
        .custome-button {
             background-color:orange;
             border-color:orange;
             width:125px;
             color:white;
        }
        .custome-button:hover {
             background-color:#F7D698;
             border-color:#F7D698;
             width:125px;
             color:black;
        }
        .custom-form-box {
	    background-color:#F1F1F1;
            border:1px solid;
            border-radius:8px;
            padding:10px;
        }
    </style>
</head>
<body>
    <!-- Page Content -->
        <div class="page-content-wrapper">
             <div class="page-content">
                 <g:render template="/layouts/smart_template_inst/breadcrumb" />
                 <div class="row">
                      <div class="col-sm-12">
                          <div class="card">
                              <div class="card-body">
                                    <div id="document">
                                        <% /* =================================================== */ %>
                                            <g:if test="${failedlist}">
                                                <div>
                                                    <h4 style="color:blue;"><center><u><b>Failed Vendor List</b></u></center></h4>
                                                    <table class="table table-bordered display" id="common" style="background-color:#FFFFFF;">
                                                        <thead>
                                                            <tr>
                                                                <th> <center>Sr No.</center> </th>
                                                                <th> <center>Company Name</center> </th>
                                                                <th> <center>Address</center> </th>
                                                                <th> <center>GST No</center> </th>
                                                                <th> <center>TAN No</center> </th>
                                                                <th> <center>Contact Person name</center> </th>
                                                                <th> <center>Conatct No</center> </th>
                                                                <th> <center>Email Id</center> </th>
                                                                <th> <center>Client List</center> </th>
                                                                <th> <center>Establishment Date</center> </th>
                                                                <th> <center>Material Category List</center> </th>
                                                                <th> <center>Error Messages</center> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <g:each in="${failedlist[0]}" var="row" status="i">
                                                                <tr>
                                                                    <td style="color:red;">${i+1}</td>
                                                                    <td style="color:red;">${row?.row?.company_name}</td>
                                                                    <td style="color:red;">${row?.row?.address}</td>
                                                                    <td style="color:red;">${row?.row?.gst_no}</td>
                                                                    <td style="color:red;">${row?.row?.tan_no}</td>
                                                                    <td style="color:red;">${row?.row?.contact_person_name}</td>
                                                                    <td style="color:red;">${row?.row?.contact_person_contact}</td>
                                                                    <td style="color:red;">${row?.row?.contact_person_email}</td>
                                                                    <td style="color:red;">${row?.row?.client_list}</td>
                                                                    <td style="color:red;">${row?.row?.date_of_establishment1}-${row?.row?.month_of_establishment}-${row?.row?.year_of_establishment}</td>
                                                                    <td style="color:red;">${row?.row?.invmaterialcategory}</td>
                                                                    <td style="color:red;">${row?.msg}</td>
                                                                </tr>
                                                            </g:each>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </g:if>
                                            <g:else>
                                                <div>
                                                    <h4 style="color:blue;"><center><u><b>Failed Vendor List</b></u></center></h4>
                                                    <span><center><b>Data Not Available</b></center></span>
                                                </div>
                                            </g:else>
                                            <br/>
                                            <br/>
                                            <hr/>
                                            <g:if test="${successlist}">
                                                <div>
                                                    <h4 style="color:blue;"><center><u><b>Success Vendor List</b></u></center></h4>
                                                    <table class="table table-bordered display" id="common1" style="background-color:#FFFFFF;">
                                                        <thead>
                                                            <tr>
                                                                <th> <center>Sr No.</center> </th>
                                                                <th> <center>Company Name</center> </th>
                                                                <th> <center>Address</center> </th>
                                                                <th> <center>GST No</center> </th>
                                                                <th> <center>TAN No</center> </th>
                                                                <th> <center>Contact Person name</center> </th>
                                                                <th> <center>Conatct No</center> </th>
                                                                <th> <center>Email Id</center> </th>
                                                                <th> <center>Client List</center> </th>
                                                                <th> <center>Establishment Date</center> </th>
                                                                <th> <center>Material Category List</center> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <g:each in="${successlist[0]}" var="row" status="j">
                                                                <tr>
                                                                    <td style="color:green;">${j+1}</td>
                                                                    <td style="color:red;">${row?.row?.company_name}</td>
                                                                    <td style="color:red;">${row?.row?.address}</td>
                                                                    <td style="color:red;">${row?.row?.gst_no}</td>
                                                                    <td style="color:red;">${row?.row?.tan_no}</td>
                                                                    <td style="color:red;">${row?.row?.contact_person_name}</td>
                                                                    <td style="color:red;">${row?.row?.contact_person_contact}</td>
                                                                    <td style="color:red;">${row?.row?.contact_person_email}</td>
                                                                    <td style="color:red;">${row?.row?.client_list}</td>
                                                                    <td style="color:red;">${row?.row?.date_of_establishment1}-${row?.row?.month_of_establishment}-${row?.row?.year_of_establishment}</td>
                                                                    <td style="color:red;">${row?.row?.invmaterialcategory}</td>
                                                                </tr>
                                                            </g:each>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </g:if>
                                            <g:else>
                                                <div>
                                                    <h4 style="color:blue;"><center><u><b>Success Vendor List</b></u></center></h4>
                                                    <span><center><b>Data Not Available</b></center></span>
                                                </div>
                                            </g:else>
                                        <% /* =================================================== */ %>
                                    </div>
                              </div>
                          </div>
                      </div>
                 </div>
            </div>
        </div>
    </div>
</body>
</html>