<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
    </head>
     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
                        <!-- add content here -->
                        <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                        </g:if>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-topline-purple">
                                    <div class="card-body " id="bar-parent">
                                        <g:form action="saveInvVendorRating" name="form">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <label >Vendor Company Name : <span style="color:red; font-size: 12px">*</span></label>
                                                    <g:select name="company_name"   from="${invVendorCompanyList}"
                                                                                                class="select2 form-control"
                                                                                                optionValue="${invvendor}"
                                                                                                noSelection="['null':'Select Company Name']" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <label for="rating">Rating(On Scale of 1 to 10) :<span style="color:red; font-size: 12px">*</span></label>
                                                    <g:field class="form-control" type="number" id="rating" name="rating" min="1" max="10" required="true" value="${rating}" />
                                                </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label for="comment">Comment : <span style="color:red; font-size: 12px">*</span></label>
                                                        <textarea class="form-control" id="comment" name="comment" rows="4" cols="40" required="true"></textarea>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <center>
                                                            <br>
                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span>Submit</button>
                                                        </center>
                                                    </div>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div>
            </div>

     </body>
</html>
