<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
         <script>
            function callme() {
                            var purchaseOrderNo= document.getElementById("poNo").value;
                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function() {
                                if (this.readyState == 4 && this.status == 200) {
                                    document.getElementById("roledivid").innerHTML = this.responseText;
                                }
                            };
                            xmlhttp.open("GET", "${request.contextPath}/InvVendor/getValuesByPurchaseOrderNo?poNo=" + purchaseOrderNo);
                            xmlhttp.send();

                            var purchaseOrderNo= document.getElementById("poNo").value;
                            var xmlhttp = new XMLHttpRequest();
                            xmlhttp.onreadystatechange = function() {
                                if (this.readyState == 4 && this.status == 200) {
                                    document.getElementById("invoiceList").innerHTML = this.responseText;
                                }
                            };
                            xmlhttp.open("GET", "${request.contextPath}/InvVendor/getInvoicelist?poNo=" + purchaseOrderNo);
                            xmlhttp.send();
                       }
            function call() {
                                var academicYear= document.getElementById("ay").value;
                                var xmlhttp = new XMLHttpRequest();
                                xmlhttp.onreadystatechange = function() {
                                    if (this.readyState == 4 && this.status == 200) {
                                        document.getElementById("rolediv").innerHTML = this.responseText;
                                    }
                                };
                                xmlhttp.open("GET", "${request.contextPath}/InvVendor/getValuesByAcademicyearaddInvoice?ay=" + academicYear);
                                xmlhttp.send();

                           }


         </script>

    </head>

     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                     <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <g:if test="${flash.error}">
                             <div class="alert alert-danger" >${flash.error}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12 mobile-table-responsive">
                            <div class="card card-topline-purple ">
                                <div class="card-head">
                                    <header class="erp-table-header">Add Invoice</header>
                                </div>
                                <div class="card-body" id="bar-parent">
                                       <g:form action="saveInvoice" name="form" enctype="multipart/form-data" >
                                            <div class="row">
                                               <div class="col-sm-4">
                                                   <label>Academic Year : <span style="color:red; font-size: 16px">*</span></label><br>

                                                   <g:select name="ay" id="ay"  from="${academicYearList}"
                                                     class="form-control select2"
                                                   optionValue="${ay}"
                                                   noSelection="['null':'Please Select Academic Year']" onchange="call()"
                                                     />
                                               </div>

                                               <div class="col-sm-4" id="rolediv">

                                               </div>

                                            </div>

                                            <div class="row" id="roledivid">
                                                    <div class="col-sm-4">
                                                       <label for="invoice"> PON :</label><br>
                                                       <g:field  id="PON" name="PON" readonly="readonly" value="${pon}" class="form-control"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                       <label for="invoice"> Purpose :</label>
                                                       <g:field  id="purpose" name="purpose" readonly="readonly" value="${purpose}"  class="form-control" />
                                                    </div>
                                                    <div class="col-sm-4">
                                                       <label for="purchase_order_date" >Purchase Order date :</label>
                                                       <g:field  id="purchase_order_date" name="purchase_order_date" readonly="readonly" value="${formatDate(format:'dd-MM-yyyy',date:purchase_order_date)}"  class="form-control"/>
                                                    </div>

                                                    <div class="col-sm-4">
                                                        <label for="amount">Amount :</label>
                                                        <g:field id="amount" name="amount" readonly="readonly" value="${amount}"  class="form-control"/>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <label for="tax">Total Tax :</label>
                                                        <g:field  id="tax" name="tax" readonly="readonly" value="${tax}"  class="form-control"/>
                                                    </div>
                                              </div>
                                                                                        <br>
                                               <div class="row" >
                                                  <div class="col-sm-12">
                                                 <center><b>Enter Following Details</b></center>
                                              </div>
                                              </div><br>
                                               <div class="row" >
                                                     <div class="col-sm-4">
                                                         <label for="rating">Invoice Amount :<span style="color:red; font-size: 16px">*</span> </label>

                                                         <g:field id="invoiceAmt" name="invoiceAmt" type="number" min="0" class="form-control"/>
                                                      </div>
                                                      <div class="col-sm-4">
                                                         <label for="inventory">Invoice date: <span style="color:red; font-size: 16px">*</span></label><br>
                                                         <g:datePicker name="invoiceDate" precision="day" value="${invoiceDate}" class="form-control"/>
                                                      </div>
                                                       <div class="col-sm-4">
                                                       <label class="my-2">Upload Invoice:</label>&nbsp;
                                                                                                                <span style="color:red; font-size: 16px">*</span>
                                                                                                                <input  accept="image/jpeg,image/png,application/pdf" type="file" class="form-control-file my-2" id="invoicefile "name="invoicefile" required="true"/>

                                                      </div>
                                               </div>

                                                <div class="row">
                                                    <div class="col-sm-12">
                                                    <center>
                                                    <br>
                                                             <input type="hidden" name="poNo" id="poNo" value="${poNo}" />
                                                         <button type="submit" class="btn btn-primary"  > <span class="glyphicon glyphicon-save"></span> Save</button>
                                                    </center>
                                                    </div>
                                                </div>
                                     </g:form>

                                 </div>
                            </div>
                        </div>
                    </div>

                    <div id="invoiceList">
                    </div>

                     <!--END OF INVOICE LIST-->
                 </div>
             </div>
      </body>
</html>


