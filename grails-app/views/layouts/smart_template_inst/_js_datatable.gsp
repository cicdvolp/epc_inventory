<!-- Js -->
<asset:javascript src="smart/plugins/material/material.min.js"/>
<asset:javascript src="smart/pages/material-select/getmdl-select.js"/>

<!-- start js include path -->
<asset:javascript src="smart/plugins/jquery/jquery.min.js"/>
<asset:javascript src="smart/plugins/popper/popper.js"/>
<asset:javascript src="smart/plugins/jquery-blockui/jquery.blockui.min.js"/>
<asset:javascript src="smart/plugins/jquery-slimscroll/jquery.slimscroll.js"/>

<!-- bootstrap -->
<asset:javascript src="smart/plugins/bootstrap/js/bootstrap.min.js"/>
<asset:javascript src="smart/plugins/bootstrap-switch/js/bootstrap-switch.min.js"/>

<asset:javascript src="smart/plugins/material-datetimepicker/moment-with-locales.min.js"/>
<asset:javascript src="smart/plugins/material-datetimepicker/bootstrap-material-datetimepicker.js"/>
<asset:javascript src="smart/plugins/material-datetimepicker/datetimepicker.js"/>

<!-- Common js-->
<asset:javascript src="smart/app.js"/>
<asset:javascript src="smart/layout.js"/>
<asset:javascript src="smart/theme-color.js"/>

<!-- Data Table -->
<asset:javascript src="smart/plugins/datatables/jquery.dataTables.min.js"/>
<asset:javascript src="smart/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/dataTables.buttons.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/buttons.flash.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/jszip.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/pdfmake.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/vfs_fonts.js"/>
<asset:javascript src="smart/plugins/datatables/export/buttons.html5.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/buttons.print.min.js"/>
<asset:javascript src="smart/pages/table/table_data.js"/>

<!-- select 2 js -->
<!--
<asset:stylesheet href="select2.min.css" />
<asset:javascript src="select2.min.js" />
-->
<!--select2-->
<asset:javascript src="smart/plugins/select2/js/select2.js"/>
<asset:javascript src="smart/pages/select2/select2-init.js"/>


<!- chartjs -->
<asset:javascript src="chart_plugins/echarts/echarts.js"/>


