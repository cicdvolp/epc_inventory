<!-- start footer -->
<div class="page-footer">
    <div class="page-footer-inner">
        2020 &copy; EduPlusCampus By
        <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- end footer -->