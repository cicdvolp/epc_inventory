<!-- google font -->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css" />
<!-- Material Design Icons -->
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">

<!--bootstrap -->
<asset:stylesheet src="smart/plugins/bootstrap/css/bootstrap.css"/>
<asset:stylesheet src="smart/plugins/bootstrap/css/bootstrap.min.css"/>

<!-- Material Design Lite CSS -->
<asset:stylesheet src="smart/plugins/material/material.min.css"/>
<asset:stylesheet src="smart/material_style.css"/>

<!-- Theme Styles -->
<asset:stylesheet src="smart/theme/light/theme_style.css"/>
<asset:stylesheet src="smart/theme/light/style.css"/>
<asset:stylesheet src="smart/plugins.min.css"/>
<asset:stylesheet src="smart/responsive.css"/>
<asset:stylesheet src="smart/theme/light/theme-color.css"/>

<!-- icons -->
<asset:stylesheet src="smart/fonts/simple-line-icons/simple-line-icons.min.css"/>
<asset:stylesheet src="smart/fonts/font-awesome/css/font-awesome.min.css"/>
<asset:stylesheet src="smart/fonts/material-design-icons/material-icon.css"/>

<!-- Date Time item CSS -->
<asset:stylesheet src="smart/plugins/material-datetimepicker/bootstrap-material-datetimepicker.css"/>

<!-- vierp custome styles -->
<asset:stylesheet src="smart/vierp_smart.css" rel="stylesheet" type="text/css" />

<!--select2-->
<asset:stylesheet src="smart/plugins/select2/css/select2.css"/>
<asset:stylesheet src="smart/plugins/select2/css/select2-bootstrap.min.css"/>
