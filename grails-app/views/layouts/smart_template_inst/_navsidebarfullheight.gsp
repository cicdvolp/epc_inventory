<!-- start sidebar menu -->
<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu" style="height:100%;">
        <div id="remove-scroll">
            <ul class="sidemenu page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                <li class="sidebar-user-panel">
                    <div class="user-panel">
                        <div class="pull-left image">
                            <g:if test="${session.isphotopresent}">
                                <img class="img-circle user-img-circle border-dark" src="${session.profilephoto}" width="155" height="85" alt="User Image">
                            </g:if>
                            <g:else>
                                <asset:image src="img_avatar.png" alt="User Image" class="img-circle user-img-circle"/>
                            </g:else>
                        </div>
                        <div class="pull-left info">
                            <p>${session.userfullname}</p>
                            <i class="fa fa-circle user-online"></i><span class="txtOnline">Online</span>
                        </div>
                    </div>
                </li>
                <g:each in="${session.smarttemplatesidebarlist}" var="roletype" status="i">
                    <li class="nav-item start">
                        <a href="#" class="nav-link nav-toggle">
                            <!-- <i class="material-icons">dashboard</i> -->
                            <span style="color:#FFBA56; font-size:20px;">&nbsp;
                                <g:if test="${roletype[0]?.roletype?.icon_path}">
                                    ${raw(roletype[0]?.roletype?.icon_path)}
                                </g:if>
                                <g:else>
                                    <i class="fa fa-hand-o-right" aria-hidden="true"></i>
                                </g:else>
                            </span>
                            <span class="title">${roletype[0]?.roletype?.type_displayname}</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <g:each in="${roletype[0]?.link}" var="role" status="j">
                                <li class="nav-item">
                                    <a href="#" class="nav-link nav-toggle">
                                        <!-- <i class="fa fa-university"></i> -->
                                        ${role?.role?.role_displayname}
                                        <span class="arrow"></span>
                                    </a>
                                    <ul class="sub-menu background-color-sidebar">
                                        <g:each in="${role.rolelinks}" var="rolelink" status="k">
                                            <li class="nav-item">
                                                <g:link controller="${rolelink?.controller_name}" action="${rolelink?.action_name}" class="nav-link nav-toggle" params="[rlid:rolelink?.id]">
                                                    <!-- <i class="fa fa-bell-o"></i> -->
                                                    ${rolelink?.sort_order}. ${rolelink?.link_displayname}
                                                </g:link>
                                            </li>
                                        </g:each>
                                    </ul>
                                </li>
                            </g:each>
                        </ul>
                    </li>
                </g:each>
            </ul>
        </div>
    </div>
</div>
<!-- end sidebar menu -->