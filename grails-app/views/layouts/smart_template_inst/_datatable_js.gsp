<!-- Data Table -->
<asset:javascript src="smart/plugins/datatables/jquery.dataTables.min.js"/>
<asset:javascript src="smart/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/dataTables.buttons.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/buttons.flash.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/jszip.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/pdfmake.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/vfs_fonts.js"/>
<asset:javascript src="smart/plugins/datatables/export/buttons.html5.min.js"/>
<asset:javascript src="smart/plugins/datatables/export/buttons.print.min.js"/>
<asset:javascript src="smart/pages/table/table_data.js"/>

<!-- select 2 js -->
<asset:stylesheet href="select2.min.css" />
<asset:javascript src="select2.min.js" />