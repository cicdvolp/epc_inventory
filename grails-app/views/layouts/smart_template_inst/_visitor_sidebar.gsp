<!-- start sidebar menu -->
<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu" style="height:100%;">
        <div id="remove-scroll">
            <ul class="sidemenu page-header-fixed slimscroll-style" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                <li class="sidebar-user-panel">
                    <div class="user-panel">
                        <asset:image src="epc.png" alt="User Image" class="" style="width : 200px;"/>
                    </div>
                </li>
                <br>
                <br>
                <li class="nav-item start">
                    <div class="nav-link nav-toggle">
                        <center>
                            <span class="title" style="color:white; font-size:20px;">Visitors Pass</span>
                        </center>
                    </div>
                </li>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <hr>
                <li class="nav-item start">
                    <center>
                        <span class="title" style="color:#57FEF6;">2020 © <a href="www.eduplusnow.com" class="nav-link nav-toggle" style="color:white;"> EduPlusCampus </a> By VGESPL</span>
                    </center>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- end sidebar menu -->