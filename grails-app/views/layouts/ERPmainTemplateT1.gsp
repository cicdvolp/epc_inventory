<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title></title>
    <link rel="stylesheet"
    href="https://fonts.googleapis.com/css?family=Tangerine">
    <link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <asset:stylesheet src="bootstrapT1.min.css" />

    <!-- MetisMenu CSS -->
    <asset:stylesheet src="metisMenuT1.min.css" />

    <!-- Custom CSS -->
    <asset:stylesheet src="sb-admimT1.min.css" />

    <!-- Custom Fonts -->
    <asset:stylesheet src="font-awesomeT1.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
         <!--[if lt IE 9]>
             <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
             <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
         <![endif]-->

     </head>

     <body>

        <div id="wrapper">

            <!-- Navigation -->
            <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

                <div href="#" style="color: white;text-align:center; font-family: Rokkitt,Tangerine; font-size: 35px;; font-weight: normal;"><span>${session?.orgheadername}</span> </div>
                <g:include controller="Login" action="dynamicrolelinks"/>
                <g:include controller="Login" action="dynamicrolelinks"/>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>

                <!-- /.navbar-header ------------------------------------------ -->

                <ul class="nav navbar-top-links navbar-right">
                 <li><a style="color: white;">Hello,&nbsp;${session.user}</a></li>
                 <!-- /.dropdown -->
                 <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="background: white;opacity: .8; color: black;">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><g:link controller="Login" action="erpchangepassword"><i class="fa fa-unlock fa-fw"></i>Change Password</g:link>
                        </li>

                        <li class="divider"></li>
                        <li><g:link controller="Login" action="erplogin" params="[logout:1]"><i class="fa fa-sign-out fa-fw"></i> Logout</g:link>
                        </li>
                        <g:if test="${session.bypass}">
                                                     <li>
                                                        <g:link controller="Instructor" action="logoutbypass"><i class="fa fa-sign-out fa-fw"></i>Bypass Logout</g:link>
                                                     </li>
                                                </g:if>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                     <g:render template="/layouts/ERPlinkTemplateT1"/>

                 </ul>
             </div>
             <!-- /.sidebar-collapse -->
         </div>
         <!-- /.navbar-static-side -->


     </nav>

     <!-- Page Content -->
     <div id="page-wrapper">
        <div class="container-fluid">
            <g:link controller="Login" action="erphome" style="text-decoration: underline;border: none;position: absolute; left:250px" class="btn btn-sm btn-default" ><span class="glyphicon glyphicon-home"> </span>&nbsp;Home</g:link>
            <button style="text-decoration: underline;border: none;position: absolute; right: 0" class="btn btn-sm btn-default" onclick="history.back(-1)"><span class="glyphicon glyphicon-triangle-left"> </span>Go Back</button>
            <hr>

            <div class="row">
                <div class="col-lg-12">

                    <!-- <h3 class="page-header">Account Section</h3> -->


            </div>
            <!-- /.col-lg-12 -->
        </div> <!-- /.row -->


    </div> <!-- /.container-fluid -->


</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->


<footer class="navbar navbar-default" style="position: relative; bottom: 0; top: 18px">
    <p style="text-align: center; color: white; padding: 10px">All Rights Reserved @ VI Groups</p>
        <p style="text-align: center; color: white; padding: 10px">For Any Queries Please Contact : dpawar@eduplusnow.com &nbsp/&nbsp npatel@eduplusnow.com </p>


</footer>

<!-- jQuery -->
<asset:javascript src="jqueryT1.min.js" />

<!-- Bootstrap Core JavaScript -->
<asset:javascript src="bootstrapT1.min.js" />

<!-- Metis Menu Plugin JavaScript -->
<asset:javascript src="metisMenuT1.min.js" />

<!-- Custom Theme JavaScript -->
<asset:javascript src="sb-adminT1.js" />

</body>

</html>
