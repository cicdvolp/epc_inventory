<g:each var="rolelinks" in= "${session.rolelinkslist}">
 <g:if test="${rolelinks.isrolelinkactive==true}">
    <li>
      <g:link controller="${rolelinks.controller_name}" action="${rolelinks.action_name}" title="click here to ${rolelinks.link_name}"><i class="glyphicon glyphicon-triangle-right"></i>&nbsp;${rolelinks.link_name}</g:link>
    </li>
 </g:if>
</g:each>
