<!-- start color quick setting -->
<div class="settingSidebar">
    <a href="javascript:void(0)" class="settingPanelToggle"> <i class="fa fa-link" aria-hidden="true"></i>
    </a>
    <div class="settingSidebar-body ps-container ps-theme-default">
        <div class=" fade show active ">
            <div class="setting-panel-header">
                QUICK LINKS
                <g:link controller="Login" action="addquicklinks">
                    <span aria-hidden="true" class="icon-plus" title="Manage Quick Links" style="float:right; font-size:20px;"></span>
                </g:link>
            </div>
            <div class="slimscroll-style">
                <g:if test="${session?.fixedrolelinkslist}">
                    <g:each var="rolelink" in= "${session?.fixedrolelinkslist}" status="i">
                        <g:if test="${rolelink?.link_displayname}">
                            <div style="background:white;" class="setting-panel-header">
                                <g:link controller="${rolelink?.controller_name}" action="${rolelink?.action_name}" params="[rlid:rolelink?.id]" title="click here for ${rolelink?.link_displayname}" style="text-decoration: none;">
                                    ${rolelink?.link_displayname.toUpperCase()}
                                </g:link>
                            </div>
                        </g:if>
                    </g:each>
                </g:if>
            </div>
        </div>
    </div>
</div>
<!-- end color quick setting -->