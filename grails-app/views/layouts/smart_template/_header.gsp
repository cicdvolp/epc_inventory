<!-- start header -->
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <!-- logo start -->
        <div class="page-logo">
            <g:link controller="login" action="erphome">
                <asset:image src="logo-white.png"  alt="Image" height="50px" width="90%"/>
            </g:link>
        </div>
        <!-- logo end -->
        <ul class="nav navbar-nav navbar-left in">
            <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
        </ul>
        <!-- start mobile menu -->
        <a href="#" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
            <span></span>
        </a>
        <!-- end mobile menu -->
        <!-- start header menu -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <li><a href="#" class="fullscreen-btn"><i class="fa fa-arrows-alt"></i></a></li>
                <!-- start manage user dropdown -->
                <li class="dropdown dropdown-user">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                        <g:if test="${session.isphotopresent}">
                            <img class="img-circle user-img-circle border-dark" src="${session.profilephoto}" width="29" height="29" >
                        </g:if>
                        <g:else>
                            <asset:image src="img_avatar.png" alt="" class="img-circle"/>
                        </g:else>
                        <span class="username username-hide-on-mobile"> ${session.userfullname} </span>
                        <i class="fa fa-angle-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-default">
                        <li>
                            <g:if test="${session.isstudent}">
                                <g:link controller="learner" action="learnerfillprofileerp">
                                    <i class="icon-user"></i> My Profile
                                </g:link>
                            </g:if>
                            <g:else>
                                <g:link controller="instructor" action="facultyProfile">
                                    <i class="icon-user"></i> My Profile
                                </g:link>
                            </g:else>
                        </li>
                        <li>
                            <g:link controller="Login" action="changepassword">
                                <i class="icon-lock"></i> Change Password
                            </g:link>
                        </li>
                        <li class="divider"> </li>
                        <li>
                            <g:link controller="Login" action="erplogin" params="[logout:1]">
                                <i class="icon-logout"></i> Log Out
                            </g:link>
                        </li>
                        <g:if test="${session?.bypass}">
                            <li>
                                <g:link controller="Instructor" action="logoutbypass"><i class="fa fa-sign-out fa-fw"></i>Bypass Logout</g:link>
                            </li>
                        </g:if>
                    </ul>
                </li>
                <!-- end manage user dropdown -->
            </ul>
        </div>
    </div>
</div>
<!-- end header -->