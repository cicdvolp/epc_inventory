<style>
    @media (max-width: 600px) {
        .hide-img {
            display: none !important;
        }
        .show-img {
            display: block !important;
        }
    }
    @media (max-width: 750px) {
        .hide-img {
            display: none !important;
        }
        .show-img {
            display: block !important;
        }
    }
    @media (max-width: 900px) {
        .hide-img {
            display: none !important;
        }
        .show-img {
            display: block !important;
        }
    }
    @media (min-width: 900px) {
        .hide-img {
            display: block !important;
        }
        .show-img {
            display: none !important;
        }
    }
</style>

<!-- start header -->
<div class="page-header navbar navbar-fixed-top">
    <div class="page-header-inner ">
        <!-- logo start -->
        <div class="page-logo hide-img">
            <g:link controller="login" action="erphome">
                <asset:image src="logo-white.png" alt="Image" height="50px" width="90%"/>
            </g:link>
        </div>
        <!-- logo end -->
        <!-- start header menu -->
        <div class="top-menu d-inline">
            <div class="d-inline page-logo show-img">
                <g:link controller="login" action="erphome">
                    <asset:image src="apple-touch-icon-57x57.png" alt="" height="50px" width="auto"/>
                </g:link>
            </div>
            <div class="d-inline">
                <ul class="nav navbar-nav pull-right">
                    <li><a href="#" class="fullscreen-btn"><i class="fa fa-arrows-alt"></i></a></li>
                    <!-- start manage user dropdown -->
                    <li class="dropdown dropdown-user">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <g:if test="${session.isphotopresent}">
                                <img class="img-circle user-img-circle border-dark" src="${session.profilephoto}" width="29" height="29" >
                            </g:if>
                            <g:else>
                                <asset:image src="img_avatar.png" alt="" class="img-circle"/>
                                <!--
                                <label>pshitre505@gmail.com</label>
                                <g:if test="${session?.fullname}">
                                    <label>${session?.fullname}</label>
                                </g:if>
                                <g:elseif test="${session?.email}">
                                    <label>${session?.fullname}</label>
                                </g:elseif>
                                <g:else>
                                    <asset:image src="img_avatar.png" alt="" class="img-circle"/>
                                </g:else>
                                -->
                            </g:else>
                            <span class="username username-hide-on-mobile"> ${session.userfullname} </span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <!--
                            <li>
                                <g:if test="${session.isstudent}">
                                    <g:link controller="learner" action="learnerfillprofileerp">
                                        <i class="icon-user"></i> My Profile
                                    </g:link>
                                </g:if>
                                <g:else>
                                    <g:link controller="instructor" action="facultyProfile">
                                        <i class="icon-user"></i> My Profile
                                    </g:link>
                                </g:else>
                            </li>
                            <li>
                                <g:link controller="Login" action="changepassword">
                                    <i class="icon-lock"></i> Change Password
                                </g:link>
                            </li>
                            <li class="divider"> </li>
                            -->
                            <li>
                                <g:link controller="EntranceAdmissionLogin" action="logout" params="[logout:1]">
                                    <i class="icon-logout"></i> Log Out
                                </g:link>
                            </li>
                        </ul>
                    </li>
                    <!-- end manage user dropdown -->
                </ul>
            </div>
        </div>
    </div>
</div>
<!-- end header -->