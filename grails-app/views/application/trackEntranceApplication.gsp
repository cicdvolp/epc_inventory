<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template/meta" />
        <g:render template="/layouts/smart_template/css" />
        <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <title>EduPlusCampus</title>
        <style>
            @media (max-width: 600px) {
                .btn {
                    width:100% !important;
                    text-align:center !important;
                    margin-top: 3px !important;
                    margin-bottom:3px !important;
                }
            }
            .btn {
                width:100% !important;
                text-align:center !important;
                margin-top: 3px !important;
                margin-bottom:3px !important;
            }
        </style>
    </head>
    <body class="page-header-fixed page-full-width sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
        <div class="page-wrapper">
            <g:render template="/layouts/smart_template/header1" />
            <div class="page-container">
                <!-- start page content -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- add content here -->
                            <div class="row">
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-primary">Applicant Profile</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceApplication" class="btn btn-primary">Application Form</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-primary">Application Form Fees</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="trackEntranceApplication" class="btn btn-light">Application Tracking</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceExamination" class="btn btn-primary">Entrance Exam</g:link>
                                </div>
                            </div>

                            <br>

                            <g:if test="${flash.error}">
                                <div class="alert alert-danger" >${flash.error}</div>
                            </g:if>
                            <g:if test="${flash.success}">
                                <div class="alert alert-success" >${flash.success}</div>
                            </g:if>
                            <g:if test="${flash.message}">
                                <div class="alert alert-success" >${flash.message}</div>
                            </g:if>

                            <div class="row">
                                <div class="col-sm-12">
                                    <g:if test="${entranceApplication}">
                                        <div class="card card-topline-purple">
                                            <div class="" id="bar-parent">
                                                <div class="table-responsive">
                                                    <table class="display nowrap erp-full-width table-striped table-hover mdl-data-table ml-table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Application ID</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Application preview</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Is Fees Paid?</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Fees Receipt</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Application Status</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Rejection Reason</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <g:each in="${entranceApplication}" var="name" status="i">
                                                                <tr>
                                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entranceversion?.academicyear?.ay}</td>
                                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entranceversion?.programtype?.name}</td>
                                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entranceversion?.entrance_name}</td>
                                                                    <td class="mdl-data-table__cell--non-numeric">${name?.applicaitionid}</td>
                                                                    <td class="mdl-data-table__cell--non-numeric">
                                                                       <g:link controller="Application" action="applicationPreview" params="[entranceVersion : name?.entranceversion?.id]" target="blank"><i style="cursor: pointer;" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                                    </td>
                                                                    <td class="mdl-data-table__cell--non-numeric">
                                                                        <g:if test="${name?.is_physically_handicapped || name?.is_visually_handicapped }">
                                                                            <g:if test="${name?.entranceversion?.is_fee_applicable_to_handicap_person}">
                                                                                <g:if test="${name?.isfeespaid}">
                                                                                    <g:link>
                                                                                        <i class='fa fa-check fa-2x' style='color:green' aria-hidden='true'></i>
                                                                                        <!--<button disabled class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Fees Paid</button>-->
                                                                                    </g:link>
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <i class='fa fa-close fa-2x' style='color:red' aria-hidden='true'></i>
                                                                                    <!--<button disabled class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Fees Not Paid</button>-->
                                                                                </g:else>
                                                                            </g:if>
                                                                            <g:else>
                                                                                -
                                                                            </g:else>
                                                                        </g:if>
                                                                        <g:else>
                                                                            <g:if test="${name?.isfeespaid}">
                                                                                <g:link>
                                                                                    <i class='fa fa-check fa-2x' style='color:green' aria-hidden='true'></i>
                                                                                    <!--<button disabled class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Fees Paid</button>-->
                                                                                </g:link>
                                                                            </g:if>
                                                                            <g:else>
                                                                                <i class='fa fa-close fa-2x' style='color:red' aria-hidden='true'></i>
                                                                                <!--<button disabled class="btn btn-primary"> <span class="glyphicon" title="Add PR Item"></span>Fees Not Paid</button>-->
                                                                            </g:else>
                                                                        </g:else>
                                                                    </td>
                                                                    <td class="mdl-data-table__cell--non-numeric"><g:link controller="application" action="getreceipt" params="[receiptno:name?.entranceapplicationreceipt?.feesreceiptid]">${name?.entranceapplicationreceipt?.feesreceiptid}</g:link></td>
                                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicationstatus?.name}</td>
                                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entrancerejectionreason?.name}</td>
                                                                    <td class="mdl-data-table__cell--non-numeric">${name?.remark}</td>
                                                                </tr>
                                                            </g:each>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </g:if>
                                    <g:else>
                                        <div class="alert alert-info">
                                            <strong>
                                                <center>
                                                    <h1>Please Fill Application First!</h1>
                                                </center>
                                            </strong>
                                        </div>
                                    </g:else>
                                </div>
                            </div>
                        <!-- end content here -->
                    </div>
                </div>
                <!-- end page content -->
            </div>
            <!-- start footer -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2020 &copy; EduPlusCampus By
                    <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- end footer -->
        </div>
        <g:render template="/layouts/smart_template/js" />
        <g:render template="/layouts/smart_template_inst/datatable_js" />
        <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
            <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
            <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>
    </body>
</html>
