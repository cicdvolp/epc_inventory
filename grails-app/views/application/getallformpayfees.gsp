<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template/meta" />
        <g:render template="/layouts/smart_template/css" />
        <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <title>EduPlusCampus</title>
        <style>
            @media (max-width: 600px) {
                .btn {
                    width:100% !important;
                    text-align:center !important;
                    margin-top: 3px !important;
                    margin-bottom:3px !important;
                }
            }
            .btn {
                width:100% !important;
                text-align:center !important;
                margin-top: 3px !important;
                margin-bottom:3px !important;
            }
        </style>
    </head>
    <body class="page-header-fixed page-full-width sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
        <div class="page-wrapper">
            <g:render template="/layouts/smart_template/header1" />
            <div class="page-container">
                <!-- start page content -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- add content here -->

                            <g:if test="${flash.error}">
                                <div class="alert alert-danger" >${flash.error}</div>
                            </g:if>
                            <g:if test="${flash.success}">
                                <div class="alert alert-success" >${flash.success}</div>
                            </g:if>
                            <g:if test="${flash.message}">
                                <div class="alert alert-success" >${flash.message}</div>
                            </g:if>

                            <div class="row">
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-primary">Applicant Profile</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceApplication" class="btn btn-primary">Application Form</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-light">Application Form Fees</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="trackEntranceApplication" class="btn btn-primary">Application Tracking</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceExamination" class="btn btn-primary">Entrance Exam</g:link>
                                </div>
                            </div>

                            <br>









                        <!-- end content here -->
                    </div>
                </div>
                <!-- end page content -->
            </div>
            <!-- start footer -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2020 &copy; EduPlusCampus By
                    <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- end footer -->
        </div>
        <g:render template="/layouts/smart_template/js" />
        <g:render template="/layouts/smart_template_inst/datatable_js" />
        <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
            <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
            <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>
    </body>
</html>
