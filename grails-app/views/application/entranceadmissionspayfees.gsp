<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template/meta" />
        <g:render template="/layouts/smart_template/css" />
        <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <title>EduPlusCampus</title>
        <style>
            @media (max-width: 600px) {
                .btn {
                    width:100% !important;
                    text-align:center !important;
                    margin-top: 3px !important;
                    margin-bottom:3px !important;
                }
            }
            .btn {
                width:100% !important;
                text-align:center !important;
                margin-top: 3px !important;
                margin-bottom:3px !important;
            }
        </style>
    </head>
    <body class="page-header-fixed page-full-width sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
        <div class="page-wrapper">
            <g:render template="/layouts/smart_template/header1" />
            <div class="page-container">
                <!-- start page content -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- add content here -->

                            <div class="row">
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-primary">Applicant Profile</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceApplication" class="btn btn-primary">Application Form</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-light">Application Form Fees</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="trackEntranceApplication" class="btn btn-primary">Application Tracking</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceExamination" class="btn btn-primary">Entrance Exam</g:link>
                                </div>
                            </div>

                            <br>

                            <g:if test="${flash.error}">
                                <div class="alert alert-danger" >${flash.error}</div>
                            </g:if>
                            <g:if test="${flash.success}">
                                <div class="alert alert-success" >${flash.success}</div>
                            </g:if>
                            <g:if test="${flash.message}">
                                <div class="alert alert-success" >${flash.message}</div>
                            </g:if>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <span>Applicant Name : </span> <span style="color:blue; font-size:14px; font-weight:bold;">${application?.entranceapplicant?.fullname}</span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <span>Academic Year : </span> <span style="color:blue; font-size:14px; font-weight:bold;">${application?.entranceversion?.academicyear?.ay}</span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <span>Program Type : </span> <span style="color:blue; font-size:14px; font-weight:bold;">${application?.entranceversion?.programtype?.displayname}</span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <!--<span>College Name : </span> <span style="color:blue; font-size:14px; font-weight:bold;">${application?.organization?.organization_name}</span>-->
                                                    <span>College Name : </span> <span style="color:blue; font-size:14px; font-weight:bold;">${application?.organization?.organization_code}</span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <span>Entrance Name : </span> <span style="color:blue; font-size:14px; font-weight:bold;">${application?.entranceversion?.entrance_name}</span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <span>Entrance Date : </span> <span style="color:blue; font-size:14px; font-weight:bold;"><g:formatDate format="dd/MM/yyyy" date="${application?.entranceversion?.version_date}" /></span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <span>Application Start Date : </span> <span style="color:blue; font-size:14px; font-weight:bold;"><g:formatDate format="dd/MM/yyyy" date="${application?.entranceversion?.application_start_date}" /></span>
                                                </div>
                                                <div class="col-sm-4">
                                                    <span>Application End Date : </span> <span style="color:blue; font-size:14px; font-weight:bold;"><g:formatDate format="dd/MM/yyyy" date="${application?.entranceversion?.application_end_date}" /></span>
                                                </div>
                                            </div>
                                            <hr>
                                            <center>
                                                <span style="font-size:14px; ">Payable Amount</span>
                                                <span style="font-size:18px; font-weight:bold; color:blue;">${total}</span>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <form action="https://smmtrad.educationdoctor.in/smmtrad/Payment/techprocess.php" method="post" onsubmit="if(document.getElementById('agree').checked) { return true; } else { alert('Please indicate that you have read and agree to the Terms and Conditions and Privacy Policy'); return false; }">

                                        <g:hiddenField value="${total}" name="amount_balance" />
                                        <!-- <center><g:submitToRemote class="btn btn-sm btn-success" url="[action: 'studentonlinefeespayment']" update="updateMe" value="Pay Now" /></center>-->
                                        <input type="hidden" name="reqType" value="${transactionrequesttype?.type}">
                                        <input type="hidden" name="mrctCode" value="${transactionorganizationonlineaccount?.merchant_code}">
                                        <input type="hidden" name="mrctTxtID" value="${merchanttxnrefno}">
                                        <input type="hidden" name="currencyType" value="${transactioncurrencycode?.code}">
                                        <input type="hidden" name="amount" value="${total}">
                                        <input type="hidden" name="reqDetail" value="${shopping_cart_detail}">
                                        <input type="hidden" name="txnDate" value="${transaction_date}">
                                        <input type="hidden" name="locatorURL" value="${transactionredirecturl?.url}">
                                        <input type="hidden" name="key" value="${transactionorganizationonlineaccount?.merchant_key}">
                                        <input type="hidden" name="iv" value="${transactionorganizationonlineaccount?.merchant_iv}">
                                        <input type="hidden" name="returnURL" value="${transactionreturnurl?.url}">

                                        <div class="card">
                                            <div class="card-body">
                                                <span style="color:red; font-weight:bold;">Please read Instructions carefully before doing payment..</span>
                                            </div>
                                            <div class="table-responsive">
                                                <table class="mdl-data-table ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Transaction Mode</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Transaction charges</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Example</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">All Net Banking Transactions for collection</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Rs. 15 per transaction</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Fees: Rs. 20,000/- Charges= Rs. 15 + GST</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">All Credit/ Debit Card Transactions (Per Transaction)</td>
                                                            <td class="mdl-data-table__cell--non-numeric">1.37% per Transaction</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Fees: Rs. 20,000/- Charges= Rs. 274 + GST</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">All Cash Card Transactions (Per Transaction)</td>
                                                            <td class="mdl-data-table__cell--non-numeric">1% per Transaction</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Fees: Rs. 20,000/- Charges= Rs. 200 + Service Tax</td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric" colspan="3" style="color:red;">
                                                                <input type="checkbox" name="checkbox" value="check" id="agree" onchange="enableButton(this)" required="true"/> I have read and agree to the Terms and Conditions and Privacy Policy</div>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <center>
                                                <br>
                                                <input type="submit" name="submit" id="button2" value="Pay Now" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-sm btn-primary" >
                                            </center>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <script>
                                $(document).on("keydown", function(e) {
                                    if (e.key == "F5" || e.key == "F11" ||
                                        (e.ctrlKey == true && (e.key == 'r' || e.key == 'R')) ||
                                        e.keyCode == 116 || e.keyCode == 82) {

                                        e.preventDefault();
                                    }
                                });

                                function enableButton(checkbox) {
                                    if (checkbox.checked == true) {
                                        document.getElementById("button2").disabled = false;
                                    } else {
                                        document.getElementById("button2").disabled = true;
                                    }
                                }
                            </script>
                        <!-- end content here -->
                    </div>
                </div>
                <!-- end page content -->
            </div>
            <!-- start footer -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2020 &copy; EduPlusCampus By
                    <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- end footer -->
        </div>
        <g:render template="/layouts/smart_template/js" />
        <g:render template="/layouts/smart_template_inst/datatable_js" />
        <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
            <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
            <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>
    </body>
</html>
