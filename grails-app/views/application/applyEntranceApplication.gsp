<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template/meta" />
        <g:render template="/layouts/smart_template/css" />
        <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <!--select2-->
        <asset:stylesheet src="smart/plugins/select2/css/select2.css"/>
        <asset:stylesheet src="smart/plugins/select2/css/select2-bootstrap.min.css"/>
        <title>EduPlusCampus</title>
        <style>
            @media (max-width: 600px) {
                .btn {
                    width:100% !important;
                    text-align:center !important;
                    margin-top: 3px !important;
                    margin-bottom:3px !important;
                }
            }
            .btn {
                width:100% !important;
                text-align:center !important;
                margin-top: 3px !important;
                margin-bottom:3px !important;
            }
            span {
                display: inline-block;
                vertical-align: middle;
                margin-right: 20px;
            }
            img {
                border-radius: 8px;
            }
            h4 {
                color:#FFBE60 !important;
            }
        </style>
    </head>
    <body class="page-header-fixed page-full-width sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
        <div class="page-wrapper">
            <g:render template="/layouts/smart_template/header1" />
            <div class="page-container">
                <!-- start page content -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- add content here -->
                            <g:if test="${flash.message}">
                                <div class="alert alert-success" >${flash.message}</div>
                            </g:if>
                            <g:if test="${flash.error}">
                                <div class="alert alert-danger" >${flash.error}</div>
                            </g:if>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card card-topline-purple">
                                        <div class="card-body ">
                                            <g:form name="ApplicationForm" url="[controller:'Application',action:'saveEntranceApplication']" enctype="multipart/form-data">
                                                <h4 style="text-align:center; font-weight: bold; color:black !important;">Application Form</h4>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="row">
                                                            <div class="col-sm-6">
                                                                <span>Name : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.fullname}</span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <span>Email : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.email}</span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <span>Date Of Birth : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;"><g:formatDate date="${EA?.dateofbirth}" format="dd-MM-yyyy"/></span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <span>Mobile No. : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.mobilenumber}</span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <span>Gender : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.gender?.type}</span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <span>Marital Status : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.maritalstatus?.name}</span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <span>Nationality : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.erpnationality?.type}</span>
                                                            </div>
                                                            <div class="col-sm-6">
                                                                <span>Domacile : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.erpdomacile?.type}</span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <span>Local Address : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.local_address}</span>
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <span>Permanent Address : </span>
                                                                <span style="color:#00aeff; font-weight:bold; float:right;">${EA?.permanent_address}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <center>
                                                            <img style="height:140px;width:140px" src="${url}"/>
                                                        </center>
                                                    </div>
                                                </div>
                                                <hr>
                                                <h4 style="text-align:left; font-weight: bold;">Application Details </h4>
                                                <table class="mdl-data-table ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application Start Date</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application End Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <g:each in="${entranceVersion}" var="name" status="i">
                                                            <tr>
                                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${name?.academicyear?.ay}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${name?.entrance_name}</td>
                                                                <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="MMMM dd, yyyy" date="${name?.application_start_date}"/></td>
                                                                <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="MMMM dd, yyyy" date="${name?.application_end_date}"/></td>
                                                            </tr>
                                                            <input type="hidden" name="version" id="version" value="${name?.id}" />
                                                            <input type="hidden" name="programtype" id="programtype" value="${programtype}" />
                                                        </g:each>
                                                    </tbody>
                                                </table>
                                                <h4 style="text-align:left; font-weight: bold;">Application Fees Structure </h4>
                                                <table class="mdl-data-table ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Category Type</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exemption Type</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Entrance Examination Fees</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <g:each in="${entranceFees}" var="name" status="i">
                                                            <tr>
                                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${name?.entrancecategorytype?.name}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${name?.exemption?.name}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${name?.fees}</td>
                                                            </tr>
                                                        </g:each>
                                                    </tbody>
                                                </table>
                                                <h4 style="text-align:left; font-weight: bold;">Branch</h4>
                                                <div class="row">
                                                    <g:each in="${entranceBranch}" var="name" status="i">
                                                        <div class="col-sm-4">
                                                            <input type="checkbox" name="branch" value="${name?.id}">
                                                            <label for="pwd">${name?.name}</label>
                                                        </div>
                                                    </g:each>
                                                </div>
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-4 ">
                                                        <h4 style="text-align:left; font-weight: bold;">Category</h4>
                                                        <g:select name="entrancecategory" from="${entranceCategoryList}" style="width: 100% !important;" class="form-control select2" required="true"  value="${EA?.entrancecategory.id}" optionKey="id" optionValue="name" />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <h4 style="text-align:left; font-weight: bold;">Disability</h4>
                                                        <g:checkBox id="is_physically_handicapped" name="is_physically_handicapped" value="${EA?.is_physically_handicapped}"/>
                                                        <label>Are You Physically Handicapped?</label>
                                                        <div id="physical">
                                                            <input type="file" name="physical_file" id="physical_file" accept=".pdf" class="btn btn-primary" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <h4 style="text-align:left; font-weight: bold;"> <br></h4>
                                                        <g:checkBox id="is_visually_handicapped" name="is_visually_handicapped" value="${EA?.is_visually_handicapped}"/>
                                                        <label>Are You Visually Handicapped?</label>
                                                        <div id="visual">
                                                            <input type="file" name="visual_file" id="visual_file" accept=".pdf" class="btn btn-primary" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <hr>
                                                <!--
                                                <h4 style="text-align:left; font-weight: bold;">Eligibility</h4>
                                                <g:each in="${Eligibility}" var="elig" status="a">
                                                    <p>${a+1}.${elig?.name}</p>
                                                </g:each>
                                                <hr>
                                                -->
                                                <h4 style="text-align:left; font-weight: bold;">Exemption from Entrance Test/ Written Test</h4>
                                                <g:checkBox name="eligible" id="eligible" value="${false}" id="myCheck" onclick="myFunction()" />
                                                <label for="myCheck">Are you eligible for exemption from Entrance examination(Please attach valid proof)</label><br>
                                                <div id="text" style="text-align:left; display:none">
                                                    <table class="mdl-data-table ml-table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                <th class="mdl-data-table__cell--non-numeric">CheckBox</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Exemption Type</th>
                                                                <th class="mdl-data-table__cell--non-numeric">Upload Document</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <g:each in="${ExemptionType}" var="exemption" status="b">
                                                                <tr>
                                                                    <td class="mdl-data-table__cell--non-numeric">${b+1}</td>
                                                                    <td class="mdl-data-table__cell--non-numeric"><input type="checkbox" name="ExemptionType" value="${exemption?.id}"></td>
                                                                    <td class="mdl-data-table__cell--non-numeric">${exemption?.name}</td>                                                                    
                                                                    <td class="mdl-data-table__cell--non-numeric">
                                                                        <input type="file" name="${exemption?.abbr}" id="${exemption?.abbr}" accept=".pdf" class="btn btn-primary"/>
                                                                        <!--<span><input type="file" name="newFile" id="photo" accept=".pdf" class="btn btn-primary" onchange="loadFilephto(event);Upload()"/></span>
                                                                        <span><center><input type="submit" value="Upload" style="cursor: pointer;" class="btn-primary" ></center></span>
                                                                        <input type="hidden" name="exemption_type" id="exemption_type" value="${exemption?.id}"/>-->
                                                                    </td>
                                                                </tr>
                                                                <script type="text/javascript">
                                                                    function validateFileType(x){
                                                                        var fileName = x.value;
                                                                        var idxDot = fileName.lastIndexOf(".") + 1;
                                                                        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
                                                                        if (extFile=="pdf"){
                                                                            //TO DO
                                                                        }else{
                                                                            alert("Only pdf file are allowed..!");
                                                                            x.value=null;
                                                                        }
                                                                    }
                                                                </script>

                                                                <script>
                                                                var loadFilephto = function(event) {
                                                                    var output = document.getElementById('outputphoto');
                                                                    output.src = URL.createObjectURL(event.target.files[0]);
                                                                };
                                                                </script>
                                                            </g:each>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <script>
                                                    function myFunction() {
                                                    var checkBox = document.getElementById("myCheck");
                                                    var text = document.getElementById("text");
                                                    if (checkBox.checked == true){
                                                        text.style.display = "block";
                                                    } else {
                                                        text.style.display = "none";
                                                    }
                                                    }
                                                </script>
                                                <hr>
                                                <h4 style="text-align:left; font-weight: bold;">DECLARATION OF THE CANDIDATE</h4>
                                                <input type="checkbox" name="declare" required="true" value="${true}">
                                                &nbsp;&nbsp; I &nbsp;&nbsp;&nbsp;&nbsp;<span class="font-weight-bold">${EA?.fullname}</span> hereby declare that the information filled in profile by me is true and correct as per my knowledge and belief. I understand that if above information or attached document are found false, incorrect or incomplete, I can be debarred from admission proceedings at any stage or after the admission.I further understand that no notice period shall be taken on request for wthdrawal of my application.
                                                <hr>
                                                <div class="row">
                                                    <div class="col-sm-6">
                                                        <label for="pwd">Place <span style="color:red; font-size: 16px">*</span> :</label>
                                                        <input type="text" class="form-control" name="place" required="true" value="">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        <br>
                                                        <br>
                                                        <label style="float:right;">Date : <g:formatDate date="${new Date()}" type="datetime" style="MEDIUM"/></label><br>
                                                    </div>
                                                </div>
                                                <br>
                                                <div class="modal-footer">
                                                    <input type="hidden" name="ProgramType" id="ProgramType" value="${ProgramType?.id}" />
                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Apply</button>
                                                </div>
                                            </g:form>
                                        </div>
                                    </div>
                                </div>
                            </div>   
                        <!-- end content here -->
                    </div>
                </div>
                <!-- end page content -->
            </div>
            <!-- start footer -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2020 &copy; EduPlusCampus By
                    <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- end footer -->
        </div>
        <g:render template="/layouts/smart_template/js" />
        <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
            <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
            <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>
        <!--select2-->
        <asset:javascript src="smart/plugins/select2/js/select2.js"/>
        <asset:javascript src="smart/pages/select2/select2-init.js"/>
    </body>
</html>
