<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template/meta" />
        <g:render template="/layouts/smart_template/css" />
        <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <title>EduPlusCampus</title>
        <style>
            @media (max-width: 600px) {
                .btn {
                    width:100% !important;
                    text-align:center !important;
                    margin-top: 3px !important;
                    margin-bottom:3px !important;
                }
            }
            .btn {
                width:100% !important;
                text-align:center !important;
                margin-top: 3px !important;
                margin-bottom:3px !important;
            }
        </style>
    </head>
    <body class="page-header-fixed page-full-width sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
        <div class="page-wrapper">
            <g:render template="/layouts/smart_template/header1" />
            <div class="page-container">
                <!-- start page content -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- add content here -->
                            <div class="row">
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-primary">Applicant Profile</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceApplication" class="btn btn-primary">Application Form</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-primary">Application Form Fees</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="trackEntranceApplication" class="btn btn-light">Application Tracking</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceExamination" class="btn btn-primary">Entrance Exam</g:link>
                                </div>
                            </div>

                            <br>

                            <g:if test="${flash.error}">
                                <div class="alert alert-danger" >${flash.error}</div>
                            </g:if>
                            <g:if test="${flash.success}">
                                <div class="alert alert-success" >${flash.success}</div>
                            </g:if>
                            <g:if test="${flash.message}">
                                <div class="alert alert-success" >${flash.message}</div>
                            </g:if>

                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card card-topline-purple">
                                        <g:if test="${receipt?.organization?.organization_code == "VIT"}">
                                            <div class="" id="bar-parent">
                                                <div style="position: relative;" class="printHeader">
                                                    <!-- Start of printHeader ---------------------------- -->
                                                    <div style="position: absolute;left: 0">
                                                        <img src="https://www.vierp.in/logo/vit_logo.png" width="80">
                                                    </div>
                                                    <div style="text-align: center; color: blue">
                                                        <p>Bansilal Ramnath Agarwal Charitable Trust's
                                                            <br>
                                                            <span style="text-transform: uppercase;"> Vishwakarma Institute of Technology, Pune </span>
                                                            <br>
                                                            <span style="font-size: 12px; font-style: italic;">(An Autonomous Institute Affiliated to Savitribai Phule
                                                                Pune University)</span> <span style="position: absolute; left: 85%"> </span>
                                                            <br> 666, Upper Indira Nagar, Bibwewadi, Pune-411037
                                                            <br>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </g:if>
                                        <g:if test="${receipt?.organization?.organization_code == "VIIT"}">
                                            <div style="position: relative;" class="printHeader">
                                                <!-- Start of printHeader ---------------------------- -->
                                                <div style="position: absolute;left: 0">
                                                    <img src="https://www.vierp.in/logo/vit_logo.png" width="80">
                                                </div>
                                                <div style="text-align: center; color: blue">
                                                    <p>BRACT's
                                                        <br>
                                                        <b style="text-transform: uppercase;">Vishwakarma Institute of Information Technology, Pune</b>
                                                        <br>
                                                        <span style="font-size: 13px">(An Autonomous Institute Affiliated to Savitribai Phule Pune University)<br>
                                                            (Approved by A.I.C.T.E. New Delhi)<br>
                                                            NAAC Accredited With 'A' Grade,An ISO 9001:2015 Certified Institute<br>
                                                            S.No.3/4,Kondhwa Bk.,Pune-411048.MAHARASHTRA,INDIA<br>
                                                            E-mail:director@viit.ac.in Website:www.viit.ac.in<br>
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                        </g:if>
                                        <hr style="border: 1px solid;">
                                        <center>
                                            <div style="position:relative; width:80%; clear: both; ">
                                                <div style="float:left; width:33%; display:inline-block;"><b> Receipt No. :&nbsp;</b>${receipt?.feesreceiptid}</div>
                                                <div style="float:right; width:33%; display:inline-block;"><b> Receipt Date. :&nbsp;</b><g:formatDate format="MMMM dd, yyyy" date="${receipt?.receiptdate}"/></div>
                                            </div>
                                        </center>
                                        <br>
                                        <br>
                                        <center>
                                            <span>Received with thanks fees of Entrance Admission from</span>
                                            <br>
                                            <span>Full Name : <b>${receipt?.entranceapplicant?.fullname}</b>,</span>
                                            <br>
                                            <span>Application Number : <b>${receipt?.entranceonlinetransaction?.entranceapplication?.applicaitionid}</b></span>
                                            <br>
                                            <span>Under ${receipt?.entranceapplicant?.entrancecategory?.name} as per following details : </span>
                                        </cneter>
                                        <br>
                                        <br>
                                        <center>
                                            <table style="width: 80%; border:1px solid black !important;">
                                                <tr>
                                                    <th align="left" style="border:1px solid black !important;">
                                                        <center>Sr.No</center>
                                                    </th>
                                                    <th style="border:1px solid black !important;"> <center>Particulars of the Fees</center></th>
                                                    <th align="left" style="border:1px solid black !important;"> <center>Amount (<i class="fa fa-inr" style="font-size: 12px"></i>)</center></th>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="border:1px solid black !important;">
                                                        <center>1</center>
                                                    </td>
                                                    <td style="border:1px solid black !important;"> <center>Entrance Admission Fees</center></td>
                                                    <td align="left" style="border:1px solid black !important;"> <center>${receipt?.amount}</center></td>
                                                </tr>
                                            </table>
                                        </center>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <br>
                                        <div style="width:80%;">
                                            <div style="float:left; display:inline-block;margin-left: 40px;">
                                                <g:if test="${stamp}">
                                                    <img style="width:150px;" src="${stamp}" />
                                                    <br>
                                                </g:if>
                                                <b>Seal of College</b>
                                            </div>
                                            <div style="float:right; display:inline-block;">
                                               <g:if test="${sign}">
                                                   <br><br>
                                                   <img style="width:200px;" src="${sign}" />
                                                   <br>
                                               </g:if>
                                               <b>Signature of Receiver</b>
                                            </div>
                                        </div>
                                        <br>
                                        <br>
                                    </div>
                                </div>
                            </div>
                        <!-- end content here -->
                    </div>
                </div>
                <!-- end page content -->
            </div>
            <!-- start footer -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2020 &copy; EduPlusCampus By
                    <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- end footer -->
        </div>
        <g:render template="/layouts/smart_template/js" />
        <g:render template="/layouts/smart_template_inst/datatable_js" />
        <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
            <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
            <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>
    </body>
</html>
