<head>
    <meta name="layout" content="ERPmainTemplateT2">


    <style>
           #spinner {
             position: fixed;
             top: 50%;
             left: 50%;
             margin-left: -50px; // half width of the spinner gif
             margin-top: -50px; // half height of the spinner gif
             z-index: 5000;
             overflow: auto;
           }
           .loader {
             border: 10px solid #f3f3f3;
             border-radius: 50%;
             //border-top: 16px solid #333;


              border-top: 6px solid #80BD9E;
              border-right: 6px solid #89DA59;
              border-bottom: 6px solid #FF420E;
              border-left: 6px solid #F98866;


             width: 45px;
             height: 45px;
             -webkit-animation: spin 2s linear infinite; /* Safari */
             animation: spin 2s linear infinite;
           }

           /* Safari */
           @-webkit-keyframes spin {
             0% { -webkit-transform: rotate(0deg); }
             100% { -webkit-transform: rotate(360deg); }
           }

           @keyframes spin {
             0% { transform: rotate(0deg); }
             100% { transform: rotate(360deg); }
           }
           </style>
</head>

<!-- Page Content  Copy Coding use Only-------------------------------------->
<div id="page-wrapper">
    <div class="container-fluid">
        <g:render template="/layouts/home_back_btn_T2"/>

        <div class="row">
            <div class="col-lg-12">

                <h3 class="page-header" style="text-align: center;">Links</h3>

                 <!-- Start coding only in This block ------------------->

                 <div id="update"></div>
                 <table class="table table-striped table-bordered"  >
                 <thead>
                     <tr class="alert-dark" >
                          <th>Registrar</th>

                     </tr>
                 </thead>
                     <tbody >

                           <g:each var="rolelink" in="${registrarroleLinks}">
                            <tr >
                                  <td>
                                       <g:link  action="${rolelink.action_name}"
                                                                          controller="${rolelink.controller_name}" >
                                                                                  <label style="background: white;opacity: 0.9; color:blue;">
                                                                                  ${rolelink.link_name}
                                                                                  </label>
                                                                          </g:link>
                                  </td>

                              </tr>
                          </g:each >

                      </tbody>
                  </table>

                   <table class="table table-striped table-bordered"  >
                       <thead>
                           <tr class="alert-dark" >
                                <th>Establishment Section </th>

                           </tr>
                       </thead>
                           <tbody >

                                 <g:each var="rolelink" in="${estroleLinks}">
                                  <tr >
                                        <td>
                                             <g:link  action="${rolelink.action_name}"
                                                                                controller="${rolelink.controller_name}" >
                                                                                        <label style="background: white;opacity: 0.9; color:blue;">
                                                                                        ${rolelink.link_name}
                                                                                        </label>
                                                                                </g:link>
                                        </td>

                                    </tr>
                                </g:each >

                            </tbody>
                        </table>

                  <table class="table table-striped table-bordered"  >
                   <thead>
                       <tr class="alert-dark" >
                            <th>HOD</th>

                       </tr>
                   </thead>
                       <tbody >

                             <g:each var="rolelink" in="${hodroleLinks}">
                              <tr >
                                    <td>
                                         <g:link  action="${rolelink.action_name}"
                                                                            controller="${rolelink.controller_name}" >
                                                                                    <label style="background: white;opacity: 0.9; color:blue;">
                                                                                    ${rolelink.link_name}
                                                                                    </label>
                                                                            </g:link>
                                    </td>

                                </tr>
                            </g:each >

                        </tbody>
                    </table>


                    <table class="table table-striped table-bordered"  >
                   <thead>
                       <tr class="alert-dark" >
                            <th>SELF</th>
                       </tr>
                   </thead>
                       <tbody >
                             <g:each var="rolelink" in="${selfroleLinks}">
                              <tr >
                                    <td>
                                         <g:link  action="${rolelink.action_name}"
                                            controller="${rolelink.controller_name}" >
                                                    <label style="background: white;opacity: 0.9; color:blue;">
                                                    ${rolelink.link_name}
                                                    </label>
                                            </g:link>
                                    </td>


                                </tr>
                            </g:each >

                        </tbody>
                    </table>
            </div>
            <!-- /.col-lg-12 -->
        </div> <!-- /.row -->


    </div> <!-- /.container-fluid -->
<script>
function changeLeaveMaster(val)
{
    alert("ChangeLeaveMaster"+val);
}
</script>


</div>
<!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->

 <script>

function showSpinner() {
               document.getElementById('spinner').style.display = 'inline';
               document.getElementById('error').style.display = 'none';
           }

           function hideSpinner() {
               document.getElementById('spinner').style.display = 'none';
               document.getElementById('error').style.display = 'none';
           }

           function showError(e) {
               var errorDiv = document.getElementById('error')
               errorDiv.innerHTML = '<ul><li>'
                        + e.responseText + '</li></ul>';
               errorDiv.style.display = 'block';
           }

   </script>
