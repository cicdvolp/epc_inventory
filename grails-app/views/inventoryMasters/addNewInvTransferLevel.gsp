<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <!-- <div class="row">
                      <div class="col-sm-12">
                             <div class="card">
                                   <div class="card-body">
                                         <div class="modal-header">
                                              <h4 class="modal-title">Add New Inventory transfer Level</h4>
                                        </div>
                                           <g:form action="saveInvTransferLevel">
                                          <div class="row">
                                          <div class="col-sm-4">
                                                <label for="pwd">Inventory Transfer Level:</label>
                                                <input type="text" class="form-control" name="transferLevelName" required="true">
                                         </div>
                                         <div class="col-sm-4">
                                         <br>
                                          <label for="pwd">Is-Active?</label>
                                             <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/>
                                      </div>
                                   </div>
                                   <div class="row">
                                      <div class="col-lg-12">
                                           <center>
                                               <br>
                                                 <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </center>
                                       </div>
                                   </div>
                                           </g:form>
                                   </div>
                             </div>
                      </div>
                </div>-->

                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">

                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Inventory Transfer Level List</header>
                                        <div class="tools">
                                            <g:link  data-toggle="modal" data-target="#addNewInvTransferLevel"><span aria-hidden="true" class="icon-plus" title="Add New Inventory transfer Level" style="float:right; font-size:20px;"></span></g:link>
                                        </div>
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Inv. Transfer Level</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invTransferLevelList}" var="TL" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${TL?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="activateInvTransferLevel" params="[invTransferLevelId : TL?.id]">
                                                                <g:if test="${TL?.isactive}">
                                                                    <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                                </g:if>
                                                                <g:else>
                                                                    <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                                </g:else>
                                                            </g:link>

                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editInvTransferLevel${i}"></i>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="deleteInvTransferLevel">
                                                                  <input type="hidden" value="${TL?.id}" name="transferLevelId">
                                                                        <button  data-toggle="modal" data-target="#${i}myModal" class="fabutton" type="submit" onclick="return confirm('Do you want to Delete ?')" ><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></button>
                                                            </g:form>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="editInvTransferLevel${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">

                                                              <h4 class="modal-title">Edit Inventory Transfer Level</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="editInvTransferLevel">
                                                                <div class="modal-body">
                                                                      <div class="form-group">
                                                                          <label for="pwd">Inventory transfer Level:</label>
                                                                          <input type="hidden" value="${TL?.id}" name="transferLevelId">
                                                                          <input type="text" class="form-control" name="transferLevelName" required="true" value="${TL?.name}">
                                                                      </div>
                                                                </div>
                                                                <div class="modal-footer">
                                                                  <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </g:each>
                                                </tbody>
                                               </table>

                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                                <!-- Modal New -->
                                  <div class="modal fade" id="addNewInvTransferLevel" role="dialog">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h4 class="modal-title">Add New Inventory Transfer Level</h4>
                                          <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                       <g:form action="saveInvTransferLevel">
                                        <div class="modal-body">
                                          <div class="form-group">
                                              <label for="pwd">Transfer Level Name:</label>
                                              <input type="text" class="form-control" name="transferLevelName" required="true">
                                          </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                        </div>
                                       </g:form>
      </div>
      </div>
      </div>
      </div>
      </div>
    </div>
  </div>
  </div>
</body>
</html>



