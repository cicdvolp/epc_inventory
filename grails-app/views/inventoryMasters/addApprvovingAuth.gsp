<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>

     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Instuctor Approving Authority</header>
                                </div>
                                <div class="card-body " id="bar-parent">
                                    <g:form controller="inventoryMasters" action="saveApprvovingAuth" name="form">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="pwd">Instructor Name</label>
                                                <g:select name="instructorId"   from="${invInstructorList}"
                                                 class="select2 form-control" required="true"
                                                optionValue="${instructor?.person?.fullname_as_per_previous_marksheet}"
                                                optionKey="id"
                                                noSelection="['null':'Select Instructor']" />
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="pwd">Finance Approving Authority</label><br>
                                                <g:each in="${invFinanceApprovingAuthorityList}" var="authority" status="i">
                                                    <g:checkBox style="margin-left:15px" name="invFinanceApprovingAuthority" value="${authority.id}"/>
                                                    &nbsp;&nbsp;
                                                    <label>${authority}</label>
                                                    &nbsp;&nbsp;
                                                    &nbsp;&nbsp;
                                                    &nbsp;&nbsp;
                                                </g:each>
                                            </div>
                                            <div class="col-sm-12">
                                                <center>
                                                    <br>
                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span>Add</button>
                                                </center>
                                            </div>
                                        </div>
                                    </g:form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-purple">
                                              <div class="table-responsive">
                                                 <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                   <thead>
                                                    <tr>
                                                        <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Name</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Authority</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        <g:each in="${invinstructorfinanceapprovingauthoritylist}" var="instructor" status="i">
                                                            <tr>
                                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${instructor?.instructor?.employee_code} ${instructor?.instructor?.person?.fullname_as_per_previous_marksheet}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">
                                                                ${instructor?.invfinanceapprovingauthority}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">
                                                                    <g:form action="deleApprvovingAuth">
                                                                          <input type="hidden" value="${instructor?.id}" name="instructorId">
                                                                                <button  data-toggle="modal"  class="fabutton" type="submit" onclick="return confirm('Do you want to Delete ?')" ><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></button>
                                                                    </g:form>
                                                                </td>
                                                            </tr>

                                                    </g:each>
                                                    </tbody>
                                                   </table>

          </div>
          </div>
          </div>
          </div>
          </div>

      </body>
      </html>
