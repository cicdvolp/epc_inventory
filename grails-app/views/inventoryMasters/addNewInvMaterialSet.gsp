<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
          <div class="page-content">
            <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!--Message and error-->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--////-->
                  
                  <!--add form-->
                <!--<div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Add a material set</header>
                                    </div>
                                    <div class="card-body" id="bar-parent">
                                          <g:form action="saveInvMaterialSet">
                                          <div class="row">
                                            <div class="form-group row mx-auto">
                                              <label>Material Set:</label>&nbsp;&nbsp;
                                              <input placeholder="e.g:-Composite,Single Unit" type="text" class="form-control" name="materialSetName" required="true">
                                             </div>
                                          </div>
                                          <div class="row">
                                            <div class="form-group row mx-auto">
                                              <input type="checkbox" id="isactive" class="css-checkbox my-2" name="isactive" checked/> &nbsp;Is Active?
                                           </div>
                                          </div>
                                          <div class="row">
                                             <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto"> <span class="glyphicon glyphicon-save"></span> Save</button>
                                          </div>
                                          </g:form>
                                    </div>
                        </div>
                  </div>
                </div>-->
                
                  <!--////-->

                <!--Table,edit,delete-->
                <div class="row">
                    <div class="col-md-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Material Set List</header>
                                        <div class="tools">
                                            <g:link  data-toggle="modal" data-target="#addNewInvMaterialSet"><span aria-hidden="true" class="icon-plus" title="Add New Material Set" style="float:right; font-size:20px;"></span></g:link>
                                        </div>
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material Set</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invMaterialSetList}" var="materialSet" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${materialSet?.name}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                               <g:link controller="inventoryMasters" action="activateInvMaterialSet" params="[invMaterialSetParam : materialSet?.id]">
                                                                        <g:if test="${materialSet?.isactive}">
                                                                             <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                                         </g:if>
                                                                       <g:else>
                                                                           <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                                       </g:else>
                                                                   </g:link>
                                                               </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editInvMaterialSet${i}"></i>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="deleteInvMaterialSet">
                                                                  <input type="hidden" value="${materialSet?.id}" name="materialSetId">
                                                                        <button  data-toggle="modal" data-target="#${i}myModal" class="fabutton" type="submit" onclick="return confirm('Do you want to Delete ?')" ><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></button>
                                                            </g:form>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="editInvMaterialSet${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">

                                                              <h4 class="modal-title">Edit Material Set</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="editInvMaterialSet">
                                                            <div class="modal-body">
                                                              <div class="form-group">
                                                                  <label for="pwd">Material Set:</label>
                                                                  <input type="hidden" value="${materialSet?.id}" name="materialSetId">
                                                                  <input type="text" class="form-control" name="materialSetName" required="true" value="${materialSet?.name}">
                                                              </div>
                                                              <!--
                                                              <div class="form-group">
                                                                  <g:if test="${materialSet?.isactive}">
                                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                   </g:if>
                                                                    <g:else>
                                                                          <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                    </g:else>
                                                                   </div>
                                                                   -->
                                                            </div>
                                                            <div class="modal-footer">
                                                              <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                            </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </g:each>
                                                </tbody>
                                               </table>

                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                                <!-- Modal New -->
                                  <div class="modal fade" id="addNewInvMaterialSet" role="dialog">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h4 class="modal-title">Add New Material Set</h4>
                                          <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                       <g:form action="saveInvMaterialSet">
                                        <div class="modal-body">
                                          <div class="form-group">
                                              <label for="pwd">Material Set:</label>
                                              <input type="text" class="form-control" name="materialSetName" required="true">
                                          </div>
                                          <div class="form-group">
                                              <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked/> &nbsp; &nbsp; &nbsp; Is Active?
                                           </div>
                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                        </div>
                                       </g:form>
      </div>
      </div>
      </div>
      </div>
      </div>
    </div>
  </div>
  </div>
</body>
</html>



