<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                    <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                    </g:if>
                <div class="row">
                    <div class="col-sm-12 mobile-table-responsive">
                        <div class="card card-topline-purple full-scroll-table">
                            <div class="card-head">
                                <header class="erp-table-header">Depreciation Percentage</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add Depreciation Percentage" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Percentage</th>
                                                <th class="mdl-data-table__cell--non-numeric">Entry_date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material Part</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material Set</th>
                                                <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                <th class="mdl-data-table__cell--non-numeric">Entry By</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active </th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           <g:each in="${inventoryDepreciationPercentage}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.percentage}</td>
                                                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${name?.entry_date}"/></td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invmaterial?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invmaterialpart.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invmaterial?.invmaterialset?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.academicyear}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entryby?.person?.fullname_as_per_previous_marksheet}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="activationinvdepreciationpercentage" params="[inventoryDepreciationPercentage : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                        <!-- Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Depreciation percentage</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editinvdepreciationpercentage">
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                 <label for="pwd">Percentage:</label><br>
                                                                                 <g:field type="number" id="percentage" name="percentage"  required="true" value="${name?.percentage}" style="width: 100% !important;"/>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Material:</label><br>
                                                                                <g:select name="invmaterial"  optionValue="${invmaterial}" from="${invmaterialNameList}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invmaterial?.name}"  noSelection="['null':'Select Material ']" /><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Material Part:</label><br>
                                                                                <g:select name="invmaterialpart"  optionValue="${invmaterialpart}" from="${invmaterialpartNameList}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invmaterialpart.name}"  noSelection="['null':'Select Material Part']"/><br>
                                                                            </div>
                                                                             <div class="form-group">
                                                                                <label for="pwd">Academic Year:</label><br>
                                                                                <g:select name="academicYearId"  optionValue="ay" from="${academicYearList}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.academicyear?.id}"  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="inventorydepreciationpercentage" id="inventorydepreciationpercentage" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End Modal edit -->
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="deleinvdepreciationpercentage" params="[inventoryDepreciationPercentage : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>
                                                </tr>
                                           </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Depreciation Percentage</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveinvdepreciationpercentage">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Percentage:</label><br>
                                                    <g:field type="number" id="percentage" name="percentage"  required="true" value="" style="width: 100% !important;"/>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Material:</label><br>
                                                    <g:select name="invmaterial"  optionValue="${invmaterial}" from="${invmaterialNameList}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Material ']" /><br>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Material Part:</label><br>
                                                    <g:select name="invmaterialpart"  optionValue="${invmaterialpart}" from="${invmaterialpartNameList}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Material Part']"/><br>
                                                </div>
                                                 <div class="form-group">
                                                    <label for="pwd">Academic Year:</label><br>
                                                    <g:select name="academicYearId"  optionValue="ay" from="${academicYearList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




