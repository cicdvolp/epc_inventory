<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                
                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Add Form-->
                 <!--<div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Add a Budget Type</header>
                                   </div>
                                   <div class="card-body" id="bar-parent">
                                        <g:form action="saveBudgetType">
                                        <div class="row">
                                            <div class="form-group mx-auto row">
                                                <label class="my-2">Budget Type Name :</label>&nbsp
                                                <input placeholder="e.g:-Regular,Activity" class="form-control col-md-6" type="text" name="budgetTypeName" required="true">
                                            </div>
                                        </div>
                                        <div class="row">
                                             <div class="form-group mx-auto">
                                                <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp;Is Active?
                                             </div>
                                        </div>  
                                         <div class="row mx-auto">  
                                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto">Save</button>     
                                         </div>      
                                       </g:form>
                                        
                                    </div>
                        </div>
                  </div>
                </div>-->
                <!--////-->
                <!--Table,Edit,Delete-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Budget Type List</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Budget Level" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active </th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${budgettypelist}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.name}</td>

                                                   <td class="mdl-data-table__cell--non-numeric">
                                                      <g:link controller="inventoryMasters" action="activateBudgetType" params="[budgetType : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                   <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                         <!--Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Budget Type</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editBudgetType">
                                                                        <div class="modal-body">
                                                                            <div class="form-group row mx-2">
                                                                                <label class="my-2">Budget Type name :</label><br>&nbsp;&nbsp;
                                                                                <input type="hidden" value="${name?.id}" name="budgetType">
                                                                             <input type="text" class="form-control col-sm-7" name="budgetTypeName" required="true" value="${name?.name}">
                                                                            </div>
                                                                            <!--
                                                                            <div class="form-group row mx-2">
                                                                                <g:if test="${name?.isactive}">
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox my-2" name="isactive" checked="true"/> &nbsp; &nbsp;Is Active?
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox my-2" name="isactive"/> &nbsp; &nbsp;Is Active?
                                                                                </g:else>
                                                                            </div>
                                                                            -->
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="budgetType" id="budgetType" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary mx-auto" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="deleteBudgetType" params="[budgetType : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>
                                                    
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                         <!---- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Budget Type</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveBudgetType">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Budget Type Name :</label>
                                                     <input class="form-control" type="text" name="budgetTypeName">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




