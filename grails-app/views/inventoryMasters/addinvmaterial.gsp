<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="pratik"/>
         <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Add New Material</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Add New Material</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Material List</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Material" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Specification</th>
                                                <th class="mdl-data-table__cell--non-numeric">Code</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material Category</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material Set</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active </th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${invmaterial}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.specification}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.code}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invmaterialtype?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invmaterialcategory?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invmaterialset?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="activationinvmaterial" params="[invMaterial : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                        <!-- Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Material</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="saveinvmaterial">
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="pwd">Name <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                                                <input type="text" class="form-control" name="name" required="true" value="${name?.name}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Specification <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                                                <input type="text" class="form-control" name="spec" required="true" value="${name?.specification}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Code <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                                                <input type="text" class="form-control" name="code" required="true" value="${name?.code}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Material Type <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                                                <g:select name="invmaterialtype"  optionValue="name" from="${invmaterialtype}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invmaterialtype?.id}"  noSelection="['null':'Select Material Type']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Material Category <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                                                <g:select name="invmaterialcategory"  optionValue="name" from="${invmaterialcategory}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invmaterialcategory?.id}"  noSelection="['null':'Select Material Category']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Material Set <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                                                <g:select name="invmaterialset"  optionValue="name" from="${invmaterialset}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invmaterialset?.id}"  noSelection="['null':'Select Material Set']" optionKey="id"/><br>
                                                                            </div>
                                                                            <!--
                                                                            <div class="form-group">
                                                                                <g:if test="${name?.isactive}">
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                </g:else>
                                                                            </div>
                                                                            -->
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="invmaterial" id="invmaterial" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--End Modal edit -->

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="deleinvmaterial" params="[invMaterial : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            <!-- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Material</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveinvmaterial">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Name <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                    <input type="text" class="form-control" name="name" required="true" value="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Specification <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                    <input type="text" class="form-control" name="spec" required="true" value="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Code <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                    <input type="text" class="form-control" name="code" required="true" value="">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Material Type <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                    <g:select name="invmaterialtype"  optionValue="name" from="${invmaterialtype}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Material Type']" optionKey="id"/><br>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Material Category <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                    <g:select name="invmaterialcategory"  optionValue="name" from="${invmaterialcategory}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Material Category']" optionKey="id"/><br>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Material Set <span style="color:red; font-size: 16px">*</span>:</label><br>
                                                    <g:select name="invmaterialset"  optionValue="name" from="${invmaterialset}" style="width: 100% !important;"  class="select2" required="true"  value=" "  noSelection="['null':'Select Material Set']" optionKey="id"/><br>
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




