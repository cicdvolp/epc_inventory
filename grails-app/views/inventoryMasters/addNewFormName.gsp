<!doctype html>
<html>
<head>
    <meta name="layout" content="smart_main_datatable_inst"/>
    <meta name="author" content="rishabh"/>

    <style>
    .fabutton {
        background: none;
        padding: 0px;
        border: none;
    }

    </style>
</head>
<body>
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <g:render template="/layouts/smart_template_inst/breadcrumb" />
    <!-- add content here -->
    <!-- <div class="row">
                      <div class="col-sm-12">
                             <div class="card">
                                   <div class="card-body">
                                         <div class="modal-header">
                                              <h4 class="modal-title">Add New Budget Level</h4>
                                        </div>
                                           <g:form action="saveFormName">
        <div class="row">
        <div class="col-sm-4">
              <label for="pwd">Budget Level:</label>
              <input type="text" class="form-control" name="budgetLevelName" required="true">
       </div>
       <div class="col-sm-4">
       <br>
        <label for="pwd">Is-Active?</label>
           <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/>
    </div>
 </div>
 <div class="row">
    <div class="col-lg-12">
         <center>
             <br>
               <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
          </center>
     </div>
 </div>
    </g:form>
                                   </div>
                             </div>
                      </div>
                </div>-->

        <g:if test="${flash.message}">
            <div class="alert alert-success" >${flash.message}</div>
        </g:if>
        <g:if test="${flash.error}">
            <div class="alert alert-danger" >${flash.error}</div>
        </g:if>
        <div class="row">
            <div class="col-md-12">

                <div class="card card-topline-purple">
                    <div class="card-head">
                        <header class="erp-table-header">Form Name List</header>
                        <div class="tools">
                            <g:link  data-toggle="modal" data-target="#addNewFormName"><span aria-hidden="true" class="icon-plus" title="Add New Budget Level" style="float:right; font-size:20px;"></span></g:link>
                        </div>
                    </div>
                    <div class="card-body " id="bar-parent">
                        <div class="table-responsive">
                            <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                <thead>
                                <tr>
                                    <th class="mdl-data-table__cell--non-numeric">Form No.</th>
                                    <th class="mdl-data-table__cell--non-numeric">Form Name</th>
                                    <th class="mdl-data-table__cell--non-numeric">Is-active?</th>
                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                    <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${formNameList}" var="form" status="i">
                                    <tr>
                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                        <td class="mdl-data-table__cell--non-numeric">${form?.name}</td>
                                        <td class="mdl-data-table__cell--non-numeric">
                                            <g:link controller="inventoryMasters" action="activateFormName" params="[formNameId : form?.id]">
                                                <g:if test="${form?.isActive}">
                                                    <!--<i class="fa fa-toggle-off fa-2x" style="color:green" aria-hidden="true"></i>-->
                                                   <center><i class="fa fa-toggle-on fa-2x" style="color:Green" title="active" aria-hidden="true"></i></center>
                                                </g:if>
                                                <g:else>
                                                    <!--<i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>-->
                                                    <center><i class="fa fa-toggle-off fa-2x" style="color:Red" title="Inactive" aria-hidden="true"></i></center>
                                                </g:else>
                                            </g:link>

                                        </td>
                                        <td class="mdl-data-table__cell--non-numeric">
                                            <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editFormName${i}"></i>
                                        </td>
                                        <td class="mdl-data-table__cell--non-numeric">
                                            <g:form action="deleteFormName">
                                                <input type="hidden" value="${form?.id}" name="deleteform">
                                                <button  data-toggle="modal" data-target="#${i}myModal" class="fabutton" type="submit" onclick="return confirm('Do you want to Delete ?')" ><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></button>
                                            </g:form>
                                        </td>
                                    </tr>
                                    <!-- Modal edit -->
                                      <div class="modal fade" id="editFormName${i}" role="dialog">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">

                                                <h4 class="modal-title">Edit Form Name</h4>
                                                <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                            </div>
                                            <g:form action="editFormName">
                                                <div class="modal-body">
                                                  <div class="form-group">
                                                      <label for="pwd">Enter Form Name:</label>
                                                      <input type="hidden" value="${form?.id}" name="editform">
                                                <input type="text" class="form-control" name="saveForm" required="true" value="${form?.name}">
                                                </div>
                                                <!--
                                                <div class="form-group">
                                                <g:if test="${form?.isActive}">
                                                    <input type="checkbox" id="isActive" class="css-checkbox" name="isActive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </g:if>
                                                <g:else>
                                                    <input type="checkbox" id="isActive" class="css-checkbox" name="isActive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </g:else>
                                                </div>
                                            </div>
                                            -->
                                                <div class="modal-footer">
                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                </div>
                                            </g:form>
                                        </div>
                                        </div>
                                    </div>
                                </g:each>
                                </tbody>
                            </table>

                            <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                            <!-- Modal New -->
                            <div class="modal fade" id="addNewFormName" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Form Name</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveFormName">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Enter Form Name:</label>
                                                    <input type="text" class="form-control" name="saveForm" required="true">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>



