<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
            <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class="pull-left">
                            <div class="page-title">Finance Approving Category</div>
                        </div>
                      
                            <ol class="breadcrumb page-breadcrumb pull-right">
                             <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>

                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Finance Approving Category</li>
                            </ol>
                     
                   
                        </div>
                        </div>
               
                <!-- MEssage and error -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--//-->

                <!--Add form
                 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Add a finance approving category</header>
                                   </div>
                                   <div class="card-body" id="bar-parent">
                                        <g:form action="saveInventoryFinanceApprovingCategoy">
                                            <div class="row">
                                                <div class="form-group row mx-auto">
                                                    <label for="pwd"> Category Name :</label>
                                                     <input placeholder="e.g:- Budget,Purchase Request,PO" class="form-control" type="text" required="true" name="invfinanceapprovingcatname">
                                                </div>
                                            </div>
                                            <div class="row">
                                                    <div class="form-group row mx-auto">
                                                    <input type="checkbox" id="isactive" class="css-checkbox my-2" name="isactive"/> &nbsp;Is Active?
                                                </div>
                                            
                                            </div>
                                            <div class="row">
                                                 <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto"> <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </div>
                                        </g:form>
                                        
                                    </div>
                        </div>
                  </div>
                </div>-->

                <!--///-->
                <!--Table,edit,delete-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Finance Approving Category List</header>
                                 <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Budget Level" style="float:right; font-size:20px;"></span></g:link>
                                 </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active </th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${invFinanceApprovingCategory}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric"><!--${name?.isactive}-->


                                                      <g:link controller="inventoryMasters" action="activateInventoryFinanceApprovingCategoy" params="[invfinanceapprovingcat : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true" checked="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>

                                                    </td>
                                                    
                                                   <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                         <!--Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Inventory Finance Approving Category</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editInventoryFinanceApprovingCategoy">
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="pwd"> Category name :</label><br>
                                                                                <input type="hidden" value="${name?.id}" name="invfinanceapprovingcat" />
                                                                             <input type="text" class="form-control" name="invfinanceapprovingcatname" required="true" value="${name?.name}" />
                                                                            </div>
                                                                            
                                                                            </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="invfinanceapprovingcat" id="invfinanceapprovingcat" value="${name?.id}" />
                                                                            <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto"> <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="deleteInventoryFinanceApprovingCategoy" params="[invfinanceapprovingcat : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <!---- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Finance Approving Category</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveInventoryFinanceApprovingCategoy">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd"> Category Name :</label>
                                                     <input class="form-control" type="text" required="true" name="invfinanceapprovingcatname">
                                                </div>
                                                
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




