<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="pratik"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">List</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add Academic Year" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Finance Approving Authority</th>
                                                <th class="mdl-data-table__cell--non-numeric">Instructor</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active </th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${invinstructorfinanceapprovingauthoritylist}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.invfinanceapprovingauthority}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.instructor?.employee_code} : ${name?.instructor?.person?.fullname_as_per_previous_marksheet}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="activationinvinstructorfinanceapprovingauthority" params="[instfinanceapprovingauthority : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                        <!-- Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Instructor Finance Authority</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="saveinvinstructorfinanceapprovingauthority">
                                                                        <div class="modal-body">
                                                                            <div class="form-group">
                                                                                <label for="pwd">Finance Approving Authority :</label><br>
                                                                                <g:select name="financeapprovingauthority"  optionValue="name" from="${financeauthority_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.invfinanceapprovingauthority?.id}"  noSelection="['null':'Select Finance Approving Athority']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Instructor :</label><br>
                                                                                <g:select name="instructor"  optionValue="employee_code" from="${inst_list}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.instructor?.id}"  noSelection="['null':'Select Instructor']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <g:if test="${name?.isactive}">
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                </g:else>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="instfinanceapprovingauthority" id="instfinanceapprovingauthority" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="inventoryMasters" action="deleteinvinstructorfinanceapprovingauthority" params="[instfinanceapprovingauthority : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>

                                                        <!--
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editAcademicYear${i}"></i>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:form action="deleteAcademicYear">
                                                              <input type="hidden" value="${name?.id}" name="nameid">
                                                              <button  data-toggle="modal" data-target="#${i}myModal" class="fabutton" type="submit" onclick="return confirm('Do you want to Delete ?')" ><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></button>
                                                        </g:form>
                                                    </td>
                                                    -->
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            <!-- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Instructor Finance Authority</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveinvinstructorfinanceapprovingauthority">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Finance Approving Authority :</label>
                                                    <g:select name="financeapprovingauthority"  optionValue="name" from="${financeauthority_list}" style="width: 100% !important;"  class="select2" required="true"  value=""  noSelection="['null':'Select Finance Approving Athority']" optionKey="id"/><br>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Instructor :</label>
                                                    <g:select name="instructor"  optionValue="employee_code" from="${inst_list}" style="width: 100% !important;"  class="select2" required="true"  value=""  noSelection="['null':'Select Instructor']" optionKey="id"/><br>
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




