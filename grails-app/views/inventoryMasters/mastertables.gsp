<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="poonam"/>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- start content here -->
                <!-- add content here -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Master Table Link's</header>
                                    </div>
                                    <!--<div class="card-body">-->
                                        <div class="table-responsive">
                                            <div class="card-body " id="bar-parent">
                                                <table id="exportTable" class="display nowrap erp-full-width mdl-data-table ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Table Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">1</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Approval Status</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addNewInvApprovalStatus" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">2</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Payment Method</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addNewInvPaymentMethod" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">3</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Serial Number</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addNewInvSerialNumber" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">4</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Room</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addinvroom" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">5</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Invitation Status</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addInvInivitationStatus" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">6</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Dead Stock Status</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addInvDeadStockStatus" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">7</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Depreciation Percentage</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addinvdepreciationpercentage" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">8</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Inventory Finance Approval Category</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addInventoryFinanceApprovingCategoy" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">9</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Inventory Finance Approval</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addinvfinanceapprovingauthority" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">10</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Instructor Finance Approving Authority</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addApprvovingAuth" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <!--
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">10</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Instructor Finance Approving Authority</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addinvinstructorfinanceapprovingauthority" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        -->
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">11</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Finance Approving Authority Level</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addinvfinanceapprovingauthoritylevel" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">12</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Purchase Type</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addinvpurchasetype" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">13</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Budget Type</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addBudgetType" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">14</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Budget Level</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addNewInvBudgetLevel" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">15</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Material Type</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addNewInvMaterialType" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">16</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Material Set</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addNewInvMaterialSet" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">17</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Material Category</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addinvmaterialcategory" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">18</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Material</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addinvmaterial" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">19</td>
                                                            <td class="mdl-data-table__cell--non-numeric">Material Part</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:link action="addNewInvMaterialPart" controller="inventoryMasters" class="btn btn-success"><i class="glyphicon glyphicon-folder-open"> </i> Open Page</g:link>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
        </div>
    </body>
</html>




