<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->

              <!--  <div class="row">
                  <div class="col-sm-12">
                         <div class="card">
                               <div class="card-body">
                                     <div class="modal-header">
                                          <h4 class="modal-title">Add New Serial Number</h4>
                                    </div>
                                       <g:form action="saveInvSerialNumber">
                                         <div class="row">
                                      <div class="col-sm-4">
                                            <label for="pwd">Serial Number:</label>
                                             <input type="text" class="form-control" name="serialNumberName" required="true">
                                     </div>
                                      <div class="col-sm-4">
                                            <label for="pwd">Departments:</label>
                                           <g:select name="departmentId"  optionValue="name" from="${departmentList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Department']" optionKey="id"/><br>
                                     </div>
                                      <div class="col-sm-4">
                                             <label for="pwd">Academic Year:</label>
                                          <g:select name="academicYearId"  optionValue="ay" from="${academicYearList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                   </div>
                               </div>
                               <div class="row">
                                  <div class="col-lg-12">
                                       <center>
                                           <br>
                                             <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                        </center>
                                   </div>
                               </div>
                                       </g:form>
                               </div>
                         </div>
                  </div>
            </div>-->

                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <div class="row">
                    <div class="col-md-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Level List</header>
                                            <div class="tools">
                                                <g:link  data-toggle="modal" data-target="#addNewInvSerialNumber"><span aria-hidden="true" class="icon-plus" title="Add New Serial Number" style="float:right; font-size:20px;"></span></g:link>
                                            </div>
                                    </div>
                                      <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                             <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Serial Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Department</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${invSerialNumberList}" var="serialnumber" status="i">
                                                    <tr>
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${serialnumber?.number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${serialnumber?.department?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${serialnumber?.academicyear?.ay}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editInvSerialNumber${i}"></i>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="deleteInvSerialNumber">
                                                                  <input type="hidden" value="${serialnumber?.id}" name="serialNumberId">
                                                                        <button  data-toggle="modal" data-target="#${i}myModal" class="fabutton" type="submit" onclick="return confirm('Do you want to Delete ?')" ><i class="fa fa-trash-o fa-2x erp-delete-icon-color"></i></button>
                                                            </g:form>
                                                        </td>
                                                    </tr>
                                                    <!-- Modal edit -->
                                                      <div class="modal fade" id="editInvSerialNumber${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title">Edit Serial Number</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="editInvSerialNumber">
                                                            <div class="modal-body">
                                                              <div class="form-group">
                                                                  <label for="pwd">Serial Number:</label>
                                                                  <input type="hidden" value="${serialnumber?.id}" name="serialNumberId">
                                                                  <input type="text" class="form-control" name="serialNumberName" required="true" value="${serialnumber?.number}">
                                                              </div>
                                                              <div class="form-group">
                                                                     <label for="pwd">Departments:</label>
                                                                         <g:select name="departmentId"  optionValue="name" from="${departmentList}" style="width: 100% !important;"  class="select2" required="true"  value="${serialnumber?.department?.id}"  noSelection="['null':'Select Department']" optionKey="id"/><br>
                                                                   </div>
                                                                   <div class="form-group">
                                                                    <label for="pwd">Academic Year:</label>
                                                                        <g:select name="academicYearId"  optionValue="ay" from="${academicYearList}" style="width: 100% !important;"  class="select2" required="true"  value="${serialnumber?.academicyear?.id}"  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                                                  </div>
                                                            <div class="modal-footer">
                                                              <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                            </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </g:each>
                                                </tbody>
                                               </table>

                                <!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
                                <!-- Modal New -->
                                  <div class="modal fade" id="addNewInvSerialNumber" role="dialog">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <h4 class="modal-title">Add New Serial Number</h4>
                                          <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                       <g:form action="saveInvSerialNumber">
                                        <div class="modal-body">
                                          <div class="form-group">
                                              <label for="pwd">Serial Number:</label>
                                              <input type="text" class="form-control" name="serialNumberName" required="true">
                                          </div>
                                          <div class="form-group">
                                           <label for="pwd">Departments:</label>
                                               <g:select name="departmentId"  optionValue="name" from="${departmentList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Department']" optionKey="id"/><br>
                                         </div>
                                         <div class="form-group">
                                          <label for="pwd">Academic Year:</label>
                                              <g:select name="academicYearId"  optionValue="ay" from="${academicYearList}" style="width: 100% !important;"  class="select2" required="true"  value="${id}"  noSelection="['null':'Select Academic Year']" optionKey="id"/><br>
                                        </div>

                                        </div>
                                        <div class="modal-footer">
                                          <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
                                        </div>
                                       </g:form>
      </div>
      </div>
      </div>
      </div>
      </div>
    </div>
  </div>
  </div>
</body>
</html>



