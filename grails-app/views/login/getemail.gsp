<div class="row">
   <div class="col-lg-12 col-md-12 col-sm-12 col-12">
       <div class="row clearfix">
           <div class="col-md-12 col-sm-6 col-12">
                <!-- Start coding only in This block ------------------->
               <div class="card">
                    <div class="panel-body">
                    <g:if test="${flash.message}">
                           <div class="alert alert-success">${flash.message}</div>
                       </g:if>
                      <g:if test="${flash.error}">
                           <div class="alert alert-danger"><strong>${flash.error}</strong></div>
                       </g:if>
                   <g:form name="myForm" action="resetemailsubmit">
                         <div class="col-sm-5">
                                    <label>Name : </label>
                                    <label style="color:red">${ename}</label></br>
                                    <label>Existing Person - Email :  ${person?.email}</label> <br>
                                    <label>Existing Login - Email :  ${emailid}</label> <br>
                                    <label>New Login - Email : </label>
                                    <input type="Text" name="email" value="${emailid}" size=40>
                         </div>
                         <div class="col-sm-2">
                            <label><g:hiddenField name="grnumber" value="${params.grnumber}" />
                            &nbsp</label><br><br>
                            <input type="hidden" value="${login1?.id}" name="login">
                            <input type="hidden" value="${learner1?.id}" name="learner">
                            <input type="hidden" value="${instructor1?.id}" name="instructor">
                            <input type="hidden" value="${person?.id}" name="person">
                            <g:submitButton id="submitButton" class="btn btn-sm btn-success" name="Update" value="Save" />
                         </div>
                    </g:form>
                </div>
           </div>
       </div>
   </div>
</div>

