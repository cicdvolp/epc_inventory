<div class="card">
    <div class="panel-body">
        <g:form name="myForm">
            <div class="row d-flex col-sm-12 justify-content-between">
                <div >
                    <label  style="font-size:12px;">PRN / EMP code</label><br/>
                    <b style="font-size:12px; color:purple;">${grno}</b>
                    <input class="form-control" id="grnumber" name="grnumber" type="hidden" value="${grno}"></input>
                </div>
                <div >
                    <label style="font-size:12px;">Name</label><br/>
                    <b style="font-size:12px;color:purple;">${name}</b>
                </div>
                <div >
                    <label style="font-size:12px;">Official email</label><br/>
                    <b style="font-size:12px;color:purple;">${offemail}</b>
                </div>
                <div >
                    <label style="font-size:12px;">Personal email</label><br/>
                    <b style="font-size:12px;color:purple;">${pemail}</b>
                </div>
                <div >
                    <label style="font-size:12px;">Contact No</label><br/>
                    <b style="font-size:12px;color:purple;">${contact}</b>
                </div>

            </div>
              <div class="row d-flex justify-content-center" style="padding :35px;">
                   <g:submitToRemote url="[action: 'reset_email_password']" update="updateMe1" value="Reset Gmail Password" class="btn btn-sm btn-primary" before="document.getElementById('smartprogressbar').style.display='block';"  onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
           </div>
        </g:form>
        <div id="updateMe1"></div>
    </div>
</div>