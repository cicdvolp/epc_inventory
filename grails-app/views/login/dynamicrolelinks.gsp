
<div>
    <span>
        <g:select class="js-example-basic-single" from='${session.dynamicrolelinks}' name="links" id="links" optionValue="${{it?.role?.roletype?.type_displayname + " : " + it?.role?.role_displayname+ " : " + it?.link_displayname}}" noSelection="['':'-Search Link-']" onchange="callMe()" optionKey="id"> </g:select>
    </span>
</div>
<script>
    function callMe() {
        var link = document.getElementById("links").value;
        //alert("link:"+link)
        jQuery.ajax({
            type:'POST',
            data:'link='+link,
            url:'/Login/redirecttopage',
            success:function(data,textStatus) {
                //jQuery('#subcontainer').html(data);
                $("body").html(data);
            },
        error:function(XMLHttpRequest,textStatus,errorThrown){}});
    }
</script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();
    });
</script>