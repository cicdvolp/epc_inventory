<!DOCTYPE html>
<html lang="en">
<head>
  <title>volp || Contactus</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <asset:stylesheet href="w3.css" />
  <asset:stylesheet href="animate.min.css" />

  <asset:stylesheet href="font-awesome.min.css" />
  <asset:stylesheet href="jquery-ui.css"/>
  <asset:stylesheet href="bootstrapVolpNewHome.min.css" />

  <asset:javascript src="jqueryVolpNewHome.min.js"/>
  <asset:javascript src="bootstrapVolpNewHome.min.js"/>
  <asset:javascript src="jquery-ui.js" />
  <asset:javascript src="jquery.viewportchecker.min.js"/>

  <style>

  body{
font-size: 16px
  }
  .margin-all{
    margin-left: 70px;
    margin-right: 70px;
  }
  /* Remove the navbar's default margin-bottom and rounded borders */
  .navbar {
    margin-bottom: 0;
    border-radius: 0;
  }

  /* Add a gray background color and some padding to the footer */
  footer {
    background-color: #f2f2f2;
    padding: 25px;
  }
  .navbar-inverse{
    background-color: #36b157;
  }
  .carousel-inner img {
    width: 100%; /* Set width to 100% */
    margin: auto;
    min-height:200px;
  }
  .carousel-indicators .active {
    width: 15px;
    height: 15px;
    margin: 0;
    background-color: #de383a;
  }

  .carousel-indicators li {
    display: inline-block;
    width: 15px;
    height: 15px;
    margin: 1px;
    text-indent: -999px;
    cursor: pointer;
    background-color: #000\9;
    background-color: rgba(0,0,0,0);
    border: 1px solid #fff;
    border-radius: 10px;
    background: #36b157;
  }
  .top-h3 {
    font-size: 2.5em;
    /*color: #de383a;*/
    margin-left: -40px;
    padding-left: 0px;
    width: 20%;

  }
  .btn-success{
    background: #36b157;
  }
  .btn-danger{
    background: #ff5252;
  }
  .btn.btn-default{
    margin-top: 10px;
  }
  .btn.btn-default:hover{
    transition: 2s ease-in;
    -webkit-transition: width 2s; /* Safari 3.1 to 6.0 */

  }
  .btn.btn-default span {

    cursor: pointer;
    display: inline-block;
    position: relative;
    transition: 0.5s;
    color: #de383a;
    font-style: oblique;
    text-transform: uppercase;
    font-weight: bold;
  }

  .btn.btn-default span:after {
    content: '\00bb';
    position: absolute;
    opacity: 0;
    top: 0;
    right: -20px;
    transition: 0.5s;
  }

  .btn.btn-default:hover span {
    padding-right: 25px;
  }

  .btn.btn-default:hover span:after {
    opacity: 1;
    right: 0;
  }
  .card {
    position: relative;
    box-shadow: 1px 1px 2px 2px rgba(0,0,0,0.2);
    transition: 0.3s;
    border-radius: 5px;
    width: 98%;
  }

  /*.card:hover {
    box-shadow: 0px 0px 0px 8px rgba(0,0,0,0.2);
    }*/

    .bookcardimg {
      border-radius: 5px 5px 0 0;
    }

    .container1 {
      padding: 2px 16px;
    }
    .bestseller{
      transform: rotate(270deg);
      transform-origin: left top 0;
      font-size: 13px;
      position: absolute;
      right: -82px;
      top:110px;
      background: #36b157;
      color:white;
      padding: 6px 12px;
      border-radius:2px;
    }
    .category{
      border-radius: 2px;
      font-size: 13px;
      position: absolute;
      left:1px; bottom:92px;
      background: #de383a;
      color:white;
      padding: 6px 12px;
      text-transform: uppercase;
    }
    .price{
      border-radius:2px;
      padding: 6px 12px;
      background: #36b157;
      position: absolute;
      bottom: 0;
      right: 5px;
      color: white
    }
    .star{
      border-radius:2px;
      padding: 6px 12px;
      background: #de383a;
      position: absolute;
      bottom: 0;
      left: 5px;
      color: white
    }
    .footer-bs {
      background-color: #3c3d41;
      padding: 60px 40px;
      color: rgba(255,255,255,1.00);
      margin-bottom: 20px;
      border-bottom-right-radius: 6px;
      border-top-left-radius: 0px;
      border-bottom-left-radius: 6px;
    }
    .footer-bs .footer-brand, .footer-bs .footer-nav, .footer-bs .footer-social, .footer-bs .footer-ns { padding:10px 25px; }
    .footer-bs .footer-nav, .footer-bs .footer-social, .footer-bs .footer-ns { border-color: transparent; }
    .footer-bs .footer-brand h2 { margin:0px 0px 10px; }
    .footer-bs .footer-brand p { font-size:12px; color:rgba(255,255,255,0.70); }

    .footer-bs .footer-nav ul.pages { list-style:none; padding:0px; }
    .footer-bs .footer-nav ul.pages li { padding:5px 0px;}
    .footer-bs .footer-nav ul.pages a { color:rgba(255,255,255,1.00); font-weight:bold; text-transform:uppercase; }
    .footer-bs .footer-nav ul.pages a:hover { color:rgba(255,255,255,0.80); text-decoration:none; }
    .footer-bs .footer-nav h4 {
      font-size: 11px;
      text-transform: uppercase;
      letter-spacing: 3px;
      margin-bottom:10px;
    }

    .footer-bs .footer-nav ul.list { list-style:none; padding:0px; }
    .footer-bs .footer-nav ul.list li { padding:5px 0px;}
    .footer-bs .footer-nav ul.list a { color:rgba(255,255,255,0.80); }
    .footer-bs .footer-nav ul.list a:hover { color:rgba(255,255,255,0.60); text-decoration:none; }

    .footer-bs .footer-social ul { list-style:none; padding:0px; }
    .footer-bs .footer-social h4 {
      font-size: 11px;
      text-transform: uppercase;
      letter-spacing: 3px;
    }
    .footer-bs .footer-social li { padding:5px 4px;}
    .footer-bs .footer-social a { color:rgba(255,255,255,1.00);}
    .footer-bs .footer-social a:hover { color:rgba(255,255,255,0.80); text-decoration:none; }

    .footer-bs .footer-ns h4 {
      font-size: 11px;
      text-transform: uppercase;
      letter-spacing: 3px;
      margin-bottom:10px;
    }
    .footer-bs .footer-ns p { font-size:12px; color:rgba(255,255,255,0.70); }
    /* ----------------------Menu Submenu--------------------------------------- */
    .dropdown-submenu{position:relative;font-size: 16px}
    .dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:-6px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
    .dropdown-submenu:hover>.dropdown-menu{display:block;font-size: 16px}
    .dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
    .dropdown-submenu:hover>a:after{border-left-color:#ffffff;}
    .dropdown-submenu.pull-left{float:none;}
    .dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
    .dropdown-menu>li>a {
      display: block;
      padding: 3px 20px;
      width: 250px;
      clear: both;
      font-weight: 400;
      line-height: 2;
      color: #333;
      white-space: nowrap;
    }

    .menu-item{
      width: 250px;
    }
    .navbar-brand{
      position: relative;
      top: -20px;
      left: -16px;
      z-index: 1;
    }
    .navbar-brand img{
      width: 68%;
    }
 .w3-border-red{
  border-bottom-color: #d9534f;
 }
    /* ----------------------End of Menu/Submenu--------------------------------------- */
    /* Hide the carousel text when the screen is less than 600 pixels wide */
    @media (max-width: 600px) {
      .carousel-caption {
        display: none;
      }
      .top-h3 {
        font-size: 2em;
        margin-left: 0px;
      }
      .navbar-brand{

      }
      .card{
        margin-top: 10px;

      }
      .footer-bs .footer-nav, .footer-bs .footer-social, .footer-bs .footer-ns { border-left:solid 1px rgba(255,255,255,0.10); }
    }
  </style>
</head>
<body>

  <nav class="navbar" >
    <div class="container-fluid">

      <div class="navbar-header">
        <a class="navbar-brand" href="#" style="" ><asset:image src="volpNewHome/200x150bold2.jpg"  alt="Image" /></a>
        <button title="Dashboard || Menu" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar" style="clear: both; border: 2px solid; border-color: black">
          <span class="icon-bar navbar-inverse"></span>
          <span class="icon-bar navbar-inverse"></span>
          <span class="icon-bar navbar-inverse"></span>
          <span class="icon-bar navbar-inverse"></span>
        </button>
      </div>

      <div class="collapse navbar-collapse " id="myNavbar">

        <ul class="nav navbar-nav" style="margin-left: 20%">
          <li class="menu-item dropdown">
            <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="color: black"><span class="glyphicon glyphicon-th-list"></span>&nbsp;COURSE CATEGORIES&nbsp;<span class="glyphicon glyphicon-triangle-bottom"> </span>
            </a>

            <ul class="dropdown-menu">
              <g:each var="obj" in="${menus}">
              <li class="menu-item dropdown dropdown-submenu">
                <g:link action="showCourses" controller="courseCategory" params="[ccid: obj[0].id]"
                class="dropdown-toggle" >
                <span> ${obj[0].name} &nbsp;<span></g:link>

                <ul class="dropdown-menu">
                                     <g:each var="sobj" in="${obj[1]}">
                     <li class="menu-item"><g:link  action="showCourses" controller="courseCategory" params="[ccid: sobj.id]">
                                          <span> ${sobj.name} </span>
                                       </g:link></li>
                   </g:each>

                                <!--<li class="menu-item dropdown dropdown-submenu"><a href="#" class="dropdown-toggle" data-toggle="dropdown">More</a>
                                  <ul class="dropdown-menu"><li><a href="#">3rd level link more options</a></li><li><a href="#">3rd level link</a></li></ul>
                              </li> -->

                             </ul>
                        </li>
                      </g:each>
                  </ul>
              </li>

              <li>
                <div style="margin-top: 7px; box-sizing: content-box;">
                 <g:form controller="courseCategory" action="findCourses">
                               		<input type="text"  placeholder="Search by Course Name..."  name="searchTxt" id="autocomplete" required="true" style="border-radius: 5px;  border: 1px solid grey; padding: 7px">
                               		<button class="btn btn-md btn-success" type="submit" style="margin-left: -4px;"><span class="glyphicon glyphicon-search"></span></button>
                                  </g:form>
                 </div>

              </li>

          </ul>

          <ul class="nav navbar-nav pull-right">
            <li><a href="${request.contextPath}/login/login" style="color: #36b157;padding: 6px 16px;margin-top: 8px;"><span class="glyphicon glyphicon-log-in"></span> &nbsp; LOGIN</a></li>
            <li><a href="${request.contextPath}/registration/verifyemail" class="btn-success" style="border-radius: 2px;color: white;padding: 6px 16px;    margin-top: 8px;"><span class="glyphicon glyphicon-user"></span> &nbsp; SIGN UP</a></li>
          </ul>

      </div> <!-- /myNavbar-->

  </div>

</nav>

<br>

<div class="margin-all container-fluid text-center" >  <!-- Start About volp------------------------------------ -->
  <div class="container">
<div class="w3-container w3-padding-64 " id="contact">
<g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
  <div class="w3-row">
    <div class="w3-col m5">
    <div class="w3-padding-16"><span class="w3-xlarge w3-border-red w3-bottombar">Contact Us</span></div>
      <h3>Address</h3>
      <p><i class="fa fa-institution  w3-xlarge" style="color: #ff5252;"></i> &nbsp;Suyog Center, Viswakarama Institutes, 7th Floor</p>
      <p><i class="fa fa-map-marker  w3-xlarge" style="color: #ff5252;"></i>&nbsp;&nbsp;No. 34A/1, Jawaharlal Nehru Rd.</p>
      <p><i class="fa fa-map-marker  w3-xlarge" style="color: #ff5252;"></i>&nbsp;&nbsp;Market Yard, Gultekdi, Pune, Maharashtra 411037.</p>
      <p><i class="fa fa-phone w3-xlarge" style="color: #ff5252;"></i>&nbsp;&nbsp;+91 077 09 601881</p>
      <p><i class="fa fa-envelope-o  w3-xlarge" style="color: #ff5252;"></i>&nbsp;&nbsp;volp.vu@gmail.com</p>
    </div>
    <div class="w3-col m7" >
      <g:form class="w3-container w3-card-2 w3-padding-16" controller="enquiry" action="saveContactDetails" method="POST">
      <div class="w3-section">
        <label>Name</label>
        <input class="form-control" type="text" name="name" required="" pattern="[A-Za-z\s]+">
      </div>
      <div class="w3-section">
        <label>Email</label>
        <input class="form-control" type="email" name="email" required="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">
      </div>
      <div class="w3-section">
        <label>Message</label>

        <g:textArea class="form-control" name="message" value="" rows="5" cols="40" required="true"/>
      </div>
        <input class="btn btn-md btn-success pull-right" type="submit" value="Send" />
      </g:form>
      <div id="updateMe">

      </div>
    </div>
  </div>
</div>
</div>
</div><br> <!-- End About volp------------------------------------ -->



<!----------- Footer ------------>
<footer class="footer-bs">
  <div class="row">
    <div class="col-md-3 footer-brand">
      <h2><asset:image src="volpNewHome/white.png" height="70" width="110" alt="Image"/> </h2>
      <p style="text-align: justify;">VOLP is a online education platform for e-classroom learning with modern tools & technologies.Our aim is to become a leading resource for learners by staying focused on the goals and principles set forth, when forming VOLP</p>
      <p>© 2018 VOPL, All rights reserved</p>
    </div>
    <div class="col-md-4 footer-nav ">
      <h4>Menu —</h4>
      <div class="col-md-6">
        <ul class="pages">
          <li><a href="${request.contextPath}/">HOME</a></li>
          <li><a href="${request.contextPath}/login/aboutvolp" target="_self">ABOUT VOLP</a></li>
          <li><a href="${request.contextPath}/login/partener">OUR CLIENTS</a></li>
          <li><a href="${request.contextPath}/login/pricing">PRICING</a></li>
          <li><a href="${request.contextPath}/login/contactus">CONTACT US</a></li>
        </ul>
      </div>

      <div class="col-md-6">

        <ul class="list">

          <li><a href="${request.contextPath}/login/faq">Terms & Condition</a></li>
          <li><a href="${request.contextPath}/login/faq">Privacy Policy</a></li>
          <li><a href="${request.contextPath}/login/faq">Faq</a></li>
        </ul>
      </div>
    </div>
    <div class="col-md-2 footer-social">
      <h4>Follow Us</h4>
      <ul>
        <li><a href="https://www.facebook.com/vishwakarma.platform.7" target="_blank">Facebook</a></li>
        <li><a href="https://plus.google.com/u/3/108067749854890374995" target="_blank">Google Plus</a></li>
        <li><a href="https://www.linkedin.com/in/vishwakarma-online-learning-platform-56658715a/" target="_blank">Linked in</a></li>

      </ul>
    </div>
    <div class="col-md-3 footer-ns">
      <h4>Newsletter</h4>
      <p>Subscribe to our news Letter</p>
      <p>
        <div class="input-group">
                <g:form action="saveMails" controller="subscribeNewsLetter" >
                <input class="form-control" type="email" name="email" placeholder="Your e-mail" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$">

                 <g:submitToRemote class="btn btn-sm btn-danger pull-right" url="[controller:'subscribeNewsLetter', action:'saveMails']" update="message1" value="Subscribe"/>

                </g:form>
                <span id="message1" ></span>
            </div><!-- /input-group -->
      </p>
    </div>
  </div>
</footer>
<section style="text-align:center; margin:10px auto;"><p>Designed by <a href="#">VOLP Team</a></p></section>

</div>
<script type="text/javascript">

  $(document).ready(function(){

    $('#autocomplete').autocomplete({
                          source: '<g:createLink controller='courseCategory' action='ajaxCourseFinder'/>'

                        });


    $('.footer-bs').mouseenter(function(){

      $('.footer-brand').addClass('animated fadeInLeft');
      $('.footer-ns').addClass('animated fadeInRight');
      $('.footer-social').addClass('animated fadeInDown');
      $('.footer-nav').addClass('animated fadeInUp');

    });

    $('.card').mouseenter(function(){
      $(this).addClass('animated pulse');
    });
    $('.card').mouseleave(function(){
      $(this).removeClass('animated pulse');
    });
    /*----------------------------menu/submenu code ------------------------*/
    $('ul.dropdown-menu [data-toggle=dropdown]').on('mouseenter', function(event) {
    // Avoid following the href location when clicking
    event.preventDefault();
    // Avoid having the menu to close when clicking
    event.stopPropagation();
    // If a menu is already open we close it
    $('ul.dropdown-menu [data-toggle=dropdown]').parent().removeClass('open');
    // opening the one you clicked on
    $(this).parent().addClass('open');

    var menu = $(this).parent().find("ul");
    var menupos = menu.offset();

    if ((menupos.left + menu.width()) + 30 > $(window).width()) {
      var newpos = - menu.width();
    } else {
      var newpos = $(this).parent().width();
    }
    menu.css({ left:newpos });

});
    /*---------------------------End of Menu/Submenu Code --------------------*/


  });

</script>
</body>
</html>
