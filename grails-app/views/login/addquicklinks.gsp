<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_inst"/>
        <meta name="author" content="pratik"/>
    </head>
    <body>
        <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <div class="page-bar">
                        <div class="page-title-breadcrumb">
                            <div class=" pull-left">
                                <div class="page-title">Set Quick Links</div>
                            </div>
                            <ol class="breadcrumb page-breadcrumb pull-right">
                                <li>
                                    <i class="fa fa-home"></i>
                                    &nbsp;
                                    <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                                    <!--<a class="parent-item" href="index.html">Home</a>-->
                                    &nbsp;
                                    <i class="fa fa-angle-right"></i>
                                </li>
                                <li class="active">Set Quick Links</li>
                            </ol>
                        </div>
                    </div>
                    <!-- add content here -->
                        <g:if test="${flash.message}">
                               <center class="alert alert-info" style="color: red;">${flash.message}</center>
                        </g:if>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Set Quick Links</header>
                                    </div>
                                    <!-- <div class="card-body"> -->
                                        <div class="table-responsive">
                                            <table class="mdl-data-table ml-table-bordered">
                                                <thead>
                                                    <tr>
                                                       <th class="mdl-data-table__cell--non-numeric">Sr. no</th>
                                                       <th class="mdl-data-table__cell--non-numeric">Module</th>
                                                       <th class="mdl-data-table__cell--non-numeric">Role</th>
                                                       <th class="mdl-data-table__cell--non-numeric">Role Link</th>
                                                       <th class="mdl-data-table__cell--non-numeric">Quick Link Set By You</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <g:each var="quicklink" in="${loginquicklinklist}" status="k">
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${quicklink?.rolelink?.role?.roletype?.type}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${quicklink?.rolelink?.role?.role}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${quicklink?.rolelink?.link_name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">
                                                                <g:if test="${quicklink?.rolelink?.isquicklink}">
                                                                    NA
                                                                </g:if>
                                                                <g:else>
                                                                    <g:if test="${quicklink?.quicklink}">
                                                                        <g:if test="${quicklink.quicklink.isactive[0]}">
                                                                            <g:link controller="Login" action="activequicklink" params="[rolelinkid:quicklink?.rolelink?.id]">
                                                                                <i class="fa fa-toggle-on fa-2x" style="color:green;" aria-hidden="true"></i>
                                                                            </g:link>
                                                                        </g:if>
                                                                        <g:else>
                                                                            <g:link controller="Login" action="activequicklink" params="[rolelinkid:quicklink?.rolelink?.id]">
                                                                                <i class="fa fa-toggle-off fa-2x" style="color:red;" aria-hidden="true"></i>
                                                                            </g:link>
                                                                        </g:else>
                                                                    </g:if>
                                                                    <g:else>
                                                                        <g:link controller="Login" action="activequicklink" params="[rolelinkid:quicklink?.rolelink?.id]">
                                                                            <i class="fa fa-toggle-off fa-2x" style="color:red;" aria-hidden="true"></i>
                                                                        </g:link>
                                                                    </g:else>
                                                                </g:else>
                                                            </td>
                                                        </tr>
                                                    </g:each>
                                                </tbody>
                                            </table>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Quick Links Set By Admin</header>
                                    </div>
                                    <!-- <div class="card-body"> -->
                                        <div class="table-responsive">
                                            <table class="mdl-data-table ml-table-bordered">
                                                <thead>
                                                    <tr>
                                                       <th class="mdl-data-table__cell--non-numeric">Sr. no</th>
                                                       <th class="mdl-data-table__cell--non-numeric">Module</th>
                                                       <th class="mdl-data-table__cell--non-numeric">Role</th>
                                                       <th class="mdl-data-table__cell--non-numeric">Role Link</th>
                                                       <!--
                                                       <th class="mdl-data-table__cell--non-numeric">Quick Link Set By Admin</th>
                                                       -->
                                                    </tr>
                                                </thead>
                                                <tbody>
	                                                <g:set var="x" value="${1}"/>
                                                    <g:each var="quicklink" in="${loginquicklinklist}" status="k">
                                                        <g:if test="${quicklink?.rolelink?.isquicklink}">
                                                            <tr>
                                                                <td class="mdl-data-table__cell--non-numeric">${x}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${quicklink?.rolelink?.role?.roletype?.type}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${quicklink?.rolelink?.role?.role}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${quicklink?.rolelink?.link_name}</td>
                                                                <!--
                                                                <td class="mdl-data-table__cell--non-numeric">
                                                                    <g:if test="${quicklink?.rolelink?.isquicklink}">
                                                                        <i class="fa fa-toggle-on fa-2x" style="color:green;" aria-hidden="true"></i>
                                                                    </g:if>
                                                                    <g:else>
                                                                        <i class="fa fa-toggle-off fa-2x" style="color:red;" aria-hidden="true"></i>
                                                                    </g:else>
                                                                </td>
                                                                -->
	                                                            <g:set var="x" value="${x+1}"/>
                                                            </tr>
                                                        </g:if>
                                                    </g:each>
                                                </tbody>
                                            </table>
                                        </div>
                                    <!-- </div> -->
                                </div>
                            </div>
                        </div>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>


