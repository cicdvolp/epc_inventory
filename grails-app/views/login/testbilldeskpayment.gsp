<!doctype html>
<html>

<head>
    <meta name="layout" content="smart_main_inst"/>
    <meta name="author" content="poonam"/>
    <asset:javascript src="jquery.min.js"/>
</head>

<body>
    <script type="text/javascript" id="resourceScript" src="https://services.billdesk.com/checkout-widget/src/app.bundle.js" async></script>

    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li>
                            <i class="fa fa-home"></i> &nbsp;
                            <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                            <i class="fa fa-angle-right"></i>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="row clearfix">
                         <div class="col-md-12 col-sm-6 col-12">

                             <!--
                                 <form action="https://pgi.billdesk.com/pgidsk/PGIMerchantPayment" method="post">
                                     <input type='hidden' name='msg' value='BDSKUATY|KOMAL1|NA|1.00|NA|NA|NA|INR|NA|R|bdskuaty|NA|NA|F|komal@edupluscampus.com|9922834870|NA|NA|NA|NA|NA|NA|DA2BB3444D44CAD6DFA22C6D0E8A52B8CFBE365920EB53AF584353D46438382C'>
                                     <input type="submit" name="submit" value="Pay Now" class="btn btn-primary">
                                 </form>
                                 <br><br>
                             -->


                             <input type="submit" name="submit" value="Pay With Test Account 1" class="btn btn-primary" onclick="pay()">
                             <input type="submit" name="submit" value="Pay With Test Account 2" class="btn btn-primary" onclick="pay2()">

                             <br><br><br>
                             <input type="submit" name="submit" value="Pay With Production 1" class="btn btn-primary" onclick="payProduction()">
                             <input type="submit" name="submit" value="Pay With Production 2" class="btn btn-primary" onclick="payProduction2()">

                         </div>
                     </div>
                </div>
            </div>

        </div>
    </div>
    <script>
        function pay() {

            var token = "BDSKUATY|testTran8998808|NA|2|NA|NA|NA|INR|NA|R|bdskuaty|NA|NA|F|Andheri|Mumbai|02240920005|support@billdesk.com|NA|NA|NA|NA";

            $.ajax({
                url: "${request.contextPath}/Login/getChecksumTest",
                data: {
                    token : token,
                },
                type: "POST",
                dataType: "html",
                success: function (data) {
                   console.log(token +"|"+ data);
                   bdPayment.initialize  ({
                       msg: token +"|"+ data,
                       options: {
                            enableChildWindowPosting: true,
                            enablePaymentRetry: true,
                            retry_attempt_count: 2,
                            txtPayCategory: "NETBANKING"
                       },
                       callbackUrl: "http://localhost:8080/login/payment_response"
                   });
                },error: function (xhr, status) {
                    alert("Sorry, there was a problem!");
                },complete: function (xhr, status) {
                    console.log("status : "+status);
                }
            });
        }

        function pay2() {

            var token = "BDSKUATY|testTran898808|NA|2|NA|NA|NA|INR|NA|R|bdskuaty|NA|NA|F|Andheri|Mumbai|02240920005|support@billdesk.com|NA|NA|NA|NA";

            $.ajax({
                url: "http://localhost/billdesk/billdesktest.php",
                type: "GET",
                dataType: "html",
                success: function (data) {
                   console.log(data);
                   bdPayment.initialize  ({
                       msg: token +"|"+ data,
                       options: {
                            enableChildWindowPosting: true,
                            enablePaymentRetry: true,
                            retry_attempt_count: 2
                       },
                       callbackUrl: "http://localhost:8080/login/payment_response"
                   });
                },error: function (xhr, status) {
                    alert("Sorry, there was a problem!");
                },complete: function (xhr, status) {
                    console.log("status : "+status);
                }
            });
        }
    </script>


    <script>
            function payProduction() {

                var token = "PCET|testTran28|NA|100|NA|NA|NA|INR|NA|R|pcet|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://localhost:8080/login/payment_response";

                $.ajax({
                    url: "${request.contextPath}/Login/checksumproduction",
                    data: {
                        token : token,
                    },
                    type: "POST",
                    dataType: "html",
                    success: function (data) {
                       console.log(token +"|"+ data);
                       bdPayment.initialize  ({
                           msg: token +"|"+ data,
                           options: {
                                enableChildWindowPosting: true,
                                enablePaymentRetry: true,
                                retry_attempt_count: 2
                           }
                       });
                    },error: function (xhr, status) {
                        alert("Sorry, there was a problem!");
                    },complete: function (xhr, status) {
                        console.log("status : "+status);
                    }
                });
            }


            function payProduction2() {

                var token = "PCET|testTran25|NA|100|NA|NA|NA|INR|NA|R|pcet|NA|NA|F|NA|NA|NA|NA|NA|NA|NA|http://localhost:8080/login/payment_response";

                $.ajax({
                    url: "http://localhost/billdesk/billdeskprod.php",
                    type: "GET",
                    dataType: "html",
                    success: function (data) {
                       console.log(data);
                       bdPayment.initialize  ({
                           msg: token +"|"+ data,
                           options: {
                                enableChildWindowPosting: true,
                                enablePaymentRetry: true,
                                retry_attempt_count: 2
                           }
                       });
                    },error: function (xhr, status) {
                        alert("Sorry, there was a problem!");
                    },complete: function (xhr, status) {
                        console.log("status : "+status);
                    }
                });
            }
    </script>

    </body>
</html>