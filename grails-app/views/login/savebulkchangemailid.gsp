<!DOCTYPE html>
<html lang="en">
<head>
   <meta name="layout" content="smart_main_datatable_inst"/>
   <meta name="author" content="pratik" />

    <!--<script>
        $(document).ready(function() {
            $('#common').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
                paging: false
            } );
            $('#common1').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
                paging: false
            } );
        } );
    </script>-->
</head>
<body>
    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <g:render template="/layouts/smart_template_inst/breadcrumb"/>
            <div class="row">
                <div id="document">
                    <% /* =================================================== */ %>
                        <g:if test="${failedlist}">
                            <div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="card card-topline-purple">
                                            <div class="card-head">
                                                <header class="erp-table-header">Failed Imported Learner List</header>

                                            </div>
                                            <div class="table-responsive">
                                                <div class="card-body " id="bar-parent">
                                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th> <center>Sr No.</center> </th>
                                                                <th> <center>Email Id</center> </th>
                                                                <th> <center>PRN No</center> </th>
                                                                <th> <center>Error Messages</center> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <g:each in="${failedlist}" var="row" status="i">
                                                                <tr>
                                                                    <td style="color:red;">${i+1}</td>
                                                                    <td style="color:red;">${row?.row?.email}</td>
                                                                    <td style="color:red;">${row?.row?.prnno}</td>
                                                                    <td style="color:red;">${row?.msg}</td>
                                                                </tr>
                                                            </g:each>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </g:if>
                        <g:else>
                            <div>
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Failed Imported Learner List</header>

                                    </div>
                                    <span><center><b>Data Not Available</b></center></span>
                                 </div>
                            </div>
                        </g:else>
                        <br/>
                        <br/>
                        <hr/>
                        <g:if test="${successlist}">
                            <div>
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="card card-topline-purple">
                                            <div class="card-head">
                                                <header class="erp-table-header">Success Imported Learner List</header>

                                            </div>
                                            <div class="table-responsive">
                                                <div class="card-body " id="bar-parent">
                                                    <table id="exportTable" class="display nowrap  table-striped  table-hover ml-table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th> <center>Sr No.</center> </th>
                                                                <th> <center>Email</center> </th>
                                                                <th> <center>PRN No</center> </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <g:each in="${successlist}" var="row" status="j">
                                                                <tr>
                                                                    <td style="color:green;">${j+1}</td>
                                                                    <td style="color:green;">${row?.row?.email}</td>
                                                                    <td style="color:green;">${row?.row?.prnno}</td>
                                                                </tr>
                                                            </g:each>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </g:if>
                        <g:else>
                            <div>
                                 <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Success Imported Learner List</header>

                                    </div>
                                    <span><center><b>Data Not Available</b></center></span>
                                 </div>
                            </div>
                        </g:else>
                    <% /* =================================================== */ %>
                </div>
            </div>
        </div>
    </div>
</body>
</html>