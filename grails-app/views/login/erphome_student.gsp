<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main"/>
        <meta name="author" content="pratik"/>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Dashboard</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                                <!--<a class="parent-item" href="index.html">Home</a>-->
                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Dashboard</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                    <g:include controller="ERPLearnerDashboard" action="dashboard"/>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>