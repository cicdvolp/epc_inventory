<g:form controller="login" action="emailotpverifyform" >
    <div class="row">
        <div class="col-sm-12">
            <label>OTP : </label>
            <input type="text" name="otp" id="otp" class="form-control" />
        </div>
        <div class="col-sm-12">
            <br>
            <center>
                <input type="hidden" name="email" value="${email}" />
                <g:submitToRemote url="[action: 'verificationotp']" update="emailform" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" value="Verify OTP"/>
            </center>
        </div>
    </div>
</g:form>