<html>
    <head>
        <title>JS Checkout Demo</title>
        <script>
            var token = '${txnToken}';
            var order_no = '${order_no}';
            function onScriptLoad(){
                  var config = {
                    "root": "",
                    "flow": "DEFAULT",
                    "data": {
                      "orderId": order_no,
                      "token": token,
                      "tokenType": "TXN_TOKEN",
                      "amount": "1.00"
                    },
                    "handler": {
                      "notifyMerchant": function(eventName,data){
                        console.log("notifyMerchant handler function called");
                        console.log("eventName => ",eventName);
                        console.log("data => ",data);
                      }
                    }
                  };

                  if(window.Paytm && window.Paytm.CheckoutJS){
                      window.Paytm.CheckoutJS.onLoad(function excecuteAfterCompleteLoad() {
                          // initialze configuration using init method
                          window.Paytm.CheckoutJS.init(config).then(function onSuccess() {
                              // after successfully updating configuration, invoke JS Checkout
                              window.Paytm.CheckoutJS.invoke();
                          }).catch(function onError(error){
                              console.log("error => ",error);
                          });
                      });
                  }
            }
        </script>
    </head>
    <body>
        <div id="paytm-checkoutjs"></div>
        <script type="application/javascript" crossorigin="anonymous" src="https://securegw-stage.paytm.in/merchantpgpui/checkoutjs/merchants/Pimpri54854078391381.js" onload="onScriptLoad();"></script>
    </body>
</html>