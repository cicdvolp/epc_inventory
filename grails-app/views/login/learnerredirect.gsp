<!doctype html>
<html>

<head>
    <meta name="layout" content="smart_main" />
    <meta name="author" content="aj" />
    <asset:javascript src="jquery.min.js"/>

    <style>
          .message, .msg{
             font-size: larger;
             color: orangered;
             font-weight: bold;
          }
    </style>
</head>

<body>
    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li>
                            <i class="fa fa-home"></i> &nbsp;
                            <g:link controller="login" action="erphome" class="parent-item">Home</g:link>
                            <!--<a class="parent-item" href="index.html">Home</a>-->
                            &nbsp;
                            <i class="fa fa-angle-right"></i>
                        </li>
                    </ol>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                     <div class="row clearfix">
                         <div class="col-md-12 col-sm-6 col-12">
                              <br>
                                 <center>
                                     <div class="alert alert-info" style="width:80%;">
                                         Please visit <a href="${org?.learner_website}">${org?.learner_website}</a> for ${form_name}<br>
                                         Username Is Your <b>Official Email Id</b> and Password Is <b>Same As VIERP</b>
                                     </div>
                                 </center>
                              <br>
                         </div>
                     </div>
                </div>
            </div>

        </div>
    </div>

    </body>
</html>