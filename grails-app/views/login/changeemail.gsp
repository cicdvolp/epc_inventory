<!DOCTYPE html>
<html lang="en">
    <head>
          <meta name="layout" content="smart_main_inst" />
          <meta name="author" content="aj" />
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- start content here -->
                <div class="row">
                   <div class="col-md-6 col-sm-6 col-6">
                       <div class="card card-topline-purple">
                           <div class="card-head">
                               <header class="erp-table-header">Change Email Id</header>
                           </div>
                           <div class="panel-body">
                               <g:form name="myForm">
                                     <div class="col-sm-12">
                                             <label for="grnumber">&nbsp;&nbsp;&nbsp;&nbsp;PRN Number / Employee code / Email</label>
                                             &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-control" id="grnumber" name="grnumber" type="text" placeholder="PRN Number / Employee code / Email" required>
                                     </div>
                                     <div class="col-sm-12">
                                     </br>
                                        <g:submitToRemote url="[action: 'getemail']" update="updateMe" value="Fetch" class="btn btn-sm btn-success"/>
                                     </div>
                               </g:form>
                            </div>
                       </div>
                       <div id="updateMe"></div>
                       <div id="updateMe2">
                            <g:if test="${flash.message}">
                                <div class="alert alert-error" style="font-size: larger;color: orangered;font-weight: bold;">${flash.message}</div>
                            </g:if>
                        </div>
                   </div>
                   <div class="col-md-6 col-sm-6 col-6">
                       <div class="card card-topline-purple">
                           <div class="card-head">
                               <header class="erp-table-header">Bulk Update Email Id By PRN No/Employee Code</header>
                           </div>
                           <div class="card-body">
                               <g:form controller="login" action="savebulkchangemailid" enctype="multipart/form-data" >
                                   <div class="row">
                                       <div class="col-sm-12">
                                           <input type="file" name="importfile" id="importfile" class="btn btn-info" accept=".xlsx"/>
                                           <br/>
                                           <br/>
                                       </div>
                                       <div class="col-sm-12">
                                           <center>
                                               <br>
                                               <input type="submit" value="Upload" name="Save" class="btn btn-primary"/>
                                           </center>
                                       </div>
                                   </div>
                               </g:form>
                           </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>
    </body>
</html>


