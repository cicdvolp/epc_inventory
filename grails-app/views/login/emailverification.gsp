<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template/meta" />
        <g:render template="/layouts/smart_template/css" />
        <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <title>EduPlusCampus</title>
    </head>
    <body class="page-header-fixed page-full-width sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
        <div class="page-wrapper">
            <g:render template="/layouts/smart_template/header" />
            <div class="page-container">
                <!-- start page content -->
                    <div class="page-content-wrapper">
                        <div class="page-content">
                            <!-- add content here -->
                                <div class="row">
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-6">
                                        <g:if test="${learner?.isemailverified == false}">
                                            <div class="card card-topline-purple">
                                                <div class="card-head">
                                                    <header class="erp-table-header">Email Verification</header>
                                                </div>
                                                <div class="card-body" id="emailform">
                                                    <g:form controller="login" action="emailotpverifyform" >
                                                        <div class="row">
                                                            <div class="col-sm-12">
                                                                <label>Email Id : </label>
                                                                <input type="text" name="email" id="email" value="${learner?.uid}" class="form-control" />
                                                            </div>
                                                            <div class="col-sm-12">
                                                                <br>
                                                                <center>
                                                                    <g:submitToRemote url="[action: 'emailotpverifyform']" update="emailform" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" value="Get OTP"/>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </g:form>
                                                </div>
                                            </div>
                                        </g:if>
                                        <g:else>
                                            <div class="card card-topline-purple">
                                                <div class="card-head">
                                                    <header class="erp-table-header">Mobile Verification</header>
                                                </div>
                                                <div class="card-body" id="mblform">
                                                    <div class="alert alert-success">
                                                        Email Verification is Done...
                                                    </div>
                                                </div>
                                            </div>
                                        </g:else>
                                    </div>
                                    <div class="col-lg-3">
                                    </div>
                                    <div class="col-lg-12">
                                        <br>
                                        <br>
                                        <center>
                                            <g:link controller="login" action="erphome_student">
                                                Skip for Now
                                            </g:link>
                                        </center>
                                    </div>
                                </div>
                            <!-- end content here -->
                        </div>
                    </div>
                <!-- end page content -->
            </div>
            <!-- start footer -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2020 &copy; EduPlusCampus By
                    <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- end footer -->
        </div>
        <g:render template="/layouts/smart_template/js" />
        <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
            <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
            <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>
    </body>
</html>
