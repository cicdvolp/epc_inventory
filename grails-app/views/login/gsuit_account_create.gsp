<!DOCTYPE html>
<html lang="en">
    <head>
          <meta name="layout" content="smart_main_inst" />
          <meta name="author" content="aj" />
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- start content here -->
                <div class="row">
                   <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                       <div class="row clearfix">
                           <div class="col-md-12 col-sm-6 col-12">
                               <div class="card card-box">
                                   <div class="panel-body">
                                       <g:form name="myForm">
                                             <div class="row col-sm-12">
                                                 <div class="col-sm-4">
                                                     <label for="grnumber">&nbsp;&nbsp;&nbsp;&nbsp;PRN Number / Employee code / Email</label>
                                                     &nbsp;&nbsp;&nbsp;&nbsp;<input class="form-control" id="grnumber" name="grnumber" type="text" placeholder="PRN Number / Employee code / Email" required>
                                                 </div>
                                                 <div class="col-sm-2" style="padding :35px;">
                                                   <g:submitToRemote url="[action: 'create_email_gsuit']" update="updateMe" value="Create Gmail Account" class="btn btn-sm btn-primary"/>
                                                 </div>
                                             </div>
                                        </g:form>
                                   </div>
                               </div>
                               <div id="updateMe"></div>
                           </div>
                       </div>
                   </div>
                </div>
            </div>
        </div>
   </body>
</html>
