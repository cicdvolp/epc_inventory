<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
    <% /* <link rel="stylesheet"
     href="https://fonts.googleapis.com/css?family=Tangerine">
    <link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">*/ %>
    <!-- Bootstrap Core CSS -->
    <asset:stylesheet src="bootstrapT1.min.css" />
    <asset:stylesheet src="metisMenuT1.min.css" />
    <asset:stylesheet src="sb-admimT1.min.css" />
    <asset:stylesheet src="font-awesomeT1.min.css" />
    <!-- jQuery -->
    <asset:javascript src="jqueryT1.min.js" />
    <!-- Bootstrap Core JavaScript -->
    <asset:javascript src="bootstrapT1.min.js" />
    <!-- Metis Menu Plugin JavaScript -->
    <asset:javascript src="metisMenuT1.min.js" />
    <!-- Custom Theme JavaScript -->
    <asset:javascript src="sb-adminT1.js" />

    <!---------------------Table data-------------------------------------------------------------->
    <asset:stylesheet src="jquery-ui.css"/>
    <asset:stylesheet src="datatables.min.css"/>
    <asset:stylesheet src="buttons.dataTables.min.css"/>
    <asset:javascript src="jquery.min.js"/>
    <asset:javascript src="datatables.min.js"/>
    <asset:javascript src="dataTables.buttons.min.js"/>
    <asset:javascript src="jszip.min.js"/>
    <asset:javascript src="pdfmake.min.js"/>
    <asset:javascript src="vfs_fonts.js"/>
    <asset:javascript src="buttons.html5.min.js"/>
    <!--------------------------------------------------------------------------------------------->

    <script>
        $(document).ready(function() {
            $('#common').DataTable( {
                dom: 'Bfrtip',
                buttons: [
                    'copyHtml5',
                    'excelHtml5',
                    'csvHtml5',
                    'pdfHtml5'
                ],
                paging: false
            } );
        } );
    </script>
    <style>
        .custome-button {
             background-color:orange;
             border-color:orange;
             width:125px;
             color:white;
        }
        .custome-button:hover {
             background-color:#F7D698;
             border-color:#F7D698;
             width:125px;
             color:black;
        }
        .custom-form-box {
	    background-color:#F1F1F1;
            border:1px solid;
            border-radius:8px;
            padding:10px;
        }
    </style>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
        <div href="#" style="color: white;text-align:center; font-family: Rokkitt,Tangerine; font-size: 35px;; font-weight: normal;">
            <span>Vishwakarma Group Of Institutes</span>
        </div>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <!-- /.navbar-header ------------------------------------------ -->
        <ul class="nav navbar-top-links navbar-right">
            <li><a style="color: white;">Hello,&nbsp;${session.user}</a></li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" style="background: white;opacity: .8; color: black;">
                    <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <g:link controller="Login" action="changepassword"><i class="fa fa-unlock fa-fw"></i>Change Password</g:link>
                    </li>
                    <li class="divider"></li>
                    <li>
                        <g:link controller="Login" action="erplogin" params="[logout:1]"><i class="fa fa-sign-out fa-fw"></i> Logout</g:link>
                    </li>
                </ul>
            <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <g:render template="/layouts/ERPlinkTemplateT1"/>
                </ul>
            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->
    </nav>
    <!----------------------------------------------------------------------------------------------------------------------------->
    <!-- Page Content -->
    <div id="page-wrapper">
        <div class="container-fluid">
            <g:link controller="Login" action="erphome" style="background-color:#f0ad4e; color: white; text-decoration: underline;border: none;position: absolute; left:250px" class="btn btn-sm btn-default" ><span class="glyphicon glyphicon-home"> </span>&nbsp;Home</g:link>
            <button style="background-color:#f0ad4e; color: white; text-decoration: underline;border: none;position: absolute; right: 0" class="btn btn-sm btn-default" onclick="history.back(-1)"><span class="glyphicon glyphicon-triangle-left"> </span>Go Back</button>
            <div class="row">
                <div id="document">
                    <% /* =================================================== */ %>
                        <h3 class="page-header"><center><b>TESTING PAGE</b></center></h3>
                        <br>
                        <br>
                        <br>
                        <br>
                        <span hidden id="ssss" style="color:blue; font-size:18px;">Exam is Going ON.....</span>
                        <div id="mytest" hidden>
                            <g:form controller="login" action="test">
                                <span style="color:red; font-size:18px;">You are Exam is over...</span>
                                <input type="submit" value="submit" />
                            </g:form>
                        </div>
                    <% /* =================================================== */ %>
                </div>
            </div>
        </div>
    </div>
    <!-- /#wrapper -->
    <footer class="navbar navbar-default" style="position: relative; bottom: 0; top: 18px">
     <p style="text-align: center; color: white; padding: 10px">All Rights Reserved @ VI Groups</p>
    </footer>

    <script type='text/javascript'>
$(document).ready(function(){
    $('#ssss').show();
});
        var count = 0;
        var myInterval;
        // Active
        window.addEventListener('focus', startTimer);
        // Inactive
        window.addEventListener('blur', stopTimer);
        function timerHandler() {
            count++;
            document.getElementById("seconds").innerHTML = count;
        }
        // Start timer
        function startTimer() {
            console.log('focus');
            myInterval = window.setInterval(timerHandler, 1000);
        }
        // Stop timer
        function stopTimer() {
            console.log('blur');
            if(!confirm('Are quite the exam ?')) {
                alert("Are quite the exam ?? ");
                $('#ssss').hide();
                $('#mytest').show();
                window.clearInterval(myInterval);
            } else {
                alert("false");
            }
        }
    </script>
</body>
</html>


