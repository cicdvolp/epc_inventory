<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Responsive Admin Template" />
        <meta name="author" content="RedstarHospital" />
        <title>EduPlusCampus</title>
        <!-- google font -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- icons -->
        <asset:stylesheet src="smart/fonts/font-awesome/css/font-awesome.min.css" />
        <asset:stylesheet src="smart/plugins/iconic/css/material-design-iconic-font.min.css" />
        <!-- bootstrap -->
        <asset:stylesheet src="smart/plugins/bootstrap/css/bootstrap.min.css" />
        <!-- style -->
        <asset:stylesheet src="smart/pages/extra_pages.css" />

    </head>
    <body>
        <div class="limiter">
            <div class="container-login100 page-background">
                <div class="wrap-login100">
                    <form class="form-404">
                        <!--
                        <span class="login100-form-logo">
                            <g:if test="${session.isphotopresent}">
                                <img class="img-circle user-img-circle" src="${session.profilephoto}" alt="User Image">
                            </g:if>
                            <g:else>
                                <asset:image src="img_avatar.png" alt="User Image" class="img-circle user-img-circle"/>
                            </g:else>
                        </span>
                        -->
                        <span class="form404-title p-b-34 p-t-27">
                            Error 404
                        </span>
                        <p class="content-404">
                        <!-- <b style="font-size:16px;">Path : ${request.forwardURI}</b>
                        <br>
                        <br> -->
                        The page you are looking for does't exist or an other error occurred.</p>
                        <div class="container-login100-form-btn">
                            <g:link controller="login" action="erphome" class="login100-form-btn">
                                Go to home page
                            </g:link>
                            <!--
                            <button class="login100-form-btn">
                                Go to home page
                            </button>
                            -->
                        </div>
                        <!--
                        <div class="text-center p-t-27">
                            <a class="txt1" href="#">
                                Need Help?
                            </a>
                        </div>
                        -->
                    </form>
                </div>
            </div>
        </div>
        <!-- start js include path -->
        <asset:javascript src="smart/plugins/jquery/jquery.min.js"/>
        <!-- bootstrap -->
        <asset:javascript src="smart/plugins/bootstrap/js/bootstrap.min.js"/>
        <asset:javascript src="smart/pages/extra-pages/pages.js"/>
        <!-- end js include path -->
    </body>
</html>