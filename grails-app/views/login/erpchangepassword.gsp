<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>VI | Login</title>

   <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Tangerine">
     <link href="https://fonts.googleapis.com/css?family=Rokkitt" rel="stylesheet">
    <!-- Bootstrap Core CSS -->
    <asset:stylesheet src="bootstrapT1.min.css" />

    <!-- MetisMenu CSS -->
    <asset:stylesheet src="metisMenuT1.min.css" />

    <!-- Custom CSS -->
    <asset:stylesheet src="sb-admimT1.min.css" />

    <!-- Custom Fonts -->
    <asset:stylesheet src="font-awesomeT1.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

   <style type="text/css">
       .btn-success{
        background: #ff5820;
        border-color: white;

       }
       .btn-success:hover, .btn-success:focus{
          background: white;
          color: #ff5820;
         border-color: #ff5820;
       }
       .panel-primary>.panel-heading {
        color: #fff;
        background: #333;
        border-color: #333;
    }
    .panel-primary {
    border-color: #333;
   </style>
</head>

<body>
	<nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">

            <div href="#" style="color: white;text-align:center; font-family: Rokkitt,Tangerine; font-size: 35px;; font-weight: normal;"><span>${session?.orgheadername}</span>

            </div>
    </nav>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-primary">

                    <div class="panel-heading">
                        <h3 class="panel-title text-center">Forgot Password </h3>
                    </div>
                    <div class="panel-body">


                                <g:form controller="Login" name="myform" action="savechangepassword" onsubmit="return checkpwd()">

                     				<div class="form-group">
                     					<span class="input">
                     					<label class="input_label">
                                            <span class="input__label-content">New Password:</span>
                                        </label>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp
                     					<input name="newpassword" id="newpassword" type="password" >
                                        <span class="input">
                                        <br></br>
                                    <label class="input_label">
                                        <span class="input__label-content">Confirm Password:</span>
                                    </label>&nbsp&nbsp&nbsp&nbsp
                                   <input name="confirmpassword" id="confirmpassword" type="password" >
                     					</span>

                     				</div>
                                 <center>    <input  class="btn btn-md btn-success" type="submit"  name="Change Password" value="Change Password"></center>
                     			</g:form>
                    </div>
                </div>
                <g:if test="${flash.message}">
                  <div class="alert alert-error" style="display: block">${flash.message}
                  </div>
                </g:if>
            </div>
        </div> <!-- End of Row  1  ------------- --><br><br>

        <div class="row" style="position: absolute; bottom: 20px">
                <div class="col-lg-3 col-md-6 col-md-offset-6">
                    <div class="panel ">
                        <div class="row">
                        <img src="https://www.vierp.in/logo/vu_vi_logo.png" alt="Card image" style="width:600px; max-height:120px" />
                                <!-- <asset:image src="vu_vi_logo.png" width="600" height="120" /> -->
                            </div>
                    </div>
                </div>


        </div> <!-- End of Row  ------------- -->

    </div> <!-- End of Main Container ------------- -->


   <!-- jQuery -->
    <asset:javascript src="jqueryT1.min.js" />

    <!-- Bootstrap Core JavaScript -->
    <asset:javascript src="bootstrapT1.min.js" />

    <!-- Metis Menu Plugin JavaScript -->
    <asset:javascript src="metisMenuT1.min.js" />

    <!-- Custom Theme JavaScript -->
    <asset:javascript src="sb-adminT1.js" />

<script>

function checkpwd(){
alert
var pwd1 = document.getElementById("newpassword").value;
var pwd2 = document.getElementById("confirmpassword").value;
//alert(pwd1+" "+pwd2)
if(pwd1==pwd2)
    return true;
else{
alert("Oops password not matched!!!")
     return false;

     }
return false;
}

function changepass()
{
    var xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                  document.getElementById("mainContentDiv").innerHTML = this.responseText;
             }
             };
              xmlhttp.open("GET", "${request.contextPath}/Login/erpchangepassword", true);
               xmlhttp.send();
}

</script>

</body>

</html>
