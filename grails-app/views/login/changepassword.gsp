<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Responsive Admin Template" />
        <meta name="author" content="aj" />
        <title>EduPlusCampus</title>
        <!-- google font -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- icons -->
        <asset:stylesheet src="smart/fonts/font-awesome/css/font-awesome.min.css" />
        <asset:stylesheet src="smart/plugins/iconic/css/material-design-iconic-font.min.css" />
        <!-- bootstrap -->
        <asset:stylesheet src="smart/plugins/bootstrap/css/bootstrap.min.css" />
        <!-- style -->
        <asset:stylesheet src="smart/pages/extra_pages.css" />


        <g:if test="${session.isstudent}">
            <meta name="layout" content="smart_main"/>
        </g:if>
        <g:else>
            <meta name="layout" content="smart_main_inst"/>
        </g:else>
    </head>
<body>
    <g:form url="[controller:'Login',action:'savechangepassword']" onsubmit="return checkpwd();">
    	<div class="limiter">
    		<div class="container-login100 page-background">
    			<div class="wrap-login100">
    				<form class="login100-form validate-form">
                        <span class="login100-form-logo">
                            <g:if test="${session.isphotopresent}">
                                <img class="img-circle user-img-circle border-dark" src="${session.profilephoto}" width="150" height="120" >
                            </g:if>
                            <g:else>
                                <asset:image src="img_avatar.png" class="img-circle user-img-circle"/>
                            </g:else>
                        </span>
                        <p class="text-center txt-locked">
                            ${session.userfullname}
                        </p>
    					<div class="wrap-input100 validate-input" data-validate="Enter password">
    						<input class="input100" type="password" name="oldPassword" id="oldPassword" placeholder="Old Password">
    						<span class="focus-input100" data-placeholder="&#xf191;"></span>
    					</div>
                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input class="input100" name="newpassword" id="newpassword" type="password" placeholder="New Password">
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input class="input100" name="confirmpassword" id="confirmpassword" type="password" placeholder="Confirm Password">
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
    					<div class="container-login100-form-btn">
    				    	<g:submitButton class="login100-form-btn" name="Change Password" value="Change Password"  ></g:submitButton>
    					</div>
    				</form>
    			</div>
    		</div>
    	</div>
    </g:form>

	<!-- start js include path -->
	<script src="../assets/plugins/jquery/jquery.min.js"></script>
	<!-- bootstrap -->
	<script src="../assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="../assets/js/pages/extra-pages/pages.js"></script>
	<!-- end js include path -->
</body>
</html>