<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Responsive Admin Template" />
        <meta name="author" content="RedstarHospital" />
        <title>EduPlusCampus</title>
        <!-- google font -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <!-- icons -->
        <asset:stylesheet src="smart/fonts/font-awesome/css/font-awesome.min.css" />
        <asset:stylesheet src="smart/plugins/iconic/css/material-design-iconic-font.min.css" />
        <!-- bootstrap -->
        <asset:stylesheet src="smart/plugins/bootstrap/css/bootstrap.min.css" />
        <!-- style -->
        <asset:stylesheet src="smart/pages/extra_pages.css" />

    </head>
    <body>
        <div class="limiter">
            <div class="container-login100 page-background">
                <div class="wrap-login100">
                    <g:form url="[controller:'login',action:'processerplogin']" class="login100-form validate-form">
                        <span class="login100-form-title p-b-34 p-t-27">
                            Log in
                        </span>
                        <div class="wrap-input100 validate-input" data-validate="Enter username">
                            <input type="text" class="input100" name="username" id="username" type="text" placeholder="Username" required>
                            <span class="focus-input100" data-placeholder="&#xf207;"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Enter password">
                            <input type="password" class="input100" name="password" id="password" type="password" placeholder="Password" required>
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
                        <!--
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox100" for="ckb1">
                                Remember me
                            </label>
                        </div>
                        -->
                        <div class="container-login100-form-btn">
                            <button type="submit" id="btnSubmit" class="login100-form-btn">
                                Login
                            </button>
                        </div>
                        <div class="text-center p-t-30">
                            <g:link controller="login" action="forgetPassword" style="color:#FFF;">
                                Forgot Password?
                            </g:link>
                        </div>
                    </g:form>
                </div>
            </div>
        </div>
        <!-- start js include path -->
        <asset:javascript src="smart/plugins/jquery/jquery.min.js"/>
        <!-- bootstrap -->
        <asset:javascript src="smart/plugins/bootstrap/js/bootstrap.min.js"/>
        <asset:javascript src="smart/pages/extra-pages/pages.js"/>
        <!-- end js include path -->
    </body>
</html>