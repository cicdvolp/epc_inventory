<g:if test="${flash.message}">
      <br>
           <center>
               <div class="alert alert-success" style="width:80%;">
                   ${flash.message}
               </div>
           </center>
      <br>
</g:if>
<g:if test="${flash.error}">
      <br>
           <center>
               <div class="alert alert-danger" style="width:80%;">
                   ${flash.error}
               </div>
           </center>
      <br>
</g:if>