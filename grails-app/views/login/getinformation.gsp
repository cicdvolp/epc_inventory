<g:if test="${flash.error}">
    <div class="alert alert-danger">
        ${flash.error}
    </div>
</g:if>
<g:else>
     <g:if test="${instructor?.employee_code || learner?.registration_number }">
        <g:if test="${instructor}">
               <div class="row">
                    <div class="col-sm-12">
                         <center>
                             <div class="card card-topline-purple ">
                                <div class="table-responsive">
                                    <table class="mdl-data-table ml-table-bordered" >
                                         <thead>
                                              <th class="mdl-data-table__cell--non-numeric"> Employee Code</th>
                                              <th class="mdl-data-table__cell--non-numeric"> Name</th>
                                              <th class="mdl-data-table__cell--non-numeric">Department</th>
                                              <th class="mdl-data-table__cell--non-numeric">Instructor-email</th>
                                              <th class="mdl-data-table__cell--non-numeric">Login-email</th>
                                              <th class="mdl-data-table__cell--non-numeric">Person-email</th>
                                              <th class="mdl-data-table__cell--non-numeric">Mobile No</th>

                                         </thead>
                                         <tbody>
                                             <td class="mdl-data-table__cell--non-numeric">${instructor?.employee_code}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${instructor?.person?.fullname_as_per_previous_marksheet}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${instructor?.department?.name}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${instructor?.uid}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${logina?.username}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${instructor?.person?.email}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${contact?.mobile_no}</td>

                                         </tbody>
                                    </table>
                                </div>
                             </div>
                         </center>
                    </div>
               </div>
               <div class="row">
                    <div class="col-sm-12">
                        <br>
                        <center>
                             <g:form action="storereseterppassword">
                                  <input type="hidden" value="${logina?.id}" name="login">
                                       <g:submitButton class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" name="Reset Password" value="Reset Password" />
                             </g:form>
                        </center>
                    </div>
               </div>
        </g:if>

        <g:if test="${learner}">
             <div class="row">
                  <div class="col-sm-12">
                       <center>
                           <div class="card card-topline-purple     ">
                               <div class="table-responsive">
                                   <table class="mdl-data-table ml-table-bordered" >
                                       <thead>
                                           <th class="mdl-data-table__cell--non-numeric"> PRN Number</th>
                                            <th class="mdl-data-table__cell--non-numeric"> Name</th>
                                            <th class="mdl-data-table__cell--non-numeric"> Program</th>
                                            <th class="mdl-data-table__cell--non-numeric">Learner-email</th>
                                            <th class="mdl-data-table__cell--non-numeric">Login-email</th>
                                            <th class="mdl-data-table__cell--non-numeric">Person-email</th>
                                            <th class="mdl-data-table__cell--non-numeric">Mobile</th>
                                       </thead>
                                       <tbody>
                                             <td class="mdl-data-table__cell--non-numeric">${learner?.registration_number}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${learner?.person?.fullname_as_per_previous_marksheet}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${learner?.program?.name}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${learner?.uid}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${logina?.username}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${learner?.person?.email}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${contact?.mobile_no}</td>
                                       </tbody>
                                   </table>
                               </div>
                           </div>
                       </center>
                  </div>
             </div>
             <div class="row">
                 <div class="col-sm-12">
                      <br>
                      <center>
                           <g:form action="storereseterppassword">
                                <input type="hidden" value="${logina?.id}" name="login">
                                <g:submitButton class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn btn-sm btn-primary" name="Reset Password" value="Reset Password" />
                           </g:form>
                      </center>
                 </div>
             </div>
        </g:if>
     </g:if>
     <g:else>
            <br>
            <div class="alert alert-danger" role="status">
                 <strong class="font-weightage"> </strong>  Please Enter PRN Number/Employee Code...
            </div>
     </g:else>
</g:else>

