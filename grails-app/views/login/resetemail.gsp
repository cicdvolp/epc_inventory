<!DOCTYPE html>
<html>
    <head>
       <meta name="layout" content="smart_main_inst"/>
       <meta name="author" content="poonam"/>
     </head>
    <body >
        <div class="page-content-wrapper">
            <div class="page-content">
              <div class="container" >
               <g:render template="/layouts/smart_template_inst/breadcrumb" />
                  <g:if test="${flash.message}">
                       <div class="alert alert-success">${flash.message}</div>
                   </g:if>
                  <g:if test="${flash.error}">
                       <div class="alert alert-danger"><strong>${flash.error}</strong></div>
                   </g:if>
                <div class="card card-login mx-auto mt-5">
                  <div class="card-header bg-danger text-white">Sign In</div>
                  <div class="card-body">

                 <g:form name="myForm" id="loginForm" url="[action:'resetemailsubmit',controller:'login']" >
                    <hr>
                     <div class="form-group">
                                <label for="grnumber">PRN Number / Employee code</label>
                                <input class="form-control" id="grnumber" name="grnumber" type="text" placeholder="PRN Number" required>
                     </div>
                      <div class="form-group">
                        <label for="exampleInputEmail1">Official Email  address</label>
                        <input class="form-control" id="email" type="email" name="email" aria-describedby="emailHelp" placeholder="Enter official email" required>
                      </div>
                      <input class="btn btn-success btn-block" type="submit"  value="Login" name="submit">
                    </g:form>
                  </div>
                </div>
              </div>
            </div>
        </div>
    </body>
</html>
