<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_inst"/>
        <meta name="author" content="pratik"/>
        <!-- jQuery -->
        <asset:javascript src="jqueryT1.min.js" />
        <!-- Metis Menu Plugin JavaScript -->
        <asset:javascript src="metisMenuT1.min.js" />
        <!-- Custom Theme JavaScript -->
        <asset:javascript src="sb-adminT1.js" />
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <!--
                        <div class=" pull-left">
                            <div class="page-title">Dashboard</div>
                        </div>
                        -->
                    </div>
                </div>
                <!-- add content here -->
                    <!--
					<div class="state-overview">
						<div class="row">
						    <div class="col-lg-12">
						        <div class="card">
                                    <div class="card-body">
                                        <center>
                                            <g:if test="${org.id == 4}">
                                                <asset:image src="logovit10.png"/>
                                            </g:if>
                                            <g:if test="${org.id == 5}">
                                                <asset:image src="VIIT-Header.png"/>
                                            </g:if>
                                        </center>
                                    </div>
						        </div>
						    </div>
						</div>
                    </div>
                    -->
					<div class="row">
					    <div class="col-sm-3">
                            <!-- start widget -->
                            <div class="state-overview">
                                <div class="col-12">
                                    <div class="info-box bg-b-green">
                                        <span class="info-box-icon push-bottom"><i class="material-icons">group</i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Students</span>
                                            <span class="info-box-number">${totalstudent}</span>
                                            <div class="progress">
                                                <div class="progress-bar" style="width: 100%"></div>
                                            </div>
                                            <br>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                                <div class="col-12">
                                    <div class="info-box bg-b-yellow">
                                        <span class="info-box-icon push-bottom"><i class="material-icons">person</i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Faculty</span>
                                            <span class="info-box-number">${instcount}</span>
                                            <div class="progress">
                                                <div class="progress-bar" style="width: 100%"></div>
                                            </div>
                                            <br>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                                <div class="col-12">
                                    <div class="info-box bg-b-blue">
                                        <span class="info-box-icon push-bottom"><i class="material-icons">school</i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Course</span>
                                            <span class="info-box-number">${coursecount}</span>
                                            <div class="progress">
                                                <div class="progress-bar" style="width: 100%"></div>
                                            </div>
                                            <br>
                                        </div>
                                        <!-- /.info-box-content -->
                                    </div>
                                    <!-- /.info-box -->
                                </div>
                                <!-- /.col -->
                                <!--
                                <div class="col-xl-3 col-md-6 col-12">
                                    <div class="info-box bg-b-pink">
                                        <span class="info-box-icon push-bottom"><i
                                                class="material-icons">monetization_on</i></span>
                                        <div class="info-box-content">
                                            <span class="info-box-text">Fees Collection</span>
                                            <span class="info-box-number">13,921</span><span>$</span>
                                            <div class="progress">
                                                <div class="progress-bar" style="width: 50%"></div>
                                            </div>
                                            <span class="progress-description">
                                                50% Increase in 28 Days
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                -->
                            </div>
                            <!-- end widget -->
					    </div>
					    <div class="col-sm-9">
					        <div class="card">
					            <div class="card-header">Quick Links</div>
					            <!-- <div class="card-body"> -->
					                <div style="padding:10px;">
                                        <g:include controller="login" action="dynamicrolelinks" style="width:50px;"/>
                                    </div>
                                    <g:each var="rolelink" in= "${session?.fixedrolelinkslist}" status="i">
                                        <g:if test="${rolelink?.link_displayname}">
                                            <div>
                                                <g:if test="${i==0}">
                                                    <hr>
                                                </g:if>
                                                <g:link controller="${rolelink?.controller_name}" action="${rolelink?.action_name}" title="click here for ${rolelink?.link_displayname}" style="text-decoration: none;" style="padding-left:20px;">
                                                    ${rolelink?.link_displayname.toUpperCase()}
                                                </g:link>
                                                <g:if test="${session?.fixedrolelinkslist.size() != i+1}">
                                                    <hr>
                                                </g:if>
                                                <g:else>
                                                    <br>
                                                </g:else>
                                            </div>
                                        </g:if>
                                    </g:each>
					            <!-- </div> -->
					        </div>
					    </div>
					</div>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>