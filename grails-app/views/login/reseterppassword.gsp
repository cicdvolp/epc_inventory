<!DOCTYPE html>
<html lang="en">
    <head>
          <meta name="layout" content="smart_main_inst" />
          <meta name="author" content="aj" />
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- start content here -->
                    <div class="row">
                       <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                           <div class="row clearfix">
                               <div class="col-md-12 col-sm-6 col-12">
                                   <div class="card card-box">
                                       <div class="panel-body">
                                            <g:if test="${flash.message}">
                                                <div class="alert alert-success" >${flash.message}</div>
                                            </g:if>
                                            <g:if test="${flash.error}">
                                                    <div class="alert alert-danger">
                                                        ${flash.error}
                                                    </div>
                                            </g:if>

                                            <g:form url="[controller:'login' ,action: 'getinformation']" >
                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Employee Code/PRN Number/Email : </label>
                                                        <g:textField class="form-control" name="grno" value="" />
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <br>
                                                        <center>

                                                           <g:submitToRemote url="[action: 'getinformation',  controller:'Login']" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" value="Fetch" update="updateMe" />
                                                        </center>
                                                    </div>
                                                </div>
                                            </g:form>
                                       </div>
                                   </div>

                                   <div id="updateMe"></div>

                               </div>
                           </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>


