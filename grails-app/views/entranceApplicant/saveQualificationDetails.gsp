<style>
  .bordernone{
     border: none !important;
  }

  #printonly {
        display: none;
  }
  @media print {
    #printonly {
        display: block;
    }
  }
  td {
  text-align: center;
}
</style>

<div class="card">
    <div class="form-row col-sm-12">
        <div class="col-sm-12">
            <div id="print" style="overflow: auto; font-size:100%;" >
                <table style="width:100%;">
                    <thead>
                       <tr class="bordernone">
                           <td class="bordernone"><h3 style="background:lightgrey;"><center>Qualification Details</center></h3></td>
                       </tr>
                    </thead>
                   <tbody>
                        <tr class="bordernone">
                             <td class="bordernone">
                                <table class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                            <th class="mdl-data-table__cell--non-numeric">Degree</th>
                                            <th class="mdl-data-table__cell--non-numeric">Class/Grade</th>
                                            <th class="mdl-data-table__cell--non-numeric">Percentage?CPI</th>
                                            <th class="mdl-data-table__cell--non-numeric">Year Of Passing</th>
                                            <th class="mdl-data-table__cell--non-numeric">University/Board</th>
                                            <th class="mdl-data-table__cell--non-numeric">Branch</th>
                                            <th class="mdl-data-table__cell--non-numeric">Specialization</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <g:each in="${EntranceApplicantAcademics}" var="name" status="i">
                                            <tr>
                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.entrancedegree?.name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.recclass?.name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.cpi_marks}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.yearofpassing}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.university}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.branch}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.specialization}</td>
                                            </tr>
                                        </g:each>
                                    </tbody>
                                </table>
                             </td>
                        </tr>
                   </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
