<!doctype html>
<html lang="en" class="no-js">
    <head>
        <g:render template="/layouts/smart_template/meta" />
        <g:render template="/layouts/smart_template/css" />
        <link rel="shortcut icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" href="${createLinkTo(dir:'images',file:'favicon.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="${createLinkTo(dir:'images',file:'epn144.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="${createLinkTo(dir:'images',file:'img/logos/apple-touch-icon-72x72.png')}" />
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="${createLinkTo(dir:'images',file:'apple-touch-icon-114x114.png')}" />
        <!--select2-->
        <asset:stylesheet src="smart/plugins/select2/css/select2.css"/>
        <asset:stylesheet src="smart/plugins/select2/css/select2-bootstrap.min.css"/>
        <title>EduPlusCampus</title>
        <style>
            @media (max-width: 600px) {
                .btn {
                    width:100% !important;
                    text-align:center !important;
                    margin-top: 3px !important;
                    margin-bottom:3px !important;
                }
            }
            .btn {
                width:100% !important;
                text-align:center !important;
                margin-top: 3px !important;
                margin-bottom:3px !important;
            }
            .butn {
                background: #cdd2d3;
                padding: 0px;
                font-size: 12px;
                font-style: bold;
                text-align: center;
                margin: 10px;
                color: black;
            }
            .pending {
                background: red;
                color: yellow;
            }
            .complete {
                background: green;
                color: yellow;
            }
            .normal {
                background: #42a7f5;
                color: #fff;
            }
            label {
                font-size: 12px;
            }
            @media only screen and (max-width: 768px) {
                .example {
                    width: 100%;
                }
            }
        </style>
    </head>
    <body class="page-header-fixed page-full-width sidemenu-closed-hidelogo page-content-white page-md header-white white-sidebar-color logo-white">
        <div class="page-wrapper">
            <g:render template="/layouts/smart_template/header1" />
            <div class="page-container">
                <!-- start page content -->
                <div class="page-content-wrapper">
                    <div class="page-content">
                        <!-- add content here -->

                            <div class="row">
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="entranceApplicant" action="eadashboard" class="btn btn-light">Applicant Profile</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceApplication" class="btn btn-primary">Application Form</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceadmissionspayfees" class="btn btn-primary">Application Form Fees</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="trackEntranceApplication" class="btn btn-primary">Application Tracking</g:link>
                                </div>
                                <div class="col-sm" style="margin:0px !important; padding:0px !important;">
                                    <g:link controller="application" action="entranceExamination" class="btn btn-primary">Entrance Exam</g:link>
                                </div>
                            </div>

                            <br>

                            <g:if test="${flash.error}">
                                <div class="alert alert-danger" >${flash.error}</div>
                            </g:if>
                            <g:if test="${flash.success}">
                                <div class="alert alert-success" >${flash.success}</div>
                            </g:if>
                            <g:if test="${flash.message}">
                                <div class="alert alert-success" >${flash.message}</div>
                            </g:if>
                            <!--
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="float-right">
                                        <g:link controller="application" action="entranceApplication" class="btn btn-primary">Proceed To Fill Application Form</g:link>
                                    </div>
                                </div>
                            </div>
                            -->
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card" style="background: #92EEF9;">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-sm-1">
                                                    <div class="float-right" style="background:green; height:25px; width:50px;"></div>
                                                </div>
                                                <div class="col-sm-11">
                                                    <span class="d-inline">Mandatory and Filled Information</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1">
                                                    <div class="float-right" style="background:red; height:25px; width:50px;"></div>
                                                </div>
                                                <div class="col-sm-11">
                                                    <span>Mandatory and Incomplete Information</span>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-1">
                                                    <div class="float-right" style="background:#42a7f5; height:25px; width:50px;"></div>
                                                </div>
                                                <div class="col-sm-11">
                                                    <span>Optional Information</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="card tab-border">
                                        <header class="card-heading custom-tab ">
                                            <ul class="nav nav-tabs">
                                                <g:if test="${applicant_profile_personal_status!=null}">
                                                    <li class="active example">
                                                        <a data-toggle="tab" href="#Personal_Details" class="butn complete">
                                                        <i class="material-icons">person</i></br>Personal Details</a>
                                                    </li>
                                                </g:if>
                                                <g:else>
                                                    <li class="active example">
                                                        <a data-toggle="tab" href="#Personal_Details" class="butn pending">
                                                        <i class="material-icons">person</i></br>Personal Details</a>
                                                    </li>
                                                </g:else>
                                                <g:if test="${applicant_profile_contact_status!=null}">
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Contact_Details" class="butn complete">
                                                        <i class="material-icons">phone</i></br>Contact Details</a>
                                                    </li>
                                                </g:if>
                                                <g:else>
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Contact_Details" class="butn pending">
                                                        <i class="material-icons">phone</i></br>Contact Details</a>
                                                    </li>
                                                </g:else>
                                                <g:if test="${applicant_profile_education_status!=null}">
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Qualification_Details" class="butn complete">
                                                        <i class="material-icons">school</i></br>Qualification Details</a>
                                                    </li>
                                                </g:if>
                                                <g:else>
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Qualification_Details" class="butn pending">
                                                        <i class="material-icons">school</i></br>Qualification Details</a>
                                                    </li>
                                                </g:else>
                                                <g:if test="${EA!=null}">
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Experience_Details" class="butn normal">
                                                        <i class="material-icons">work</i></br>Experience</a>
                                                    </li>
                                                </g:if>
                                                <g:else>
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Experience_Details" class="butn pending">
                                                        <i class="material-icons">work</i></br>Experience</a>
                                                    </li>
                                                </g:else>
                                                <g:if test="${applicant_profile_documents_status!=null}">
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Document_Details" class="butn complete">
                                                        <i class="material-icons">file_upload</i></br>Documents</a>
                                                    </li>
                                                </g:if>
                                                <g:else>
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Document_Details" class="butn pending">
                                                        <i class="material-icons">file_upload</i></br>Documents</a>
                                                    </li>
                                                </g:else>
                                                <g:if test="${applicant_profile_photo_status!=null}">
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Photo_Details" class="butn complete">
                                                        <i class="material-icons">portrait</i></br>Photo</a>
                                                    </li>
                                                </g:if>
                                                <g:else>
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Photo_Details" class="butn pending">
                                                        <i class="material-icons">portrait</i></br>Photo</a>
                                                    </li>
                                                </g:else>
                                                <g:if test="${applicant_profile_sign_status!=null}">
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Sign_Details" class="butn complete">
                                                        <i class="material-icons">portrait</i></br>Sign</a>
                                                    </li>
                                                </g:if>
                                                <g:else>
                                                    <li class="example">
                                                        <a data-toggle="tab" href="#Sign_Details" class="butn pending">
                                                        <i class="material-icons">portrait</i></br>Sign</a>
                                                    </li>
                                                </g:else>
                                            </ul>
                                        </header>
                                        <div class="card-body">
                                            <div class="tab-content">
                                                <div id="Personal_Details" class="tab-pane active">
                                                    <g:include controller="EntranceApplicant" action="fillPersonalInformation"/>
                                                </div>
                                                <div id="Contact_Details" class="tab-pane">
                                                    <g:include controller="EntranceApplicant" action="fillContactInformation"/>
                                                </div>
                                                <div id="Qualification_Details" class="tab-pane">
                                                    <g:include controller="EntranceApplicant" action="fillQualificationDetails"/>
                                                </div>
                                                <div id="Experience_Details" class="tab-pane">
                                                    <g:include controller="EntranceApplicant" action="fillExperienceDetails"/>
                                                </div>
                                                <div id="Document_Details" class="tab-pane">
                                                    <g:include controller="EntranceApplicant" action="fillDocumentDetails"/>
                                                </div>
                                                <div id="Photo_Details" class="tab-pane">
                                                    <g:include controller="EntranceApplicant" action="fillPhotoDetails"/>
                                                </div>
                                                <div id="Sign_Details" class="tab-pane">
                                                    <g:include controller="EntranceApplicant" action="fillSignDetails"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <!-- end content here -->
                    </div>
                </div>
                <!-- end page content -->
            </div>
            <!-- start footer -->
            <div class="page-footer">
                <div class="page-footer-inner">
                    2020 &copy; EduPlusCampus By
                    <a href="https://edupluscampus.com/" target="_top" class="makerCss">VGESPL</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- end footer -->
        </div>
        <g:render template="/layouts/smart_template/js" />
        <div id="smartprogressbar" style="display:none; position:fixed; top: 50%; left: 35%; padding:20px; background:#e5e5e9;">
            <span style="color:blue; font-size:14px; font-weight:700; padding-bottom:5px;">Please Wait...</span>
            <div class="mdl-progress mdl-js-progress mdl-progress__indeterminate"></div>
        </div>
        <!--select2-->
        <asset:javascript src="smart/plugins/select2/js/select2.js"/>
        <asset:javascript src="smart/pages/select2/select2-init.js"/>
    </body>
</html>
