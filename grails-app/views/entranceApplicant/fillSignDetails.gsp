<html>
<head>
    <meta name="layout" content="ERPmainTemplateT2">
    <style>
        #spinner {
          position: fixed;
          top: 50%;
          left: 50%;
          margin-left: -50px; // half width of the spinner gif
          margin-top: -50px; // half height of the spinner gif
          z-index: 5000;
          overflow: auto;
        }
    </style>
</head>
    <g:form action="saveSignDetails" controller="EntranceApplicant" enctype="multipart/form-data" >
        <input type="hidden" name="EAID" value="${EA?.id}">
        <input type="hidden" name="contact_tab" value="Upload Sign">
        <fieldset>
            <legend>Sign Upload</legend>
            <div class="card-body row">
                <div class="col-md-4">
                    <div class="panel btn-warning disabled">
                        <div class="panel-heading">Sign Should be in .jpg format only. Sign Resolution should be W=350px. H=450px. Sign size should be less than or equal to 200 Kb.</div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="sign" >Sign Select</label>
                        <input type="file" name="newFile" id="sign" accept=".jpg" class="btn btn-primary" onchange="loadFileSign(event);validateFileType(this);Upload()" required="true"/>
                    </div>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-2">
                    <label for="sign">Sign</label>
                    <img id="outputsign" style="width:150px;height:170px;" src="${url}"/>
                </div>
             </div> <!-- /.row -->
             <br>
             <center><input type="submit" value="Save" class="btn btn-primary"></center>
        </fieldset>
    </g:form>

<!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->

<script type="text/javascript">
    function validateFileType(x){
        var fileName = x.value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="jpg"){
            //TO DO
        }else{
            alert("Only jpg file are allowed!");
            x.value=null;
        }
    }
</script>

<script>
  var loadFileSign = function(event) {
    var output = document.getElementById('outputsign');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<script>
    $(document).ready(function() {
        $("#outputsign").hide();
        var _URL = window.URL || window.webkitURL;
        $("#sign").change(function(e) {
            var file, img;
            var file_size = $('#sign')[0].files[0].size;
            if ((file = this.files[0]))
            {
                img = new Image();
                img.onload = function()
                {

                    if(file_size<=2000000 && this.width<=350 && this.height<=450)
                    {
                        $("#outputsign").show();
                    }
                    else
                    {
                        alert("Sign Resolution should be W=350px. H=450px. OR File size=200kb.");
                        $("#outputsign").hide();
                        $("#sign").val(null);
                    }
                };
                img.onerror = function() {
                    alert( "not a valid file: " + file.type);
                };
                img.src = _URL.createObjectURL(file);
            }
        });
    });
</script>

 <script type="text/javascript">
    /*function Upload() {
        var fileUpload = document.getElementById("sign");
        if (typeof (fileUpload.files) != "undefined") {
            var size = parseFloat(fileUpload.files[0].size / 1024).toFixed(2);
            alert(size + " KB.");
            if(size<=10000)
            {
                alert(" file Size 2KB.");
            }
        } else {
            alert("This browser does not support HTML5.");
        }
    }*/
</script>
</html>