<style>
  .bordernone{
     border: none !important;
  }

  #printonly {
        display: none;
  }
  @media print {
    #printonly {
        display: block;
    }
  }
  td {
  text-align: center;
}
</style>

<div class="card">
    <div class="form-row col-sm-12">
        <div class="col-sm-12">
            <div id="print" style="overflow: auto; font-size:100%;" >
                <table style="width:100%;">
                    <thead>
                        <tr class="bordernone">
                           <td class="bordernone"><h3 style="background:lightgrey;"><center>Experience Details</center></h3></td>
                        </tr>
                    </thead>
                   <tbody>
                        <tr class="bordernone">
                             <td class="bordernone">
                                <table class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                    <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                            <th class="mdl-data-table__cell--non-numeric">Experience Type</th>
                                            <th class="mdl-data-table__cell--non-numeric">Name Of Organization</th>
                                            <th class="mdl-data-table__cell--non-numeric">From Date</th>
                                            <th class="mdl-data-table__cell--non-numeric">To Date</th>
                                            <th class="mdl-data-table__cell--non-numeric">Is this Current Job?</th>
                                            <th class="mdl-data-table__cell--non-numeric">Designation</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <g:each in="${EntranceApplicantExperience}" var="name" status="i">
                                            <tr>
                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.recexperiencetype?.type}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.organization_name}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.from_date}</td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.to_date}</td>
                                                <td class="mdl-data-table__cell--non-numeric">
                                                    <g:if test="${name?.is_current_job}">
                                                        <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                    </g:if>
                                                    <g:else>
                                                        <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                    </g:else>
                                                </td>
                                                <td class="mdl-data-table__cell--non-numeric">${name?.designation}</td>
                                            </tr>
                                        </g:each>
                                    </tbody>
                                </table>
                             </td>
                        </tr>
                   </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
