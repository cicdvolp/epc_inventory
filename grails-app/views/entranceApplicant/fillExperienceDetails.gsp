<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
    function phonenumber(inputtxt) {
        var phoneno = /^\d{10}$/;
        if (inputtxt.value.match(phoneno)) {
            return true;
        } else {
            alert("Not a valid Phone Number");
            return false;
        }
    }
    function cuurent() {
        //alert(document.getElementById("cuurent_working").checked)
        if(document.getElementById("cuurent_working").checked)
        {
            document.getElementById("todate").disabled = true;  
        }
        if(!document.getElementById("cuurent_working").checked)
        {
            document.getElementById("todate").disabled = false;
        }
    }
</script>
<script type="text/javascript">
    if ("${cuurent_working}"=='true'){
       alert("${cuurent_working}");
       $("#todate").attr('disabled','disabled')
       $("#todate").attr('disabled','disabled')               
    }
</script>

<style>
    legend {
        border-style: none;
        border-width: 0;
        font-size: 20px;
        line-height: 40px;
        background-color: gainsboro;
        margin-bottom: 0;
        width: auto;
        padding: 0 10px;
        border: 1px solid #e0e0e0;
    }

    fieldset {
        border: 1px solid #e0e0e0;
        padding: 10px;
    }
</style>
<g:form controller="EntranceApplicant" name="myform" action="saveExperienceDetails">
    <input type="hidden" name="EAID" value="${EA?.id}">
    <input type="hidden" name="contact_tab" value="Experience Details">
    <fieldset>
        <legend> Add Experience Details </legend>
        <br/>
        <div class=" row">
            <div class="col-sm-4">
                Experience Type:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                <g:select name="RecExperienceType"  optionValue="type" from="${RecExperienceType}" style="width: 100% !important;"  class="form-control select2" required="true"  value=""  noSelection="['null':'Select Experience Type']" optionKey="id"/>
            </div>
            <div class="col-sm-4">
                Name Of Organization:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                <input type="text" class="form-control" name="organization" required="true" value="">
            </div>
            <div class="col-sm-4">
                Designation:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                <input type="text" class="form-control" name="designation" required="true" value="">
            </div>
        </div>
        <div class=" row">
            <div class="col-sm-4">
                <br>
                <g:checkBox name="cuurent_working" id="cuurent_working" value="${false}" onclick="cuurent();" />
                Is Currently Working?
            </div>
            <div class="col-sm-4">
                From Date:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                <input type="date" class="form-control" name="fromdate" id="fromdate">
            </div>
            <div class="col-sm-4">
                To Date:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                <input type="date" class="form-control " name="todate" id="todate">
            </div>
        </div>
        <center>
            <br>
            <button type="submit" class="btn btn-primary text-center">Save</button>
        </center>
    </fieldset>
</g:form>
<br>
<br>
<fieldset>
    <legend> Experience Details List</legend>
    <br/>
    <table class="mdl-data-table ml-table-bordered">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                <th class="mdl-data-table__cell--non-numeric">Experience Type</th>
                <th class="mdl-data-table__cell--non-numeric">Name Of Organization</th>
                <th class="mdl-data-table__cell--non-numeric">From Date</th>
                <th class="mdl-data-table__cell--non-numeric">To Date</th>
                <th class="mdl-data-table__cell--non-numeric">Is this Current Job?</th>
                <th class="mdl-data-table__cell--non-numeric">Designation</th>
                <th class="mdl-data-table__cell--non-numeric">Delete</th>
            </tr>
        </thead>
        <tbody>
            <g:each in="${EntranceApplicantExperience}" var="name" status="i">
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.recexperiencetype?.type}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.organization_name}</td>
                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate date="${sdf?.parse(name?.from_date)}" format="dd-MMM-yyyy"/></td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <g:if test="${name?.to_date}">
                            <g:formatDate date="${sdf?.parse(name?.to_date)}" format="dd-MMM-yyyy"/>
                        </g:if>
                        <g:else>
                        </g:else>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <g:if test="${name?.is_current_job}">
                            <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                        </g:if>
                        <g:else>
                            <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                        </g:else>
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.designation}</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <g:link controller="EntranceApplicant" action="deleteExperienceDetails" params="[EAA : name?.id]">
                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                        </g:link>
                    </td>
                </tr>
            </g:each>
        </tbody>
    </table>
</fieldset>
