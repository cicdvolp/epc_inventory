<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
    legend {
        border-style: none;
        border-width: 0;
        font-size: 20px;
        line-height: 40px;
        background-color: gainsboro;
        margin-bottom: 0;
        width: auto;
        padding: 0 10px;
        border: 1px solid #e0e0e0;
    }

    fieldset {
        border: 1px solid #e0e0e0;
        padding: 10px;
    }
</style>
<g:form controller="EntranceApplicant" name="myform" action="saveQualificationDetails">
    <input type="hidden" name="EAID" value="${EA?.id}">
    <input type="hidden" name="contact_tab" value="Qualification Details">
    <fieldset>
        <legend>Add Qualification Details </legend>
        <br/>
        <div class=" row">
            <div class="col-sm-3">
                <div class="alert alert-warning" >
                    <strong>Compulsory Degree : </strong>
                    <br>
                    <ul>
                        <g:each in="${entranceDegree}" var="deg" status="z">
                            <g:if test="${deg?.iscompulsory}">
                                <li>${deg?.name}</li>
                            </g:if>
                        </g:each>
                    </ul>
                </div>
            </div>
            <div class="col-sm-9">
                <div class=" row">
                    <div class="col-sm-4">
                        Degree:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                        <g:select name="entranceDegree"  optionValue="name" from="${entranceDegree}" style="width: 100% !important;"  class="form-control select2" required="true"  value=""  noSelection="['null':'Select Degree']" optionKey="id"/>
                    </div>
                    <div class="col-sm-4">
                        Class/Grade:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                        <g:select name="entranceClass"  optionValue="name" from="${entranceClass}" style="width: 100% !important;"  class="form-control select2" required="true"  value=""  noSelection="['null':'Select Class']" optionKey="id"/>
                    </div>
                    <div class="col-sm-4">
                        Percentage/CPI/CGPA:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                        <input type="text" class="form-control" name="cpi" required="true" value="">
                    </div>
                </div>
                <div class=" row">
                    <div class="col-sm-4">
                        Year Of Passing:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                        <input type="text" class="form-control" name="yrofpass" required="true" value="">
                    </div>
                    <div class="col-sm-4">
                        University/Board:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                        <input type="text" class="form-control" name="board" required="true" value="">
                    </div>
                    <div class="col-sm-4">
                        Branch:
                        <input type="text" class="form-control" name="branch" value="">
                    </div>
                </div>
                <div class=" row">
                    <div class="col-sm-4">
                        Specialization:
                        <input type="text" class="form-control" name="specialization" value="">
                    </div>
                </div>
            </div>
        </div>
        <center>
            <br>
            <button type="submit" class="btn btn-primary text-center">Save</button>
        </center>
    </fieldset>
</g:form>
<br>
<br>


<fieldset>
    <legend> Qualification Details List</legend>
    <br/>
    <table class="mdl-data-table ml-table-bordered">
        <thead>
            <tr>
                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                <th class="mdl-data-table__cell--non-numeric">Degree</th>
                <th class="mdl-data-table__cell--non-numeric">Class/Grade</th>
                <th class="mdl-data-table__cell--non-numeric">Percentage?CPI</th>
                <th class="mdl-data-table__cell--non-numeric">Year Of Passing</th>
                <th class="mdl-data-table__cell--non-numeric">University/Board</th>
                <th class="mdl-data-table__cell--non-numeric">Branch</th>
                <th class="mdl-data-table__cell--non-numeric">Specialization</th>
                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                <th class="mdl-data-table__cell--non-numeric">Delete</th>
            </tr>
        </thead>
        <tbody>
            <g:each in="${EntranceApplicantAcademics}" var="name" status="i">
                <tr>
                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.entrancedegree?.name}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.recclass?.name}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.cpi_marks}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.yearofpassing}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.university}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.branch}</td>
                    <td class="mdl-data-table__cell--non-numeric">${name?.specialization}</td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>
                        <!-- Modal edit -->
                        <div class="modal fade" id="editModel${i}" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="col-sm-12">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Edit Qualification Details</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form controller="EntranceApplicant" action="editQualificationDetails">
                                            <div class="modal-body">
                                                <div class=" row">
                                                    <div class="col-sm-12">
                                                        Degree:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                                                        <g:select name="entranceDegree"  optionValue="name" from="${entranceDegree}" class="select2" required="true"  value="${name?.entrancedegree?.id}"  noSelection="['null':'Select Degree']" optionKey="id"/>
                                                    </div>
                                                    <div class="col-sm-12">
                                                        class:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                                                        <g:select name="entranceClass"  optionValue="name" from="${entranceClass}" class="select2" required="true"  value="${name?.recclass?.id}"  noSelection="['null':'Select Class']" optionKey="id"/>
                                                    </div>
                                                    <div class="col-sm-6">
                                                        Percentage/CPI:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                                                        <input type="text" class="form-control" name="cpi" required="true" value="${name?.cpi_marks}">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        Year Of Passing:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                                                        <input type="text" class="form-control" name="yrofpass" required="true" value="${name?.yearofpassing}">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        University Board:<label style="color:red;font-size:12px;font-style:bold;">*</label>
                                                        <input type="text" class="form-control" name="board" required="true" value="${name?.university}">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        Branch:
                                                        <input type="text" class="form-control" name="branch" value="${name?.branch}">
                                                    </div>
                                                    <div class="col-sm-6">
                                                        Specialization:
                                                        <input type="text" class="form-control" name="specialization" value="${name?.specialization}">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="hidden" name="entranceApplicantAcademics" id="entranceApplicantAcademics" value="${name?.id}"/>
                                                <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--End Modal edit -->
                    </td>
                    <td class="mdl-data-table__cell--non-numeric">
                        <g:link controller="EntranceApplicant" action="deleteQualificationDetails" params="[EAA : name?.id]">
                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                        </g:link>
                    </td>
                </tr>
            </g:each>
        </tbody>
    </table>
</fieldset>