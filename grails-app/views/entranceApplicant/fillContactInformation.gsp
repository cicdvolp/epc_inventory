<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<style>
    legend {
        border-style: none;
        border-width: 0;
        font-size: 20px;
        line-height: 40px;
        background-color: gainsboro;
        margin-bottom: 0;
        width: auto;
        padding: 0 10px;
        border: 1px solid #e0e0e0;
    }

    fieldset {
        border: 1px solid #e0e0e0;
        padding: 10px;
    }
</style>
<script>
    function sameaddress() {
        if(document.getElementById("myCheckbox").checked)
        {
            document.getElementById("permanent_address").value=document.getElementById("local_address").value;
        }
        if(!document.getElementById("myCheckbox").checked)
        {
            document.getElementById("permanent_address").value="";
        }
    }
</script>
<g:form controller="EntranceApplicant" name="myform" action="saveEAContactDetails" >
    <input type="hidden" name="EAID" value="${EA?.id}">
    <input type="hidden" name="contact_tab" value="Contact Details">
    <fieldset>
    <legend> Contact Details </legend>
    <br />
   <div class=" row">
        <div class="col-sm-4">
            Mobile Number<label style="color:red;font-size:12px;font-style:bold;">*</label> :
            <g:if test="${EA.mobilenumber==null}">
                <g:textField class="form-control" name="mobilenumber"/>
            </g:if>
            <g:else>
                <g:textField class="form-control" name="mobilenumber" value="${EA.mobilenumber}" readonly="readonly"/>
            </g:else>
        </div>
        <div class="col-sm-4">
            Aadhar Number:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <input type="text" class="form-control" name="aadhar_number" id="aadhar_number" value="${EA.aadhar_number}"/>
            <label id="ac_error"></label>
        </div>
   </div>
   <div class=" row">
        <div class="col-sm-12">
            Local Address:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:textArea class="form-control" name="local_address" value="${EA.local_address}" rows="2" cols="47"/>
        </div>
        <div class="col-sm-12">
            <br>
            <label style="background:#FFFFFF; color:orange;"><b>Whether Permanant Address is Same as Current Address?</b> </label>&nbsp;&nbsp;&nbsp;<g:checkBox name="myCheckbox" id="myCheckbox" value="${false}" onclick="sameaddress();" />
        </div>
        <div class="col-sm-12">
            Permenant Address:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:textArea class="form-control" name="permanent_address" value="${EA.permanent_address}" rows="2" cols="47"/>
        </div>
    </div>
    <div>
        <center>
            <br>
            <button type="submit" class="btn btn-primary text-center" ><span class="glyphicon glyphicon-save"></span> Save</button>
        </center>
    </div>
</g:form>
    </fieldset>
<script>
    function adharcardno() {
        var a = document.getElementById("aadhar_number").value;
        if (a.length == 16) {
            alert("invalid Aadharcard Number!!!");
            //document.getElementById("ac_error").innerHTML = "<span style='color:red; font-size:10px !important;'>Aadhar card no must be in 16 digits</span>";
            return false;
        }
    }
</script>
<script>
    function phonenumber(inputtxt) {
        var phoneno = /^\d{10}$/;
        if (inputtxt.value.match(phoneno)) {
            return true;
        } else {
            alert("Not a valid Phone Number");
            return false;
        }
    }
</script>

<script>
    $(function() {
    $("#adharno").change(function() {
        //var number= $("#adharno").val();
        if ($(number.length != 12) {
                alert("Invalid  Adhar Number !!" + number.length);
                $("#adharno").focus();
                $("#adharno").val("");
                //$("#adharno").focus();
            } else {
                alert("valid  Adhar Number");
            }
        });
    });
</script>