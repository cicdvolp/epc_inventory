<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script>
    function phonenumber(inputtxt) {
        var phoneno = /^\d{10}$/;
        if (inputtxt.value.match(phoneno)) {
            return true;
        } else {
            alert("Not a valid Phone Number");
            return false;
        }
    }
</script>

<style>
    legend {
        border-style: none;
        border-width: 0;
        font-size: 20px;
        line-height: 40px;
        background-color: gainsboro;
        margin-bottom: 0;
        width: auto;
        padding: 0 10px;
        border: 1px solid #e0e0e0;
    }

    fieldset {
        border: 1px solid #e0e0e0;
        padding: 10px;
    }
</style>
<g:form controller="EntranceApplicant" name="myform" action="saveEAPersonalDetails" >
    <input type="hidden" name="EAID" value="${EA?.id}">
    <input type="hidden" name="Personal_tab" value="Personal Details">

    <fieldset>
    <legend> Personal Details </legend>
    <br/>
    <div class="row">
        <div class="col-sm-4">
            Full Name:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:textField class="form-control" name="fullname" value="${EA?.fullname}" readonly="readonly" />
        </div>
        <div class="col-sm-4">
            Email:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:textField class="form-control" name="email" value="${EA?.email}" readonly="readonly" />
        </div>
        <div class="col-sm-4">
            <label>Date Of Birth:</label>
            </br>
            <g:datePicker class="form-control" name="dateofbirth" value="${EA?.dateofbirth}" precision="day" years="${1950..2000}" />
        </div>
   </div>

    <div class=" row">
        <div class="col-sm-4">
            Gender:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:select name="gender"  optionValue="type" from="${genderList}" style="width: 100% !important;"  class="select2" required="true"  value="${EA?.gender?.id}"  noSelection="['null':'Select Gender']" optionKey="id"/>
        </div>
        <div class="col-sm-4">
            Marital Status:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:select name="maritalstatus"  optionValue="name" from="${maritalStatusList}" style="width: 100% !important;"  class="select2" required="true"  value="${EA?.maritalstatus?.id}"  noSelection="['null':'Select Marital Status']" optionKey="id"/>
        </div>
        <div class="col-sm-4">
            Category:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:select name="entrancecategory"  optionValue="name" from="${entranceCategoryList}" style="width: 100% !important;"  class="select2" required="true"  value="${EA?.entrancecategory?.id}"  noSelection="['null':'Select Entrance Category']" optionKey="id"/>
        </div>
    </div>
   <div class=" row">
        <div class="col-sm-4">
            Nationality:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:select name="erpnationality"  optionValue="type" from="${nationalityList}" style="width: 100% !important;"  class="select2" required="true"  value="${EA?.erpnationality?.id}"  noSelection="['null':'Select Nationality']" optionKey="id"/>
        </div>
         <div class="col-sm-4">
            Domacile:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:select name="erpdomacile"  optionValue="type" from="${erpdomacileList}" style="width: 100% !important;"  class="select2" required="true"  value="${EA?.erpdomacile?.id}"  noSelection="['null':'Select Domacile']" optionKey="id"/>
        </div>
        <div class="col-sm-4">
            Employment Status:<label style="color:red;font-size:12px;font-style:bold;">*</label>
            <g:select name="employmentstatus"  optionValue="name" from="${employmentstatusList}" style="width: 100% !important;"  class="select2" required="true"  value="${EA?.employmentstatus?.id}"  noSelection="['null':'Select Employment Status']" optionKey="id"/>
        </div>
   </div>
   <div class=" row">
        <div class="col-sm-4">
            <br>
            <g:if test="${EA.is_physically_handicapped}">
                <input type="checkbox" checked name="physical_handi" value="${EA.is_physically_handicapped}">
            </g:if>
            <g:else>
                <input type="checkbox" name="physical_handi" value="${EA.is_physically_handicapped}">
            </g:else>
            <label>Are you physically handicapped?:</label>
        </div>
         <div class="col-sm-4">
            <br>
            <g:if test="${EA.is_visually_handicapped}">
                <input type="checkbox" checked name="visually_handi" value="${EA.is_visually_handicapped}">
            </g:if>
            <g:else>
                <input type="checkbox" name="visually_handi" value="${EA.is_visually_handicapped}">
            </g:else>
            <label>Are you visually handicapped?:</label>
        </div>
   </div>
   <div>
    <br>
    <center>
        <button type="submit" class="btn btn-primary text-center" onclick="adharcardno();">Save</button>
    </center>
</g:form>
    </fieldset>
<script>
    function adharcardno() {
        alert()
    }
    var a = document.getElementById("adharno").value;
    if (a.length == 12) {
        alert("invalid Adharcard Number!!!");
        return false;
    }
    }
</script>

<script>
    $(function() {
    $("#adharno").change(function() {
        //var number= $("#adharno").val();
        if ($(number.length != 12) {
                alert("Invalid  Adhar Number !!" + number.length);
                $("#adharno").focus();
                $("#adharno").val("");
                //$("#adharno").focus();
            } else {
                alert("valid  Adhar Number");
            }
        });
    });
</script>