<html>
<head>
    <meta name="layout" content="ERPmainTemplateT2">
    <style>
        #spinner {
          position: fixed;
          top: 50%;
          left: 50%;
          margin-left: -50px; // half width of the spinner gif
          margin-top: -50px; // half height of the spinner gif
          z-index: 5000;
          overflow: auto;
        }
    </style>
</head>
    <input type="hidden" name="EAID" value="${EA?.id}">
    <input type="hidden" name="contact_tab" value="Upload Document">
    <fieldset>
        <legend>Document Upload</legend>
        <div class=" row">
            <div class="col-md-4">
                <div class="panel btn-warning disabled">
                    <div class="panel-heading">Document Should be in PDF format only. Size of document should be less than or equal to 200KB. </div>
                </div>
            </div>
            <div class="col-sm-4">
                <div class="alert alert-warning">
                    <strong>Compulsary document List :</strong><br>
                    <g:each in="${compulsarydocumentlist}" var="doc">
                        ${doc?.name}<br>
                    </g:each>
                </div>
            </div>
            <div class="col-sm-12">
                <table class="mdl-data-table ml-table-bordered">
                    <thead>
                        <tr>
                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                            <th class="mdl-data-table__cell--non-numeric">Document Type</th>
                            <th class="mdl-data-table__cell--non-numeric">File is Compulsary?</th>
                            <th class="mdl-data-table__cell--non-numeric">File Upload</th>
                            <th class="mdl-data-table__cell--non-numeric">Document</th>
                            <th class="mdl-data-table__cell--non-numeric">Successfully Uploaded</th>
                        </tr>
                    </thead>
                    <tbody>
                        <g:each in="${EntranceDocumentType}" var="name" status="i">
                            <tr>
                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                <td class="mdl-data-table__cell--non-numeric">${name?.type}</td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <g:if test="${name}">
                                        <i class='fa fa-check fa-2x' style='color:green' aria-hidden='true'></i>
                                    </g:if>
                                    <g:else>
                                        <i class='fa fa-close fa-2x' style='color:red' aria-hidden='true'></i>
                                    </g:else>
                                </td>
                                <g:form action="saveDocumentDetails" controller="EntranceApplicant" enctype="multipart/form-data">
                                <td class="mdl-data-table__cell--non-numeric">
                                    <span><input type="file" name="newFile" id="photo" accept=".pdf" class="btn btn-primary" onchange="loadFilephto(event);Upload()"/></span>
                                    <span><center><input type="submit" value="Upload" style="cursor: pointer;" class="btn-primary" ></center></span>
                                    <input type="hidden" name="EntranceDocumentType" id="EntranceDocumentType" value="${name?.type?.id}"/>
                                </td>
                                </g:form>
                                <td class="mdl-data-table__cell--non-numeric">
                                <g:if test="${name?.document}">
                                <g:include controller="entranceApplicant" action="getdocumenturl" params="[filepath:name?.document?.filepath, filename:name?.document?.filename]" />
                                </g:if>
                                </td>
                                <td class="mdl-data-table__cell--non-numeric">
                                    <g:if test="${name?.document}">
                                        <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                    </g:if>
                                    <g:else>
                                        <i class="fa fa-close fa-2x" style="color:red" aria-hidden="true"></i>
                                    </g:else>
                                </td>
                            </tr>
                        </g:each>
                    </tbody>
                </table>
            </div>
        </div>
    </fieldset>
<br>

<!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->

<script type="text/javascript">
    function validateFileType(x){
        var fileName = x.value;
        var idxDot = fileName.lastIndexOf(".") + 1;
        var extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
        if (extFile=="pdf"){
            //TO DO
        }else{
            alert("Only pdf file are allowed..!");
            x.value=null;
        }
    }
</script>

<script>
  var loadFilephto = function(event) {
    var output = document.getElementById('outputphoto');
    output.src = URL.createObjectURL(event.target.files[0]);
  };
</script>
<script>
    $(document).ready(function() {
        $("#outputphoto").hide();
        var _URL = window.URL || window.webkitURL;
        $("#photo").change(function(e) {
            var file, img;
            var file_size = $('#photo')[0].files[0].size;
            if ((file = this.files[0]))
            {
                img = new Image();
                img.onload = function()
                {
                    if(file_size<=2000000)
                    {
                        $("#outputphoto").show();
                    }
                    else
                    {
                        alert("File size=200kb.");
                        $("#outputphoto").hide();
                        $("#photo").val(null);
                    }
                };
                img.onerror = function() {
                    alert( "not a valid file: " + file.type);
                };
                img.src = _URL.createObjectURL(file);
            }
        });
    });
</script>
</html>