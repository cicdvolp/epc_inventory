<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_inst"/>
        <meta name="author" content="reena"/>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">
                                        Approving Authority : ${invWriteOffRequestEscalation?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}, Category : ${invWriteOffRequestEscalation?.invfinanceapprovingauthoritylevel?.invfinanceapprovingcategory?.name} <!-- ?.name -->
                                    </header>
                                </div>
                                <g:if test="${next_level_approved == false}">
                                     <div class="card-body">
                                                                            <g:form controller="inventoryManage" action="saveApproveWriteOff">
                                                                                <input type="hidden" name="woId" value="${invWriteOffRequestEscalation?.id}" />
                                                                                <div class="row">
                                                                                    <div class="col-sm-4">
                                                                                        <label>Remark : </label>
                                                                                        <input type="text" name="remark" id="remark" class="form-control" />
                                                                                    </div>
                                                                                    <div class="col-sm-4">
                                                                                        <label>Approving Status : </label>
                                                                                        <g:select name="approvingstatus" optionValue="name" from="${invapprovalstatus_list}" class="form-control select2" required="true" optionKey="id"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-sm-12">
                                                                                        <br>
                                                                                        <center>
                                                                                            <g:submitToRemote url="[action: 'saveApproveWriteOff', controller:'inventoryManage']" update="approve" value="Update" class="btn btn-primary" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                                                                        </center>
                                                                                    </div>
                                                                                </div>
                                                                            </g:form>
                                                                        </div>
                                </g:if>
                                <g:else>
                                 <span style="margin-left:20px" >Cannot Update, Already Approved By Higher Authority.</span>
                                </g:else>
                            </div>
                        </div>
                    </div>
                    <div id="approve">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">
                                            Approving History for Write Off Request : ${invWriteOffRequestEscalation?.invwriteoffrequest?.inventory?.accession_number} - ${invWriteOffRequestEscalation?.invwriteoffrequest?.reason}
                                        </header>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="mdl-data-table ml-table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="mdl-data-table__cell--non-numeric">Level No</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Approving Authority</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Approving Status</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Action Date</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Approved By</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <g:each in="${writeoffEscalation_list}" var="wo" status="k">
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${wo?.invfinanceapprovingauthoritylevel?.level_no}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${wo?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</td>
                                                            <g:if test="${wo?.invapprovalstatus?.name != 'In-Process'}">
                                                                <td class="mdl-data-table__cell--non-numeric">${wo?.invapprovalstatus?.name}</td>
                                                                <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd/MM/yyyy" date="${wo?.action_date}"/></td>
                                                                <td class="mdl-data-table__cell--non-numeric">${wo?.remark}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${wo?.actionby}</td>
                                                            </g:if>
                                                            <g:else>
                                                                <td class="mdl-data-table__cell--non-numeric">Not Approved</td>
                                                                <td class="mdl-data-table__cell--non-numeric">-</td>
                                                                <td class="mdl-data-table__cell--non-numeric">-</td>
                                                                <td class="mdl-data-table__cell--non-numeric">-</td>
                                                            </g:else>
                                                        </tr>
                                                    </g:each>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>