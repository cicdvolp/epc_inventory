<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>

                            <script>
                            function transfer()
                                       {
                                             var transfer = document.getElementById("transferLevel").value;
                                             <!--alert(transfer)-->
                                              var xmlhttp = new XMLHttpRequest();
                                            xmlhttp.onreadystatechange = function() {
                                                if (this.readyState == 4 && this.status == 200) {
                                                    document.getElementById("transfer").innerHTML = this.responseText;
                                                }
                                            };
                                            xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getTransferTo?transfer=" + transfer);
                                            xmlhttp.send();
                                       }
                            </script>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">View Inventory Details</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>

                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">View Inventory Details</li>
                        </ol>
                    </div>
                </div>
              <!--message and error-->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
              <!--////-->
              
              
                 <!--Table -->
                <div class="row">
                            <div class="col-md-12">

                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Inventory List</header>
                                   </div>
                                    <div class="card-body " id="bar-parent">
                                        <div class="table-responsive">
                                            <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr no</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Accession Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material Part</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Room Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Department</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Inward Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purchase Cost</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Depreciation Value</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Write Down Value</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IsActive</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Transfer</th>

                                                   
                                                    <!--<th class="mdl-data-table__cell--non-numeric">Delete</th>-->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${inventoryList}" var="inventory" status="i">
                                                    <tr>
                                                        
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.accession_number}</td>
                                                       <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterial?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterialpart?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invroom?.number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.department?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${inventory?.inward_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.purchase_cost}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.depreciation_value}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.written_down_value}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:link controller="inventoryManage" action="activateInventory" params="[InventoryId : inventory?.id]">
                                                            <g:if test="${inventory?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                        </td>
                                                        <td>
                                                        <i class="fa fa-edit  fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#editInv${i}"></i>

                                                        <!-- Modal edit -->
                                                      <div class="modal fade" id="editInv${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title">Edit Inventory</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="createTransferRequest">
                                                                <input type="hidden" name="inventoryId" value="${inventory?.id}">
                                                               <div class="modal-body">
                                                                        <div class="col-sm-10">
                                                                            <label>Inventory Accession Number:</label>
                                                                            <input type="text" name="accNo" readonly class="form-control"  value="${inventory?.accession_number}"/>
                                                                        </div>
                                                                        <br>
                                                                        <div class="col-sm-10">
                                                                            <label>Inventory Current Department:</label>
                                                                            <input type="text" name="oldDept" readonly class="form-control"  value="${inventory?.department?.name}"/>
                                                                        </div>
                                                                        <br>
                                                                        <div class="col-sm-10" id="transfer">
                                                                             <label for="pwd">New Department:</label>
                                                                                    <span style="color:red; font-size: 16px">*</span>
                                                                                    <g:select name="dept" optionValue="name" from="${deptList}" style="width: 60% !important;"  class="select2" required="true" optionKey="id" noSelection="['null':'Select Department']"/>
                                                                         </div>
                                                                 <br>
                                                               </div>
                                                                <div class="modal-footer">
                                                                  <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span>Request Transfer</button>
                                                                </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                        </td>
                                                    </tr>
                                                
                                                </g:each>
                                                </tbody>
                                               </table>

                                         </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                  </div>
                </div>
  
               
  
  
</body>
</html>









