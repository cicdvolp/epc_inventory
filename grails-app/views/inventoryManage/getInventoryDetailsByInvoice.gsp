 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Save Inventory</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                     <g:form action="saveInventoryDetails">
                                     <input type="hidden" value="${invoice?.id}" name="invoiceId">
                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                    <label>Department:</label>
                                                    <span style="color:red; font-size: 16px">*</span>
                                                     <g:select id="department" name="department"  optionKey="id" optionValue="${{it.name}}" from="${departmentList}" class="form-control select2" required="true" noSelection="['null':'Select Department']" onChange="getRoomList()"/>
                                                   </div>
                                                 <div class="col-sm-4">
                                                     <label>Material Name:</label>
                                                     <span style="color:red; font-size: 16px">*</span>
                                                      <g:select id="material" name="material"  optionKey="id" optionValue="${{it.name}}" from="${materialList}" class="form-control select2" required="true" noSelection="['null':'Select Material']" onChange="getMaterialDetails()"/>
                                                  </div>
                                                <div class="col-sm-4" id="roomlist">
                                                 </div>
                                            </div><br>
                                            <div class="form-row" id ="matDetails">

                                            </div><br>
<!--
                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                         <label>Accession Number:</label>
                                                         <input type="text" name="accNo" readonly class="form-control"  value="${invAccNumber}"/>
                                                 </div>
                                            </div>
-->
                                            <div class="row">
                                                 <div class="col-sm-4">
                                                        <label>Inward Date:</label>
                                                        <span style="color:red; font-size: 16px">*</span><br>
                                                        <g:datePicker class="form-control" name="inwardDate" value="${new Date()}" precision="day"/>
                                                </div>
                                                 <div class="col-sm-4">
                                                         <label>Purchase Cost Including Tax:</label>
                                                         <span style="color:red; font-size: 16px">*</span>
                                                         <input type="text" name="purchaseCost" placeholder="Enter Purchase Cost"  class="form-control" required="true"/>
                                                </div>
                                                <div class="col-sm-4">
                                                     <label>Quantity:</label>
                                                     <span style="color:red; font-size: 16px">*</span>
                                                     <input type="number" min="1" name="addQty" placeholder="Enter Quantity"  class="form-control" required="true"/>
                                                </div>

                                            </div>
                                      <center>
                                            <div class="form-row mx-auto my-2">
                                                   <button id="btnSubmit" type="submit" onclick="this.disabled=true;this.value='Saving, please wait...';this.form.submit();" class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                            </div>
                                       </center>
                                     </g:form>
                                   </div>
                        </div>
                          </div> 
                            </div>    