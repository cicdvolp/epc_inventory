<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
    </head>
     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Material inspection</header>
                                </div>
                                <div class="card-body " id="bar-parent">
                                   <div>
                                         <g:form action="saveMaterialInspection">
                                                <div class="row">
                                                     <div class="col-sm">
                                                        <label for="rating">Note : </label>
                                                        <span style="color:red; font-size: 16px">*</span>
                                                        <label for="rating">Values mandatory for <strong>Invoice</strong> to get Approved</label>
                                                     </div>
                                                 </div><br>
                                              <div class="row">
                                                  <div class="col-sm-6">
                                                       <g:checkBox name="is_inspected" value="${is_inspected}"/>
                                                       <label><strong>Is Material inspected? </strong><span style="color:red; font-size: 16px">*</span></label><br>
                                                       <g:checkBox name="is_material_inspected_tested_installed" value="${is_material_inspected_tested_installed}"/>
                                                       <label><strong>Is Material inspected,tested,installed? </strong><span style="color:red; font-size: 16px">*</span></label><br>
                                                       <g:checkBox name="is_training_given" value="${is_training_given}"/>
                                                       <label>Is training provided by vendor?</label><br>
                                                       <g:checkBox name="is_original_copies_of_invoice_collected" value="${is_original_copies_of_invoice_collected}"/>
                                                       <label>Is original copies of invoice collected?</label><br>
                                                       <g:checkBox name="is_delivery_challen_collected" value="${is_delivery_challen_collected}"/>
                                                       <label>Is delivery challen collected?</label><br>
                                                       <g:checkBox name="is_test_certificate_received" value="${is_test_certificate_received}"/>
                                                       <label>Is test certificate received?</label><br>
                                                  </div>
                                                  <div class="col-sm-6">
                                                        <g:checkBox name="is_warranty_card_supplied" value="${is_warranty_card_supplied}"/>
                                                        <label><strong>Is warranty card supplied?</strong> <span style="color:red; font-size: 16px">*</span></label><br>
                                                        <g:checkBox name="is_amc_certificate_provided" value="${is_amc_certificate_provided}"/>
                                                        <label><strong>Is amc certificate provided? </strong><span style="color:red; font-size: 16px">*</span></label><br>
                                                        <g:checkBox name="is_technical_demonstration_done" value="${is_technical_demonstration_done}"/>
                                                        <label>Is technical demonstration done?</label><br>
                                                        <g:checkBox name="is_user_manual_provided" value="${is_user_manual_provided}"/>
                                                        <label>Is user manual provided?</label><br>
                                                        <g:checkBox name="is_installation_manual_provided" value="${is_installation_manual_provided}"/>
                                                        <label>Is installation manual provided?</label><br>
                                                        <g:checkBox name="is_maintainance_manual_provided" value="${is_maintainance_manual_provided}"/>
                                                        <label>Is maintainance manual provided?</label><br>
                                                  </div>
                                              </div><br>
                                              <div class="row">
                                                  <div class="col-sm">
                                                      <center>
                                                     <label for="rating">Remarks : </label>
                                                     <g:field id="remark" name="remark" type="text" min="0" style="width: 50% !important;"/><br>
                                                     </center>
                                                  </div>
                                              </div><br>
                                              <center>
                                                <div>
                                                     <input type="hidden" value="${invoice1?.id}" name="invoiceId">
                                                     <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span>Save</button>
                                                </div>
                                              </center>
                                         </g:form>
                                    <div>
                                 </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </body>
      </html>


