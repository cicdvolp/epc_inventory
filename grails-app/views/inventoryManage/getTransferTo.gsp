
<g:if test="${deptFlag}">
        <label for="pwd">New Department:</label>
        <span style="color:red; font-size: 16px">*</span>
        <g:select name="dept" optionValue="name" from="${deptList}" style="width: 60% !important;"  class="select2" required="true" optionKey="id" noSelection="['null':'Select Department']"/>
</g:if>
<g:else>
             <label>Transfer To:</label>
             <span style="color:red; font-size: 16px">*</span>
             <input type="text" name="transferOrg" placeholder="Enter Organization Name To Transfer Inventory" style="width:100%!important;" required="true"/>
</g:else>
