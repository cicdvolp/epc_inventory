<div class="col-sm-3">
<label>Material Part:</label>
    <input type="text" name="materialPart" class="form-control" readonly value="${materialPartList?.name}">
</div>
<div class="col-sm-3">
    <label>Material Cost:</label>
    <input type="text" name="cost" readonly class="form-control" value="${materialCost}"/>
</div>
<div class="col-sm-3">
    <label>Avaliable Quantity:</label>
    <input type="text" name="avaQuantity" readonly class="form-control"  value="${materialQty}"/>
</div>
<div class="col-sm-3">
    <label>Depreciation Percentage:</label>
    <input type="text" name="depreciation" readonly class="form-control" value="${depreciation?.percentage}"/>
</div>