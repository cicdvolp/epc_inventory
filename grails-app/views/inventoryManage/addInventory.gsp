<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
        
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>

        <script>
           function getInventoryForm()
           {
                 var materialName = document.getElementById("MaterialName").value;
                var department=document.getElementById("Department").value 
                 var materialPart=document.getElementById('MaterialPart').value
                  var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("InventoryForm").innerHTML = this.responseText;
                    }
                };
                xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getInventoryDetailsByMaterialName?material=" + materialName +"&department="+department+"&materialPart="+materialPart);
                xmlhttp.send();
           }

           function getReciepts(){
                           var department=document.getElementById("Department").value
                           var xmlhttp = new XMLHttpRequest();
                           xmlhttp.onreadystatechange = function() {
                               if (this.readyState == 4 && this.status == 200) {
                                   document.getElementById("recieptList").innerHTML = this.responseText;
                               }
                           };
                           xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getRecieptsByDepartment?dept=" + department);
                           xmlhttp.send();
           }

           function getMaterialByReciept(){
                            var reciept=document.getElementById("reciept").value
                            var xmlhttp = new XMLHttpRequest();
                           xmlhttp.onreadystatechange = function() {
                               if (this.readyState == 4 && this.status == 200) {
                                   document.getElementById("materialList").innerHTML = this.responseText;
                               }
                           };
                           xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getMaterialByReciept?reciept=" + reciept);
                           xmlhttp.send();
           }
           function getMaterialDetails(){
                           var material=document.getElementById("material").value
                           var invoice=document.getElementById("invoice").value
                           var xmlhttp = new XMLHttpRequest();
                          xmlhttp.onreadystatechange = function() {
                              if (this.readyState == 4 && this.status == 200) {
                                  document.getElementById("matDetails").innerHTML = this.responseText;
                              }
                          };
                          xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getMaterialDetailsByMaterial?materialId=" + material+"&invoiceId="+invoice);
                          xmlhttp.send();
                      }
            function getRoomList()
            {
                               var dept=document.getElementById("department").value
                               var xmlhttp = new XMLHttpRequest();
                              xmlhttp.onreadystatechange = function() {
                                  if (this.readyState == 4 && this.status == 200) {
                                      document.getElementById("roomlist").innerHTML = this.responseText;
                                  }
                              };
                              xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getRoomListByDept?departmentId=" + dept);
                              xmlhttp.send();
            }
        </script>
    
                 
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Fields Selection-->
                 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Add Inventory</header>
                                     <g:link controller="inventoryManage" action="viewInventoryDetails" style="color:white;">   <button class="my-3 float-right mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">View Inventory</button></g:link> 
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                    
                                     <g:form action="getInventoryDetailsByMaterialName"> 
                                        <center>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <div class="form-group row">
                                                    <label class="col-form-label">Invoice Number:  <span style="color:red; font-size: 16px">*</span></label>

                                                        <g:select id="invoice" name="invoice"  optionKey="id" optionValue="${{it.invoice_number}}" from="${invoiceList}" class="form-control select2" required="true" noSelection="['null':'Select Invoice']"/>
                                                 </div>
                                            </div>

                                            <div class="col-sm-4" id="recieptList">

                                            </div>
                                            <div class="col-sm-4" id="materialList">

                                            </div>
                                         </div>
<div class="row">
                                              <div class="col-sm-12">
                                              <center>
                                              <br>
                                                 <g:submitToRemote class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" url="[action: 'getInventoryDetailsByInvoice']" update="InventoryForm" value="Fetch"/>
                                              </center>
                                              </div>

                                     </div>

                                       </center>
                                    </g:form>
                                  </div>  
                        </div>
                          </div> 
                            </div> 
                        
                        <!--Inventory Form-->
                   
                             <div id="InventoryForm">
                                <input type="hidden" name="reciept" class="form-control" readonly value="${invMaterial?.name}">
                                       
                             </div>
                
            </div>
        </div>
      
</body>
</html>



<!---->