<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
         <script>
                function callme() {
                                alert("in callme : "+dept)
                                var department= document.getElementById("dept").value;
                                var xmlhttp = new XMLHttpRequest();
                                xmlhttp.onreadystatechange = function() {
                                    if (this.readyState == 4 && this.status == 200) {
                                        document.getElementById("roledivid").innerHTML = this.responseText;
                                    }
                                };
                                xmlhttp.open("GET", "${request.contextPath}/InventoryManage/getRoomNobyDepartment?dept=" + department);
                                xmlhttp.send();
                           }
         </script>

    </head>

     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                   <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12 mobile-table-responsive">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Approve Write-off Request</header>
                                </div>
                                <div class="card-body " id="bar-parent">
                                    <div>
                                       <g:form action="approveWriteoffRequest" name="form">
                                           <div class="row">
                                               <div class="col-sm-4">
                                                   <label for="pwd">Approval Status:</label>
                                                   <g:select name="approvestatus" id="approvestatus"  from="${invApprovalStatusList?.name}"
                                                   style="width: 65% !important;"  class="select2"
                                                   optionValue="${approvestatus}"
                                                   noSelection="['null':'Please Select Approval Status']"
                                                     />
                                               </div>
                                               <div class="col-sm-4">
                                                  <label for="pwd">Department:</label>
                                                  <g:select name="dept" id="dept"  from="${department}"
                                                  style="width: 65% !important;"  class="select2"
                                                  optionValue="${dept}"
                                                  noSelection="['null':'Please Select Department']" onchange="callme()"
                                                    />
                                               </div>
                                               <div class="col-sm-4" id="roledivid">
                                                  <label for="pwd">Room Number:</label>
                                                  <g:select name="room" id="room"  from="${roomNo?.number}"
                                                  style="width: 65% !important;"  class="select2"
                                                  optionValue="${room}"
                                                  noSelection="['null':'Please Select Room Number']"
                                                    />
                                               </div>
                                           </div>
                                           <br>
                                             <center>
                                                 <div>
                                                    <button type="submit" class="btn btn-primary" >Fetch</button>
                                                 </div>
                                             </center>
                                       </g:form>
                                    </div>
                                 </div>
                               </div>
                            </div>
                            <div class="row">
                               <div class="col-md-12 mobile-table-responsive">
                                  <div class="card card-topline-purple full-scroll-table">
                                     <div class="table-responsive">
                                         <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                           <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Request Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Requested By</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Accession Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material </th>
                                                    <th class="mdl-data-table__cell--non-numeric">Material Part</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval status</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Inventory Details</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Approval</th>
                                                    <th class="mdl-data-table__cell--non-numeric">History</th>
                                                </tr>
                                           </thead>
                                           <tbody>
                                                 <g:each in="${invWriteOffRequestEscalation}" var="approveWO" status="i">
                                                     <tr>
                                                         <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                         <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${approveWO?.invwriteoffrequest?.request_date}"/></td>
                                                         <td class="mdl-data-table__cell--non-numeric">${approveWO?.actionby}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${approveWO?.invwriteoffrequest?.inventory?.accession_number}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${approveWO?.invwriteoffrequest?.inventory?.invmaterial?.name}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${approveWO?.invwriteoffrequest?.inventory?.invmaterialpart?.name}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${approveWO?.invapprovalstatus?.name}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                               <g:form action="inventoryDetailsAwo">
                                                                    <input type="hidden" value="${approveWO?.id}" name="approveWOId">
                                                                    <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                               </g:form>
                                                         </td>
                                                         <td class="mdl-data-table__cell--non-numeric">${approveWO?.remark}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                              <i class="fa fa-edit fa-2x erp-edit-icon-color" data-toggle="modal" data-target="#writeOffRequestApproval${i}"></i>
                                                         </td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                            <g:form action="writeOffApprovalhistory">
                                                                 <input type="hidden" value="${approveWO?.invwriteoffrequest?.id}" name="invwriteoffrequestId">
                                                                 <input type="hidden" value="${approveWO?.id}" name="approveWOId">
                                                                 <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                            </g:form>
                                                         </td>
                                                    </tr>
                                                    <div class="modal fade" id="writeOffRequestApproval${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title">Update Approval</h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                           <g:form action="writeOffRequestApproval">
                                                                <div class="modal-body">
                                                                      <div class="form-group">
                                                                          <input type="hidden" value="${approveWO?.id}" name="approveWOId">
                                                                          <input type="hidden" value="${approveWO?.invwriteoffrequest?.id}" name="invwriteoffrequestId">
                                                                       </div>
                                                                      <div>
                                                                        <label>Approval Status:</label>
                                                                            <g:select name="approvalStatus" optionKey="id" optionValue="name" from="${invApprovalStatusList}" style="width: 100% !important;"  class="select2" required="required"  value=""  noSelection="['null':'Select Approval Status']"/><br>
                                                                      </div>
                                                                      <div>
                                                                        <label>Remark:</label>
                                                                            <input type="text" class="form-control" name="approveWORemark" value="${approveWO?.remark}">
                                                                      </div>
                                                                 </div>
                                                                <div class="modal-footer">
                                                                  <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto" >Update</button>
                                                                </div>
                                                           </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                </g:each>
                                           </tbody>
                                         </table>
                                     </div>
                                  </div>
                               </div>
                            </div>

                </div>
            </div>
      </body>
</html>


