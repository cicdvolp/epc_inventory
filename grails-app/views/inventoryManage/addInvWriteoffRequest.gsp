<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
         <script>
                function callme() {
                                var invId= document.getElementById("inventoryId").value;
                                var xmlhttp = new XMLHttpRequest();
                                xmlhttp.onreadystatechange = function() {
                                    if (this.readyState == 4 && this.status == 200) {
                                        document.getElementById("roledivid").innerHTML = this.responseText;
                                    }
                                };
                                xmlhttp.open("GET", "${request.contextPath}/InventoryManage/getValuesByAccesionNumber?invId=" + invId);
                                xmlhttp.send();
                           }
         </script>

    </head>

     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <g:if test="${flash.error}">
                             <div class="alert alert-danger" >${flash.error}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                        <div class="card-head">
                                            <header class="erp-table-header">Add Write-off Request</header>
                                        </div>
                                       <div class="card-body " id="bar-parent">

                                            <g:form action="saveInvWriteoffRequestfetchdata" name="form">

                                               <div class="row">
                                                   <div class="col-sm-6">
                                                           <label>Accession Number : </label>
                                                           <g:select name="invId" id="inventoryId"  from="${inventoryAccNoList}"
                                                           class="form-control select2"
                                                           optionValue="${{it.accession_number}}" optionKey="id"
                                                           noSelection="['null':'Please Select Accession Number']" onchange="callme()" />
                                                   </div>
                                               <div>
                                               <hr>

                                            <div id="roledivid">

                                            </div>

                                        </g:form>
                                    </div>
                                </div>
                             </div>
                           </div>
                        </div>
                       </div>
                       </div>
                       <!--
                        <div class="row">
                            <div class="col-md-12 mobile-table-responsive">
                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="table-responsive">
                                     <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                       <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Accession Number</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Material Part</th>
                                                <th class="mdl-data-table__cell--non-numeric">Room No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Department</th>
                                                <th class="mdl-data-table__cell--non-numeric">Inward Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Write-off Request Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">DeadStock Status</th>
                                                <th class="mdl-data-table__cell--non-numeric">Write-off Request Status</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active</th>
                                            </tr>
                                        </thead>
                                        <!--
                                        <tbody>
                                             <g:each in="${invWriteOffRequest}" var="writeoffReq" status="i">
                                                 <tr>
                                                     <td class="mdl-data-table__cell--non-numeric">${i+1}</td>


                                                </tr>
                                             </g:each>
                                        </tbody>
                                        -->
                                     </table>
                                   </div>
                                 </div>
                               </div>
                             </div>
                          </div>
                    </div>
                </div>
            </div>
      </body>
</html>


