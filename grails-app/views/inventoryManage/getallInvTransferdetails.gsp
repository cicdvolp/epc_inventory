<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-purple">
            <div class="card-head">
                <header class="erp-table-header">
                    Category : ${invFinanceApprovingCategory?.name}, Approving Authority Post : ${invInstructorFinanceApprovingAuthority?.invfinanceapprovingauthority?.name}
                </header>
            </div>
            <!--<div class="card-body">-->
                <div class="table-responsive">
                    <table class="mdl-data-table ml-table-bordered">
                        <thead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                <th class="mdl-data-table__cell--non-numeric">Inventory Transfer Request </th>
                                <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${invTransferEscalationList}" var="ite" status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <span>Inventory Acc No: </span><span class="erp-table-header" style="font-weight:900;">${ite?.inventory?.accession_number}</span>
                                        <br>
                                        <span>Requested By: </span><span class="erp-table-header" style="font-weight:900;">${ite?.actionby?.person?.firstName} ${ite?.actionby?.person?.lastName} </span>
                                        <br>
                                        <span>Requested Date: </span><span class="erp-table-header" style="font-weight:900;"><g:formatDate format="dd/MM/yyyy" date="${ite?.creation_date}"/></span>
                                        <br>
                                        <span>Transfer To Department: </span><span class="erp-table-header" style="font-weight:900;">${ite?.department?.name} </span>
                                        <br>

                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:link target="blank" controller="inventoryManage" action="getInvTransferEscalationforApprove" params="[invTransferEscalationId:ite?.id]" class="btn btn-primary">Proceed</g:link>
                                    </td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
            <!--</div>-->
        </div>
    </div>
</div>