<g:form action="saveInvWriteoffRequestfetchdata" name="form">

<div class="row">
    <div class="col-sm-3">
          <label for="rating">Material Type :</label>
          <g:field id="materialtype" name="materialtype"  readonly="readonly" value="${materialtype}" class="form-control"/>
    </div>
    <div class="col-sm-3">
          <label for="rating">Material Category :</label>
         <g:field id="materialcategory" name="materialcategory"  readonly="readonly" value="${invmaterialcategory}" class="form-control" />
    </div>
    <div class="col-sm-3">
          <label for="invroom">Room Number</label>
          <g:field id="invroom" name="invroom" readonly="readonly" value="${inventory?.invroom?.number}" class="form-control"/>
    </div>
    <div class="col-sm-3">
          <label for="inventory">Inward date</label>
          <g:field id="inward_date" name="inward_date" readonly="readonly" value="${formatDate(format:'dd-MM-yyyy',date:inventory?.inward_date)}" class="form-control"/>
    </div>
<div>

<div class="row" style="left-margin=20%">
    <div class="col-sm-4">
        <label for="departmentS" >Department :</label>
        <g:field  id="department" name="department" readonly="readonly" value="${inventory?.department?.name}" class="form-control" />
    </div>
    <div class="col-sm-4">
        <label for="material">Material Name :</label>
        <g:field id="materialname" name="materialname" readonly="readonly" value="${materialname}" class="form-control"/>
    </div>
    <div class="col-sm-4">
        <label for="invmaterialpart">Material Part :</label>
        <g:field  id="materialpart" name="materialpart" readonly="readonly" value="${inventory?.invmaterialpart?.name}" class="form-control"/>
    </div>
</div>

<div class="row" >
     <div class="col-sm-6">
         <label for="comment">Reason</label><br>
         <textarea id="reason" name="reason" required="true"  class="form-control"></textarea>
    </div>
    <div class="col-sm-6">
      <label >Deadstock Status</label>
       <g:select name="status" from="${invdeadstockstatusList}"
       class="form-control select2"
        optionValue="${{it.status}}"
        optionKey="id"
        noSelection="['null':'Select Deadstock Status']"
        required="true"/><br>
    </div>
<div>
</div class="col-sm-6">
    <center>
        <div>
             <input type="hidden" name="accession_number" id="accession_number" value="${accession_number}" />
            <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
        </div>
    </center>
    </g:form>
</div>