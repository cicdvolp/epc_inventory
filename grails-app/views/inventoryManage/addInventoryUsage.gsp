<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
        
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    
     <!--Scripts--><!--invMaterial?.organization?.icard_organization_name-->
        <script>
        function callme() {
                var materialName = document.getElementById("MaterialName").value;
                var department=document.getElementById("Department").value
                window.alert(materialName +" Dept "+ department)
                /*alert("role_type : " + role_type + ", user_type : " + user_type);*/
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("secondSelect").innerHTML = this.responseText;
                        
                    }
                };
                xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getMaterialParts?material=" + materialName );
                xmlhttp.send();
           }
           function validate() {
               var department=document.getElementById('Department').value
             
               var materialName=document.getElementById('MaterialName').value
               var materialPart=document.getElementById('MaterialPart').value
                 window.alert("department"+ department + "materialName "+ materialName + "materialPart "+materialPart)
               if(department==null || materialName==null || materialPart==null)
               {
                   window.alert('Please select a department, a material, a material part and then click fetch')
                   return false;
                   
               }
               else{
                   window.alert(department+" "+materialName+" "+materialPart)
                   return true;
               }
           }

           function getInventoryForm()
           {
                 var materialName = document.getElementById("MaterialName").value;
                var department=document.getElementById("Department").value 
                 var materialPart=document.getElementById('MaterialPart').value
                  var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("InventoryForm").innerHTML = this.responseText;
                        
                    }
                    else{
                        window.alert(responseText)
                    }
                };
                xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getInventoryDetailsByMaterialName?material=" + materialName +"&department="+department+"&materialPart="+materialPart);
                xmlhttp.send();
           }
       /* function getInventoryDetails()
        {
          var materialName = document.getElementById("MaterialName").value;
          var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                      var data=this.responseText
                      window.alert(data)
                      document.getElementById("InvoiceNumber").innerHTML = data.invoice?.invoice_number;
                        
                    }
                };
                xmlhttp.open("GET", "${request.contextPath}/inventoryManage/getInventoryDetailsByMaterialName?material=" + materialName);
                xmlhttp.send();
           }
        }*/
        </script>
    
                 
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <div class="page-bar">
                    <div class="page-title-breadcrumb">
                        <div class=" pull-left">
                            <div class="page-title">Inventory Usage</div>
                        </div>
                        <ol class="breadcrumb page-breadcrumb pull-right">
                            <li>
                                <i class="fa fa-home"></i>
                                &nbsp;
                                <g:link controller="login" action="erphome" class="parent-item">Home</g:link>

                                &nbsp;
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li class="active">Inventory Usage</li>
                        </ol>
                    </div>
                </div>
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Fields Selection-->
                 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Enter Accession Number</header>
                                     <g:link controller="inventoryManage" action="viewInventoryDetails" style="color:white;">   <button class="my-3 float-right mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored">View Inventory</button></g:link> 
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                    
                                     <g:form action="getInventoryDetailsByMaterialName"> 
                                        <center>
                                        <div class="form-row">
                                         
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="inputPassword" class="col-sm-3 col-form-label">Department:</label>
                                                    <div class="col-sm-4">
                                                        <g:select id="Department" name="department" optionKey="id" optionValue="${department?.name}" from="${departmentList}" class="select2" required="true" value="${id}"  noSelection="['null':'Select Department']"/>
                                                    </div>
                                                 </div>
                                            </div>
                                         
                                         
                                        
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-3">Material Name:</label>
                                                     <div class="col-sm-6">
                                                         <g:select id="MaterialName" name="material" style="width:100%!important;" optionValue="${name}" from="${materialNamesList}" class="select2" required="true" noSelection="['null':'Select Material']"
                                                         onChange="callme()"/> <!--${remoteFunction(action:'getMaterialParts',update:'second',params:'\'material=\'+this.value')}-->
                                                     </div>
                                                 </div>
                                            </div>
                                           
                                          <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label class="col-sm-3">Material Part:</label>
                                                     <div class="col-sm-6" id="secondSelect">
                                                         
                                                     </div>
                                                 </div>
                                            </div>
                                           
                                          <div class="col">
                                             <g:submitToRemote class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" url="[action: 'getInventoryDetailsByMaterialName']" update="InventoryForm" value="Fetch"/>
                                            
                                          </div> 
                                           
                                     </div>
                                          
                                         
                                        
                                        </center>
                                    </g:form>
                                   <!---->
                                   
                                   
                                   <!---->
                                  </div>  
                        </div>
                          </div> 
                            </div> 
                        
                        <!--Inventory Form-->
                   
                             <div id="InventoryForm">
                                       
                                     </div>
                
            </div>
        </div>
      
</body>
</html>



<!---->