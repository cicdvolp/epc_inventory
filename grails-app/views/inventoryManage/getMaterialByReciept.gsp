<div class="form-group row">
<label class="col-form-label">Material Name:</label>
<g:select id="material" name="material" style="width:90%!important;" optionValue="${{it?.name}}" from="${materialList}" optionKey="id" class="select2" required="true" noSelection="['null':'Select Material']"/>
</div>