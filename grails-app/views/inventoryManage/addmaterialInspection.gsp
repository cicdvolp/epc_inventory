<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="rishabh"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
          <script>
                 function callme() {
                                 var academicyear= document.getElementById("ay").value;
                                 var xmlhttp = new XMLHttpRequest();
                                 xmlhttp.onreadystatechange = function() {
                                     if (this.readyState == 4 && this.status == 200) {
                                         document.getElementById("roledivid").innerHTML = this.responseText;
                                     }
                                 };
                                 xmlhttp.open("GET", "${request.contextPath}/InventoryManage/getValuesByAcademicyear?ay=" + academicyear);
                                 xmlhttp.send();
                            }
                 function call() {
                                  var poNumber= document.getElementById("poNo").value;
                                  var xmlhttp = new XMLHttpRequest();
                                  xmlhttp.onreadystatechange = function() {
                                      if (this.readyState == 4 && this.status == 200) {
                                          document.getElementById("rolediv").innerHTML = this.responseText;
                                      }
                                  };
                                  xmlhttp.open("GET", "${request.contextPath}/InventoryManage/getVendorByPoNumber?poNo=" + poNumber);
                                  xmlhttp.send();
                             }
          </script>
    </head>
    <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                   <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-sm-12 mobile-table-responsive">
                            <div class="card card-topline-purple full-scroll-table">
                                <div class="card-head">
                                    <header class="erp-table-header">Material Inspection</header>
                                </div>
                                <div class="card-body " id="bar-parent">
                                        <g:form action="addmaterialInspection" name="form">
                                            <div class="row">
                                                   <div class="col-sm-4">
                                                         <label for="ay">Academic Year : <span style="color:red; font-size: 16px">*</span></label>

                                                         <g:select name="ay" id="ay"  from="${academicYearList}"
                                                         class="form-control select2"
                                                         optionValue="${ay}"
                                                         noSelection="['null':'Please Select Academic Year']" onchange="callme()" required="true"/>
                                                   </div>
                                                   <div class="col-sm-4" id="roledivid">
                                                         <label >Purchase Order :</label>
                                                         <span style="color:red; font-size: 16px">*</span>
                                                         <g:select name="poNo" id="poNo"  from="${invPurchaseOrderList}"
                                                           class="form-control select2"
                                                         optionValue="${poNo?.pon}"
                                                         noSelection="['null':'Please Select Purchase Order']"onchange="call()" required="true"/>
                                                   </div>
                                                   <div class="col-sm-4" id="rolediv">
                                                         <label>Vendor</label>
                                                         <span style="color:red; font-size: 16px">*</span>
                                                             <g:field  id="vendor" name="vendor" readonly="readonly" value="" class="form-control "/>
                                                   </div>
                                            </div><br>
                                            <center>
                                                <div>
                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span>Fetch</button>
                                                </div>
                                            </center>
                                        </g:form>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 ">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header"> Material Inspection List</header>
                                </div>
                                 <div class="card-body " id="bar-parent">
                                    <div class="table-responsive">
                                         <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                           <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Invoice Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Invoice Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Total Cost(Rs).</th>
                                                    <!--
                                                    <th class="mdl-data-table__cell--non-numeric">Invoice File</th>
                                                    -->
                                                    <th class="mdl-data-table__cell--non-numeric">Invoice Approved</th>
                                                    <th class="mdl-data-table__cell--non-numeric">PO Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">View Invoice</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Add Material Inspection</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                 <g:each in="${invoicelist}" var="invoice" status="i">
                                                      <tr>
                                                         <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${invoice?.invoice_number}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${academicyear}</td>
                                                         <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${invoice?.invoice_date}"/></td>
                                                         <td class="mdl-data-table__cell--non-numeric">${invoice?.total}</td>

                                                         <td class="mdl-data-table__cell--non-numeric">
                                                           <center> <g:if test="${invoice?.isapproved}">
                                                               <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                           </g:if>
                                                           <g:else>
                                                               <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                           </g:else></center>
                                                         </td>
                                                         <td class="mdl-data-table__cell--non-numeric">${invoice?.invpurchaseorder?.pon}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                             <center>
                                                                <g:link controller="InventoryManage" action="viewInvoice" params="[invoiceId : invoice?.id]"><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                            </center>
                                                         </td>
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                              <center>   <g:link controller="InventoryManage" action="invoicematerialInspection" params="[invoiceId : invoice?.id]"><i class="fa fa-plus fa-2x erp-edit-icon-color"></i></g:link>
                                                                            </center>
                                                         </td>
                                                         <!--
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                              <g:link controller="inventoryManage" action="activateMaterialInspection" params="[invoiceId : invoice?.id]">
                                                                  <g:if test="${invoice?.isactive}">
                                                                      <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                                  </g:if>
                                                                  <g:else>
                                                                      <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                                  </g:else>
                                                              </g:link>
                                                         </td>
                                                         -->
                                                      </tr>
                                                </g:each>
                                             </tbody>
                                         </table>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
      </body>
</html>


