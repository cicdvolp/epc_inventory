<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
    </head>
     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                  <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12 mobile-table-responsive">
                            <div class="card card-topline-purple full-scroll-table">
                                        <div class="card-head">
                                            <header class="erp-table-header">Inventory Details</header>
                                        </div>
                                        <div class="card-body " id="bar-parent">
                                          <div class="table-responsive">
                                            <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                    <tr>
                                                        <th class="mdl-data-table__cell--non-numeric">Sr no</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Request Date</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Requested By</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Accession Number</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Material name</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Material Part</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Inward Date</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Purchase Cost</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Deadstock Status</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Depreciation Rate</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Depreciation Value</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Write-down Value</th>
                                                    </tr>
                                                 </thead>
                                                 <tbody>
                                                    <g:each in="${invWriteOffRequestEscalation}" var="inventory" status="i">
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                            <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${inventory?.invwriteoffrequest?.request_date}"/></td>
                                                            <td class="mdl-data-table__cell--non-numeric">${inventory?.actionby}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${inventory?.invwriteoffrequest?.inventory?.accession_number}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${inventory?.invwriteoffrequest?.inventory?.invmaterial?.name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${inventory?.invwriteoffrequest?.inventory?.invmaterialpart?.name}</td>
                                                            <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${inventory?.invwriteoffrequest?.inventory?.inward_date}"/></td></td>
                                                            <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.invwriteoffrequest?.inventory?.purchase_cost}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${inventory?.invwriteoffrequest?.inventory?.invdeadstockstatus?.status}</td>
                                                            <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.invwriteoffrequest?.inventory?.depreciation_rate}</td>
                                                            <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.invwriteoffrequest?.inventory?.depreciation_value}</td>
                                                            <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.invwriteoffrequest?.inventory?.written_down_value}</td>
                                                        </tr>
                                                    </g:each>
                                                </tbody>
                                            </table>
                                          </div>
                                        </div>
                            </div>
                       </div>
                    </div>
      </body>
</html>


