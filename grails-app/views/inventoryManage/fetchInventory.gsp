<div class="row">
                            <div class="col-md-12 mobile-table-responsive">

                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Inventory List</header>
                                   </div>

                                        <div class="table-responsive">
                                            <table class="mdl-data-table ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr no</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Accession Number</th>
                                                   
                                                    <th class="mdl-data-table__cell--non-numeric">Material name</th>
                                                      <th class="mdl-data-table__cell--non-numeric">Material Part</th>
                                                      <th class="mdl-data-table__cell--non-numeric">Material Type</th>
                                                      <th class="mdl-data-table__cell--non-numeric">Material Category</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Room Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Department</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Inward Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purchase Cost</th>
                                                    <th class="mdl-data-table__cell--non-numeric">DeadStock Status</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Depreciation Rate in %</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Depreciation Value</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Write Down Value</th>
                                                    
                                                    <th class="mdl-data-table__cell--non-numeric">Is write-off</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Write-off Date</th>
                                                    

                                                  
                                                   
                                                    <!--<th class="mdl-data-table__cell--non-numeric">Delete</th>-->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${inventoryList}" var="inventory" status="i">
                                                    <tr>
                                                        
                                                        <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.accession_number}</td>
                                                       <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterial?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterialpart?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterial?.invmaterialtype?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterial?.invmaterialcategory?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invroom?.number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.department?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${inventory?.inward_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.purchase_cost}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invdeadstockstatus?.status}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.depreciation_rate}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.depreciation_value}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.written_down_value}</td>
                                                      
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                        <center> <g:if test="${inventory?.iswriteoff}">
                                                                    <i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>
                                                                </g:if>
                                                                <g:else>
                                                                    <i class="fa fa-times fa-2x" style="color:red" aria-hidden="true"></i>
                                                                </g:else></center>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.writeoff_date}</td>
                                                     
                                                        
                                                        
                                                       
                                                    </tr>
                                                
                                                </g:each>
                                                </tbody>
                                               </table>


                                    </div>
                                </div> 
                            </div>
                        </div>
                       <!--Table End-->    