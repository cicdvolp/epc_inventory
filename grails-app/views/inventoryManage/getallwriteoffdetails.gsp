<div class="row">
    <div class="col-sm-12">
        <div class="card card-topline-purple">
            <div class="card-head">
                <header class="erp-table-header">
                    Category : ${invFinanceApprovingCategory?.name}, Approving Authority Post : ${invInstructorFinanceApprovingAuthority?.invfinanceapprovingauthority?.name}
                </header>
            </div>
            <!--<div class="card-body">-->
                <div class="table-responsive">
                    <table class="mdl-data-table ml-table-bordered">
                        <thead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                <th class="mdl-data-table__cell--non-numeric">Write Off Request </th>
                                <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <g:each in="${invWriteOffRequestEscalation_list}" var="wo" status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <span>Academic Year : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.academicyear?.ay}</span>
                                        <br>
                                        <span>Accession Number : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.inventory?.accession_number}</span>
                                        <br>
                                        <span>Reason : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.reason}</span>
                                        <br>
                                        <span>Requested Date : </span><span class="erp-table-header" style="font-weight:900;"><g:formatDate format="dd/MM/yyyy" date="${wo?.invwriteoffrequest?.request_date}"/></span>
                                        <br>
                                        <span>Purchase Cost : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.inventory?.purchase_cost}</span>
                                        <br>
                                        <span>Depreciation Rate : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.inventory?.depreciation_rate}</span>
                                        <br>
                                        <span>Depreciation Value : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.inventory?.depreciation_value}</span>
                                        <br>
                                        <span>Department : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.inventory?.department?.abbrivation}</span>
                                        <br>
                                        <span>Requested By : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.requestby}</span>
                                        <br>
                                        <span>Material : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.inventory?.invmaterial?.name}</span>
                                        <br>
                                        <span>Material Part : </span><span class="erp-table-header" style="font-weight:900;">${wo?.invwriteoffrequest?.inventory?.invmaterialpart?.name}</span>
                                        <br>
                                    <!--    <span>View More Details : </span><span class="erp-table-header" style="font-weight:900;">----</span>-->
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:link target="blank" controller="inventoryManage" action="getWOEscalationforApprove" params="[woId:wo?.id]" class="btn btn-primary">Proceed</g:link>
                                    </td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
            <!--</div>-->
        </div>
    </div>
</div>