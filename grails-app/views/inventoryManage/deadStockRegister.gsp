<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
        
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    
     <!--Scripts--><!--invMaterial?.organization?.icard_organization_name-->
      <script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script>
        function callMe() {
                
                var departmentName=document.getElementById("Department").value
               
                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("secondSelect").innerHTML = this.responseText;
                        
                    }
                };
                xmlhttp.open("POST","${request.contextPath}/inventoryManage/getRoomNumbers?department="+departmentName);
                xmlhttp.send();
           }

           function validate() {
               var department=document.getElementById('Department').value
             
            
                 window.alert("department"+ department + "materialName "+ materialName + "materialPart "+materialPart)
               if(department==null || materialName==null || materialPart==null)
               {
                   window.alert('Please select a department, a material, a material part and then click fetch')
                   return false;
                   
               }
               else{
                   window.alert(department+" "+materialName+" "+materialPart)
                   return true;
               }
           }

           function getInventoryForm()
           {
                 var materialName = document.getElementById("MaterialName").value;
                var department=document.getElementById("Department").value 
                 var materialPart=document.getElementById('MaterialPart').value
                  var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("InventoryForm").innerHTML = this.responseText;
                        
                    }
                    else{
                        window.alert(responseText)
                    }
                };
                xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getInventoryDetailsByMaterialName?material=" + materialName +"&department="+department+"&materialPart="+materialPart);
                xmlhttp.send();
           }
       /* function getInventoryDetails()
        {
          var materialName = document.getElementById("MaterialName").value;
          var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                      var data=this.responseText
                      window.alert(data)
                      document.getElementById("InvoiceNumber").innerHTML = data.invoice?.invoice_number;
                        
                    }
                };
                xmlhttp.open("GET", "${request.contextPath}/inventoryManage/getInventoryDetailsByMaterialName?material=" + materialName);
                xmlhttp.send();
           }
        }*/
        </script>
    
                 
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                  <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Fields Selection-->
                 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Select Details</header>
                                     
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                    
                                     <g:form action="fetchInventory"> 
                                        <center>
                                        <div class="form-row">
                                                <div class="form-group row mx-auto">
                                                    <label>Department:</label>&nbsp;&nbsp;
                                                    <div>
                                                        <g:select  id="Department" name="department" optionKey="id" optionValue="${department?.name}" from="${invDepartmentList}" class="form-control select2" required="true" value="${department?.id}"  noSelection="['null':'Select Department']"
                                                        onChange="callMe()"/>
                                                    </div>
                                                 </div>
                                         </div>
                                          <div class="form-row">
                                                <div class="form-group row mx-auto">
                                                    <label>Room Number:</label>&nbsp;&nbsp;
                                                    <div id="secondSelect">
                                                        <g:select id="RoomNumber" name="roomNumber" value="${{it?.id}}" optionKey="${{it?.id}}" optionValue="${{it?.name+":"+it.number}}" from="${invRoomList}" class="form-control select2" required="true" noSelection="['null':'Select Room Number']"/>
                                                    </div>
                                                 </div>
                                         </div>
                                           <div class="form-row">
                                            <div class="col">
                                            <g:submitToRemote class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" url="[action: 'fetchInventory']" update="InventoryList" value="Fetch"/>
                                          </div>
                                         </div> 
                                           
                                          
                                           
                                     
                                          
                                         
                                        
                                        </center>
                                    </g:form>
                                   <!---->
                                   
                                   
                                   <!---->
                                  </div>  
                        </div>
                          </div> 
                            </div> 
                        
                        <!--Inventory Details-->
                             <!--Table -->
                     <div id="InventoryList">
                     <div class="row">
                            <div class="col-md">


                            </div>
                        </div>
                    </div>
                       <!--Table End-->     
            </div>
        </div>
      
</body>
</html>



<!---->