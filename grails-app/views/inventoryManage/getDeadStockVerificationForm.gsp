    <div class="row">
                  <div class="col-sm-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Dead Stock Verification</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">                                    
                                    
                                          <g:form action="saveDeadStockVerification">
                                                <div class="row">

                                                    <div class="col-sm-4">
                                                     <label>Department:</label>
                                                       <input name="department" class="form-control" readonly type="text" value="${inventory?.department?.name}">
                                                    </div>
                                                     <div class="col-sm-4">
                                                     <label>Material Name:</label>
                                                     <input type="text" name="material" class="form-control" readonly value="${inventory?.invmaterial?.name}">
                                                    </div>
                                                    <div class="col-sm-4">
                                                           <label >Material Part:</label>
                                                           <input type="text" name="materialPart" class="form-control" readonly value="${inventory?.invmaterialpart?.name}">
                                                    </div>
                                                 </div>


                                                <div class="row">
                                                    <div class="col-sm-4">
                                                        <label>Accession Number/Dead Stock Number:</label>
                                                        <input type="text" value="${inventory?.accession_number}" readonly name="accessionNumber" id="InventoryData" class="form-control"/>
                                                    </div>
                                                     <div class="col-sm-4">
                                                         <label>Material Type:</label>
                                                         <input type="text" name="materialType" readonly class="form-control" value="${inventory?.invmaterial?.invmaterialtype?.name}"/>
                                                     </div>

                                                    <div class="col-sm-4">
                                                      <label >Material Category:</label>
                                                      <input type="text" name="materialCategory" readonly class="form-control" value="${inventory?.invmaterial?.invmaterialcategory?.name}"/>
                                                    </div>
                                                 </div>


                                                <div class="row">
                                                     <div class="col-sm-4">
                                                         <label>Room No:</label>
                                                         <input type="text" name="roomNo" readonly class="form-control" value="${inventory?.invroom?.number}"/>
                                                     </div>
                                                     <div class="col-sm-4">
                                                          <label>Inward Date:</label><br>
                                                           <g:formatDate format="yyyy-MM-dd" date="${inventory?.inward_date}" />
                                                     </div>
                                                     <div class="col-sm-4">
                                                           <label>Purchase Cost Including Tax:</label>
                                                            <input type="text" readonly name="purchaseCost" value="${inventory?.purchase_cost}" class="form-control"/>
                                                     </div>
                                                </div>
                                                <div class="row">
                                                     <div class="col-sm-4">
                                                          <label>Depreciation Rate in % : </label>
                                                           <input type="text" readonly name="depreciationRate" value="${inventory?.depreciation_rate}"   class="form-control"/>
                                                    </div>
                                                     <div class="col-sm-4">
                                                      <label>Depreciation Value:</label>
                                                        <input type="text" readonly name="depreciationValue" value="${inventory?.depreciation_value}"   class="form-control"/>
                                                    </div>
                                                     <div class="col-sm-4">
                                                            <label>Written Down Value :</label>
                                                             <input type="text" readonly name="writtenDownValue" value="${inventory?.written_down_value}"   class="form-control" />
                                                      </div>
                                                </div>

                                                <div class="row">
                                                      <div class="col-sm-4">
                                                         <label>Dead-Stock Status:</label>
                                                        <g:select id="DeadStockStatus" name="deadStockStatus"  value="${{it?.id}}" optionKey="${{it?.id}}" optionValue="${{it?.status}}" from="${deadStockStatusList}" class="form-control select2"  required="true" noSelection="['null':'Select Dead Stock Status']"/>
                                                      </div>
                                                      <div class="col-sm-4">
                                                             <label>Write Off Date : </label><br>
                                                             <g:datePicker class="form-control" name="writeOffDate" value="${new Date()}"  default="none" noSelection="['':'--']"  precision="day"/>
                                                      </div>

                                                      <div class="col-sm-4">
                                                         <br>
                                                           <label>Is Write-Off:</label>&nbsp;&nbsp;
                                                           <input type="checkbox" name="isWriteOff">
                                                      <div>
                                                </div>

                                                <div class="row">
                                                     <div class="col-sm-12">
                                                     <center>
                                                     <br>
                                                       <button id="btnSubmit" type="submit" class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                                     </center>
                                                    </div>
                                                </div>

                                        </g:form>
                              </div>  
                         </div>
                     </div> 
                </div>    
                              