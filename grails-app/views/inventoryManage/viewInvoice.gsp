<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
    </head>
     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Invoice Details</header>
                                </div>
                                <div class="card-body " id="bar-parent">
                                        <div class="row">

                                            <div class="col-sm-6">
                                               <div class="row">
                                                  <div class="col-sm-4">
                                                   <label for="pon">PO Number :</label>
                                                  </div>
                                                   <div class="col-sm-6">
                                                    <g:field  id="pon" name="pon" readonly="readonly" value="${invoice1?.invpurchaseorder?.pon}"  class="form-control"/>
                                                    </div>
                                               </div>
                                               <div class="row">
                                                 <div class="col-sm-4">
                                                 <label for="podate" >PO Date :</label>
                                                 </div>
                                                  <div class="col-sm-6">
                                                 <g:field  id="podate" name="podate" readonly="readonly" value="${formatDate(format:'dd-MM-yyyy',date:invoice1?.invpurchaseorder?.purchase_order_date)}"  class="form-control"/>
                                                   </div>
                                              </div>
                                            <div class="row">
                                                      <div class="col-sm-4">
                                                      <label for="material">Purpose :</label>
                                                      </div>
                                                       <div class="col-sm-6">
                                                 <g:field id="purpose" name="purpose" readonly="readonly" value="${invoice1?.purpose}"  class="form-control"/>
                                                        </div>
                                                   </div>
                                             <div class="row">
                                                   <div class="col-sm-4">
                                                    <label for="amount">Amount :</label>
                                                   </div>
                                                    <div class="col-sm-6">
                                                  <g:field id="total" name="total"  readonly="readonly" value="${invoice1?.total}"  class="form-control"/>
                                                     </div>
                                                </div>
                                                <div class="row">
                                                                           <div class="col-sm-4">
                                                                            <label for="amount">Tax :</label>
                                                                           </div>
                                                                            <div class="col-sm-6">
                                                 <g:field id="tax" name="tax"  readonly="readonly" value="${invoice1?.tax}"  class="form-control"/>
                                                                             </div>
                                                                        </div>
                                                </div>
                                             <div class="col-sm-6">
                                                 <div class="row">
                                                      <div class="col-sm-4">
                                                       <label for="Total">Total Amount :</label>
                                                      </div>
                                                       <div class="col-sm-6">
                                                 <g:field id="purpose" name="purpose" readonly="readonly" value="${invoice1?.purpose}"  class="form-control"/>
                                                        </div>
                                                   </div>
                                             <div class="row">
                                                   <div class="col-sm-4">
                                                     <label for="inventory">Invoice Number</label>
                                                   </div>
                                                    <div class="col-sm-6">
                                                  <g:field id="invoicenumber" name="invoicenumber" readonly="readonly" value="${invoice1?.invoice_number}"  class="form-control"/>
                                                     </div>
                                                </div>
                                                <div class="row">
                                                                           <div class="col-sm-4">
                                                                            <label for="inventory">File Name</label>
                                                                           </div>
                                                                            <div class="col-sm-6">
                                                  <g:field id="filename" name="filename" readonly="readonly" value="${invoice1?.file_name}"  class="form-control" />
                                                                             </div>
                                                                        </div>

                                            </div>
                                            </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-purple">
                               <div class="table-responsive">
                                   <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                     <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                            <th class="mdl-data-table__cell--non-numeric">Material</th>
                                            <th class="mdl-data-table__cell--non-numeric">Cost Per Unit(Rs.)</th>
                                            <th class="mdl-data-table__cell--non-numeric">Quantity</th>
                                            <th class="mdl-data-table__cell--non-numeric">Total Cost(Rs).</th>
                                        </tr>
                                     </thead>
                                     <tbody>
                                        <g:each in="${invoiceDetailsList}" var="invDetails" status="i">
                                          <tr>
                                             <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${invDetails?.invmaterial?.name}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${invDetails?.cost_per_unit}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${invDetails?.quantity}</td>
                                             <td class="mdl-data-table__cell--non-numeric">${invDetails?.total_cost}</td>
                                          </tr>
                                        </g:each>
                                     </tbody>
                                   </table>
                               </div>
                        </div>
                    </div>
                </div>
             </div>
         </div>
     </body>
</html>


