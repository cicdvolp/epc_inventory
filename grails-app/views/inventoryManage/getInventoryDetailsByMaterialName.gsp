 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Save Inventory</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                     <g:form action="saveInventoryDetails">
                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                    <label>Department:</label>
                                                    <input name="department" class="form-control" readonly type="text" value="${invdepartment?.name}">
                                                </div>
                                                 <div class="col-sm-4">
                                                     <label>Material Name:</label>
                                                     <input type="text" name="material" class="form-control" readonly value="${invMaterial?.name}">
                                                </div>
                                                 <div class="col-sm-4">
                                                     <label for="staticEmail">Invoice Number:</label>
                                                     <input name="invoiceNumber" class="form-control" type="text" readonly value="${invoice?.invoice_number}" />
                                                </div>
                                            </div><br>
                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                    <label>Material Part:</label>
                                                     <!--<g:select id="MaterialPart" name="materialPart" value="${{it?.id}}" style="width:100%!important;" optionKey="${{it?.id}}" optionValue="${{it?.name}}" from="${materialPartList}" class="select2" required="true" noSelection="['null':'Select Material Part']"/>-->
                                                     <input type="text" name="materialPart" class="form-control" readonly value="${materialPartList?.name}">
                                                </div>
                                                 <div class="col-sm-4">
                                                     <label for="staticEmail" >Quantity:</label>
                                                     <input type="text" name="inventoryQuantity" readonly class="form-control"  value="${invPurchaseOrderDetails?.quantity}"/>
                                                </div>
                                                <!--
                                                 <div class="col-sm-4">
                                                       <label for="inputPassword" >Accession Number/Dead Stock Number:</label>
                                                       <input type="text"  value="${invMaterial?.organization?.organization_code+"/"+invdepartment?.name+"/"+singleMaterialPart?.name+"/"+invSerialNumber?.number}" readonly name="accessionNumber" id="InventoryData" class="form-control"/>
                                                </div>
                                                -->
                                                <div class="col-sm-4">
                                                        <label>Depreciation Percentage:</label>
                                                        <input type="text" name="depreciation" readonly class="form-control" value="${depreciation?.percentage}"/>
                                                </div>
                                            </div><br>

                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                         <label>Accession Number:</label>
                                                         <input type="text" name="accNo" readonly class="form-control"  value="${invAccNumber}"/>
                                                 </div>
                                            </div>

                                            <div class="form-row">
                                                <div class="col-sm-4">
                                                         <label>Room No:</label>
                                                         <g:select id="InventoryRoom" name="inventoryRoom" style="width:100%!important;" value="${{it?.id}}" optionKey="${{it?.id}}" optionValue="${{it?.name+":"+it.number}}" from="${invRoomList}" class="select2" required="true" noSelection="['null':'Select Room Number']" required="true"/>
                                                         <!--${remoteFunction(action:'getMaterialParts',update:'second',params:'\'material=\'+this.value')}"-->
                                                </div>
                                                 <div class="col-sm-4">
                                                        <center>
                                                        <label>Inward Date:</label><br>
                                                        <g:datePicker class="form-control" name="inwardDate" value="${new Date()}" precision="day"/>
                                                        </center>
                                                </div>
                                                 <div class="col-sm-4">
                                                         <label>Purchase Cost Including Tax:</label>
                                                         <input type="text" name="purchaseCost" placeholder="Enter Purchase Cost" style="width:100%!important;" class="form-control" required="true"/>
                                                </div>
                                            </div>
                                      <center>
                                            <div class="form-row mx-auto my-2">
                                                 <button id="btnSubmit" type="submit" onclick="this.disabled=true;this.value='Saving, please wait...';this.form.submit();" class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                            </div>
                                       </center>
                                     </g:form>
                                   </div>
                        </div>
                          </div> 
                            </div>    