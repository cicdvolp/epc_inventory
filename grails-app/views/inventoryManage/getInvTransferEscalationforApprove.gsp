<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_inst"/>
        <meta name="author" content="rishabh"/>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">
                                        Approving Authority : ${invTransferEscalation?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}, Category : ${invTransferEscalation?.invfinanceapprovingauthoritylevel?.invfinanceapprovingcategory?.name} <!-- ?.name -->
                                    </header>
                                </div>
                                <g:if test="${next_level_approved == false}">
                                     <div class="card-body">
                                        <g:form controller="inventoryManage" action="saveApproveInvTransfer">
                                            <input type="hidden" name="iteId" value="${invTransferEscalation?.id}" />
                                            <div class="row">
                                                <div class="col-sm-4">
                                                    <label>Remark : </label>
                                                    <input type="text" name="remark" id="remark" class="form-control" />
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Approving Status : </label>
                                                    <g:select name="approvingstatus" optionValue="name" from="${invapprovalstatus_list}" class="form-control select2" required="true" optionKey="id"/>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <br>
                                                    <center>
                                                        <g:submitToRemote url="[action: 'saveApproveInvTransfer', controller:'inventoryManage']" update="approve" value="Update" class="btn btn-primary" before="document.getElementById('smartprogressbar').style.display='block';" onComplete="document.getElementById('smartprogressbar').style.display='none';"/>
                                                    </center>
                                                </div>
                                            </div>
                                        </g:form>
                                    </div>
                                </g:if>
                                <g:else>
                                 <span style="margin-left:20px" >Cannot Update, Already Approved By Higher Authority.</span>
                                </g:else>
                            </div>
                        </div>
                    </div>
                    <div id="approve">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">
                                            Approving History for Inventory Transfer Request : Acc No:-${invTransferEscalation?.inventory?.accession_number}
                                        </header>
                                    </div>
                                    <div class="card-body">
                                        <div class="table-responsive">
                                            <table class="mdl-data-table ml-table-bordered">
                                                <thead>
                                                    <tr>
                                                        <th class="mdl-data-table__cell--non-numeric">Level No</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Approving Authority</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Approving Status</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Transfered To</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Action Date</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                        <th class="mdl-data-table__cell--non-numeric">Approved By</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <g:each in="${invTransferEscalation_List}" var="ite" status="k">
                                                        <tr>
                                                            <td class="mdl-data-table__cell--non-numeric">${ite?.invfinanceapprovingauthoritylevel?.level_no}</td>
                                                            <td class="mdl-data-table__cell--non-numeric">${ite?.invfinanceapprovingauthoritylevel?.invfinanceapprovingauthority?.name}</td>
                                                            <g:if test="${ite?.invapprovalstatus?.name != 'In-Process'}">
                                                                <td class="mdl-data-table__cell--non-numeric">${ite?.invapprovalstatus?.name}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${ite?.department?.name}</td>
                                                                <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd/MM/yyyy" date="${ite?.action_date}"/></td>
                                                                <td class="mdl-data-table__cell--non-numeric">${ite?.remark}</td>
                                                                <td class="mdl-data-table__cell--non-numeric">${ite?.actionby}</td>
                                                            </g:if>
                                                            <g:else>
                                                                <td class="mdl-data-table__cell--non-numeric">Not Approved</td>
                                                                <td class="mdl-data-table__cell--non-numeric">-</td>
                                                                <td class="mdl-data-table__cell--non-numeric">-</td>
                                                                <td class="mdl-data-table__cell--non-numeric">-</td>
                                                            </g:else>
                                                        </tr>
                                                    </g:each>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>