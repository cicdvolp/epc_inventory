<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
        
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
 
     <!--Scripts--><!--invMaterial?.organization?.icard_organization_name-->
        <script>
        function callme() {
                var accession_number=document.getElementById("AccessionNumber").value

                var xmlhttp = new XMLHttpRequest();
                xmlhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("DeadStockForm").innerHTML = this.responseText;
                        
                    }
                };
                xmlhttp.open("POST", "${request.contextPath}/inventoryManage/getDeadStockVerificationForm?accessionNumber=" + accession_number );
                xmlhttp.send();
           }
           function validate() {
               var department=document.getElementById('Department').value
             
               var materialName=document.getElementById('MaterialName').value
               var materialPart=document.getElementById('MaterialPart').value
                 window.alert("department"+ department + "materialName "+ materialName + "materialPart "+materialPart)
               if(department==null || materialName==null || materialPart==null)
               {
                   window.alert('Please select a department, a material, a material part and then click fetch')
                   return false;
                   
               }
               else{
                   window.alert(department+" "+materialName+" "+materialPart)
                   return true;
               }
           }
        </script>
    
                 
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                 <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                         <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                  <g:if test="${flash.error}">
                         <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--Fields Selection-->
                 <div class="row">
                  <div class="col-md-12">
                       <div class="card card-topline-purple">
                                    <div class="card-head">
                                        <header class="erp-table-header">Enter an accession number to get form</header>
                                   </div>
                                   <div class="card-body inline-block" id="bar-parent">
                                     <g:form action="getDeadStockVerificationForm"> 
                                        <center>
                                            <div class="form-row">
                                                 <div class="form-group row mx-auto">
                                                    <label class="my-2 form-label">Accession Number:</label>&nbsp;&nbsp;
                                                    <div>
                                                        <input id="AccessionNumber" required="true" placeholder="Enter a accession number" style="width:21rem ! important" name="accessionNumber" type="text" class="form-control">
                                                    </div>
                                                 </div>
                                            </div>
                                                <div class="form-row">
                                                    <div class="col">
                                                         <g:submitToRemote class="my-3 mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored" url="[action: 'getDeadStockVerificationForm']" update="DeadStockForm" value="Fetch"/>
                                                    </div>
                                                </div>
                                        </center>
                                    </g:form>
                                  </div>  
                        </div>
                          </div> 
                            </div> 
                        
                        <!--Inventory Form-->
                   
                             <div id="DeadStockForm">
                                       
                            </div>
                        <!---->
                                 <!--Table -->
                <div id="InventoryList">
                     <div class="row">
                            <div class="col-md-12 mobile-table-responsive">

                                <div class="card card-topline-purple full-scroll-table">
                                    <div class="card-head">
                                        <header class="erp-table-header">Inventory List</header>
                                   </div>
                                    <div class="card-body " id="bar-parent">
                                        <div class="table-responsive">
                                            <table id="exportTable" class="display nowrap erp-full-width table-striped table-hover ml-table-bordered">
                                               <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr no</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Accession Number</th>
                                                   <th class="mdl-data-table__cell--non-numeric">Material name</th>
                                                      <th class="mdl-data-table__cell--non-numeric">Material Part</th>
                                                      <th class="mdl-data-table__cell--non-numeric">Material Type</th>
                                                      <th class="mdl-data-table__cell--non-numeric">Material Category</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Room Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Department</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Inward Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Purchase Cost</th>
                                                    <th class="mdl-data-table__cell--non-numeric">DeadStock Status</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Depreciation Rate in %</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Depreciation Value</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Write Down Value</th>
                                                    
                                                    <th class="mdl-data-table__cell--non-numeric">Is write-off</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Write-off Date</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                    <th class="mdl-data-table__cell--non-numeric">IsActive</th>
                                                    

                                                  
                                                   
                                                    <!--<th class="mdl-data-table__cell--non-numeric">Delete</th>-->
                                                </tr>
                                                 </thead>
                                                 <tbody>
                                                <g:each in="${inventoryList}" var="inventory" status="i">
                                                    <tr>
                                                       <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.accession_number}</td>
                                                       <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterial?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterialpart?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterial?.invmaterialtype?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invmaterial?.invmaterialcategory?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invroom?.number}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.department?.name}</td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${inventory?.inward_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-inr" aria-hidden="true"></i>${inventory?.purchase_cost}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.invdeadstockstatus?.status}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.depreciation_rate}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.depreciation_value}</td>
                                                        <td class="mdl-data-table__cell--non-numeric">${inventory?.written_down_value}</td>
                                                      
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                            <g:if test="${inventory?.iswriteoff==true}">
                                                                <i class="fa fa-check" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </g:else>
                                                        </td>
                                                        <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${inventory?.writeoff_date}"/></td>
                                                        <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-edit fa-2x erp-edit-icon-color mx-auto" data-toggle="modal" data-target="#editInventory${i}"></i></td>
                                                        <td class="mdl-data-table__cell--non-numeric">
                                                             <g:link controller="inventoryManage" action="activateDeadStockInventory" params="[InventoryId : inventory?.id]">
                                                            <g:if test="${inventory?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                        </td>
                                                        
                                                        
                                                       
                                                    </tr>
                                                    <!--Modal Edit-->
                                                    <div class="modal fade" id="editInventory${i}" role="dialog">
                                                        <div class="modal-dialog">
                                                          <div class="modal-content">
                                                            <div class="modal-header">
                                                              <h4 class="modal-title ml-3"><strong>Edit DeadStock Verification</strong></h4>
                                                              <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                            </div>
                                                                 <g:form action="editDeadStockVerification">
                                                                  <div class="modal-body mx-3">
                                                                    
                                                                  <div class="form-group row">
                                                                    <label class="col-sm-4 my-auto">Dead-Stock Status:</label>
                                                                     <g:select id="DeadStockStatus" style="width:20rem!important" name="deadStockStatus"  value="${{it?.status}}" optionKey="${{it?.id}}" optionValue="${{it?.status}}" from="${deadstockstatuslist}" class="select2" required="true" noSelection="['null':'Select Dead Stock Status']"/>  
                                                                   </div>
                                                                   
                                                                    <div class="form-group row">
                                                                    <label class="col-sm-3 my-auto">Write-Off Date:</label>
                                                                   <g:datePicker class="form-control" name="writeOffDate" value="${inventory?.writeoff_date}" default="none" noSelection="['':'--']" precision="day"/>
                                                                   </div>

                                                                    <div class="form-group">
                                                                                <g:if test="${inventory?.iswriteoff}">
                                                                                    <input type="checkbox" id="isWriteOff" class="css-checkbox" name="isWriteOff" checked="true"/> &nbsp; &nbsp;Is Write-Off?
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <input type="checkbox" id="isWriteOff" class="css-checkbox" name="isWriteOff"/> &nbsp; &nbsp;Is Write-Off?
                                                                                </g:else>
                                                                            </div>
                                                                </div>
                                                                    <div class="modal-footer">
                                                                        <input type="hidden" name="inventoryId" id="inventoryId" value="${inventory?.id}">
                                                                        <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto mx-auto">Update</button>
                                                                    </div>
                                                                 
                                                                </g:form>
                                                          </div>
                                                        </div>
                                                      </div>
                                                    <!--End of Modal edit-->                                                
                                                </g:each>
                                                </tbody>
                                               </table>

                                         </div>
                                    </div>
                                </div> 
                            </div>
                        </div>
                    </div>
                       <!--Table End-->
                
            </div>
        </div>
      
</body>
</html>



<!---->