<!DOCTYPE html>
<html>

<head>
    <meta name="layout" content="ERPmainTemplateT2">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>

</head>
<body>
<!-- Page Content  Copy Coding use Only-------------------------------------->
<div id="page-wrapper">
        <g:render template="/layouts/home_back_btn_T2"/>
                <h3 class="page-header" style="text-align: center;">Branch wise Admission Intake</h3>
                 <!-- Start coding only in This block ------------------->
  <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#addNewAccountType"><span class="glyphicon glyphicon-plus-sign"></span> Add New Account Type</button>
  <br>
  <br>
               <table  class="table table-striped"
                <tr>
                    <th>Sr. No.</th>
                    <th>Account Type </th>
                    <th>Edit</th>
                    <th>Delete</th>
                </tr>
                <g:each in="${erpAccountTypeList}" var="name" status="i">
                    <tr>
                        <td>${i+1}</td>
                        <td>${name?.name}</td>
                        <td>
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#${i}editAccountType"><span class="glyphicon glyphicon-edit"></span> Edit AccountType</button>
                        </td>
                        <td>
                            <g:form action="deleteAccountType">
                                  <input type="hidden" value="${name?.id}" name="nameid">
                                <button  data-toggle="modal" data-target="#${i}myModal" class="btn btn-danger" type="submit" onclick="return confirm('Do you want to Delete ?')" ><span class="glyphicon glyphicon-trash"></span> Delete</button>
                            </g:form>
                        </td>
                    </tr>
                    <!-- Modal edit -->
                      <div class="modal fade" id="${i}editAccountType" role="dialog">
                        <div class="modal-dialog">
                          <div class="modal-content">
                            <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal">&times;</button>
                              <h4 class="modal-title">Edit AccountType</h4>
                            </div>
                           <g:form action="editAccountType">
                            <div class="modal-body">
                              <div class="form-group">
                                  <label for="pwd">AccountType:</label>
                                  <input type="hidden" value="${name?.id}" name="nameid">
                                  <input type="text" class="form-control" name="name" required="true" value="${name?.name}">
                              </div>
                            </div>
                            <div class="modal-footer">
                              <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Update</button>
                            </div>
                           </g:form>
                          </div>
                        </div>
                      </div>
                </g:each>
               </table>

<!-- /#page-wrapper  Page Content  Copy Coding use Only------------------------------------- -->
<!-- Modal New -->
  <div class="modal fade" id="addNewAccountType" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Add New AccountType</h4>
        </div>
       <g:form action="saveAccountType">
        <div class="modal-body">
          <div class="form-group">
              <label for="pwd">AccountType:</label>
              <input type="text" class="form-control" name="name" required="true">
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span> Save</button>
        </div>
       </g:form>
      </div>
    </div>
  </div>
  </div>
</body>
</html>



