
<style>
       .hidden {
           display: inline-block;
            width: -webkit-fill-available;
            height: 50px;
            background-color: transparent;
            border: none;
            outline:none !important;
       }
}
    </style>
<div class="mobile-table-responsive">
	<div class="card card-topline-purple full-scroll-table">
		<div class="card-head">
			<header class="erp-table-header">Exempted for Entrance Test Report</header>
		</div>
		<div class="panel-body">
			<div id="document">
				<!--<table id="exportTable" class="table table-bordered table-condensed">-->
				<table id="common" class="table table-bordered table-condensed">
					<thead>
					<tr>
						<th >Sr No</th>
						<th >Application ID`</th>
						<th >Full Name</th>
						<th >Branch</th>
						<th >Exam Exempted</th>
						<th >Category</th>
						<th >Email</th>
						<th >Mobiile</th>
						<th >Fees Paid In Rs</th>
						<th >Application Status</th>
						<th >Reason</th>
					</tr>
					</thead>
					<tbody>
					<g:each var="d" in="${reportdata}" status="i">
						<tr>
							<td>${i+1}</td>
							<td>${d?.applicaitionid}</td>
							<td>${d?.entranceapplicant?.fullname}</td>
							<td>${d?.entrancebranch?.name}</td>
							<td>${d?.entranceapplicant?.exemption.name}</td>
							<td>${d?.entrancecategory?.name}</td>
							<td>${d?.entranceapplicant?.email}</td>
							<td>${d?.entranceapplicant?.mobilenumber}</td>
							<td></td>
							<td>${d?.entranceapplicationstatus?.name}</td>
							<td>${d?.entrancerejectionreason?.name}</td>
						</tr>


					</g:each>
					</tbody>
				</table>
			</div>
		</div>
	</div>

</div>
<div class="modal fade" id="applicationmodel" role="dialog">
	<div class="modal-dialog" style=" padding-right: 1000px;">
		<!-- Modal content-->
		<div class="modal-content" style=" width:1000px;">
			<div class="modal-header" style="background-color:#E6E5E2;">
				<h4 class="modal-title page-head-title"><b>Admission Form</b></h4>
				<i class="fa fa-close fa-2x" data-dismiss="modal" onclick="closeremoveblackdrop()"></i>
			</div>
			<div id="aaplicationbody"></div>
		</div>
	</div>
</div>

<div class="modal fade" id="uploaddocumentmodel" role="dialog">
	<div class="modal-dialog" style="width:100%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="background-color:#E6E5E2;">
				<h4 class="modal-title page-head-title"><b>Uploaded Document</b></h4>
				<i class="fa fa-close fa-2x" data-dismiss="modal" onclick="closeremoveblackdrop()"></i>
			</div>
			<div id="updateplanmaster1"></div>
		</div>
	</div>
</div>

<div class="modal fade " id="feesstatusmodel" role="dialog" >
	<div class="modal-dialog" style=" padding-right: 1000px;">
		<!-- Modal content-->
		<div class="modal-content" style=" width:1000px;">
			<div class="modal-header" style="background-color:#E6E5E2;">
				<h4 class="modal-title page-head-title"><b>Fess Details</b></h4>
				<i class="fa fa-close fa-2x" data-dismiss="modal" onclick="closeremoveblackdrop()"></i>
			</div>
			<div id="feesstatusmodelbody"></div>
		</div>
	</div>
</div>


<div class="modal fade " id="statuschangemodel" role="dialog" >
	<div class="modal-dialog full_modal-dialog" >
		<!-- Modal content-->
		<div class="modal-content" >
			<div class="modal-header" style="background-color:#E6E5E2;">
				<h4 class="modal-title page-head-title"><b>Status Change</b></h4>
				<i class="fa fa-close fa-2x" data-dismiss="modal" onclick="closeremoveblackdrop()"></i>
			</div>
			<div id="statuschangebody"></div>
		</div>
	</div>
</div>


<div class="modal fade" id="editplanmastercategoryNew" role="dialog">
	<div class="modal-dialog" style="width:90%">
		<div class="modal-content">
			<div class="modal-header" style="background-color:#E6E5E2;">
				<h4 class="modal-title page-head-title"><b>Edit Fees Category</b></h4>
				<i class="fa fa-close fa-2x" onclick="closemodalcategorynew()"></i>
			</div>
			<div id="categorychange"></div>
		</div>
	</div>
</div>
<script>



     $('#common').DataTable( {
          dom: 'Bfrtip',
          searching: true,
          buttons: [
             'copyHtml5',
              {
                  extend: 'excel',
                  title: 'Attendance Report'
              },
              'csvHtml5',
              'pdfHtml5'
          ],
          paging: false
     } );

</script>
</script>

