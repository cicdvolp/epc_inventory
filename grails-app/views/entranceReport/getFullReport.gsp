<div class="row">
    <div class="col-md-12 mobile-table-responsive">
        <div class="card card-topline-purple full-scroll-table">
            <div class="card-head">
                <header class="erp-table-header">ATTENDANCE TABLE</header>
            </div>
            <!--<div class="card-body">-->
                <div class="table-responsive">
                    <table id="exportTable" class="mdl-data-table ml-table-bordered">
                        <tdead>
                            <tr>
                                <th class="mdl-data-table__cell--non-numeric"><center><input type="checkbox" name="cb" /></center></th>
                                <th class="mdl-data-table__cell--non-numeric">Sr No</th>
                                <th class="mdl-data-table__cell--non-numeric">Appliocation ID</th>
                                <th class="mdl-data-table__cell--non-numeric">Application Date</th>
                                <!-- Entrance Applicant -->
                                <th class="mdl-data-table__cell--non-numeric">Candidate Name</th>
                                <th class="mdl-data-table__cell--non-numeric">Email Id</th>
                                <th class="mdl-data-table__cell--non-numeric">Date Of Birth</th>
                                <th class="mdl-data-table__cell--non-numeric">Mobile Number</th>
                                <th class="mdl-data-table__cell--non-numeric">Local Address</th>
                                <th class="mdl-data-table__cell--non-numeric">permanent_address</th>
                                <th class="mdl-data-table__cell--non-numeric">Aadhar Card Number</th>
                                <th class="mdl-data-table__cell--non-numeric">Gender</th>
                                <th class="mdl-data-table__cell--non-numeric">Marital Status</th>
                                <th class="mdl-data-table__cell--non-numeric">Nationality</th>
                                <th class="mdl-data-table__cell--non-numeric">Domacile</th>
                                <th class="mdl-data-table__cell--non-numeric">Employment Status</th>
                                <!-- Entrance Version -->
                                <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                <th class="mdl-data-table__cell--non-numeric">Version Date</th>
                                <!-- <th class="mdl-data-table__cell--non-numeric">Is Fee Applicable To Handicap Person</th>-->
                                <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                <!-- Entrance Category -->
                                <th class="mdl-data-table__cell--non-numeric">Category Type</th>
                                <th class="mdl-data-table__cell--non-numeric">Category</th>
                                <!-- Entrance Application -->
                                <th class="mdl-data-table__cell--non-numeric">View Application</th>
                                <th class="mdl-data-table__cell--non-numeric">Exemption</th>
                                <th class="mdl-data-table__cell--non-numeric">Entrance Rejection Reason</th>
                                <th class="mdl-data-table__cell--non-numeric">Entrance Application Status</th>
                                <th class="mdl-data-table__cell--non-numeric">Is Physically Handicapped?</th>
                                <th class="mdl-data-table__cell--non-numeric">Is Visually handicapped?</th>
                                <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                <th class="mdl-data-table__cell--non-numeric">Is Exemption From Test</th>
                                <!-- Multiple records -->
                                <th class="mdl-data-table__cell--non-numeric">Is Fees Paid?</th>
                                <th class="mdl-data-table__cell--non-numeric">Receipts</th>
                                <!-- Multiple records -->
                                <g:each var="type" in="${entrancebranch_list}" status="x">
                                    <th class="mdl-data-table__cell--non-numeric">Branch (${type?.name})</th>
                                </g:each>
                                <g:each var="type" in="${exemptiontype_list}" status="y">
                                    <th class="mdl-data-table__cell--non-numeric" >Exemption Type (${type?.name})</th>
                                </g:each>
                                <!--<th class="mdl-data-table__cell--non-numeric" style="white-space: pre-wrap !important; width:500px !important;">exemptiondocument${application?.exemption?.name}</th>-->
                                <!-- Educational details -->
                                <g:each var="type" in="${entrancedegree_list}" status="z">
                                    <th class="mdl-data-table__cell--non-numeric">Degree (${type?.name})</th>
                                </g:each>
                                <!-- Documents details -->
                                <g:each var="type" in="${entrancedocumenttype_list}" status="q">
                                    <th class="mdl-data-table__cell--non-numeric">Documents (${type?.name})</th>
                                </g:each>
                                <!-- Experience details -->
                                <th class="mdl-data-table__cell--non-numeric">Experience</th>
                                <th class="mdl-data-table__cell--non-numeric">Actions</th>
                            </tr>
                        </tdead>
                        <tbody>
                            <g:each var="application" in="${entranceapplication_list}"  status="k">
                                <tr>
                                    <td class="mdl-data-table__cell--non-numeric"><center><input type="checkbox" name="cb" /></center></td>
                                    <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.applicaitionid}</td>
                                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${application?.applicationdate}"/></td>
                                    <!-- Entrance Applicant -->
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.fullname}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.email}</td>
                                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${application?.entranceapplicant?.dateofbirth}"/></td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.mobilenumber}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.local_address}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.permanent_address}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.aadhar_number}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.gender?.type}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.maritalstatus?.name}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.erpnationality?.type}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.erpdomacile?.type}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicant?.employmentstatus?.name}</td>
                                    <!-- Entrance Version -->
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceversion?.entrance_name}</td>
                                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${application?.entranceversion?.version_date}"/></td>
                                    <!-- <td class="mdl-data-table__cell--non-numeric">${application?.entranceversion?.is_fee_applicable_to_handicap_person}</td>-->
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceversion?.academicyear?.ay}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceversion?.programtype?.displayname}</td>
                                    <!-- Entrance Category -->
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entrancecategorytype?.name}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entrancecategory?.name}</td>
                                    <!-- Entrance Application -->
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:link controller="entranceReport" action="applicationPreviewInReport" params="[entranceVersion : application?.entranceversion?.id,applicantId:application?.entranceapplicant?.id]"><i style="cursor: pointer;" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.exemption?.name}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entrancerejectionreason?.name}</td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.entranceapplicationstatus?.name}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:if test="${application?.is_physically_handicapped}">
                                            <i class="fa fa-check fa-2x" style="color:green"></i>
                                            <font color="blue" class="float-right"><b><i class="fa fa-download fa-2x"></i></b></font>
                                        </g:if>
                                        <g:else>
                                            <i class="fa fa-close fa-2x" style="color:red"></i>
                                        </g:else>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:if test="${application?.is_visually_handicapped}">
                                            <i class="fa fa-check fa-2x" style="color:green"></i>
                                            <font color="blue" class="float-right"><b><i class="fa fa-download fa-2x"></i></b></font>
                                        </g:if>
                                        <g:else>
                                            <i class="fa fa-close fa-2x" style="color:red"></i>
                                        </g:else>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">${application?.remark}</td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:if test="${application?.is_exemption_from_test}">
                                            <i class="fa fa-check fa-2x" style="color:green"></i>
                                        </g:if>
                                        <g:else>
                                            <i class="fa fa-close fa-2x" style="color:red"></i>
                                        </g:else>
                                    </td>
                                    <!-- Entrance receipt -->
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:if test="${application?.isfeespaid}">
                                            <i class="fa fa-check fa-2x" style="color:green"></i>
                                        </g:if>
                                        <g:else>
                                            <i class="fa fa-close fa-2x" style="color:red"></i>
                                        </g:else>
                                    </td>
                                    <td class="mdl-data-table__cell--non-numeric">
                                        <g:if test="${application?.entranceapplicationreceipt}">
                                            ${application?.entranceapplicationreceipt?.feesreceiptid} :  ${application?.entranceapplicationreceipt?.amount} : <g:formatDate format="dd-MM-yyyy" date="${application?.entranceapplicationreceipt?.receiptdate}"/>
                                        </g:if>
                                    </td>
                                    <!-- Multiple records -->
                                    <g:each var="type" in="${entrancebranch_list}" status="x">
                                        <td class="mdl-data-table__cell--non-numeric">
                                            <g:include action="getApplicantBranchDetails" controller="entranceReport" params="[branch : type?.id, applicationid: application?.id]"/>
                                        </td>
                                    </g:each>
                                    <g:each var="type" in="${exemptiontype_list}" status="y">
                                        <td class="mdl-data-table__cell--non-numeric">
                                            <g:include action="getApplicantExemptionTypeDetails" controller="entranceReport" params="[exemptionType : type?.id, applicationid: application?.id]"/>
                                        </td>
                                    </g:each>

                                    <!-- Educational details -->
                                    <g:each var="type" in="${entrancedegree_list}" status="z">
                                        <td class="mdl-data-table__cell--non-numeric">
                                             <g:include action="getApplicantEntranceDegreeDetails" controller="entranceReport" params="[entrancedegree : type?.id, applicationid: application?.id]"/>
                                         </td>
                                    </g:each>
                                    <!-- Documents details -->
                                    <g:each var="type" in="${entrancedocumenttype_list}" status="q">
                                        <td class="mdl-data-table__cell--non-numeric">
                                            <g:include action="getApplicantEntranceDocumentType" controller="entranceReport" params="[entrancedocumenttype : type?.id, applicationid: application?.id]"/>
                                        </td>
                                    </g:each>
                                    <!-- Experience details -->
                                    <td class="mdl-data-table__cell--non-numeric"></td>
                                    <td class="mdl-data-table__cell--non-numeric">-</td>
                                </tr>
                            </g:each>
                        </tbody>
                    </table>
                </div>
            <!-- </div> -->
        </div>
    </div>
</div>
<g:render template="/layouts/smart_template_inst/datatable_js" />