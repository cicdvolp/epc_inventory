<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="pratik"/>
	    <asset:javascript src="jquery.min.js"/>
        <script>
            $(document).ready(function(){
                getversion();
            });
            function getversion() {
                var d = document.getElementById("programtype").value;
                var d1 = document.getElementById("ay").value;
                <g:remoteFunction controller="EntranceReport" action="getEntranceversionAll" update="entrance_name" params="'programtypeId='+d+'&ay='+d1"/>
            }
        </script>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card card-topline-purple">
                                <div class="card-body">
                                    <g:form controller="EntranceReport">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label>Academic Year : </label>
                                                <g:select id="ay" name="ay" class="form-control" optionKey="id" optionValue="ay" from="${academicyear_list}" value="${aay?.academicyear?.id}" onchange="getversion();"/>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Program Type : </label>
                                                <g:select id="programtype" name="programtype" class="form-control" optionKey="id" optionValue="name" from="${programtype_list}" onchange="getversion();"/>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Entrance Version : </label>
                                                <span id="entrance_name"></span>
                                                <!--<g:select id="version" name="version" class="form-control" optionKey="id" optionValue="entrance_name" from="${entrance_version_list}"/>-->
                                            </div>
                                            <div class="col-sm-12">
                                                <center>
                                                    <br>
                                                    <g:submitToRemote id="search" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" url="[action:'getFullReport']"  update="report" value="Eligible Candidate" before="document.getElementById('smartprogressbar').style.display='block';"  onComplete="document.getElementById('smartprogressbar').style.display='none';" />
                                                </center>
                                            </div>
                                        </div>
                                    </g:form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="report"></div>
                <!-- end content here -->
            </div>
        </div>
        <!-- end page content -->
    </body>
</html>