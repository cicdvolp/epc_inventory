<div class="card">


			<div class="card-body row justify-content-center">


					<g:form action="Eligible_Candidate" controller="EntranceReport">
						<input type="hidden" name="ay" value="${ay}">
						<input type="hidden" name="programtype" value="${programtype}">
						<input type="hidden" name="entrance" value="${entrance}">
						<input type="hidden" name="program" value="${program}">
						<g:submitToRemote id="search" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" url="[action:'Eligible_Candidate']"  update="studentrecord1" value="Eligible Candidate" before="document.getElementById('smartprogressbar').style.display='block';"  onComplete="document.getElementById('smartprogressbar').style.display='none';" />
					</g:form>

			</div>
	<div class="card-body row justify-content-center">


					<g:form action="Exempted_for_Entrance_Test" controller="EntranceReport">
						<input type="hidden" name="ay" value="${ay}">
						<input type="hidden" name="programtype" value="${programtype}">
						<input type="hidden" name="entrance" value="${entrance}">
						<input type="hidden" name="program" value="${program}">
						<g:submitToRemote id="search" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" url="[action:'Exempted_for_Entrance_Test']"  update="studentrecord1" value="Exempted for Entrance Test" before="document.getElementById('smartprogressbar').style.display='block';"  onComplete="document.getElementById('smartprogressbar').style.display='none';" />

					</g:form>

</div>
	<div class="card-body row justify-content-center">

					<g:form action="Candidates_Appearing_for_Entrance_Test" controller="EntranceReport">
						<input type="hidden" name="ay" value="${ay}">
						<input type="hidden" name="programtype" value="${programtype}">
						<input type="hidden" name="entrance" value="${entrance}">
						<input type="hidden" name="program" value="${program}">
						<g:submitToRemote id="search" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" url="[action:'Candidates_Appearing_for_Entrance_Test']"  update="studentrecord1" value="Candidates Appearing for Entrance Test" before="document.getElementById('smartprogressbar').style.display='block';"  onComplete="document.getElementById('smartprogressbar').style.display='none';" />

					</g:form>

	</div>
	<div class="card-body row justify-content-center">

					<g:form action="Not_Eligible_Candidates" controller="EntranceReport">
						<input type="hidden" name="ay" value="${ay}">
						<input type="hidden" name="programtype" value="${programtype}">
						<input type="hidden" name="entrance" value="${entrance}">
						<input type="hidden" name="program" value="${program}">
						<g:submitToRemote id="search" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" url="[action:'Not_Eligible_Candidates']"  update="studentrecord1" value="Not Eligible Candidates" before="document.getElementById('smartprogressbar').style.display='block';"  onComplete="document.getElementById('smartprogressbar').style.display='none';" />

					</g:form>

	</div>
	<div class="card-body row justify-content-center">

					<g:form action="All_Applied_Candidates" controller="EntranceReport">
						<input type="hidden" name="ay" value="${ay}">
						<input type="hidden" name="programtype" value="${programtype}">
						<input type="hidden" name="entrance" value="${entrance}">
						<input type="hidden" name="program" value="${program}">
						<g:submitToRemote id="search" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" url="[action:'All_Applied_Candidates']"  update="studentrecord1" value="All Applied Candidates" before="document.getElementById('smartprogressbar').style.display='block';"  onComplete="document.getElementById('smartprogressbar').style.display='none';" />

					</g:form>

	</div>

</div>
</div>
