<html>
<head>
	<meta name="layout" content="smart_main_datatable_inst_scroll" />
	<asset:javascript src="jquery.min.js"/>
	<meta name="author" content="YASH" />
</head>

<body>
<div class="page-content-wrapper">
	<div class="page-content">
		<g:render template="/layouts/smart_template_inst/breadcrumb"/>
		<g:if test="${flash.message}">
			<div class="alert alert-success" >${flash.message}</div>
		</g:if>

		<div class="card">
			<g:form action="getreporttabs" controller="EntranceReport">
				<div class="card-body row">

					<div class="col-sm-3">
						<label>Academic Year : </label>
						<g:select id="ay"  name="ay" class="form-control"  value="${ayid}" optionKey="id" optionValue="ay" from="${aylist}"  onchange="updatePY()"/>
					</div>


					<div class="col-sm-3">
						<label>Program Type : </label>
						<g:select id="programtype"  name="programType" class="form-control"  optionKey="id" optionValue="name" from="${programtypelist}"  onchange="updatePY();programall()"/>
					</div>

					<div class="col-sm-3">
						<label>Entrance Version Name : </label>
						<span id="entrance_name"></span>
					</div>
					<div class="col-sm-3">
						<label>Program : </label>
						<span id="program"></span>
					</div>
					<div class="col-sm-1">
						<center><br/>
							<!--<input type="submit" class="mdl-button mdl-js-button mdl-button&#45;&#45;raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" value="Fetch"/>-->
							<g:submitToRemote id="search" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-30 btn-primary" url="[action:'getreporttabs']"  update="studentrecord" value="Search " before="document.getElementById('smartprogressbar').style.display='block';"  onComplete="document.getElementById('smartprogressbar').style.display='none';" />
						</center>
				</div>
			</g:form>
		</div>

	</div>
	<div id="studentrecord"></div>
	<div id="studentrecord1"></div>
</div>
<script>
   $(document).ready(function(){
                    updatePY();
                    programall();
                });

    document.getElementById('dteid').addEventListener('keypress', function(event) {
        if (event.keyCode == 13) {
            event.preventDefault();
        }
    });

$('#dteid').keydown(function(event){
    var keyCode = (event.keyCode ? event.keyCode : event.which);
    if (keyCode == 13) {
        $('#search').trigger('click');
    }
});



               function updatePY(){
                     var d = document.getElementById("programtype").value;
                     var d1 = document.getElementById("ay").value;

                     <g:remoteFunction controller="EntranceReport" action="getEntranceversionAll" update="entrance_name" params="'programtypeId='+d+'&ay='+d1"/>
                }
                function programall(){
                     var d = document.getElementById("programtype").value;


                     <g:remoteFunction controller="EntranceReport" action="getProgramAll" update="program" params="'programtypeId='+d"/>
                }




</script>
</script>
</body>
</html>