<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <div class="row">
                    <div class="col-md-12 ">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Entrance Version Details</header>
                            </div>
                                <div class="table-responsive">
                                     <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                       <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">Version Number</th>
                                                <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Version Date </th>
                                            </tr>
                                         </thead>
                                         <tbody>
                                             <g:each in="${entranceVersion}" var="name" status="i">
                                                  <tr>
                                                     <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${name?.academicyear}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${name?.version_number}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${name?.entrance_name}</td>
                                                     <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${name?.version_date}"/></td>
                                                  </tr>
                                            </g:each>
                                         </tbody>
                                     </table>
                                   </div>
                               </div>
                           </div>
                       </div>
                         <div class="card card-topline-purple">
                             <div class="card-head">
                                 <header class="erp-table-header">Set Authority Level</header>
                             </div>
                             <g:form action="saveEntranceAuthorityLevel">
                                 <div class="row" style="margin-left:30px">
                                     <div class="col-sm-4">
                                         <label for="pwd">Entrance Authority Level :</label>
                                          <input class="form-control" type="number" required="true" name="entranceAuthLevel">
                                     </div>
                                     <div class="col-sm-4">
                                        <label for="pwd">Entrance Authority Type :</label>
                                        <g:select name="entranceAuthorityType" from="${entranceAuthorityType_list?.name}" style="width: 100% !important;"   class="select2" required="true"  value=" "  noSelection="['null':'Select Entrance Authority Type']"  /><br>
                                     </div>
                                     <div class="col-sm-4" style="margin-top:30px">
                                         <input type="checkbox" id="islastauthority" class="css-checkbox" name="islastauthority"/> &nbsp; &nbsp; &nbsp; Is Last Authority?
                                     </div>
                                 </div><br>
                                 <center>
                                 <div>
                                     <input type="hidden" value="${entranceVersion?.id}" name="entranceVersionId">
                                     <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                 </div>
                                 </center>
                                 <br>
                             </g:form>
                         </div>

                <!--Table,Edit,Delete-->
               <div class="row">
                   <div class="col-md-12 ">
                       <div class="card card-topline-purple">
                           <div class="card-head">
                                <header class="erp-table-header">Entrance Authority Level List</header>
                               <!-- <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Entrance Authority Level" style="float:right; font-size:20px;"></span></g:link>
                                </div>-->
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Level</th>
                                                <th class="mdl-data-table__cell--non-numeric">Entrance Authority Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">EntranceVersion</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Last Authority</th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${entranceAuthorityLevel_list}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.level}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entranceauthoritytype?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entranceversion?.entrance_name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                      <g:link controller="entranceAdmissionMaster" action="activateIsLast_EntranceAuthLevel" params="[entranceAuthorityLevelId : name?.id]">
                                                            <g:if test="${name?.islastauthority}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                   <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>
                                                         <!--Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Entrance Authority Level</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editEntranceAuthorityLevel">
                                                                        <div class="modal-body">
                                                                            <div class="form-group row mx-2">
                                                                                <label class="my-2">Entrance Authority Level:</label><br>
                                                                                <g:field type="number" class="form-control " name="entranceAuthLevel" required="true" value="${name?.level}"/>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Entrance Authority Type :</label><br>
                                                                                <g:select name="entranceAuthorityType" from="${entranceAuthorityType_list?.name}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.entranceauthoritytype?.name}"  noSelection="['null':'Select Entrance Authority Type']"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Entrance Version :</label><br>
                                                                                <g:textField class="form-control " name="entranceVersion" required="true" value="${name?.entranceversion?.entrance_name}"/>
                                                                             <!--<g:select name="entranceVersion" from="${entranceVersion_list?.entrance_name}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.entranceversion?.entrance_name}"  noSelection="['null':'Select Entrance Version']"/><br>-->
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="entranceAuthorityLevelId" id="entranceAuthorityLevelId" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary mx-auto" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="entranceAdmissionMaster" action="deleteEntranceAuthorityLevel" params="[entranceAuthorityLevelId : name?.id, entranceVersionId:name?.entranceversion?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>

                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                         <!---- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Entrance Authority Level</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveEntranceAuthorityLevel">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Entrance Authority Level :</label>
                                                     <input class="form-control" type="number" required="true" name="entranceAuthLevel">
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Entrance Authority Type :</label><br>
                                                   <g:select name="entranceAuthorityType" from="${entranceAuthorityType_list?.name}" style="width: 100% !important;"   class="select2" required="true"  value=" "  noSelection="['null':'Select Entrance Authority Type']"  /><br>
                                                </div>
                                                <div class="form-group">
                                                      <label for="pwd">Entrance Version :</label><br>
                                                      <g:select name="entranceVersion"   from="${entranceVersion_list?.entrance_name}" style="width: 100% !important;"   class="select2" required="true"  value=" "  noSelection="['null':'Select Entrance Version']"  /><br>
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="islastauthority" class="css-checkbox" name="islastauthority"/> &nbsp; &nbsp; &nbsp; Is Last Authority?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <!--<button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>-->
                                                <g:link target="blank" controller="entranceAdmissionMaster" action="saveEntranceAuthorityLevel" params="[entranceAuthorityLevelId : entranceVersion?.id]" class="btn btn-primary">Proceed</g:link>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




