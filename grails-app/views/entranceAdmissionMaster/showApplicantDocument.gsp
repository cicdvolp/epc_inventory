<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->

                            <div class="row">
                               <div class="col-md-12  mobile-table-responsive">
                                   <div class="card card-topline-purple">
                                       <div class="card-head">
                                           <header class="erp-table-header">Documents Of : ${entranceApplication?.entranceapplicant?.fullname}</header>
                                       </div>
                                            <div class="table-responsive">
                                                <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Physically Handicapped</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Physically Handicapped Document</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Visually Handicapped</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Visually Handicapped Document</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exemption Type Details</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Entrance Documents</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <g:each in="${entranceApplication}" var="name" status="i">
                                                            <tr>
                                                                <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                <g:if test="${name?.is_physically_handicapped == true}">
                                                                    <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-check fa-2x" style="color:Green;text-size:5px" aria-hidden="true"></td>
                                                                </g:if>
                                                                <g:else>
                                                                    <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></td>
                                                                </g:else>
                                                                <g:if test="${name?.is_physically_handicapped == true}">
                                                                     <td class="mdl-data-table__cell--non-numeric">
                                                                         <g:include controller="entranceAdmissionMaster" action="getPhysicallyHandicappeddocumenturl" params="[is_physically_handicapped:name?.is_physically_handicapped, applicationId:name?.id]" />
                                                                     </td>
                                                                </g:if>
                                                                <g:else>
                                                                     <td class="mdl-data-table__cell--non-numeric">-</td>
                                                                </g:else>
                                                                <g:if test="${name?.is_visually_handicapped == true}">
                                                                     <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-check fa-2x" style="color:Green;text-size:5px" aria-hidden="true"></td>
                                                                </g:if>
                                                                <g:else>
                                                                     <td class="mdl-data-table__cell--non-numeric"><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></td>
                                                                </g:else>
                                                                <g:if test="${name?.is_visually_handicapped == true}">
                                                                      <td class="mdl-data-table__cell--non-numeric">
                                                                           <g:include controller="entranceAdmissionMaster" action="getVisuallyHandicappeddocumenturl" params="[is_visually_handicapped:name?.is_visually_handicapped, applicationId:name?.id]" />
                                                                       </td>
                                                                </g:if>
                                                                <g:else>
                                                                      <td class="mdl-data-table__cell--non-numeric">-</td>
                                                                </g:else>
                                                                <td class="mdl-data-table__cell--non-numeric">
                                                                   <div class="table-responsive ">
                                                                       <table id="exportTable" class="display nowrap erp-full-width ml-table-bordered" >
                                                                           <thead>
                                                                               <tr>
                                                                                   <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                                                   <th class="mdl-data-table__cell--non-numeric">Exemption Type</th>
                                                                                   <th class="mdl-data-table__cell--non-numeric">Download</th>
                                                                               </tr>
                                                                           </thead>
                                                                           <tbody>
                                                                               <g:each in="${name?.exemptiontype}" var="exemptionType" status="k">
                                                                                   <tr>
                                                                                       <td class="mdl-data-table__cell--non-numeric">${k+1}</td>
                                                                                       <td class="mdl-data-table__cell--non-numeric">${exemptionType?.name}</td>
                                                                                       <td class="mdl-data-table__cell--non-numeric">
                                                                                           <g:include controller="entranceAdmissionMaster" action="getExemptionTypedocumenturl" params="[exemptionTypeId:exemptionType?.id,applicationId:name?.id]" />
                                                                                       </td>
                                                                                   </tr>
                                                                               </g:each>
                                                                           </tbody>
                                                                       </table>
                                                                   </div>
                                                                </td>
                                                                 <td class="mdl-data-table__cell--non-numeric">
                                                                   <g:include controller="entranceAdmissionMaster" action="getEntranceApplicantDocumenturl" params="[applicationId:name?.id]" />
                                                                 </td>
                                                            </tr>
                                                        </g:each>
                                                    </tbody>
                                                </table>
                                            </div>
                                     </div>
                                </div>
                             </div>
                        </div>
                    </div>
        </div>
    </body>
</html>




