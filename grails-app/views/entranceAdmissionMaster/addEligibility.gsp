<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Eligibility List</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Budget Level" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable"  class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">

                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th  class="mdl-data-table__cell--non-numeric">Eligibility Statement</th>
                                                <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">Display Order</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active</th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${eligibility_list}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td style="height:30px;width:10px;word-wrap:break-word;" class="mdl-data-table__cell--non-numeric">${name?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.sort_order}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                      <g:link controller="entranceAdmissionMaster" action="activateEligibility" params="[eligibilityId : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                   <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                         <!--Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Eligibility</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editEligibility">
                                                                        <div class="modal-body">
                                                                            <div class="form-group row mx-2">
                                                                                <label class="my-2">Eligibility Statement:</label><br>
                                                                                <input type="hidden" value="${name?.id}" name="eligibilityId">
                                                                                <g:textArea class="form-control" id="eligibilityName" name="eligibilityName" rows="5" cols="40" required="true" value="${name?.name}"/>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Program Type :</label><br>
                                                                                <g:select name="programType" from="${programTypeList?.name}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.programtype?.name}"  noSelection="['null':'Select ProgramType']"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                               <label for="pwd">Display Order :</label><br>
                                                                               <input type="number" class="form-control" name="sortOrder" required="true" value="${name?.sort_order}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                  <g:if test="${name?.isactive}">
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive" checked="true"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                   </g:if>
                                                                                    <g:else>
                                                                                          <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                                                    </g:else>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="eligibilityId" id="eligibilityId" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary mx-auto" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="entranceAdmissionMaster" action="deleteEligibility" params="[eligibilityId : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>

                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                         <!---- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Eligibility</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveEligibility">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Eligibility Statement :</label>
                                                    <g:textArea class="form-control" id="eligibilityName" name="eligibilityName" rows="5" cols="40" required="true" value=""/>

                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Display Order :</label><br>
                                                   <input type="number" class="form-control" name="sortOrder" required="true" value="">
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Program Type :</label><br>
                                                   <g:select name="programType"   from="${programTypeList?.name}" style="width: 100% !important;"   class="select2" required="true"  value=" "  noSelection="['null':'Select Program type ']"  /><br>
                                               </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




