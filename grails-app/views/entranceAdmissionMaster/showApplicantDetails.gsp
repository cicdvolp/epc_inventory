<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                            <div class="row">
                                <div class="col-sm-12 mobile-table-responsive">
                                    <div class="card card-topline-purple full-scroll-table">
                                        <div class="card-head">
                                            <header class="erp-table-header">Applicant Details</header>
                                        </div>
                                        <div class="card-body " id="bar-parent">
                                            <div class="table-responsive">
                                                <table style="max-width: 100%;table-layout: auto;" id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Full Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Email</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Date of Birth</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Mobile Number</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Local Address</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Permanent Address</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Adhar Number</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Marital Status</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Nationality</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Category</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Domacile</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         <g:each in="${entranceapplicant}" var="name" status="i">
                                                              <tr>
                                                                 <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.fullname}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.email}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${name?.dateofbirth}"/></td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.mobilenumber}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.local_address}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.permanent_address}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.aadhar_number}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.maritalstatus}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.erpnationality}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.entrancecategory}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.erpdomacile}</td>
                                                              </tr>
                                                          </g:each>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
        </div>
    </body>
</html>




