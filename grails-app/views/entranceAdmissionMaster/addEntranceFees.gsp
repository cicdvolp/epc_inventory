<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Entrance Fees List</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Entrance Authority Level" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Fees</th>
                                                <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">Exemption</th>
                                                <th class="mdl-data-table__cell--non-numeric">Category Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">Entrance Version</th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${entranceFees_list}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.fees}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.exemption?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entrancecategorytype?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entranceversion?.entrance_name}</td>

                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>
                                                        <!--Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Entrance Fees</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editEntranceFees">
                                                                        <div class="modal-body">
                                                                            <div class="form-group row mx-2">
                                                                                <label class="my-2">Entrance Fees:</label><br>

                                                                                <input type="number" class="form-control " name="fees" required="true" value="${name?.fees}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Program Type :</label><br>
                                                                                <g:select name="programType" from="${programType_list}" optionValue="name"  style="width: 100% !important;"  class="select2" value="${name?.programtype?.id}"  noSelection="['null':'Select Program Type']" optionKey="id"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Exemption :</label><br>
                                                                                <g:select name="exemption" from="${exemption_list?.name}" style="width: 100% !important;"  class="select2" value="${name?.exemption?.name}"  noSelection="['null':'Select Exemption']"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Entrance Category Type :</label><br>
                                                                                <g:select name="entranceCategoryType" from="${entranceCategoryType_list?.name}" style="width: 100% !important;"  class="select2" value="${name?.entrancecategorytype?.name}"  noSelection="['null':'Select Entrance Categorytype Type']"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Entrance Version :</label><br>
                                                                                <g:select name="entranceVersion" from="${entranceVersion_list?.entrance_name}" style="width: 100% !important;"  class="select2" value="${name?.entranceversion?.entrance_name}"  noSelection="['null':'Select Entrance Version']"/><br>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="entranceFeesId" id="entranceFeesId" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary mx-auto" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="entranceAdmissionMaster" action="deleteEntranceFees" params="[entranceFeesId : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>
                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                         <!---- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Entrance Fees</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveEntranceFees">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Entrance Fees :</label>
                                                     <input class="form-control" type="number" required="true" name="fees">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Program Type :</label><br>
                                                    <g:select name="programType" from="${programType_list}" optionValue="name" style="width: 100% !important;"  class="select2" value=""  noSelection="['null':'Select Program Type']" optionKey="id" /><br>
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Exemption :</label><br>
                                                    <g:select name="exemption" from="${exemption_list?.name}" style="width: 100% !important;"  class="select2" value=""  noSelection="['null':'Select Exemption']"/><br>
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Entrance  Category Type :</label><br>
                                                   <g:select name="entranceCategoryType" from="${entranceCategoryType_list?.name}" style="width: 100% !important;"   class="select2" value=" "  noSelection="['null':'Select Entrance Category Type']"  /><br>
                                                </div>
                                                <div class="form-group">
                                                      <label for="pwd">Entrance Version :</label><br>
                                                      <g:select name="entranceVersion"   from="${entranceVersion_list?.entrance_name}" style="width: 100% !important;"   class="select2" value=" "  noSelection="['null':'Select Entrance Version']"  /><br>
                                                </div>

                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




