<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Entrance Branch List</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Budget Level" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Entrance Branch Name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Program</th>
                                                <th class="mdl-data-table__cell--non-numeric">Entrance Version</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active</th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${entranceBranch_list}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.program?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entranceversion?.entrance_name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                      <g:link controller="entranceAdmissionMaster" action="activateEntranceBranch" params="[entranceBranchId : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                   <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                         <!--Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Entrance Branch</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editEntranceBranch">
                                                                        <div class="modal-body">
                                                                            <div class="form-group row mx-2">
                                                                                <label class="my-2">Entrance Branch :</label><br>
                                                                                <input type="hidden" value="${name?.id}" name="entranceBranchId">
                                                                                <input type="text" class="form-control " name="entranceBranchName" required="true" value="${name?.name}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Program :</label><br>
                                                                                <g:select name="program" from="${program_list?.name}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.program?.name}"  noSelection="['null':'Select Program']"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Entrance Version :</label><br>
                                                                                <g:select name="entranceVersion" from="${entranceVersion_list?.entrance_name}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.entranceversion?.entrance_name}"  noSelection="['null':'Select Entrance Version']"/><br>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="entranceBranchId" id="entranceBranchId" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary mx-auto" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="entranceAdmissionMaster" action="deleteEntranceBranch" params="[entranceBranchId : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>

                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                         <!---- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Entrance Branch</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveEntranceBranch">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Entrance Branch :</label>
                                                     <input class="form-control" type="text" required="true" name="entranceBranchName">
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Program :</label><br>
                                                   <g:select name="program"   from="${program_list?.name}" style="width: 100% !important;"   class="select2" required="true"  value=" "  noSelection="['null':'Select Program ']"  /><br>
                                                </div>
                                                <div class="form-group">
                                                      <label for="pwd">Entrance Version :</label><br>
                                                      <g:select name="entranceVersion"   from="${entranceVersion_list?.entrance_name}" style="width: 100% !important;"   class="select2" required="true"  value=" "  noSelection="['null':'Select Entrance Version']"  /><br>
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




