<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                            <div class="row">
                                <div class="col-sm-12 mobile-table-responsive">
                                    <div class="card card-topline-purple full-scroll-table">
                                        <div class="card-head">
                                            <header class="erp-table-header">Entrance Version Details</header>
                                        </div>
                                        <div class="card-body " id="bar-parent">
                                            <div class="table-responsive">
                                                <table style="max-width: 100%;table-layout: auto;" id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exam Link</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Version Number</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application Track </th>
                                                            <th class="mdl-data-table__cell--non-numeric">Receipt Track</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Academic Year </th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application Start Date</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application End Date</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exam Date</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exam Start Time</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exam End Time</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Version Date</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         <g:each in="${version}" var="name" status="i">
                                                              <tr>
                                                                 <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.entrance_name}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.exam_link}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.version_number}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.applicationtrack}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.receipttrack}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.academicyear?.ay}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${name?.application_start_date}"/></td>
                                                                 <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${name?.application_end_date}"/></td>
                                                                 <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${name?.exam_date}"/></td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.exam_start_time}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.exam_end_time}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${name?.version_date}"/></td>
                                                              </tr>
                                                          </g:each>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
        </div>
    </body>
</html>




