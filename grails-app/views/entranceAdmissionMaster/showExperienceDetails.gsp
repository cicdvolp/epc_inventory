<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                            <div class="row">
                                <div class="col-sm-12 mobile-table-responsive">
                                    <div class="card card-topline-purple full-scroll-table">
                                        <div class="card-head">
                                            <header class="erp-table-header">Experience Details Of ${entranceApplicantExperience?.entranceapplicant?.fullname}</header>
                                        </div>
                                        <div class="card-body " id="bar-parent">
                                            <div class="table-responsive">
                                                <table style="max-width: 100%;table-layout: auto;" id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Organization Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">From Date</th>
                                                            <th class="mdl-data-table__cell--non-numeric">To Date</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Designation</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Is Currently Working?</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         <g:each in="${entranceApplicantExperience}" var="name" status="i">
                                                              <tr>
                                                                 <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.organization_name}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.from_date}"</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.to_date}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">${name?.designation}</td>
                                                                 <td class="mdl-data-table__cell--non-numeric">
                                                                     <g:if test="${name?.is_current_job}">
                                                                        <center><i class="fa fa-check fa-2x" style="color:Green" aria-hidden="true"></i></center>
                                                                    </g:if>
                                                                    <g:else>
                                                                           <center><i class="fa fa-close fa-2x" style="color:Red" aria-hidden="true"></i></center>
                                                                     </g:else>
                                                                 </td>
                                                              </tr>
                                                          </g:each>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
        </div>
    </body>
</html>




