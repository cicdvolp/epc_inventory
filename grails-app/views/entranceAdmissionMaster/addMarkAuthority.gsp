<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
                            .fabutton {
                              background: none;
                              padding: 0px;
                              border: none;
                            }
                            </style>
    </head>
     <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                    <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Entrance Authority</header>
                                </div>
                                <div class="card-body " id="bar-parent">
                                    <g:form controller="entranceAdmissionMaster" action="saveMarkedEntranceAuthority" name="form">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="pwd">Instructor Name</label>
                                                <g:select name="instructorId"   from="${instructor_list}"
                                                 class="select2 form-control" required="true"
                                                optionValue="${instructor?.person?.fullname_as_per_previous_marksheet}"
                                                optionKey="id"
                                                noSelection="['null':'Select Instructor']" />
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="pwd">Entrance Authority</label><br>
                                                <g:each in="${entranceAuthorityType_list?.name}" var="authority" status="i">
                                                    <g:checkBox style="margin-left:15px" name="entranceAuthority" value="${authority}"/>
                                                    &nbsp;&nbsp;
                                                    <label>${authority}</label>
                                                    &nbsp;&nbsp;
                                                    &nbsp;&nbsp;
                                                    &nbsp;&nbsp;
                                                </g:each>
                                            </div>
                                            <div class="col-sm-12">
                                                <center>
                                                    <br>
                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span>Add</button>
                                                </center>
                                            </div>
                                        </div>
                                    </g:form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-purple">
                                  <div class="table-responsive">
                                     <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                       <thead>
                                        <tr>
                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                            <th class="mdl-data-table__cell--non-numeric">Instructor Name</th>
                                            <th class="mdl-data-table__cell--non-numeric">Authority</th>
                                        <!--<th class="mdl-data-table__cell--non-numeric">Edit</th>-->
                                            <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                             <g:each in="${final_list}" var="name" status="i">
                                                 <tr>
                                                     <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${name}</td>
                                                     <td class="mdl-data-table__cell--non-numeric">${name?.entranceauthoritytype?.name}
                                                     </td>
                                                 <!--<td class="mdl-data-table__cell--non-numeric">
                                                         <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>
                                                          <!--Modal edit -->
                                                         <div class="modal fade" id="editModel${i}" role="dialog">
                                                             <div class="modal-dialog">
                                                                 <div class="modal-content">
                                                                     <div class="modal-header">
                                                                         <h4 class="modal-title">Edit Entrance Authority</h4>
                                                                         <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                     </div>
                                                                     <g:form action="editMarkEntranceAuthority">
                                                                         <div class="modal-body">
                                                                             <div class="form-group row mx-2">
                                                                                 <label class="my-2">Instructor :</label><br>&nbsp;&nbsp;
                                                                                 <input type="text" class="form-control" name="instructor" disabled="true" value="${name}">
                                                                             </div>
                                                                             <div class="form-group">
                                                                                 <label for="pwd">Authority:</label>
                                                                                    <g:each in="${entranceAuthorityType_list?.name}" var="authority" status="k">
                                                                                      <g:checkBox style="margin-left:15px" name="entranceAuthority" value="${authority}"/>
                                                                                      &nbsp;&nbsp;
                                                                                      <label>${authority}</label>
                                                                                      &nbsp;&nbsp;
                                                                                  </g:each>
                                                                             </div>
                                                                         </div>
                                                                         <div class="modal-footer">
                                                                             <input type="hidden" name="instructorId" id="final_listId" value="${name?.id}" />
                                                                             <button type="submit" class="btn btn-primary mx-auto" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                         </div>
                                                                     </g:form>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                     </td>
                                                     -->
                                                     <td class="mdl-data-table__cell--non-numeric">
                                                         <g:link controller="entranceAdmissionMaster" action="deleteMarkEntranceAuthority" params="[instructorId : name?.id,authority:name?.entranceauthoritytype?.name]">
                                                             <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                         </g:link>
                                                     </td>
                                                 </tr>
                                             </g:each>
                                        </tbody>
                                     </table>
                                  </div>
                            </div>
                        </div>
                    </div>
                </div>
          </div>
      </body>
      </html>
