<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
              background: none;
              padding: 0px;
              border: none;
            }
         </style>
          <script>

          </script>
    </head>
    <body>
            <!-- start page content -->
            <div class="page-content-wrapper">
                <div class="page-content">
                   <g:render template="/layouts/smart_template_inst/breadcrumb" />
                    <!-- add content here -->
                    <g:if test="${flash.message}">
                             <div class="alert alert-success" >${flash.message}</div>
                    </g:if>
                    <div class="row">
                        <div class="col-md-12 mobile-table-responsive">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Set Authority Level</header>
                                </div>
                                <div class="card-body " id="bar-parent">
                                        <g:form action="showEntranceVersionDetails" name="form">
                                            <div class="row">
                                                   <div class="col-sm-6">
                                                         <label for="ay">Academic Year</label>
                                                         <g:select name="ay" id="ay"  from="${academicYearList}"
                                                         style="width: 60% !important;"  class="select2"
                                                         optionValue="${ay}"
                                                         noSelection="['null':'Please Select Academic Year']"/>
                                                   </div>
                                                   <div class="col-sm-6">
                                                         <label style="margin-left:20px" for="poNo">Program Type :</label>
                                                         <g:select name="programType" id="programType"  from="${programTypeList?.name}"
                                                         style="width: 60% !important;"  class="select2"
                                                         optionValue="${programType}"
                                                         noSelection="['null':'Please Select Program Type']"/>
                                                   </div>
                                            </div><br>
                                            <center>
                                                <div>
                                                    <button type="submit" class="btn btn-primary" > <span class="glyphicon glyphicon-save"></span>Fetch</button>
                                                </div>
                                            </center>
                                        </g:form>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12  mobile-table-responsive">
                            <div class="card card-topline-purple">
                                <div class="card-head">
                                    <header class="erp-table-header">Entrance Version Details</header>
                                </div>
                                    <div class="table-responsive">
                                         <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                           <thead>
                                                <tr>
                                                    <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Academic Year</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Version Number</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                                    <th class="mdl-data-table__cell--non-numeric">Version Date </th>
                                                    <th class="mdl-data-table__cell--non-numeric">Set Entrance Authority Level</th>
                                                </tr>
                                             </thead>
                                             <tbody>
                                                 <g:each in="${entranceVersion}" var="name" status="i">
                                                      <tr>
                                                         <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${name?.academicyear}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${name?.version_number}</td>
                                                         <td class="mdl-data-table__cell--non-numeric">${name?.entrance_name}</td>
                                                         <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${name?.version_date}"/></td>
                                                        <!-- <td class="mdl-data-table__cell--non-numeric">
                                                              <g:form action="addEntranceAuthorityLevel">
                                                                   <input type="hidden" value="${name?.id}" name="entranceVersionId">
                                                                   <button><i class="fa fa-eye fa-2x erp-edit-icon-color"></i></button>
                                                             </g:form>
                                                         </td>-->
                                                         <td class="mdl-data-table__cell--non-numeric">
                                                             <g:link target="blank" controller="entranceAdmissionMaster" action="addEntranceAuthorityLevel" params="[entranceVersionId:name?.id]" >Set Authority Level</g:link>
                                                         </td>
                                                      </tr>
                                                </g:each>
                                             </tbody>
                                         </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
      </body>
</html>


