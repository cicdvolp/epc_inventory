<!doctype html>
<html>
    <head>
        <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
        <style>
            .fabutton {
                background: none;
                padding: 0px;
                border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />
                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>
                <!--////-->
                <!--Table,Edit,Delete-->
                <div class="row">
                    <div class="col-sm-12 mobile-table-responsive">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Entrance Version List</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Budget Level" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Entrance Name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Exam Link</th>
                                                <th class="mdl-data-table__cell--non-numeric">Version Number</th>
                                                <th class="mdl-data-table__cell--non-numeric">Application Track </th>
                                                <th class="mdl-data-table__cell--non-numeric">Receipt Track</th>
                                                <th class="mdl-data-table__cell--non-numeric">Program Type</th>
                                                <th class="mdl-data-table__cell--non-numeric">Academic Year </th>
                                                <th class="mdl-data-table__cell--non-numeric">Application Start Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Application End Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Exam Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Exam Start Time</th>
                                                <th class="mdl-data-table__cell--non-numeric">Exam End Time</th>
                                                <th class="mdl-data-table__cell--non-numeric">Version Date</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active </th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${entranceVersion_list}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.entrance_name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.exam_link}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.version_number}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.applicationtrack}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.receipttrack}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.programtype?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.academicyear?.ay}</td>
                                                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${name?.application_start_date}"/></td>
                                                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${name?.application_end_date}"/></td>
                                                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="dd-MM-yyyy" date="${name?.exam_date}"/></td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.exam_start_time}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.exam_end_time}</td>
                                                    <td class="mdl-data-table__cell--non-numeric"><g:formatDate format="yyyy-MM-dd" date="${name?.version_date}"/></td>

                                                    <td class="mdl-data-table__cell--non-numeric">
                                                      <g:link controller="entranceAdmissionMaster" action="activateEntranceVersion" params="[entranceVersionId : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                   <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>
                                                        <!--Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Entrance Version</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editEntranceVersion">
                                                                        <div class="modal-body">
                                                                            <div class="form-group row mx-2">
                                                                                <label class="my-2">Entrance Name :</label><br>
                                                                                <input type="text" class="form-control " name="entranceName" required="true" value="${name?.entrance_name}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Exam Link :</label><br>
                                                                                <input type="text" class="form-control" name="exam_link" required="true" value="${name?.exam_link}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Version Number :</label><br>
                                                                                <input type="number" class="form-control" name="version_number" required="true" value="${name?.version_number}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Application Track :</label><br>
                                                                                <input type="number" class="form-control" name="applicationtrack" required="true" value="${name?.applicationtrack}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Receipt Track :</label><br>
                                                                                <input type="number" class="form-control" name="receipttrack" required="true" value="${name?.receipttrack}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Program Type :</label><br>
                                                                                <g:select name="programType" from="${programTypeList?.name}" style="width: 100% !important;"  class="select2" required="true"  value="${name?.programtype?.name}"  noSelection="['null':'Select ProgramType']"/><br>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Academic Year :</label><br>
                                                                                <g:select style="width: 100% !important;" class="select2" id="academicYear" name='academicYear' value="${name?.academicyear?.id}" noSelection="${['null':'Select Academic Year']}" from='${academicYearList}' optionKey="id" optionValue="ay"></g:select>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Application Start Date :</label><br>
                                                                                <g:datePicker class="form-control" name="application_start_date" value="${name?.application_start_date}" precision="day"/>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                 <label>Application End Date :</label><br>
                                                                                <g:datePicker class="form-control" name="application_end_date" value="${name?.application_end_date}" precision="day"/>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Version Date :</label><br>
                                                                                <g:datePicker class="form-control" name="version_date" value="${name?.version_date}" precision="day"/>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Exam Date :</label><br>
                                                                                <g:datePicker class="form-control" name="exam_date" value="${new Date()}" precision="day"/>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Exam Start Time :</label><br>
                                                                                <g:field type="time" style="width:217px" required="true" name="exam_start_time" value="${name?.exam_start_time}"/>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label>Exam End Time :</label><br>
                                                                                <g:field type="time" style="width:217px" required="true" name="exam_end_time" value="${name?.exam_end_time}"/>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="entranceVersionId" id="entranceVersionId" value="${name?.id}"/>
                                                                            <button type="submit" class="btn btn-primary mx-auto"> <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="entranceAdmissionMaster" action="deleteEntranceVersion" params="[entranceVersionId : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>

                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                         <!---- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Entrance Version</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveEntranceVersion">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Entrance Name</label>
                                                     <input class="form-control" type="text" required="true" name="entranceName">
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Exam Link :</label><br>
                                                   <input type="text" class="form-control" name="exam_link" required="true" value="">
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Version Number :</label><br>
                                                   <input type="number" class="form-control" name="version_number" required="true" value="">
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Application Track :</label><br>
                                                   <input type="number" class="form-control" name="applicationtrack" required="true" value="">
                                                </div>

                                                <div class="form-group">
                                                   <label for="pwd">Receipt Track</label><br>
                                                   <input type="number" class="form-control" name="receipttrack" required="true" value="">
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Program Type :</label><br>
                                                   <g:select name="programType"   from="${programTypeList?.name}" style="width: 100% !important;"   class="select2" required="true"  value=" "  noSelection="['null':'Select Program type ']"  /><br>
                                                </div>
                                                <div class="form-group">
                                                   <label for="pwd">Academic Year :</label><br>
                                                   <g:select name="academicYear"   from="${academicYearList}" style="width: 100% !important;"   class="select2" required="true"  value=" "  noSelection="['null':'Select Academic Year ']"  /><br>
                                                </div>
                                                <div class="form-group">
                                                     <label>Application Start Date:</label><br>
                                                     <g:datePicker class="form-control" name="application_start_date" value="" precision="day"/>
                                                </div>
                                                <div class="form-group">
                                                     <label>Application End Date:</label><br>
                                                     <g:datePicker class="form-control" required="true" name="application_end_date" value="" precision="day"/>
                                                </div>
                                                <div class="form-group">
                                                     <label>Version Date:</label><br>
                                                     <g:datePicker class="form-control" required="true" name="version_date" value="" precision="day"/>
                                                </div>
                                                <div class="form-group">
                                                     <label>Exam Date:</label><br>
                                                     <g:datePicker class="form-control" required="true" name="exam_date" value="" precision="day"/>
                                                </div>
                                                <div class="form-group">
                                                     <label>Exam Start Time:</label><br>
                                                     <g:field type="time" style="width:217px" required="true" name="exam_start_time" value=""/>
                                                </div>
                                                <div class="form-group">
                                                     <label>Exam End Time:</label><br>
                                                     <g:field type="time" style="width:217px" required="true" name="exam_end_time" value=""/>
                                                    <!-- <g:datePicker class="form-control" name="exam_end_time" value="" precision="hour" precision="minute" />-->
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




