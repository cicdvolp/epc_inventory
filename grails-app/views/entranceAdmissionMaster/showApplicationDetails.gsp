<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="reena"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                            <div class="row">
                                <div class="col-sm-12 mobile-table-responsive">
                                    <div class="card card-topline-purple full-scroll-table">
                                        <div class="card-head">
                                            <header class="erp-table-header">Entrance Application Details</header>
                                        </div>
                                        <div class="card-body " id="bar-parent">
                                            <div class="table-responsive">
                                                <table style="max-width: 100%;table-layout: auto;" id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application Id</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Full Name</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Branch</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exam Exempted?</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Place</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Remark</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Categorytype</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Exam Exemption</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Rejection Reason</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application Status</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Application Receipt</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Applicant Details</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Version Details</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Qualification Details</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Experience Details</th>
                                                            <th class="mdl-data-table__cell--non-numeric">Applicant Documents</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                         <g:each in="${entranceApplication_list}" var="name" status="i">
                                                              <tr>
                                                                  <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.applicaitionid}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicant?.fullname}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entrancebranch?.name.join(', ')}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.is_exemption_from_test}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.place}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.remark}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entrancecategorytype?.name}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.exemption?.name}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entrancerejectionreason}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicationstatus}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">${name?.entranceapplicationreceipt}</td>
                                                                  <td class="mdl-data-table__cell--non-numeric">
                                                                      <g:link controller="entranceAdmissionMaster" action="showApplicantDetails" params="[entranceApplicationId : name?.id]">
                                                                      <i  style="cursor: pointer;" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                                  </td>
                                                                  <td class="mdl-data-table__cell--non-numeric">
                                                                      <g:link controller="entranceAdmissionMaster" action="showVersionDetails" params="[entranceApplicationId : name?.id]">
                                                                      <i  style="cursor: pointer;" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                                  </td>
                                                                  <td class="mdl-data-table__cell--non-numeric">
                                                                      <g:link controller="entranceAdmissionMaster" action="showQualificationDetails" params="[entranceApplicationId : name?.id]">
                                                                      <i  style="cursor: pointer;" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                                  </td>
                                                                  <td class="mdl-data-table__cell--non-numeric">
                                                                      <g:link controller="entranceAdmissionMaster" action="showExperienceDetails" params="[entranceApplicationId : name?.id]">
                                                                      <i  style="cursor: pointer;" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                                  </td>
                                                                  <td class="mdl-data-table__cell--non-numeric">
                                                                      <g:link controller="entranceAdmissionMaster" action="showApplicantDocument" params="[entranceApplicationId : name?.id]">
                                                                      <i  style="cursor: pointer;" class="fa fa-eye fa-2x erp-edit-icon-color"></i></g:link>
                                                                  </td>
                                                              </tr>
                                                          </g:each>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
        </div>
    </body>
</html>




