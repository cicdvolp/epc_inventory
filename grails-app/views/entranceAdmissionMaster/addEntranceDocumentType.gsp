<!doctype html>
<html>
    <head>
         <meta name="layout" content="smart_main_datatable_inst"/>
        <meta name="author" content="shreyas"/>
         <style>
            .fabutton {
                  background: none;
                  padding: 0px;
                  border: none;
            }
        </style>
    </head>
    <body>
        <!-- start page content -->
        <div class="page-content-wrapper">
            <div class="page-content">
             <div class="page-bar">
                <g:render template="/layouts/smart_template_inst/breadcrumb" />

                <!-- add content here -->
                <g:if test="${flash.message}">
                     <div class="alert alert-success" >${flash.message}</div>
                </g:if>
                <g:if test="${flash.error}">
                     <div class="alert alert-danger" >${flash.error}</div>
                </g:if>

                <!--////-->
                <!--Table,Edit,Delete-->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="card card-topline-purple">
                            <div class="card-head">
                                <header class="erp-table-header">Entrance Document Type List</header>
                                <div class="tools">
                                    <g:link  data-toggle="modal" data-target="#addModal"><span aria-hidden="true" class="icon-plus" title="Add New Budget Level" style="float:right; font-size:20px;"></span></g:link>
                                </div>
                            </div>
                            <div class="card-body " id="bar-parent">
                                <div class="table-responsive">
                                    <table id="exportTable" class="display nowrap erp-full-width table-striped  table-hover ml-table-bordered">
                                        <thead>
                                            <tr>
                                                <th class="mdl-data-table__cell--non-numeric">Sr. No.</th>
                                                <th class="mdl-data-table__cell--non-numeric">Document Name</th>
                                                <th class="mdl-data-table__cell--non-numeric">Size</th>
                                                <th class="mdl-data-table__cell--non-numeric">Extension</th>
                                                <th class="mdl-data-table__cell--non-numeric">Info</th>
                                                <th class="mdl-data-table__cell--non-numeric">Resolution</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Compulsory</th>
                                                <th class="mdl-data-table__cell--non-numeric">Is Active?</th>
                                                <th class="mdl-data-table__cell--non-numeric">Edit</th>
                                                <th class="mdl-data-table__cell--non-numeric">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <g:each in="${entranceDocumentType_list}" var="name" status="i">
                                                <tr>
                                                    <td class="mdl-data-table__cell--non-numeric">${i+1}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.name}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.size}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.extension}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.info}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">${name?.resolution}</td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                      <g:link controller="entranceAdmissionMaster" action="activate_IsCompulsoryEntranceDocumentType" params="[entranceDocumentTypeId : name?.id]">
                                                            <g:if test="${name?.iscompulsory}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                      <g:link controller="entranceAdmissionMaster" action="activateEntranceDocumentType" params="[entranceDocumentTypeId : name?.id]">
                                                            <g:if test="${name?.isactive}">
                                                                <i class="fa fa-toggle-on fa-2x" style="color:green" aria-hidden="true"></i>
                                                            </g:if>
                                                            <g:else>
                                                                <i class="fa fa-toggle-off fa-2x" style="color:red" aria-hidden="true"></i>
                                                            </g:else>
                                                        </g:link>
                                                    </td>
                                                   <td class="mdl-data-table__cell--non-numeric">
                                                        <i class="fa fa-2x fa-edit " style="color:green" aria-hidden="true" data-toggle="modal" data-target="#editModel${i}"></i>

                                                         <!--Modal edit -->
                                                        <div class="modal fade" id="editModel${i}" role="dialog">
                                                            <div class="modal-dialog">
                                                                <div class="modal-content">
                                                                    <div class="modal-header">
                                                                        <h4 class="modal-title">Edit Entrance Document Name</h4>
                                                                        <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                                                    </div>
                                                                    <g:form action="editEntranceDocumentType">
                                                                        <div class="modal-body">
                                                                            <div class="form-group row mx-2">
                                                                                <label class="my-2">Document Type :</label><br>&nbsp;&nbsp;
                                                                                <input type="hidden" value="${name?.id}" name="entranceDocumentTypeId">
                                                                                <input type="text" class="form-control" name="documentname" required="true" value="${name?.name}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Document Size :</label>
                                                                                 <input class="form-control" type="text" name="documentsize" required="true" value="${name?.size}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Document Extension :</label>
                                                                                 <input class="form-control" type="text" name="documentextension" required="true" value="${name?.extension}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label for="pwd">Document Info :</label>
                                                                                <input class="form-control" type="text" name="documentinfo" required="true" value="${name?.info}">
                                                                            </div>
                                                                            <div class="form-group">
                                                                                 <label for="pwd">Document Resolution :</label>
                                                                                 <input class="form-control" type="text" name="documentresolution" required="true" value="${name?.resolution}">
                                                                            </div>
                                                                            <div class="form-group row mx-2">
                                                                                <g:if test="${name?.iscompulsory}">
                                                                                <input type="checkbox" id="iscompulsory" class="css-checkbox my-2" name="iscompulsory" checked="true"/> &nbsp; &nbsp;Is Compulsory?
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <input type="checkbox" id="iscompulsory" class="css-checkbox my-2" name="iscompulsory"/> &nbsp; &nbsp;Is Compulsory?
                                                                                </g:else>
                                                                            </div>
                                                                            <div class="form-group row mx-2">
                                                                                <g:if test="${name?.isactive}">
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox my-2" name="isactive" checked="true"/> &nbsp; &nbsp;Is Active?
                                                                                </g:if>
                                                                                <g:else>
                                                                                    <input type="checkbox" id="isactive" class="css-checkbox my-2" name="isactive"/> &nbsp; &nbsp;Is Active?
                                                                                </g:else>
                                                                            </div>
                                                                        </div>
                                                                        <div class="modal-footer">
                                                                            <input type="hidden" name="entranceDocumentTypeId" id="entranceDocumentTypeId" value="${name?.id}" />
                                                                            <button type="submit" class="btn btn-primary mx-auto" > <span class="glyphicon glyphicon-save"></span> Update</button>
                                                                        </div>
                                                                    </g:form>
                                                                </div>
                                                            </div>
                                                        </div>

                                                    </td>
                                                    <td class="mdl-data-table__cell--non-numeric">
                                                        <g:link controller="entranceAdmissionMaster" action="deleteEntranceDocumentType" params="[entranceDocumentTypeId : name?.id]">
                                                            <i class="fa fa-2x fa-trash" style="color:red" aria-hidden="true" onclick="return confirm('Are you sure you have Delete?')" ></i>
                                                        </g:link>
                                                    </td>

                                                </tr>
                                            </g:each>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                         <!---- Modal New -->
                            <div class="modal fade" id="addModal" role="dialog">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h4 class="modal-title">Add New Entrance Document Type</h4>
                                            <a data-dismiss="modal"><i class="fa fa-close fa-2x" style="color:grey !important; display: inline-block !important;"></i></a>
                                        </div>
                                        <g:form action="saveEntranceDocumentType">
                                            <div class="modal-body">
                                                <div class="form-group">
                                                    <label for="pwd">Document Name :</label>
                                                    <input class="form-control" type="text" name="documentName" required="true">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Document Size :</label>
                                                    <input class="form-control" type="text" name="documentsize" required="true">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Document Extension :</label>
                                                     <input class="form-control" type="text" name="documentextension" required="true">
                                                </div>
                                                <div class="form-group">
                                                    <label for="pwd">Document Info :</label>
                                                     <input class="form-control" type="text" name="documentinfo" required="true">
                                                </div>
                                                <div class="form-group">
                                                     <label for="pwd">Document Resolution :</label>
                                                     <input class="form-control" type="text" name="documentresolution" required="true">
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="iscompulsory" class="css-checkbox" name="iscompulsory"/> &nbsp; &nbsp; &nbsp; Is Compulsory?
                                                </div>
                                                <div class="form-group">
                                                    <input type="checkbox" id="isactive" class="css-checkbox" name="isactive"/> &nbsp; &nbsp; &nbsp; Is Active?
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--colored mx-auto mx-auto">Save</button>
                                            </div>
                                        </g:form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>




