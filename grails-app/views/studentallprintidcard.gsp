<!DOCTYPE html>
<html>
<head>
	<title>All Id Catd</title>

</head>
<style type="text/css">
	@media print{@page {size: landscape}}

	@media print{
                     #noprint{
                         display:none;
                     }
             }


</style>
<body>

<div id="noprint">
	<center><button class="button" onclick="printPage()" style="background:blue;color:#fff;width:90px;height:35px;font-size:15px;"><span class="glyphicon glyphicon-print"></span>&nbsp;<b>Print</b></button></center>
	<ul style="display: inline-block;">
	    <li><b style="color:red;">Note:</b></li>
	    <li>Install CutTPDF or Win2PDF Software from Internet, Install</li>
	    <li>Select Printer CutTPDF or Win2PDF </li>
	    <li>Browser-Google Chrome</li>
	    <li>Paper Size-A4</li>
	    <li>Margin-None </li>
	</ul>
</div>

	<g:each in="${allstudentList}" var="studentinfo" status="i">
	    <div style="width:204px; height:702px; border: 1px solid black;   display: inline-block; margin-top: 35px;margin-left: 10px;" id='DivIdToPrint'>
	        <div>
              <div style="font-size: 8px; font-weight: bold; font-family:Tahoma; margin: 2px;" align="center">
                                   ${studentinfo?.organization?.trust_name}
              </div>
              <div align="center">
                  <g:if test="${studentinfo?.organization?.icard_organization_name=='Vishwakarma Institute of Technology'}">
                      <img src="https://www.vierp.in/logo/VITLogoName.jpg" style="width:190px;height:40px; ">
                  </g:if>
                  <g:else>
                      <img src="https://www.vierp.in/logo/VIITLogoName.jpg"  style="width:190px;height:40px; ">
                  </g:else>
              </div>
              <div style="font-size: 5px; font-weight: bold; font-family:Tahoma; margin: 2px;" align="center">
                  ${studentinfo?.organization?.icard_organization_tag}
              </div>
              <div style="font-size:7px; font-weight: bold; font-family:Tahoma; margin: 2px;" align="center">
                  ${studentinfo?.organization?.address}
              </div>
              <div style="font-size:8px; font-weight: bold; font-family:Tahoma; margin: 4px;" align="center">
                  Website: ${studentinfo?.organization?.website}
              </div>
              <div align="center">
                   <img style="width:60px;height:70px;border-radius:5px;" src="${createLink(controller:'studentidcard',action:'renderIcardPhoto',id:"${studentinfo.learner.registration_number}")}" params:'"${grnumber}"'>
              </div>
              <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;" align="center">
                  Gen.Reg. No.:
                  <g:if test="${studentinfo}">
                      ${studentinfo?.grno}
                  </g:if>
              </div>
              <div style="font-size:10px; font-weight: bold; font-family:Tahoma; margin: 4px; width:200px;min-height:30px;"  align="center">
                  <g:if test="${studentinfo}">
                      ${studentinfo?.fullname}
                  </g:if>
              </div>
              <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;" align="center">
                  <g:if test="${studentinfo?.program}">
                      ${studentinfo?.program}
                  </g:if>
              </div>

              <div align="center">
                  <g:if test="${studentinfo?.organization?.icard_organization_name=='Vishwakarma Institute of Technology'}">
                      <qrcode:image   text="${studentinfo?.grno}" alt="${studentinfo?.grno}" style="height:60px;width:60px;float:left;margin-left:10px;"/>
                      <img src="https://www.vierp.in/logo/vitdirectorsign_new.png"  style="margin-top:10px;width:50px;height:30px;float:right;margin-right:20px;">
                       </br>
                       </br>
                       </br>
                      <label style="font-size:8px;font-family:Tahoma;font-weight: bold;float:right;margin-right:20px;">DIRECTOR</label>
                  </g:if>
                  <g:else>
                      <qrcode:image   text="${studentinfo?.grno}" alt="${studentinfo?.grno}" style="height:60px;width:60px;float:left;margin-left:10px;"/>
                      <img src="https://vierp.in/logo/viitdirectorsign.jpg"  style="margin-top:10px;width:50px;height:30px;float:right;margin-right:20px;">
                      </br>
                      </br>
                      </br>
                      <label style="font-size:8px;font-family:Tahoma;font-weight: bold;float:right;margin-right:20px;">DIRECTOR </label>
                  </g:else>
              </div>

               <div style="color:#0caef9;font-family:Tahoma;font-weight: bold;margin-top:20px;font-size:9px;" align="center">
                 ${studentinfo.validupto}"
             </div>
            </div>
            <hr>
            <div style="margin:55px"></div>
            <hr>
            <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;width:200px;min-height:65px;margin-left: 4px;">
                 <label style="color:#0caef9">Permanent Address:</label>
                 ${studentinfo?.permanentaddress}
             </div>
             <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;">
                <label style="color:#0caef9">Mobile No: </label>
                ${studentinfo?.mobileno}
             </div>
             <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;">
                <label style="color:#0caef9">Course:</label>
                           ${studentinfo?.program}
             </div>
             <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;">
                <label style="color:#0caef9">Phone No:</label>
                ${studentinfo?.phone}
             </div>
             <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;">
                <label style="color:#0caef9">Birth Date:</label>
                      <g:formatDate date="${studentinfo?.dob}" format="dd-MM-yyyy"/>
             </div>
             <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;">
               <label style="color:#0caef9">Blood Group:</label>
                   ${studentinfo?.bloodgroup}
             </div>
             <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;">
               <label style="color:#0caef9">Admission Year:</label>
                   ${studentinfo?.admissionyear}
             </div>
             <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px; width:200px;min-height:30px;" >
               <label style="color:#0caef9">Emergency Contact Person:</label><br>
                        ${studentinfo?.emergency_contact_person_name}
             </div>
             <div style="font-size:9px; font-weight: bold; font-family:Tahoma; margin: 4px;">
               <label style="color:#0caef9">Emergency Contact Number:</label><br>
               ${studentinfo?.emergency_contact_person_number}
               <br>
               <br>

               <qrcode:image   text="${studentinfo?.studentinfo?.grno}" alt="${studentinfo?.grno}" style="height:70px;width:70px;float:left;margin-left:10px;"/>
             </div>
        </div>
    </g:each>
</body>
<asset:javascript src="jquery-2.2.0.min.js"/>


<script>
$(documents).ready(function(){
    alert("Print");
});
</script>
<script>
    function printPage() {
     window.print();
    }
</script>


<script>
    var preloader=document.getElementById("loader");
    function myfunload() {
     preloader.style.display='none';
    }
</script>
</html>