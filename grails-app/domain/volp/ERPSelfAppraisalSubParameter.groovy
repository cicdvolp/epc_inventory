package volp

class ERPSelfAppraisalSubParameter {

    int subparameter_no
    String subparameter_name
    String subparameter_description
    double basemarks
    double maxmarks //0 means scalable
    boolean isActive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpselfappraisalparameter:ERPSelfAppraisalParameter,
                      academicyear:AcademicYear]
    static mapping = {
        maxmarks defaultValue: 0
    }
    static constraints = {
    }
}

//[ex1 phD done -> basemark=200, maxmarks=200 =>not scalable]
//[ex2 research grant received -> basemark=300 (per 3lacs), //maxmarks=0 => scalable -> if 9lacs of grant received then 900 //marks]
