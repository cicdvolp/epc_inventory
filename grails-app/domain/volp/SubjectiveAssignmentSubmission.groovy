package volp

class SubjectiveAssignmentSubmission {
    String student_answer_file_path    //d:/cloud/courseoffering/courseofferingid/learnerid/ha/qno_filename
    String student_answer_file_name
    String student_answer_text
    Date submission_date
    double marks
    String teacher_remark
    double plagarisum_percentage
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[learner:Learner,subjectiveassignmentallocation:SubjectiveAssignmentAllocation,
                      erpcourse:ERPCourse,erpcourseoffering:ERPCourseOffering,
                      erpcourseofferinglearner:ERPCourseOfferingLearner,
                      organization:Organization,academicyear:AcademicYear,semester:Semester]
    static constraints = {
        student_answer_file_path nullable:true
        student_answer_file_name nullable:true
        student_answer_text nullable:true
        teacher_remark nullable:true
        erpcourse nullable:true
        erpcourseoffering nullable:true
        erpcourseofferinglearner nullable:true
        academicyear nullable:true
        semester nullable:true
    }
    static mapping = {
        marks defaultValue: -1
        plagarisum_percentage defaultValue: 0
    }
}
