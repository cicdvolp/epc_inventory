package volp

class ERPExamRemunerationTransaction
{
    int quantity
    double amount
    double extraamount
    double total_amount
    boolean isapproved
    boolean ispassed
    String remark
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      academicyear:AcademicYear,
                      semester:Semester,
                      instructor:Instructor,
                      erpassementtype:ERPAssementType,
                      erpexamdutytype:ERPExamDutyType,
                      erpexamdutycategory:ERPExamDutyCategory]
    static constraints = {
    }
    static mapping = {
        isapproved defaultValue:false
        ispassed defaultValue:false
    }
}
