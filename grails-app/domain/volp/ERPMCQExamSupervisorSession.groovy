package volp

class ERPMCQExamSupervisorSession {

    int sessionnumber
    Date sessiondate   //current date
    String starttime  //start slot
    String endtime   //end slot
    Date actualstarttime   //it should be time..plz correct it
    Date actualendtime  //it should be time..plz correct it
    boolean isCurrent
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpmcqexamsubjectgroupmaster:ERPMCQExamSubjectGroupMaster,
                      organization:Organization,mcqexamstatus:MCQExamStatus,
                      erpmcqbankassementtypelinkingmaster:ERPMCQBankAssementTypeLinkingMaster,erpmcqexamsupervisor:ERPMCQExamSupervisor]
    static constraints = {
        actualstarttime nullable:true
        actualendtime nullable:true
    }
}
