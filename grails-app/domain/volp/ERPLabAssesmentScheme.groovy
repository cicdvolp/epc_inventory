package volp

class ERPLabAssesmentScheme
{
    String name    //schemeName
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erplabassesmentschemetype:ERPLabAssesmentSchemeType]
    static constraints = {
    }
}
