package volp

class Template {

    String template_code
    String template_name
    double total_credits
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isactive
    static belongsTo=[organization:Organization,
                      templatetype:TemplateType,
                      academicyear:AcademicYear,
                      semester:Semester,
                      module:Module,
                      year:Year,
                      programtype:ProgramType,
                      program:Program]
    static constraints = {
        academicyear nullable:true
        semester nullable:true
        module nullable:true
        year nullable:true
        programtype nullable:true
        program nullable:true
    }
    static mapping = {
        isactive defaultValue:true
    }
    String toString(){
        template_code
    }

}
