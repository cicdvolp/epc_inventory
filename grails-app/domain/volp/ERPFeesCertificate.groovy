package volp

class ERPFeesCertificate {

    int outwardno
    String filename
    String filepath
    Date issuedon

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String foryears   //comma separated //track
    String total      //comma separated //track

    String rawprintdiv

    static belongsTo=[organization:Organization,academicyear:AcademicYear,year:Year,learner:Learner]
    static constraints = {
        foryears nullable : true
        total nullable: true
    }
}
//ay -> for which academic year
//ay+outwardno -> unique
//learner+year -> fees link --> which master