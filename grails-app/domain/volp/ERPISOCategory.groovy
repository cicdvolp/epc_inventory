package volp

class ERPISOCategory
{
    String category
    boolean isactive

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=
            [organization:Organization,erpisocategory:ERPISOCategory,roletype:RoleType]
    String toString()
    {
        category
    }
    static constraints = {
        erpisocategory nullable:true
    }

  }
