package volp

class ERPHostelFeeStructure {
    double comp_amt
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erphostelfeesmaster:ERPHostelFeesMaster,erphostelfeescomponent:ERPHostelFeesComponent,organization:Organization]

    static constraints = {
    }
}
