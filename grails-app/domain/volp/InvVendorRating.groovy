package volp

class InvVendorRating {

    double rating
    double rating_outof
    String comment
    Date rating_date

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization  : Organization,
                      invvendor     : InvVendor,
                      ratingby      : Instructor]

    static constraints = {
        comment nullable:true
        rating_date nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        rating defaultValue: 0
    }
}
