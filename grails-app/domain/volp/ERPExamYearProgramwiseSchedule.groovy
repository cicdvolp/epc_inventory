package volp

class ERPExamYearProgramwiseSchedule
{
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamdetailedschedule:ERPExamDetailedSchedule,erpexamplan:ERPExamPlan,year:Year,program:Program,programtype:ProgramType,module:Module]
    static constraints = {
        module nullable:true
        programtype nullable:true
    }
}
