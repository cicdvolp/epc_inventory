package volp

class Division {
	String name
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isregular   // false : passout , YD ....
    boolean  isActive
	static belongsTo=[organization:Organization]

    static mapping = {
        isregular defaultValue:true
    }

    static constraints = {

    }

    String toString(){
        name
    }
}
