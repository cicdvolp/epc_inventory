package volp

class HostelInstallmentDetails {

    Date date
    Date payment_date
    int payment_sequence
    double amount
    boolean isPaid

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[learner:Learner,hostelstudentreceipt:HostelStudentReceipt,
                      hostelfeestudentmaster:HostelFeeStudentMaster,
                      academicyear:AcademicYear,year:Year,organization:Organization]

    static constraints = {
        hostelstudentreceipt nullable: true
    }
}
