package volp

class DummyReceiptNumberTracking {
    String year
    String number
    boolean isactive
    static belongsTo=[organization:Organization, academicyear:AcademicYear]
    static constraints = {
        organization nullable: true
        academicyear nullable: true
    }
    static mapping = {
        isactive defaultValue: false
    }
}
