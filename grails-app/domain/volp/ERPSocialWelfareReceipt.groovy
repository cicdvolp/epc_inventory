package volp

class ERPSocialWelfareReceipt {
    String receipt_no
    double amount
    String remark
    Date date

    boolean iscancelled

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpfeestudentmaster:ERPFeeStudentMaster,erpfeedepositetype:ERPFeesDepositeType,year:Year,learner:Learner,socialwelfarecomponent:SocialWelfareComponent,organization:Organization]
    static hasMany = [socialwelfarereceipttransactions:SocialWelfareReceiptTransactions]
    static constraints = {
        erpfeestudentmaster nullable: true
        remark nullable: true
        organization nullable :true
    }
    String toString()
    {
        receipt_no
    }
    static mapping = {
        iscancelled defaultValue: false
    }
}
