package volp

class MitraSelfDeclaration {

    String area

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, lastvisiteddistrict:ERPDistrict,
                       learner:Learner, instructor:Instructor, mitratypeofstay:MitraTypeOfStay,
                       zone:MitraZone, mitravisitor:MitraVisitor]

    static constraints = {
        learner nullable:true
        instructor nullable:true
        mitravisitor nullable:true
        organization nullable:true
    }
}
