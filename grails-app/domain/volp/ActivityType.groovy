package volp

class ActivityType {

    String type //Laboratory Development, Internship, Placement, Incubation/Enterprenurship,startup support, Collaboration, Alumni Interaction, Services to Community, Academic Innovation, PhD Guidance, Session Chair, Reviewer, PG Guidance, UG Guidance
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
    static belongsTo=[organization:Organization]

}
