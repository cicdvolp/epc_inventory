package volp

class ERPHostelReceiptTransactions {
    String tid//dd no,chk no,utrn
    Date dddate
    String bank_name
    double amount
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erppaymentmode:ERPPaymentMode,erphostelstudentreceipt:ERPHostelStudentReceipt]

    static constraints = {
        bank_name nullable:true
        tid nullable:true
    }
}