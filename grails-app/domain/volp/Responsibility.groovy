package volp

class Responsibility {

    String description
    String filename
    String filepath
    Date fromdate
    Date todate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        filename nullable: true
        filepath nullable: true
    }
    static mapping = {
        isactive defaultValue: true
    }
    static belongsTo=[organization:Organization,instructor:Instructor,responsibilitytype: ResponsibilityType]

}
