package volp

class ERPRefundReceipt {
    String receipt_no
    double amount
    String remark
    Date date

    boolean isadmissioncancel
    boolean isyeardown

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpfeestudentmaster:ERPFeeStudentMaster,erpfeedepositetype:ERPFeesDepositeType,year:Year,learner:Learner,erpforeignnationalfeesstudentmaster:ERPForeignNationalFeesStudentMaster,organization:Organization]
    static hasMany = [refundreceipttransactions:RefundReceiptTransactions]
    static constraints = {
        erpfeestudentmaster nullable: true
        erpforeignnationalfeesstudentmaster nullable: true
        remark nullable: true
        organization nullable :true
        isadmissioncancel nullable :false
    }
    String toString()
    {
        receipt_no
    }
    static mapping = {
        isyeardown defaultValue: false
    }
}
