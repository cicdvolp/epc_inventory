package volp

class ERPPOAttainment {

    boolean isAttained
    double attained_value
    double po_threshold

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erppo:ERPPO, organization:Organization, academicyear:AcademicYear, program:Program]

    static constraints = {
    }
}
