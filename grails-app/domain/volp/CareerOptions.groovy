package volp

class CareerOptions {

    String name
    boolean isactive
    boolean isdeleted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[careeroptions: CareerOptions , organization:Organization]

    static constraints = {
        careeroptions nullable : true
    }

    static mapping = {
        isdeleted defaultValue: false
        isactive defaultValue: true
    }
}
