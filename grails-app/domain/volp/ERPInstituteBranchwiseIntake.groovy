package volp

class ERPInstituteBranchwiseIntake {

    int intake
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      program:Program,
                      programtype:ProgramType,
                      year:Year,
                      erpshift:ERPShift,
                      academicyear:AcademicYear]
    static constraints = {
    }
}
