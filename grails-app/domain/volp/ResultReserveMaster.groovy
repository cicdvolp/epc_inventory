package volp

class ResultReserveMaster {

    String reason   //Fees Not Paid   /  Document Non-Submission
    String gradename   //RR/FNP
    int sequence
    String remark
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    remark nullable:true
    }

}
