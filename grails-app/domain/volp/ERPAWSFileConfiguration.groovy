package volp

class ERPAWSFileConfiguration {
    String applicationtype //example ASPortal
    int filesizelimit                        // example 1 in Mb
    String allowedextension                  //comma separated //example -> doc,txt
    static belongsTo=[organization:Organization]
    static constraints = {
        allowedextension nullable :true
    }
}
