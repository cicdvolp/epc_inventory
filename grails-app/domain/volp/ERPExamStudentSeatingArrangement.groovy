package volp

class ERPExamStudentSeatingArrangement
{

    int benchnumber
    String benchside     //R/L
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamcapcode:ERPExamCAPCode,learner:Learner,erproom:ERPRoom,erpcourseofferinglearner:ERPCourseOfferingLearner,erpexamplan:ERPExamPlan,erpexamcoursewiseschedule:ERPExamCoursewiseSchedule,erpexamroomofferingschedule:ERPExamRoomOfferingSchedule,erpexamroomofferingbenches:ERPExamRoomOfferingBenches]
    static constraints = {
        erpexamcapcode nullable:true
    }
}
