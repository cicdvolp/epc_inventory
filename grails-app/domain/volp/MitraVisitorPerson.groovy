package volp

class MitraVisitorPerson {

    String mobile  // unique
    String email    // unique
    String otp
    String name
    String filepathofphoto  //(optional)
    String folderpathofphoto //(optional)
    boolean isotpverified

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static mapping = {
        isotpverified defaultValue: true
    }

    static constraints = {
        filepathofphoto nullable:true
        folderpathofphoto nullable:true
        name nullable:true
        mobile unique: true, nullable:false
        email unique: true, nullable:false
    }
}
