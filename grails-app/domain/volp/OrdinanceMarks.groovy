package volp

class OrdinanceMarks {

    double gracemarks
    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, academicyear:AcademicYear,
                       semester:Semester,  erpcourseofferinglearner:ERPCourseOfferingLearner,
                       erplearnerassesmentmarks : ERPLearnerAssesmentMarks,
                       erplearnerbacklogassesmentmarks : ERPLearnerBacklogAssesmentMarks,
                       ordinancemaster:OrdinanceMaster]

    static constraints = {
        erplearnerbacklogassesmentmarks nullable:true
        erplearnerassesmentmarks nullable:true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
