package volp

class InvPurchaseRequisition {

    int prn         // purchase requisition number –serial number
    double amount
    String purpose
    String file_name
    String file_path
    Date request_date
    boolean isapproved              // not used
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization              : Organization,
                      academicyear              : AcademicYear,
                      invpurchasetype           : InvPurchaseType,
                      invbudget                 : InvBudget,
                      invinvitationstatus       : InvInvitationStatus,
                      department                : Department,
                      invvendor                 : InvVendor,
                      invapprovalstatus         : InvApprovalStatus,
                      requestedby               : Instructor]

    static constraints = {
        purpose nullable : true
        file_name nullable : true
        file_path nullable : true
        invpurchasetype nullable : true
        invinvitationstatus nullable : true
        invbudget nullable : true
        invvendor nullable : true
        department nullable : true
    }

    static mapping = {
        isactive defaultValue: true
        isapproved defaultValue: false
        prn defaultValue: 0
        amount defaultValue: 0
    }
}
