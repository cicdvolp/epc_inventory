package volp

class ERPLearnerAssesmentMarks {

    double actual_marks   //actual total cowise marks  (sum of all cowise marks)  evaluated_marks
    double converted_marks    //converted marks
    boolean isAbsent

    double reevalmarks
    double reeval_converted_marks
    boolean isappliedforreeval
    boolean is_approved_by_faculty  //second examiner will approve
    boolean issubmitted   //true mean submitted,, false means NOT submitted   first examiner will submit

    double reexammarks
    double reexam_converted_marks
    double reexam_actual_marks
    boolean isappliedforreexam
    boolean is_reexam_approved_by_faculty  //second examiner will approve
    boolean isreexam_submitted   //true mean submitted,, false means NOT submitted   first examiner will submit

    boolean isordinanceapplied

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,approvedby:Instructor,submittedby:Instructor,
                      learner:Learner,instructor:Instructor,erpcourseofferinginstructor:ERPCourseOfferingInstructor,erpexamcapcode:ERPExamCAPCode,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,erpcurseofferingbatchlearner:ERPCourseOfferingBatchLearner,erpcourseoffering:ERPCourseOffering,
                      erpassesmentschemedetails:ERPAssesmentSchemeDetails,erpcourseofferinglearner:ERPCourseOfferingLearner,
                      program:Program,
                      programtype:ProgramType,
                      academicyear:AcademicYear,
                      semester:Semester,
                      divisionoffering:DivisionOffering,erpexamconducttype:ERPExamConductType]
    static constraints = {
        learner nullable:true
        instructor nullable:true
        erpexamcapcode nullable:true
        erpcourseoffering nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcurseofferingbatchlearner nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcourseofferinginstructor nullable:true
        erpcourseofferinglearner nullable:true
        approvedby nullable:true
        submittedby nullable:true
        program nullable:true
        programtype nullable:true
        academicyear nullable:true
        divisionoffering nullable:true
        erpexamconducttype nullable:true
    }
    static mapping = {
        isordinanceapplied defaultValue: false

        isAbsent defaultValue: false
        is_approved_by_faculty defaultValue: false
        issubmitted defaultValue: false
        isappliedforreeval defaultValue:false
        reevalmarks defaultValue : -999
        reeval_converted_marks defaultValue : -999

        isappliedforreexam defaultValue:false
        reexammarks defaultValue : -999
        reexam_converted_marks defaultValue : -999
        reexam_actual_marks defaultValue : -999
        is_reexam_approved_by_faculty defaultValue: false
        isreexam_submitted defaultValue: false
    }
}
