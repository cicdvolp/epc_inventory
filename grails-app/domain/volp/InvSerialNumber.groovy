package volp

class InvSerialNumber {

    int number

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      department            : Department,
                      academicyear          : AcademicYear,
                      invserialnumbertype   : InvSerialNumberType]

    static constraints = {
        invserialnumbertype nullable:true
    }

    static mapping = {
        number defaultValue: 0
    }
}
