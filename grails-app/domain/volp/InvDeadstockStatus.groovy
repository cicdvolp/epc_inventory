package volp

class InvDeadstockStatus {

    String status       //Missing/Working-Condition/Not-Working-But-Repairable/Not-Working-And-Not-Repairable/WriteOff-Recomonded/Writeoff-Approved
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {}

    static mapping = {
        isactive defaultValue: true
    }

}
