package volp

class ERPFeedbackQuestionOptions {

    int option_sno
    String option_display_no  //a,b,c...
    String option_statement
    double option_weightage
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpfeedbackquestionare:ERPFeedbackQuestionare]
    static constraints = {
    }
}
