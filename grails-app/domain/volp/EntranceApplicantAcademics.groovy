package volp

class EntranceApplicantAcademics {

    String yearofpassing
    String university           //university or Board Name
    String branch
    String specialization
    double cpi_marks            // CPI/Marks

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[entranceapplicant : EntranceApplicant,
                      entrancedegree    : EntranceDegree,
                      recclass          : RecClass,
                      organization      : Organization]

    static constraints = {
        organization nullable: true
    }
}
