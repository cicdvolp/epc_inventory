package volp

class InvMaterial {

    String name         // Desktop, Laptop
    String specification
    String code
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      invmaterialtype       : InvMaterialType,
                      invmaterialcategory   : InvMaterialCategory,
                      invmaterialset        : InvMaterialSet]

    static hasMany = [invmaterialpart      : InvMaterialPart]

    static constraints = {}

    static mapping = {
        isactive defaultValue: true
    }
}
