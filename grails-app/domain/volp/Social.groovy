package volp

class Social {
    String site_name    //facebook, twitter, linkeddin, skype
    String site_profile_link    //profile link
    String siteid
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address    
    static constraints = {
        siteid nullable:true
    }
}
