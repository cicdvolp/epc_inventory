package volp

class InvPurchaseOrder {

    int pon             // purchase order number –serial number
    double amount
    double tax
    double total
    String purpose
    String file_name
    String file_path
    Date purchase_order_date
    boolean isapproved

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization      : Organization,
                      invvendor         : InvVendor,
                      invquotation      : InvQuotation,
                      releasedby        : Instructor]

    static constraints = {
        purpose nullable:true
        file_name nullable:true
        file_path nullable:true
        purchase_order_date nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        isapproved defaultvalue:false
        pon defaultvalue:0
        amount defaultvalue:0
        tax defaultvalue:0
        total defaultvalue:0
    }
}
