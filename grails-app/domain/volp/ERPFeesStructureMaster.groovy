package volp

class ERPFeesStructureMaster
{
    double total_fees_amount
    double fees_receivable_from_sw   //by default it should be zero
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    //currentyear -> aay.ay.......................................currentacademicyear:AcademicYear
    //academicyear -> this is learner admission/admitted year
    static belongsTo=[academicyear:AcademicYear,semester:Semester,organization:Organization,currentacademicyear:AcademicYear,
                      feescurrencytype:FeesCurrencyType,erpfeesdepositetype:ERPFeesDepositeType,
                      erpstudentfeescategory:ERPStudentFeesCategory,feesprogramtype:FeesProgramType,
                      year:Year,erpstudenttype:ERPStudentType,erpcountry:ERPCountry,
                      erpfeesaccountdetails:ERPFeesAccountDetails , program:Program]
    static constraints = {
        erpfeesaccountdetails nullable : true
        semester nullable : true
        currentacademicyear nullable: true
        program nullable: true
    }
    String toString()
    {
        total_fees_amount
    }
    static mapping = {
        fees_receivable_from_sw defaultValue: 0
    }
}
