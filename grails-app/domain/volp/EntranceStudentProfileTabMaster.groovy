package volp

class EntranceStudentProfileTabMaster {

    String name
    boolean iscompulsory

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[]

    static mapping = {
        iscompulsory defaultValue: false
    }

    static constraints = { }
}
