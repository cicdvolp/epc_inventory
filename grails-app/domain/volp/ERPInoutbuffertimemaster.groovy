package volp

class ERPInoutbuffertimemaster {

    Date fromdate
    Date todate

    int inbuffertime
    int outbuffertime

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]


    String toString(){
        inbuffertime + " - " + outbuffertime
    }


}
