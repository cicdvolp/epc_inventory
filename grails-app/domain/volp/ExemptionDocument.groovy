package volp

class ExemptionDocument {

    String file_path
    String file_name

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo = [organization  		: Organization,
                        exemptiontype 		: ExemptionType,
                        exemption 			: Exemption,
                        exemptionBy 		: Instructor,
                        entranceversion     : EntranceVersion,
                        entranceapplicant   : EntranceApplicant]

    static constraints = {
        exemptionBy nullable:true
        exemption nullable:true
    }
}