package volp

class ERPSelfAppraisalParameterAuthority {

    int level_no  //1,2,3   3->last/approve
    boolean islastauthority
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      academicyear:AcademicYear,
                      erpselfappraisalparameter:ERPSelfAppraisalParameter,
                      erpselfappraisalauthoritymaster:ERPSelfAppraisalAuthorityMaster,
                      erpselfappraisalfacultytype:ERPSelfAppraisalFacultyType]
    static constraints = {
    }
    static mapping = {
        islastauthority defaultValue: false
    }
}
