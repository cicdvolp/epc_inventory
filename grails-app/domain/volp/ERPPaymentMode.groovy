package volp

class ERPPaymentMode {

    String mode    //online/DD/Cash
    boolean isbankdetailsrequired
    boolean isvisibletostudent
    boolean isutrapplicable
    boolean isofflinepaymentmode
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
    }

    static mapping = {
        isbankdetailsrequired defaultValue: true
        isvisibletostudent defaultValue: false
        isutrapplicable defaultValue: false
        isofflinepayment defaultValue: false
    }

    String toString(){
        mode
    }
}

