package volp

class ERPCourseOfferingLearnerChoice
{
    int priority
    boolean isallocated
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourseoffering:ERPCourseOffering,learner:Learner,academicyear:AcademicYear,semester:Semester,organization:Organization]
    static constraints = {
        academicyear nullable:true
        semester nullable:true
        organization nullable:true
    }
    static mapping = {
        isallocated defaultValue: false
    }
}
