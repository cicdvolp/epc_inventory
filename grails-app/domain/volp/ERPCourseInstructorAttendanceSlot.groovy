package volp

class ERPCourseInstructorAttendanceSlot {

    int period_number  //theory/practical/tutorial number
    Date execution_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseofferingbatch:ERPCourseOfferingBatch,
                      erpcourseoffering:ERPCourseOffering,
                      slot :Slot,
                      divisionoffering:DivisionOffering,
                      instructor:Instructor,
                      academicyear:AcademicYear,
                      loadtypecategory:LoadTypeCategory,
                      semester:Semester,organization:Organization]

    static constraints = {
        divisionoffering nullable: true
    }
}
