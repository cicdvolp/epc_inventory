package volp

class ERPExamCoursewiseSchedule {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamplan:ERPExamPlan,erpexamyearprogramwiseschedule:ERPExamYearProgramwiseSchedule,erpcourseoffering:ERPCourseOffering]
    static constraints = {
    }
}
