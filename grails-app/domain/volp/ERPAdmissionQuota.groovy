package volp

class ERPAdmissionQuota {

    String type
    String username
    Date creation_date
    Date updation_date
    boolean isForeignNational
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpadmissionquotagroup:ERPAdmissionQuotaGroup]
    static constraints = {
        erpadmissionquotagroup nullable: true
    }
    static mapping = {
        isForeignNational defaultValue: false
    }
    String toString(){
        type
    }
}
