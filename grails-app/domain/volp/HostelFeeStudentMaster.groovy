package volp

class HostelFeeStudentMaster {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostelfeesstructuremaster:HostelFeesStructureMaster,
                      academicyear:AcademicYear, learner:Learner]

    static hasMany = [hostelstudentreceipt:HostelStudentReceipt]

    static constraints = {
        learner unique: 'hostelfeesstructuremaster'
    }
}
