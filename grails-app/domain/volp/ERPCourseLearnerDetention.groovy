package volp

class ERPCourseLearnerDetention
{
    boolean isdetained   //true->detained   false->Not detained
    String remark
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpcoursedetaintionreasonmaster:ERPCourseDetaintionReasonMaster,erpcourseoffering:ERPCourseOffering,erpcourseofferinglearner:ERPCourseOfferingLearner,learner:Learner,academicyear:AcademicYear,semester:Semester]
    static constraints = {
    }
    static mapping = {
        isdetained defaultValue: false
    }
}
