package volp

class ERPCommonReceiptNumberTracker {

    String year
    String number
    Boolean isactive
    static belongsTo=[organization:Organization, erpreceipttypemaster:ERPReceiptTypeMaster, academicyear:AcademicYear]
    static constraints = {
    }
}
