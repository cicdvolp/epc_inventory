package volp

class ERPLearnerGrievence {

    String query    //grivence query by learner
    Date pushdate
    Date actiontakendate
    String filename
    String filepath
    String remark
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,learner:Learner,
                      erplearnergrievencetype:ERPLearnerGrievenceType,
                      academicyear:AcademicYear,semester:Semester,erpcourse:ERPCourse,
                      erpcourseoffering:ERPCourseOffering,
                      erpgrievencestatus:ERPGrievenceStatus,erpexamconducttype:ERPExamConductType,
                      actionby:Instructor]
    static constraints = {
        filename nullable:true
        filepath nullable:true
        remark nullable:true
        actiontakendate nullable:true
        erpcourse nullable:true
        erpcourseoffering nullable:true
        actionby nullable:true
    }
}
