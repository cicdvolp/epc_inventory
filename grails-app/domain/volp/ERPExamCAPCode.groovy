package volp

class ERPExamCAPCode {

    String capcode //Examtype(MSE-ESE)-B-Year-uniquenumber   Ex. ESER-B-FY-1  R:Regular,B:Backlog,S:Summer    B-BTech,M-MTech, C-MCA
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,instructor:Instructor,erpexamplan:ERPExamPlan,
                      erpexamcoursewiseschedule:ERPExamCoursewiseSchedule,
                      erpexamroomofferingschedule:ERPExamRoomOfferingSchedule,
                      divisionoffering:DivisionOffering,erpexamcapcodestatus:ERPExamCAPCodeStatus,
                      erpexamconducttype:ERPExamConductType]
    static hasMany = [erpcourseoffering:ERPCourseOffering]
    static constraints = {
        instructor nullable:true
        erpexamcapcodestatus nullable:true
        erpexamconducttype nullable:true
        erpexamcoursewiseschedule nullable:true
    }
}
