package volp

class  AdmissionApplicant {
    String applicationid
    String name_of_candidate
    String merit_no
    String mht_cet_score
    String jee_marks
    String diploma_marks

    String fullname_as_per_previous_marksheet
    String firstName
    String middleName
    String nameasperaadhar

    String lastName
    Date date_of_birth
    String father_first_name
    String mother_first_name
    double family_income

    String other_cast
    String other_subcast

    String passport_no
    Date passport_valid_upto

    String visa_no
    Date visa_valid_upto

    String resedential_permit_no
    Date resedential_permit_issue_date
    Date resedential_permit_valid_upto_date

    String fsisnumber

    boolean isapplicationsubmitted
    boolean isdocumentsubmitted
    boolean isphysicaldocumentsubmitted

    double applicable_fees
    boolean isebccandidate
    boolean isfeespaid
    boolean isofflinepayment

    Date application_submission_date

    String creation_user_name
    String updation_user_name
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    String email
    String grno

    static belongsTo=[organization      :Organization, erpround:ERPAdmissionRound, gender:Gender,
                      category          :ERPStudentAdmissionMainCategory, erpshift:ERPShift,
                      erpseattype       :ERPSeatType, program:Program, academicyear:AcademicYear,
                      admissionlogin    :AdmissionLogin, year:Year, admissionyear:AcademicYear,
                      erpdomacile       :ERPDomacile, erpadmissionquota:ERPAdmissionQuota, erpnationality:ERPCountry,
                      erpscholarshiptype:ERPScholarshipType, erpcast:ERPCast,
                      erpsubcast        :ERPSubCast, admissionstatus:AdmissionStatus,
                      erpstudentfeescategory:ERPStudentFeesCategory
    ]

    static mapping = {

        isapplicationsubmitted defaultValue:false
        isdocumentsubmitted defaultValue:false
        isfeespaid defaultValue:false
        isofflinepayment defaultValue:false
            isphysicaldocumentsubmitted defaultValue:false
    }

    static constraints = {
        erpdomacile nullable: true
        erpsubcast nullable: true
        email nullable:true
        grno nullable:true
        erpdomacile nullable: true
        passport_no nullable: true
        passport_valid_upto nullable: true
        visa_no nullable: true
        visa_valid_upto nullable: true
        fsisnumber nullable: true
        resedential_permit_no nullable: true
        resedential_permit_issue_date nullable: true
        resedential_permit_valid_upto_date nullable: true
        other_cast nullable: true
        other_subcast nullable: true
        erpcast nullable: true
        erpstudentfeescategory nullable: true
        application_submission_date nullable: true
        jee_marks nullable : true
        diploma_marks nullable : true
        erpscholarshiptype nullable: true
        mht_cet_score nullable: true
    }

}
