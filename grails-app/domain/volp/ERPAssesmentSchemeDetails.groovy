package volp

class ERPAssesmentSchemeDetails
{
    double converted_marks
    double evaluated_marks
    double passing_marks
    boolean ispassingmarksapplicable   //true:applicable     false:Not Applicable
    boolean isapplicabletonba   //true:applicable     false:Not Applicable
    boolean iscarryforward   //true:if regualar exam mark used as it is -- false:Need To enter Marks at every Exam
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpassesmentscheme:ERPAssesmentScheme,
                      erpassementtype:ERPAssementType,erpentrytype:ERPEntryType,
                      erpevaluationtype:ERPEvaluationType,erpassesmentschemegroup:ERPAssesmentSchemeGroup]
    static constraints = {
        erpevaluationtype nullable : true
        erpassesmentschemegroup nullable : true
    }
    static mapping = {
        passing_marks defaultValue: 0
        ispassingmarksapplicable defaultValue: false
        isapplicabletonba defaultValue: true
        iscarryforward defaultValue: false
    }
    String toString()
    {
        erpassesmentscheme?.schemename +" : "+erpassementtype?.type +" : "+ erpentrytype?.type +" : "+erpevaluationtype?.type +" (marks :"+ evaluated_marks +")"
    }
}
