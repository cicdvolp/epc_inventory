package volp

class ERPStudentAdmissionMainCategory {

    boolean isoverandabove
    boolean isscholershipsectionapplicable
    boolean isecapplicable
    boolean isactive

    String category
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    String toString() {
        category
    }
    static mapping = {
        isoverandabove defaultValue: false
        isscholershipsectionapplicable defaultValue: false
        isecapplicable defaultValue: false
        isactive defaultValue: false
    }
}