package volp

class EmployeeVaccationMaster {
    int allowedDays

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,employeetype:EmployeeType,
                      organization:Organization,leavetype:LeaveType,
                      vacationtype:VacationType,academicyear:AcademicYear, leavemaster:LeaveMaster]
    static constraints = {
        employeetype nullable: true
        leavemaster nullable: true
    }
}
