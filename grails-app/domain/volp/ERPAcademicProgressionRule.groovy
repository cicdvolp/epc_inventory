package volp

class ERPAcademicProgressionRule {

    double rulesforcurrentyear
    double rulesforpreviousyear
    boolean rulesforauditcourseforpreviousyear
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,programtype:ProgramType,previousyear:Year,currentyear:Year,nextyear:Year]
    static constraints = {
        previousyear nullable:true
        currentyear nullable:true
        nextyear nullable:true
    }
}
