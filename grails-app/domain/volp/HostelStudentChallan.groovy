package volp

class HostelStudentChallan {

    boolean isapproved
    double previouslypaid
    double tobepaid

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostelfeestudentmaster:HostelFeeStudentMaster, learner:Learner,
                      organization:Organization, hostelstudentreceipt:HostelStudentReceipt]

    static constraints = {
    }

    static mapping = {
        previouslypaid defaultValue: 0
        tobepaid defaultValue: 0
        isapproved defaultValue: false
    }
}
