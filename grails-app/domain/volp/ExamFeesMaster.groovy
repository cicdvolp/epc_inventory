package volp

class ExamFeesMaster {

    Date startdate
    Date enddate
    double fees
    double latefees  //perday

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[learnerexamapplicationtype:LearnerExamApplicationType,
                      academicyear:AcademicYear,
                      semester:Semester,
                      organization:Organization]

    static mapping = {
        fees defaultValue: 0
        latefeesperday defaultValue: 0
    }
}
