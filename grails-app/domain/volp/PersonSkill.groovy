package volp

class PersonSkill {
    int selfrating
    int years_of_experience
    int months

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo= [learner:Learner,instructor:Instructor,skill:Skill,organization:Organization]
    static constraints = {
        organization nullable: true
    }
    String toString()
    {
        selfrating
    }
}
