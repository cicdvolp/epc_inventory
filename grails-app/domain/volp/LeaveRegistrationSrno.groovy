package volp

class LeaveRegistrationSrno {

    int leave_registation_srno

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[leavetype:LeaveType,erpcalendaryear:ERPCalendarYear,organization:Organization]

    static constraints = {
    }
}
