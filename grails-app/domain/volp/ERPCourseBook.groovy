package volp

class ERPCourseBook {

    String name   //Name of Book
    int sortorder
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourse:ERPCourse,organization:Organization,erpcoursebooktype:ERPCourseBookType]
    static constraints = {
    }
}
