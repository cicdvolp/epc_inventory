package volp

class ERPReRegistrationCourseTypeOffering {

    double reregfees
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, academicyear:AcademicYear, semester:Semester, erpreregistrationcoursetype:ERPReRegistrationCourseType]

    static constraints = {
    }
    static mapping = {
        reregfees defaultValue:0
    }
}
