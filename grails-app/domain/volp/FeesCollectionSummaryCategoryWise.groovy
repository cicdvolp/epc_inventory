package volp

class FeesCollectionSummaryCategoryWise {
    double totalamount
    int numberofstudents

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, academicyear:AcademicYear,
                       program : Program, year : Year, erpstudentfeescategory : ERPStudentFeesCategory]
}
