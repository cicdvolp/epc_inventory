package volp

class QueryTransaction {
    static belongsTo=[query:Query,instructor:Instructor,querystatus:QueryStatus]

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static constraints = {
    }
}
