package volp

class ERPStudentInstituteFeedback
{
    Date feedback_given_date
    String answer //Only for subjective Questions , erpfeedbackquestionoptions will be null for subjective Questions
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      learner:Learner,
                      erpfeedbackquestionare:ERPFeedbackQuestionare,erpfeedbackquestionoptions:ERPFeedbackQuestionOptions,
                      academicyear:AcademicYear,
                      erpfeedbackcategorytype:ERPFeedbackCategoryType
                   ]
    static constraints =
            {
                erpfeedbackcategorytype nullable:true
                erpfeedbackquestionoptions nullable:true
                answer nullable:true
            }
}
