package volp

class CompOffTransactionApproval {

    String remark
    Date application_push_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,
                      employeeleavemaster:EmployeeLeaveMaster,
                      leaveapprovalstatus:LeaveApprovalStatus,
                      leaveapprovalhierarchy:LeaveApprovalHierarchy,
                      approvedby:Instructor]

    static constraints = {
        approvedby nullable:true
    }
}
