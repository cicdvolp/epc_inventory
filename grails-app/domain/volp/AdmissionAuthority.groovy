package volp

class AdmissionAuthority {

    String name //Student Section / Accounts Section / Scholarship Section
    int level
    boolean islast
    int approvalsequence
    boolean  generategr
    boolean  isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
        generategr defaultValue: false
        isactive defaultValue: true
    }


}
