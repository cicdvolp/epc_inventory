package volp

class ERPCourseOfferingPlan {

    String problem_statement_actually_covered   //only for practical and tutorial
    int lecture_number_planned
    boolean isdone //true :completed false:not Completed
    Date plan_date
    Date execution_date
    String remark

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                        instructor:Instructor,erpcourseoffering:ERPCourseOffering,
                        erpcourse:ERPCourse,
                      erpcourseplan:ERPCoursePlan,academicyear:AcademicYear,semester:Semester,
                      loadtype:LoadTypeCategory,organization:Organization]
    static constraints =
    {
        problem_statement_actually_covered nullable:true
        plan_date nullable:true
        execution_date nullable:true
        remark nullable:true
        isdone nullable:true
        erpcourseplan nullable:true
    }

    static mapping = {
        isdone defaultValue: false
    }

    static hasMany = [erpcoursetopic:ERPCourseTopic]
}
