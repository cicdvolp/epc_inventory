package volp

class SocialWelfareReceiptNumberTracking {
    String year
    String number
    static belongsTo=[organization:Organization]
    static constraints = {
        organization nullable: true
    }

}
