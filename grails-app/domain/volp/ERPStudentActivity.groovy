package volp

class ERPStudentActivity
{
    String name_of_activity
    String role
    String level
    Date from_date
    Date to_date
    String honours
    String remarks
    String location
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,erpstudentactivitytype:ERPStudentActivityType,year:Year,academicyear:AcademicYear]
    static constraints = {
        location nullable: true
        year nullable:true
        academicyear nullable: true
    }
}
