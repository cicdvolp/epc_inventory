package volp

class ERPDocumentApprovalHierarchy {

    int level
    int approval_period_days
    boolean is_last_approval
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isActive
    static belongsTo=[organization:Organization,erpdocumentapprovalauthority:ERPDocumentApprovalAuthority,
                      erpdocumentrequesttype:ERPDocumentRequestType,erpdocumentmaster:ERPDocumentMaster]
    static mapping = {
        is_last_approval defaultValue: false
        isActive defaultValue: true
    }
    static constraints = {
    }
}
