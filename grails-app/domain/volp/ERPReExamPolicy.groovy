package volp

class ERPReExamPolicy {

    double exammarks
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,examconduction:ERPAssementType,
                      erpgradedetails:ERPGradeDetails,scaledown:ERPEvaluationType]
    static hasMany = [retainmentevaluationtype:ERPEvaluationType,retainmentassesmenttype:ERPAssementType]
    static constraints = {
    }
}
