package volp

class ERPExamRoomOfferingBenches {
    int benchnumber
    String benchside   //R/L
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamroomoffering:ERPExamRoomOffering]
    static constraints = {
    }
    String toString()
    {
        erpexamroomoffering.erproom.roomnumber+"-"+benchside+benchnumber
    }
}
