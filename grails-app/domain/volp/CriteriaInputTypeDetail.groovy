package volp

class CriteriaInputTypeDetail {

    String field_name  // Table
    String field_value    //Comma separated for drop down
    int columsequence // Table
    int minwordcount //default 0
    int maxwordcount

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, criterion:Criterion, inputtype : InputType,
                       criterioninputtype:CriterionInputType, criterionfieldtype:CriterionFieldType]

    static constraints = {
    }
}
