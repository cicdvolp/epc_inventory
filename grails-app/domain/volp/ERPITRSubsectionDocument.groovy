package volp

class ERPITRSubsectionDocument {

    String name
    String filename
    String filepath
    int sequence_no

    Date proof_submission_date

    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor, organization:Organization, itrsubsection : ITRSubSection,
                      itrsubsectioninstructor:ITRSubSectionInstructor, itrdocumentapprovalstatus:ITRDocumentApprovalStatus ]

    static mapping = {
        sequence_no defaultValue: 0
    }

    static constraints = {
        remark nullable : true
        proof_submission_date nullable : true
    }
}
