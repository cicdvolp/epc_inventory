package volp

class ERPStudentFeedbackVersion {

    int feedback_number
    boolean isCurrent
    boolean  issecretcoderequired
    Date startdate
    Date enddate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,academicyear:AcademicYear,semester:Semester,program:Program,year:Year]
    static constraints = {
        program nullable: true
        year nullable: true

    }
    static mapping = {
        issecretcoderequired defaultValue: true
    }
}
