package volp

class HostelRoomOffering {

    int capacity
    boolean isreserved
    boolean isorganizationgrouplevelroom
    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostelroom:HostelRoom, organizationgroup:OrganizationGroup,
                      academicyear:AcademicYear, hosteloffering:HostelOffering,
                      hostelcategory:HostelCategory, hostel:Hostel,organization:Organization]

    static constraints = {
        hostelroom unique: 'hosteloffering'
        organization nullable : true
        remark nullable : true
    }

    static mapping = {
        isreserve defaultValue: false
    }
}
