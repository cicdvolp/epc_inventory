package volp

class SubjectGroupMaster {

    String name  // MPSC, MPGS

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,stream:Stream, year : Year, semester : Semester]

//    static hasMany = [erpcourse: ERPCourse]

    static constraints = {
    }
}
