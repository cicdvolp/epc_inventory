package volp

class ERPHostelStudentFeesMaster {
    double paid_till_now
    Date cancellation_date
    String cancellation_remark
    static belongsTo=[organization:Organization,learner:Learner,academicyear:AcademicYear,year:Year,erphostelfeesmaster:ERPHostelFeesMaster]
    static hasMany = [erphostelstudentreceipt:ERPHostelStudentReceipt]
    static constraints = {
        cancellation_date nullable:true
        cancellation_remark nullable:true
        year nullable:true



    }
}
