package volp

class ERPRelativeGradingConfiguration  //this is common configuration for Institute
{
    double value
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpgradedetails:ERPGradeDetails,erpgrademaster:ERPGradeMaster,erpgrademasteroffering:ERPGradeMasterOffering]
    static mapping = {
        value defaultValue: 0.0
    }
}
