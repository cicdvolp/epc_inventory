package volp

class ERPCourseOfferingBatchInstructor {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    int allocatecount   //while entering first time it is 1 then increment by 1 for every subsequent entry
    static belongsTo=[instructor:Instructor,erpcourseoffering:ERPCourseOffering,
                      erpcourseofferingbatch:ERPCourseOfferingBatch]
    static hasMany = [extrainstructor:Instructor]
    static constraints = {
    }
}
