package volp

class AdmissionOTP {
    String username   //DTE Application ID
    String email
    String mobile
    String email_otp
    String mobile_otp

    boolean isemailverified
    boolean ismobileverified

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization]
    static constraints = {
        organization nullable:true
        //access_token nullable:true
    }

    static mapping = {
        isemailverified defaultValue: false
        ismobileverified defaultValue: false
    }
}
