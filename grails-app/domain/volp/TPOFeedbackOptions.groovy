package volp

class TPOFeedbackOptions {

    int opno
    String option_statement
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,tpo:TPO,tpofeedbacktype:TPOFeedbackType,tpofeedbackquestions:TPOFeedbackQuestions]
    static constraints = {
    }
}
