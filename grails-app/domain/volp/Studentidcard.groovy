package volp

class Studentidcard {

    String icardbarcodeFile
    Date validupto
    String icardPath
    String icardPhotoFile
    String icardSignatureFile
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner: Learner,organization:Organization]
    static constraints = {
        icardSignatureFile nullable:true
        icardbarcodeFile nullable:true
    }
}
