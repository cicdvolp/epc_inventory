package volp

class LeaveLoadAdjustment {

    Date leave_date
    String start_time
    String end_time
    String load_description
    String remark
    boolean isloadadjusted
    boolean isloadrejected
    boolean iscustomload
    boolean isdeleted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor, employeeleavetransaction:EmployeeLeaveTransaction,
                      organization:Organization, rejectedby : Instructor]

    static constraints = {
        rejectedby nullable :true
        remark nullable :true
    }

    static mapping = {
        isloadrejected defaultValue: false
        iscustomload defaultValue: false
        isloadadjusted defaultValue: false
        isdeleted defaultValue: false
    }
}
