package volp

class ERPMCQOptions {

    String opno
    String option_statement
    String option_file_path
    String option_file_name
    boolean iscorrecetoption
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpmcquestionbank:ERPMCQQuestionBank]
    static mapping = {
        iscorrecetoption defaultValue: false
    }
    static constraints = {
        option_statement nullable: true
        option_file_path nullable: true
        option_file_name nullable: true
    }
}
