package volp

class ERPCommonReceiptTransaction {

    String tid //chegueno//dd
    Date date

    double amount

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erppaymentmode:ERPPaymentMode,erpcommonreceipt:ERPCommonReceipt, bank_name:ERPBankName]

    static constraints = {
        tid nullable : true
        bank_name nullable : true
        erpcommonreceipt nullable : true
    }
}
