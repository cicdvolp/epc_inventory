package volp

class ERPCourseResultReserve
{
    boolean isresearved   //true->reserved   false->Not reserved
    String remark
    boolean isdeleted    //true:deleted
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpcourseresultreservereasonmaster:ERPCourseResultReserveReasonMaster,erpcourseoffering:ERPCourseOffering,erpcourseofferinglearner:ERPCourseOfferingLearner,learner:Learner,academicyear:AcademicYear,semester:Semester]
    static constraints = {
    }
    static mapping = {
        isresearved defaultValue: false
        isdeleted defaultValue: false
    }
}
