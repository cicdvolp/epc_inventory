package volp

class ERPSelfAppraisalThresholdMarks {

    double threshold
    boolean iselfapraisalprocesscompleted    //true means completed and false means Not cpmpleted
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static mapping = {
        iselfapraisalprocesscompleted defaultValue: false
    }
    static constraints = {
    }
}
