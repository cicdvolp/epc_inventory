package volp

class TPOCompanyOfferingLearner { //student registration to company

    Date registration_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,tpocompanyoffering:TPOCompanyOffering,tpo:TPO,tpocompany:TPOCompany]
    static constraints = {
    }
}
