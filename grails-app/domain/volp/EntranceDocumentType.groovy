package volp

class EntranceDocumentType {

    String name             //Cast Certificate, UG Degree Certificate etc.
    boolean iscompulsory
    boolean isactive
    String size
    String extension        //comma separated
    String info
    String resolution

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization : Organization]

    static constraints = {
        size nullable: true
        extension nullable: true
        info nullable: true
        organization nullable: true

    }
    static mapping = {
        isactive defaultValue: true
        iscompulsory defaultValue: true
    }
    String toString() {
        name
    }
}
