package volp

class Instructor {
    String uid     //This should be simmilar to login.username
    String official_email_id//for payroll
    String pfno//payroll
    String uan_no//payroll
    String lastpfno//payroll
    String gratuatynumber//payroll
    String reasonfornotactive//payroll
    String bankaccountnumber//payroll

    String ifsccode//payroll
    String micrcode//payroll
    Date consolidateddate//payroll
    Date scaledate//payroll
	String employee_code
    String employeeabbr
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address	
    double rating
    Date dateofjoining
    Date dateofrejoining
    Date dateofreleving
    String external_organization_name
    double current_basic_salary
    boolean iscurrentlyworking
    boolean ismanagementroleapplicable
    boolean is_eligible_for_increment//payroll
    boolean is_handicap//payroll
    boolean is_house_provided//payroll
    boolean is_car_provided//payroll
    String mobile_no   //primary mobile no
    String subject_expertise
    String hindex
    String aditionalinfo1
    String payscale

    double estimatedsalary

    int displayorder

    boolean iseligibleforbypasslogin
    boolean isloadadustmentrequired
    boolean is_salary_blocked
    boolean is_salary_sleep_blocked
    static belongsTo=[gender:Gender,erpexaminertype:ERPExaminerType,
                      department:Department,employeetype:EmployeeType,
                      erpservicetype:ERPServiceType,person:Person,
                      organization:Organization,designation:Designation,
                      program:Program,reportingdepartment:Department,
                      reportingorganization:Organization,reporttingauthority:Instructor,reporttingdean:Instructor,
                      currentshift : ERPShift, inoutweeklyplanmaster:InoutWeeklyPlanMaster,
                      erpnationality:ERPCountry,category: ERPStudentAdmissionMainCategory,payrollemployeetype:PayrollEmployeeType,payrollemployeecategory:PayrollEmployeeCategory,
                      payrolldesignation:PayrollDesignation,payrolldepartment:PayrollDepartment,bankorganization:BankOrganization
    ]
    static hasMany = [erpfacultypost:ERPFacultyPost, entranceauthoritytype:EntranceAuthorityType]
    String toString()
    {
        employee_code+":"+person.fullname_as_per_previous_marksheet
    }
    def toJson(){
        HashMap hm = new HashMap()
        hm.put("code",employee_code)
        hm.put("email",uid)
        return hm
    }
    static constraints = {
        uan_no nullable: true
        scaledate nullable: true
        consolidateddate nullable: true
        micrcode nullable: true
        ifsccode nullable: true
        bankaccountnumber nullable: true
        reasonfornotactive nullable: true
        gratuatynumber nullable: true
        lastpfno nullable: true
        pfno nullable: true
        mobile_no nullable: true
        employee_code nullable: true
        employeetype nullable: true
        rating defaultValue: 0
        organization nullable: true
        designation nullable: true
        program nullable: true
        reporttingdean nullable: true
        dateofjoining nullable: true
        dateofrejoining nullable: true
        dateofreleving nullable: true
        department nullable: true
        program nullable: true
        reportingdepartment nullable: true
        reportingorganization nullable: true
        reporttingauthority nullable: true
        erpexaminertype nullable: true
        uid nullable: true
        external_organization_name nullable: true
        erpfacultypost nullable: true
        erpservicetype nullable: true
        gender nullable: true
        employeeabbr nullable: true
        inoutweeklyplanmaster nullable: true
        currentshift nullable: true
        subject_expertise nullable: true
        official_email_id nullable: true
        hindex nullable: true
        erpnationality nullable: true
        category nullable: true
        current_basic_salary defaultValue: 0
        payrollemployeetype nullable: true//payroll
        payrollemployeecategory nullable: true//payroll
        payrolldesignation nullable: true//payroll
        payrolldepartment nullable: true//payroll
        payscale nullable: true//payroll
        bankorganization nullable: true//payroll
        aditionalinfo1 nullable: true//payroll
    }
    static mapping = {
        ismanagementroleapplicable defaultValue: false
        estimatedsalary defaultValue: 0
        iscurrentlyworking defaultValue: true
        iseligibleforbypasslogin defaultValue: false
        is_eligible_for_increment defaultValue: true
        is_handicap defaultValue: false
        is_house_provided defaultValue: false
        is_car_provided defaultValue: false
        isloadadustmentrequired  defaultValue: false
        is_salary_blocked defaultValue: false
        is_salary_sleep_blocked  defaultValue: false
    }
}
