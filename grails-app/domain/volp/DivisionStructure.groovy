package volp

class DivisionStructure
{
    int count  //strengh of quota of division..used in special case like first year
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isForeignNational
    static belongsTo=[divisionoffering:DivisionOffering,crslanguage:ERPCourseOffering,
                      erpadmissionquota:ERPAdmissionQuota,
                      erpprogramgroup:ERPProgramGroup,erpshift:ERPShift,
                      specificprogram:Program,organization:Organization,academicyear:AcademicYear,
                      semester:Semester]
    static hasMany = [erpcourseoffering:ERPCourseOffering]
    static constraints = {
        erpadmissionquota nullable:true
        erpprogramgroup nullable:true
        erpshift nullable:true
        specificprogram nullable:true
        crslanguage nullable:true
        academicyear nullable:true
        semester nullable:true

    }
    static mapping = {
        isForeignNational defaultValue: false
        count defaultValue: 0
    }
}
