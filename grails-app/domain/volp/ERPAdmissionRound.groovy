package volp

class ERPAdmissionRound {

    String name
    boolean isfeestypeapplicabletoround

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[admissionroundtype:AdmissionRoundType, organization:Organization , erpstudentfeescategory:ERPStudentFeesCategory]

    static constraints = {
        erpstudentfeescategory nullable : true
    }
    static mapping = {
        isfeestypeapplicabletoround defaultValue: false
    }
    String toString(){
        name
    }
}
