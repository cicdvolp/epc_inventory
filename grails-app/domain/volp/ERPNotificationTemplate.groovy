package volp

class ERPNotificationTemplate {

    String templatename
    String subject
    String body
    boolean isActive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,roletype:RoleType,erpcommunicationmode:ERPCommunicationMode]
    static constraints = {
    }
}
