package volp

class ERPCOAttainment
{
    boolean isAttained   //true->attained      false->not attained
    double attained_value     //attained percentages (n/N)
    double threshold

    double evaluated_weightage             //(ew)
    double mapped_weightage                //(mw)
    int no_of_student_above_threshould     //(n)
    int total_students                     //(N)

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpcourseoffering:ERPCourseOffering,erpco:ERPCO,organization:Organization, academicyear:AcademicYear, semester:Semester]

    static constraints = {
        academicyear nullable:true
        semester nullable:true
    }
}
