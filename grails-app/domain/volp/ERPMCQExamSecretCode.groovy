package volp

class ERPMCQExamSecretCode {

    double obtained_score
    String secret_code
    boolean isexamgiven
    Date examgivendate
    Date start_time
    Date end_time
    int extra_time

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,recapplicant:RecApplicant,recapplication:RecApplication,
                      recversion:RecVersion,recdeptgroup:RecDeptGroup]
    static mapping = {
        isexamgiven defaultValue: false
    }
    static constraints = {
        recdeptgroup nullable:true
        start_time nullable:true
        end_time nullable:true
        extra_time nullable:true
        examgivendate nullable:true
    }
}
