package volp

class ProgramType {

    String name    //UG/PG/PhD
    String displayname
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]

    static constraints = {
        displayname nullable:true
        organization nullable: true

    }
}
