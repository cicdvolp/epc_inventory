package volp

class ERPNotificationEvent {

    String eventname     //ex.notify_student_about_company_scheduling
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,applicationtype:ApplicationType]
    static constraints = {
    }
}
