package volp

class Degree {

    String name    //SSC,HSC,Diploma, Graduation, Post Graduation, PhD
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }

}
