package volp

class ERPCOPOMapping {
    double value
    boolean isfreezed
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpco:ERPCO,erppo:ERPPO]
    static constraints = {
        isfreezed defaultValue: false
    }
}
