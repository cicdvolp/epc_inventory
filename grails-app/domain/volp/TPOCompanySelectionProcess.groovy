package volp

class TPOCompanySelectionProcess {
    int selection_process_number
    boolean  selection_process_freezed
    static belongsTo=[tPOCompanyOffering :TPOCompanyOffering,tPOSelectionProcess:TPOSelectionProcess]

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String toString()
    {
        "Round " + selection_process_number + " : " + tPOCompanyOffering.tPOCompany.name +" : " +  tPOCompanyOffering.academicYear.ay + " : " + tPOSelectionProcess.name
    }

    static mapping = {      
        selection_process_freezed defaultValue: false
    }
    static constraints = {
    }
}
