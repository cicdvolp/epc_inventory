package volp

class ERPNotificationHistory {

    String subject
    String body
    String receiver
    Date sentdate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,learner:Learner,instructor:Instructor,applicationtype:ApplicationType,erpnotificationsendingtechnique:ERPNotificationSendingTechnique,erpnotificationsender:ERPNotificationSender,erpnotificationevent:ERPNotificationEvent,erpnotificationtemplate:ERPNotificationTemplate]
    static hasMany = [erpnotificationattachment:ERPNotificationAttachment]
    static constraints = {
        learner nullable:true
        instructor nullable:true
    }
}
