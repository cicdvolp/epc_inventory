package volp

class MobileBiometricRegistration {

    String mobile_no
    String mobile_imei_no
    boolean isdeleted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, instructor:Instructor, mobilebiometriclocationmaster : MobileBiometricLocationMaster]

    static constraints = {
        mobile_imei_no(unique: ['instructor', 'mobilebiometriclocationmaster'])
        mobile_no nullable: true
        mobilebiometriclocationmaster nullable: true
    }

    static mapping = {
        isdeleted defaultValue: false
    }
}
