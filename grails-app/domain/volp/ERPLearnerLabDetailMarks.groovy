package volp

class ERPLearnerLabDetailMarks
{
    double obtainedmarks
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[academicyear:AcademicYear,semester:Semester,learner:Learner,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseofferingbatchlearner:ERPCourseOfferingBatchLearner,erpcourseplanproblemstatementallocation:ERPCoursePlanProblemStatementAllocation,
                      loadtype:LoadTypeCategory,erplabassesmentschemedetails:ERPLabAssesmentSchemeDetails,
                      erplabassesmentscheme:ERPLabAssesmentScheme,organization:Organization]
    static constraints = {
    }
}
