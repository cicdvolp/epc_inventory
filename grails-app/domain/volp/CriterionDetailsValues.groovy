package volp

class CriterionDetailsValues {

    int sequence // Row Sequence
    String strvalue
    boolean isapproved
    boolean submitted
    Date approvaldate
    Date entrydate


    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, criterionoffering:CriterionOffering,
                       criteriainputtypedetailsoffering:CriteriaInputTypeDetailsOffering,
                       approvedby : Instructor  , dataentryby : Instructor]

    static constraints = {
        approvaldate nullable : true
        approvedby nullable : true
    }

    static mapping = {
        isapproved defaultValue: false
        submitted defaultValue: false
    }
}
