package volp

class ERPBacklogCap {
    String grno
    String full_name
    String year
    String branch
    String module
    String division
    String rollno
    String sub_code
    String subject_name
    Date date
    String time
    String room_no
    String cap_code


    static constraints = {
        date nullable: true
        time nullable: true
    }
}