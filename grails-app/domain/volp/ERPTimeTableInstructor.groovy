package volp

class ERPTimeTableInstructor {
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,instructor:Instructor,department:Department]
    static constraints = {
    }
    String toString(){
        instructor.employee_code+":" + instructor.employeeabbr
    }
}
