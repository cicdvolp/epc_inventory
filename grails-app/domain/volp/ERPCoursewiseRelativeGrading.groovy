package volp

class ERPCoursewiseRelativeGrading {

    //this domain is for course... relative grading will vary according to course
    double min   //minimum range  after applying formula
    double max   //maximum range after applying formula
    int total_students  //count
    double range_percentage   //copy from ERP relative grading configuration, and allowed to change
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpcourseoffering:ERPCourseOffering,
                      erpgradedetails:ERPGradeDetails]
    static constraints = {
    }
}
