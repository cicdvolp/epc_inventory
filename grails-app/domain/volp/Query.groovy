package volp

class Query {
    String firstName
    String lastName
    String description
    String mobile_no//optional
    String email
    String file_name
    String file_path
    String title

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[usertype:UserType,querytype:QueryType,querycategory:QueryCategory,login:Login,currentquerystatus:QueryStatus,currentinstructor:Instructor]

    static constraints = {
        mobile_no nullable:true
        file_name nullable:true
        file_path nullable:true
        login nullable:true

    }
}
