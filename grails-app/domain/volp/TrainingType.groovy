package volp

class TrainingType {

    String type //industry, workshop, seminar, STTP, conference
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
    static belongsTo=[organization:Organization]

}
