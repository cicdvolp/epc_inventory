package volp

class InvAdvanceRequestReceipt {

    int receipt_no
    Date receipt_date
    double amount_transferred
    String account_number
    String bank_name
    String bank_branch
    String ifsc_code
    String transaction_number
    boolean is_advance_closed
    boolean isactive
    String return_details
    double amount_returned

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization              : Organization,
                      invadvancerequest         : InvAdvanceRequest,
                      invpaymentmethod          : InvPaymentMethod,
                      paymentby                 : Instructor,
                      returnpaymentmethod          : InvPaymentMethod]

    static constraints = {
        account_number nullable:true
        bank_name nullable:true
        bank_branch nullable:true
        ifsc_code nullable:true
        transaction_number nullable:true
        return_details nullable:true
        returnpaymentmethod nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        is_advance_closed defaultValue: false
        amount_returned defaultValue: 0
    }
}
