package volp

class EmployeeSalaryRegisterPartialDetails {
    double value
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[salarymonth:SalaryMonth,instructor:Instructor, salarycomponent:SalaryComponent, employeesalaryregisterpartial:EmployeeSalaryRegisterPartial, employeesalarymaster:EmployeeSalaryMaster ,organization:Organization]
    static constraints = {
    }
}
