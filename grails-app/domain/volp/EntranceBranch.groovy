package volp

class EntranceBranch {

    String name
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization      : Organization,
                      entranceversion   : EntranceVersion,
                      program           : Program]

    static constraints = { }

    static mapping = {
        isactive defaultValue: true
    }

}
