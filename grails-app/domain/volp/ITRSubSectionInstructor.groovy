package volp

class ITRSubSectionInstructor {

    Date lastupdateddate
    double amount
    double proof_amount
    double approved_amount

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[itroption:ITROption,itrsection:ITRSection, finincialyear:FinincialYear, instructor:Instructor, organization:Organization, itrsubsection : ITRSubSection ]

    static mapping = {
        amount defaultValue: 0
        proof_amount defaultValue: 0
        approved_amount defaultValue: 0
    }

    static constraints = {
    }
}
