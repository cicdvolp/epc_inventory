package volp

class ERPGradingPolicyThreshold {

    int appearedstudentcountforcourse ////Threshold for Applying Absolute Grading, generally it is 25
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpgradingpolicy:ERPGradingPolicy,academicyear:AcademicYear,semester:Semester]
    static constraints = {
        semester nullable:true
    }
}
