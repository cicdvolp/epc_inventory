package volp

class EntranceFees {

    double fees

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      exemption             : Exemption,
                      programtype           : ProgramType,
                      entrancecategorytype  : EntranceCategoryType,
                      entranceversion       : EntranceVersion]

    static constraints = {
        exemption nullable:true
    }
}
