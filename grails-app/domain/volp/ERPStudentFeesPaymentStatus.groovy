package volp

class ERPStudentFeesPaymentStatus {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[learner:Learner,year:Year,erpfeesdepositetype:ERPFeesDepositeType,erpadmissionstatus:ERPAdmissionStatus]
    static constraints = {
    }
}
