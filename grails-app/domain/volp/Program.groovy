package volp

class Program {

    String name
    String abbrivation
    boolean isdeleted
    boolean isfeesprogramwise
    String examdisplayname
    boolean isapplicabletoadmission

    String displayname       //used in passing certificate(VIT)

	static belongsTo=[department:Department,
                      organization:Organization,
                      programtype:ProgramType,
                      erpprogramgroup:ERPProgramGroup]  //Desh/Computer
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String toString()
    {
        name
    }
    static mapping = {
        isapplicabletoadmission defaultValue: true
    }
    static constraints = {
        displayname nullable: true
        erpprogramgroup nullable: true
        organization nullable: true
        abbrivation nullable:true
        examdisplayname nullable:true
    }
}
