package volp

class LeaveAutomaticApprovalHistory
{
    Date approvaldate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[academicyear:AcademicYear,organization:Organization,employeeleavetransactionapproval:EmployeeLeaveTransactionApproval]
    static constraints = {
    }
}
