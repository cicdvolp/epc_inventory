package volp

class ERPAssesmentScheme {
    String schemename // user defined name for a scheme

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]
    String toString(){
        schemename
    }
}
