package volp

class Facultyidcard {
    String icardbarcodeFile
    Date validupto
    String icardPath
    String icardPhotoFile
    String icardSignatureFile

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,organization:Organization]
    static constraints = {
        icardSignatureFile nullable:true
        icardbarcodeFile nullable:true
        validupto nullable:true
        icardPhotoFile nullable:true
        icardbarcodeFile nullable:true
        icardPath nullable:true
    }
}
