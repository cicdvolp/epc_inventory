package volp

class ERPFacultySelfAppraisalAuthorityMarks {

    double marks
    boolean isapproved
    boolean issaved
    String remark
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      self:Instructor,authinstrutor:Instructor,
                      erpselfappraisalauthoritytype:ERPSelfAppraisalAuthorityType,
                      erpselfappraisalsubparameter:ERPSelfAppraisalSubParameter,
                      academicyear:AcademicYear,
                      erpselfappraisalparameterauthority:ERPSelfAppraisalParameterAuthority,
                      erpfacultyposttype:ERPFacultyPostType,erpfacultypost:ERPFacultyPost,
                      erpselfappraisalauthoritymaster:ERPSelfAppraisalAuthorityMaster,
                      erpselfappraisalparameter:ERPSelfAppraisalParameter]
    static constraints = {
        remark nullable:true
        authinstrutor nullable:true
        self nullable:true
        erpfacultyposttype nullable:true
        erpfacultypost nullable:true
        erpselfappraisalauthoritymaster nullable:true
        erpselfappraisalauthoritytype nullable:true
        erpselfappraisalparameter nullable:true
        issaved nullable:true
    }
    static mapping = {
        marks defaultValue: 0
        isapproved defaultValue: false
        issaved defaultValue: false
    }
}
