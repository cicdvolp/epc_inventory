package volp

class InvFinanceApprovingAuthorityLevel {

    int level_no
    boolean islast
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization                  : Organization,
                      invfinanceapprovingauthority  : InvFinanceApprovingAuthority,
                      invfinanceapprovingcategory   : InvFinanceApprovingCategory]

    static constraints = {
        isactive defaultvalue:true
        islast defaultvalue:false
        level_no defaultvalue:0
    }
}
