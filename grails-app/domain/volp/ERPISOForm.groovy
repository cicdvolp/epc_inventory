package volp

class ERPISOForm {
    int revisionnumber
    String ffno
    String description
    String filepath
    String filename
    boolean isactive
    Date effectivefromdate

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=
            [organization:Organization,erpisocategory:ERPISOCategory]
    String toString()
    {
        ffno
    }

    static constraints = {
        ffno  nullable: false
        description  nullable: false
        revisionnumber  nullable: false
        filepath  nullable: false
        filename  nullable: false
    }
}
