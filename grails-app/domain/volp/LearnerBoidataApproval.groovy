package volp

class LearnerBoidataApproval {

    String filename
    String filepath
    boolean isapproved
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,organization:Organization,tpo:TPO]
    static constraints = {
        learner nullable: true
        organization nullable: true
        tpo nullable: true
    }
}
