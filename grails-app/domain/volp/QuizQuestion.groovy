package volp

class QuizQuestion {

    int qno
    int unitnumber
    String question
    boolean isactive
    boolean isapproved
    boolean isdeleted
    double weightage

    String question_attachment_file_name
    String question_attachment_file_path

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, quizmaster :QuizMaster,
                       quizquestiontype:QuizQuestionType, quizquestiondifficultylevel:QuizQuestionDifficultyLevel,
                       quizquestionbank : QuizQuestionBank, erpco:ERPCO]

    static constraints = {
        question_attachment_file_name nullable:true
        question_attachment_file_path nullable:true
        quizquestionbank nullable:true
        erpco nullable:true
    }

    static mapping = {
        isdeleted defaultValue: false
    }
}
