package volp

class Learner {
    String uid     //This should be similar to login.username   ..it can be gr number for ERP
	String registration_number
	String graduation_status   //passout remark
    boolean ispassout   //false:Not passed out   //true:Passed out
    String dte_application_number
    String admission_form_no
    Date admission_date
    String merit_no
    String mht_cet_score
    int institute_preference_number
    double family_income
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isadmissioncancelled  //false:Not cancelled   //True:cancelled
    boolean isicardissued   //false:Not issued   //True:issued
    int result_blocked_flag
    int rollno
    String mobileno

    boolean isemailverified
    boolean ismobileverfied

    static belongsTo=[gender:Gender,erpuniversity:ERPUniversityList,admissionyear:AcademicYear,
                      passoutyear:AcademicYear,
                      readmissionyear:AcademicYear,erpseattype:ERPSeatType,person:Person,
                      organization:Organization,current_module:Module,current_year:Year,
                      program:Program,erpschoolboard:ERPSchoolBoard,
                      erpspecialization:ERPSpecialization,erpadmissionquota:ERPAdmissionQuota,
                      erpadmissiontype:ERPAdmissionType,erpshift:ERPShift,
                      erpadmissionround:ERPAdmissionRound,
                      allotederpstudentadmissionmaincategory:ERPStudentAdmissionMainCategory,
                      allotederpstudentadmissionsubcategory:ERPStudentAdmissionSubCategory,
                      erpdomacile:ERPDomacile,
                      admittederpstudentadmissionmaincategory:ERPStudentAdmissionMainCategory,
                      admittederpstudentadmissionsubcategory:ERPStudentAdmissionSubCategory,
                      erpnationality:ERPCountry,erpreligion:ERPReligion,erpcast:ERPCast,
                      erpsubcast:ERPSubCast,erpmaritalstatus:ERPMaritalStatus,erpareatype:ERPAreaType,
                      resultreservemaster:ResultReserveMaster,erpscholarshiptype:ERPScholarshipType,
                      erpvocationalsubjectmaster:ERPVocationalSubjectMaster]
    static mapping = {
        family_income defaultValue: 0
        institute_preference_number defaultValue: 0
        result_blocked_flag defaultValue: 0
        isadmissioncancelled defaultValue:false
        isicardissued defaultValue:false
        rollno defaultValue: 0
        ispassout defaultValue:false
        //isemailverified defaultValue:false
        //ismobileverfied defaultValue:false
    }

    String toString()
    {
        person.fullname_as_per_previous_marksheet
    }
    static constraints = {
        erpvocationalsubjectmaster nullable: true
        //isemailverified nullable: true
        //ismobileverfied nullable: true
        readmissionyear nullable: true
        erpuniversity nullable: true
        admissionyear nullable: true
        passoutyear nullable: true
        erpseattype nullable: true
        merit_no nullable: true
        mht_cet_score nullable: true
        registration_number nullable: true
        graduation_status nullable: true
        organization nullable: true
        current_module nullable: true
        program nullable: true
        current_year nullable: true
        erpschoolboard nullable: true
        erpspecialization nullable: true
        erpadmissionquota nullable: true
        erpadmissiontype nullable: true
        erpshift nullable: true
        erpadmissionround nullable: true
        allotederpstudentadmissionmaincategory nullable: true //it is used for Admission
        allotederpstudentadmissionsubcategory nullable: true  //it is used for Admission
        erpdomacile nullable: true
        admittederpstudentadmissionmaincategory nullable: true  //it is used for Accounts
        admittederpstudentadmissionsubcategory nullable: true  //it is used for Accounts
        erpnationality nullable: true
        erpreligion nullable: true
        erpcast nullable: true
        erpsubcast nullable: true
        erpmaritalstatus nullable: true
        erpareatype nullable: true
        dte_application_number nullable: true
        admission_date nullable: true
        admission_form_no nullable: true
        username nullable: true
        creation_date nullable: true
        updation_date nullable: true
        creation_ip_address nullable: true
        updation_ip_address nullable: true
        gender nullable: true
        resultreservemaster nullable: true
        erpscholarshiptype nullable: true
        mobileno nullable: true
    }
}
