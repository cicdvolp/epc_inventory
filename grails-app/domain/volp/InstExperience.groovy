package volp

class  InstExperience {
    String organizationname
    int no_of_years
    int no_of_months
    boolean iscurrentjob
    String filename
    String filepath
    String approval_letter_file_name
    String approval_letter_file_path
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization, experiencetype:RecExperienceType,instructor:Instructor]
    static constraints = {
        filename nullable : true
        filepath nullable : true
        approval_letter_file_name nullable : true
        approval_letter_file_path nullable : true
    }
    static mapping = {
        iscurrentjob defaultValue: false
    }
}
