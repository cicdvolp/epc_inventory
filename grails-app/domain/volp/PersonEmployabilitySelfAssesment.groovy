package volp

class PersonEmployabilitySelfAssesment {

    double selfrating      // 1 to 10
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[tposelectionprocess:TPOSelectionProcess,learner:Learner,instructor:Instructor,organization:Organization]
    static constraints = {
        learner nullable: true
        instructor nullable: true
    }
}
