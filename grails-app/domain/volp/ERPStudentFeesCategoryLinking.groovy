package volp

class ERPStudentFeesCategoryLinking {

    boolean iscurrent   //which linking is active

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    //learner.admissionyear->master is linked to admission year
    static belongsTo=[learner:Learner,erpstudentfeescategory:ERPStudentFeesCategory,erpscholarshiptype:ERPScholarshipType,
                      academicyear:AcademicYear,year:Year,organization:Organization]
    static constraints = {
        organization nullable:true
        erpscholarshiptype nullable:true
        username nullable:true
        creation_date nullable:true
        updation_date nullable:true
        creation_ip_address nullable:true
        updation_ip_address nullable:true
    }
}
