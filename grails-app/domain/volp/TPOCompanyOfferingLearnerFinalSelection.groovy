package volp

class TPOCompanyOfferingLearnerFinalSelection {

    double packageamount
    String location
    Date joiningdate
    Date selectiondate
    boolean isacceptedbylearner
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[tpocompanyoffering:TPOCompanyOffering,learner:Learner,tpo:TPO,tpocompany:TPOCompany]
    static constraints = {
        location nullable:true
        joiningdate nullable:true
        selectiondate nullable:true
    }
    static mapping = {
        packageamount defaultValue: 0
        isacceptedbylearner defaultValue: false
    }
}
