package volp

class TPOCompanyOffering {
    String minCPI
    String maxCPI
    String eligiblityCriteria
    String salaryPackage
    String skillSet
    String contactemail
    String contactphone
    int visitnumber
    double minpackage  //lakhs
    double maxpackage  //lakhs
    String jobdescription
    static belongsTo=[tpo:TPO,tpoplacementtype:TPOPlacementType,academicYear:AcademicYear,tPOCompany:TPOCompany]
    static hasMany = [organization:Organization,skill:Skill,location:ERPCity,program:Program,tPOJobLocation:TPOJobLocation,tPOCompanySpecialization:TPOCompanySpecialization]

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address


    String toString()
    {
        tPOCompany.name +" : " +  academicYear
    }

    static constraints = {
        skillSet defaultValue : null
        tpo nullable: true
        tpoplacementtype nullable: true
        contactemail nullable: true
        contactphone nullable: true
        jobdescription nullable: true
        eligiblityCriteria nullable: true
        minCPI nullable: true
        maxCPI nullable: true
        salaryPackage nullable: true
    }
    static mapping = {
        visitnumber defaultValue: 0
        minpackage defaultValue: 0
        maxpackage defaultValue: 0
    }
}
