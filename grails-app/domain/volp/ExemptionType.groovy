package volp

class ExemptionType {

    String name             //GATE/Approved Teacher/Senior Citizen/Industry Experience
    boolean isactive

    String abbr                     // used for uplode document

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization  : Organization,
                      programtype   : ProgramType]

    static constraints = {}

    static mapping = {
        isactive defaultValue: true
    }
}
