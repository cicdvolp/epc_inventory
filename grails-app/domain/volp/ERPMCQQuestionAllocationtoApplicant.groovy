package volp

class ERPMCQQuestionAllocationtoApplicant {

    String answeripaddress
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,recapplicant:RecApplicant,recapplication:RecApplication,recdeptgroup:RecDeptGroup,
                      reccourse:RecCourse,erpmcqquestionbank:ERPMCQQuestionBank,studentselectedoption:ERPMCQOptions,recversion:RecVersion]
    static constraints = {
        answeripaddress nullable:true
        studentselectedoption nullable:true
    }
}
