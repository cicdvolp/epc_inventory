package volp

class ERPStudentWorkExperience {

    String external_guide
    String internal_guide
    String experience_letter_file_name
    String experience_letter_file_path
    String organization_name
    String organization_address
    String designation
    String brief_discription_about_nature_of_work
    int experience_in_years
    int experience_in_months
    Date from_date
    Date to_date
    boolean isCurrentJob
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,organization:Organization,erpstudentworkexperiencetype:ERPStudentWorkExperienceType]
    static constraints = {
        organization nullable:true
        erpstudentworkexperiencetype nullable:true
        external_guide nullable:true
        internal_guide nullable:true
        experience_letter_file_name nullable:true
        experience_letter_file_path nullable:true
    }
    static mapping = {
        isCurrentJob defaultValue: false
    }
}
