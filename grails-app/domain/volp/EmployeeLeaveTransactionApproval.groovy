package volp

class EmployeeLeaveTransactionApproval {
    String remark
    Date application_push_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,
                      employeeleavetransaction:EmployeeLeaveTransaction,
                      leaveapprovalstatus:LeaveApprovalStatus,
                      leaveapprovalhierarchy:LeaveApprovalHierarchy,
                      approvedby:Instructor]
    static constraints = {
        approvedby nullable:true
    }
}
