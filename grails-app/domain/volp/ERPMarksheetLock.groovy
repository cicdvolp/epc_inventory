package volp

class ERPMarksheetLock {

    int spicalculationsequence  // 201111,  201112 , 201113
    boolean isfirst
    boolean islocked

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, academicyear :AcademicYear, semester:Semester,
                       year : Year, program : Program, erpmarksheettype : ERPMarksheetType]


    static mapping = {
        isfirst defaultValue: false
        islocked defaultValue: false
    }
}
