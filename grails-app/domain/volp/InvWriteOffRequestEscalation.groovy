package volp

class InvWriteOffRequestEscalation {

    Date action_date
    String remark

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization                      : Organization,
                      invwriteoffrequest                : InvWriteOffRequest,
                      actionby                          : Instructor,
                      invfinanceapprovingauthoritylevel : InvFinanceApprovingAuthorityLevel,
                      invapprovalstatus                 : InvApprovalStatus]

    static constraints = {
        remark nullable:true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
