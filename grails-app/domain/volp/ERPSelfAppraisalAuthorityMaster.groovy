package volp

class ERPSelfAppraisalAuthorityMaster {

    boolean isActive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpselfappraisalauthoritytype:ERPSelfAppraisalAuthorityType,
                      erpfacultypost:ERPFacultyPost]
    static mapping = {
        isActive defaultValue: true
    }
    static constraints = {
    }
}
