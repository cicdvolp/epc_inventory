package volp

class ERPCO {

    int co_code
    String co_statement
    String codifficultylevel_remark
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,erpcodifficultylevel:ERPCODifficultyLevel,
                      erpcourseoffering:ERPCourseOffering, academicyear:AcademicYear, erpcourse:ERPCourse]

    static constraints = {
        erpcodifficultylevel nullable:true
        co_statement blank: false
        erpcourseoffering nullable:true
        academicyear nullable:true
        erpcourse nullable:true
        codifficultylevel_remark nullable:true
    }
    String toString()
    {
        co_code
    }
    static mapping = {
        isactive defaultValue: true
    }
}
