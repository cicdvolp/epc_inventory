package volp

class ERPDocumentRequestTransactionApproval {

    String remark
    Date application_push_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpdocumentrequesttransaction:ERPDocumentRequestTransaction,
                      erpdocumentstatus:ERPDocumentStatus,erpdocumentapprovalhierarchy:ERPDocumentApprovalHierarchy,
                      approvedby:Instructor]
    static constraints = {
        remark nullable:true
        approvedby nullable:true
    }
}
