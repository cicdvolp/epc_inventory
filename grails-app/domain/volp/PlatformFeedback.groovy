package volp

class PlatformFeedback {
    int rating // out of 10
    String description
    static belongsTo = [login: Login]
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
}
