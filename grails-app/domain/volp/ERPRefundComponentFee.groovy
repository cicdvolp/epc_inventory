package volp

class ERPRefundComponentFee {
    double amount
    static belongsTo=[erprefundreceipt:ERPRefundReceipt,erpfeescomponent:ERPFeesComponent]
    static constraints = {
    }
    String toString()
    {
        amount
    }
}
