package volp

class StudentGuardianMeetingDetails {

    Date conductiondate
    String conductiontime
    String meetinglink
    String mom
    boolean studentacknoledgement

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[guardian:Instructor,guardianmeetingschedulemaster:GuardianMeetingScheduleMaster,
                      organization:Organization, program:Program,year:Year,
                      academicyear:AcademicYear,semester: Semester, learner:Learner]

    static constraints = {
    }

    static mapping = {
        studentacknoledgement defaultValue: false
    }
}
