package volp

class InvQuotationDetails {

    int quantity
    double cost_per_unit
    double total_cost

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization  : Organization,
                      invquotation  : InvQuotation,
                      invmaterial   : InvMaterial]

    static constraints = {}

    static mapping = {
        isactive defaultValue: true
        quantity defaultvalue:0
        cost_per_unit defaultvalue:0
        total_cost defaultvalue:0
    }
}
