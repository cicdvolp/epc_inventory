package volp

class ERPCoursePlanDetails {

    int sno
    String value
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourseplanteachingparadigm:ERPCoursePlanTeachingParadigm,
                      erpcourseunit:ERPCourseUnit,erpcoursetopic:ERPCourseTopic,
                      erpcourse:ERPCourse,erpcourseplan:ERPCoursePlan,organization:Organization]
    static constraints = {
        erpcourseunit nullable:true
        erpcoursetopic nullable:true
        erpcourseplan nullable:true
    }
}
