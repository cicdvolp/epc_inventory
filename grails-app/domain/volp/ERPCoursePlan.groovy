package volp

class ERPCoursePlan {

    String problem_statement   //used ony for practical/tutoril
    int lecture_number_planned  //lecture or practical or tutorial number
    boolean iscurrent           //AY Sem redundant -> use iscurrent
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    //for ASP
    boolean isapplicableforasp //true dydeafault ASP - Assignement submission portal
    String aspfilepath   //material for ASP
    String aspfilename
    static belongsTo=[erpcourse:ERPCourse, instructor:Instructor,
                      loadtype:LoadTypeCategory,
                      academicyear:AcademicYear,semester:Semester,  //not applicable
                      organization:Organization,erplabassesmentschemetype:ERPLabAssesmentSchemeType]
    static constraints = {
        problem_statement nullable:true
        erplabassesmentschemetype nullable:true
        aspfilepath nullable:true
        aspfilename nullable:true
        instructor nullable:true
    }
    static mapping =
    {
        iscurrent defaultValue: false
        isapplicableforasp defaultValue: true
    }
    static hasMany = [erpcoursetopic:ERPCourseTopic]
}
