package volp

class ERPMCQBankAssementTypeLinkingMaster {

    String examname   //online mcq test2
    int actualexamtimeinminutespercourse
    int buffertimeinminutespercourse
    boolean iscurrentexam
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isgroupingapplicable
    static belongsTo=[organization:Organization,erpassementtype:ERPAssementType,academicyear:AcademicYear,semester:Semester]
    static constraints = {
    }
    static mapping = {
        iscurrentexam defaultValue: true
        isgroupingapplicable defaultValue: true
    }
    String toString()
    {
        examname
    }
}
