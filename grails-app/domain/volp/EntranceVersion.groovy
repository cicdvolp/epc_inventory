package volp

class EntranceVersion {

    String entrance_name
    boolean isactive
    int version_number
    Date version_date
    int applicationtrack            //by default, insert 0, then increment by 1
    int receipttrack                //by default, insert 0, then increment by 1
    Date application_start_date
    Date application_end_date
    Date exam_date
    String exam_start_time
    String exam_end_time
    String exam_link

    boolean is_fee_applicable_to_handicap_person

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization  : Organization,
                      academicyear  : AcademicYear,
                      programtype   : ProgramType]

    static constraints = {}
}
