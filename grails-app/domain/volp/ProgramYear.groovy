package volp

class ProgramYear {

    static belongsTo=[program:Program,year:Year,organization:Organization]
    static constraints = {
        organization nullable:true
    }
}
