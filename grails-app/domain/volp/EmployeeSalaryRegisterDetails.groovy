package volp

class EmployeeSalaryRegisterDetails {
    double value

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[instructor:Instructor,organization:Organization,employeesalaryregister:EmployeeSalaryRegister,employeesalarymaster:EmployeeSalaryMaster,salarycomponent:SalaryComponent]
    static constraints = {
    }
}
