package volp

class InvMaterialInspection {

    boolean is_inspected    //Is Inspected by Concerned person?
    boolean is_training_given //Is training given by Vendor?
    boolean is_material_inspected_tested_installed
    boolean is_original_copies_of_invoice_collected
    boolean is_delivery_challen_collected
    boolean is_test_certificate_received
    boolean is_warranty_card_supplied
    boolean is_amc_certificate_provided
    boolean is_technical_demonstration_done
    boolean is_user_manual_provided
    boolean is_installation_manual_provided
    boolean is_maintainance_manual_provided

    boolean isactive
    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization  : Organization,
                      invoice       : Invoice,
                      inspectionby  : Instructor]

    static constraints = {}

    static mapping = {
        isactive defaultValue: true
        remark nullable:true
    }

}
