package volp

class TPOCompanyInvitation
{
    boolean isinvited
    boolean isaccepted
    int visitNumber
    Date invitationdate
    String remark
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[tpo:TPO,tpocompany:TPOCompany,academicyear:AcademicYear]
    static constraints = {
    }
}
