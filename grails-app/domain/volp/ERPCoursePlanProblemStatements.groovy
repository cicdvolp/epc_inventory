package volp

class ERPCoursePlanProblemStatements {

    String problemstatement
    boolean isapproved
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourseplan:ERPCoursePlan,erpcourse:ERPCourse,loadtype:LoadTypeCategory,
                      instructor:Instructor,organization:Organization]
    static constraints = {
    }
    static mapping = {
        isapproved defaultValue: true
            }
}
