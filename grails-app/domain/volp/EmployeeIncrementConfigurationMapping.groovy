package volp
//deleted
class EmployeeIncrementConfigurationMapping {
    double current_basic
    double updated_basic
    boolean isActive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[incrementyear:IncrementYear,instructor:Instructor,
                      incrementconfiguration:IncrementConfiguration,
                      organization:Organization]
    static constraints = {
    }
    static mapping={
        isActive defaultValue:true
    }
}
