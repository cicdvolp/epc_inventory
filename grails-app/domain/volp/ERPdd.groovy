package volp

class ERPdd {

    String dd_no
    Date dd_date
    String drawn_on_bank_name
    String bank_branch_name
    double dd_amount
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpstudentfeesmaster:ERPStudentFeesMaster]
    static constraints = {
    }
    String toString(){
        dd_no
    }
}
