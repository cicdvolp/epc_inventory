package volp
//employee type is not in use
class IncrementConfiguration {
    boolean is_percent_based
    String formula
    String display_formula
    String template_name //only use to give name for particular configuration
    double value
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[incrementyear:IncrementYear,paycommision:PayCommision,
                      payrolldesignation:PayrollDesignation,
                      payrollemployeetype:PayrollEmployeeType,
                      employeetype:EmployeeType,organization:Organization]
    static constraints = {
        payrolldesignation nullable: true
        employeetype nullable: true //this is no more in use
        formula nullable:true
        display_formula nullable:true
        payrollemployeetype nullable:true
    }
}
