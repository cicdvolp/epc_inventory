package volp

class ERPExamRoomOfferingSchedule {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpexamroomoffering:ERPExamRoomOffering,
                      erpexamdetailedschedule:ERPExamDetailedSchedule]
    static constraints = {
    }
}
