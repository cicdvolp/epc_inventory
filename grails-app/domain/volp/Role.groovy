package volp

class Role {

    String role
    int sort_order
    String role_displayname
    boolean isRoleSet
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address    
    static belongsTo=[roletype:RoleType,usertype:UserType,organization:Organization]
    static constraints = {
        organization nullable: true
    	role nullable:false
        usertype nullable:true
        role_displayname nullable:true
    }
    String toString(){
    	role
    }
}
