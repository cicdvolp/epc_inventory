package volp

class EntranceApplication {

    String applicaitionid           //Ex.120190517XXXXX       Organization Number:1 + Date:yyyymmdd + 5 digit application tracking number
    Date applicationdate
    String place                    //Ex. Pune
    boolean is_declaration_accepted
    boolean isfeespaid
    boolean is_exemption_from_test

    boolean is_physically_handicapped
    boolean is_visually_handicapped

    String is_physically_handicapped_file_path
    String is_physically_handicapped_file_name
    String is_visually_handicapped_file_path
    String is_visually_handicapped_file_name

    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization                      : Organization,
                      entranceapplicant                 : EntranceApplicant,
                      entranceversion                   : EntranceVersion,
                      entrancecategory                  : EntranceCategory,
                      entrancecategorytype              : EntranceCategoryType,
                      actionby                          : Instructor,
                      exemption                         : Exemption,
                      entrancerejectionreason           : EntranceRejectionReason,
                      entranceapplicationstatus         : EntranceApplicationStatus,
                      entranceapplicationreceipt        : EntranceApplicationReceipt]

    static hasMany = [entrancebranch                    : EntranceBranch,
                      exemptiontype                     : ExemptionType,
                      exemptiondocument                 : ExemptionDocument]

    static constraints = {
        entranceapplicationreceipt nullable:true
        actionby nullable:true
        is_physically_handicapped_file_path nullable:true
        is_physically_handicapped_file_name nullable:true
        is_visually_handicapped_file_path nullable:true
        is_visually_handicapped_file_name nullable:true
        remark nullable:true
    }

    static mapping = {
        isfeespaid defaultValue: false
        is_physically_handicapped defaultValue: false
        is_visually_handicapped defaultValue: false
        is_exemption_from_test defaultValue: false
    }
}
