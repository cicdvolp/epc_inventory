package volp

class ERPProgramwiseCreditMapping {

    double total_number_of_credits
    double minimum_cgpa
    double minimum_sgpa
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,admissionyear:AcademicYear,
                      program:Program,programtype:ProgramType,syllabuspattern:SyllabusPattern]
    static constraints = {
        program nullable:true
        programtype nullable:true
        syllabuspattern nullable:true
    }
    static mapping = {
        minimum_cgpa defaultValue:0
        minimum_sgpa defaultValue:0
    }
}
