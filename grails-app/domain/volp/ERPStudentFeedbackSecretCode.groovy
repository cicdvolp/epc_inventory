package volp

class ERPStudentFeedbackSecretCode
{
    String secret_code
    boolean isFeedbackGiven
    boolean  iseligibleforfeedback
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,learner:Learner,erpstudentfeedbackversion:ERPStudentFeedbackVersion,
                      program:Program,year:Year,divisionoffering:DivisionOffering]
    static mapping = {
        isFeedbackGiven defaultValue: false
        iseligibleforfeedback defaultValue: true
    }
    static constraints = {
        program nullable:true
        year nullable:true
        divisionoffering nullable:true
    }
}
