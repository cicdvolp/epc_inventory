package volp

class ERPHostelStudentReceipt {
    String receipt_no
    double amount
    String remark
    String date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erphostelstudentfeemaster:ERPHostelStudentFeesMaster,year:AcademicYear,learner:Learner,organization:Organization]
    static hasMany=[erphostelreceipttransactions:ERPHostelReceiptTransactions]


    static constraints = {
         year nullable: true
        remark nullable:true
    }
}
