package volp

class ERPAssesmentSchemeOffering {


    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization ,
                      erpassesmentscheme:ERPAssesmentScheme,
                      academicyear:AcademicYear,semester:Semester]

    static constraints = {
    }
}
