package volp

class DTEAllotmentList {

    String applicationid
    String name_of_candidate
    String merit_no
    String mht_cet_score
    String jee_marks
    String diploma_marks
    static belongsTo=[erpround:ERPAdmissionRound,
                      organization:Organization,
                      gender:Gender,
                      category:ERPStudentAdmissionMainCategory,
                      erpshift:ERPShift,
                      erpseattype:ERPSeatType,
                      program:Program,
                      academicyear:AcademicYear,
                      year:Year,
                      erpdomacile:ERPDomacile,
                      erpadmissionquota:ERPAdmissionQuota]
    static constraints = {
        year nullable : true
        mht_cet_score nullable : true
        jee_marks nullable : true
        diploma_marks nullable : true
        erpdomacile nullable : true
        erpadmissionquota nullable : true
    }

    String toString(){
        applicationid
    }
}
