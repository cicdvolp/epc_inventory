package volp

class EmployeeLeaveMaster {
    Date fromdate
    Date todate
    Date expirydate
    double numberofdays
    double numberofhours

    String file_name
    String file_path

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,
                      organization:Organization,
                      leavetype:LeaveType,
                      employeetype:EmployeeType,
                      academicyear:AcademicYear,
                      leavemaster:LeaveMaster,
                      compoffstatus:CompOffStatus,
                      employeeleavetransaction:EmployeeLeaveTransaction]

    static constraints = {
        expirydate nullable: true
        leavemaster nullable: true
        employeetype nullable: true
        compoffstatus nullable: true
        employeeleavetransaction nullable: true
        file_name nullable: true
        file_path nullable: true
        numberofhours defaultValue: 0
    }

    static mapping = {
        numberofhours defaultValue: 0
    }
}
