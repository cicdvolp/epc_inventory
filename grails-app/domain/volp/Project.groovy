package volp

class Project {

    String title
    Date startdate
    Date enddate
    String domain  //(optional)
    String companyname  //(optional)
    String description
    String internal_guide
    String external_guide
    String role
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,company:Company,instructor:Instructor,organization:Organization]
    static constraints = {
    }
}
