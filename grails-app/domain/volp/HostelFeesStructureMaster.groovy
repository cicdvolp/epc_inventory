package volp

class HostelFeesStructureMaster {

    String name
//    double total_amount

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hosteloffering:HostelOffering, academicyear:AcademicYear,
                      hostelmesstype:HostelMessType, hostel:Hostel]

    static constraints = {
        hosteloffering(unique: ['academicyear', 'hostelmesstype', 'hostel'])
    }

}
