package volp

class ERPAddmissionCancel {
    double institute_share_amount
    String reason   //accounts section
    String student_section_reason
    String amount_cancel // full or partial
    String student_reason
    Date studsec_date
    Date accsec_date
    String studsec_username
    String accsec_username
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean bypassstudentflow
    //Refunddetails
    double refundedamount
    String utrnumber
    Date refunddate


    static belongsTo=[organization:Organization,academicyear:AcademicYear,semester:Semester,
                      erpadmissioncancelationreason:ERPAdmissionCancelationReason,erpfeestudentmaster:ERPFeeStudentMaster,
                      erpfeedepositetype:ERPFeesDepositeType,year:Year,learner:Learner,accountsectionadmissionstatus:ERPAdmissionStatus,
                      studentsectionadmissionstatus:ERPAdmissionStatus,refundedby:Instructor]
    static constraints = {
        reason nullable: true
        amount_cancel nullable: true
        student_section_reason nullable: true
        student_reason nullable: true
        refundedby nullable: true
        utrnumber nullable: true
        refunddate nullable: true
        year nullable: true
        erpfeestudentmaster nullable: true
        erpfeedepositetype nullable: true
        studentsectionadmissionstatus nullable: true
        accountsectionadmissionstatus nullable: true

        studsec_date nullable: true
        studsec_username nullable: true
        accsec_date nullable: true
        accsec_username nullable: true
        organization nullable: true
        academicyear nullable: true
        semester nullable: true
        erpadmissioncancelationreason nullable: true
        username nullable: true
        creation_date nullable: true
        updation_date nullable: true
        creation_ip_address nullable: true
        updation_ip_address nullable: true
    }
}
