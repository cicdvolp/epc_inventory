package volp

class ERPAssesmentSchemeOfferingLinking {

    String username
    String creation_date
    String updation_date
    String creation_ip_address
    String updation_ip_address
    String program
    String programtype
    String year
    String coursetype
    String erpcourseoffering
    static belongsTo=[erpassesmentschemeoffering:ERPAssesmentSchemeOffering,
                      organization:Organization
    ]
    static constraints = {
        program nullable : true
        programtype nullable : true
        year nullable : true
        coursetype nullable : true
        erpcourseoffering  nullable : true
    }
}