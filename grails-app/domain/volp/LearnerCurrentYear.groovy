package volp

class LearnerCurrentYear {

    boolean ispassout  //true mean student is passout
    boolean isdeleted  //true mean student is no more with college
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    //every learner will have only one entry for module
    static belongsTo=[organization:Organization,
                      learner:Learner,
                      roletype:RoleType,
                      currentyear:Year,
                      academicyear:AcademicYear,
                      semester:Semester]
    static constraints = {
        semester nullable:true
        //isdeleted nullable:true
        username nullable:true
        creation_date nullable:true
        updation_date nullable:true
        creation_ip_address nullable:true
        updation_ip_address nullable:true
    }
    static mapping = {
        ispassout defaultValue: false
        isdeleted defaultValue: false
    }
}
