package volp

class EntranceApplicant {

    String email        //user name
    String password
    boolean isblocked
    String email_otp
    String email_otp_generation_date
    String fullname
    Date dateofbirth
    String mobilenumber
    String photopath
    String photoname
    String local_address
    String permanent_address
    String aadhar_number
    String signpath
    String signname
    boolean is_physically_handicapped
    boolean is_visually_handicapped

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo = [entrancecategory    : EntranceCategory,
                        gender              : Gender,
                        maritalstatus       : MaritalStatus,
                        erpnationality      : ERPNationality,
                        erpdomacile         : ERPDomacile,
                        employmentstatus    : EmploymentStatus,
                        exemption           : Exemption,
                        exemptiontype       : ExemptionType]

    static constraints = {
        dateofbirth nullable:true
        local_address nullable:true
        permanent_address nullable:true
        aadhar_number nullable:true
        entrancecategory nullable:true
        gender nullable:true
        maritalstatus nullable:true
        erpnationality nullable:true
        erpdomacile nullable:true
        employmentstatus nullable:true
        exemption nullable:true
        exemptiontype nullable:true
        photopath nullable:true
        photoname nullable:true
        signpath nullable:true
        signname nullable:true
    }

    static mapping = {
        isblocked defaultValue: false
        is_physically_handicapped defaultValue: false
        is_visually_handicapped defaultValue: false
    }
}
