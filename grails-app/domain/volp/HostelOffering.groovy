package volp

class HostelOffering {

    int hostel_capacity
    int min_fees_amount
    int min_room_booking_amount


    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostel:Hostel, hostelcategory:HostelCategory, gender :Gender,
                      academicyear:AcademicYear, organizationgroup:OrganizationGroup]

    static constraints = {
        hostel(unique: ['academicyear', 'organizationgroup' , 'hostelcategory' , 'gender'])
    }
}
