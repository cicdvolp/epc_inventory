package volp

class TPOCompanyOfferingSchedule {

    Date schedule_date
    String description
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[tpocompanyoffering:TPOCompanyOffering,tpo:TPO]
    static constraints = {
    }
}
