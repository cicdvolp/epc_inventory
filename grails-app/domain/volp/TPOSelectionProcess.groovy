package volp

class TPOSelectionProcess {

    String name
    boolean isSelectionProcess

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    String toString()
    {
        name
    }
    static belongsTo=[tpo:TPO]
    static constraints = {
        name unique: true
        isSelectionProcess defaultValue : true
        tpo nullable:true
    }
}
