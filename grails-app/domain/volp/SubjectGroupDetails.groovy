package volp

class SubjectGroupDetails {

    int display_order

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, subjectgroupmaster:SubjectGroupMaster, erpcourse: ERPCourse]

    static constraints = {
    }

    static mapping = {
        display_order defaultValue:0
    }
}
