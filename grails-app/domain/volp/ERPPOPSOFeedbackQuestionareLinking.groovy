package volp

class ERPPOPSOFeedbackQuestionareLinking
{
    boolean isActive   //Yes:Active   False:Inactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpfeedbackquestionare:ERPFeedbackQuestionare,
                      erppo:ERPPO,program:Program,programtype:ProgramType]
    static constraints = {
        isActive defaultValue: true
    }
}
