package volp

class ERPCoursePlanBook {
    String title
    String auther
    String publication
    String isbn
    String volume
    static belongsTo=[erpbooktype:ERPBookType,erpcourse:ERPCourse,organization:Organization]

    static constraints = {

        publication nullable:true
        isbn nullable:true
        volume nullable:true

    }

}