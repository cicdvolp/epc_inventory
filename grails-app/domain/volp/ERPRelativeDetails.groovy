package volp

class ERPRelativeDetails {
    String full_name
    String education
    String income
    String email
    String organization_name
    String department
    String designation
    String office_address
    String mobile_no
    String phone_no

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erprelationtype:ERPRelationType,erpoccupation:ERPOccupation,learner:Learner]
    static constraints = {
        full_name nullable: true
        education nullable: true
        income nullable: true
        email nullable: true
        organization_name nullable: true
        department nullable: true
        designation nullable: true
        office_address nullable: true
        mobile_no nullable: true
        phone_no nullable: true
        erpoccupation nullable: true
    }
}
