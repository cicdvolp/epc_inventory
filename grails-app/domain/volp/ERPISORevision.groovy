package volp

class ERPISORevision
{
    String issue_number
    String revision_number
    Date revision_date
    String ff_number
    String formname  //form description
    String username
    boolean iscurrent
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,roletype:RoleType,academicyear:AcademicYear]
    static constraints = {
        academicyear nullable:true
    }
}
