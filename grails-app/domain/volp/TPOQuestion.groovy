package volp

class TPOQuestion {
    String question
    String answer
    boolean isAproved
    boolean isDeleted
    String question_file_name
    String question_file_path
    String question_file_link

    static belongsTo=[  learner : Learner,
                        instructor  : Instructor,
                        tPOCompanyOffering:TPOCompanyOffering,
                        tPOQuestionType:TPOQuestionType,
                        tPOSelectionProcess :TPOSelectionProcess,
                        aprovedBy : Instructor,
                        tPOQuestionStatus:TPOQuestionStatus  ]

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String toString()
    {
        question
    }

    static constraints = {
        answer nullable:true
        learner nullable:true
        instructor nullable:true
        aprovedBy nullable:true
        question_file_name nullable:true
        question_file_path nullable:true
        question_file_link nullable:true
        isAproved defaultValue : true
        isDeleted defaultValue : false
        tPOQuestionStatus defaultValue : false

        //ADD
        tPOQuestionStatus nullable : true

    }
}
