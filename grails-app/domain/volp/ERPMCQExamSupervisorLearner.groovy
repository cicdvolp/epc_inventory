package volp

class ERPMCQExamSupervisorLearner {

    boolean iscompleted
    String ipaddress
    Date starttime   //it should be time..plz correct it
    Date endtime   //it should be time..plz correct it
    Date extratime  //it should be time..plz correct it

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpmcqexamsubjectgroupmaster:ERPMCQExamSubjectGroupMaster,
                      organization:Organization,
                      mcqexamstatus:MCQExamStatus,
                      erpmcqexamsupervisorsession:ERPMCQExamSupervisorSession,
                      learner:Learner,
                      instructor:Instructor,
                      erpmcqbankassementtypelinkingmaster:ERPMCQBankAssementTypeLinkingMaster]
    static constraints = {
        starttime nullable:true
        endtime nullable:true
        extratime nullable:true
        ipaddress nullable:true
    }
    static mapping = {
        iscompleted defaultValue: false
    }
}
