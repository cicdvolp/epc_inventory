package volp

class ERPGradeMaster {

    String gradingschemename  // //uniquename   collegename_scheme_name
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpgradingpolicy:ERPGradingPolicy]
    static constraints = {
    }
    String toString(){
        gradingschemename
    }
}