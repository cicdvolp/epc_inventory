package volp

class ERPExamPlan {

    String exam_name        // apr-mar, oct-nov
    String plan_name
    boolean isApproved
    boolean isCurrent
    Date startdate
    Date enddate
    Date resultdate
    int maximum_number_of_slots_in_day
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamtype:ERPExamType,
                      academicyear:AcademicYear,semester:Semester,erpexamconductmethod:ERPExamConductMethod,erpexamconducttype:ERPExamConductType]
    static hasMany = [erpexamconductType:ERPExamConductType,program:Program,programtype:ProgramType]
    static constraints = {
        program nullable:true
        erpexamconductmethod nullable:true
        erpexamconducttype nullable:true
        exam_name nullable:true
    }
    static mapping = {
        isCurrent defaultValue: true
        isApproved defaultValue: false
    }
    String toString()
    {
        return plan_name
    }
}
