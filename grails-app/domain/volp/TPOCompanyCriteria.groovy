package volp

class TPOCompanyCriteria {

    double  min_value
    double  max_value
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[tpocompanycriteriatype:TPOCompanyCriteriaType,tpocompanyoffering:TPOCompanyOffering,tpo:TPO]
    static constraints = {
    }
}
