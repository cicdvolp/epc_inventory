package volp

class QuizOffering {

    Date start_date   //include time also
    Date end_date //include time also
    boolean isactive
    boolean isdeleted
    double weightage
    String meeting_url

    boolean isallocated

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, quizmaster: QuizMaster,
     erpcourse:ERPCourse,erpcourseoffering:ERPCourseOffering,
    instructor:Instructor,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
    erpcourseofferingbatch:ERPCourseOfferingBatch, supervisor:Instructor, academicyear:AcademicYear,semester:Semester]

    static constraints = {
        meeting_url nullable:true
        erpcourse nullable:true
        academicyear nullable:true
        semester nullable:true
        erpcourseoffering nullable:true
        instructor nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcourseofferingbatch nullable:true
        supervisor nullable:true
    }

    static mapping = {
        isallocated defaultValue: false
        isdeleted defaultValue: false
    }
}
