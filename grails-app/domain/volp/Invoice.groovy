package volp

class Invoice {

    int invoice_number          // serial number
                                // system generated invoice number
                                // purchase order may have multiple invoices
    double amount
    double tax
    double total
    String purpose
    String file_name
    String file_path
    Date invoice_date

    boolean isapproved
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      invvendor             : InvVendor,
                      invpurchaseorder      : InvPurchaseOrder]

    static constraints = {
        purpose nullable : true
        file_name nullable : true
        file_path nullable : true
        invoice_date nullable : true
    }

    static mapping = {
        isactive defaultValue: true
        isapproved defaultValue: true
        invoice_number defaultvalue:0
        amount defaultvalue:0
        total defaultvalue:0
        tax defaultvalue:0
    }
}
