package volp

class TemplateDetails {

    double credit_per_course
   // String subject_head
    int bucket_number
    boolean isProgramSpectificCompulsoryElective   //Yes/No
    int no_of_electives//or ps crs
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[coursetypeacademics:CourseTypeAcademics,organization:Organization,template:Template,erpcoursecategory:ERPCourseCategory,courserule:CourseRule,coursetype:CourseType,erpcoursehead:ERPCourseHead]
    static constraints = {
        erpcoursecategory nullable:true
        coursetypeacademics nullable: true
    }
    static mapping = {
        isProgramSpectificCompulsoryElective defaultValue: false
    }


}
