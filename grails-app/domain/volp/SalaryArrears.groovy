package volp

class SalaryArrears {
    double actual_changed_component_value
    double total_arrears   //cumulative amount
    boolean is_percentage
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,salarycomponent:SalaryComponent,givenmonth:SalaryMonth,frommonth:SalaryMonth,tomonth:SalaryMonth,paycommision:PayCommision]
    static constraints = {
    }
    static mapping={
        is_percentage defaultValue:false
    }
}
