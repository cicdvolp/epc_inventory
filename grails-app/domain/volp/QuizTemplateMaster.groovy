package volp

class QuizTemplateMaster {

    String quiztemplatename
    double total_marks

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, instructor:Instructor]

    static constraints = {
        instructor nullable:true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
