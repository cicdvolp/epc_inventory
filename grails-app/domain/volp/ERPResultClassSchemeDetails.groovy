package volp

class ERPResultClassSchemeDetails {

    String classname
    double min
    double max
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpresultclassscheme:ERPResultClassScheme]
    static constraints = {
    }
}
