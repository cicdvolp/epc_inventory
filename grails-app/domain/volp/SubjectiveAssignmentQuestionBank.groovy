package volp

class SubjectiveAssignmentQuestionBank
{
    int subjective_assignment_number
    double weightage    //maxmarks
    boolean isapproved
    boolean isdeleted    //true:deleted
    String problem_statement  //problem statement
    String subjective_assignment_path     //d:/cloud/course/courseid/subjectiveassignment/
    String subjective_assignment_file_name   //subjective_assignment_number_actualfilename
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[coursetopic:CourseTopic,courseoutline:CourseOutline,course:Course,
                      erpcourse:ERPCourse,erpcourseunit:ERPCourseUnit,
                      instructor:Instructor,
                      assignmenttype:AssignmentType,
                      difficultylevel:DifficultyLevel,organization:Organization]
    static constraints = {
        problem_statement nullable: true
        subjective_assignment_path nullable: true
        subjective_assignment_file_name nullable: true
        coursetopic nullable: true
        courseoutline nullable: true
        course nullable: true
        erpcourse nullable: true
        instructor nullable: true
        erpcourseunit nullable: true
        organization nullable: true
    }
    static mapping = {
        isdeleted defaultValue: false
        isapproved defaultValue: false
    }
    String toString()
    {
        subjective_assignment_number
    }
}
