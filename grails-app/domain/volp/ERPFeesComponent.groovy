package volp

class ERPFeesComponent
{
    String name_of_component    //Fees/ Development Fees/ Exam Fees
    int collection_order
    int display_order
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpfeecomponentype:ERPFeeComponentType]
    static constraints = {
        organization nullable:true
    }
    String toString(){
        name_of_component
    }
}
