package volp

class GrayQuestCredential {

    String username
    String password
    String apikey

    boolean isenabled

    static belongsTo=[ organization:Organization]

    static constraints = {
    }

    static mapping = {
        isenabled defaultValue: false
    }
}
