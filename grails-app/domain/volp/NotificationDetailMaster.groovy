package volp

class NotificationDetailMaster {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String description
    String subject
    String body
    String attachmentname
    String attachmentpath
    double interval

    static belongsTo=[notificationtype:NotificationType,notificationcategory:NotificationCategory,notificationapplicablecategory:NotificationApplicableCategory,notificationsendingtechnique:NotificationSendingTechnique]
    static constraints = {
        description  nullable:true
        subject nullable:true
        body nullable:true
        attachmentname nullable:true
        attachmentpath nullable:true
    }
}
