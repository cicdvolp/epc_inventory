package volp

class HostelLearnerRoom {

    Date allocation_date
    boolean isactive
    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostellearner:HostelLearner, hostel:Hostel, hosteloffering :HostelOffering,
                      hostelroom:HostelRoom, hostelroomoffering:HostelRoomOffering, learner:Learner,
                      hostelbedoffering:HostelBedOffering, hostelroombookingstatus:HostelRoomBookingStatus]

    static constraints = {
        hostellearner unique: true
        remark nullable : true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
