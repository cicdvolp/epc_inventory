package volp

class HostelRoom {

    String roomno
    String display_name
    String room_plan_file_name
    String room_plan_file_path
    boolean isactive


    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostel:Hostel, organizationgroup:OrganizationGroup]

    static constraints = {
        roomno unique: 'hostel'
        room_plan_file_name nullable : true
        room_plan_file_path nullable : true
    }

    static mapping = {
        isactive defaultValue: true
    }

}
