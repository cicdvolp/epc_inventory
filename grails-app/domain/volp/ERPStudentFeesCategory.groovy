package volp

class ERPStudentFeesCategory
{

    String type   //SC/ST/Open/OBC
    boolean isoverandabove
    boolean isactive
    boolean isfeesstructureapplicable

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    String toString(){
        type
    }
    static mapping = {
        isoverandabove defaultValue: false
        isfeesstructureapplicable defaultValue: false
        isactive defaultValue: false
    }
}
