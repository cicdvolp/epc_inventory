package volp

class MitraTravelHistory {

    Date fromdate
    Date todate
    boolean isselfquarantined

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, erpdistrict:ERPDistrict, erpstate:ERPState,
                       erpcountry :ERPCountry, learner:Learner, instructor:Instructor]

    static mapping = {
        isselfquarantined defaultValue: false
    }

    static constraints = {
        learner nullable:true
        instructor nullable:true
    }
}
