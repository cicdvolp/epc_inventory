package volp

class SubjectiveAssignmentAllocation {

    Date allocation_date
    Date deadline_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,courseofferinglearner:CourseOfferingLearner,
                      erpcourse:ERPCourse,erpcourseoffering:ERPCourseOffering,
                      erpcourseofferinglearner:ERPCourseOfferingLearner,
                      subjectiveassignmentquestionbank:SubjectiveAssignmentQuestionBank,
                      instructor:Instructor,academicyear:AcademicYear,semester:Semester,
                      organization:Organization]
    static constraints = {
        courseofferinglearner nullable:true
        allocation_date nullable:true
        deadline_date nullable:true
        organization nullable:true
        erpcourse nullable:true
        erpcourseoffering nullable:true
        instructor nullable:true
        academicyear nullable:true
        semester nullable:true
    }
}
