package volp

class ERPFacultySelfAppraisalAttachment
{
    boolean isapproved
    String filename
    String filepath
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpselfappraisalsubparameter:ERPSelfAppraisalSubParameter,
                      instructor:Instructor,academicyear:AcademicYear,
                      erpfacultyselfappraisalauthoritymarks:ERPFacultySelfAppraisalAuthorityMarks]
    static constraints = {
    }
    static mapping = {
        isapproved defaultValue: false
        erpfacultyselfappraisalauthoritymarks nullable:true
    }
}
