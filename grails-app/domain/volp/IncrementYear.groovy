package volp

class IncrementYear {
        String name//2019-20
    Date fromdate
    Date todate
    boolean isactive
    boolean iscalculationdone
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpyear:ERPCalendarYear,organization:Organization]

    static constraints = {
        fromdate nullable:true
        todate nullable:true
    }

    static mapping={
        isactive defaultValue:false
        iscalculationdone defaultValue: false
    }
}
