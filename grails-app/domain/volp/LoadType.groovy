package volp

class LoadType
{
	int hrs   //total hours per week
    int numberofturnspersemester
    String loadtype_name   //Theory2,Lab2,Theory3 //display name
    String username
    int actual_execution_hrs   //total hours per turn
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,type:LoadTypeCategory]
    static constraints = {
    }

    static mapping = {
        numberofturnspersemester defaultValue : 0
    }


}
