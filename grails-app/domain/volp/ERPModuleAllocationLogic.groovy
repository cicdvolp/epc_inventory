package volp

class ERPModuleAllocationLogic {
    static belongsTo=[program:Program,year:Year,organization:Organization,
                      erpprogramgroup:ERPProgramGroup,module:Module]
    static constraints = {
        erpprogramgroup nullable:true
    }
}
