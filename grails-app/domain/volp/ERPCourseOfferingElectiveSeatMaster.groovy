package volp

class ERPCourseOfferingElectiveSeatMaster
{
    //This is only for elective subject
    int max    //max divisionwise capacity
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpcourseoffering:ERPCourseOffering,erpcourse:ERPCourse,divisionoffering:DivisionOffering,academicyear:AcademicYear,semester:Semester]
    static constraints = {
        academicyear nullable:true
        semester nullable:true
    }
}
