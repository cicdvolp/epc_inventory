package volp

class Criterion {

    String number
    String name
    String description
    boolean isactive

    int sortorder
    boolean islast
    String help_filename
    String help_folderpath

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, criterion:Criterion,
                       criterionaccesslevel:CriterionAccessLevel, criterionhierarchylevel:CriterionHierarchyLevel,
                       report : Report]

    static constraints = {
        criterion nullable : true
        criterionaccesslevel nullable : true
        description nullable : true
        help_filename nullable : true
        help_folderpath nullable : true
        report nullable : true
    }

    static mapping = {
        isactive defaultValue: false
        islast defaultValue: false
    }
}
