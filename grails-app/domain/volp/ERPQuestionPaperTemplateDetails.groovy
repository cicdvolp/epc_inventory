package volp

class ERPQuestionPaperTemplateDetails {

    int qno
    char opno
    double optionmaxmarks    // single option max marks
    double questionmaxmarks  // question with all options max marks
    double questionmaxmarksexcludingoptions // question with all options max marks
    int questiongroupno
    int optiongroupno
    int questiongrouppickupcount
    int optiongrouppickupcount

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,erpquestionpapertemplateoffering:ERPQuestionPaperTemplateOffering]
    static constraints = {
    }
    static mapping = {
        questiongroupno defaultValue: 0
        optiongroupno defaultValue: 0
        questiongrouppickupcount defaultValue: 0
        optiongrouppickupcount defaultValue: 0
    }
}
