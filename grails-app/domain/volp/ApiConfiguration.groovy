package volp

class ApiConfiguration {

    String name         // volp
    String url          // classroom.volp

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static constraints = { }
}
