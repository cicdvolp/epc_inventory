package volp

class EmployeeSalaryMasterDetails {
    double value
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[instructor:Instructor,employeesalarymaster:EmployeeSalaryMaster,salarycomponent:SalaryComponent,organization:Organization]
    static constraints = {
    }
}
