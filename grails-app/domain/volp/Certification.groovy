package volp

class Certification {

    String certificationlink
    String certification_file_name
    String certification_file_path
    Date certification_date
    Date certification_valid_till
    String grade
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,instructor:Instructor,skill:Skill,organization:Organization]

    static constraints = {
        learner nullable: true
        instructor nullable: true
        skill nullable: true
        certificationlink nullable: true
        certification_file_name nullable: true
        certification_file_path nullable: true
        certification_date nullable: true
        certification_valid_till nullable: true
        grade nullable: true
    }
}
