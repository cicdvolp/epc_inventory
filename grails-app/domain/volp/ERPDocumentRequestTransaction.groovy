package volp

class ERPDocumentRequestTransaction
{
    String filename //generated document
    String filepath // generated document file path
    String reason   // What is this document , for passport or visa or something else
    Date application_push_date // Application request date
    boolean ismailsent
    boolean isfeespaid
    Date mailsentdate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpdocumentrequesttype:ERPDocumentRequestType,erpdocumentmaster:ERPDocumentMaster,
                      learner:Learner,instructor:Instructor,academicyear:AcademicYear,erpdocumentstatus:ERPDocumentStatus, year:Year]
    static constraints = {
        filename nullable:true
        filepath nullable:true
        mailsentdate nullable:true
        learner nullable:true
        instructor nullable:true
        erpdocumentstatus nullable:true
        year nullable: true
        reason nullable: true
    }
    static mapping = {
        ismailsent defaultValue: false
        isfeespaid defaultValue: false
    }
}
