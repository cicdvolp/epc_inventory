package volp

class ERPHostelFeesMaster {
    double total_fees_amount
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,academicyear:AcademicYear,hosteltype:ERPHostelType]

    static constraints = {
    }
}
