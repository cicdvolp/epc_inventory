package volp

class TPOInstructorCompanySelectionProcess {
    static belongsTo=[instructor :Instructor ,tPOCompanySelectionProcess:TPOCompanySelectionProcess]

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String toString()
    {
        instructor.employee_code + " : " + tPOCompanySelectionProcess
    }
    static constraints = {

        tPOCompanySelectionProcess unique : true
    }
}
