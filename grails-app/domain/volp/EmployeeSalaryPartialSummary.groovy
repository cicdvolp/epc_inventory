package volp

class EmployeeSalaryPartialSummary {
    double percentage//50%
    Date generation_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[salarymonth:SalaryMonth,organization:Organization,payrollemployeecategory:PayrollEmployeeCategory,payrolltmployeetype:PayrollEmployeeType]
    static constraints = {
        payrollemployeecategory nullable:true
        payrolltmployeetype nullable: true
    }
}
