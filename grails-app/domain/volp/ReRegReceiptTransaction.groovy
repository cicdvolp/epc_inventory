package volp

class ReRegReceiptTransaction {
    String tid //chegueno//dd
    Date date
    String bank_name

    double amount

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erppaymentmode:ERPPaymentMode,reregfees:ReRegFees,organization:Organization]

    static constraints = {
        tid nullable : true
        bank_name nullable : true
    }
}
