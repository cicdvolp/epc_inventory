package volp

class ERPCoursewiseCommonRelativeGradingConfiguration {

    int val1    //n for method2
    int gracemarks   //grace marks
    String val1description
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpgrademaster:ERPGradeMaster,erpcourseoffering:ERPCourseOffering]
    static constraints = {
        val1description nullable : true
    }
    static mapping = {
        val1 defaultValue: 0
        gracemarks defaultValue: 0
    }
}
