package volp

class InvQuotation {

    double amount
    double tax
    double total
    String file_path
    String file_name
    Date quotation_date
    boolean isapproved
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization              : Organization,
                      invvendor                 : InvVendor,
                      invpurchaserequisition    : InvPurchaseRequisition]

    static constraints = {
        file_name nullable:true
        file_path nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        isapproved defaultValue: false
        amount defaultValue: 0
        tax defaultValue: 0
        total defaultValue: 0
    }
}
