package volp

class ERPCOYearwiseAttainment {

    boolean isAttained
    int no_of_students
    double attained_value
    double level
    double threshold

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpco:ERPCO, organization:Organization, academicyear:AcademicYear, erpcourse:ERPCourse]

    static constraints = {
    }
}
