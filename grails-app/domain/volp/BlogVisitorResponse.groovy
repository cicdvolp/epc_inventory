package volp

class BlogVisitorResponse {
    String visitorname
    String visitoremail
    String comment
    Date responsedate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[blog:Blog]
    static constraints = {
    }
}
