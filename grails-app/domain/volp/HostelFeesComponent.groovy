package volp

class HostelFeesComponent {

    String name  // Hostel Fee/Mess Fee/Laundry Fee/Deposit

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organizationgroup:OrganizationGroup]

    static constraints = {
        name unique: 'organizationgroup'
    }
}
