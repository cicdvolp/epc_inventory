package volp

class ERPFacultyCombinedPerformanceIndex
{
    int institute_rank
    int department_rank
    double total_combined_out_of
    double total_combined_obtained_marks
    double total_combined_pi
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[academicyear:AcademicYear,instructor:Instructor,department:Department,organization:Organization]
    static constraints = {
        department nullable:true
    }
    static mapping = {
        institute_rank defaultValue: -1
        department_rank defaultValue: -1
    }
}
