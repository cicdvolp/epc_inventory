package volp

class ERPCourseUnit
{
    int unitno
    String unitname
    String username
    Date creation_date
    Date updation_date
    boolean isDeleted
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourse:ERPCourse,organization:Organization]
    String toString()
    {
        unitno +" : "+ unitname
    }
    static constraints = {
        isDeleted defaultValue:false
    }
}
