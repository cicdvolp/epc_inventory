package volp

class ERPLeranerResultReserve {
    boolean isresearved   //true->reserved   false->Not reserved
    String remark
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo = [organization       : Organization,
                        resultreservemaster: ResultReserveMaster,
                        learner            : Learner]
    static constraints = {
    }
    static mapping = {
        isresearved defaultValue: false
    }
}
