package volp

class LeaveType {
    String	type //(FD_CL,HD_CL,EL,OD,ML,HPL,LWP,CO,Vacation,Maternity Leave)   Vaccation1,vaccation2,vaccation3
    String	display_name
    boolean isactive
    double aditionalinfo1  // ML To HPL

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String toString(){
        type
    }

    static belongsTo=[organization:Organization,vacationtype:VacationType, leaveexpirytype:LeaveExpiryType]
    static constraints = {
        vacationtype nullable: true
        leaveexpirytype nullable: true
    }
    static mapping = {
        isactive defaultValue: true
        aditionalinfo1 defaultValue: 0
    }

}
