package volp

class Inventory {

    String accession_number     // QR Code/Bar Code / Ex.-> VIT/Comp/AY/Desk/Key/2000
                                // College Name-Department Name-Material Code-MaterialPart Code-serial number
                                // This number will be embded in QR code

    Date inward_date
    double purchase_cost        //50,000
    Date depreciation_date
    double depreciation_rate    //10%

    double depreciation_value   //5000
    double written_down_value   // purchase_cost-depreciation_value=45,000

    boolean iswriteoff
    Date writeoff_date

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      invoice               : Invoice,
                      invmaterial           : InvMaterial,
                      invmaterialpart       : InvMaterialPart,
                      invdeadstockstatus    : InvDeadstockStatus,
                      department            : Department,
                      invroom               : InvRoom,
                      invreceipt            : InvReceipt]

    static constraints = {
        depreciation_date nullable:true
        writeoff_date nullable:true
        invoice nullable: true
        invmaterialpart nullable: true
        invreceipt nullable: true
    }

    static mapping = {
        isactive defaultValue: true
        purchase_cost defaultValue: 0
        depreciation_value defaultValue: 0
        depreciation_rate defaultValue: 0
        written_down_value defaultValue: 0
        iswriteoff defaultValue: false
    }

}
