package volp

class ERPCourseOfferingBatchLearnerAttendance {

    int period_number  //theory/practical/tutorial number
    Date execution_date
    boolean is_present
    boolean smssendonabsent
    boolean emailsendonabsent

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourseofferingbatchlearner:ERPCourseOfferingBatchLearner,
                      erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseofferingbatch:ERPCourseOfferingBatch,learner:Learner,
                      instructor:Instructor,erpcourseoffering:ERPCourseOffering,slot:Slot]
    static constraints = {
        slot  nullable:true
    }

    static mapping = {
        smssendonabsent defaultValue: false
        emailsendonabsent defaultValue: false
    }
}
