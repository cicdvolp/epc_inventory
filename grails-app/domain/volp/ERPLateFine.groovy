package volp
class ERPLateFine   //This master is filled by student section
{
    boolean iscurrent
    double amount
    Date registrationstartdate //Excluding cuttoff date
    Date cutoffdate    //Excluding cuttoff date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,academicyear:AcademicYear,
                      semester:Semester,erplatefinecategory:ERPLateFineCategory,programtypeyear:ProgramTypeYear]
    static constraints = {
        cutoffdate nullable:true
        registrationstartdate nullable:true
        semester nullable:true
        programtypeyear nullable:true
    }
}