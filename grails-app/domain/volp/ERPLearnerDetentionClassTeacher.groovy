package volp

class ERPLearnerDetentionClassTeacher {

    boolean isApproved    //approval by classteacher
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,erpcourseoffering:ERPCourseOffering,erpcourseofferingbatch:ERPCourseOfferingBatch,erpdetentionpolicy:ERPDetentionPolicy,divisionoffering:DivisionOffering]
    static constraints = {
        erpcourseoffering  nullable : true
        erpcourseofferingbatch  nullable : true
    }

}
