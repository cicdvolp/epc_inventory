package volp

class FinincialYear {

    String year
    String shortcut
    boolean isactive
    boolean iscurrent
    boolean isattachmentenabled

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[previousyear:FinincialYear,nextyear:FinincialYear]
    String toString()
    {
        year
    }
    static constraints = {
        nextyear nullable:true
        previousyear nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        iscurrent defaultValue: true
        isattachmentenabled defaultValue: false
    }
}
