package volp

class MitraVisitor {

    String idcardnumber
    int visitorid
    String purpose
    Date dateofvisit
    String timeofvisit
    String whomtomeet

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, mitravisitorperson:MitraVisitorPerson,
                       idcardtype:IdcardType]

    static constraints = {
    }
}
