package volp

class ERPAssesmentSchemeOfferingApproval {
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    boolean isLocked   //true means locked....false means unlocked...

    static belongsTo=[organization:Organization ,
                      academicyear:AcademicYear,
                      semester:Semester]
    static constraints = {
    }
}
