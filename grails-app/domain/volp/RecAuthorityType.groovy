package volp

class RecAuthorityType
{
    String type   //HOD/Registrar/Management
    boolean islastauthority
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpfacultypost:ERPFacultyPost]
    static constraints = {
        erpfacultypost nullable:true
    }
    static mapping = {
        islastauthority defaultValue: false
    }
}
