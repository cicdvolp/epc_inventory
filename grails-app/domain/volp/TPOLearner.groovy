package volp

class TPOLearner {

    Date registrationdate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,tpo:TPO,academicyear:AcademicYear,organization:Organization,tpoplacementtype:TPOPlacementType,learnerboidataapproval:LearnerBoidataApproval,job:TPOLearnerStatus,internship:TPOLearnerStatus]
    static constraints = {
        job nullable:true
        internship nullable:true
    }
}
