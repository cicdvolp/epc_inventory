package volp

class ERPDay {

    int daysequence // 1 , 2
    String day   // Monday , Tuesday .....
    String displayname // MON , TUE
    static constraints = {
        daysequence nullable:true
    }
    String toString()
    {
        day
    }
}
