package volp

class ERPFeedbackQuestionare
{
    int question_sno
    String question_display_no
    String question_statement
    double question_weightage
    boolean isActive   //Yes:Active   False:Inactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpfeedbackquestiontype:ERPFeedbackQuestionType,organization:Organization,erpfeedbackcategoryoffering:ERPFeedbackCategoryOffering,program:Program,programtype:ProgramType]
    static constraints = {
        isActive defaultValue: true
        program nullable:true
        programtype nullable:true
        erpfeedbackquestiontype nullable:true
    }
}
