package volp

class EntranceApplicationReceipt {

    String feesreceiptid        //Ex. R120190517XXXXX       Organization Number:1 + Date:yyyymmdd + 5 digit
    double amount
    Date receiptdate

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization                  : Organization,
                      entranceapplicant             : EntranceApplicant,
                      entranceversion               : EntranceVersion,
                      entranceonlinetransaction     : EntranceOnlineTransaction]
    static hasMany = [entranceapplication : EntranceApplication]

    static constraints = { }
}
