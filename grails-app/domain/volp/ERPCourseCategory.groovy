package volp

class ERPCourseCategory {

    String name   //DC,MD
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,courserule:CourseRule]

    static constraints = {
        isactive nullable : true
        organization nullable : true
        courserule nullable : true
    }
    String toString(){
        name
    }

}
