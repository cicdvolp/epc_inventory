package volp

class ERPMCQPolicy {

    int unitno
    int numberofrandomquestionstobepicked
    double weightagetoeachquestion
    double totalmarks
    int numberofquestionsinbank
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpmcqbankassementtypelinkingmaster:ERPMCQBankAssementTypeLinkingMaster]
    static constraints = {
    }
}
