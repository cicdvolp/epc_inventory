package volp

class ERPCourseAssesmentResultReserve {
    String remark
    boolean isdeleted    //true:deleted
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpassesmentschemedetails:ERPAssesmentSchemeDetails,erpevaluationtype:ERPEvaluationType, erpcourseresultreservereasonmaster:ERPCourseResultReserveReasonMaster,erpcourseoffering:ERPCourseOffering,erpcourseofferinglearner:ERPCourseOfferingLearner,learner:Learner,academicyear:AcademicYear,semester:Semester]

    static constraints = {
        isdeleted defaultValue: false
        erpassesmentschemedetails nullable:true
        erpevaluationtype nullable:true
    }
}
