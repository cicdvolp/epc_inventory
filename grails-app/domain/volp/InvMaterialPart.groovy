package volp

class InvMaterialPart {

    String name             // Mouse/Keyboard/CPU/Monitor
    String specification
    String code
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      invmaterial           : InvMaterial]

    static constraints = {
        specification nullable : true
    }

    static mapping = {
        isactive defaultValue: true
    }
}
