package volp

class ERPExamRoomOffering {

    String block
    int number_of_benches
    int capacity_per_bench
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamplan:ERPExamPlan,erproom:ERPRoom,erproomtype:ERPRoomType]
    static constraints = {
        block nullable: true
    }
    String toString()
    {
        return erproom.roomnumber
    }
}
