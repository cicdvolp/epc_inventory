package volp

class LearnerCourseProgress {

    boolean isViewed   //false:NOT Viewed     //true:Viewed
    double currenttime
    static belongsTo=[courseofferinglearner:CourseOfferingLearner,coursevideos:CourseVideos]
    static constraints = {

    currenttime nullable:true
    }
}
