package volp

class ResponsibilityType {
    String type //ClassTeacher, Head, Dean, Institute-Coordinator, Department-Coordinator,  Institute-Committee-Member, Department- Committee-Member, Assistant Head, Assistant to Dean
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
    static belongsTo=[organization:Organization]

}
