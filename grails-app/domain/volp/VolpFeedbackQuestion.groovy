package volp

class VolpFeedbackQuestion {

    int qno
    String question_statement
    double maxrating
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[volpfeedbacktype:VolpFeedbackType]
    static constraints = {
    }
    static mapping = {
        qno defaultValue: -1
    }
}
