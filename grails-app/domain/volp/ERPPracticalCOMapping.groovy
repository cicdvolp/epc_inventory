package volp

class ERPPracticalCOMapping  //Common mapping for practical/assignment/tutorial
{
    String pracno
    double maxmarks
    boolean isfreezed
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpcourseoffering:ERPCourseOffering,erpassesmentschemedetails:ERPAssesmentSchemeDetails,erpco:ERPCO]
}
