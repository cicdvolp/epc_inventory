package volp

class HostelFeesComponentBreakup {

    double amount
    int sequence_of_payment
    double gst_percentage

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hosteloffering:HostelOffering, hostelfeescomponent:HostelFeesComponent,
                      hostelfeesstructuremaster:HostelFeesStructureMaster]

    static constraints = {
        hosteloffering(unique: ['hostelfeescomponent', 'hostelfeesstructuremaster'])
    }

    static mapping = {
        gst_percentage defaultValue: 0
    }
}
