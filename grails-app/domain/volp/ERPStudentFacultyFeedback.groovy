package volp

class ERPStudentFacultyFeedback {

    Date feedback_given_date
    String username
    String answer //Only for subjective Questions , erpfeedbackquestionoptions will be null for subjective Questions
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpstudentfeedbackversion:ERPStudentFeedbackVersion,
                      erpcourseofferinglearner:ERPCourseOfferingLearner,
                      erpcourseofferinginstructor:ERPCourseOfferingInstructor,
                      erpcourseofferingbatchlearner:ERPCourseOfferingBatchLearner,
                      erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseoffering:ERPCourseOffering,
                      learner:Learner,
                      instructor:Instructor,
                      erpfeedbackquestionare:ERPFeedbackQuestionare,
                      erpfeedbackquestionoptions:ERPFeedbackQuestionOptions,
                      erpstudentguardian:ERPStudentGuardian,guardian:Instructor]
    static constraints = {
        erpcourseofferinglearner nullable:true
        erpcourseofferinginstructor nullable:true
        erpcourseoffering nullable:true
        learner nullable:true
        instructor nullable:true
        erpcourseofferingbatchlearner nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpstudentguardian nullable:true
        guardian nullable:true
        erpfeedbackquestionoptions nullable:true
        answer nullable:true

    }
}
