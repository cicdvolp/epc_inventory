package volp

class StudentAttainment {

    boolean isAttained   //true->attained      false->not attained
    double attained_value     //attained percentages
    static belongsTo=[organization:Organization,erpco:ERPCO,learner:Learner,instructor:Instructor,erpexamcapcode:ERPExamCAPCode,erpcourseofferinglearner:ERPCourseOfferingLearner,erpcourseofferinginstructor:ERPCourseOfferingInstructor,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,erpcurseofferingbatchlearner:ERPCourseOfferingBatchLearner]
    static constraints = {
        erpcourseofferinglearner nullable:true
        erpcourseofferinginstructor nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcurseofferingbatchlearner nullable:true
        erpexamcapcode nullable:true
    }
}
