package volp

class QuizSetting {

    String name
    String fixed_variable_value
    double value

    boolean isquestionpoolapplicable
    boolean isquestionpoolreadonly
    boolean isinstituteleveltemplate
    int minquestion
    int pickupcount

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, quizexamtype : QuizExamType]

    static constraints = {
        quizexamtype nullable : true
    }

    static mapping = {
        isquestionpoolapplicable defaultValue: false
        isinstituteleveltemplate defaultValue: false
        isquestionpoolreadonly defaultValue: false
        pickupcount defaultValue:0
        minquestion defaultValue:0
    }
}
