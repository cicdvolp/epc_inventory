package volp

class ERPNotificationText
{
    String subject
    String body
    String notificationfrom
    Date sentdate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,sentby:Instructor,erpcommunicationmode:ERPCommunicationMode,erpnotificationtotype:ERPNotificationToType]
    static constraints = {
        subject nullable:true
    }

}
