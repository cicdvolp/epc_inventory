package volp

class InstructorAcademics {


    boolean ishighestqualification
    String yearofpassing
    String university  //university or Board Name
    String institute_name  //university or Board Name
    String branch
    double cpi_marks   // CPI/Marks
    String phdtopic
    static belongsTo=[organization:Organization,instructor:Instructor,instdegree:InstDegree,recclass:RecClass]

    static constraints = {

        phdtopic nullable : true
            }
    static mapping = {
        ishighestqualification defaultValue: false
    }
}
