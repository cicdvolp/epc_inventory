package volp

class ERPEntryType {

    String type   //type   (VIT::Insemester/EndSemester)    (VIIT:ISE/CE/ESE)
    String displayname
    int sort_order //by Komal
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        sort_order defaultValue:0
    }
    String toString(){
        displayname
    }
}
