package volp

class ERPLearnerAttendanceDefaulterCount {

    long defaulter_count

    static belongsTo=[erpcourseofferingbatch            : ERPCourseOfferingBatch,
                      erplearnerattendancethreshold     : ERPLearnerAttendanceThreshold]

    static constraints = { }

    static mapping = {
        defaulter_count defaultValue: 0
    }
}
