package volp

class ERPCourseOfferingInstructor {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpcourseoffering:ERPCourseOffering,instructor:Instructor,verifyinginstructor:Instructor,feedbackextrainstructor:Instructor,divisionoffering:DivisionOffering,courseoffering:CourseOffering] //courseoffering linking to volp

    static constraints = {
        courseoffering nullable :true
        instructor nullable :true
        verifyinginstructor nullable :true  //used only for VIIT exam
        feedbackextrainstructor nullable :true  //used only for VIIT feedback
    }
}
