package volp

class QuizMaster {

    String title
    String description
    double weightage
    boolean isactive
    boolean isdeleted

    String fixed_variable_value
    boolean isquestionpoolapplicable
    boolean isquestionpoolreadonly
    boolean isinstituteleveltemplate
    int minquestion
    int pickupcount
    double pickupweightage

    String quiz_fixed_variable_value
    double quiz_setting_weightage
    boolean iscentralycontrolled

    int max_extratime_count
    String max_extratime
    boolean reviewSetting

    boolean isreviewed
    boolean isCoApplicable
    boolean iskeyboarddisabled

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, erpcourse:ERPCourse, erpcourseoffering:ERPCourseOffering,
                       instructor:Instructor,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                       erpcourseofferingbatch:ERPCourseOfferingBatch, reviewer:Instructor,
                       quiztemplatemaster : QuizTemplateMaster, quizexamtype : QuizExamType, academicyear :AcademicYear,semester:Semester]

    static constraints = {
        fixed_variable_value nullable:true
        quiz_fixed_variable_value nullable:true
        max_extratime nullable:true
        reviewer nullable:true
        quizexamtype nullable:true
        quiztemplatemaster nullable:true
        erpcourseoffering nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcourseofferingbatch nullable:true
        academicyear nullable:true
        semester nullable:true
    }

    static mapping = {
        isquestionpoolapplicable defaultValue: false
        isquestionpoolreadonly defaultValue: false
        isinstituteleveltemplate defaultValue: false
        isreviewed defaultValue: false
        isdeleted defaultValue: false
        iscentralycontrolled defaultValue: false
        reviewSetting defaultValue: false
        isCoApplicable defaultValue: false
        iskeyboarddisabled defaultValue: false
        pickupcount defaultValue:0
        minquestion defaultValue:0
        pickupweightage defaultValue:0
        quiz_setting_weightage defaultValue:0
        max_extratime_count defaultValue:0
    }
}
