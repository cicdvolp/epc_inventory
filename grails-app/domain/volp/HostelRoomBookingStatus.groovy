package volp

class HostelRoomBookingStatus {

    String name   //Provisional, Confirm

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static constraints = {
    }
}
