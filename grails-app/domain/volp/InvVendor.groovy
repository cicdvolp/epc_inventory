package volp

class InvVendor {

    String company_registration_number
    String company_name
    String address
    String gst_no
    String tan_no
    String contact_person_name
    String contact_person_contact
    String contact_person_email
    String client_list                      // client list store as , saperated
    Date date_of_establishment
    double rating
    String profile_file_path
    String profile_file_name

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization : Organization]

    static hasMany=[invmaterialcategory : InvMaterialCategory]

    static constraints = {
        address nullable:true
        gst_no nullable:true
        tan_no nullable:true
        contact_person_name nullable:true
        contact_person_contact nullable:true
        contact_person_email nullable:true
        client_list nullable:true
        date_of_establishment nullable:true
        profile_file_path nullable:true
        profile_file_name nullable:true
        company_registration_number nullable:true
    }

    static mapping = {
        isactive defaultValue: true
        rating defaultValue: 0
    }
}
