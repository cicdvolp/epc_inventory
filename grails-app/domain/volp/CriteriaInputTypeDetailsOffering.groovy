package volp

class CriteriaInputTypeDetailsOffering {

    String field_name  // Table
    String field_value    //Comma separated for drop down
    int minwordcount
    int maxwordcount
    int columsequence
    boolean isoptional
    boolean isnotduplicateallow


    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ academicyear:AcademicYear, organization:Organization,
                       criterionoffering:CriterionOffering, inputtype:InputType,
                       criterionfieldtype:CriterionFieldType, criteriainputtypeoffering:CriteriaInputTypeOffering]

    static constraints = {
    }

    static mapping = {
        isoptional defaultValue: false
        isnotduplicateallow defaultValue: false
    }
}
