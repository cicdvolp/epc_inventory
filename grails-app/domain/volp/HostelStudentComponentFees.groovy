package volp

class HostelStudentComponentFees {

    double amount
    double gst_amount
    boolean isdeleted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostelfeestudentmaster:HostelFeeStudentMaster,
                      hostelfeescomponentbreakup:HostelFeesComponentBreakup,
                      hostelstudentreceipt:HostelStudentReceipt,
                      hostelfeescomponent:HostelFeesComponent, learner:Learner]

    static mapping = {
        isdeleted defaultValue: false
        gst_amount defaultValue: 0
    }

    static constraints = {
        hostelstudentreceipt unique: 'hostelfeescomponentbreakup'
    }
}
