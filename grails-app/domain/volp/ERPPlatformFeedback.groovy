package volp

class ERPPlatformFeedback {

    String feedback
    String feedback_file_path
    String feedback_file_name

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpplatformfeedbacktype:ERPPlatformFeedbackType,
                      instructor:Instructor, learner:Learner, organization:Organization]

    static constraints = {
        feedback_file_path nullable:true
        feedback_file_name nullable:true
        instructor nullable:true
        learner nullable:true
    }

}
