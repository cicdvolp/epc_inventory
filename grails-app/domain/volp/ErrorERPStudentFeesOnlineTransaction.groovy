package volp

class ErrorERPStudentFeesOnlineTransaction
{
    double amount
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization, learner:Learner,admissionapplicant:AdmissionApplicant,
                      academicyear:AcademicYear, erpstudentfeesonlinetransaction:ERPStudentFeesOnlineTransaction]
    static constraints = {
        learner nullable: true
        admissionapplicant nullable:true
    }
}