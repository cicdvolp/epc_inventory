package volp

class EntranceApplicantExperience {

    String organization_name
    String from_date
    String to_date
    String designation
    boolean is_current_job

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[recexperiencetype     : RecExperienceType,
                      entranceapplicant     : EntranceApplicant,
                      organization          : Organization]

    static constraints = {
        to_date nullable:true
    }
}
