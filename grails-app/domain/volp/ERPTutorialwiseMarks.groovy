package volp

class ERPTutorialwiseMarks {
    double actual_marks
    boolean isAbsent
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamcapcode:ERPExamCAPCode,learner:Learner,instructor:Instructor,erptutorialcomapping:ERPTutorialCOMapping,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,erpcurseofferingbatchlearner:ERPCourseOfferingBatchLearner]
    static constraints = {
        erpcourseofferingbatchinstructor nullable:true
        erpcurseofferingbatchlearner nullable:true
        erpexamcapcode nullable:true
        learner nullable:true
        instructor nullable:true
    }
    static mapping = {
        isAbsent defaultValue: false
    }
}
