package volp

class ERPLabAssesmentSchemeDetails {

    double maxmarks   //in percentage but fraction
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erplabassesmentcomponent:ERPLabAssesmentComponent,erplabassesmentscheme:ERPLabAssesmentScheme,organization:Organization]
    static constraints = {
    }
}
