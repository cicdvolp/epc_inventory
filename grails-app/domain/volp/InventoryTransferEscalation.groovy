package volp

class InventoryTransferEscalation {

    Date action_date
    String remark
    String transfer_to
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization                      : Organization,
                      inventory                         : Inventory,
                      invfinanceapprovingauthoritylevel : InvFinanceApprovingAuthorityLevel,
                      invapprovalstatus                 : InvApprovalStatus,
                      actionby                          : Instructor,
                      invtransferlevel  	            : InvTransferLevel,
                      department                        : Department    ]

    static constraints = {
        remark nullable : true
        transfer_to nullable : true
        department nullable: true
    }

    static mapping = {
        isactive defaultValue: true
    }
}

