package volp

class ERPDocumentsCategory {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    boolean isoptional

    static belongsTo=[ erpdocumenttype : ERPDocumentType,
                       organization:Organization ,
                       catergory: ERPStudentAdmissionMainCategory,
                       programtype:ProgramType,
                       year:Year,
                       admissionroundtype:AdmissionRoundType]

    static mapping = {
        isoptional defaultValue: false
    }

    static constraints = {
        programtype nullable:true
    }
}


