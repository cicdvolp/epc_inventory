package volp

class ERPCOQuestionPaperTemplateMapping {

    double weightage
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpco:ERPCO,
                      erpcourseofferingquestionpapertemplatemapping:ERPCourseOfferingQuestionPaperTemplateMapping,
                      erpquestionpapertemplatedetails:ERPQuestionPaperTemplateDetails,
                      erpquestionpapertemplateoffering:ERPQuestionPaperTemplateOffering,
                      erpcourseoffering:ERPCourseOffering]
    static constraints = {
        erpcourseoffering nullbale:true
        erpquestionpapertemplateoffering nullbale:true
    }
}
