package volp

class QuizTemplateDetails {

    int  unit_number
    double weightage
    int numberofquestions
    int minimumnumberofquestions

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, quiztemplatemaster :QuizTemplateMaster, quizquestiondifficultylevel:QuizQuestionDifficultyLevel]

    static constraints = {
    }
}
