package volp

class ERPLearnerDetentionHOD {
    boolean isApproved    //approval by HOD
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,erpdetentionpolicy:ERPDetentionPolicy,erpcourseoffering:ERPCourseOffering,erpcourseofferingbatch:ERPCourseOfferingBatch]
    static constraints = {
    }
}
