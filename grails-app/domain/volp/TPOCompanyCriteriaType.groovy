package volp

class TPOCompanyCriteriaType {

    String type  //ex.tenthpercentage
    int sortorder
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
}
