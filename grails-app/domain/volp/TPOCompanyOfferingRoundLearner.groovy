package volp

class TPOCompanyOfferingRoundLearner {

    Date round_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,tpocompanyoffering:TPOCompanyOffering,tpo:TPO,tpocompany:TPOCompany,tpocompanyofferinground:TPOCompanyOfferingRound]
    static constraints = {
    }
}
