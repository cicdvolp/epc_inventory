package volp

class CourseTypeAcademics
{
    String type
    String displayname
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,academicyear:AcademicYear]

    static constraints = {
    }
    String toString(){
        displayname
    }
    }
