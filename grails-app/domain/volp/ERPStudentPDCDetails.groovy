package volp

class ERPStudentPDCDetails {
    Date date
    String cheque_no
    Date post_date
    String bank_name
    double amount
    boolean isCleared
    String remark

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpfeestudentmaster:ERPFeeStudentMaster,learner:Learner,year:Year, collectedby: Instructor]
    static constraints = {
        remark nullable : true
        username nullable : true
        creation_date nullable : true
        updation_date nullable : true
        creation_ip_address nullable : true
        updation_ip_address nullable : true
        collectedby nullable : true
    }
    static mapping = {
        isCleared defaultValue: false
    }
}
