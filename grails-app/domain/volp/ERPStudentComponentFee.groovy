package volp

class ERPStudentComponentFee {  //erpfeesstructure  -> use only erpfeesstructure.erpfeescomponent.name_of_component ; dont use erpfeesstructuremaster and amount
    double amount
    static belongsTo=[erpstudentreceipt:ERPStudentReceipt,erpfeesstructure:ERPFeesStructure]
    static constraints = {
    }
    String toString()
    {
        amount
    }
}
