package volp

class ERPStudentProfileTabMaster {

    String name
    boolean iscompulsory
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static mapping = {
        iscompulsory defaultValue: false
    }
    static constraints = {
    }
}
