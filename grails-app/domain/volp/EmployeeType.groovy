package volp

class EmployeeType {
    String type //(Teaching/Non Teaching/class IV)
    String display_name

    boolean isloadadustmentrequired

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpselfappraisalfacultytype:ERPSelfAppraisalFacultyType,
                      employeecategory:EmployeeCategory]
    static constraints = {
        organization nullable: true
        erpselfappraisalfacultytype nullable: true
        employeecategory nullable: true
    }
    String toString(){
        return type
    }

    static mapping = {
        isloadadustmentrequired  defaultValue: false
    }
}