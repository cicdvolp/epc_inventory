package volp

class EntranceApplicationTracking {

    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization              : Organization,
                      entranceapplication       : EntranceApplication,
                      entranceauthoritylevel    : EntranceAuthorityLevel,
                      actionby                  : Instructor,
                      entranceapplicationstatus : EntranceApplicationStatus,
                      entrancerejectionreason   : EntranceRejectionReason]

    static constraints = { }
}
