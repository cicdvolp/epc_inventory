package volp

class AdhockFeesLearnerAmount {

    Double amount

    Double paid_amount
    Date paid_date

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,
                      learner:Learner,
                      academicyear:AcademicYear,
                      semester:Semester,
                      erpcommonreceipt : ERPCommonReceipt,
                      adhockfeespuropsemaster:AdhockFeesPuropseMaster]
    static hasMany = [erpcommonreceiptmany:ERPCommonReceipt]
    static constraints = {
        paid_date nullable:true
        erpcommonreceipt nullable:true
    }

    static mapping = {
        paid_amount defaultValue:0.0
        amount defaultValue:0.0
    }
}
