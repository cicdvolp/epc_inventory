package volp

class ERPCourseOfferingQuestionPaperTemplateMapping {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpquestionpapertemplateoffering:ERPQuestionPaperTemplateOffering,
                      erpassesmentschemedetails:ERPAssesmentSchemeDetails,erpcourseoffering:ERPCourseOffering,
                      erpexamconducttype:ERPExamConductType,erpevaluationtype:ERPEvaluationType,
                      erpassementtype:ERPAssementType]
    static constraints = {
    }
}
