package volp

class LeaveMaster {
    Date fromdate
    Date todate
    Date expirydate
    double numberofdays
    int vacation_slot_number

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,leavetype:LeaveType,
                      academicyear:AcademicYear,employeetype:EmployeeType,
                      vacationtype:VacationType]
    static constraints = {
        expirydate nullable: true
        vacationtype nullable: true
        employeetype nullable: true
    }
    static mapping = {
        vacation_slot_number defaultValue: 0
    }
}
