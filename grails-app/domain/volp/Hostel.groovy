package volp

class Hostel {

    String name  //Gadia, Bibwe, Swami Samarth
    String address
    String hostel_broacher_file_name // Broacher
    String hostel_broacher_file_path

    String hostel_plan_file_name // Broacher
    String hostel_plan_file_path

    String google_map_location
    String display_name

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organizationgroup:OrganizationGroup]

    static constraints = {
        name unique: 'organizationgroup'
    }

}
