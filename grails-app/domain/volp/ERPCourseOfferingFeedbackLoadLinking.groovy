package volp

class ERPCourseOfferingFeedbackLoadLinking
{
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      erpcourseoffering:ERPCourseOffering,
                      loadtypecategory:LoadTypeCategory,erpcourse:ERPCourse,
                      academicyear:AcademicYear,semester:Semester,year:Year,program:Program]
    static constraints = {
    }
}
