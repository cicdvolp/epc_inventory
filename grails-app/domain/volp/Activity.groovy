package volp

class Activity {

    String title
    String description
    String source   //University/Agency/Company/Institute
    int  count   //number_of_students
    String filename
    String filepath
    Date fromdate
    Date todate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
    static belongsTo=[organization:Organization,instructor:Instructor, activitytype: ActivityType]

}
