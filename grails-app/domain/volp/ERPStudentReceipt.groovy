package volp

class ERPStudentReceipt {
    String receipt_no     // should be linked with financial year
    double amount   //actual total amount of the receipt(current year+fees receivable paid) also includes fnexcess
    String remark
    Date date //receipt date

    boolean iscancelled

    double fees_receivable_paid
    double fnexcess   //only for fn
    //in receipt for FN

    /*
        Fees Receivable	: fees_receivable_paid
        Tution Fees	: amount - fees_receivable_paid - fnexcess
        Excess : fnexcess
     */

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpfeestudentmaster:ERPFeeStudentMaster,erpfeedepositetype:ERPFeesDepositeType,year:Year,learner:Learner,erpforeignnationalfeesstudentmaster:ERPForeignNationalFeesStudentMaster,organization:Organization]
    static hasMany = [receipttrasactions:ReceiptTransactions]
    static constraints = {
        erpfeestudentmaster nullable: true
        erpforeignnationalfeesstudentmaster nullable: true
        remark nullable: true
        organization nullable :true
        date nullable :true
    }
    String toString()
    {
        receipt_no
    }
    static mapping = {
        fees_receivable_paid defaultValue: 0
        iscancelled defaultValue: false
    }
}
