package volp

class ActivityLoggers {
    String activitydescrption
    String grno_empid
    String email
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,instructor:Instructor,roletype:RoleType,role:Role,usertype:UserType,role_link:RoleLink]
    static constraints = {
        learner nullable:true
        instructor nullable:true
    }
}
