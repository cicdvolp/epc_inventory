package volp

class LearnerDivision {

    int rollno
    String examseatno
    boolean isdeleted   // 0 - default (not deleted) , 1- means deleted
    boolean hasjoined
    Date joining_date
    String rollnumbersuffix

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[academicyear:AcademicYear,
                      semester:Semester,
                      year:Year,
                      learner:Learner,
                      program:Program, //learner program and not desh
                      shift:ERPShift,
                      divisionoffering:DivisionOffering,
                       organization:Organization,
                        guardian:Instructor,
                      divisionofferingsubjectgroupoffering:DivisionOfferingSubjectGroupOffering]
    static constraints = {
        examseatno nullable:true
        organization nullable:true
        rollnumbersuffix nullable:true
        shift nullable:true
        program nullable:true
        guardian nullable:true
        joining_date nullable:true
        divisionofferingsubjectgroupoffering nullable:true
    }

    static mapping = {
        isdeleted defaultValue: false
        hasjoined defaultValue: false
    }
}
