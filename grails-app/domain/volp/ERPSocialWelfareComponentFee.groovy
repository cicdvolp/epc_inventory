package volp

class ERPSocialWelfareComponentFee {
    double amount
    static belongsTo=[erpsocialwelfarereceipt:ERPSocialWelfareReceipt,erpfeescomponent:ERPFeesComponent]
    static constraints = {
    }
    String toString()
    {
        amount
    }
}
