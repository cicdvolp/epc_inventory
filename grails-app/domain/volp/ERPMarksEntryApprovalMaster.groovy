package volp

class ERPMarksEntryApprovalMaster {

    String faculty
    String hod
    String coe
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpassementtype:ERPAssementType,academicyear:AcademicYear,semester:Semester]
    static constraints = {
        faculty nullable: true
        hod nullable: true
        coe nullable: true
    }

}
