package volp

class HostelReceiptTransaction {

    String tid
    Date date
    double amount
    String transaction_proof_filename
    String transaction_proof_filepath
    String bank_name
    String remark

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostelpaymentmode:HostelPaymentMode,
                      learner:Learner, hostelstudentreceipt:HostelStudentReceipt,
                      hostelfeesstatus: HostelFeesStatus, hostelfeestudentmaster:HostelFeeStudentMaster,
                      hosteloffering:HostelOffering, academicyear:AcademicYear]

    static constraints = {
        tid unique: 'learner'
        hostelstudentreceipt nullable : true
        remark nullable : true
    }
}
