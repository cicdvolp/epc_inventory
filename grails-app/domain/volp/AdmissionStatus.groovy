package volp

class AdmissionStatus {

    String name //Initiated/Verified/Rejected/Pending
    String display_name //Initiated/Verified/Rejected/Pending

    boolean isvisible

    String creation_user_name
    String updation_user_name
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]
    static constraints = {
    }

    static mapping = {
        isvisible defaultValue: true
    }

    String toString(){
        name
    }
}
