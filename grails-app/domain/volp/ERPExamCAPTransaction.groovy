package volp

class ERPExamCAPTransaction
{
    Date transactiondate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamplan:ERPExamPlan,erpexamcapcode:ERPExamCAPCode,erpexamcapcodestatus:ERPExamCAPCodeStatus,capdeputedstaff:Instructor]
    static constraints = {
        capdeputedstaff nullable:true
    }
}
