package volp

class Skill {

    String name
    boolean isApproved

    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[skillcategory:SkillCategory,organization:Organization]
    static constraints = {
        organization nullable: true
    }
    String toString()
    {
        name
    }
}
