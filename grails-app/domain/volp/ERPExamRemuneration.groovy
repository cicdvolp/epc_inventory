package volp

class ERPExamRemuneration {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    double rate
    double local_conveyance  //(remuneration=rate*quantity+local_conveyance)
    static belongsTo=[organization:Organization,erpexamdutytype:ERPExamDutyType,
                      programtype:ProgramType,academicyear:AcademicYear,semester:Semester,
                      erpassementtype:ERPAssementType]
    static constraints = {
    }
    static mapping = {
        local_conveyance defaultValue:0
    }
}
