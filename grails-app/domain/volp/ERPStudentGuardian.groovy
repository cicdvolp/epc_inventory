package volp

class ERPStudentGuardian {

    boolean isCurrent
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,learner:Learner,instructor:Instructor,academicyear:AcademicYear,program:Program]
    static constraints = {
        program nullable: true
    }
    static mapping = {
        isCurrent defaultValue: true
    }
}
