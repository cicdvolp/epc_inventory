package volp

class VolpFeedback {

    String answertext
    double rating
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,learner:Learner,instructor:Instructor,volpfeedbacktype:VolpFeedbackType,volpfeedbackquestion:VolpFeedbackQuestion,volpfeedbackoption:VolpFeedbackOptions]
    static constraints = {
        answertext nullable:true
        learner nullable:true
        instructor nullable:true
        volpfeedbackoption nullable:true
    }
    static mapping = {
        rating defaultValue: -1
    }
}
