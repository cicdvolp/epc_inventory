package volp

class EmployeeLeaveBalanceSheet {
    double total_allowed_leaves
//    double total_availed_leaves
//    double total_inprocess_leaves
//    double balance
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,organization:Organization,
                      leavetype:LeaveType,employeetype:EmployeeType,
                      academicyear:AcademicYear]
    static constraints = {
        academicyear nullable: true
        username nullable: true
    }
}
