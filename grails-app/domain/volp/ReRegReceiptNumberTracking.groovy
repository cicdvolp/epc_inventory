package volp

class ReRegReceiptNumberTracking {
    String number
    String year
    boolean isactive
    static belongsTo=[organization:Organization, academicyear:AcademicYear]
    static constraints = {
        organization nullable: true
        academicyear nullable: true
    }
    static mapping = {
        isactive defaultValue: false
    }

}
