package volp

class HostelLearner {

    boolean is_last_year_hostel_inmate
    String last_year_hostel
    String last_year_roomno
    boolean is_clause_accepted
    boolean is_alergetic_to_medicine
    String alergetic_medicine_details
    boolean is_suffering_from_cronic_disease
    String cronic_disease_details

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hosteloffering:HostelOffering, academicyear:AcademicYear, learner:Learner,
                      hostel :Hostel, year : Year]

    static constraints = {
        learner unique: 'academicyear'
        hosteloffering nullable:true
        hostel nullable:true
        last_year_hostel nullable:true
        last_year_roomno nullable:true
        alergetic_medicine_details nullable:true
        cronic_disease_details nullable:true
    }

    static mapping = {
        is_clause_accepted defaultValue: false
    }
}

