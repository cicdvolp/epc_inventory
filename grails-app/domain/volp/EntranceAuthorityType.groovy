package volp

class EntranceAuthorityType {

    String name             //HOD/Registrar/Director
    boolean islastauthority

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization : Organization]

    static constraints = { }

    static mapping = {
        islastauthority defaultValue: false
    }
}
