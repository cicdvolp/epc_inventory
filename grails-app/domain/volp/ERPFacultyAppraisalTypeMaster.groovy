package volp

class ERPFacultyAppraisalTypeMaster {
    String type   //ex: course/guardian/self/combine/index
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static belongsTo=[organization:Organization]
}
