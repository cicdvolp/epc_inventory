package volp

class LearnerQuizSubmission {

    String answeripaddress
    Date submission_date
    double marks

    boolean markforreview
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[learner:Learner,
                      learnerselectedoption: QuizMCQOptions,
                      quizquestion:QuizQuestion,organization:Organization,
                      quizoffering: QuizOffering]

    static constraints = {
        learnerselectedoption nullable:true
        submission_date nullable:true
        answeripaddress nullable:true
    }

    static mapping = {
        markforreview defaultValue: false
    }
}
