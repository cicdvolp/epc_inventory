package volp

class ERPQuestionCOMapping
{
    String qno
    String opno
    double maxmarks
    boolean isfreezed
    int questiongroupno
    int optiongroupno
    int questiongrouppickupcount
    int optiongrouppickupcount
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpcourseoffering:ERPCourseOffering,
                      academicyear:AcademicYear,semester:Semester,
                      erpassesmentschemedetails:ERPAssesmentSchemeDetails,erpco:ERPCO,
                      syllabuspattern:SyllabusPattern]
    static constraints = {
        opno nullable:true
        syllabuspattern nullable:true
        academicyear nullable:true
        semester nullable:true
    }
    static mapping = {
        passing_marks defaultValue: 0
        ispassingmarksapplicable defaultValue: false
        questiongroupno defaultValue: 0
        optiongroupno defaultValue: 0
        questiongrouppickupcount defaultValue: 0
        optiongrouppickupcount defaultValue: 0
    }
}
