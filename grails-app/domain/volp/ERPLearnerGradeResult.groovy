package volp

class ERPLearnerGradeResult
{
    int total_marks
    boolean isAbsent
    boolean islast
    boolean isdetained
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[approvedby:Instructor,moderatedby:Instructor,
                      learner:Learner,erpgradedetails:ERPGradeDetails,
                      erpgradingpolicy:ERPGradingPolicy,
                      erpcourseofferinglearner:ERPCourseOfferingLearner,
                      erpcourseofferinginstructor:ERPCourseOfferingInstructor,
                      erplearnermarksheet: ERPLearnerMarkSheet,
                      erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcourseofferingbatchlearner:ERPCourseOfferingBatchLearner,erpexamconducttype:ERPExamConductType]
    static constraints = {
        moderatedby nullable:true
        erpgradingpolicy nullable:true
        erpcourseofferinginstructor nullable:true
        approvedby nullable:true
        erplearnermarksheet nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcourseofferingbatchlearner nullable:true
        erpexamconducttype nullable:true
    }
    static mapping = {
        islast defaultValue:false
        isdetained defaultValue:false
    }
}
