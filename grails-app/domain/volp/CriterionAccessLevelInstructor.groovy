package volp

class CriterionAccessLevelInstructor {

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, academicyear:AcademicYear,
                       instructor : Instructor  ,  department : Department,
                       criterionaccesslevel : CriterionAccessLevel,
                       criterionaccesstype : CriterionAccessType]

    static constraints = {
    }
}
