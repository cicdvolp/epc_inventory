package volp

class ITRSection {

    String name
    String description
    double maximumlimit
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, itroption : ITROption ]

    static mapping = {
        maximumlimit defaultValue: 0
        isactive defaultValue: true
    }

    static constraints = {
    }
}
