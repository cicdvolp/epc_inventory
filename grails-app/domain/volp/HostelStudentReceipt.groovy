package volp

class HostelStudentReceipt {

    String receipt_no
    String remark
    Date date

    boolean iscancelled

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[hostelfeestudentmaster:HostelFeeStudentMaster,
                      learner:Learner]

    static hasMany = [hostelstudentcomponentfees:HostelStudentComponentFees,
                      hostelreceipttransaction:HostelReceiptTransaction]

    static constraints = {
        receipt_no unique : true
    }

    static mapping = {
        iscancelled defaultValue: false
    }
}
