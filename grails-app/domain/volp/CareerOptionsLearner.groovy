package volp

class CareerOptionsLearner {

    int priority
    boolean isdeleted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    String prioritydetails

    static belongsTo=[learner:Learner,academicyear:AcademicYear,semester:Semester,
                      careeroptions:CareerOptions, organization:Organization]

    static constraints = {
        prioritydetails nullable: true
    }

    static mapping = {
        isdeleted defaultValue: false
        priority defaultValue:0
    }

}
