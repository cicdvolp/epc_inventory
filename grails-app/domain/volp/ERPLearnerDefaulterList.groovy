package volp

class ERPLearnerDefaulterList {

    long total_lecture_count
    long attended_lecture_count

    static belongsTo=[erpcourseofferingbatch            : ERPCourseOfferingBatch,
                      erpcourseoffering                 : ERPCourseOffering,
                      learner                           : Learner,
                      academicyear                      : AcademicYear,
                      semester                          : Semester,
                      learnerdivision                   : LearnerDivision,
                      erplearnerattendancethreshold     : ERPLearnerAttendanceThreshold]

    static constraints = {
        erpcourseofferingbatch nullable:true
        erpcourseoffering nullable:true
        learner nullable:true
        academicyear nullable:true
        semester nullable:true
        learnerdivision nullable:true
    }

    static mapping = {
        total_lecture_count defaultValue: 0
        attended_lecture_count defaultValue: 0
    }
}
