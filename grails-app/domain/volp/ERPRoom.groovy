package volp

class ERPRoom {

    String roomnumber
    String roomname
    String username
    boolean isActive
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erproomtype:ERPRoomType]
    static constraints = {
        erproomtype nullable : true
    }
    String toString(){
        roomnumber
    }
}
