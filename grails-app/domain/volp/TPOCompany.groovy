package volp

class TPOCompany {
    String name
    String companyusername     //companyname_tponame

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address


    String toString()
    {
        name
    }
    static belongsTo=[tpo:TPO,tpocompanytype:TPOCompanyType]
    static constraints = {
        name unique: true
        tpo nullable: true
        tpocompanytype nullable: true
        companyusername  nullable: true
    }
}
