package volp

class ERPLearnerCourseHistory {

    boolean ispassed     //true:passed   fail:not passed  //modifiy while update
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,
                      learner:Learner,
                      erpcourse:ERPCourse,
                      erpgradedetails:ERPGradeDetails, //modifiy while update
                      academicyear:AcademicYear,
                      semester:Semester,
                      year:Year,
                      erpcourseoffering:ERPCourseOffering,  //modifiy while update
                      erpcourseofferinglearner:ERPCourseOfferingLearner
    ]
    static constraints = {
        erpcourseofferinglearner nullable : true
    }
}
