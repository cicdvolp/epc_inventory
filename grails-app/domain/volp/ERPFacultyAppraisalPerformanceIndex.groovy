package volp

class ERPFacultyAppraisalPerformanceIndex {

    double index
    double totalmarks
    double obtainedmarks
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,academicyear:AcademicYear,instructor:Instructor,erpfacultyappraisaltypemaster:ERPFacultyAppraisalTypeMaster]
    static constraints = {
    }
    static mapping = {
        index defaultValue: 0
        totalmarks defaultValue: 0
        obtainedmarks defaultValue: 0
    }
}
