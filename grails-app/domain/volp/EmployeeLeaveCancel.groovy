package volp

class EmployeeLeaveCancel {
    String remark
    String filepath
    String filename
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[instructor:Instructor,
                      employeeleavetransaction:EmployeeLeaveTransaction]

    static constraints = {
        filename nullable : true
        filepath nullable : true
    }
}
