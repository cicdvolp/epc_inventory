package volp

class SalaryComponent {
    String name// basic ,TA,DA,HRA
    String displayname // basic ,TA,DA,HRA
    int display_order//
    int display_order_for_report//
    boolean is_considered_in_employee_master
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[salarycomponenttype:SalaryComponentType,organization:Organization]

    static constraints = {
    }
    static mapping={
        is_considered_in_employee_master defaultValue:true
        isactive defaultValue:true
    }
}
