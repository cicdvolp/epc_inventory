package volp

class ERPFeedbackCategory {

    int category_sno
    String category_display_no   //A,B,C..
    boolean notvisible
    String category_name
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpfeedbackcategorytype:ERPFeedbackCategoryType]
    static constraints = {
    }
}
