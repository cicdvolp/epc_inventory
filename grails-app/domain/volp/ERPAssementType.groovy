package volp

class ERPAssementType
{
    String type   //(VIT:TA,MSE,ESE)   (VIIT:T1,T2,CE,ESE)
    String displayname
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    String toString(){
        type
    }
}
