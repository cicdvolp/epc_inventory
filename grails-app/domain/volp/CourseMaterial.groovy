package volp

class CourseMaterial {

	String material_path
	String display_name
	String material_name//save material file with this name
    String material_link
    String meta_data
	String description
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    boolean isDeleted
	static belongsTo=[coursetopic:CourseTopic,course:Course,courseoutline:CourseOutline,fileformat:FileFormat]
    static constraints = {
        courseoutline nullable:true
        coursetopic nullable:true
        material_path nullable:true
        fileformat nullable:true
        meta_data nullable:true
        description nullable:true
        material_link nullable:true
    }
    static mapping={
        isDeleted defaultValue:false
    }
}
