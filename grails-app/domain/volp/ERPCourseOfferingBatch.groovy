package volp

class ERPCourseOfferingBatch {

    String batch_number
    String display_name
    int expectednooflecture
    boolean  isapplicabletofeedback
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourseoffering:ERPCourseOffering,divisionoffering:DivisionOffering,loadtype:LoadType,
                      erpcoursetypeacademicsloadtype:ERPCourseTypeAcademicsLoadTypes]

    static constraints = {
        batch_number nullable:true
        divisionoffering nullable:true
        erpcoursetypeacademicsloadtype nullable:true
        display_name nullable:true
    }
    static mapping = {
        expectednooflecture defaultValue : 0
        isapplicabletofeedback defaultValue : false
    }
    String toString()
    {
        batch_number + " : " + loadtype.type.type + " : " +  erpcourseoffering.erpcourse.course_code + " : " + erpcourseoffering.erpcourse.course_name
    }
}
