package volp

class ERPMonth {

    String monthname
    int monthnumber // Month number is used to get previous month and next month
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
}
