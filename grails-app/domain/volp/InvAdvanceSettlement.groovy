package volp

class InvAdvanceSettlement {

    Date amount_spent_date
    double amount_spent
    double amount_returned   //extra amount given by Accounts
    String purpose
    String file_name    //only one copy of advance proofs
    String file_path
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization              : Organization,
                      invadvancerequestreceipt  : InvAdvanceRequestReceipt,
                      invadvancerequest         : InvAdvanceRequest,
                      submissionby              : Instructor]

    static constraints = {
        file_name nullable:true
        file_path nullable:true
        invadvancerequestreceipt nullable:true
    }
    static mapping = {
        isactive defaultValue: true
        amount_spent defaultValue: 0
        amount_returned defaultValue: 0
    }

}
