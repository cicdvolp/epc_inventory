package volp

class NEFTAccountDetails {

    String bank_name
    String account_no
    String ifsc_code
    String branch
    String imps_code
    String account_name
    String branch_code
    String account_type
    String online_payment_account_no

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, programtype:ProgramType]

    static constraints = {
        programtype nullable : true
        online_payment_account_no nullable : true
    }
}
