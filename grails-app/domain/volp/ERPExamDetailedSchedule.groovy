package volp

class ERPExamDetailedSchedule
{
    int session_number   //1,2,3..
    int slot_number      //1,2
    Date date
    String timefrom   //time  ...copied from ERPExamSlot, but should be editable
    String timeto     //time ...copied from ERPExamSlot, but should be editable
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamplan:ERPExamPlan,erpday:ERPDay]
    static constraints = {
    }
}
