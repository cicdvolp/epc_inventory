package volp

class ERPCourseTopic {
    int topicno
    String topicname
    String username
    boolean isDeleted
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpcourse:ERPCourse,erpcourseunit:ERPCourseUnit,organization:Organization]
    static constraints = {
        isDeleted defaultValue:false
    }
}
