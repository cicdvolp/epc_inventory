package volp

class ERPPracticalwiseMarks  //Common for Assignment/Practical/Tutorial
{
    double actual_marks
    boolean isAbsent

    double reevalmarks
    boolean isappliedforreeval
    boolean is_approved_by_faculty
    boolean issubmitted

    double reexammarks
    boolean isappliedforreexam
    boolean is_reexam_approved_by_faculty  //second examiner will approve
    boolean isreexam_submitted   //true mean submitted,, false means NOT submitted   first examiner will submit

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,approvedby:Instructor,submittedby:Instructor,erpexamcapcode:ERPExamCAPCode,learner:Learner,instructor:Instructor,erppracticalcomapping:ERPPracticalCOMapping,erpcourseofferinginstructor:ERPCourseOfferingInstructor,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,erpcurseofferingbatchlearner:ERPCourseOfferingBatchLearner,erpcourseoffering:ERPCourseOffering,erpcourseofferinglearner:ERPCourseOfferingLearner,erpassesmentschemedetails:ERPAssesmentSchemeDetails]
    static constraints = {
        approvedby nullable:true
        erpcourseofferinginstructor nullable:true
        erpcourseoffering nullable:true
        erpcourseofferinglearner nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcurseofferingbatchlearner nullable:true
        erpexamcapcode nullable:true
        learner nullable:true
        submittedby nullable:true
        instructor nullable:true
        erpassesmentschemedetails nullable:true
    }
    static mapping = {
        isAbsent defaultValue: false

        is_approved_by_faculty defaultValue: false
        issubmitted defaultValue: false
        isappliedforreeval defaultValue:false
        reevalmarks defaultValue : -999

        isappliedforreexam defaultValue:false
        reexammarks defaultValue : -999
        is_reexam_approved_by_faculty defaultValue: false
        isreexam_submitted defaultValue: false
    }
}
