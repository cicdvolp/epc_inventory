package volp

class DivisionOfferingSubjectGroupOffering {
    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, stream:Stream, subjectgroupoffering:SubjectGroupOffering,
                      divisionoffering : DivisionOffering]

    static constraints = {
    }
}
