package volp

class ERPFeedbackCategoryOffering {

    boolean isactive   //Yes:Active   False:Inactive
    double category_weightage
    boolean isstuentcommentapplicable

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpfeedbackcategory:ERPFeedbackCategory,academicyear:AcademicYear]
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
        isstuentcommentapplicable defaultValue: true
        category_weightage defaultValue: 0
    }
}
