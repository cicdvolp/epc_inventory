package volp

class ERPFeesStructure {  //erpfeesstructuremaster use only for tracking if access trhough erpstudentcomponentfee

    double component_amount
    int collection_order
    int display_order
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[erpfeesstructuremaster:ERPFeesStructureMaster,erpfeescomponent:ERPFeesComponent]
    static constraints = {
        //component_amount scale: 2
    }
}
