package volp

class ERPCourseOfferingInstructorLoadchart
{
    int hrs
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpcourseoffering:ERPCourseOffering,instructor:Instructor,loadtype:LoadType,
                      loadtypecategory:LoadTypeCategory]
    static constraints = {
        loadtype nullable:true
    }
}
