package volp

class Semester {

	String sem

    String display_name         // used in attempt certificate form for VIT

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
        display_name nullable: true
    }
    String toString(){
        sem
    }
}
