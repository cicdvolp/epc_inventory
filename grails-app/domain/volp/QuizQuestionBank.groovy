package volp

class QuizQuestionBank {

    int qno
    int unitnumber
    String question
    boolean isactive
    boolean isapproved
    boolean iscopiedfromerpmcqquestion
    boolean isvisibletootherfaculty
    double weightage

    String question_attachment_file_name
    String question_attachment_file_path

    boolean isdeleted

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, quizquestiontype:QuizQuestionType, erpco:ERPCO,
                       quizquestiondifficultylevel:QuizQuestionDifficultyLevel, erpcourse:ERPCourse, instructor:Instructor,
                       erpmcquestionbank:ERPMCQQuestionBank]

    static constraints = {
        question_attachment_file_name nullable:true
        question_attachment_file_path nullable:true
        erpco nullable:true
        erpmcquestionbank nullable:true
    }

    static mapping = {
        isvisibletootherfaculty defaultValue: false
        iscopiedfromerpmcqquestion defaultValue: false
        isdeleted defaultValue: false
    }
}
