package volp

class Training {

    String title
    String description
    String source   //University/Agency/Company/Institute
    int number_of_participents
    String filename
    String filepath
    Date fromdate
    Date todate
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
    }
    static mapping = {
        isactive defaultValue: true
    }
    static belongsTo=[organization:Organization,instructor:Instructor, academicyear:AcademicYear, trainingstatus: TrainingStatus, trainingtype :TrainingType]

}
