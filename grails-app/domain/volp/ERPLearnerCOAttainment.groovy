package volp

class ERPLearnerCOAttainment {

    boolean isAttained
    double attained_value
    double threshold

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpcourseoffering:ERPCourseOffering, erpco:ERPCO, organization:Organization,
                      academicyear:AcademicYear, semester:Semester, learner:Learner, erpcourseofferinglearner:ERPCourseOfferingLearner]

    static constraints = {
    }

}
