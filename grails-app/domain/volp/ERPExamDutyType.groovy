package volp

class ERPExamDutyType
{
    String type  //Juniour Supervisor/Senior Supervisor/Core Comitee/Squad/Class-IV,Receiving_Officer,COE,Director,External Paper Setter,Internal Paper Setter,Paper Checking
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpexamdutycategory:ERPExamDutyCategory]
    static constraints = {
    }
}
