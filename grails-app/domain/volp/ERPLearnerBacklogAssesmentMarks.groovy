package volp

class ERPLearnerBacklogAssesmentMarks {

    double actual_marks   //actual total cowise marks  (sum of all cowise marks)  evaluated_marks
    double converted_marks    //converted marks
    boolean isAbsent
    boolean is_approved_by_faculty  //second examiner will approve
    boolean issubmitted   //true mean submitted,, false means NOT submitted   first examiner will submit
    boolean isordinanceapplied

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization,approvedby:Instructor,submittedby:Instructor,
                      learner:Learner,instructor:Instructor,erpcourseofferinginstructor:ERPCourseOfferingInstructor,
                      erpexamcapcode:ERPExamCAPCode,erpcourseofferingbatchinstructor:ERPCourseOfferingBatchInstructor,
                      erpcurseofferingbatchlearner:ERPCourseOfferingBatchLearner,erpcourseoffering:ERPCourseOffering,
                      erpassesmentschemedetails:ERPAssesmentSchemeDetails,erpcourseofferinglearner:ERPCourseOfferingLearner,
                      program:Program,
                      programtype:ProgramType,
                      academicyear:AcademicYear,
                      semester:Semester,
                      divisionoffering:DivisionOffering,erpexamconducttype:ERPExamConductType]
    static constraints = {
        learner nullable:true
        instructor nullable:true
        erpexamcapcode nullable:true
        erpcourseoffering nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcurseofferingbatchlearner nullable:true
        erpcourseofferingbatchinstructor nullable:true
        erpcourseofferinginstructor nullable:true
        erpcourseofferinglearner nullable:true
        approvedby nullable:true
        submittedby nullable:true
        program nullable:true
        programtype nullable:true
        academicyear nullable:true
        divisionoffering nullable:true
        erpexamconducttype nullable:true
    }
    static mapping = {
        isAbsent defaultValue: false
        isordinanceapplied defaultValue: false
        is_approved_by_faculty defaultValue: false
        issubmitted defaultValue: false
    }
}
