package volp

class ERPQuestionPaperTemplateOffering {

    int templatemarks
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,academicyear:AcademicYear,semester:Semester,erpquestionpapertemplate:ERPQuestionPaperTemplate,syllabuspattern:SyllabusPattern]
    static constraints = {
        syllabuspattern nullable:true
    }
    static mapping = {
        templatemarks defaultValue: 0
    }
}
