package volp

class ERPExamCAPCodeStatus {

    String status
    int status_number
    boolean isfinalstatus
    String colorcode
    String colorvalue
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
    static mapping = {
        isfinalstatus defaultValue: false
    }
}
