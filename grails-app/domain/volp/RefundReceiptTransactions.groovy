package volp

class RefundReceiptTransactions {
    String tid //chegueno//dd
    Date date
    String bank_name

    double amount

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erppaymentmode:ERPPaymentMode,erprefundreceipt:ERPRefundReceipt]

    static constraints = {
        tid nullable : true
        bank_name nullable : true
    }
}
