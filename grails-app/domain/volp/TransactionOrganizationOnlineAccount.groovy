package volp

class TransactionOrganizationOnlineAccount
{
    String merchant_code
    String merchant_key
    String merchant_iv
    String host // paytm
    boolean isactive

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization, erppaymentgatewaymaster:ERPPaymentGatewayMaster]

    static constraints = {
        host nullable : true
    }

    static mapping = {
        isactive defaultValue: false
    }
}