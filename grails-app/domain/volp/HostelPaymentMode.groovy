package volp

class HostelPaymentMode {

    String name
    boolean isvisibletostudent
    boolean isbankdetailsrequired

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organizationgroup:OrganizationGroup]

    static constraints = {
        name unique: 'organizationgroup'
    }
    static mapping = {
        isvisibletostudent defaultValue: false
        isbankdetailsrequired defaultValue: false
    }
}
