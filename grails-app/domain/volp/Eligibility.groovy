package volp

class Eligibility {

    int sort_order
    String name
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization  : Organization,
                      programtype   : ProgramType]

    static constraints = { }
}
