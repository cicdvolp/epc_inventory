package volp

class RefundReceiptNumberTracking {
    String year
    String number
    static belongsTo=[organization:Organization]
    static constraints = {
        organization nullable: true
    }

}
