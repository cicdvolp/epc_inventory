package volp

class ERPMCQQuestionAllocationtoStudent {

    String answeripaddress
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,learner:Learner,erpcourseofferinglearner:ERPCourseOfferingLearner,erpcourseoffering:ERPCourseOffering,erpmcqquestionbank:ERPMCQQuestionBank,studentselectedoption:ERPMCQOptions,erpmcqbankassementtypelinkingmaster:ERPMCQBankAssementTypeLinkingMaster]
    static constraints = {
        answeripaddress nullable:true
        studentselectedoption nullable:true
    }
}
