package volp

class ERPDocuments
{
    String documentpath
    String documentname
    String remark
    String student_remark
    boolean isoriginalsubmitted
    boolean isattestedsubmitted
    boolean ispending
    Date expected_submission_date
    boolean isreturned

    boolean isverified
    boolean isrejected
    Date verification_date

    Date submission_date
    Date return_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,academicyear:AcademicYear,erpdocumenttype:ERPDocumentType,
                      organization:Organization,admissionapplicant:AdmissionApplicant, verifyingperson : Instructor]
    static constraints = {
        learner nullable: true
        admissionapplicant nullable: true
        documentpath nullable:true
        documentname nullable:true
        remark nullable:true
        student_remark nullable:true
        submission_date nullable:true
        return_date nullable:true
        organization nullable:true
        academicyear nullable:true
        return_date nullable:true
        verification_date nullable:true
        verifyingperson nullable:true
        expected_submission_date nullable:true
    }
    static mapping = {
        isoriginalsubmitted defaultValue: false
        isattestedsubmitted defaultValue: false
        ispending defaultValue: false
        isreturned defaultValue: false
        isverified defaultValue: false
        isrejected defaultValue: false
    }
}
