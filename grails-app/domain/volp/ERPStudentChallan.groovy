package volp

class ERPStudentChallan {

    boolean isapproved
    double previouslypaid
    double tobepaid

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[erpfeestudentmaster:ERPFeeStudentMaster, learner:Learner,
                      erpforeignnationalfeesstudentmaster:ERPForeignNationalFeesStudentMaster,
                      organization:Organization, erpstudentreceipt:ERPStudentReceipt,
                      erpstudentfeesonlinetransaction:ERPStudentFeesOnlineTransaction]

    static constraints = {
        erpfeestudentmaster nullable: true
        erpforeignnationalfeesstudentmaster nullable: true
        erpstudentfeesonlinetransaction nullable: true
    }

    static mapping = {
        previouslypaid defaultValue: 0
        tobepaid defaultValue: 0
        isapproved defaultValue: false
    }
}
