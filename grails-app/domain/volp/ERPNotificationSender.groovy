package volp

class ERPNotificationSender {

    String from
    String password
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpnotificationsendingtechnique:ERPNotificationSendingTechnique,applicationtype:ApplicationType]
    static constraints = {
    }
}
