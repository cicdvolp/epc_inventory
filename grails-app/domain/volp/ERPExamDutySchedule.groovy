package volp

class ERPExamDutySchedule {

    //Juniour supervisior Duties, may have multiple entries depending upon number of students
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,instructor:Instructor,erpexamdutytype:ERPExamDutyType,erpexamplan:ERPExamPlan,erpexamroomofferingschedule:ERPExamRoomOfferingSchedule]
    static constraints = {
    }
}
