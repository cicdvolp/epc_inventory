package volp
//logic ALF/S/T/B-19[0000]
//Autonomous Late Fy/Sy...-AY0000
class LateFeesNumberTracking {
    String year
    String number
    static belongsTo=[organization:Organization]
    static constraints = {
        organization nullable: true
    }
}
