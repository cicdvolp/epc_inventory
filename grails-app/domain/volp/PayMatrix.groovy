package volp


class PayMatrix {
    int level
    double basic_pay
    double grade_pay
    double increment_percentage
    boolean isactive
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static constraints = {
        level min:1
    }
    static belongsTo=[paycommision:PayCommision,payrolldesignation:PayrollDesignation,payband:PayBand,organization:Organization]
}
