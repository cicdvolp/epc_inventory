package volp

class InvInstructorFinanceApprovingAuthority {

    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization  : Organization,
                      instructor    : Instructor,
                      invfinanceapprovingauthority : InvFinanceApprovingAuthority]

    static constraints = {}

    static mapping = {
        isactive defaultValue: true
    }
}
