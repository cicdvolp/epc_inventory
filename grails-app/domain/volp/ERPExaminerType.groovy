package volp

class ERPExaminerType
{
    String type    //internal/external
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
        organization nullable: true
    }
    String toString(){
        return type
    }
}
