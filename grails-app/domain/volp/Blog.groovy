package volp

class Blog
{
    String title
    String subtitle
    Date pushdate
    boolean ispublished
    Date publishdate
    int viewedcount
    String headerimagename
    String headerimagepath
    String body
    String keywords
    int fbcounter
    int twittercounter
    int linkedincounter
    int googlepluscounter
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static hasMany = [course:Course,coursecategory:CourseCategory]
    static belongsTo=[blogcategory:BlogCategory]
    static constraints = {
        headerimagename nullable: true
        headerimagepath nullable: true
        subtitle nullable: true
        keywords nullable: true
    }
    static mapping = {
        ispublished defaultValue: false
        viewedcount defaultValue: 0
        fbcounter defaultValue: 0
        twittercounter defaultValue: 0
        linkedincounter defaultValue: 0
        googlepluscounter defaultValue: 0
    }
}
