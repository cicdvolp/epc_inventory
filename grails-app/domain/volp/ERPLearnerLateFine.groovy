package volp

class ERPLearnerLateFine {

    double amount
    Date paymentdate
    String remark
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    String tid //chegueno//dd
    Date dddate //dd date
    String bank_name
    String receipt_no

    static belongsTo=[erplatefine:ERPLateFine,learner:Learner,academicyear:AcademicYear,erppaymentmode:ERPPaymentMode,year:Year,
                      semester:Semester,organization:Organization]
    static constraints = {
        remark nullable:true
        erppaymentmode nullable:true
        tid nullable:true
        dddate nullable:true
        bank_name nullable:true
        year nullable:true
    }
}
