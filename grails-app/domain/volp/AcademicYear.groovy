package volp

class AcademicYear
 {
	String ay
    String shortcut     //2018-19 -> 18
    String username
    boolean isactive
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[previousyear:AcademicYear,nextyear:AcademicYear]
    String toString()
    {
       ay
    }
    static constraints = {
       nextyear nullable:true
       previousyear nullable:true
    }
    static mapping = {
       isactive defaultValue: true
    }
}
