package volp

class EmployeeSalaryCertificateApplication {
    int application_number
    Date application_date
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,instructor:Instructor,employeesalarycertificatestatus:EmployeeSalaryCertificateStatus,employeesalarycertificatereason:EmployeeSalaryCertificateReason,frommonth:SalaryMonth,tomonth:SalaryMonth]
    static constraints = {
    }
}
