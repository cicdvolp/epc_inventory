package volp

class LearnerHistory
{
    String remark
    boolean isregular  //True if regular student otherwise false
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[learner:Learner,learnerstatus:LearnerStatus,year:Year,academicyear:AcademicYear,organization:Organization]
    static mapping = {
        isregular defaultValue:true
    }
    static constraints = {
    }
}
