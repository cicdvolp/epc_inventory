package volp

class TPOFeedback {

    String answer
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,instructor:Instructor,learner:Learner,tpocompanyoffering:TPOCompanyOffering,tpo:TPO,tpofeedbacktype:TPOFeedbackType,tpofeedbackquestions:TPOFeedbackQuestions,tpofeedbackoptions:TPOFeedbackOptions]
    static constraints = {
        answer nullable:true
        instructor nullable:true
        learner nullable:true
        tpocompanyoffering nullable:true
        tpo nullable:true
    }
}
