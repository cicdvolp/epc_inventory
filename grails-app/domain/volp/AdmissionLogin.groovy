package volp

class AdmissionLogin {
    String username   //DTE Application ID
    String password

    String email
    String mobile

    boolean isemailverified
    boolean ismobileverified

    boolean isloginblocked
    String access_token

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
        organization nullable:true
        access_token nullable:true
    }
}



