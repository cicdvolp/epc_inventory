package volp

class ERPHostelType {
    String type//girls A boys A
    String name //VITBOYS HOSTEL etc.
    int capacity
    int balance
    String address
    String username

    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,gender:Gender,academicyear:AcademicYear]

    static constraints = {
        address nullable:true
        name nullable:true
        balance nullable: true
    }

}
