package volp

class ERPStudentGrayQuestDetails {

    String greayquestapplicaionid
    String redirecturl

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, erpstudentfeescategorylinking : ERPStudentFeesCategoryLinking,
                       learner:Learner, erpfeestudentinstallmentdetails : ERPFeeStudentInstallmentDetails]

    static constraints = {
        erpfeestudentinstallmentdetails nullable:true
    }
}
