package volp

class EmployeeSalaryRegisterPartial {

    Date generation_date
    int working_days
    Date fromdate
    Date todate
    double gross_salary
    double gross_deductions
    double net_salary
    //double arrears
    //boolean isarears // if true then dont carry forward pick up from master
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[instructor:Instructor,employeeSalarypartialsummary:EmployeeSalaryPartialSummary,salarymonth:SalaryMonth,employeesalarymaster:EmployeeSalaryMaster,organization:Organization]
    static constraints = {
    }
    static mapping={
        gross_salary defaultValue:0.0
        gross_deductions defaultValue:0.0
        net_salary defaultValue:0.0

    }
}
