package volp

class LeaveApporvalAuthority {
    String authority //(HOD,Establishment,Registrar,Director,Trust)
    String display_name
    int minnoofleavesforinstanceapproval

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]
    static constraints = {
    }

    static mapping = {
        minnoofleavesforinstanceapproval defaultValue:0
    }
}
