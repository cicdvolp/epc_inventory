package volp

class ERPExamSlot {

    int slot_number     //default slots
    String timefrom   //time
    String timeto     //time
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization]
    static constraints = {
    }
}
