package volp

class ReRegFees {
    double totalfees
    Date date
    String remark //only if whole receipt is cancelled
    String receiptno

    boolean iscancelled

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static hasMany = [reregreceipttransaction:ReRegReceiptTransaction,erpcourseofferinglearner:ERPCourseOfferingLearner]

    static belongsTo=[learner:Learner,academicyear:AcademicYear,semester:Semester,organization:Organization]

    static constraints = {
    }

    static mapping = {
        iscancelled defaultValue: false
    }
}
