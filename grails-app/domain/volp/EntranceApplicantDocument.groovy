package volp

class EntranceApplicantDocument {

    String filepath
    String filename

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[entranceapplicant     : EntranceApplicant,
                      entrancedocumenttype  : EntranceDocumentType,
                      organization          : Organization]

    static constraints = { }
}
