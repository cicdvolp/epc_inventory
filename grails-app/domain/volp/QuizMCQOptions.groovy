package volp

class QuizMCQOptions {

    boolean isactive
    String option_name
    String option_value
    boolean iscorrect
    String mcq_option_file_name
    String mcq_option_file_path

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[ organization:Organization, quizmaster :QuizMaster, quizquestion: QuizQuestion]

    static constraints = {
        mcq_option_file_name nullable:true
        mcq_option_file_path nullable:true
    }
}
