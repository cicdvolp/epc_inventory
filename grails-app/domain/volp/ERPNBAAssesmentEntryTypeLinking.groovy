package volp

class ERPNBAAssesmentEntryTypeLinking {

    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[organization:Organization,erpnbamarksentrytype:ERPNBAMarksEntryType,erpassesmentscheme:ERPAssesmentScheme,erpassesmentschemedetails:ERPAssesmentSchemeDetails]
    static constraints = {
    }
}
