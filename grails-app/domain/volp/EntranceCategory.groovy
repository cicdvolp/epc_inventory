package volp

class EntranceCategory {

    String name     //SC/ST/OBC/OPEN/NT
    boolean isactive

    String creation_username
    String updation_username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization          : Organization,
                      entrancecategorytype  : EntranceCategoryType]

    static constraints = { }

    static mapping = {
        isactive defaultValue: true
    }

    String toString() {
        name
    }
}
