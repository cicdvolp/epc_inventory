package volp

class ERPPO
{
    int po_code
    String po_statement
    boolean ispso    //true mean PSO; false mean PO
    boolean isactive   ////run script every academic year to copy previous pos  …change AY
    boolean iscurrent  //only current PO's will be true
    String username
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address
    static belongsTo=[program:Program,academicyear:AcademicYear,organization:Organization]
    static constraints = {
    }
    String toString(){
        po_code
    }
    static mapping = {
        isactive defaultValue: true
        iscurrent defaultValue: true
    }
}
