package volp

class Year {
	String year
    String display_name
    int sort_oder
    String username    
    Date creation_date
    Date updation_date
    String creation_ip_address
    String updation_ip_address

    static belongsTo=[organization:Organization]

    static constraints = {
        organization nullable: true
    }

    String toString(){
        display_name
    }
    static mapping = {
        sort_oder defaultValue: 0
    }
}
